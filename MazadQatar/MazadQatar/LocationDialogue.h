//
//  LocationDialogue.h
//  Mzad Qatar
//
//  Created by GuruUgam on 7/23/16.
//  Copyright © 2016 Mohammed abdulfattah attia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GAITrackedViewController.h"
@protocol LocationDialogueDelegate <NSObject>
- (void)OkBtnClicked:(NSString *)requestId;
- (void)mapTypeBtnClicked: (int) index;
@optional
- (void)cancelBtnClicked:(NSString *)requestId;
@end

@interface LocationDialogue : GAITrackedViewController

@property(strong, nonatomic) IBOutlet UILabel *lblLoactionName;

- (IBAction)OkBtnClicked:(id)sender;
- (IBAction)cancelBtnClicked:(id)sender;
@property(nonatomic, weak)id<LocationDialogueDelegate> delegate;

@property(nonatomic, weak) NSString *strLocationNameText;
@property(nonatomic, weak) NSString *requestId;

@end
