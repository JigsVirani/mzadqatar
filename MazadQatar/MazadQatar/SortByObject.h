//
//  SoryByObject.h
//  Mzad Qatar
//
//  Created by samo on 8/27/14.
//  Copyright (c) 2014 Mohammed abdulfattah attia. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SortByObject : NSObject <NSCoding>

@property(strong) NSString *sortByFilterId;
@property(strong) NSString *SortByFilterName;
@property(strong) NSString *SortByFilterImageUrl;

@end
