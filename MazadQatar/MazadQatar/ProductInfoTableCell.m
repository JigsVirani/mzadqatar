//
//  ProductInfoTableCell.m
//  MazadQatar
//
//  Created by samo on 4/17/13.
//  Copyright (c) 2013 Mohammed abdulfattah attia. All rights reserved.
//

#import "ProductInfoTableCell.h"
#import <QuartzCore/QuartzCore.h>
#import "DisplayUtility.h"
#import "UserSetting.h"
#import "ServerManager.h"

@implementation ProductInfoTableCell

{
    NSString * finalPrice;
    NSMutableArray * priceArray;
}

int viewSpaceInTheProductCell = 40;
int extraProductCellHeight = 20;

- (id)initWithStyle:(UITableViewCellStyle)style
    reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
    
        
    }
    return self;
}



- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
    
}
- (IBAction)btnAdvertiseClicked:(id)sender {
    
    //    NSString *categoryId;
    
    //    if([selectedCategoryId isEqualToString:@"-1"]==true)
    //    {
    //        ProductObject* categoryObject=[[SharedData
    //        getCategories]objectAtIndex:selectedCategoryId];
    //        categoryId=categoryObject.productId;
    //    }
    //    else
    //    {
    //        categoryId=@"-1";
    //    }
    
    NSString *productTitle;
    NSString *productTitleِEnglish;
    NSString *productTitleArabic;
    
    NSString *productDescription;
    NSString *productDescriptionEnglish;
    NSString *productDescriptionArabic;
    
    NSString *selectedLangServerParam;
    //NSString *language = [[[NSBundle mainBundle] preferredLocalizations] objectAtIndex:0];
    NSString *language = [LanguageManager currentLanguageCode];
    
    if (selectedLanguageIndex == [UserSetting EnglishAndArabicLanguageKey]) {
        // set language as device locale
        if ([language isEqualToString:[ServerManager LANGUAGE_ARABIC]]) {
            productTitle = self.productTitleArabic.text;
            productDescription = self.productDescriptionArabic.text;
        } else {
            productTitle = self.productTitleEnglish.text;
            productDescription = self.productDescriptionEnglish.text;
        }
        
        productTitleِEnglish = self.productTitleEnglish.text;
        productTitleArabic = self.productTitleArabic.text;
        productDescriptionEnglish = self.productDescriptionEnglish.text;
        productDescriptionArabic = self.productDescriptionArabic.text;
        
        // set server param language to en_ar
        selectedLangServerParam = @"aren";
    } else {
        productTitle = self.productTitleTextView.text;
        productDescription = self.productDescriptionTextView.text;
        
        if (selectedLanguageIndex == 1) {
            selectedLangServerParam = @"en";
            
            productTitleِEnglish = productTitle;
            productTitleArabic = @"";
            
            productDescriptionEnglish = productDescription;
            productDescriptionArabic = @"";
        } else if (selectedLanguageIndex == 2) {
            selectedLangServerParam = @"ar";
            
            productTitleArabic = productTitle;
            productTitleِEnglish = @"";
            
            productDescriptionArabic = productDescription;
            productDescriptionEnglish = @"";
        }else if (selectedLanguageIndex == 3)
        {
           [DialogManager showErrorProductLanguage];
            return;
        }
    }
    
    [productInfoDelegate
     addAdvertiseClickedInCategory:selectedCategoryId
     withTitle:productTitle
     withTitleEnglish:productTitleِEnglish
     withTitleArabic:productTitleArabic
     withProductPrice:[self getPriceValueFromTextViews]
     withProductDescription:productDescription
     withProductDescriptionEnglish:productDescriptionEnglish
     withProductDescriptionArabic:productDescriptionArabic
     withProductLanguage:selectedLangServerParam withIsCategoryFiltersChoosed:productObject.isAllFiltersSelected];
}

- (NSString* )getPriceValueFromTextViews {
    
    NSString* priceValue = @"";
    
    BOOL hasPreviousNumber = false;
    
    if (![[[NSLocale preferredLanguages] objectAtIndex:0] containsString:@"en"]){
        
        if ( _nineNumber.text.length > 0 && [self checkHasIntegerValue:_nineNumber.text]){
            
            hasPreviousNumber = true;
            
            priceValue =  [priceValue stringByAppendingString:_nineNumber.text];
        }
        
        
        if ( _eightNumber.text.length > 0 && [self checkHasIntegerValue:_eightNumber.text]){
            hasPreviousNumber = true;
            priceValue = [priceValue stringByAppendingString:_eightNumber.text];
        }else{
            
            if(hasPreviousNumber){
                
                priceValue = [priceValue stringByAppendingString:@"0"];
                
            }
        }
        if ( _sevenNumber.text.length > 0 && [self checkHasIntegerValue:_sevenNumber.text]){
            hasPreviousNumber = true;
            priceValue =[priceValue stringByAppendingString:_sevenNumber.text];
        }else{
            
            if(hasPreviousNumber){
                
                priceValue = [priceValue stringByAppendingString:@"0"];
                
            }
        }
        if ( _sixNumber.text.length > 0 && [self checkHasIntegerValue:_sixNumber.text]){
            hasPreviousNumber = true;
            priceValue = [priceValue stringByAppendingString:_sixNumber.text];
        }else{
            
            if(hasPreviousNumber){
                
                priceValue = [priceValue stringByAppendingString:@"0"];
                
            }
        }
        if ( _fifthNumber.text.length > 0 && [self checkHasIntegerValue:_fifthNumber.text]){
            hasPreviousNumber = true;
            priceValue = [priceValue stringByAppendingString:_fifthNumber.text];
        }else{
            
            if(hasPreviousNumber){
                
                priceValue = [priceValue stringByAppendingString:@"0"];
                
            }
        }
        if ( _forthNumber.text.length > 0 && [self checkHasIntegerValue:_forthNumber.text]){
            hasPreviousNumber = true;
            priceValue = [priceValue stringByAppendingString:_forthNumber.text];
        }else{
            
            if(hasPreviousNumber){
                
                priceValue = [priceValue stringByAppendingString:@"0"];
                
            }
        }
        if ( _thirdNumber.text.length > 0 && [self checkHasIntegerValue:_thirdNumber.text]){
            hasPreviousNumber = true;
            priceValue = [priceValue stringByAppendingString:_thirdNumber.text];
        }else{
            
            if(hasPreviousNumber){
                
                priceValue = [priceValue stringByAppendingString:@"0"];
                
            }
        }
        if ( _secondNumber.text.length > 0 && [self checkHasIntegerValue:_secondNumber.text]){
            hasPreviousNumber = true;
            priceValue = [priceValue stringByAppendingString:_secondNumber.text];
        }else{
            
            if(hasPreviousNumber){
                
                priceValue = [priceValue stringByAppendingString:@"0"];
                
            }
        }
        if ( _firstNumber.text.length > 0 && [self checkHasIntegerValue:_firstNumber.text]){
            hasPreviousNumber = true;
            priceValue = [priceValue stringByAppendingString:_firstNumber.text];
        }else{
            
            if(hasPreviousNumber){
                
                priceValue = [priceValue stringByAppendingString:@"0"];
                
            }
        }
        
    }else{
        
        if ( _firstNumber.text.length > 0 && [self checkHasIntegerValue:_firstNumber.text]){
            
            hasPreviousNumber = true;
            
            priceValue =  [priceValue stringByAppendingString:_firstNumber.text];
        }
        
        
        if ( _secondNumber.text.length > 0 && [self checkHasIntegerValue:_secondNumber.text]){
            hasPreviousNumber = true;
            priceValue = [priceValue stringByAppendingString:_secondNumber.text];
        }else{
            
            if(hasPreviousNumber){
                
                priceValue = [priceValue stringByAppendingString:@"0"];
                
            }
        }
        if ( _thirdNumber.text.length > 0 && [self checkHasIntegerValue:_thirdNumber.text]){
            hasPreviousNumber = true;
            priceValue =[priceValue stringByAppendingString:_thirdNumber.text];
        }else{
            
            if(hasPreviousNumber){
                
                priceValue = [priceValue stringByAppendingString:@"0"];
                
            }
        }
        if ( _forthNumber.text.length > 0 && [self checkHasIntegerValue:_forthNumber.text]){
            hasPreviousNumber = true;
            priceValue = [priceValue stringByAppendingString:_forthNumber.text];
        }else{
            
            if(hasPreviousNumber){
                
                priceValue = [priceValue stringByAppendingString:@"0"];
                
            }
        }
        if ( _fifthNumber.text.length > 0 && [self checkHasIntegerValue:_fifthNumber.text]){
            hasPreviousNumber = true;
            priceValue = [priceValue stringByAppendingString:_fifthNumber.text];
        }else{
            
            if(hasPreviousNumber){
                
                priceValue = [priceValue stringByAppendingString:@"0"];
                
            }
        }
        if ( _sixNumber.text.length > 0 && [self checkHasIntegerValue:_sixNumber.text]){
            hasPreviousNumber = true;
            priceValue = [priceValue stringByAppendingString:_sixNumber.text];
        }else{
            
            if(hasPreviousNumber){
                
                priceValue = [priceValue stringByAppendingString:@"0"];
                
            }
        }
        if ( _sevenNumber.text.length > 0 && [self checkHasIntegerValue:_sevenNumber.text]){
            hasPreviousNumber = true;
            priceValue = [priceValue stringByAppendingString:_sevenNumber.text];
        }else{
            
            if(hasPreviousNumber){
                
                priceValue = [priceValue stringByAppendingString:@"0"];
                
            }
        }
        if ( _eightNumber.text.length > 0 && [self checkHasIntegerValue:_eightNumber.text]){
            hasPreviousNumber = true;
            priceValue = [priceValue stringByAppendingString:_eightNumber.text];
        }else{
            
            if(hasPreviousNumber){
                
                priceValue = [priceValue stringByAppendingString:@"0"];
                
            }
        }
        if ( _nineNumber.text.length > 0 && [self checkHasIntegerValue:_nineNumber.text]){
            hasPreviousNumber = true;
            priceValue = [priceValue stringByAppendingString:_nineNumber.text];
        }else{
            
            if(hasPreviousNumber){
                
                priceValue = [priceValue stringByAppendingString:@"0"];
            }
        }
    }
    
    productObject.productPrice= priceValue;
    
    return priceValue;
    
}

- (BOOL)checkHasIntegerValue:(NSString *) text {
    
    int value = [text intValue];
    
    if(value > 0)
        return true;
        
        return false;
    
}
- (void)setProductInfoCell:(ProductObject *)productObjectParam
             andParentView:(UIViewController *)parentViewParam
               andDelegate:(id<ProductInfoTableCellDelegate>)delegate
         fromEditAdvertise:(BOOL)isEditAdvertise
{
    
    self.lblAddAdvertiseRight.text = NSLocalizedString(@"Add_Adv_InfoText", nil);
    
    
    if([[SharedData getDeviceLanguage]isEqualToString:@"en"]){
        
        [self.productTitleTextView setTextAlignment:NSTextAlignmentLeft];
        [self.productTitleEnglish setTextAlignment:NSTextAlignmentLeft];
        [self.productTitleArabic setTextAlignment:NSTextAlignmentLeft];
        
        [self.productDescriptionTextView setTextAlignment:NSTextAlignmentLeft];
        [self.productDescriptionEnglish setTextAlignment:NSTextAlignmentLeft];
        [self.productDescriptionArabic setTextAlignment:NSTextAlignmentLeft];
        
        [self.languageOfAdvertiseButton setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
        [self.btnPriceInfoDetail setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
        [self.categoryBtn setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
        [self.subCategoryBtn setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
        [self.productTitleTextView setTextAlignment:NSTextAlignmentLeft];
        
        [self.btnLangArrow setImage:[UIImage imageNamed:@"ic_english_arrow"] forState:UIControlStateNormal];
        [self.btnCatArrow setImage:[UIImage imageNamed:@"ic_english_arrow"] forState:UIControlStateNormal];
        [self.btnFilterArrow setImage:[UIImage imageNamed:@"ic_english_arrow"] forState:UIControlStateNormal];
        
        [self.languageOfAdvertiseButton setTitleEdgeInsets:UIEdgeInsetsMake(0, 8, 0, 0)];
        [self.categoryBtn setTitleEdgeInsets:UIEdgeInsetsMake(0, 8, 0, 0)];
        [self.subCategoryBtn setTitleEdgeInsets:UIEdgeInsetsMake(0, 8, 0, 0)];
        
    }else{
        
        [self.productTitleTextView setTextAlignment:NSTextAlignmentRight];
        [self.productTitleEnglish setTextAlignment:NSTextAlignmentRight];
        [self.productTitleArabic setTextAlignment:NSTextAlignmentRight];
        
        [self.productDescriptionTextView setTextAlignment:NSTextAlignmentRight];
        [self.productDescriptionEnglish setTextAlignment:NSTextAlignmentRight];
        [self.productDescriptionArabic setTextAlignment:NSTextAlignmentRight];
        
        [self.languageOfAdvertiseButton setContentHorizontalAlignment:UIControlContentHorizontalAlignmentRight];
        [self.btnPriceInfoDetail setContentHorizontalAlignment:UIControlContentHorizontalAlignmentRight];
        [self.categoryBtn setContentHorizontalAlignment:UIControlContentHorizontalAlignmentRight];
        [self.subCategoryBtn setContentHorizontalAlignment:UIControlContentHorizontalAlignmentRight];        
        [self.productTitleTextView setTextAlignment:NSTextAlignmentRight];
        
        [self.btnLangArrow setImage:[UIImage imageNamed:@"ic_arabic_arrow"] forState:UIControlStateNormal];
        [self.btnCatArrow setImage:[UIImage imageNamed:@"ic_arabic_arrow"] forState:UIControlStateNormal];
        [self.btnFilterArrow setImage:[UIImage imageNamed:@"ic_arabic_arrow"] forState:UIControlStateNormal];
        
        [self.languageOfAdvertiseButton setTitleEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 8)];
        [self.categoryBtn setTitleEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 8)];
        [self.subCategoryBtn setTitleEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 8)];
    }
    productInfoDelegate = delegate;
    [self createKeyboardAccessoryView];
    
    [self.firstNumber setInputAccessoryView:inputAccView];
    [self.secondNumber setInputAccessoryView:inputAccView];
    [self.thirdNumber setInputAccessoryView:inputAccView];
    [self.forthNumber setInputAccessoryView:inputAccView];
    [self.fifthNumber setInputAccessoryView:inputAccView];
    [self.sixNumber setInputAccessoryView:inputAccView];
    [self.sevenNumber setInputAccessoryView:inputAccView];
    [self.eightNumber setInputAccessoryView:inputAccView];
    [self.nineNumber setInputAccessoryView:inputAccView];
    
    productObject = productObjectParam;
    
    if (isEditAdvertise) {
        if([productObject.productAdvertiseTypeId isEqualToString:@""]==false)
        {
            productObject.isAllFiltersSelected=true;
        }
        [self.btnAddAdvertise
         setTitle:NSLocalizedString(@"EDIT_ADVERTISE_BTN_TEXT", nil)
         forState:UIControlStateNormal];
    }
    
    [self restoreProductInfo:isEditAdvertise];
    
    parentView = parentViewParam;
}

- (void) loadText
{
    if (selectedLanguageIndex == 1) {
        self.productTitleLabel.text = NSLocalizedString(@"Ad name in English",nil);
        self.productDescriptionLabel.text = NSLocalizedString(@"Ad description in English",nil);
    }else if (selectedLanguageIndex == 2)
    {
        self.productTitleLabel.text = NSLocalizedString(@"Ad name in Arabic",nil);
        self.productDescriptionLabel.text = NSLocalizedString(@"Ad description in Arabic",nil);
    }
}
- (void)restoreProductInfo:(BOOL)isEditAdvertise {
    if (isEditAdvertise) {
        // restore the product info details
        selectedLanguageIndex =
        [UserSetting getAdvertiseTypeServerIndex:productObject.productLanguage];
        [self loadText];
        [self.languageOfAdvertiseButton
         setTitle:[UserSetting
                   getUserAddAdvertiseLanguageName:
                   [UserSetting getAdvertiseTypeServerIndex:
                    productObject.productLanguage]]
         forState:UIControlStateNormal];
        
     
        
        if ([productObject.productLanguage
             isEqualToString:[UserSetting ENGLISH_ARABIC_CONSTANT_STRING]]) {
            self.productTitleEnglish.text = productObject.productTilteEnglish;
            self.productTitleArabic.text = productObject.productTilteArabic;
            self.productDescriptionEnglish.text =
            productObject.productDescriptionEnglish;
            self.productDescriptionArabic.text =
            productObject.productDescriptionArabic;
        } else {
            self.productTitleTextView.text = productObject.productTilte;
            self.productDescriptionTextView.text = productObject.productDescription;
        }
        
    } else {
        selectedLanguageIndex = [UserSetting getUserAddAdvertiseLanguage];
        [self loadText];
        [self.languageOfAdvertiseButton
         setTitle:[UserSetting getUserAddAdvertiseLanguageName:
                   [UserSetting getUserAddAdvertiseLanguage]]
         forState:UIControlStateNormal];
       // [self.languageOfAdvertiseButton setTitle:NSLocalizedString(@"SELECT_ADD_LANGAUGE", nil) forState:UIControlStateNormal];
    }
    
    if([productObject.productCategoryName isEqualToString:@""]==false&&productObject.productCategoryName!=nil)
    {
    [self.categoryBtn setTitle:productObject.productCategoryName
                      forState:UIControlStateNormal];
    selectedCategoryId = productObject.productCategoryId;
    }
    
//    if([productObject.productSubCategoryName isEqualToString:@""]==false&&productObject.productSubCategoryName!=nil)
//    {
//        [self.subCategoryBtn setTitle:productObject.productSubCategoryName
//                             forState:UIControlStateNormal];
//        selectedSubCategoryId = productObject.productSubCategoryId;
//    }
    
    if(productObject.isAllFiltersSelected==true)
    {
        [self.subCategoryBtn setTitle:NSLocalizedString(@"All_FILTERS_SELECTED", nil)
                             forState:UIControlStateNormal];
    }
    
    if(productObject.productTilte!=nil)
    {
        [self.productTitleTextView setText:productObject.productTilte];
    }
    if(productObject.productTilteEnglish!=nil)
    {
        [self.productTitleEnglish setText:productObject.productTilteEnglish];

    }
    if(productObject.productTilteArabic!=nil)
    {
        [self.productTitleArabic setText:productObject.productTilteArabic];

    }
    if(productObject.productPrice!=nil)
    {
        
        if (productObject.productPrice.length > 0 ) {
            
            NSInteger len = productObject.productPrice.length;
            
            NSUInteger virtualIndex = 9 - len;
            NSInteger i = len-1;
            
            for(  ; i >= 0 ; i--){
                
                NSString *s = [NSString stringWithFormat:@"%c", [productObject.productPrice characterAtIndex:i]];
                NSInteger value;
                if (![[[NSLocale preferredLanguages] objectAtIndex:0] containsString:@"en"]){
                    value = (8-(i + virtualIndex));
                }else
                {
                    value = (i + virtualIndex);
                }
                switch (value) {
                    case 0:
                        [_firstNumber setText:s];
                        break;
                    case 1:
                        [_secondNumber setText:s];
                        break;
                    case 2:
                        [_thirdNumber setText:s];
                        break;
                    case 3:
                        [_forthNumber setText:s];
                        break;
                    case 4:
                        [_fifthNumber setText:s];
                        break;
                    case 5:
                        [_sixNumber setText:s];
                        break;
                    case 6:
                        [_sevenNumber setText:s];
                        break;
                    case 7:
                        [_eightNumber setText:s];
                        break;
                    case 8:
                        [_nineNumber setText:s];
                        break;
                        
                        
                        
                    default:
                        break;
                }
            }
        }

    }
    if(productObject.productDescription!=nil)
    {
        [self.productDescriptionTextView setText:productObject.productDescription];

    }
    if(productObject.productDescriptionEnglish!=nil)
    {
        [self.productDescriptionEnglish setText:productObject.productDescriptionEnglish];

    }
    if(productObject.productDescriptionArabic!=nil)
    {
        [self.productDescriptionArabic setText:productObject.productDescriptionArabic];

    }
    
}
//=================================================delegate



-(void)textViewDidEndEditing:(UITextView *)textView
{
    [super textViewDidEndEditing: textView];
    
    if(textView == self.productTitleTextView)
    {
        productObject.productTilte=textView.text;
    }
    else if(textView == self.productTitleEnglish)
    {
        productObject.productTilteEnglish=textView.text;
    }
    else if(textView == self.productTitleArabic)
    {
        productObject.productTilteArabic=textView.text;
    }

    
    else if(textView == self.productDescriptionTextView)
    {
        productObject.productDescription=textView.text;
    }
    else if(textView == self.productDescriptionEnglish)
    {
        productObject.productDescriptionEnglish=textView.text;
        
    }
    else if(textView == self.productDescriptionArabic)
    {
        productObject.productDescriptionArabic=textView.text;
    }else   if (textView == self.firstNumber ||
                textView == self.secondNumber ||
                textView == self.thirdNumber||
                textView == self.forthNumber||
                textView == self.fifthNumber ||
                textView == self.sixNumber ||
                textView == self.sevenNumber||
                textView == self.eightNumber||
                textView == self.nineNumber
                ) {
        if ([textView.text length]==0) {
            textView.text=@"0";
        }
        productObject.productPrice = [self getPriceValueFromTextViews];
    }
}

-(void) getPriceOfProduct
{
    priceArray = [NSMutableArray new];
    
}

//- (void)categoryChoosed:(int)index andCategoryId:(NSString *)categoryId;
- (void)categoryChoosed:(int)index andCategoryId:(NSString *)categoryId withArray:(NSMutableArray *)arrCategory;
{
    selectedCategoryId = categoryId;
    
    /*ProductObject *categoryObject =
    [[SharedData getCategories] objectAtIndex:index];*/
     ProductObject *categoryObject = [arrCategory objectAtIndex:index];
    
    [self.categoryBtn setTitle:categoryObject.productTilte
                      forState:UIControlStateNormal];
    
    [self.subCategoryBtn setTitle:NSLocalizedString(@"CHOOSE_CATEGORY_FILTERS", NIL)
                         forState:UIControlStateNormal];
    
    productObject.productCategoryId = categoryId;
    productObject.productCategoryName = categoryObject.productTilte;
    // reset subcategory
    [self resetFitlers];
    
    [productInfoDelegate productInfoChanged:productObject];
}

-(void)resetFitlers
{
    productObject.isAllFiltersSelected=false;
    productObject.productSubCategoryId = @"";
    productObject.productSubCategoryName =nil;
    productObject.productAdvertiseTypeId=@"";
    productObject.productAdvertiseTypeName=nil;
    productObject.productCityId=@"";
    productObject.productCityName=nil;
    productObject.productRegionId=@"";
    productObject.productRegionName=nil;
    productObject.productSubsubCategoryId=@"";
    productObject.productSubsubCategoryName=nil;
    productObject.productFurnishedTypeId=@"";
    productObject.productFurnishedTypeName=nil;
    productObject.productManfactureYear=@"";
    productObject.productKm=@"";
    productObject.productNumberOfRooms=@"";
}

- (IBAction)btnCategoryClicked:(id)sender {
    
    // rotation listener
    NSNotificationCenter *nc =
    [NSNotificationCenter defaultCenter]; // Get the notification centre for
    // the app
    [nc removeObserver:self
                  name:UIApplicationDidChangeStatusBarOrientationNotification
                object:nil];
    
    // issue in keyboard when moving another screen
    [parentView.view endEditing:YES];
    
    /*CategoryChooser *categoryChooser = [parentView.storyboard
                                        instantiateViewControllerWithIdentifier:@"CategoryChoserID"];*/
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Company" bundle:nil];
    CategoryChooser *categoryChooser = [storyboard instantiateViewControllerWithIdentifier:@"CategoryChoserID"];
    [categoryChooser setIsFromAddAdvts:true];
    [categoryChooser setSelectedCategoryId:selectedCategoryId];
    [categoryChooser setDelegate:self];
    
    [[parentView navigationController] pushViewController:categoryChooser
                                                 animated:YES];
}

- (IBAction)btnCategoryFilterClicked:(id)sender {
    if (selectedCategoryId==nil) {
        [DialogManager showChooseCategoryFirst];
        return;
    }
    
    // rotation listener
    NSNotificationCenter *nc =
    [NSNotificationCenter defaultCenter]; // Get the notification centre for
    // the app
    [nc removeObserver:self
                  name:UIApplicationDidChangeStatusBarOrientationNotification
                object:nil];
    
    // issue in keyboard when moving another screen
    [parentView.view endEditing:YES];
    
    for (ProductObject *productObjectLoadedFromServer in
         [SharedData getCategories]) {
        if ([productObjectLoadedFromServer.productId
             isEqualToString:productObject.productCategoryId]) {
            productObject.productFilterType =
            productObjectLoadedFromServer.productFilterType;
        }
    }
    
    if ([productObject.productFilterType
         isEqualToString:[SharedData CAR_FILTER_VALUE]]) {
        /*AddCarFiltersViewController *addCarFiltersViewController = [parentView
                                                                    .storyboard
                                                                    instantiateViewControllerWithIdentifier:@"AddCarFiltersViewController"];*/
        AddCarFiltersViewController *addCarFiltersViewController = [[SharedData getCurrentStoryBoard]instantiateViewControllerWithIdentifier:@"AddCarFiltersViewController"];
        
        [addCarFiltersViewController setProductObjectUi:productObject];
        [addCarFiltersViewController setDelegate:self];
        addCarFiltersViewController.view.backgroundColor = [UIColor blackColor];
        [[parentView navigationController]
         pushViewController:addCarFiltersViewController
         animated:YES];
    } else if ([productObject.productFilterType
                isEqualToString:[SharedData PROPERTY_FILTER_VALUE]]) {
        
        /*AddPropertyFiltersViewController *addPropertyFiltersViewController =
        [parentView.storyboard instantiateViewControllerWithIdentifier:
         @"AddPropertyFiltersViewController"];*/
        
        AddPropertyFiltersViewController *addPropertyFiltersViewController =
        [[SharedData getCurrentStoryBoard] instantiateViewControllerWithIdentifier:
         @"AddPropertyFiltersViewController"];
        
        addPropertyFiltersViewController.view.backgroundColor = [UIColor blackColor];
        [addPropertyFiltersViewController setProductObject:productObject];
        [addPropertyFiltersViewController setDelegate:self];
        
        [[parentView navigationController]
         pushViewController:addPropertyFiltersViewController
         animated:YES];
    } else {
        
        /*AddOtherFiltersViewController *addOtherFiltersViewController =
        [parentView.storyboard instantiateViewControllerWithIdentifier:
         @"AddOtherFiltersViewController"];*/
        
        AddOtherFiltersViewController *addOtherFiltersViewController =
        [[SharedData getCurrentStoryBoard] instantiateViewControllerWithIdentifier:
         @"AddOtherFiltersViewController"];
        
        addOtherFiltersViewController.view.backgroundColor = [UIColor blackColor];
        [addOtherFiltersViewController setProductObjectUi:productObject];
        [addOtherFiltersViewController setDelegate:self];
        
        [[parentView navigationController]
         pushViewController:addOtherFiltersViewController
         animated:YES];
    }
}

//- (void)subCategoryChoosedInList:(NSString *)subCategoryId
//              andSubCategoryName:(NSString *)subCategorName {
//    selectedSubCategoryId = subCategoryId;
//    
//    [self.subCategoryBtn setTitle:subCategorName forState:UIControlStateNormal];
//    
//    productObject.productSubCategoryName = subCategorName;
//    productObject.productSubCategoryId = subCategoryId;
//    
//    [productInfoDelegate productInfoChanged:productObject];
//}

- (IBAction)chooseLanguageClicked:(id)sender {
    // rotation listener
    NSNotificationCenter *nc =
    [NSNotificationCenter defaultCenter]; // Get the notification centre for
    // the app
    [nc removeObserver:self
                  name:UIApplicationDidChangeStatusBarOrientationNotification
                object:nil];
    
    // issue in keyboard when moving another screen
    [parentView.view endEditing:YES];
    
    /*LanguageChooserViewController *languageChooser = [parentView.storyboard
                                                      instantiateViewControllerWithIdentifier:@"LanguageChooserViewController"];*/
    LanguageChooserViewController *languageChooser = [[SharedData getCurrentStoryBoard]
                                                      instantiateViewControllerWithIdentifier:@"LanguageChooserViewController"];
    
    [languageChooser setSelectedIndex:selectedLanguageIndex];
    [languageChooser setDelegate:self];
    
    [[parentView navigationController] pushViewController:languageChooser
                                                 animated:YES];
}

- (void)languageChanged:(int)index withLanguageName:(NSString *)languageName {
    
    [self resetProductText];
    
    selectedLanguageIndex = index;
    
    [self.languageOfAdvertiseButton setTitle:languageName
                                    forState:UIControlStateNormal];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [productInfoDelegate addAdvertiseLanguageChanged:index];
    });
}

-(void)resetProductText
{
    productObject.productTilte=nil;
    productObject.productTilteEnglish=nil;
    productObject.productTilteArabic=nil;
    productObject.productDescription=nil;
    productObject.productDescriptionEnglish=nil;
    productObject.productDescriptionArabic=nil;
}

- (void)carFiltersChoosedWithProductObject:(ProductObject *)productObjectParam {
    
    productObject = productObjectParam;
    
    [self.subCategoryBtn setTitle:NSLocalizedString(@"All_FILTERS_SELECTED", nil)
                         forState:UIControlStateNormal];
    
    [productInfoDelegate productInfoChanged:productObjectParam];
}

- (void)propertyFiltersChoosedWithProductObject:
(ProductObject *)productObjectParam {
    
    productObject = productObjectParam;
    
    [self.subCategoryBtn setTitle:NSLocalizedString(@"All_FILTERS_SELECTED", nil)
                         forState:UIControlStateNormal];
    
    [productInfoDelegate productInfoChanged:productObjectParam];
}

- (void)otherFiltersChoosedWithProductObject:
(ProductObject *)productObjectParam {
    
    productObject = productObjectParam;
    
    [self.subCategoryBtn setTitle:NSLocalizedString(@"All_FILTERS_SELECTED", nil)
                         forState:UIControlStateNormal];
    
    [productInfoDelegate productInfoChanged:productObjectParam];
}

//====================================================limit the characters
- (BOOL)textView:(UITextView *)textView
shouldChangeTextInRange:(NSRange)range
 replacementText:(NSString *)text {
//    if (textView == _productTitleTextView || textView == _productTitleEnglish ||
//        textView == _productTitleArabic) {
//        [super textView:textView
//shouldChangeTextInRange:range
//        replacementText:text];
//    }
    
    if (textView == self.productTitleTextView ||
        textView == self.productTitleEnglish ||
        textView == self.productTitleArabic ) {
        NSUInteger newLength =
        [textView.text length] + [text length] - range.length;
        return (newLength > 18) ? NO : YES;
    }
    if (textView == self.firstNumber ||
        textView == self.secondNumber ||
        textView == self.thirdNumber||
        textView == self.forthNumber||
        textView == self.fifthNumber ||
        textView == self.sixNumber ||
        textView == self.sevenNumber||
        textView == self.eightNumber||
        textView == self.nineNumber
        ) {
        NSUInteger newLength = [textView.text length] + [text length] - range.length;
        
        if([text length] == 0 || [textView.text isEqualToString:@"0"]){
            textView.text = @"";
            return YES;
        }else if (textView.text.length == 1){
            return NO;
        }else{
            return YES;
        }

    }
    if (textView == _productDescriptionTextView) {
        NSUInteger newLength =
        [textView.text length] + [text length] - range.length;
        return (newLength > 500) ? NO : YES;
    }
    
    if (textView == _productDescriptionEnglish) {
        NSUInteger newLength =
        [textView.text length] + [text length] - range.length;
        return (newLength > 500) ? NO : YES;
    }
    
    if (textView == _productDescriptionArabic) {
        NSUInteger newLength =
        [textView.text length] + [text length] - range.length;
        return (newLength > 500) ? NO : YES;
    }
    
    return YES;
}

//=================================================keyboard accessor view
-(BOOL)textViewShouldBeginEditing:(UITextView *)textView {
    if (textView == self.firstNumber ||
        textView == self.secondNumber ||
        textView == self.thirdNumber||
        textView == self.forthNumber||
        textView == self.fifthNumber ||
        textView == self.sixNumber ||
        textView == self.sevenNumber||
        textView == self.eightNumber||
        textView == self.nineNumber
        ) {
        if ([textView.text isEqualToString:@"0"]) {
            textView.text=@"";
        }
    }
    return YES;
}

- (void)textViewDidBeginEditing:(UITextView *)textView {
    
    _currentSelectedTextView = textView;
    
    [self createKeyboardAccessoryView];
    
    [super textViewDidBeginEditing:textView];
}

- (void)createKeyboardAccessoryView {
    if ([DisplayUtility isPad] == false) {
        
        // Call the createInputAccessoryView method we created earlier.
        // By doing that we will prepare the inputAccView.
        [self createInputAccessoryView];
        
        // Now add the view as an input accessory view to the selected textfield.
        [_productTitleTextView setInputAccessoryView:inputAccView];
        [_productTitleEnglish setInputAccessoryView:inputAccView];
        [_productTitleArabic setInputAccessoryView:inputAccView];
        
        //[_currentSelectedTextView setInputAccessoryView:inputAccView];
        [_productDescriptionTextView setInputAccessoryView:inputAccView];
        [_productDescriptionEnglish setInputAccessoryView:inputAccView];
        [_productDescriptionArabic setInputAccessoryView:inputAccView];
    }
}

- (void)orientationChanged:(NSNotification *)note {
    if (parentView.view != nil) {
        isRotating = YES;
        
        [self recreateInputAccessoryView];
        // issue in keyboard when moving another screen
        [parentView.view endEditing:YES];
        //[self createKeyboardAccessoryView];
        //[self.view reloadInputViews];
        isRotating = NO;
    }
}

- (void)recreateInputAccessoryView {
    
    CGSize screenSize = [DisplayUtility getScreenSize];
    
    [inputAccView setFrame:CGRectMake(10.0, 0.0, screenSize.width, 40.0)];
    
    [btnDone setFrame:CGRectMake(screenSize.width - 90, 3, 80.0f, 40.0f)];
    
    
    
    [_productTitleEnglish setInputAccessoryView:inputAccView];
    [_productTitleArabic setInputAccessoryView:inputAccView];
    [_currentSelectedTextView setInputAccessoryView:inputAccView];
    [_productDescriptionTextView setInputAccessoryView:inputAccView];
    [_productDescriptionEnglish setInputAccessoryView:inputAccView];
    [_productDescriptionArabic setInputAccessoryView:inputAccView];
}


- (void)doneTyping {
    // When the "done" button is tapped, the keyboard should go away.
    // That simply means that we just have to resign our first responder.
    [_productTitleTextView resignFirstResponder];
    [_productTitleEnglish resignFirstResponder];
    [_productTitleArabic resignFirstResponder];
    [_productDescriptionTextView resignFirstResponder];
    [_productDescriptionEnglish resignFirstResponder];
    [_productDescriptionArabic resignFirstResponder];
  [_currentSelectedTextView resignFirstResponder];
}

- (void)dealloc {
    
    // UIDevice *device = [UIDevice currentDevice];					//Get the device
    // object
    // [device endGeneratingDeviceOrientationNotifications];			//Tell it to
    // start monitoring the accelerometer for orientation
    NSNotificationCenter *nc =
    [NSNotificationCenter defaultCenter]; // Get the notification centre for
    // the app
    [nc removeObserver:self
                  name:UIApplicationDidChangeStatusBarOrientationNotification
                object:nil];
    
    //[btnDone release];
    //[inputAccView release];
    
    //----- SETUP DEVICE ORIENTATION CHANGE NOTIFICATION -----
}
@end
