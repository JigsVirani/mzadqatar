//
//  SocialShareUtility.h
//  Mzad Qatar
//
//  Created by Mohammed AbdulFattah on 11/12/14.
//  Copyright (c) 2014 Mohammed abdulfattah attia. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Social/Social.h>

@interface SocialShareUtility : NSObject

@property(nonatomic, strong)
    UIDocumentInteractionController *documentInteractionController;

- (void)shareOnFacebookInView:(UIViewController *)parentView
                     withText:(NSString *)shareText
                    withImage:(UIImage *)shareImage
                      withUrl:(NSString *)shareUrl;
- (void)shareOnTwitterInView:(UIViewController *)parentView
                    withText:(NSString *)shareText
                   withImage:(UIImage *)shareImage
                     withUrl:(NSString *)shareUrl;
- (void)shareOnInstagramInView:(UIViewController *)parentView
                     withImage:(UIImage *)shareImage;
- (void)shareOnWhatsAppInView:(UIViewController *)parentView
                     withText:(NSString *)shareText;
- (void)shareOnOtherAppsInView:(UIViewController *)parentView
            andFrameToSHowFrom:(CGRect)frame
                      withText:(NSString *)shareText
                     withImage:(UIImage *)shareImage
                       withUrl:(NSString *)shareUrl;

@end
