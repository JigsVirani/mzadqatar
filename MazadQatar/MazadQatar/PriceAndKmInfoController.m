//
//  PriceAndKmInfoController.m
//  Mzad Qatar
//
//  Created by GuruUgam on 6/23/17.
//  Copyright © 2017 Mohammed abdulfattah attia. All rights reserved.
//

#import "PriceAndKmInfoController.h"
#import "SharedData.h"

@interface PriceAndKmInfoController ()

@end

@implementation PriceAndKmInfoController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if([[LanguageManager currentLanguageCode]isEqualToString:@"ar"]){
        
        if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            [self.imgView setImage:[UIImage imageNamed:@"explain_ar.png"]];
        }else{
            [self.imgView setImage:[UIImage imageNamed:@"explain_ar.png"]];
        }
        
    }else{
        
        if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            [self.imgView setImage:[UIImage imageNamed:@"explain_en_ipad.png"]];
        }else{
            [self.imgView setImage:[UIImage imageNamed:@"explain_en.png"]];
        }
    }
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
