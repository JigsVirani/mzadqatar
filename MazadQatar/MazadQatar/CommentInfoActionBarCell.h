//
//  CommentInfoActionBarCell.h
//  MazadQatar
//
//  Created by samo on 4/23/13.
//  Copyright (c) 2013 Mohammed abdulfattah attia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ServerManager.h"
#import "ProductObject.h"

// Delgate
@class CommentInfoActionBarCell;
@protocol CommentInfoActionBarDelegate <NSObject>
@required
- (void)
    moreClickedFinishedWithLoadingIndicator:(UIActivityIndicatorView *)indicator
                         andResultTextField:(UITextField *)resultTextFieldParam;
@required
@end

@interface CommentInfoActionBarCell : UITableViewCell {
  id<CommentInfoActionBarDelegate> delegate;
}

@property(strong, nonatomic) IBOutlet UIButton *moreCommentBtn;
@property(strong, nonatomic)
    IBOutlet UIActivityIndicatorView *moreCommentIndicator;
- (void)setMoreBtnDelegate:(id<CommentInfoActionBarDelegate>)delegateParam;
- (IBAction)moreBtnClikced:(id)sender;
@property(strong, nonatomic) IBOutlet UITextField *resultTextField;
@end
