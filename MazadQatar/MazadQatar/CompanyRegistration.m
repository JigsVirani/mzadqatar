//
//  CompanyRegistration.m
//  Mzad Qatar
//
//  Created by AHMED HEGAB on 11/3/16.
//  Copyright © 2016 Mohammed abdulfattah attia. All rights reserved.
//

#import "CompanyRegistration.h"
#import "SuccessPopView.h"

NSString *requestRegisterCompanyBtnClicked = @"registerCompanyBtnClicked";

@interface CompanyRegistration ()<ServerManagerResponseDelegate, SuccessPopViewDelegate>

@end

@implementation CompanyRegistration

UIActivityIndicatorView *spinner ;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = NSLocalizedString(@"REGISTER_AS_COMPANY_TITLE", nil);
    self.companyDetails.delegate = self;
    
    spinner = [[UIActivityIndicatorView alloc]       initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    spinner.center = CGPointMake(self.view.frame.size.width / 2.0, self.view.frame.size.height / 2.0);;

    self.companyDetails.text = NSLocalizedString(@"COMPANY_DETAILS_HINT", nil);
    self.companyDetails.textColor = [UIColor lightGrayColor];
    self.companyDetails.delegate = self;
    
    //Set Text Allignment Dynamically
    if ([[LanguageManager currentLanguageCode] isEqualToString:[ServerManager LANGUAGE_ARABIC]]) {
        self.companyName.textAlignment = NSTextAlignmentRight;
        self.companyEmail.textAlignment = NSTextAlignmentRight;
        self.companyPhoneNumber.textAlignment = NSTextAlignmentRight;
        self.companyDetails.textAlignment = NSTextAlignmentRight;
    } else {
        self.companyName.textAlignment = NSTextAlignmentLeft;
        self.companyEmail.textAlignment = NSTextAlignmentLeft;
        self.companyPhoneNumber.textAlignment = NSTextAlignmentLeft;
        self.companyDetails.textAlignment = NSTextAlignmentLeft;
    }
    
    // Do any additional setup after loading the view.
    [self createKeyboardAccessoryView];
}

- (void)createKeyboardAccessoryView {
    if ([DisplayUtility isPad] == false) {
        UIToolbar* keyboardDoneButtonView = [[UIToolbar alloc] init];
        [keyboardDoneButtonView sizeToFit];
        UIImage* image3 = [UIImage imageNamed:@"btn_advertise.png"];
        CGRect frameimg = CGRectMake(0, 0, image3.size.width, image3.size.height);
        UIButton *someButton = [[UIButton alloc] initWithFrame:frameimg];
        [someButton setBackgroundImage:image3 forState:UIControlStateNormal];
        [someButton addTarget:self action:@selector(doneClicked:)
             forControlEvents:UIControlEventTouchUpInside];
        [someButton setTitle:NSLocalizedString(@"DONE", nil) forState:UIControlStateNormal];
        [someButton setTintColor:[UIColor whiteColor]];
        [someButton setShowsTouchWhenHighlighted:YES];
        
        UIBarButtonItem *doneButton =[[UIBarButtonItem alloc] initWithCustomView:someButton];
        [keyboardDoneButtonView setItems:[NSArray arrayWithObjects:doneButton, nil]];
        
        _companyDetails.inputAccessoryView = keyboardDoneButtonView;
        _companyName.inputAccessoryView = keyboardDoneButtonView;
        _companyPhoneNumber.inputAccessoryView = keyboardDoneButtonView;
        _companyEmail.inputAccessoryView = keyboardDoneButtonView;
    }
}

-(void)doneClicked:(id)sender
{
    [self.view endEditing:YES];
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.tabBarController.tabBar.hidden = YES;
    [self.tabBarController.tabBar setTranslucent:YES];
}

- (void) viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)registerCompany:(UIButton *)sender {
    
      NSString *companyNameText = self.companyName.text;
      NSString *companyPhoneText = self.companyPhoneNumber.text;
      NSString *companyEmailText = self.companyEmail.text;
      NSString *companyDetailsText = self.companyDetails.text;
    
    if ([companyNameText length] == 0) {
        [DialogManager showErrorCompanyNameEmtpy];
        return;
    }
    if ([companyPhoneText length] == 0) {
        [DialogManager showErrorCompanyNumberEmtpy];
        return;
    }
    if ([companyEmailText length] == 0) {
        [DialogManager showErrorCompanyEmailEmtpy];
        return;
    }
    if ([companyDetailsText length] == 0) {
        [DialogManager showErrorCompanyDetailsEmtpy];
        return;
    }
    
    if (![self NSStringIsValidEmail:companyEmailText]) {
        [DialogManager showErrorCompanyEmailValidation];
        return;
    }
    
    [spinner startAnimating];
    
    [self.view addSubview:spinner];
    [self.view endEditing:YES];
    [ServerManager registeCompanyRequest:companyNameText AndCompanyPhoneNumber:companyPhoneText AndCompanyEmail:companyEmailText AndCompanyDetails:companyDetailsText AndDelegate:self withRequestId:requestRegisterCompanyBtnClicked];
}

-(void)textViewDidBeginEditing:(UITextView *)textView
{
    if([textView.text isEqualToString: [NSString stringWithFormat:@"%@",NSLocalizedString(@"COMPANY_DETAILS_HINT", nil)]]){
        textView.text = @"";
        textView.textColor = [UIColor blackColor];
    }
}

-(void) textViewDidEndEditing:(UITextView *)textView{

    if([textView.text length] == 0)
    {
        textView.text = [NSString stringWithFormat:@"%@",NSLocalizedString(@"COMPANY_DETAILS_HINT", nil)];
        textView.textColor = [UIColor lightGrayColor];
    }else{
         textView.textColor = [UIColor blackColor];
    }
}

- (void)requestFinished:(ServerManagerResult *)serverManagerResult
            ofRequestId:(NSString *)requestId
    withNumberOfRequest:(int)numberOfRequest {
    
        [spinner stopAnimating];

    //NSLog(@"%@",serverManagerResult.jsonArray);
       [self.scrollView willRemoveSubview:spinner];
    
    if ([serverManagerResult.opertationResult
         isEqualToString:[ServerManager ERROR_IN_CONNECTION_OPERATION]]) {
        [DialogManager showErrorInConnection];
        self.navigationItem.hidesBackButton = NO;
        
        return;
    }

    else if ([serverManagerResult.opertationResult
              isEqualToString:[ServerManager FAILED_OPERATION]]) {
        //[TestFlight passCheckpoint:@"Sms limit in registeration screen"];
   
        self.navigationItem.hidesBackButton = NO;
        
        //solve the problem If user navigate away during registration loading
        [DialogManager showErrorWithMessage:serverManagerResult.opertationMsg];
    } else if ([serverManagerResult.opertationResult
                isEqualToString:[ServerManager SUCCESS_OPERATION]]) {
        
        SuccessPopView *success = [[SuccessPopView alloc] initWithFrame:self.view.frame];
        success.delegate = self;
        [self.view addSubview:success];
//          [DialogManager showSuccessMessage:serverManagerResult.opertationMsg];
        
    }
}

-(BOOL) NSStringIsValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = NO;
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}

-(void)dneClicked {
    [self.navigationController popViewControllerAnimated:true];
}

@end
