//
//  DeepLinkVC.h
//  Mzad Qatar
//
//  Created by GuruUgam on 8/15/16.
//  Copyright © 2016 Mohammed abdulfattah attia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DeepLinkVC : UIViewController
{
    IBOutlet UIWebView *objWebview;
}
@property(nonatomic,strong)NSString *strUrl;
@end
