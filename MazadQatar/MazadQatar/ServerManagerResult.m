//
//  ServerResponseObject.m
//  MazadQatar
//
//  Created by samo on 3/17/13.
//  Copyright (c) 2013 Mohammed abdulfattah attia. All rights reserved.
//

#import "ServerManagerResult.h"

@implementation ServerManagerResult

- (instancetype)init
{
  self = [super init];
  if (self) {
    self.userProfileObject = [[ProductObject alloc] init];
  }
  return self;
}

@end
