//
//  ProductDetailActionBarCell.h
//  MazadQatar
//
//  Created by samo on 4/23/13.
//  Copyright (c) 2013 Mohammed abdulfattah attia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ServerManager.h"
#import "SharedViewManager.h"
#import "SocialShareUtility.h"
#import "ReportItemViewController.h"

@interface ProductDetailActionBarCell
: UITableViewCell <ServerManagerResponseDelegate,
ReportItemViewControllerDelegate> {
    ProductObject *productObject;
    UIViewController *parentView;
    UITableView *tableView;
    SocialShareUtility *socialShareUtility;
}

@property(strong, nonatomic) IBOutlet UIButton *likeBtn;
@property(strong, nonatomic) IBOutlet UIButton *addToFavoriteBtn;

- (IBAction)addToFavoriteBtnClicked:(id)sender;
- (IBAction)likeBtnClicked:(id)sender;
- (IBAction)reportBtnClicked:(id)sender;

@property(strong, nonatomic) IBOutlet UIActivityIndicatorView *likeIndicator;
@property(strong, nonatomic)
IBOutlet UIActivityIndicatorView *addToFavoriteIndicator;
@property(strong, nonatomic) IBOutlet UIActivityIndicatorView *reportIndicator;
@property(strong, nonatomic) IBOutlet UITextField *resultTextField;

- (void)setProductObject:(ProductObject *)productObjectParam
            InParentView:(UIViewController *)parentViewParam
             inTableView:(UITableView *)tableViewParam;

@end
