//
//  SubcategoriesController.m
//  Mzad Qatar
//
//  Created by GuruUgam on 7/13/16.
//  Copyright © 2016 Mohammed abdulfattah attia. All rights reserved.
//

#import "SubcategoriesController.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "SharedData.h"
#import "CategoryObject.h"
#import "AppDelegate.h"
#import "CategoryProducts.h"
#import "ProductDetailsViewController.h"

#define requestBannerCategory @"requestBannerCategory"
#define requestCategory @"requestGetCategory"

@interface SubcategoriesController ()
{
    NSMutableArray *arrSubcategories;
    NSMutableArray *TempArrSubcategories;
    NSMutableArray *arrBanners;
    IBOutlet UICollectionView *collectionSubCategory;
    IBOutlet UICollectionView *collectionBanner;
    __weak IBOutlet NSLayoutConstraint *banneeViewHeight;
    IBOutlet UITextField *searchField;
    __weak IBOutlet NSLayoutConstraint *subViewHeights;
    __weak IBOutlet NSLayoutConstraint *searchViewHeights;
}
@end

@implementation SubcategoriesController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"BACK", nil) style:UIBarButtonItemStylePlain target:nil action:nil];

    [[_AdvertisementButton layer] setBorderWidth:1.0f];
    [[_AdvertisementButton layer] setBorderColor:[UIColor whiteColor].CGColor];
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
        searchViewHeights.constant = 75.0f;
        self.searchBGView.layer.cornerRadius = 25.0;
        self.searchBGView.layer.masksToBounds = YES;
    }else{
        searchViewHeights.constant = 50.0f;
        self.searchBGView.layer.cornerRadius = 15.0;
        self.searchBGView.layer.masksToBounds = YES;
    }
    
    self.screenName = @"Subcategories Screen";
    banneeViewHeight.constant = 0.0f;
    subViewHeights.constant = 0.0f;
    [searchField setValue:[UIColor colorWithRed:133/255.0 green:27/255.0 blue:76/255.0 alpha:1]forKeyPath:@"_placeholderLabel.textColor"];
    [self setInitiateData];
    // Do any additional setup after loading the view.
    
    [self setFontSizeForIpad];
    //[self.navigationController.navigationBar.topItem setTitle:NSLocalizedString(@"BACK", nil)];
    
    if([LanguageManager currentLanguageIndex] == 1){
        searchField.textAlignment = NSTextAlignmentRight;
    }else{
        searchField.textAlignment = NSTextAlignmentLeft;
    }
}

- (void) setFontSizeForIpad
{
    if([DisplayUtility isPad]){
        self.AdvertisementButton.titleLabel.font = [UIFont systemFontOfSize:22.0];
        self.advertiseBtnWidthConstant.constant = 250.0;
        searchField.font = [UIFont systemFontOfSize:24.0];
    }
}
-(void)viewWillAppear:(BOOL)animated
{
    [self.navigationController.tabBarController.tabBar setHidden:YES];
    
    NSString *strNotificationCount = [SharedData getNotificationCountFromSharedData];
    
    if([strNotificationCount isEqualToString:@"0"]||[strNotificationCount isEqualToString:@"٠"] || strNotificationCount==nil)
    {
        [self.notificationCountLbl setHidden:YES];
    }else{
        [self.notificationCountLbl setHidden:NO];
        self.notificationCountLbl.text = strNotificationCount;
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [searchField resignFirstResponder];
}
#pragma mark - SetInitiate Data
-(void)setInitiateData
{
    
    
//    [arrCategories removeAllObjects];
    //NSString *language = [[[NSBundle mainBundle] preferredLocalizations] objectAtIndex:0];
    NSString *language = [LanguageManager currentLanguageCode];
    
    if ([[UserSetting getLastUpdateTimeForCategoriesFromServer] intValue] >
        [[UserSetting getLastUpdateTimeForCategoriesFromLocal] intValue] ||
        [language isEqualToString:[UserSetting getAllCategoriesFileLanguage]] ==
        false) {
        [self loadSubCategoryTableFromServer];
    }else
    {
        NSData *arrayData = [[NSUserDefaults standardUserDefaults]objectForKey:[SharedData CATEGORY_ARRAY_FILE_KEY]];
        NSMutableArray *arrCategories = [NSKeyedUnarchiver unarchiveObjectWithData:arrayData];
        [self getSubCategoriesArray:arrCategories];
    }
    
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(notificationbtn:)];
    tapGestureRecognizer.numberOfTapsRequired = 1;
    [self.notificationCountLbl addGestureRecognizer:tapGestureRecognizer];
    self.notificationCountLbl.userInteractionEnabled = YES;
    
//    if(arrCategories.count>0){
//        [self getSubCategoriesArray:arrCategories];
//    }else{
//        [self loadSubCategoryTableFromServer];
//    }
}

#pragma mark - LoadSubCategoryFromServer
//////Start sending request to get categories data
- (void)loadSubCategoryTableFromServer {
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [hud setLabelText:NSLocalizedString(@"LOADING_CATEGORIES", Nil)];
    [self startLoadingUserSubCategoryWithRequestId:requestCategory andIsNewSession:YES];
}
//////Start sending request to get categories data
- (void)startLoadingUserSubCategoryWithRequestId:(NSString *)requestId andIsNewSession:(bool)isNewSession {
    
    //NSString *language = [[[NSBundle mainBundle] preferredLocalizations] objectAtIndex:0];
    NSString *language = [LanguageManager currentLanguageCode];
    
    if ([language isEqualToString:[ServerManager LANGUAGE_ARABIC]]) {
        language = [ServerManager LANGUAGE_ARABIC];
    } else {
        language = [ServerManager LANGUAGE_ENGLISH];
    }
    [ServerManager getAllCategories:language withDelegate:self withRequestId:requestCategory];
}
#pragma mark - SetInitiate Data
-(void)getSubCategoriesArray:(NSMutableArray *)jsonArray
{
    CategoryObject *categoryMain = [[CategoryObject alloc]init];
    for (CategoryObject *category in jsonArray) {
        if([category.categoryID isEqualToString:self.strCategoryID]){
            categoryMain = category;
            break;
        }
    }
    arrSubcategories = [[NSMutableArray alloc]init];
    for (CategoryObject *obj in categoryMain.categorySubcategories) {
        
        obj.productAdvertiseTypes=categoryMain.productAdvertiseTypes;
        [arrSubcategories addObject:obj];
    }
    TempArrSubcategories = [arrSubcategories mutableCopy];
    [collectionSubCategory reloadData];
    NSLog(@"%lu",(unsigned long)arrSubcategories.count);
    [self performSelector:@selector(setContentSizeSubview) withObject:self afterDelay:0.5];
    
    //[self loadBannerFromServer];
}
#pragma mark - LoadBannerFromServer
- (void)loadBannerFromServer {
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [hud setLabelText:NSLocalizedString(@"LOADING_BANNERS", Nil)];
    [self startLoadingUserCategoryWithRequestId:requestBannerCategory andIsNewSession:YES];
}
//////Start sending request to get categories data
- (void)startLoadingUserCategoryWithRequestId:(NSString *)requestId andIsNewSession:(bool)isNewSession {
    
    //NSString *language = [[[NSBundle mainBundle] preferredLocalizations] objectAtIndex:0];
    NSString *language = [LanguageManager currentLanguageCode];
    
    if ([language isEqualToString:[ServerManager LANGUAGE_ARABIC]]) {
        language = [ServerManager LANGUAGE_ARABIC];
    } else {
        language = [ServerManager LANGUAGE_ENGLISH];
    }
    [ServerManager getAllBannersForCategories:language withDelegate:self withRequestId:requestBannerCategory];
}
- (void)requestFinished:(ServerManagerResult *)serverManagerResult ofRequestId:(NSString *)requestId withNumberOfRequest:(int)numberOfRequest
{
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    
    if ([requestId isEqualToString:requestBannerCategory]) {
        
        if ([serverManagerResult.opertationResult isEqualToString:[ServerManager ERROR_IN_CONNECTION_OPERATION]]) {
            [DialogManager showErrorInConnection];
        } else if ([serverManagerResult.opertationResult isEqualToString:[ServerManager SUCCESS_OPERATION]]) {
            
            for (NSDictionary *dicCatObject in serverManagerResult.jsonArray) {
                
                if([[dicCatObject objectForKey:@"categoryId"]isEqualToString:self.strCategoryID]){
                    arrBanners = [[dicCatObject objectForKey:@"categoryBanners"]mutableCopy];
                    [collectionBanner reloadData];
                    break;
                }
            }
            if((arrBanners.count>0)){
                if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){banneeViewHeight.constant = 175.0f;}
                else{banneeViewHeight.constant = 80.0f;}
            }
        }else if ([serverManagerResult.opertationResult isEqualToString:[ServerManager FAILED_OPERATION]])
        {
            //Comment [DialogManager showErrorInServer];
            [DialogManager showErrorWithMessage:serverManagerResult.opertationMsg];
        }
    }else if([requestId isEqualToString:requestCategory]){
        
        if ([serverManagerResult.opertationResult isEqualToString:[ServerManager ERROR_IN_CONNECTION_OPERATION]]) {
            
            [DialogManager showErrorInConnection];
        } else if ([serverManagerResult.opertationResult isEqualToString:[ServerManager SUCCESS_OPERATION]]) {
            
            NSData *data = [NSKeyedArchiver archivedDataWithRootObject:serverManagerResult.jsonArray];
            [[NSUserDefaults standardUserDefaults]setObject:data forKey:[SharedData CATEGORY_ARRAY_FILE_KEY]];
            
            [UserSetting saveLastUpdateTimeForCategoriesFromLocal:[UserSetting getLastUpdateTimeForCategoriesFromServer]];
            
            // save language so whenever we change locale we load file again with
            // other language
            //NSString *language = [[[NSBundle mainBundle] preferredLocalizations] objectAtIndex:0];
            NSString *language = [LanguageManager currentLanguageCode];
            
            if ([language isEqualToString:[ServerManager LANGUAGE_ARABIC]]) {
                [UserSetting saveAllCategoriesFileLanguage:[ServerManager LANGUAGE_ARABIC]];
            } else {
                [UserSetting saveAllCategoriesFileLanguage:[ServerManager LANGUAGE_ENGLISH]];
            }
            
            NSData *arrayData = [[NSUserDefaults standardUserDefaults]objectForKey:[SharedData CATEGORY_ARRAY_FILE_KEY]];
            NSMutableArray *arrCategories = [NSKeyedUnarchiver unarchiveObjectWithData:arrayData];
            [self getSubCategoriesArray:arrCategories];
            
        }else if ([serverManagerResult.opertationResult isEqualToString:[ServerManager FAILED_OPERATION]]){
            //Comment [DialogManager showErrorInServer];
            [DialogManager showErrorWithMessage:serverManagerResult.opertationMsg];
        }
    }
}


#pragma mark - NavigationBar Action
- (IBAction)addAdvertiseClicked:(id)sender {
    if ([UserSetting isUserRegistered] == false) {
        //[SharedViewManager showLoginScreen:self];
        RegisterScreen *registerScreen = (RegisterScreen*)[[SharedData getCurrentStoryBoard]instantiateViewControllerWithIdentifier:@"RegisterScreen"];
        [registerScreen setSuccessScreenSegueName:nil];
        [self.navigationController pushViewController:registerScreen animated:YES];
    } else {
        [SharedViewManager showAddAdvertiseScreen:self];
    }
}

- (IBAction)btnSearchAction:(id)sender
{
    [self performSegueWithIdentifier:@"segueSearchController" sender:nil];
}

- (IBAction)notificationbtn:(id)sender {
    
    NotificationViewController *controller = (NotificationViewController *)[[SharedData getCurrentStoryBoard] instantiateViewControllerWithIdentifier:@"NotificationView"];
    [self.navigationController pushViewController:controller animated:YES];
}

#pragma mark - UICollectionView DataSource Delegate
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    if([collectionView isEqual:collectionSubCategory]){
        //return [TempArrSubcategories count] + 1;
        return [TempArrSubcategories count];
    }else{
        return 1;
    }
    return 0;
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionView *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return 5; // This is the minimum inter item spacing, can be more
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGSize cellSize;
 
    if(collectionView == collectionSubCategory){
//        CategoryObject *category = [TempArrSubcategories objectAtIndex:indexPath.item];
        if (indexPath.row == 0) {
            
            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
                cellSize.width = ([UIScreen mainScreen].bounds.size.width-10);
                cellSize.height = 150;
            }else{
                cellSize.width = ([UIScreen mainScreen].bounds.size.width-10);
                cellSize.height = 90;
            }
            CategoryObject *category = [TempArrSubcategories objectAtIndex:0];
            if([category.productWebsiteUrl isEqualToString:@""] && [category.productID isEqualToString:@""]){
                cellSize.height = 0.0;
            }
        }
        else {
            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
                cellSize.width = ([UIScreen mainScreen].bounds.size.width-50)/4;
                cellSize.height = 150;
            }else{
                cellSize.width = floor(([UIScreen mainScreen].bounds.size.width-20)/3);
                cellSize.height = 85;
            }
        }
    }else {
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            cellSize.width = ([UIScreen mainScreen].bounds.size.width-20);
            banneeViewHeight.constant = 105;
            cellSize.height = 105;
        }else{
            cellSize.width = ([UIScreen mainScreen].bounds.size.width-20);
            cellSize.height = 70;
        }
    }
    return cellSize;
/*if(arrBanners.count==1){
 if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
 cellSize.width = [UIScreen mainScreen].bounds.size.width-20;
 cellSize.height = collectionView.bounds.size.height-5;
 }else{
 cellSize.width = 300;
 cellSize.height = 75;
 }
 return cellSize;
 }else if(arrBanners.count==2){
 if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
 cellSize.width = ([UIScreen mainScreen].bounds.size.width-15)/2;
 cellSize.height = 100;
 }else{
 cellSize.width = ([UIScreen mainScreen].bounds.size.width-15)/2;
 cellSize.height = 75;
 }
 return cellSize;
 }else if (arrBanners.count==3){
 if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
 cellSize.width = ([UIScreen mainScreen].bounds.size.width-20)/3;
 cellSize.height = 100;
 }else{
 cellSize.width = ([UIScreen mainScreen].bounds.size.width-20)/3;;
 cellSize.height = 75;
 }
 return cellSize;
 }
 return cellSize;*/
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)cv cellForItemAtIndexPath:(NSIndexPath *)indexPath;
{
    ProductGridCell *cell;
    if([cv isEqual:collectionSubCategory]){
        
        cell = [cv dequeueReusableCellWithReuseIdentifier:@"GridCell" forIndexPath:indexPath];
        
        cell.productImageView.clipsToBounds = YES;
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            cell.lblHeightConstant.constant=40;
        }else{
            cell.lblHeightConstant.constant=25;
        }
        if (indexPath.row == 0) {
            
            CategoryObject *category = [TempArrSubcategories objectAtIndex:indexPath.item];
            
            [cell.loadingIndicator startAnimating];
            [cell.productTitle setHidden:YES];
            cell.lblHeightConstant.constant=0;
            if ([category.categoryFullWidthImageURL isKindOfClass:[NSString class]])
            {
                [cell.productImageView sd_setImageWithURL:[NSURL URLWithString:category.categoryFullWidthImageURL] placeholderImage:[UIImage imageNamed:@"blankImage"] options:SDWebImageRetryFailed completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                    [cell.loadingIndicator stopAnimating];
                }];
            }else{
                [cell.loadingIndicator startAnimating];
            }
        }else{
            
            CategoryObject *category = [TempArrSubcategories objectAtIndex:indexPath.item - 1];
            
            [cell.loadingIndicator startAnimating];
            NSLog(@"category name:%@",category.categoryName);
            if ([category.categoryImageURL isKindOfClass:[NSString class]])
            {
                [cell.productImageView sd_setImageWithURL:[NSURL URLWithString:category.categoryImageURL] placeholderImage:[UIImage imageNamed:@"blankImage"] options:SDWebImageRetryFailed completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                    [cell.loadingIndicator stopAnimating];
                }];
            }else{
                [cell.loadingIndicator startAnimating];
            }
            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
                cell.lblHeightConstant.constant=40;
            }else{
                cell.lblHeightConstant.constant=25;
            }
            
            cell.productTitle.hidden=NO;
            [cell.productTitle setText:category.categoryName];
        }
        return cell;
    }else if([cv isEqual:collectionBanner]){
        
        if(arrBanners.count==1){
            
            cell = [cv dequeueReusableCellWithReuseIdentifier:@"BannerCell" forIndexPath:indexPath];
            
            cell.productImageView.layer.cornerRadius = 10.0;
            cell.productImageView.clipsToBounds = YES;
            [cell.loadingIndicator startAnimating];
            
            [cell.productImageView sd_setImageWithURL:[NSURL URLWithString:[[arrBanners objectAtIndex:0]objectForKey:@"bannerUrl"]] placeholderImage:[UIImage imageNamed:@"blankImage"] options:SDWebImageRetryFailed completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                [cell.loadingIndicator stopAnimating];
            }];
            
        }else if (arrBanners.count==2){
            
            cell = [cv dequeueReusableCellWithReuseIdentifier:@"BannerCellTwo" forIndexPath:indexPath];
            
            [cell.productImageView sd_setImageWithURL:[NSURL URLWithString:[[arrBanners objectAtIndex:0]objectForKey:@"bannerUrl"]] placeholderImage:[UIImage imageNamed:@"blankImage"] options:SDWebImageRetryFailed completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                [cell.loadingIndicator stopAnimating];
            }];
            [cell.productImageViewTwo sd_setImageWithURL:[NSURL URLWithString:[[arrBanners objectAtIndex:1]objectForKey:@"bannerUrl"]] placeholderImage:[UIImage imageNamed:@"blankImage"] options:SDWebImageRetryFailed completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                [cell.loadingIndicatorTwo stopAnimating];
            }];
        }else if (arrBanners.count==3){
            
            cell = [cv dequeueReusableCellWithReuseIdentifier:@"BannerCellThree" forIndexPath:indexPath];
            
            cell.productImageView.layer.cornerRadius = 10.0;
            cell.productImageView.clipsToBounds = YES;
            [cell.loadingIndicator startAnimating];
            
            [cell.productImageView sd_setImageWithURL:[NSURL URLWithString:[[arrBanners objectAtIndex:0]objectForKey:@"bannerUrl"]] placeholderImage:[UIImage imageNamed:@"blankImage"] options:SDWebImageRetryFailed completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                [cell.loadingIndicator stopAnimating];
            }];
            [cell.productImageViewTwo sd_setImageWithURL:[NSURL URLWithString:[[arrBanners objectAtIndex:1]objectForKey:@"bannerUrl"]] placeholderImage:[UIImage imageNamed:@"blankImage"] options:SDWebImageRetryFailed completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                [cell.loadingIndicatorTwo stopAnimating];
            }];
            [cell.productImageViewThree sd_setImageWithURL:[NSURL URLWithString:[[arrBanners objectAtIndex:2]objectForKey:@"bannerUrl"]] placeholderImage:[UIImage imageNamed:@"blankImage"] options:SDWebImageRetryFailed completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                [cell.loadingIndicatorThree stopAnimating];
            }];
        }
        
        /*cell.productImageView.layer.cornerRadius = 10.0;
         cell.productImageView.clipsToBounds = YES;
         [cell.loadingIndicator startAnimating];
         if ([[[arrBanners objectAtIndex:indexPath.item]objectForKey:@"bannerUrl"] isKindOfClass:[NSString class]])
         {
         [cell.productImageView sd_setImageWithURL:[NSURL URLWithString:[[arrBanners objectAtIndex:indexPath.item]objectForKey:@"bannerUrl"]] placeholderImage:[UIImage imageNamed:@"blankImage"] options:SDWebImageRetryFailed completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
         [cell.loadingIndicator stopAnimating];
         }];
         }else{
         [cell.loadingIndicator startAnimating];
         }*/
        return cell;
    }
    return nil;
}
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    if([collectionView isEqual:collectionSubCategory]){
        if(indexPath.item == 0){
            
            CategoryObject *category = [TempArrSubcategories objectAtIndex:0];
            if(category.productWebsiteUrl != nil && ![category.productWebsiteUrl isEqualToString:@""]){
                
                if([[UIApplication sharedApplication]canOpenURL:[NSURL URLWithString:category.productWebsiteUrl]]){
                    [[UIApplication sharedApplication]openURL:[NSURL URLWithString:category.productWebsiteUrl]];
                }
            }else{
                if(category.productID != nil){
                    ProductDetailsViewController *productDetail = [self.storyboard
                                                                   instantiateViewControllerWithIdentifier:@"ProductDetailsViewController"];
                    [productDetail setProductId:category.productID];
                    [self.navigationController pushViewController:productDetail animated:YES];
                }
            }
        }
    }
}
#pragma mark - UITextFiled Delegate

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    
    if (textField == searchField) {
        [self performSegueWithIdentifier:@"segueSearchController" sender:nil];
        
        return false;
    }
    return true;
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *strSearch = [textField.text stringByAppendingString:string];
    strSearch = [strSearch stringByReplacingCharactersInRange:range withString:@""];
    
    if(strSearch.length){
        NSPredicate *arabicCheck=[NSPredicate predicateWithFormat: @"SELF MATCHES %@", @"^[\\p{Arabic}\\s\\p{N}]+$"];
        
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(SELF.categoryName BEGINSWITH[cd] %@ OR SELF.categoryID==%@)",strSearch, @"0"];
        if ([arabicCheck evaluateWithObject:strSearch]) {
            predicate = [NSPredicate predicateWithFormat:@"(SELF.categoryName ENDSWITH[cd] %@ OR SELF.categoryID==%@)",strSearch, @"0"];
        }
        TempArrSubcategories = [[arrSubcategories filteredArrayUsingPredicate:predicate] mutableCopy];
    }else{
        TempArrSubcategories = [arrSubcategories mutableCopy];
    }
    [collectionSubCategory reloadData];
    [self performSelector:@selector(setContentSizeSubview) withObject:self afterDelay:0.5];
    return YES;
}
-(void)setContentSizeSubview
{
    subViewHeights.constant = collectionSubCategory.contentSize.height+55;
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    return [searchField resignFirstResponder];
}
#pragma mark - Navigation
-(BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender{
    
    if([identifier isEqualToString:@"SubImageViewClicked"]){
        
        UICollectionViewCell *cell = (UICollectionViewCell *)sender;
        NSIndexPath *indexPath = [collectionSubCategory indexPathForCell:cell];
        if(indexPath.row == 0){
            return false;
        }
    }
    return true;
}
// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    [super prepareForSegue:segue sender:sender];
    
    UICollectionViewCell *cell = (UICollectionViewCell *)sender;
    CategoryProducts *catProduct = [segue destinationViewController];
    
    if ([[segue identifier] isEqualToString:@"SubImageViewClicked"]) {
        // get the index clicked
        NSIndexPath *indexPath = [collectionSubCategory indexPathForCell:cell];
        
        CategoryObject *category;
        
        if (indexPath.row == 0 || indexPath.row == 1) {
            category = [TempArrSubcategories objectAtIndex:0];
        }else{
            category = [TempArrSubcategories objectAtIndex:indexPath.item-1];
        }
       // category = [TempArrSubcategories objectAtIndex:indexPath.item];
        [catProduct setCategoryProductsCategoryId:category.subCategoryParent andSubcategoryId:category.categoryID andSubcategoryName:category.categoryName];
        catProduct.tabBarNames = category.productAdvertiseTypes;
        catProduct.strCategory = self.strCategoryName;
        
    }else if ([[segue identifier] isEqualToString:@"addAdvertise"]) {
        
        ProductDetailsViewController *productDetails = [segue destinationViewController];
        [productDetails setIsAddProductDetail:YES];

    }else if ([[segue identifier] isEqualToString:@"segueSearchController"]) {
        SearchViewController *searchScreen = [segue destinationViewController];
        [searchScreen setStrCategories:[NSString stringWithFormat:NSLocalizedString(@"SearchSubCategory", nil),self.strCategoryName]];
        [searchScreen setStrId:self.strCategoryID];
    }else{
        
        CategoryObject *category = [TempArrSubcategories objectAtIndex:0];
        [catProduct setCategoryProductsCategoryId:self.strCategoryID andSubcategoryId:category.categoryID andSubcategoryName:category.categoryName];
        
        catProduct.tabBarNames = category.productAdvertiseTypes;
        for (int i = 0; i<catProduct.tabBarNames.count; i++) {
            AdvertiseType *obj = [catProduct.tabBarNames objectAtIndex:i];
            NSLog(@"Ad id %@",obj.advertiseTypeId);
            NSLog(@"Ad name %@",obj.advertiseTypeName);
            NSLog(@"Ad view %@",obj.advertiseTypeDefaultView);
        }
        if([[segue identifier] isEqualToString:@"BannerImageViewClicked"]){
            for (AdvertiseType *adName in catProduct.tabBarNames) {
                if ([adName.advertiseTypeId isEqualToString:[[arrBanners objectAtIndex:0]objectForKey:@"tabId"]]) {
                    NSString *index = [NSString stringWithFormat:@"%lu",(unsigned long)[catProduct.tabBarNames indexOfObject: adName]];
                    [catProduct selectedTabFromSubMenu:self.strCategoryID withTabID:index];
                }
            }
            //[catProduct selectedTabFromSubMenu:self.strCategoryID withTabID:[[arrBanners objectAtIndex:0]objectForKey:@"tabId"]];
        }else if([[segue identifier] isEqualToString:@"TwoBannerImageViewFirstImage"]){
            for (AdvertiseType *adName in catProduct.tabBarNames) {
                if ([adName.advertiseTypeId isEqualToString:[[arrBanners objectAtIndex:0]objectForKey:@"tabId"]]) {
                    NSString *index = [NSString stringWithFormat:@"%lu",(unsigned long)[catProduct.tabBarNames indexOfObject: adName]];
                    [catProduct selectedTabFromSubMenu:self.strCategoryID withTabID:index];
                }
            }
           // [catProduct selectedTabFromSubMenu:self.strCategoryID withTabID:[[arrBanners objectAtIndex:0]objectForKey:@"tabId"]];
        }else if([[segue identifier] isEqualToString:@"TwoBannerImageViewSecondImage"]){
            for (AdvertiseType *adName in catProduct.tabBarNames) {
                if ([adName.advertiseTypeId isEqualToString:[[arrBanners objectAtIndex:1]objectForKey:@"tabId"]]) {
                    NSString *index = [NSString stringWithFormat:@"%lu",(unsigned long)[catProduct.tabBarNames indexOfObject: adName]];
                    [catProduct selectedTabFromSubMenu:self.strCategoryID withTabID:index];
                }
            }
            //[catProduct selectedTabFromSubMenu:self.strCategoryID withTabID:[[arrBanners objectAtIndex:1]objectForKey:@"tabId"]];
        }else if([[segue identifier] isEqualToString:@"ThreeBannerImageViewFirstImage"]){
            for (AdvertiseType *adName in catProduct.tabBarNames) {
                if ([adName.advertiseTypeId isEqualToString:[[arrBanners objectAtIndex:0]objectForKey:@"tabId"]]) {
                    NSString *index = [NSString stringWithFormat:@"%lu",(unsigned long)[catProduct.tabBarNames indexOfObject: adName]];
                    [catProduct selectedTabFromSubMenu:self.strCategoryID withTabID:index];
                }
            }
            //[catProduct selectedTabFromSubMenu:self.strCategoryID withTabID:[[arrBanners objectAtIndex:0]objectForKey:@"tabId"]];
        }else if([[segue identifier] isEqualToString:@"ThreeBannerImageViewSecondImage"]){
            for (AdvertiseType *adName in catProduct.tabBarNames) {
                if ([adName.advertiseTypeId isEqualToString:[[arrBanners objectAtIndex:1]objectForKey:@"tabId"]]) {
                    NSString *index = [NSString stringWithFormat:@"%lu",(unsigned long)[catProduct.tabBarNames indexOfObject: adName]];
                    [catProduct selectedTabFromSubMenu:self.strCategoryID withTabID:index];
                }
            }
            //[catProduct selectedTabFromSubMenu:self.strCategoryID withTabID:[[arrBanners objectAtIndex:1]objectForKey:@"tabId"]];
        }else if([[segue identifier] isEqualToString:@"ThreeBannerImageViewThirdImage"]){
            for (AdvertiseType *adName in catProduct.tabBarNames) {
                if ([adName.advertiseTypeId isEqualToString:[[arrBanners objectAtIndex:2]objectForKey:@"tabId"]]) {
                    NSString *index = [NSString stringWithFormat:@"%lu",(unsigned long)[catProduct.tabBarNames indexOfObject: adName]];
                    [catProduct selectedTabFromSubMenu:self.strCategoryID withTabID:index];
                }
            }
           // [catProduct selectedTabFromSubMenu:self.strCategoryID withTabID:[[arrBanners objectAtIndex:2]objectForKey:@"tabId"]];
        }
    }
}

@end
