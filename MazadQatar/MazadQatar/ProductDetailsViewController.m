//
//  ProductDetailsViewController.m
//  MazadQatar
//
//  Created by samo on 4/17/13.
//  Copyright (c) 2013 Mohammed abdulfattah attia. All rights reserved.
//

#import "ProductDetailsViewController.h"
#import "LanguageChooserViewController.h"
#import "UserSetting.h"
#import "GAI.h"
#import "GAIDictionaryBuilder.h"
#import "UserProducts.h"
#import "UIView+Toast.h"
#import "CompanyInformationVC.h"
#import "DeepLinkVC.h"
#import "ALChatViewController.h"
#import "ALChatManager.h"
#import "ALUserDefaultsHandler.h"
#import "ALRegisterUserClientService.h"
#import "ALDBHandler.h"
#import "ALContact.h"
#import "ALDataNetworkConnection.h"
#import "ALMessageService.h"
#import "ALContactService.h"
#import "ALUserService.h"
#import "ALImagePickerHandler.h"
#import "DisplayUtility.h"

@interface ProductDetailsViewController ()<GADInterstitialDelegate>
@property(nonatomic, strong) DFPInterstitial *interstitial;
@property(nonatomic, strong) NSString *textOnImage;
@property(nonatomic, strong) UIImage *tempHoldingImage;
@property(nonatomic, strong) NSString *language;

@end

@implementation ProductDetailsViewController
{
    RelatedAdsTableViewCell* relatedAdvCell ;
    BOOL checkResponse;
}
@synthesize productObject, tempHoldingImage;

// static NSString * CellIdentifier = @"GridCell";
static NSString *ImageViewerCellIdentifier = @"ImageViewerCell";
static NSString *ImagePreviewerCellIdentifier = @"ImagePreviewerCell";
static NSString *RelatedImageViewCellIdentifier = @"RelatedImageViewCell";
static NSString *ProductInfoCellIdentifier = @"ProductInfoCell";
static NSString *ProductInfoCellOneLanguageIdentifier =
@"ProductInfoCellOneLanguage";
static NSString *addingAsCompanyIdentifier = @"AddAsCompany";

static NSString *ProductInfoCellViewerIdentifier = @"ProductInfoCellViewer";
static NSString *CommentInfoCellIdentifier = @"NewCell";
static NSString *FirstCommentInfoCellIdentifier = @"NewCellFirst";
static NSString *NewMoewCommentCellIdentifier = @"NewMoewCommentCell";
static NSString *NoCommentInfoCellIdentifier = @"NoCommentInfoCell";
static NSString *LastCommentInfoCellIdentifier = @"NewCellLast";
static NSString *WriteCommentCellIdentifier = @"WriteCommentCell";
static NSString *AdCellIdentifier = @"AdCell";
static NSString *ProductDetailActionBarCellIdentifier =
@"ProductDetailActionBarCell";
static NSString *CommentInfoActionBarCellIdentifier =
@"CommentInfoActionBarCell";
static NSString * descriptionCell =
@"descriptionCell";
static NSString *UserProfileCellIdentifier = @"UserProfileCell";

NSString *requestGetProductComments = @"requestGetProductComments";
NSString *requestDeleteProductComment = @"requestDeleteProductComment";
NSString *requestMoreCommentsBtn = @"requestMoreCommentsBtn";
NSString *requestGetProductDetails = @"requestGetProductDetails";
NSString *requestCallAdvertiser = @"requestCallAdvertiser";
NSString *getRelateAds = @"getRelateAds";
NSString *requestAddViewToAdvertise = @"addViewToAdvertise";

const int imageViewerPos = 1;
const int productInfoPos = 2;
const int productDetailActionBar = 3;
const int Addcell = 4;

int viewProductDetailsCellsCount = 8;
int addProductDetailsCellsCount = 3;
int numberOfCellsAfterCommentInfo = 3;

int commentDescriptionheightDefault = 0;
int commentCellheightDefault = 0;

int productDescriptionheightDefault = 0;
int productCellheightDefault = 0;

bool isCurrentViewPortrait;
static bool isProductDetailViewFinished = false;

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"BACK", nil) style:UIBarButtonItemStylePlain target:nil action:nil];
    
    self.tableView.estimatedRowHeight = 1000;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    
    self.screenName = @"Product Details Screen";

    _chatBtn.layer.cornerRadius = 3;
    [_chatBtn setClipsToBounds:true];
    
    _callBtn.layer.cornerRadius = 3;
    [_callBtn setClipsToBounds:true];
    
    _smsBtn.layer.cornerRadius = 3;
    [_smsBtn setClipsToBounds:true];
    
    isProductDetailViewFinished = false;
    isAddAdcertiseBtnClicked = false;
    
    [UserSetting saveUserAddAdvertiseLanguage:3];
    
    ////To disable swipe left right functionality
    //self.revealViewController.panGestureRecognizer.enabled = NO;
    
    // set background
    //UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"bg.png"]];
    //self.tableView.backgroundView = imageView;
    [self.tableView setBackgroundColor:[UIColor blackColor]];
    
    if (self.isAddProductDetail == false) {
        isCurrentViewPortrait = ![DisplayUtility isLandscape];
        
        self.chatBtn.hidden = false;
        self.callBtn.hidden = false;
        self.smsBtn.hidden = false;
        
        productCommentsPageNumber = 1;
        productCommentsNumberOfPages = 10;
        
        if (_isFromCategoryProducts == true) {
            checkResponse = YES;
            isProductDetailLoading = NO;
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.tableView reloadData];
                [self refershProductRelatedAds];
                /*** Increase View Count*/
                [self addViewToAdvertise];
            });
            //[self performSelector:@selector(refershProductRelatedAds) withObject:nil afterDelay:0.1];
            /*** Increase View Count*/
            //[self performSelector:@selector(addViewToAdvertise) withObject:nil afterDelay:0.1];
        }else{
            [self loadData];
        }
        [self performSelector:@selector(refershProductComments:) withObject:requestGetProductComments afterDelay:0.1];
    } else if (self.isEditAdvertise) {
        if ([UserSetting isShowPressEditAdvertise] == true) {
            [DialogManager showPressEditInfoDialog];
            
            [UserSetting setIsShowPressEditAdvertiseAlert:false];
        }
        
        [self loadData];
    } else {
        productObject = [[ProductObject alloc] init];
    }
    //self.language = [[[NSBundle mainBundle] preferredLocalizations] objectAtIndex:0];
    self.language = [LanguageManager currentLanguageCode];
    
    if ([self isAddProductDetail] == false) {
        UIView *headerView;
        DFPBannerView *bannerView;
        UIActivityIndicatorView *loadingIndicator = [[UIActivityIndicatorView alloc]init];
        
        if([DisplayUtility isPad]){
            
            [loadingIndicator setFrame:CGRectMake(384, 40, 20, 20)];
            
            headerView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.tableView.frame.size.width, 100)];
            bannerView = [[DFPBannerView alloc]initWithFrame:CGRectMake(20, 5, 728, 90)];
            
            if([self.language isEqualToString:[ServerManager LANGUAGE_ARABIC]]) {
                bannerView.adUnitID = @"/271990408/SectioniOSTabletAppTopBannerArabic";
            }else{
                bannerView.adUnitID = @"/271990408/SectioniOSTabletAppTopBannerEnglish";
            }
        }else{
            
            [loadingIndicator setFrame:CGRectMake(self.view.center.x, 25, 20, 20)];
            
            headerView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.tableView.frame.size.width, 60)];
            bannerView = [[DFPBannerView alloc]initWithFrame:CGRectMake(self.tableView.center.x-160, 5, 320, 50)];
            
            if([self.language isEqualToString:[ServerManager LANGUAGE_ARABIC]]) {
                bannerView.adUnitID = @"/271990408/SectioniOSMobileAppTopBannerArabic";
            }else{
                bannerView.adUnitID = @"/271990408/SectioniOSMobileAppTopBannerEnglish";
            }
        }
        bannerView.adSize = kGADAdSizeSmartBannerPortrait;
        bannerView.rootViewController = self;
        [bannerView loadRequest:[DFPRequest request]];
        [headerView addSubview:loadingIndicator];
        [loadingIndicator startAnimating];
        [headerView addSubview:bannerView];
        self.tableView.tableHeaderView = headerView;
    }
    /*NSDate *date = [NSDate date];
     if(![[NSUserDefaults standardUserDefaults]objectForKey:@"CurrentDateProductScreen"]){
     [self loadInterstitialAd];
     [appDelegate Add24HoursToDate:date withKey:@"24HoursDateProductScreen" withCurrentDateKey:@"CurrentDateProductScreen"];
     }else{
     if([[appDelegate.dateFormatter dateFromString:[appDelegate.dateFormatter stringFromDate:date]]compare:[[NSUserDefaults standardUserDefaults]objectForKey:@"24HoursDateProductScreen"]]==NSOrderedDescending){
     [self loadInterstitialAd];
     [appDelegate Add24HoursToDate:[[NSUserDefaults standardUserDefaults]objectForKey:@"24HoursDateProductScreen"] withKey:@"24HoursDateProductScreen" withCurrentDateKey:@"CurrentDateProductScreen"];
     }
     }*/
    //[self.navigationController.navigationBar.topItem setTitle:NSLocalizedString(@"BACK", nil)];
    [self loadInterstitialAd];
    
    
    if ([DisplayUtility isPad]) {
        CGRect frame = self.chatBtn.frame;
        frame.size.width = 100.0;
        self.chatBtn.frame = frame;
        self.smsBtn.frame = frame;
        self.callBtn.frame = frame;
        self.chatBtn.titleLabel.font = [UIFont systemFontOfSize:20.0];
        self.smsBtn.titleLabel.font = [UIFont systemFontOfSize:20.0];
        self.callBtn.titleLabel.font = [UIFont systemFontOfSize:20.0];
    }
}
-(void)loadInterstitialAd{
    if([DisplayUtility isPad]){
        if([self.language isEqualToString:[ServerManager LANGUAGE_ARABIC]]) {
            //self.interstitial = [[DFPInterstitial alloc] initWithAdUnitID:@"/271990408/TestIOSTabletAppPortraitFullPage"];
            self.interstitial = [[DFPInterstitial alloc] initWithAdUnitID:@"/271990408/SectioniOSTabletAppFullPageArabic"];
        }else{
            self.interstitial = [[DFPInterstitial alloc] initWithAdUnitID:@"/271990408/SectioniOSTabletAppFullPageEnglish"];
        }
    }else{
        if([self.language isEqualToString:[ServerManager LANGUAGE_ARABIC]]) {
            //self.interstitial = [[DFPInterstitial alloc] initWithAdUnitID:@"/271990408/TestIOSMobileAppPortraitFullPage"];
            self.interstitial = [[DFPInterstitial alloc] initWithAdUnitID:@"/271990408/SectioniOSMobileAppFullPageArabic"];
        }else{
            self.interstitial = [[DFPInterstitial alloc] initWithAdUnitID:@"/271990408/SectioniOSMobileAppFullPageEnglish"];
        }
    }
    self.interstitial.delegate = self;
    [self.interstitial loadRequest:[DFPRequest request]];
}
#pragma mark - interstitialDelegates
// Called when an interstitial ad request succeeded.
- (void)interstitialDidReceiveAd:(DFPInterstitial *)ad {
    NSLog(@"interstitialDidReceiveAd");
    [self.interstitial presentFromRootViewController:self];
}
// Called when an interstitial ad request failed.
- (void)interstitial:(DFPInterstitial *)ad didFailToReceiveAdWithError:(GADRequestError *)error {
    NSLog(@"interstitial:didFailToReceiveAdWithError: %@", [error localizedDescription]);
}
// Called just before presenting an interstitial.
- (void)interstitialWillPresentScreen:(DFPInterstitial *)ad {
    NSLog(@"interstitialWillPresentScreen");
}
// Called before the interstitial is to be animated off the screen.
- (void)interstitialWillDismissScreen:(DFPInterstitial *)ad {
    NSLog(@"interstitialWillDismissScreen");
}
// Called just after dismissing an interstitial and it has animated off the screen.
- (void)interstitialDidDismissScreen:(DFPInterstitial *)ad {
    NSLog(@"interstitialDidDismissScreen");
}
// Called just before the app will background or terminate because the user clicked on an
// ad that will launch another app (such as the App Store).
- (void)interstitialWillLeaveApplication:(DFPInterstitial *)ad {
    NSLog(@"interstitialWillLeaveApplication");
}
/////////////
//////////////////
//- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer
// shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer
//*)otherGestureRecognizer
//{
//    if ([otherGestureRecognizer isKindOfClass:[UITapGestureRecognizer class]])
//    {
//        [otherGestureRecognizer
//        requireGestureRecognizerToFail:gestureRecognizer];
//        NSLog(@'added failure requirement to: %@', otherGestureRecognizer);
//    }
//}

- (void)setProductId:(NSString *)productId{
    productObject = [[ProductObject alloc] init];
    productObject.productTilte = NSLocalizedString(@"LOADING", nil);
    productObject.productPrice = NSLocalizedString(@"LOADING", nil);
    productObject.productDescription = NSLocalizedString(@"LOADING", nil);
    productObject.productId = productId;
}

//=====================================================================update
// product object
//should be changed this happen because object of comment adn details same , so in case of comment all values of details null so it override the other values
- (void)updateProductObject:(ProductObject *)productObjectParam andIsAppendComments:(bool)isAppendComments
         andIsDeleteComment:(bool)isDeleteComment {
    if (productObjectParam.productId != nil) {
        productObject.productId = productObjectParam.productId;
    }
    
    if (productObjectParam.productAdvClickType != nil) {
        productObject.productAdvClickType = productObjectParam.productAdvClickType;
    }
    
    if (productObjectParam.productTilte != nil) {
        productObject.productTilte = productObjectParam.productTilte;
    }
    if (productObjectParam.productTilteEnglish != nil) {
        productObject.productTilteEnglish = productObjectParam.productTilteEnglish;
    }
    if (productObjectParam.productTilteArabic != nil) {
        productObject.productTilteArabic = productObjectParam.productTilteArabic;
    }
    if (productObjectParam.productMainImageUrl != nil) {
        productObject.productMainImageUrl = productObjectParam.productMainImageUrl;
    }
    if (productObjectParam.productShareImageUrl != nil) {
        productObject.productShareImageUrl = productObjectParam.productShareImageUrl;
    }
    if (productObjectParam.productImagesUrls != nil && productObjectParam.productImagesUrls.count > 0) {
        productObject.productImagesUrls = productObjectParam.productImagesUrls;
    }
    if (productObjectParam.productImagesOnDevices != nil) {
        productObject.productImagesOnDevices =
        productObjectParam.productImagesOnDevices;
    }
    if (productObjectParam.productComments != nil || isDeleteComment == true) {
        if (isAppendComments == true) {
            [productObject.productComments
             addObjectsFromArray:productObjectParam.productComments];
        } else {
            productObject.productComments =  productObjectParam.productComments;
        }
    }
    if (productObjectParam.productPrice != nil) {
        productObject.productPrice = productObjectParam.productPrice;
    }
    if (productObjectParam.productNumberOfViews != nil) {
        productObject.productNumberOfViews =
        productObjectParam.productNumberOfViews;
    }
    if (productObjectParam.productDate != nil) {
        productObject.productDate = productObjectParam.productDate;
    }
    if (productObjectParam.productDescription != nil) {
        productObject.productDescription = productObjectParam.productDescription;
    }
    if (productObjectParam.productDescriptionWithTitles != nil) {
        productObject.productDescriptionWithTitles = productObjectParam.productDescriptionWithTitles;
    }
    if (productObjectParam.productDescriptionEnglish != nil) {
        productObject.productDescriptionEnglish =
        productObjectParam.productDescriptionEnglish;
    }
    if (productObjectParam.productDescriptionArabic != nil) {
        productObject.productDescriptionArabic =
        productObjectParam.productDescriptionArabic;
    }
    if (productObjectParam.productWebsiteUrl != nil) {
        productObject.productWebsiteUrl = productObjectParam.productWebsiteUrl;
    }
    
    if (productObjectParam.productUserUrl != nil) {
        productObject.productUserUrl = productObjectParam.productUserUrl;
    }
    
    if (productObjectParam.productCategoryId != nil) {
        productObject.productCategoryId = productObjectParam.productCategoryId;
    }
    if (productObjectParam.productCategoryName != nil) {
        productObject.productCategoryName = productObjectParam.productCategoryName;
    }
    if (productObjectParam.productSubCategoryId != nil) {
        productObject.productSubCategoryId =
        productObjectParam.productSubCategoryId;
    }
    if (productObjectParam.productSubCategoryName != nil) {
        productObject.productSubCategoryName =
        productObjectParam.productSubCategoryName;
    }
    if (productObjectParam.productSubsubCategoryId != nil) {
        productObject.productSubsubCategoryId =
        productObjectParam.productSubsubCategoryId;
    }
    if (productObjectParam.productSubsubCategoryName != nil) {
        productObject.productSubsubCategoryName =
        productObjectParam.productSubsubCategoryName;
    }
    if (productObjectParam.productCountryCode != nil) {
        productObject.productCountryCode = productObjectParam.productCountryCode;
    }
    if (productObjectParam.productUserNumber) {
        productObject.productUserNumber = productObjectParam.productUserNumber;
    }
    
    if (productObjectParam.productUserisCompany != nil) {
        productObject.productUserisCompany = productObjectParam.productUserisCompany;
    }
    if (productObjectParam.productUserlang != nil) {
        productObject.productUserlang = productObjectParam.productUserlang;
    }
    if (productObjectParam.productUserlong != nil) {
        productObject.productUserlong = productObjectParam.productUserlong;
    }
    if (productObjectParam.productUserAddress != nil) {
        productObject.productUserAddress = productObjectParam.productUserAddress;
    }
    if (productObjectParam.productDescriptionSizePortrait.height != 0) {
        productObject.productDescriptionSizePortrait =
        productObjectParam.productDescriptionSizePortrait;
    }
    if (productObjectParam.productDescriptionSizeLandscape.height != 0) {
        productObject.productDescriptionSizeLandscape =
        productObjectParam.productDescriptionSizeLandscape;
    }
    if (productObjectParam.productDescriptionWithTitlesSizePortrait.height != 0) {
        productObject.productDescriptionWithTitlesSizePortrait =
        productObjectParam.productDescriptionWithTitlesSizePortrait;
    }
    if (productObjectParam.productDescriptionWithTitlesSizeLandscape.height != 0) {
        productObject.productDescriptionWithTitlesSizeLandscape =
        productObjectParam.productDescriptionWithTitlesSizeLandscape;
    }
    if (productObjectParam.productLikeCount) {
        productObject.productLikeCount = productObjectParam.productLikeCount;
    }
    if (productObjectParam.productCommentCount) {
        productObject.productCommentCount = productObjectParam.productCommentCount;
    }
    if (productObjectParam.productLanguage != nil) {
        productObject.productLanguage = productObjectParam.productLanguage;
    }
    if (productObjectParam.productUserName != nil) {
        productObject.productUserName = productObjectParam.productUserName;
    }
    if (productObjectParam.productUserEmail != nil) {
        productObject.productUserEmail = productObjectParam.productUserEmail;
    }
    if (productObjectParam.productUserShortDescription != nil) {
        productObject.productUserShortDescription =
        productObjectParam.productUserShortDescription;
    }
    if (productObjectParam.productUserImageUrl != nil) {
        productObject.productUserImageUrl = productObjectParam.productUserImageUrl;
    }
    if (productObjectParam.productUserImageUrl != nil) {
        productObject.productUserImage = productObjectParam.productUserImage;
    }
    if (productObjectParam.productUserNumberOfAds != nil) {
        productObject.productUserNumberOfAds =
        productObjectParam.productUserNumberOfAds;
    }
    if (productObjectParam.productUserNumberOfAds != nil) {
        productObject.isHideAddCommentsAndLikes =
        productObjectParam.isHideAddCommentsAndLikes;
    }
    if (productObjectParam.productManfactureYear != nil) {
        productObject.productManfactureYear =
        productObjectParam.productManfactureYear;
    }
    if (productObjectParam.productKm != nil) {
        productObject.productKm =
        productObjectParam.productKm;
    }
    if (productObjectParam.productAdvertiseTypeId != nil) {
        productObject.productAdvertiseTypeId =
        productObjectParam.productAdvertiseTypeId;
    }
    if (productObjectParam.productAdvertiseTypeName != nil) {
        productObject.productAdvertiseTypeName =
        productObjectParam.productAdvertiseTypeName;
    }
    if (productObjectParam.productCityId != nil) {
        productObject.productCityId =
        productObjectParam.productCityId;
    }
    if (productObjectParam.productCityName != nil) {
        productObject.productCityName =
        productObjectParam.productCityName;
    }
    if (productObjectParam.productRegionId != nil) {
        productObject.productRegionId =
        productObjectParam.productRegionId;
    }
    if (productObjectParam.productRegionName != nil) {
        productObject.productRegionName =
        productObjectParam.productRegionName;
    }
    if (productObjectParam.productFurnishedTypeId != nil) {
        productObject.productFurnishedTypeId =
        productObjectParam.productFurnishedTypeId;
    }
    if (productObjectParam.productFurnishedTypeName != nil) {
        productObject.productFurnishedTypeName =
        productObjectParam.productFurnishedTypeName;
    }
    if (productObjectParam.productNumberOfRooms != nil) {
        productObject.productNumberOfRooms =
        productObjectParam.productNumberOfRooms;
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.tableView reloadData];
    });
}

//=====================================================================comments
- (void)refershProductComments:(NSString *)requestId {
    isCommentLoading = YES;
    // bool isNewSession;
    
    int productCommentsPageNumberToBeSent = productCommentsPageNumber;
    int productCommentsNumberPerPageToBeSent = 10;
    
    if (requestId == requestGetProductComments) {
        productCommentsPageNumber = 1;
        productCommentsPageNumberToBeSent = productCommentsPageNumber;
        productCommentlastUpdateTime = @"0";
        
        //   isNewSession=YES;
    } else if (requestId == requestDeleteProductComment) {
        productCommentsNumberPerPageToBeSent =
        ((productCommentsPageNumber - 1) * productCommentsNumberOfPages);
        productCommentsPageNumberToBeSent = 1;
    }
    
    [ServerManager getCommentOfProduct:productObject
         withProductCommentsPageNumber:productCommentsPageNumberToBeSent
      withProductCommentsNumberOfPages:productCommentsNumberPerPageToBeSent
      withProductcommentLastUpdateTime:productCommentlastUpdateTime
                           AndDelegate:self
                         withRequestId:requestId];
}
- (void)refershProductRelatedAds {
    if (productObject != nil) {
        [ServerManager getRelateAds:productObject AndDelegate:self withRequestId:getRelateAds];
    }
}

-(void)addViewToAdvertise{
    
    [ServerManager addViewToAdvertise:productObject.productId withDelegate:self withRequestId:requestAddViewToAdvertise];
}

- (void)requestFinished:(ServerManagerResult *)serverManagerResult
            ofRequestId:(NSString *)requestId
    withNumberOfRequest:(int)numberOfRequest {
    checkResponse = YES;
    if (isProductDetailViewFinished == false)
    {
        if ([serverManagerResult.opertationResult
             isEqualToString:[ServerManager ERROR_IN_CONNECTION_OPERATION]]) {
            [DialogManager showErrorInConnection];
            
            return;
        }
        if ([requestId isEqualToString:requestGetProductComments]) {
            productCommentlastUpdateTime = serverManagerResult.lastUpdateTime;
            isCommentLoading = NO;
            
            ProductObject *productObjectFromServer = [serverManagerResult.jsonArray objectAtIndex:0];
            [self updateProductObject:productObjectFromServer andIsAppendComments:NO andIsDeleteComment:NO];
            productCommentsPageNumber++;
        }
        if ([requestId isEqualToString:getRelateAds]) {
            relatedAdsArry = serverManagerResult.jsonArray;
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.tableView reloadData];
            });
        }
        else if ([requestId isEqualToString:requestMoreCommentsBtn]) {
            [moreBtnIndicator stopAnimating];
            moreBtn.hidden = NO;
            isCommentLoading = NO;
            
            ProductObject *productObjectFromServer =
            [serverManagerResult.jsonArray objectAtIndex:0];
            
            [self updateProductObject:productObjectFromServer
                  andIsAppendComments:YES
                   andIsDeleteComment:NO];
            
            if (productObjectFromServer.productComments.count == 0) {
                moreBtnResultTextField.hidden = NO;
                [moreBtnResultTextField
                 setText:NSLocalizedString(@"NO_MORE_COMMENTS", nil)];
            } else {
                productCommentsPageNumber++;
            }
        }
        else if ([requestId isEqualToString:requestDeleteProductComment]) {
            isCommentLoading = NO;
            
            ProductObject *productObjectFromServer =
            [serverManagerResult.jsonArray objectAtIndex:0];
            
            [self updateProductObject:productObjectFromServer
                  andIsAppendComments:NO
                   andIsDeleteComment:YES];
        }
        
        else if ([requestId isEqualToString:requestGetProductDetails]) {
            if ([serverManagerResult.opertationResult
                 isEqualToString:[ServerManager ALREADED_ADDED_OPERATION]]) {
                [DialogManager showErrorProductNotFound];
                [self.navigationController popToRootViewControllerAnimated:YES];
                return;
            }
            
            isProductDetailLoading = NO;
            
            NSMutableArray *productItems = serverManagerResult.jsonArray;
            
            [self updateProductObject:[productItems objectAtIndex:0]
                  andIsAppendComments:NO
                   andIsDeleteComment:NO];
            [self refershProductRelatedAds];

        }
    }
}


//=========================================================product  detail view
// loading
- (void)loadData {
    isProductDetailLoading = YES;
    
    NSString *isAd;
    if (productObject.isAd == true) {
        isAd = @"0";
    } else {
        isAd = @"1";
    }
    
    NSString* isEditAdvertise;
    if(self.isEditAdvertise)
    {
       isEditAdvertise=@"0";
    }
    else
    {
        isEditAdvertise=@"1";
    }
    // set the product details items from server
    
    [ServerManager
     getProductDetails:productObject.productId
     withDelegate:self
     withRequestId:requestGetProductDetails
     byLanguage:
     [UserSetting
      getAdvertiseLanguageServerParam:
      [UserSetting getUserFilterViewAdvertiseLanguage]]
     isAdvertise:isAd
     andUserCountryCode:[UserSetting getUserCountryCode]
     andUserNumber:[UserSetting getUserNumber] andIsEditAdvertise:isEditAdvertise];

}

//=============================================== table handling

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self getTableCellsCount];
}

//- (nullable UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
//{
//    UIView *footer = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 40.0)];
//    UIButton *btnChat = [UIButton buttonWithType:UIButtonTypeCustom];
//    btnChat.frame = CGRectMake(0, 0, footer.frame.size.width, 40.0);
//    btnChat.backgroundColor = [UIColor colorWithRed:160.0/255.0 green:43.0/255.0 blue:95.0/255.0 alpha:1.0];
//    [btnChat setTitle:NSLocalizedString(@"Chat", nil) forState:UIControlStateNormal];
//    [btnChat.titleLabel setFont:[UIFont systemFontOfSize:15.0]];
//    [btnChat addTarget:self action:@selector(btnChatAction:) forControlEvents:UIControlEventTouchUpInside];
//    [footer addSubview:btnChat];
//    footer.backgroundColor = [UIColor clearColor];
//    return footer;
//}
//
//- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
//{
//    return 40.0;
//}

#pragma mark get list of row count
- (int)getTableCellsCount {
    totalCountOfCells = 0;
    // Return the number of rows in the section.
    if (self.isAddProductDetail)
    {
        return addProductDetailsCellsCount;
    } else if (checkResponse == YES){
        totalCountOfCells = viewProductDetailsCellsCount;
        
        if (productObject.productComments.count > 0) {
            totalCountOfCells += productObject.productComments.count;
            numberOfCellsAfterCommentInfo = 3;
        } else {
            // remove more cell
            totalCountOfCells = viewProductDetailsCellsCount+1;
            numberOfCellsAfterCommentInfo = 3;
            totalCountOfCells--;
        }
        // all product detail cells + comment count - more cell will be removed if
        // not comments
        return totalCountOfCells;
    }else
    {
       return totalCountOfCells;
    }
}
#pragma mark CompanyInfoDetail Button Event
-(IBAction)btnCompanyInfoDetailTapped:(UIButton*)sender{
    CompanyInformationVC *CompanyDetailVC = [[SharedData getCurrentStoryBoard] instantiateViewControllerWithIdentifier:@"CompanyInformationVC"];
    [self.navigationController pushViewController:CompanyDetailVC animated:YES];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
   
    /** Add advertise cell on top index 0 **/
    if (indexPath.row == 0)  {
        AddAsCompanyTableViewCell * cellAdd;
        cellAdd = [tableView dequeueReusableCellWithIdentifier:addingAsCompanyIdentifier forIndexPath:indexPath];
        if (self.isAddProductDetail == true)  {
        }else{
            cellAdd.hidden = true;
        }
        
        if([LanguageManager currentLanguageIndex]==1){
            [cellAdd.btnAddDetail setBackgroundImage:[UIImage imageNamed:@"ic_Top_Banner_AR"] forState:UIControlStateNormal];
        }else{
            [cellAdd.btnAddDetail setBackgroundImage:[UIImage imageNamed:@"ic_Top_Banner_EN"] forState:UIControlStateNormal];
        }
        return cellAdd;
    }
    /** Add image  on index 1 **/
    if (self.isAddProductDetail == true) {
        if (indexPath.row == imageViewerPos) {
            return [self loadImageViewCell:tableView withIndex:indexPath];
        } else {
            return [self loadProductInfoCell:tableView withIndex:indexPath];
        }
    }
    /** Load image  on index 1 **/
    else {
        if (indexPath.row == imageViewerPos) {
            return [self loadImagePreviewCell:tableView withIndex:indexPath];
        } else if (indexPath.row == productInfoPos) {
            return [self loadProductInfoCell:tableView withIndex:indexPath];
        }
        else if (indexPath.row == productDetailActionBar) {
            return [self loadProductDetailActionBarCell:tableView withIndex:indexPath];
        }
        else if (indexPath.row == Addcell)
        {
            CustomTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:AdCellIdentifier];
            [cell.activityIndicator startAnimating];
            if(productObject.isLoadedAddInList==false){
                if([DisplayUtility isPad]){
                    if([self.language isEqualToString:[ServerManager LANGUAGE_ARABIC]]) {
                        cell.bannerView.adUnitID = @"/271990408/SectioniOSTabletAppMPUArabic";
                    }else{
                        cell.bannerView.adUnitID = @"/271990408/SectioniOSTabletAppMPUEnglish";
                    }
                }else{
                    if([self.language isEqualToString:[ServerManager LANGUAGE_ARABIC]]) {
                        cell.bannerView.adUnitID = @"/271990408/SectioniOSMobileAppMPUArabic";
                    }else{
                        cell.bannerView.adUnitID = @"/271990408/SectioniOSMobileAppMPUEnglish";
                    }
                }
                
                cell.bannerView.rootViewController = self;
                [cell.bannerView loadRequest:[DFPRequest request]];
            }
            return cell;
        }
        else if (indexPath.row < totalCountOfCells - numberOfCellsAfterCommentInfo) {
            // comments of product
            // MINUS THE comment cell info cell and write comment cell
            int exactIndexInCommnet =
            viewProductDetailsCellsCount - numberOfCellsAfterCommentInfo;
            //LastCommentInfoWithNoAccessCell
            ProductCommentObject *commnetObject = [productObject.productComments
                                                   objectAtIndex:indexPath.row - exactIndexInCommnet];

            NewCommentInfoTableCell *cell;
            if (productObject.productComments.count > 0)
            {
                if(indexPath.row - exactIndexInCommnet == 0)
                {
                    cell = [tableView dequeueReusableCellWithIdentifier:FirstCommentInfoCellIdentifier forIndexPath:indexPath];
                    
                    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0ul);
                    dispatch_async(queue, ^{
                        // Load from file or Bundle as you want
                        dispatch_sync(dispatch_get_main_queue(), ^{
                            //Set the image to image view not, wither cell.imageview or [cell add subviw:imageview later ]
                            [cell roundCornersOnView:[cell viewWithTag:100] onTopLeft:YES topRight:YES bottomLeft:NO bottomRight:NO radius:5];
                            [cell roundCornersOnView:[cell viewWithTag:101] onTopLeft:YES topRight:YES bottomLeft:NO bottomRight:NO radius:5];
                        });
                    });
                }
                else if(indexPath.row - exactIndexInCommnet == productObject.productComments.count-1)
                {
                    cell = [tableView dequeueReusableCellWithIdentifier:LastCommentInfoCellIdentifier forIndexPath:indexPath];
                }else{
                    cell = [tableView dequeueReusableCellWithIdentifier:CommentInfoCellIdentifier forIndexPath:indexPath];
                }
            }
            if (isCommentLoading) {
                if (productObject.productComments.count == 0) {
                    [cell setIsLoadingCommentView:YES];
                    [cell setIsNoCommentLblHidden:YES];
                }
            } else {
                [cell setIsLoadingCommentView:NO];
                if (productObject.productComments.count > 0) {
                    [cell setComment:commnetObject productObj:productObject
                  andAdvertiseNumber:productObject.productUserNumber
                              inView:self
                         andDelegate:self
               andCommentInfoDelegae:self
                         inProductId:self.productObject.productId
                         inTableView:self.tableView isAdOwnerOrCommentOwner:[[UserSetting getUserNumber]
                      isEqualToString:commnetObject.commenterNumber]?YES:NO];
                } else {
                    [cell setIsNoCommentLblHidden:NO];
                }
            }
            
            // ios 7 background issue
            [self setBackgroundClear:cell];
            return cell;
        }
        else if (indexPath.row == totalCountOfCells - 3) {
            NewCommentInfoTableCell * cell;
            if (productObject.productComments.count > 0)
            {
                cell = [tableView dequeueReusableCellWithIdentifier:NewMoewCommentCellIdentifier forIndexPath:indexPath];
                [cell setDelegate:self];
                [cell roundCornersOnView:cell.moreAdBtn onTopLeft:NO topRight:NO bottomLeft:YES bottomRight:YES radius:5];                                
            }
            else
            {
                cell = [tableView dequeueReusableCellWithIdentifier:NoCommentInfoCellIdentifier forIndexPath:indexPath];
                [cell roundCornersOnView:[cell viewWithTag:100] onTopLeft:YES topRight:YES bottomLeft:YES bottomRight:YES radius:5];
                [cell roundCornersOnView:[cell viewWithTag:101] onTopLeft:YES topRight:YES bottomLeft:YES bottomRight:YES radius:5];
                [cell.contentView setNeedsUpdateConstraints];
                [cell.contentView layoutIfNeeded];
                
            }
            return cell;
        }
        else if (indexPath.row == totalCountOfCells-2) {
            NewSendCommentTableCell *cell = [tableView
                                             dequeueReusableCellWithIdentifier:WriteCommentCellIdentifier
                                             forIndexPath:indexPath];
            
            [cell setProductOfComment:productObject
                         withDelegate:self
                     inViewController:self];
            return cell;
        }
        else if (indexPath.row == totalCountOfCells-1)
        {
            return [self loadRelatedViewCell:tableView withIndex:indexPath];
        }
    }
    return nil;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    
    if (self.isAddProductDetail == false) {
        NSMutableArray *uploadingProductList =
        [SharedGraphicInfo getcurrentUploadingProductList];
        if (uploadingProductList.count > 0) {
            [self.navigationController popViewControllerAnimated:YES];
            return;
        }
    }
    if(!self.isAddProductDetail)
    {
        [self registerForKeyboardNotifications];
    }
    //**Check [self.tableView reloadData];
    
    [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithRed:154.0/255 green:40.0/255 blue:92.0/255 alpha:1]];

    [self.view endEditing:YES];
}
-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    if(!self.isAddProductDetail)
    {
        [self unregisterForKeyboardNotifications];
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (self.isAddProductDetail == true) {
        if (indexPath.row == 0) {
            if([DisplayUtility isPad]){
                return 200;
            }else{
                if([[UIScreen mainScreen]bounds].size.width == 320){
                    return [self returnAddingAsCompanyCellHeight:tableView withIndexPath:indexPath];
                }else if([[UIScreen mainScreen]bounds].size.width == 375){
                    return 100;
                }else{
                    return 110;
                }
            }
        }
        if (indexPath.row == imageViewerPos) {
            return
            [self returnImageViewerCellHeight:tableView withIndexPath:indexPath];
        } else {
            if (self.isAddProductDetail == true) {
                return [self returnProductInfoCellHeight:tableView
                                           withIndexPath:indexPath];
            } else {
                return UITableViewAutomaticDimension;
            }
        }
    } else {
        if (indexPath.row == 0) {
            return 0;
        }
        if (indexPath.row == imageViewerPos) {
            return
            [self returnImagePreviewerCellHeight:tableView withIndexPath:indexPath];
        } else if (indexPath.row == productInfoPos) {
            
            if (self.isAddProductDetail == true) {
                return [self returnProductInfoCellHeight:tableView withIndexPath:indexPath];
            } else {
                return UITableViewAutomaticDimension;
            }
        }
        else if (indexPath.row == productDetailActionBar) {
            return UITableViewAutomaticDimension;
        }
        else if (indexPath.row == Addcell){
            return [self returnAdCellHeight:tableView withIndexPath:indexPath];
        }
        else if (indexPath.row < totalCountOfCells - numberOfCellsAfterCommentInfo) {
            
            int exactIndexInCommnet = viewProductDetailsCellsCount - numberOfCellsAfterCommentInfo;
            if (productObject.productComments.count > 0) {
                
                if(indexPath.row - exactIndexInCommnet == 0)
                {
                    //return [self returnFirstCommentInfoCellHeight:tableView withIndexPath:indexPath];
                    return UITableViewAutomaticDimension;
                }
                else if(indexPath.row - exactIndexInCommnet == productObject.productComments.count-1)
                {
                    //return [self returnLastCommentInfoCellHeight:tableView withIndexPath:indexPath];
                    return UITableViewAutomaticDimension;
                }
                else
                {
                    return UITableViewAutomaticDimension;
                }
            }
            else
            {
                return
                [self returnNoCommentInfoCellHeight:tableView withIndexPath:indexPath];
            }
        }
        else if (indexPath.row == totalCountOfCells - 3) {
            if (productObject.productComments.count == 0)
            return [self returnNoCommentInfoCellHeight:tableView withIndexPath:indexPath];
            else
            return 40;
        }
        else if (indexPath.row == totalCountOfCells - 2) {
            return
            [self returnWriteCommentCellHeight:tableView withIndexPath:indexPath];
        }
        else if (indexPath.row == totalCountOfCells - 1) {
            return
            [self returnRelatedViewerCellHeight:tableView withIndexPath:indexPath];
        }
    }
    
    return 0;
}
//===============================================height of cells
- (CGFloat)returnAddingAsCompanyCellHeight:(UITableView *)tableView
                         withIndexPath:(NSIndexPath *)indexPath {
    addingAsCompanyTableCell = [self.tableView
                          dequeueReusableCellWithIdentifier:addingAsCompanyIdentifier];
    
    return addingAsCompanyTableCell.frame.size.height;
}

//===============================================height of cells
- (CGFloat)returnImageViewerCellHeight:(UITableView *)tableView
                         withIndexPath:(NSIndexPath *)indexPath {
    imageViewTableCell = [self.tableView
                          dequeueReusableCellWithIdentifier:ImageViewerCellIdentifier];
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
        //return 768;
        return imageViewTableCell.frame.size.height;
    }else{
        return imageViewTableCell.frame.size.height;
    }
}
- (CGFloat)returnImagePreviewerCellHeight:(UITableView *)tableView
                         withIndexPath:(NSIndexPath *)indexPath {
    imagePreviewTableCell = [self.tableView
                             dequeueReusableCellWithIdentifier:ImagePreviewerCellIdentifier];
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
        return [UIScreen mainScreen].bounds.size.width;
        //return 550;//imagePreviewTableCell.frame.size.height;
    }else{
        return [UIScreen mainScreen].bounds.size.width;
        //return imagePreviewTableCell.frame.size.height*([DisplayUtility changeLaoutAsPerHeight]);
    }
}
- (CGFloat)returnRelatedViewerCellHeight:(UITableView *)tableView
                         withIndexPath:(NSIndexPath *)indexPath {
    relateAdTableCell = [self.tableView
                          dequeueReusableCellWithIdentifier:RelatedImageViewCellIdentifier];
    
    return relateAdTableCell.frame.size.height;
}

//- (CGFloat)returnUserProfileCellHeight:(UITableView *)tableView
//                         withIndexPath:(NSIndexPath *)indexPath {
//    userProfileCell = [self.tableView
//                       dequeueReusableCellWithIdentifier:UserProfileCellIdentifier];
//    
//    return userProfileCell.background.frame.size.height;
//}

- (CGFloat)returnProductInfoCellHeight:(UITableView *)tableView
                         withIndexPath:(NSIndexPath *)indexPath {
    int languageIndexToCompare;
    if (self.isEditAdvertise) {
        languageIndexToCompare =
        [UserSetting getAdvertiseTypeServerIndex:productObject.productLanguage];
    } else {
        languageIndexToCompare = UserSetting.getUserAddAdvertiseLanguage;
    }
    
    if (languageIndexToCompare == UserSetting.EnglishAndArabicLanguageKey) {
        productInfoCell = [self.tableView
                           dequeueReusableCellWithIdentifier:ProductInfoCellIdentifier];
    } else {
        productInfoCell = [self.tableView
                           dequeueReusableCellWithIdentifier:ProductInfoCellOneLanguageIdentifier];
    }
    
    return productInfoCell.frame.size.height;
}

- (CGFloat)returnFirstCommentInfoCellHeight:(UITableView *)tableView
                         withIndexPath:(NSIndexPath *)indexPath {
    
        int extraHeight = 0;
        
        ProductCommentObject *productComment = [productObject.productComments
                                                objectAtIndex:indexPath.row - (viewProductDetailsCellsCount -
                                                                               numberOfCellsAfterCommentInfo)];
        if (commentDescriptionheightDefault == 0) {
            commentDescriptionheightDefault = 27;
        }
        commentCellheightDefault = 130;

        
        if (isCurrentViewPortrait) {
            productComment.commentdescriptionSizeCurrent =
            productComment.commentDescriptionSizePortrait;
        } else {
            productComment.commentdescriptionSizeCurrent =
            productComment.commentDescriptionSizeLandscape;
        }

        CGFloat cellHeight =
        (commentCellheightDefault - commentDescriptionheightDefault) +
        productComment.commentdescriptionSizeCurrent.height;
        
        if (productComment.commentdescriptionSizeCurrent.height >
            (commentDescriptionheightDefault + 10)) {
            extraHeight = 0;
        } else {
            extraHeight = 10;
        }
        // extra cell height
        return cellHeight + extraHeight;

}

- (CGFloat)returnLastCommentInfoCellHeight:(UITableView *)tableView
                         withIndexPath:(NSIndexPath *)indexPath {

        int extraHeight = 0;
        
        ProductCommentObject *productComment = [productObject.productComments
                                                objectAtIndex:indexPath.row - (viewProductDetailsCellsCount -
                                                                               numberOfCellsAfterCommentInfo)];
        if (commentDescriptionheightDefault == 0) {
            commentDescriptionheightDefault = 27;
        }
        commentCellheightDefault = 100;
        
        if (isCurrentViewPortrait) {
            productComment.commentdescriptionSizeCurrent =
            productComment.commentDescriptionSizePortrait;
        } else {
            productComment.commentdescriptionSizeCurrent =
            productComment.commentDescriptionSizeLandscape;
        }
        CGFloat cellHeight =
        (commentCellheightDefault - commentDescriptionheightDefault) +
        productComment.commentdescriptionSizeCurrent.height;
        
        if (productComment.commentdescriptionSizeCurrent.height >
            (commentDescriptionheightDefault + 10)) {
            extraHeight = 0;
        } else {
            extraHeight = 10;
        }
        // extra cell height
        return cellHeight + extraHeight;

}
- (CGFloat)returnNoCommentInfoCellHeight:(UITableView *)tableView
                         withIndexPath:(NSIndexPath *)indexPath {
    commentInfoCell = [tableView
                       dequeueReusableCellWithIdentifier:NoCommentInfoCellIdentifier];
        return commentInfoCell.frame.size.height;
}

- (CGFloat)returnWriteCommentCellHeight:(UITableView *)tableView
                          withIndexPath:(NSIndexPath *)indexPath {
    writeCommentCellTable = [self.tableView
                             dequeueReusableCellWithIdentifier:WriteCommentCellIdentifier];
    
    return writeCommentCellTable.frame.size.height;
}
- (CGFloat)returnAdCellHeight:(UITableView *)tableView withIndexPath:(NSIndexPath *)indexPath {
    
    objCustomTableViewCell = [self.tableView dequeueReusableCellWithIdentifier:AdCellIdentifier];
    return objCustomTableViewCell.frame.size.height;
}



//===============================================call btn aaction
- (IBAction)callBtnClicked:(id)sender {
    [DialogManager
     showConfirmationDialogueWithDescription:
     [NSString
      stringWithFormat:@"%@ %@%@ %@",
      NSLocalizedString(@"CONFIRMATION_CALL_MSG", nil),
      productObject.productCountryCode,productObject.productUserNumber, NSLocalizedString(@"?",nil)]
     andConfirmBtnText:NSLocalizedString(
                                         @"CONFIRMATION_CALL_BTN_TEXT",
                                         nil)
     andDelegate:self
     andRequestId:requestCallAdvertiser];
}

- (IBAction)copyBtnClicked:(id)sender {
    
    //NSString * stringUserNumberCopy = [@"00974" stringByAppendingString:productObject.productUserNumber];
    NSString * stringUserNumberCopy = [productObject.productCountryCode stringByAppendingString:productObject.productUserNumber];
    [UIPasteboard generalPasteboard].string = stringUserNumberCopy;
    
    [DialogManager toastWithImageAndText: NSLocalizedString(@"Number_Copied",nil)];
    
}

#pragma mark : Email Button Click

- (IBAction)emailBtnClicked:(id)sender {
    
    if (productObject.productUserEmail.length == 0|| productObject.productUserEmail == nil)
    {
        [DialogManager showErrorNoEmailAccountAvailable];
    }else{
        NSArray *toRecipents = [NSArray arrayWithObject:productObject.productUserEmail];
        //NSArray *toRecipents = [NSArray arrayWithObject:self.userEmail];
    if ([MFMailComposeViewController canSendMail]) {
        
        MFMailComposeViewController *mc =
        [[MFMailComposeViewController alloc] init];
        [mc setToRecipients:toRecipents];
        mc.mailComposeDelegate = self;
        
        // Present mail view controller on screen
        [self presentViewController:mc animated:YES completion:NULL];
    } else {
        [DialogManager showErrorNoEmailAccountAvailable];
    }
    }
}

#pragma mark -- Mail Composer Delegate

- (void)mailComposeController:(MFMailComposeViewController *)controller
          didFinishWithResult:(MFMailComposeResult)result
                        error:(NSError *)error {
    // Close the Mail Interface
    [self dismissViewControllerAnimated:YES completion:NULL];
}


- (void)confirmBtnClicked:(NSString *)requestId {
    if (requestId == requestCallAdvertiser) {
        //[SystemUtility callNumber:productObject.productUserNumber];
        [SystemUtility callNumberWithCountryCode:productObject.productCountryCode withNumber:productObject.productUserNumber];
    }
}

//===============================================Chat btn aaction

- (IBAction)chatBtnClicked:(id)sender
{
    if ([UserSetting isUserRegistered] == false) {
        [SharedViewManager showLoginScreen:self];
        return;
    }
    if (![ALUserDefaultsHandler isLoggedIn]){
    
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [hud setLabelText:NSLocalizedString(@"Loading Chat", Nil)];
        
        ALChatManager * chatManager = [[ALChatManager alloc] initWithApplicationKey:APPLOZIC_APPLICATION_ID];
        [chatManager registerUserWithCompletion:[UserSetting generateUserForApplozic] withHandler:^(ALRegistrationResponse *rResponse, NSError *error) {
           
            if (error) {
                [DialogManager makeToastWithString:NSLocalizedString(@"There is some problem with chat", nil)];
                //[DialogManager makeToastOnly:NSLocalizedString(@"There is some problem with chat", nil)];
                return;
            }
            
            [hud hide:true];
            
            [self openChatView];
            
        }];
    }else{
        [self openChatView];
    }
}

-(void) openChatView{
    
    NSString* userIdOfAdPoster = productObject.productUserNumber;
    NSString* clientGroupId = [NSString stringWithFormat:@"%@%@%@",[UserSetting getUserNumber], productObject.productId, userIdOfAdPoster];//[self buildUniqueClientId:itemId withUserId:userIdOfReceiver];
    ALChatManager * chatManager = [[ALChatManager alloc] initWithApplicationKey:APPLOZIC_APPLICATION_ID];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.navigationController = self.navigationController;
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [hud setLabelText:NSLocalizedString(@"Loading Chat", Nil)];
    
    [chatManager launchGroupOfTwoWithClientId:clientGroupId withMetaData:[self getSellerGroupMetadata] andWithUser:userIdOfAdPoster andFromViewController:self andLoader:hud];
}

-(NSMutableDictionary*)getSellerGroupMetadata{
    
    NSMutableDictionary *metaData = [NSMutableDictionary new];
    
    [metaData setObject:productObject.productId forKey:@"topicId"];
    [metaData setObject:productObject.productTilte forKey:@"title"];
    [metaData setObject:productObject.productMainImageUrl forKey:@"link"];
    [metaData setObject:productObject.productUserImageUrl forKey:@"key1"];
    [metaData setObject:[productObject.productUserName isEqualToString:@"No Name"] ? productObject.productUserNumber : productObject.productUserName forKey:@"value1"];
    [metaData setObject:productObject.productDate forKey:@"key2"];
    [metaData setObject:productObject.productPrice forKey:@"value2"];

    [metaData setObject:[metaData valueForKey:@"value1"] forKey:@"receiver"];
    [metaData setObject:productObject.productUserNumber forKey:@"receiverPhone"];
    [metaData setObject:productObject.productUserImageUrl forKey:@"receiverPhoto"];
    
    [metaData setObject:[UserSetting getUserName] forKey:@"sender"];
    [metaData setObject:[UserSetting getUserNumber] forKey:@"senderPhone"];
    [metaData setObject:[UserSetting getUserPhoto] forKey:@"senderPhoto"];
    
//    [metaData setObject:[metaData valueForKey:@"value1"] forKey:@"ProductOwnerName"];
//    [metaData setObject:productObject.productUserNumber forKey:@"ProductOwnerNumber"];
//    [metaData setObject:[UserSetting getUserName] forKey:@"ProductCustomerName"];
//    [metaData setObject:[UserSetting getUserNumber] forKey:@"ProductCustomerNumber"];
    
    return metaData;
}

-(NSString*) buildUniqueClientId:(NSString*)ItemId withUserId:(NSString*)userId
{
    NSString * loggedInId =  [ALUserDefaultsHandler getUserId];
    NSArray * sortedArray = [ @[loggedInId,userId] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    return [NSString stringWithFormat:@"%@_%@_%@", ItemId,sortedArray[0],sortedArray[1]];
    
}

//===============================================Start Chat From Adposter side

-(void)createChatFromAdOwnerSideWithNumber:(NSString*)strCommenterNumber commenterName:(NSString*)strCommenterName commenterPhoto:(NSString*)strCommenterPhoto
{
    if (![ALUserDefaultsHandler isLoggedIn]){
        
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [hud setLabelText:NSLocalizedString(@"Loading Chat", Nil)];
        
        ALChatManager * chatManager = [[ALChatManager alloc] initWithApplicationKey:APPLOZIC_APPLICATION_ID];
        [chatManager registerUserWithCompletion:[UserSetting generateUserForApplozic] withHandler:^(ALRegistrationResponse *rResponse, NSError *error) {
            
            if (error) {
                [DialogManager makeToastWithString:NSLocalizedString(@"There is some problem with chat", nil)];
                //[DialogManager makeToastOnly:NSLocalizedString(@"There is some problem with chat", nil)];
                return;
            }
            
            [hud hide:true];
            
            [self openChatForAdPosterWithNumber:strCommenterNumber commenterName:strCommenterName commenterPhoto:strCommenterPhoto];
            
        }];
    }else{
        [self openChatForAdPosterWithNumber:strCommenterNumber commenterName:strCommenterName commenterPhoto:strCommenterPhoto];
    }
}

-(void) openChatForAdPosterWithNumber:(NSString*)strCommenterNumber commenterName:(NSString*)strCommenterName commenterPhoto:(NSString*)strCommenterPhoto{
    
    NSString* userIdOfCustomer = strCommenterNumber;
    NSString* clientGroupId = [NSString stringWithFormat:@"%@%@%@",strCommenterNumber, productObject.productId, [UserSetting getUserNumber]];
    ALChatManager * chatManager = [[ALChatManager alloc] initWithApplicationKey:APPLOZIC_APPLICATION_ID];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.navigationController = self.navigationController;
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [hud setLabelText:NSLocalizedString(@"Loading Chat", Nil)];
    
    
    NSMutableDictionary *metaData = [NSMutableDictionary new];
    
    [metaData setObject:productObject.productId forKey:@"topicId"];
    [metaData setObject:productObject.productTilte forKey:@"title"];
    [metaData setObject:productObject.productMainImageUrl forKey:@"link"];
    [metaData setObject:productObject.productUserImageUrl forKey:@"key1"];
    [metaData setObject:[UserSetting getUserName] forKey:@"value1"];
    [metaData setObject:productObject.productDate forKey:@"key2"];
    [metaData setObject:productObject.productPrice forKey:@"value2"];
    
    
    [metaData setObject:strCommenterName forKey:@"receiver"];
    [metaData setObject:strCommenterNumber forKey:@"receiverPhone"];
    [metaData setObject:strCommenterPhoto forKey:@"receiverPhoto"];
    
    [metaData setObject:[UserSetting getUserName] forKey:@"sender"];
    [metaData setObject:[UserSetting getUserNumber] forKey:@"senderPhone"];
    [metaData setObject:[UserSetting getUserPhoto] forKey:@"senderPhoto"];

    
    /*[metaData setObject:[metaData valueForKey:@"value1"] forKey:@"ProductOwnerName"];
    [metaData setObject:[UserSetting getUserNumber] forKey:@"ProductOwnerNumber"];
    [metaData setObject:strCommenterName forKey:@"ProductCustomerName"];
    [metaData setObject:strCommenterNumber forKey:@"ProductCustomerNumber"];*/
    
    
    [chatManager launchGroupOfTwoWithClientId:clientGroupId withMetaData:metaData andWithUser:userIdOfCustomer andFromViewController:self andLoader:hud];
}


//===============================================msg btn aaction
- (IBAction)messagBtnClicked:(id)sender {
    //[SystemUtility smsNumberInView:self andNumber:productObject.productUserNumber andDelegate:self];
    [SystemUtility smsNumberInView:self andCountryCode:productObject.productCountryCode andNumber:productObject.productUserNumber andDelegate:self];
}

- (void)alertView:(UIAlertView *)alertView
didDismissWithButtonIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 1) {
        UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
        //pasteboard.string = [NSString stringWithFormat:@"%@%@", @"00974", productObject.productUserNumber];
        pasteboard.string = [NSString stringWithFormat:@"%@%@", productObject.productCountryCode, productObject.productUserNumber];
    }
}

- (void)messageComposeViewController:
(MFMessageComposeViewController *)controller
                 didFinishWithResult:(MessageComposeResult)result {
    [self dismissViewControllerAnimated:YES completion:Nil];
}
//===============================================load cells
- (UITableViewCell *)loadImageViewCell:(UITableView *)tableView
                             withIndex:(NSIndexPath *)indexPath {
    //AddAsCompany
    
    // image collection viewer
    ImageViewerTableCell *cell =
    [tableView dequeueReusableCellWithIdentifier:ImageViewerCellIdentifier
                                    forIndexPath:indexPath];
    
    if([[SharedData getDeviceLanguage]isEqualToString:@"en"]){
        /*[cell.addImageInEditBtn setContentHorizontalAlignment:UIControlContentHorizontalAlignmentRight];
        [cell.setAsDefaultInEditBtn setContentHorizontalAlignment:UIControlContentHorizontalAlignmentRight];
        [cell.deleteInEditBtn setContentHorizontalAlignment:UIControlContentHorizontalAlignmentRight];*/
    }else{
        
        [cell.setAsDefaultInEditBtn.titleLabel setFont:[UIFont systemFontOfSize:9 weight:UIFontWeightBold]];
        
        [cell.addImageInEditBtn setTitleEdgeInsets:UIEdgeInsetsMake(0, 3, 0, 0)];
        [cell.setAsDefaultInEditBtn setTitleEdgeInsets:UIEdgeInsetsMake(0, 2, 0, 0)];
        [cell.deleteInEditBtn setTitleEdgeInsets:UIEdgeInsetsMake(0, 3, 0, 0)];
        
        [cell.addImageInEditBtn setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
        [cell.setAsDefaultInEditBtn setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
        [cell.deleteInEditBtn setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    }
    [cell loadCell:self isAddCell:self.isAddProductDetail isEditAdvertise:self.isEditAdvertise
 withProductObject:productObject isloadingProductDetail:isProductDetailLoading withImageViewerDelegate:self];
    
    // ios 7 background issue
    [self setBackgroundClear:cell];
    return cell;
}
- (UITableViewCell *)loadImagePreviewCell:(UITableView *)tableView
                             withIndex:(NSIndexPath *)indexPath {
    // image collection viewer
    ImagePreviewerTableCell *cell =
    [tableView dequeueReusableCellWithIdentifier:ImagePreviewerCellIdentifier
                                    forIndexPath:indexPath];
    [cell loadCell:self isAddCell:self.isAddProductDetail isEditAdvertise:self.isEditAdvertise withProductObject:productObject isloadingProductDetail:isProductDetailLoading isFromCategoryProduct:self.isFromCategoryProducts withImageViewerDelegate:self];
    
    return cell;
}
//===============================================load cells
- (UITableViewCell *)loadRelatedViewCell:(UITableView *)tableView
                             withIndex:(NSIndexPath *)indexPath {
    // image collection viewer
    relateAdTableCell =
    [tableView dequeueReusableCellWithIdentifier:RelatedImageViewCellIdentifier
                                    forIndexPath:indexPath];
    
    [relateAdTableCell loadCell:self
                      isAddCell:self.isAddProductDetail
                isEditAdvertise:self.isEditAdvertise
              withProductObject:productObject
         isloadingProductDetail:isProductDetailLoading withArray:relatedAdsArry
        withImageViewerDelegate:self];

    // ios 7 background issue
        [self setBackgroundClear:relateAdTableCell];
    
    
    return relateAdTableCell;
}
- (void)setBackgroundClear:(UITableViewCell *)cell {
    cell.backgroundColor = [UIColor clearColor];
}

- (UITableViewCell *)loadProductInfoCell:(UITableView *)tableView
                               withIndex:(NSIndexPath *)indexPath {
    // product info viewer
    if ([self isAddProductDetail] == false) {
        
        NewProductInfoViewerTableCell * cell = [tableView
                                            dequeueReusableCellWithIdentifier:ProductInfoCellViewerIdentifier
                                            forIndexPath:indexPath];
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
            cell.bottomViewHeight.constant = 55;
            cell.accessoryBtnHeight.constant = 66;
            cell.accessoryBtnWidth.constant = 316;
        }else{
            cell.bottomViewHeight.constant = 40;
            cell.accessoryBtnHeight.constant = 33;
            cell.accessoryBtnWidth.constant = (self.view.frame.size.width*207)/320;
        }
        [cell setViewProductInfoUi:productObject];
        [cell setProductObject:productObject
                  InParentView:self
                   inTableView:self.tableView];
        // ios 7 background issue
        [self setBackgroundClear:cell];
        
        return cell;
    } else {
        ProductInfoTableCell *cell = nil;
        
        int languageIndexToCompare;
        if (self.isEditAdvertise) {
            languageIndexToCompare = [UserSetting
                                      getAdvertiseTypeServerIndex:productObject.productLanguage];
        } else {
            languageIndexToCompare = UserSetting.getUserAddAdvertiseLanguage;
        }
        
        if (languageIndexToCompare == UserSetting.EnglishAndArabicLanguageKey) {
            cell =
            [tableView dequeueReusableCellWithIdentifier:ProductInfoCellIdentifier
                                            forIndexPath:indexPath];
        } else {
            cell = [tableView
                    dequeueReusableCellWithIdentifier:ProductInfoCellOneLanguageIdentifier
                    forIndexPath:indexPath];
        }
        
        [cell setProductInfoCell:productObject
                   andParentView:self
                     andDelegate:self
               fromEditAdvertise:self.isEditAdvertise];
        
        // ios 7 background issue
        [self setBackgroundClear:cell];
        
        return cell;
    }
}
#pragma mark NewDescriptionTableViewCell
- (UITableViewCell *)loadProductDetailActionBarCell:(UITableView *)tableView
                                          withIndex:(NSIndexPath *)indexPath {
    // product info viewer
    __weak NewDescriptionTableViewCell *cell = [tableView
                                        dequeueReusableCellWithIdentifier:descriptionCell];
    if (productObject.isHideAddCommentsAndLikes == true)
    {
        cell.likeBtn.enabled = NO;
    }
   
    cell.delegate = self;
    [cell setViewDescriptionInfoUi:productObject InParentView:self];
    [cell setLikeCoundUpdate:^{
        [cell setViewDescriptionInfoUi:productObject InParentView:self];
    }];
    
    // ios 7 background issue
    [self setBackgroundClear:cell];
    
    return cell;
}
-(void)setUrlToDeepLink:(NSString *)strURL{
    DeepLinkVC *deepLinkVC = [self.storyboard instantiateViewControllerWithIdentifier:@"DeepLinkVC"];
    deepLinkVC.strUrl = strURL;
    [self.navigationController pushViewController:deepLinkVC animated:YES];
}

//===============================================comment sent

//===============================================comment sent
- (void)commentSent {
    [self refershProductComments:requestGetProductComments];
}
//===============================================more button clicked
- (void)
moreClickedFinishedWithLoadingIndicator:(UIActivityIndicatorView *)indicator
andResultTextField:(UITextField *)resultTextFieldParam andButton:(UIButton *)moreButton;
{
    moreBtn = moreButton;
    
    moreBtnIndicator = indicator;
    moreBtnResultTextField = resultTextFieldParam;
    
    [moreBtnIndicator startAnimating];
    moreBtnResultTextField.hidden = YES;
    
    [self refershProductComments:requestMoreCommentsBtn];
}

//===============================================delete button clicked
- (void)deleteCommentCLicked:(UIActivityIndicatorView *)indicator
          andResultTextField:(UITextField *)resultTextFieldParam {
    [self refershProductComments:requestDeleteProductComment];
}

//#TODO: must be handled in different way
//=================================================product info update
// camera image
- (void)imageAddedToProductObject:(UIImage *)newImage
                  isEditAdvertise:(BOOL)isEditAdvertise {
    if (tempHoldingImage == nil) {
        tempHoldingImage = [[UIImage alloc] init];
    }
    
    tempHoldingImage = nil;
    
    tempHoldingImage = [newImage copy];
    
    if (productObject.productImagesOnDevices == nil) {
        productObject.productImagesOnDevices = [[NSMutableArray alloc] init];
    }
    
    BOOL my_value = [SharedData getIsFromSelectImageViewControllerBool];
    if (my_value) {
        [self showAlertViewFromImageTitle];
        [SharedData setIsFromSelectImageViewController:NO];
    } else {
        [productObject.productImagesOnDevices addObject:newImage];
        [self.tableView reloadData];
    }
}

- (void)productInfoChanged:(ProductObject *)productObjectParam {
    productObject = productObjectParam;
}
//==============================================rotation

- (void)willRotateToInterfaceOrientation:
(UIInterfaceOrientation)toInterfaceOrientation
                                duration:(NSTimeInterval)duration {
    if (self.isAddProductDetail == false) {
        if (toInterfaceOrientation == UIInterfaceOrientationPortrait ||
            toInterfaceOrientation == UIInterfaceOrientationPortraitUpsideDown) {
            isCurrentViewPortrait = true;
        } else {
            isCurrentViewPortrait = false;
        }
        
        [self.tableView reloadData];
    } else {
        [super willRotateToInterfaceOrientation:toInterfaceOrientation
                                       duration:duration];
    }
}

//===================================================prepare for segue

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:[SharedViewManager registerSegue]]) {
        RegisterScreen *registerScreen = [segue destinationViewController];
        
        [registerScreen setSuccessScreenSegueName:nil];
    }
    else if ([[segue identifier] isEqualToString:@"moreInfo"]) {
        MoreDescriptionViewController *userProducts = [segue destinationViewController];
        [userProducts setProductObject:self.productObject];
    }
    else if ([[segue identifier] isEqualToString:@"UserProfileIdentifier"]) {
        UserProducts *userProducts = [segue destinationViewController];
        
        [userProducts setUserProfileProductObject:self.productObject];
        [userProducts showBackButton:YES];
        [userProducts setHideTabBar:YES];
        
    }else if([[segue identifier] isEqualToString:@"WhyCompanyAdding"]){
        NSLog(@"WhyCompanyAdding Viewcontroller");
    }
}

- (void)addAdvertiseClickedInCategory:(NSString *)categoryId
                            withTitle:(NSString *)productTitle
                     withTitleEnglish:(NSString *)productTitleEnglish
                      withTitleArabic:(NSString *)productTitleArabic
                     withProductPrice:(NSString *)productPrice
               withProductDescription:(NSString *)productDescription
        withProductDescriptionEnglish:(NSString *)productDescriptionEnglish
         withProductDescriptionArabic:(NSString *)productDescriptionArabic
                  withProductLanguage:(NSString *)productLang
         withIsCategoryFiltersChoosed:(BOOL)isCategoryFiltersChoosed
{
    if ([self validateInfoToAddwithTitle:productTitle
                 withProductTitleEnglish:productTitleEnglish
                  withProductTitleArabic:productTitleArabic
                        withProductPrice:productPrice
                  withProductDescription:productDescription
           withProductDescriptionEnglish:productDescriptionEnglish
            withProductDescriptionArabic:productDescriptionArabic
                              inCategory:categoryId
                              inLanguage:productLang isCategoryFiltersChoosed:isCategoryFiltersChoosed
         ] == false) {
        return;
    }
    
    if ([SystemUtility checkConnection] == false) {
        return;
    }
    
    isAddAdcertiseBtnClicked = true;
    // issue in keyboard when moving another screen
    [self.view endEditing:YES];

    ProductObject *productToUpload = [[ProductObject alloc] init];
    
    if (_isEditAdvertise) {
        productToUpload.productId = productObject.productId;
    } else {
        productToUpload.productId = @"";
    }
    
    productToUpload.productTilte = productTitle;
    productToUpload.productTilteEnglish = productTitleEnglish;
    productToUpload.productTilteArabic = productTitleArabic;
    productToUpload.productPrice = productPrice;
    productToUpload.productDescription = productDescription;
    productToUpload.productDescriptionEnglish = productDescriptionEnglish;
    productToUpload.productDescriptionArabic = productDescriptionArabic;
    productToUpload.productImagesOnDevices = productObject.productImagesOnDevices;
    productToUpload.productLikeCount = @"0";
    productToUpload.productCommentCount = @"0";
    productToUpload.productCategoryId = categoryId;
    productToUpload.productSubCategoryId = productObject.productSubCategoryId;
    productToUpload.productLanguage = productLang;
    productToUpload.isUploadAdvertiseImages =
    productObject.isUploadAdvertiseImages;
    productToUpload.isResetProductImages = productObject.isResetProductImages;
    productToUpload.isEditingAdvertise = self.isEditAdvertise;
    productToUpload.isNeedToUploadedAdvertiseOnServer = YES;
    productToUpload.productAdvertiseTypeId = productObject.productAdvertiseTypeId;
    productToUpload.productCityId = productObject.productCityId;
    productToUpload.productRegionId = productObject.productRegionId;
    productToUpload.productSubsubCategoryId =
    productObject.productSubsubCategoryId;
    productToUpload.productManfactureYear = productObject.productManfactureYear;
    productToUpload.productKm = productObject.productKm;
    productToUpload.productFurnishedTypeId = productObject.productFurnishedTypeId;
    productToUpload.productNumberOfRooms = productObject.productNumberOfRooms;
    
    [SharedGraphicInfo addToUploadingProductList:productToUpload];
    
    
    self.tabBarController.selectedIndex = 2;
    
    [self.navigationController popToRootViewControllerAnimated:NO];
    
    NSDictionary *articleParams =
    [NSDictionary dictionaryWithObjectsAndKeys:
     productToUpload.productCategoryId, @"category id",
     productToUpload.productSubCategoryId, @"subctegory",
     productToUpload.productLanguage, @"language", nil];
    
    //Google analytics send dictionary
    //=======================================================================
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];

    GAIDictionaryBuilder *m =
    [GAIDictionaryBuilder createEventWithCategory:@"ui"
                                           action:@"Advertisement Added"
                                            label:@"Advertisement"
                                            value:nil];
    [tracker send:[[m setAll:articleParams]build]];
    //=======================================================================
}

- (void)addAdvertiseLanguageChanged:(int)languageIndex {
    [UserSetting saveUserAddAdvertiseLanguage:languageIndex];
    
    productObject.productLanguage =
    [UserSetting getAdvertiseTypeServerParam:languageIndex];
    
    [self.tableView reloadData];
}

- (bool)validateInfoToAddwithTitle:(NSString *)productTitle
           withProductTitleEnglish:(NSString *)productTitleEnglish
            withProductTitleArabic:(NSString *)productTitleArabic
                  withProductPrice:(NSString *)productPrice
            withProductDescription:(NSString *)productDescription
     withProductDescriptionEnglish:(NSString *)productDescriptionEnglish
      withProductDescriptionArabic:(NSString *)productDescriptionArabic
                        inCategory:(NSString *)categoryId
                        inLanguage:(NSString *)language
          isCategoryFiltersChoosed:(BOOL)isCategoryFIltersChoosed
{
    if (productObject.productImagesOnDevices.count < 1) {
        [DialogManager showErrorProductImagesCount];
        
        return false;
    }
    
    if ([categoryId isEqualToString:@"-1"]) {
        [DialogManager showErrorCategoryNotChosen];
        
        return false;
    }
    
    if (isCategoryFIltersChoosed ==false) {
        [DialogManager showErrorCompleteAllFieldsInCategoryFilters];
        
        return false;
    }
    
    if ([language isEqualToString:@"aren"]) {
        if ([productTitleEnglish stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length<1) {
            [DialogManager showErrorProductTitleEnglishEmpty];
            
            return false;
        }
        
        if ([productTitleArabic stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length<1) {
            [DialogManager showErrorProductTitleArabicEmpty];
            
            return false;
        }
        
        if ([productDescriptionEnglish stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length<1) {
            [DialogManager showErrorProductDescriptionEnglishEmpty];
            
            return false;
        }
        
        if ([productDescriptionArabic stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length<1) {
            [DialogManager showErrorProductDescriptionArabicEmpty];
            
            return false;
        }
    } else {
        if ([productTitle stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length<1) {
            [DialogManager showErrorProductTitleEmpty];
            
            return false;
        }
        
        if ([productDescription stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length<1) {
            [DialogManager showErrorProductDescriptionEmpty];
            
            return false;
        }
    }
    
    if ([productPrice stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length<1  && productObject.isAd == false) {
        [DialogManager showErrorProductPriceEmpty];
        
        return false;
    }
    
    return true;
}

- (void)dealloc {
    //  [SharedData clearSdImageMemory];
    
    isProductDetailViewFinished = true;
    
}

- (void)viewDidUnload {
       
    [super viewDidUnload];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    //[SharedData clearSdImageMemory];
}
#pragma mark - KeyBoard Functioning -
- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillChangeFrame:) name:UIKeyboardWillChangeFrameNotification object:nil];
}
- (void)unregisterForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillChangeFrameNotification object:nil];
}
-(void)keyboardWillChangeFrame:(NSNotification *)notification
{
    AppDelegate * appDel = (AppDelegate *)[UIApplication sharedApplication].delegate;
    NSDictionary* info = [notification userInfo];
    // keyboard frame is in window coordinates
    CGRect keyboardFrame = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    // convert own frame to window coordinates, frame is in superview's coordinates
    CGRect ownFrame = [appDel.window convertRect:self.view.frame fromView:self.view.superview];
    // calculate the area of own frame that is covered by keyboard
    CGRect coveredFrame = CGRectIntersection(ownFrame, keyboardFrame);
    // now this might be rotated, so convert it back
    coveredFrame = [appDel.window convertRect:coveredFrame toView:self.view.superview];
    CGSize tblsize = self.tableView.frame.size;
    if(keyboardSize.height == 0)
    {
        
//        tblsize = CGSizeMake(self.tableView.contentSize.width, [UIScreen mainScreen].bounds.size.height - coveredFrame.size.height);
    }
    else
    {
        tblsize = CGSizeMake(self.tableView.contentSize.width, [UIScreen mainScreen].bounds.size.height-self.navigationController.navigationBar.frame.size.height-[UIApplication sharedApplication].statusBarFrame.size.height);
    }
    self.tableView.frame = CGRectMake(self.tableView.frame.origin.x, self.tableView.frame.origin.y, tblsize.width, tblsize.height);
    keyboardSize=coveredFrame.size;
}

#pragma mark - Adding text over Image.

- (void)showAlertViewFromImageTitle {
    UIAlertView *alertView = [[UIAlertView alloc]
                              initWithTitle:NSLocalizedString(@"IMAGE_TITLE", Nil)
                              message:@"Please enter a title to be displayed on the image"
                              delegate:self
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil];
    alertView.tag = 1;
    alertView.alertViewStyle = UIAlertViewStylePlainTextInput;
    [alertView textFieldAtIndex:0].delegate = self;
    [alertView show];
}

- (void)alertView:(UIAlertView *)alertView
clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (alertView.tag == 1) {
        //--> Get the text field result and pass it to the function that will take
        // the newImage and text and make it a new image.
        
        // UITextField *title1 = [alertView textFieldAtIndex:0];
        
        // title1= [alertView textFieldAtIndex:0];
        //        NSString *title = title1.text;
        // NSLog(@"The name is %@",title);
        // NSLog(@"Using the Textfield: %@",[[alertView textFieldAtIndex:0] text]);
    }
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range
replacementString:(NSString *)string {
    /////GET TEXT LIMIT FROM NSUSERDEFAULT
    
    int *limitFromWeb = [[[NSUserDefaults standardUserDefaults]
                               objectForKey:@"characterLimit"] intValue];
    
    int *length = textField.text.length;
    
    if (length > limitFromWeb) {
        return NO;
    } else {
        return YES;
    }
}


-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    textField.placeholder = nil;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    if ([textField.text isEqualToString:@""]==false) {
        _textOnImage = textField.text;
    }else
    {
        textField.text = @"0";
    }
    // NSLog(@"%@",_textfOnImage);
    
    [productObject.productImagesOnDevices
     addObject:[self createNewImageWithText:_textOnImage
                                   andImage:tempHoldingImage]];
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    NSArray *indexPaths = [[NSArray alloc] initWithObjects:indexPath, nil];
    [self.tableView reloadRowsAtIndexPaths:indexPaths
                          withRowAnimation:UITableViewRowAnimationNone];
}

#pragma mark - TextView Delegate -
-(BOOL)textViewShouldBeginEditing:(UITextView *)textView {
    if ([textView.text isEqualToString:@"0"]) {
        textView.text=@"";
    }
    return YES;
}
-(void)textViewDidEndEditing:(UITextView *)textView {
    if([textView.text length]==0) {
        textView.text=@"0";
    }
}
/////Convert HexColor String to UiColor
- (UIColor *)colorFromHexString:(NSString *)hexString {
    unsigned rgbValue = 0;
    NSScanner *scanner = [NSScanner scannerWithString:hexString];
    [scanner setScanLocation:1]; // bypass '#' character
    [scanner scanHexInt:&rgbValue];
    return [UIColor colorWithRed:((rgbValue & 0xFF0000) >> 16) / 255.0
                           green:((rgbValue & 0xFF00) >> 8) / 255.0
                            blue:(rgbValue & 0xFF) / 255.0
                           alpha:1.0];
}

#pragma mark -Add text on image
- (UIImage *)createNewImageWithText:(NSString *)title
                           andImage:(UIImage *)image
{
    if (title == nil || [title isEqualToString:@""]) {
        title = @"";
    }
    
    /////Get Color from NSUSERDefault
    NSData *colorData =
    [[NSUserDefaults standardUserDefaults] objectForKey:@"myColorData"];
    UIColor *Color = [NSKeyedUnarchiver unarchiveObjectWithData:colorData];
    
    CGSize size = CGSizeMake(image.size.width, image.size.height);
    UIGraphicsBeginImageContextWithOptions(size, YES, 0.0);
    
    // draw base image
    [image drawInRect:CGRectMake(0, 0, size.width, size.height)];
    
    // draw text over image
    CGRect txtRect = CGRectMake(0, image.size.width / 2.5, image.size.width,
                                image.size.height);
    UITextView *txtView = [[UITextView alloc] init];
    [txtView setText:[NSString stringWithFormat:@"%@", title]];
    NSMutableParagraphStyle *textStyle = [
                                           [NSMutableParagraphStyle defaultParagraphStyle] mutableCopy];
    textStyle.lineBreakMode = NSLineBreakByWordWrapping;
    textStyle.alignment = NSTextAlignmentCenter;
    
    UIFont *textFont = [UIFont systemFontOfSize:50];
    NSDictionary *dictionary = @{
                                 NSFontAttributeName : textFont,
                                 NSParagraphStyleAttributeName : textStyle,
                                 NSForegroundColorAttributeName : Color
                                 };
    
    [txtView.text drawInRect:txtRect withAttributes:dictionary];
    
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return img;
}
-(void)showProductWithId:(NSString *)productId
{
    ProductDetailsViewController *productDetail = [self.storyboard
                                                   instantiateViewControllerWithIdentifier:@"ProductDetailsViewController"];
    [productDetail setProductId:productId];
    [self.navigationController pushViewController:productDetail animated:YES];
}
- (IBAction)backButtonPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}


@end
