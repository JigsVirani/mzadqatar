#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>
@protocol SelectLatLong <NSObject>
- (void)selectedLatitude:(NSString *)strLatitude withLongitude:(NSString *)strLongitude withAddress:(NSString *)strAddress;

@end

@interface LocationVC : UIViewController<CLLocationManagerDelegate,MKMapViewDelegate>{
    CLLocationManager *locationManager;
    float selected_latitude,selected_longitude,current_lat,current_long;
    __weak IBOutlet UIActivityIndicatorView *loader;
    NSString *strAddress;
    UIImageView *pinCustom;
}
@property (weak, nonatomic) IBOutlet UILabel *lblCurrentLocation;
@property (weak, nonatomic) IBOutlet UILabel *lblCurrnetLocTitle;
@property (weak, nonatomic) IBOutlet UIButton *btnLocation,*btnRecentLocation;
@property(nonatomic, strong) id<SelectLatLong> delegate;
@property (weak, nonatomic) IBOutlet MKMapView *mapLocation;
- (IBAction)back:(id)sender;
- (IBAction)selectLocation:(id)sender;
- (IBAction)getCurrentLocation:(id)sender;

@end
