//
//  SendCommentTableCell.m
//  MazadQatar
//
//  Created by samo on 4/17/13.
//  Copyright (c) 2013 Mohammed abdulfattah attia. All rights reserved.
//

#import "SendCommentTableCell.h"
#import "GAI.h"
#import "GAIDictionaryBuilder.h"

@implementation SendCommentTableCell

NSString *requestAddProductComments = @"requestAddProductComments";

- (id)initWithStyle:(UITableViewCellStyle)style
    reuseIdentifier:(NSString *)reuseIdentifier {
  self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
  if (self) {
    // Initialization code
  }
  return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
  [super setSelected:selected animated:animated];

  // Configure the view for the selected state
}

- (void)setProductOfComment:(ProductObject *)productObjectParam
               withDelegate:(id<SendCommentDelegate>)delegateParam
           inViewController:(UIViewController *)view;
{
  delegate = delegateParam;
  productObject = productObjectParam;
  parentView = view;

  [self createKeyboardAccessoryView];

  if ([[UserSetting getUserName] isEqualToString:@" "] ||
      [[UserSetting getUserName] length] == 0) {
    self.userCommentName.text = NSLocalizedString(@"WRITE_COMMENT_NAME", nil);
    // self.userCommentName.textColor = [UIColor lightGrayColor];
  } else {
    self.userCommentName.text = [UserSetting getUserName];
  }

  self.userComment.text = NSLocalizedString(@"WRITE_COMMENT", nil);

  if (productObject.isHideAddCommentsAndLikes == true) {
    [self.sendBtn setEnabled:false];
    [self.sendBtnResultText setHidden:false];
    [self.sendBtnResultText
        setText:NSLocalizedString(@"SEND_COMMENT_NOT_ENABLED_IN_ADVERTISE",
                                  nil)];
  }
}

- (IBAction)sendBtnClicked:(id)sender {
  [self endEditing:YES];
     if ([UserSetting isUserRegistered] == false) {
    [SharedViewManager showLoginScreen:parentView];
    return;
  }

  if ([self validateInput] == false) {
    return;
  }

  self.sendBtnResultText.hidden = YES;

  [UserSetting saveUserName:self.userCommentName.text];
  [self.sendCommentIndicator startAnimating];
  [ServerManager addCommentToProduct:productObject
                        fromUserName:self.userCommentName.text
                  andUserCountryCode:@"00974"
                       andUserNumber:[UserSetting getUserNumber]
                         withComment:self.userComment.text
                         AndDelegate:self
                       withRequestId:requestAddProductComments];

    //=================================================
    // Log setting open event with category="ui", action="open", and label="settings".
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];

    [tracker send:
     [[GAIDictionaryBuilder createEventWithCategory:@"ui"
                                             action:@"Comment Sent"
                                              label:@"Add Comment"
                                              value:nil] build]];
    //=================================================
    

}

- (bool)validateInput {
  if ([self validateCommentInput] == false) {
    [DialogManager showErrorUserCommentEmpty];
    return false;
  }

  if ([self validateCommentNameInput] == false) {
    [DialogManager showErrorUserCommentNameEmpty];
    return false;
  }

  return true;
}

- (bool)validateCommentInput {
  NSCharacterSet *set = [NSCharacterSet whitespaceCharacterSet];
  if ([[self.userComment.text stringByTrimmingCharactersInSet:set] length] ==
          0 ||
      [self.userComment.text
          isEqualToString:NSLocalizedString(@"WRITE_COMMENT", nil)]) {
    return false;
  }

  return true;
}

- (bool)validateCommentNameInput {
  NSCharacterSet *set = [NSCharacterSet whitespaceCharacterSet];

  if ([[self.userCommentName.text
          stringByTrimmingCharactersInSet:set] length] == 0 ||
      [self.userCommentName.text
          isEqualToString:NSLocalizedString(@"WRITE_COMMENT_NAME", nil)]) {
    return false;
  }

  return true;
}

- (void)requestFinished:(ServerManagerResult *)serverManagerResult
            ofRequestId:(NSString *)requestId
    withNumberOfRequest:(int)numberOfRequest {
  if ([serverManagerResult.opertationResult
          isEqualToString:[ServerManager ERROR_IN_CONNECTION_OPERATION]]) {
    [DialogManager showErrorInConnection];
  }

  if (requestId == requestAddProductComments) {
    //[ self.sendCommentIndicator stopAnimating];
    [self.sendCommentIndicator stopAnimating];

    // user is blocked
    if ([serverManagerResult.opertationResult
            isEqualToString:[ServerManager ERROR_USER_BLOCKED_FROM_ADMIN]]) {
      [DialogManager showErrorUserIsBlocked];
      return;
    }

    // show status text field
    self.sendBtnResultText.hidden = NO;

    // set status text
    if ([serverManagerResult.opertationResult
            isEqualToString:[ServerManager SUCCESS_OPERATION]]) {
      self.sendBtnResultText.text = NSLocalizedString(@"SUCCESS_ADD", nil);
    } else if ([serverManagerResult.opertationResult
                   isEqualToString:[ServerManager FAILED_OPERATION]])

    {
      self.sendBtnResultText.text = NSLocalizedString(@"ERROR_ADD", nil);
    } else if ([serverManagerResult.opertationResult
                   isEqualToString:[ServerManager ALREADED_ADDED_OPERATION]])

    {
      self.sendBtnResultText.text = NSLocalizedString(@"ALREADY_ADD", nil);
    }

    [delegate commentSent];
  }
}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView {
  [self createKeyboardAccessoryView];

  if (textView == self.userCommentName) {
    if ([self.userCommentName.text
            isEqualToString:NSLocalizedString(@"WRITE_COMMENT_NAME", nil)]) {
      self.userCommentName.text = @"";
    }
  } else if (textView == self.userComment) {
    if ([self.userComment.text
            isEqualToString:NSLocalizedString(@"WRITE_COMMENT", nil)]) {
      self.userComment.text = @"";
    }
  }

  return YES;
}

// handle keyboard text count
- (BOOL)textView:(UITextView *)textView
    shouldChangeTextInRange:(NSRange)range
            replacementText:(NSString *)text {
  if (textView == self.userCommentName) {
    NSUInteger newLength =
        [textView.text length] + [text length] - range.length;
    return (newLength > 30) ? NO : YES;
  } else if (textView == self.userComment) {
    NSUInteger newLength =
        [textView.text length] + [text length] - range.length;
    return (newLength > 500) ? NO : YES;
  }

  return YES;
}

- (BOOL)textViewShouldEndEditing:(UITextView *)textView {
  if (textView == self.userComment) {
    if ([StringUtility isTextEmpty:self.userComment.text] == 0) {
      //   self.userComment.textColor = [UIColor lightGrayColor];
      self.userComment.text = NSLocalizedString(@"WRITE_COMMENT", nil);
      [self.userComment resignFirstResponder];
    }
  } else if (textView == self.userCommentName) {
    if ([StringUtility isTextEmpty:self.userCommentName.text] == 0) {
      //  self.userCommentName.textColor = [UIColor lightGrayColor];
      self.userCommentName.text = NSLocalizedString(@"WRITE_COMMENT_NAME", nil);
      [self.userCommentName resignFirstResponder];
    }
  }

  return YES;
}

- (void)createKeyboardAccessoryView {
  if ([DisplayUtility isPad] == false) {
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    [nc addObserver:self
           selector:@selector(orientationChanged:)
               name:UIApplicationDidChangeStatusBarOrientationNotification
             object:nil];

    // Call the createInputAccessoryView method we created earlier.
    // By doing that we will prepare the inputAccView.
    [self createInputAccessoryView];

    // Now add the view as an input accessory view to the selected textfield.
    [_userCommentName setInputAccessoryView:inputAccView];
    [_userComment setInputAccessoryView:inputAccView];
  }
}


- (void)orientationChanged:(NSNotification *)note {
  isRotating = YES;

  [self recreateInputAccessoryView];
  // issue in keyboard when moving another screen
  [parentView.view endEditing:YES];
  //[self createKeyboardAccessoryView];
  //[self.view reloadInputViews];
  isRotating = NO;
}

- (void)recreateInputAccessoryView {

  CGSize screenSize = [DisplayUtility getScreenSize];

  [inputAccView setFrame:CGRectMake(10.0, 0.0, screenSize.width, 40.0)];

  [btnDone setFrame:CGRectMake(screenSize.width - 90, 3, 80.0f, 40.0f)];
}

- (void)doneTyping {
  // When the "done" button is tapped, the keyboard should go away.
  // That simply means that we just have to resign our first responder.
  [_userCommentName resignFirstResponder];
  [_userComment resignFirstResponder];
}

- (void)dealloc {

  NSNotificationCenter *nc =
      [NSNotificationCenter defaultCenter]; // Get the notification centre for
                                            // the app
  [nc removeObserver:self
                name:UIApplicationDidChangeStatusBarOrientationNotification
              object:nil];

}

@end
