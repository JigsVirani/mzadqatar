//
//  FileManager.m
//  Darefeud
//
//  Created by Waris Ali on 18/02/2013.
//  Copyright (c) 2013 auradynamic. All rights reserved.
//

#import "FileManager.h"

@implementation FileManager

+ (void)initPlistByName:(NSString *)fileName
{
  NSString *fullFileName = [NSString stringWithFormat:@"%@.plist", fileName];

  NSString *destPath = [NSSearchPathForDirectoriesInDomains(
      NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
  destPath = [destPath stringByAppendingPathComponent:fullFileName];

  // If the file doesn't exist in the Documents Folder, copy it.

  NSFileManager *fileManager = [NSFileManager defaultManager];

  if (![fileManager fileExistsAtPath:destPath]) {
    NSString *sourcePath =
        [[NSBundle mainBundle] pathForResource:fileName ofType:@"plist"];
    if (sourcePath != nil)
      [fileManager copyItemAtPath:sourcePath toPath:destPath error:nil];
    //   NSLog(@"Copying file into sandbox : %@",fileName);
  }
}

#pragma mark - Getting Plist

+ (NSMutableDictionary *)getDictFromFileName:(NSString *)fileName {
  NSString *fullFileName = [NSString stringWithFormat:@"%@.plist", fileName];
  NSString *docFolder = [NSSearchPathForDirectoriesInDomains(
      NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
  NSString *path = [docFolder stringByAppendingPathComponent:fullFileName];
  return [NSMutableDictionary dictionaryWithContentsOfFile:path];
}

+ (NSMutableArray *)getArrayFromFileName:(NSString *)fileName {
  NSString *fullFileName = [NSString stringWithFormat:@"%@.plist", fileName];
  NSString *docFolder = [NSSearchPathForDirectoriesInDomains(
      NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
  NSString *path = [docFolder stringByAppendingPathComponent:fullFileName];
  return [NSMutableArray arrayWithContentsOfFile:path];
}

+ (NSString *)getValueForKey:(NSString *)key fromPlist:(NSString *)fileName {
  NSDictionary *dict = [self getDictFromFileName:fileName];
  return [dict objectForKey:key];
}

#pragma mark - Writing to plist

+ (void)writeArray:(NSArray *)array toPlist:(NSString *)fileName {
  NSString *fullFileName = [NSString stringWithFormat:@"%@.plist", fileName];
  NSString *docFolder = [NSSearchPathForDirectoriesInDomains(
      NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
  NSString *path = [docFolder stringByAppendingPathComponent:fullFileName];
  [array writeToFile:path atomically:YES];
}

+ (void)writeDictionary:(NSDictionary *)dict toPlist:(NSString *)fileName {
  NSString *fullFileName = [NSString stringWithFormat:@"%@.plist", fileName];
  NSString *docFolder = [NSSearchPathForDirectoriesInDomains(
      NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
  NSString *path = [docFolder stringByAppendingPathComponent:fullFileName];
  [dict writeToFile:path atomically:YES];
}

@end
