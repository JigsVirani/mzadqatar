//
//  ProductObject.m
//  MazadQatar
//
//  Created by samo on 3/3/13.
//  Copyright (c) 2013 Mohammed abdulfattah attia. All rights reserved.
//

#import "ProductObject.h"

@implementation ProductObject

- (id)init {
    self = [super init];
    [self setIsNeedToUploadedAdvertiseOnServer:NO];
    [self setProductUploadingPercent:0];
    [self setIsUploading:NO];
    [self setIsUploadAdvertiseImages:NO];
    [self setIsResetProductImages:@"false"];
    [self setIsAd:false];
    [self setIsEditingAdvertise:NO];
    [self setIsReportingAdvertise:NO];
    [self setProductAdvertiseTypeId:@""];
    
    [self setProductCityId:@""];
    [self setProductRegionId:@""];
    [self setProductSubCategoryId:@""];
    [self setProductSubsubCategoryId:@""];
    [self setProductManfactureYear:@""];
    [self setProductManfactureYearFrom:@""];
    [self setProductManfactureYearTo:@""];
    [self setProductKm:@""];
    [self setProductKmFrom:@""];
    [self setProductKmTo:@""];
    [self setProductFurnishedTypeId:@""];
    [self setProductNumberOfRooms:@""];
    [self setProductNumberOfRoomsFrom:@""];
    [self setProductNumberOfRoomsTo:@""];
    [self setProductFilterType:@""];
    [self setProductPriceFrom:@""];
    [self setProductPriceTo:@""];
    [self setIsAllFiltersSelected:false];
    [self setProductLanguage:@""];
    [self setProductCompanyNumberOfAds:@""];

    self.productUserImage = [[UIImage alloc] init];
    self.productImagesOnDevices = [[NSMutableArray alloc] init];
    return self;
}

-(id) copyWithZ
{
    ProductObject *copyProductObject = [[ProductObject alloc] init];
    

    copyProductObject.productCityId = self.productCityId;
    copyProductObject.productCityName = self.productCityName;
    copyProductObject.productRegionId = self.productRegionId;
    copyProductObject.productRegionName = self.productRegionName;
    copyProductObject.productSubCategoryId = self.productSubCategoryId ;
    copyProductObject.productSubCategoryName = self.productSubCategoryName ;
    copyProductObject.productSubsubCategoryId = self.productSubsubCategoryId ;
    copyProductObject.productSubsubCategoryName = self.productSubsubCategoryName ;
    copyProductObject.productManfactureYear = self.productManfactureYear ;
    copyProductObject.productManfactureYearFrom = self.productManfactureYearFrom ;
    copyProductObject.productManfactureYearTo = self.productManfactureYearTo ;
    copyProductObject.productPrice = self.productPrice ;
    copyProductObject.productPriceFrom = self.productPriceFrom ;
    copyProductObject.productPriceTo = self.productPriceTo ;
    copyProductObject.productFurnishedTypeId = self.productFurnishedTypeId ;
    copyProductObject.productFurnishedTypeName = self.productFurnishedTypeName ;
    copyProductObject.productKm = self.productKm ;
    copyProductObject.productKmFrom = self.productKmFrom ;
    copyProductObject.productKmTo = self.productKmTo ;
    copyProductObject.productNumberOfRooms = self.productNumberOfRooms ;
    copyProductObject.productNumberOfRoomsFrom = self.productNumberOfRoomsFrom ;
    copyProductObject.productNumberOfRoomsTo = self.productNumberOfRoomsTo ;
    copyProductObject.isAllFiltersSelected=self.isAllFiltersSelected;
    copyProductObject.productAdvertiseTypeId=self.productAdvertiseTypeId;
    copyProductObject.productAdvertiseTypeName=self.productAdvertiseTypeName;
    copyProductObject.productCategoryId=self.productCategoryId;
    copyProductObject.productCompanyNumberOfAds=self.productCompanyNumberOfAds;
    
    return copyProductObject;
}

//-(id)copyWithZ
//{
//        // We'll ignore the zone for now
//        ProductObject *copyProductObject = [[ProductObject all] init];
//        copyProductObject.productCityId = [self.productCityId copyWithZone: zone];
//        copyProductObject.productCityName = [self.productCityName copyWithZone: zone];
//        copyProductObject.productRegionId = [self.productRegionId copyWithZone: zone];
//        copyProductObject.productRegionName = [self.productRegionName copyWithZone: zone];
//        copyProductObject.productSubCategoryId = [self.productSubCategoryId copyWithZone: zone];
//        copyProductObject.productSubCategoryName = [self.productSubCategoryName copyWithZone: zone];
//        copyProductObject.productSubsubCategoryId = [self.productSubsubCategoryId copyWithZone: zone];
//        copyProductObject.productSubsubCategoryName = [self.productSubsubCategoryName copyWithZone: zone];
//        copyProductObject.productManfactureYear = [self.productManfactureYear copyWithZone: zone];
//        copyProductObject.productManfactureYearFrom = [self.productManfactureYearFrom copyWithZone: zone];
//        copyProductObject.productManfactureYearTo = [self.productManfactureYearTo copyWithZone: zone];
//        copyProductObject.productPrice = [self.productPrice copyWithZone: zone];
//        copyProductObject.productPriceFrom = [self.productPriceFrom copyWithZone: zone];
//        copyProductObject.productPriceTo = [self.productPriceTo copyWithZone: zone];
//        copyProductObject.productFurnishedTypeId = [self.productFurnishedTypeId copyWithZone: zone];
//        copyProductObject.productFurnishedTypeName = [self.productFurnishedTypeName copyWithZone: zone];
//        copyProductObject.productKm = [self.productKm copyWithZone: zone];
//        copyProductObject.productKmFrom = [self.productKmFrom copyWithZone: zone];
//        copyProductObject.productKmTo = [self.productKmTo copyWithZone: zone];
//        copyProductObject.productNumberOfRooms = [self.productNumberOfRooms copyWithZone: zone];
//        copyProductObject.productNumberOfRoomsFrom = [self.productNumberOfRoomsFrom copyWithZone: zone];
//        copyProductObject.productNumberOfRoomsTo = [self.productNumberOfRoomsTo copyWithZone: zone];
//        
//        return copyProductObject;
//}




@end
