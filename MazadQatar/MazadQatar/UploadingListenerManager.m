//
//  UploadingListenerBroadcast.m
//  MazadQatar
//
//  Created by samo on 3/31/13.
//  Copyright (c) 2013 Mohammed abdulfattah attia. All rights reserved.
//

#import "UploadingListenerManager.h"

@implementation UploadingListenerManager

@synthesize productUploadingDelegate, uploadingListners;

+ (UploadingListenerManager *)getManager {
  static UploadingListenerManager *uploadingListnerManger;

  if (uploadingListnerManger == nil) {
    uploadingListnerManger = [[UploadingListenerManager alloc] init];

    uploadingListnerManger.productUploadingDelegate = uploadingListnerManger;
  }

  return uploadingListnerManger;
}

- (void)registerUploadingListner:(id<ProductsUploadingDelegate>)delegate {
  if (uploadingListners == nil) {
    uploadingListners = [[NSMutableArray alloc] init];
  }

  [uploadingListners addObject:delegate];
}

- (void)unRegisterUploadingListner:(id<ProductsUploadingDelegate>)delegate {
  [uploadingListners removeObject:delegate];
}

- (void)uploadingStarted {
  for (id<ProductsUploadingDelegate> delegate in uploadingListners) {

    [delegate uploadingStarted];
  }
}

- (void)uploadingProgressOfProduct:(ProductObject *)productObjectToUpload;
{

  for (id<ProductsUploadingDelegate> delegate in uploadingListners) {

    [delegate uploadingProgressOfProduct:productObjectToUpload];
  }
}

- (void)uploadingFinished:(ProductObject *)productObjectToUpload;
{
  for (id<ProductsUploadingDelegate> delegate in uploadingListners) {

    [delegate uploadingFinished:productObjectToUpload];
  }
}


@end
