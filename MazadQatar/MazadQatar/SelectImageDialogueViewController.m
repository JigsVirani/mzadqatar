//
//  SelectImageDialogueViewController.m
//  Mzad Qatar
//
//  Created by Waris Ali on 29/12/2014.
//  Copyright (c) 2014 Mohammed abdulfattah attia. All rights reserved.
//

#import "SelectImageDialogueViewController.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import "SharedData.h"

//#define kPhotosActionSheetTag 1
//#define kVideosActionSheetTag 2
//#define kVideosOrPhotosActionSheetTag 3

@interface SelectImageDialogueViewController ()

@end

@implementation SelectImageDialogueViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.screenName = @"Select Image Dialogue";
    self.takeController = [[FDTakeController alloc] init];
    self.takeController.delegate = self;
    self.takeController.allowsEditingPhoto = YES;
}

- (IBAction)cameraBtnClicked:(id)sender {
    
    [self.view removeFromSuperview];

    [self.takeController openCameraOrGallery:0];
    
    
}

- (IBAction)galleryBtnClicked:(id)sender {
    
    [self.view removeFromSuperview];

    [self.takeController openCameraOrGallery:1];
    
    
}

- (IBAction)createImageClicked:(id)sender {
    
    [self.view removeFromSuperview];

    SelectImageViewController *controller = [[SharedData getCurrentStoryBoard]
                                             instantiateViewControllerWithIdentifier:@"SelectImageViewController"];
    
    controller.delegate = self;
    
    [self.navigationController pushViewController:controller animated:YES];
    
    
}

- (IBAction)closeBtnClicked:(id)sender {
    [self.view removeFromSuperview];
}

- (void)takeController:(FDTakeController *)controller
              gotPhoto:(UIImage *)photo
              withInfo:(NSDictionary *)info {
    [self.delegate SelectImageDelegatePhoto:photo];
}

- (void)SelectImageViewControllerDelegateSelected:(UIImage *)image {
    
    [self.delegate SelectImageDelegatePhoto:image];
}
@end
