//
//  RateViewController.h
//  Mzad Qatar
//
//  Created by Waris Ali on 23/12/2014.
//  Copyright (c) 2014 Mohammed abdulfattah attia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "iRate.h"

#import "GAITrackedViewController.h"
@interface RateViewController : GAITrackedViewController {
  iRate *rateController;
}

- (IBAction)btnClose:(UIButton *)sender;
- (IBAction)rateBtnPressed:(UIButton *)sender;
- (IBAction)laterbtnPressed:(UIButton *)sender;
- (IBAction)noThanksBtnPressed:(UIButton *)sender;

@end
