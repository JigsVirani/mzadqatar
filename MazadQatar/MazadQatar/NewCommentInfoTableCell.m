//
//  CommentInfoTableCell.m
//  MazadQatar
//
//  Created by samo on 4/17/13.
//  Copyright (c) 2013 Mohammed abdulfattah attia. All rights reserved.
//

#import "NewCommentInfoTableCell.h"
#import "DisplayUtility.h"
#import "UserSetting.h"
#import "SharedViewManager.h"
#import "ProductDetailsViewController.h"
@implementation NewCommentInfoTableCell

NSString *NewrequestDeleteComment = @"requestDeleteComment";
NSString *NewrequestCallComment = @"requestCallComment";
NSString *NewrequestReportComment = @"requestReportComment";
NSString *NewrequestBlockUser = @"requestBlockUser";

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
   [super setSelected:selected animated:animated];
  // Configure the view for the selected state
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    if ([DisplayUtility isPad]) {
        self.commentsCountLabel.font = [UIFont systemFontOfSize:22.0];
        self.moreAdBtn.titleLabel.font = [UIFont systemFontOfSize:22.0];
    }
}
- (IBAction)callBtnClicked:(UIButton *)sender {
  [DialogManager
      showConfirmationDialogueWithDescription:
          [NSString
              stringWithFormat:@"%@ %@%@ %@",
                               NSLocalizedString(@"CONFIRMATION_CALL_MSG", nil),
                               productCommentObject.commenterCountryCode,productCommentObject.commenterNumber, NSLocalizedString(@"?",nil)]
                            andConfirmBtnText:NSLocalizedString(
                                                  @"CONFIRMATION_CALL_BTN_TEXT",
                                                  nil)
                                  andDelegate:self
                                 andRequestId:NewrequestCallComment];
}
-(UIView *)roundCornersOnView:(UIView *)view onTopLeft:(BOOL)tl topRight:(BOOL)tr bottomLeft:(BOOL)bl bottomRight:(BOOL)br radius:(float)radius {
    
    if (tl || tr || bl || br) {
        UIRectCorner corner = 0; //holds the corner
        //Determine which corner(s) should be changed
        if (tl) {
            corner = corner | UIRectCornerTopLeft;
        }
        if (tr) {
            corner = corner | UIRectCornerTopRight;
        }
        if (bl) {
            corner = corner | UIRectCornerBottomLeft;
        }
        if (br) {
            corner = corner | UIRectCornerBottomRight;
        }
        
        UIView *roundedView = view;
        UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:roundedView.bounds byRoundingCorners:corner cornerRadii:CGSizeMake(radius, radius)];
        CAShapeLayer *maskLayer = [CAShapeLayer layer];
        maskLayer.frame = roundedView.bounds;
        maskLayer.path = maskPath.CGPath;
        roundedView.layer.mask = maskLayer;
        return roundedView;
    } else {
        return view;
    }
}
- (void)goToCommentClassCallAdvertiseMethod {

  //[SystemUtility callNumber:productCommentObject.commenterNumber];
    [SystemUtility callNumberWithCountryCode:productCommentObject.commenterCountryCode withNumber:productCommentObject.commenterNumber];
}
- (IBAction)copyBtnClicked:(id)sender {
    //NSString * stringUserNumberCopy = [@"00974" stringByAppendingString:productCommentObject.commenterNumber];
    NSString * stringUserNumberCopy = [productCommentObject.commenterCountryCode stringByAppendingString:productCommentObject.commenterNumber];
    [UIPasteboard generalPasteboard].string = stringUserNumberCopy;
   
    [DialogManager toastWithImageAndText: NSLocalizedString(@"Number_Copied",nil)];
}

- (IBAction)smsBtnClicked:(id)sender {
  //[SystemUtility smsNumberInView:currentView andNumber:productCommentObject.commenterNumber andDelegate:smsDelegate];
    [SystemUtility smsNumberInView:currentView andCountryCode:productCommentObject.commenterCountryCode andNumber:productCommentObject.commenterNumber andDelegate:smsDelegate];
}

- (IBAction)deleteBtnclicked:(id)sender {
  [DialogManager
      showConfirmationDialogueWithDescription:
          [NSString
              stringWithFormat:@"%@ %@%@ %@",
                               NSLocalizedString(
                                   @"CONFIRMATION_DELETE_COMMENT_MSG", nil),
                               productCommentObject.commenterCountryCode,productCommentObject.commenterNumber, NSLocalizedString(@"?",nil)]
                            andConfirmBtnText:
                                NSLocalizedString(
                                    @"CONFIRMATION_DELETE_BTN_TEXT", nil)
                                  andDelegate:self
                                 andRequestId:NewrequestDeleteComment];
}

- (void)goToDeleteAdvt {
  [ServerManager deleteComment:productCommentObject
                   AndDelegate:self
                 withRequestId:NewrequestDeleteComment
                 withProductId:productId];
}

- (IBAction)reportBtnClicked:(id)sender {
    self.reportBtn.hidden = YES;
    self.moreCommentIndicator.hidden = NO;
    [self.moreCommentIndicator startAnimating];
    [self showReportDialogue];
}

- (IBAction)blockBtnClicked:(id)sender {
    if ([UserSetting isUserRegistered] == false) {
        [SharedViewManager showLoginScreen:currentView];
    } else {
        [DialogManager
         showConfirmationDialogueWithDescription:
         [NSString
          stringWithFormat:@"%@ %@",
          NSLocalizedString(@"CONFIRMATION_BLOCK_MSG", nil),
          NSLocalizedString(@"?",nil)]
         andConfirmBtnText:NSLocalizedString(
                                             @"OK",
                                             nil)
         andDelegate:self
         andRequestId:NewrequestBlockUser];

        self.blockBtn.hidden = YES;
        self.blockIndicator.hidden = NO;
        [self.blockIndicator startAnimating];
       // [ServerManager blockUserFromComment:productCommentObject andProductObj:productObject AndDelegate:self withRequestId:NewrequestBlockUser];
    }
}

- (IBAction)chatBtnClicked:(id)sender {
    if ([UserSetting isUserRegistered] == false) {
        [SharedViewManager showLoginScreen:currentView];
    } else {
        
        [currentView createChatFromAdOwnerSideWithNumber:productCommentObject.commenterNumber commenterName:productCommentObject.commentUserName commenterPhoto:productCommentObject.userImageUrl];
    }
}

-(void)setDelegate:(id<NewCommentInfoTableCellDelegate>)commentInfoDelegateParam
{
    commentInfoDelegate = commentInfoDelegateParam;
}

- (void)setComment:(ProductCommentObject *)productCommentObjectParam
productObj:(ProductObject *)productObjectfromCell
       andAdvertiseNumber:(NSString *)advertiseNumberParam
                   inView:(ProductDetailsViewController *)currentViewParam
              andDelegate:
                  (id<MFMessageComposeViewControllerDelegate>)smsDelegateParam
    andCommentInfoDelegae:
        (id<NewCommentInfoTableCellDelegate>)commentInfoDelegateParam
              inProductId:(NSString *)productIdParam
       inTableView:(UITableView *)tableViewParam isAdOwnerOrCommentOwner:(BOOL)isAdOwnerOrCommentOwner {
    
    [self setUserInteractionEnabled:false];
    [self.loadingAvtarIndicator startAnimating];
    self.deleteActivityIndicator.hidden = YES;
    self.blockIndicator.hidden = YES;
    self.moreCommentIndicator.hidden = YES;
    [self.avtarImage layoutIfNeeded];
	self.avtarImage.layer.cornerRadius = self.avtarImage.bounds.size.height/2.0;
    self.avtarImage.clipsToBounds = YES;
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0ul);
    dispatch_async(queue, ^{
        // Load from file or Bundle as you want
        
        [self.avtarImage
         sd_setImageWithURL:[NSURL URLWithString:productCommentObjectParam.userImageUrl]
         placeholderImage:nil
         completed:^(UIImage *image, NSError *error,
                     SDImageCacheType cacheType, NSURL *imageURL) {
             
             [self setUserInteractionEnabled:true];
             
             dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{ // 1
                 dispatch_async(dispatch_get_main_queue(), ^{ // 2
                     [self createRoundedImage]; // 3
                 });
             });
         }];
    });
    
    productCommentObject = productCommentObjectParam;
    productObject = productObjectfromCell;
    currentView = currentViewParam;
    
    smsDelegate = smsDelegateParam;
    
    commentInfoDelegate = commentInfoDelegateParam;
    
    advertiseNumber = advertiseNumberParam;
    
    productId = productIdParam;
    self.nameTextEdit.text = productCommentObject.commentUserName;
    self.commentText.text = nil;
    self.commentText.text = productCommentObject.commentDescription;
    if([LanguageManager currentLanguageIndex] == 1){
        self.commentText.textAlignment = NSTextAlignmentRight;
    }else{
        self.commentText.textAlignment = NSTextAlignmentLeft;
    }
    self.dateText.text = productCommentObject.commentDate;
    isCommentOwner = isAdOwnerOrCommentOwner;
    
    //self.contactNumberLabel.text = [NSString stringWithFormat:@"+974%@",productCommentObject.commenterNumber];
    self.contactNumberLabel.text = [NSString stringWithFormat:@"%@%@",productCommentObject.commenterCountryCode,productCommentObject.commenterNumber];
    self.lblContactNumber.text = [NSString stringWithFormat:@"%@%@",productCommentObject.commenterCountryCode,productCommentObject.commenterNumber];
    
    NSString* string = NSLocalizedString(@"Comments_Lable_Text",nil);
    
    NSString* string2 =[NSString stringWithFormat:@"(%@)",productObject.productCommentCount];
    self.commentsCountLabel.text =[string stringByAppendingString:string2];
    
    if ([productCommentObjectParam.commenterNumber
         isEqualToString:advertiseNumberParam]) {
        self.adOwnerLabel.text =NSLocalizedString(@"Ad_Owner_Lable_Text",nil);
        self.addOwnerHeight.constant = 20;
    } else {
        self.adOwnerLabel.text = @"";
        self.addOwnerHeight.constant = 0;
    }
    self.lblCommentActionResult.hidden = YES;
    
    // check if comment equal to advertise number or user number , so u can show
    // delete btn
    //  if ([[UserSetting getUserNumber] isEqualToString:advertiseNumberParam] ||
    //      [[UserSetting getUserNumber]
    //          isEqualToString:productCommentObject.commenterNumber]) {
    //    [self.deleteImageView setHidden:NO];
    //  } else {
    //    [self.deleteImageView setHidden:YES];
    //  }
    
    if (productCommentObject.isReportingAdvertise) {
        [self.moreCommentIndicator startAnimating];
        
        [ServerManager reportComment:productCommentObject
                         AndDelegate:self
                       withRequestId:NewrequestReportComment];
    }
}

- (void)createRoundedImage {
    
	
    
    [self.loadingAvtarIndicator stopAnimating];
    
}


- (void)setIsLoadingCommentView:(BOOL)isLoading {
  if (isLoading) {
      self.loadingCommentsIndicator.hidden = NO;
    [self.loadingCommentsIndicator startAnimating];
  } else {
      self.loadingCommentsIndicator.hidden = YES;
    [self.loadingCommentsIndicator stopAnimating];
  }
}

- (void)setIsNoCommentLblHidden:(BOOL)isHidden {
//  [self.noCommentLbl setHidden:isHidden];
  [self.nameTextEdit setHidden:!isHidden];
  [self.commentText setHidden:!isHidden];
//  [self.callBtn setHidden:!isHidden];
//  [self.callIcon setHidden:!isHidden];
//  [self.smsBtn setHidden:!isHidden];
//  [self.smsIcon setHidden:!isHidden];
  [self.dateText setHidden:!isHidden];
  [self.reportBtn setHidden:!isHidden];
    [self.deleteBtn setHidden:!isHidden];
    [self.blockBtn setHidden:!isHidden];
//  [self.reportImageView setHidden:!isHidden];
}

- (void)requestFinished:(ServerManagerResult *)serverManagerResult
            ofRequestId:(NSString *)requestId
    withNumberOfRequest:(int)numberOfRequest {
  // stop animation
  if (requestId == NewrequestReportComment) {
    [productCommentObject setIsReportingAdvertise:false];
      self.reportBtn.hidden = NO;
    [self.moreCommentIndicator stopAnimating];
      self.moreCommentIndicator.hidden = YES;
  } else if (requestId == NewrequestDeleteComment) {
    [self.deleteActivityIndicator stopAnimating];
      self.deleteBtn.hidden = NO;
      self.deleteActivityIndicator.hidden = YES;
  }else if (requestId == NewrequestBlockUser) {
      [self.blockIndicator stopAnimating];
      self.blockBtn.hidden = NO;
      self.blockIndicator.hidden = YES;
      
      [DialogManager showDialogWithMessage:serverManagerResult.opertationMsg];
      return;
  }
    
  if ([serverManagerResult.opertationResult
          isEqualToString:[ServerManager ERROR_IN_CONNECTION_OPERATION]])
  {
    [DialogManager showErrorInConnection];
    return;
  }
    
  // show status text field
  self.lblCommentActionResult.hidden = NO;
  // set status text
  if ([serverManagerResult.opertationResult
          isEqualToString:[ServerManager SUCCESS_OPERATION]]) {
    self.lblCommentActionResult.text = serverManagerResult.opertationMsg;
  } else if ([serverManagerResult.opertationResult
                 isEqualToString:[ServerManager FAILED_OPERATION]]) {
    self.lblCommentActionResult.text = NSLocalizedString(@"ERROR_ADD", nil);
  } else if ([serverManagerResult.opertationResult
                 isEqualToString:[ServerManager ALREADED_ADDED_OPERATION]]) {
    self.lblCommentActionResult.text = NSLocalizedString(@"ALREADY_ADD", nil);
  }

  if (requestId == NewrequestDeleteComment) {
    [commentInfoDelegate deleteCommentCLicked:self.deleteActivityIndicator
                           andResultTextField:self.lblCommentActionResult];
  }
}

- (void)showReportDialogue {
  ReportItemViewController *controller = [[SharedData getCurrentStoryBoard]
      instantiateViewControllerWithIdentifier:@"ReportCommentViewController"];
  controller.delegate = self;
  [currentView.navigationController pushViewController:controller animated:YES];
}

- (void)reportAdWithReasonWithReasonID:(NSString *)reasonID
                         andReasonText:(NSString *)reasonText
{
  //[self.reportIndicator startAnimating];
  [productCommentObject setIsReportingAdvertise:YES];
  [productCommentObject setReportReasonId:reasonID];
  [productCommentObject setReportReasonText:reasonText];

  [tableView reloadData];
}

#pragma confirmation dialog buttons clicked events
- (void)confirmBtnClicked:(NSString *)requestId {
    if (requestId == NewrequestCallComment) {
        //[SystemUtility callNumber:productCommentObject.commenterNumber];
        [SystemUtility callNumberWithCountryCode:productCommentObject.commenterCountryCode withNumber:productCommentObject.commenterNumber];
    } else if (requestId == NewrequestDeleteComment) {
        [self.deleteActivityIndicator startAnimating];
        
        [ServerManager deleteComment:productCommentObject
                         AndDelegate:self
                       withRequestId:NewrequestDeleteComment
                       withProductId:productId];
    }
    else
    {
        [ServerManager blockUserFromComment:productCommentObject andProductObj:productObject AndDelegate:self withRequestId:NewrequestBlockUser];
    }
}

- (IBAction)goToProfile:(id)sender
{
    UserProducts *controller = [[SharedData getCurrentStoryBoard]instantiateViewControllerWithIdentifier:@"UserProducts"];
    
    ProductObject *commentUserObject=[[ProductObject alloc] init];
    commentUserObject.productCountryCode=productCommentObject.commenterCountryCode;
    commentUserObject.productUserNumber=productCommentObject.commenterNumber;
    
    [controller setUserProfileProductObject:commentUserObject];
    
    [controller showBackButton:YES];
    [controller setHideTabBar:YES];
    [currentView.navigationController pushViewController:controller animated:YES];
}

- (IBAction)moreBtnClikced:(UIButton *)sender {
    sender.hidden = YES;
    [commentInfoDelegate moreClickedFinishedWithLoadingIndicator:self.moreCommentIndicator
                                   andResultTextField:self.lblCommentActionResult andButton:sender];
}

-(IBAction)showOption:(UIButton *)sender
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Select_Option",nil) message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *cancelAction = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(@"Cancel", nil)
                                   style:UIAlertActionStyleDestructive
                                   handler:^(UIAlertAction *action)
                                   {
                                   }];
    alertController.view.backgroundColor = [UIColor whiteColor];

    UIAlertAction *callAction = [UIAlertAction
                               actionWithTitle:NSLocalizedString(@"Call", nil)
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action)
                               {
                                   [self callBtnClicked:sender];
                               }];
    
    UIAlertAction *copyAction = [UIAlertAction
                                 actionWithTitle:NSLocalizedString(@"Copy", nil)
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction *action)
                                 {
                                     [self copyBtnClicked:sender];
                                 }];
    
    UIAlertAction *messageAction = [UIAlertAction
                                  actionWithTitle:NSLocalizedString(@"Message", nil)
                                  style:UIAlertActionStyleDefault
                                  handler:^(UIAlertAction *action)
                                  {
                                      [self smsBtnClicked:sender];
                                  }];
    UIAlertAction *deleteAllAction = [UIAlertAction
                                      actionWithTitle:NSLocalizedString(@"Delete", nil)
                                      style:UIAlertActionStyleDefault
                                      handler:^(UIAlertAction *action)
                                      {
                                          [self deleteBtnclicked:sender];
                                      }];
    UIAlertAction *reportAction = [UIAlertAction
                                    actionWithTitle:NSLocalizedString(@"Report", nil)
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction *action)
                                    {
                                        [self reportBtnClicked:sender];
                                    }];
    UIAlertAction *blockAction = [UIAlertAction
                                    actionWithTitle:NSLocalizedString(@"Block", nil)
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction *action)
                                    {
                                        [self blockBtnClicked:sender];
                                    }];
    UIAlertAction *chatAction = [UIAlertAction
                                  actionWithTitle:NSLocalizedString(@"Chat", nil)
                                  style:UIAlertActionStyleDefault
                                  handler:^(UIAlertAction *action)
                                  {
                                      [self chatBtnClicked:sender];
                                  }];
    
    [alertController addAction:callAction];
    [alertController addAction:copyAction];
    [alertController addAction:messageAction];
    NSLog(@"productNumber:%@\n userNumber:%@",productObject.productUserNumber,[UserSetting getUserNumber]);
    if(isCommentOwner)
    {
        [alertController addAction:deleteAllAction];
        
    }else if ([productObject.productUserNumber isEqualToString:[UserSetting getUserNumber]])
    {
        [alertController addAction:reportAction];
        [alertController addAction:blockAction];
        [alertController addAction:deleteAllAction];
        [alertController addAction:chatAction];
    }
    else //if([productCommentObject.commenterNumber isEqualToString:[UserSetting getUserNumber]])
    {
        [alertController addAction:reportAction];
        [alertController addAction:blockAction];
    }
    [alertController addAction:cancelAction];
    // Remove arrow from action sheet.
    [alertController.popoverPresentationController setPermittedArrowDirections:UIPopoverArrowDirectionAny];
    //For set action sheet to middle of view.
    CGRect rect = currentView.view.frame;
    CGRect buttonFrame = [sender convertRect:sender.bounds toView:currentView.view];
    //    NSIndexPath *indexPath = [self.notificationTableView indexPathForRowAtPoint:buttonFrame.origin];
    rect.origin.x = buttonFrame.origin.x;
    rect.origin.y = buttonFrame.origin.y;
    alertController.popoverPresentationController.sourceView = currentView.view;
    alertController.popoverPresentationController.sourceRect = buttonFrame;
    
    [currentView presentViewController:alertController animated:YES completion:nil];
    alertController.view.tintColor = [UIColor blackColor];
}

@end
