//
//  UserFavoriteViewController.h
//  MazadQatar
//
//  Created by samo on 4/28/13.
//  Copyright (c) 2013 Mohammed abdulfattah attia. All rights reserved.
//

#import "GridView.h"
#import "ServerManager.h"

@interface UserFavorite : GridView <ServerManagerResponseDelegate>

- (void)startLoadingUserFavoriteWithRequestId:(NSString *)requestId
                              andIsNewSession:(bool)isNewSession;
- (void)refreshCurrentGrid;

@end
