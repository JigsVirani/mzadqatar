//
//  YearChooserViewController.h
//  Mzad Qatar
//
//  Created by AHMED HEGAB on 7/12/16.
//  Copyright © 2016 Mohammed abdulfattah attia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GoogleAnalyticsTableViewController.h"
@class YearChooserViewController;

@protocol YearChooserDelegate <NSObject>

@required
- (void) yearChoosed:(NSString *) yearChooser;

@required

@end

@interface YearChooserViewController : GoogleAnalyticsTableViewController {

    id <YearChooserDelegate> delegate;
    NSString *selectedYear;
}


- (void)setYearDelegate:
(id<YearChooserDelegate>)delegateParam;

@property(strong,nonatomic) IBOutlet UITableView *yearTableView;
@property(strong,nonatomic) IBOutlet UITextField *txtSearch;

@property(strong,nonatomic)  NSMutableArray *yearArrayList;
@property(strong,nonatomic)  NSMutableArray *TempArrYear;

@end
