//
//  RegionListChooser.h
//  Mzad Qatar
//
//  Created by samo on 9/2/14.
//  Copyright (c) 2014 Mohammed abdulfattah attia. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ChooseListWithSearch.h"
#import "SharedData.h"
#import "CitiesObject.h"
#import "RegionsObject.h"

// Delgate
@class RegionListChooser;
@protocol RegionViewControllerDelegate <NSObject>
@required
- (void)regionChoosedInList:(NSString *)regionId
              andRegionName:(NSString *)regionName;
@required
@end

@interface RegionListChooser
    : ChooseListWithSearch <UITableViewDataSource, UITableViewDelegate> {
  NSMutableArray *citiesAndRegionArray;
  NSString *currentCitySelectedID;
  NSString *currentRegionSelectedID;
  NSString *isSHowAllText;
  id<RegionViewControllerDelegate> delegate;
  bool isOpenedFromAddAdvertise;
}

- (void)setRegionParamsWithRegionId:(NSString *)regionIdParam
                  andSelectedCityId:(NSString *)selectedCityIdParam
        andIsOpenedFromAddAdvertise:(BOOL)isOpenedFromAddAdvertiseParam      andDelegate:
                            (id<RegionViewControllerDelegate>)delegateParam;

- (void)startloadingData;

@end
