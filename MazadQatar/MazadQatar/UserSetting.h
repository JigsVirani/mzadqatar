//
//  UserSetting.h
//  MazadQatar
//
//  Created by samo on 3/19/13.
//  Copyright (c) 2013 Mohammed abdulfattah attia. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "ALChatViewController.h"
#import "ALChatManager.h"
#import "ALUserDefaultsHandler.h"
#import "ALRegisterUserClientService.h"
#import "ALDBHandler.h"
#import "ALContact.h"
#import "ALDataNetworkConnection.h"
#import "ALMessageService.h"
#import "ALContactService.h"
#import "ALUserService.h"
#import "ALImagePickerHandler.h"

@interface UserSetting : NSObject

+ (void)loadUserSetting;
+ (void)saveUserSettingWithName:(NSString *)name
                withCountryCode:(NSString *)countryCode
                     withNumber:(NSString *)number
                      withEmail:(NSString *)email;
+ (void)saveUserPhoto:(NSString *)strImageName;
+ (void)saveUserName:(NSString *)userNameParam;
+ (void)saveUserToken:(NSString *)userTokenParam;
+ (void)saveUserPushNotificationLanguage:(NSString *)userLanguageParam;
+ (void)saveAllCategoriesFileLanguage:(NSString *)userFiltersLanguageParam;
+ (void)saveSortByFileLanguage:(NSString *)soryByLangParam;
+ (void)saveCitiesAndRegionFileLanguage:
        (NSString *)citiesAndRegionLanguageParam;
+ (void)saveUserAddAdvertiseLanguage:(int)userLanguageParam;
+ (void)saveFilterViewAdvertiseLanguage:(int)userLanguageParamIndex;

+ (NSString *)getUserName;
+ (NSString *)getUserCountryCode;
+ (NSString *)getUserNumber;
+ (NSString *)getUserEmail;
+ (NSString *)getUserPhoto;
+ (NSString *)getuserToken;
+ (NSString *)getUserLanguage;
+ (NSString *)getAllCategoriesFileLanguage;
+ (NSString *)getSortByFileLanguage;
+ (NSString *)getCitiesAndRegionFileLanguage;
+ (int)getUserAddAdvertiseLanguage;
+ (int)EnglishAndArabicLanguageKey;
+ (NSString *)getUserAddAdvertiseLanguageName:(int)index;
+ (int)getUserFilterViewAdvertiseLanguage;
+ (NSString *)getUserFilterViewAdvertiseLanguageBtnParam:(int)index;
+ (NSString *)getAdvertiseTypeServerParam:(int)index;
+ (int)getAdvertiseTypeServerIndex:(NSString *)serverLangParam;
+ (NSString *)getAdvertiseLanguageServerParam:(int)index;

+ (void)setIsShowPressEditAdvertiseAlert:
        (BOOL)isShowPressEditAdvertiseAlertParam;
+ (bool)isShowPressEditAdvertise;

+ (int)ENGLISH_ARABIC_CONSTANT;
+ (int)ENGLISH_CONSTANT;
+ (int)ARABIC_CONSTANT;

+ (NSString *)ENGLISH_ARABIC_CONSTANT_STRING;
+ (NSString *)ENGLISH_CONSTANT_STRING;
+ (NSString *)ARABIC_CONSTANT_STRING;

+ (void)setIsUserRegistered:(BOOL)isRegistered;
+ (bool)isUserRegistered;

// save the last update time for categories
+ (NSString *)getLastUpdateTimeForCategoriesFromLocal;
+ (void)saveLastUpdateTimeForCategoriesFromLocal:(NSString *)lastUpdateTime;
+ (NSString *)getLastUpdateTimeForCategoriesFromServer;

// save the last update time for categories
+ (NSString *)getLastUpdateTimeForSortByFromLocal;
+ (void)saveLastUpdateTimeForSortByFromLocal:(NSString *)lastUpdateTime;
+ (NSString *)getLastUpdateTimeForSortByFromServer;

// save the last update time for categories
+ (NSString *)getLastUpdateTimeForCitiesFromLocal;
+ (void)saveLastUpdateTimeForCitiesFromLocal:(NSString *)lastUpdateTime;
+ (NSString *)getLastUpdateTimeForCitiesFromServer;

+ (void)saveAllLastUpdateTimeFromServer:(NSString *)lastUpdateTimeForCategories
                    andSoryByUpdateTime:(NSString *)lastUpdateTimeForSortBy
                    andCitiesUpdateTime:(NSString *)lastUpdateTimeForCities;

// For Share and Rate Dialogue.
+ (BOOL)shouldShowRateDialogue;
+ (BOOL)shouldShowShareAppDialogue;

+ (NSString *)getUserCurrentLanguage;

+(void)registerUserToApplozic;

+(ALUser*)generateUserForApplozic;

@end
