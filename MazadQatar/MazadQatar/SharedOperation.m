//
//  CheckTokenOrLanguageChange.m
//  Mzad Qatar
//
//  Created by samo on 10/1/13.
//  Copyright (c) 2013 Mohammed abdulfattah attia. All rights reserved.
//

#import "SharedOperation.h"
//#import "TestFlight.h"

@implementation SharedOperation

NSString *requestAddDeviceToken = @"requestAddDeviceToken";
NSString *currentToken;
NSString *currentLanguage;

+ (SharedOperation *)sharedInstance {
  static SharedOperation *sharedOperation = nil;
  if (sharedOperation == nil) {
    sharedOperation = [[SharedOperation alloc] init];
  }
  return sharedOperation;
}

- (void)checkTokenOrLanguageChange:(NSString *)userToken {
  bool isLanguageChanged = false;
  bool isTokenChanged = false;

    //NSString *language = [[[NSBundle mainBundle] preferredLocalizations] objectAtIndex:0];
    NSString *language = [LanguageManager currentLanguageCode];
    
  if (([language isEqualToString:[UserSetting getUserLanguage]]) == false) {
    //        if([language isEqualToString:[ServerManager LANGUAGE_ARABIC]])
    //        {
    //            [UserSetting saveUserLanguage:[ServerManager
    //            LANGUAGE_ARABIC]];
    //        }
    //        else
    //        {
    //            [UserSetting saveUserLanguage:[ServerManager
    //            LANGUAGE_ENGLISH]];
    //        }
    currentLanguage = language;

    isLanguageChanged = true;
  }

  if (([userToken isEqualToString:[UserSetting getuserToken]]) == false) {
    currentToken = userToken;
    isTokenChanged = true;
  }

  if (isLanguageChanged == true || isTokenChanged == true) {
    [ServerManager addDeviceToken:userToken
                     withLangauge:language
              withUserCountryCode:[UserSetting getUserCountryCode]
                   withUserNumber:[UserSetting getUserNumber]
                      AndDelegate:self
                    withRequestId:requestAddDeviceToken];
  }
}

- (void)requestFinished:(ServerManagerResult *)serverManagerResult
            ofRequestId:(NSString *)requestId
    withNumberOfRequest:(int)numberOfRequest {
  if (requestId == requestAddDeviceToken) {
    if ([serverManagerResult.opertationResult
            isEqualToString:[ServerManager SUCCESS_OPERATION]]) {
     
        [UserSetting saveUserToken:currentToken];

      if ([currentLanguage isEqualToString:[ServerManager LANGUAGE_ARABIC]]) {
        [UserSetting
            saveUserPushNotificationLanguage:[ServerManager LANGUAGE_ARABIC]];
      } else {
        [UserSetting
            saveUserPushNotificationLanguage:[ServerManager LANGUAGE_ENGLISH]];
      }
    }
  }
}
@end
