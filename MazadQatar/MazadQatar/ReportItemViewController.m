//
//  Advertise DialogueViewController.m
//  Mzad Qatar
//
//  Created by Waris Ali on 25/12/2014.
//  Copyright (c) 2014 Mohammed abdulfattah attia. All rights reserved.
//

#import "ReportItemViewController.h"
#import "OtherReasonTableViewCell.h"
#import "DialogManager.h"

@interface ReportItemViewController ()

@end

NSString *reasonOneIdentifier = @"reasonOneCell";
NSString *reasonTwoIdentifier = @"reasonTwoCell";
NSString *reasonThreeIdentifier = @"reasonThreeCell";
NSString *reasonFourIdentifier = @"reasonFourCell";
NSString *reasonFiveIdentifier = @"reasonFiveCell";
NSString *reasonSixIdentifier = @"reasonSixCell";

NSString *reasonOneId = @"1";
NSString *reasonTwoId = @"2";
NSString *reasonThreeId = @"3";
NSString *reasonFourId = @"4";
NSString *reasonFiveId = @"5";

@implementation ReportItemViewController

- (void)viewDidLoad {
    
    self->screenName = @"Report Item Screen";
  [super viewDidLoad];
    

    currentSelectedIndex = 5;
    
  self.tableOfChoices.tableFooterView =
      [[UIView alloc] initWithFrame:CGRectZero];
}

- (IBAction)textFieldFinished:(id)sender {
  // [sender resignFirstResponder];
}

- (void)didReceiveMemoryWarning {
  [super didReceiveMemoryWarning];
  // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView
    numberOfRowsInSection:(NSInteger)section {
  return 6;
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
  UITableViewCell *cell = nil;

  // comments of product
  if (indexPath.row == ([tableView numberOfRowsInSection:0] - 1))
  {
    OtherReasonTableViewCell *cellOtherReason = (OtherReasonTableViewCell *)
        [tableView dequeueReusableCellWithIdentifier:reasonSixIdentifier];

    if (currentSelectedIndex == 5) {
      cell.accessoryType = UITableViewCellAccessoryCheckmark;
      cellOtherReason.submitOtherReasonButton.hidden = NO;

    } else
    {
      cell.accessoryType = UITableViewCellAccessoryNone;
      cellOtherReason.submitOtherReasonButton.hidden = YES;
    }

    [cellOtherReason setDelegate:self];

    [cellOtherReason setBackgroundColor:[UIColor clearColor]];
    return cellOtherReason;

  }
  else if (indexPath.row == 0) {
    cell = [tableView dequeueReusableCellWithIdentifier:reasonOneIdentifier
                                           forIndexPath:indexPath];
  } else if (indexPath.row == 1) {
    cell = [tableView dequeueReusableCellWithIdentifier:reasonTwoIdentifier
                                           forIndexPath:indexPath];
  } else if (indexPath.row == 2) {
    cell = [tableView dequeueReusableCellWithIdentifier:reasonThreeIdentifier
                                           forIndexPath:indexPath];
  } else if (indexPath.row == 3) {
    cell = [tableView dequeueReusableCellWithIdentifier:reasonFourIdentifier
                                           forIndexPath:indexPath];
  } else if (indexPath.row == 4) {
    cell = [tableView dequeueReusableCellWithIdentifier:reasonFiveIdentifier
                                           forIndexPath:indexPath];
  }

  if (currentSelectedIndex == indexPath.row) {
    cell.accessoryType = UITableViewCellAccessoryCheckmark;
  } else {
    cell.accessoryType = UITableViewCellAccessoryNone;
  }

  [cell setBackgroundColor:[UIColor clearColor]];

  return cell;
}

- (CGFloat)tableView:(UITableView *)tableView
    heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([DisplayUtility isPad] && indexPath.row == 5) {
        return 240;
    }else if (indexPath.row == 5) {
        return 140;
    }
  if ([DisplayUtility isPad]) {
    return 86;
  }
  return 46;
}

- (void)tableView:(UITableView *)tableView
    didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

  currentSelectedIndex = (int)indexPath.row;

  if (indexPath.row == ([tableView numberOfRowsInSection:0] - 1)) {
    // UITableViewCell *cell =[tableView cellForRowAtIndexPath:indexPath];

    [self.tableView reloadData];
    return;
  } else if (indexPath.row == 0) {
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    reasonID = reasonOneId;
    reasonText = cell.textLabel.text;

  } else if (indexPath.row == 1) {
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    reasonID = reasonTwoId;
    reasonText = cell.textLabel.text;
  } else if (indexPath.row == 2) {
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    reasonID = reasonThreeId;
    reasonText = cell.textLabel.text;
  } else if (indexPath.row == 3) {
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    reasonID = reasonFourId;
    reasonText = cell.textLabel.text;
  } else if (indexPath.row == 4) {
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    reasonID = reasonFiveId;
    reasonText = cell.textLabel.text;
  }

        
  [self.navigationController popViewControllerAnimated:YES];

  [self.delegate reportAdWithReasonWithReasonID:reasonID
                                  andReasonText:reasonText];
    
}

- (void)submitOtherReasonClicked:(NSString *)reasonIdParam
                   andReasonText:(NSString *)reasonTextParam;
{
    
  [self.navigationController popViewControllerAnimated:YES];

  [self.delegate reportAdWithReasonWithReasonID:reasonIdParam
                                  andReasonText:reasonTextParam];
}

@end
