//
//  CheckTokenOrLanguageChange.h
//  Mzad Qatar
//
//  Created by samo on 10/1/13.
//  Copyright (c) 2013 Mohammed abdulfattah attia. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UserSetting.h"
#import "ServerManager.h"

@interface SharedOperation : NSObject <ServerManagerResponseDelegate>

+ (SharedOperation *)sharedInstance;
- (void)checkTokenOrLanguageChange:(NSString *)userToken;

@end
