//
//  ServerManager.h
//  MazadQatar
//
//  Created by samo on 3/4/13.
//  Copyright (c) 2013 Mohammed abdulfattah attia. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ServerManagerResult.h"
#import "ProductObject.h"
#import "MkNetworkEngine.h"
#import "SharedData.h"
#import "ProductCommentObject.h"
#import "DisplayUtility.h"
#import "UserSetting.h"
#import "StringUtility.h"
#import "DialogManager.h"
#import "NotificationModal.h"
#import "SortByObject.h"
#import "CitiesObject.h"
#import "RegionsObject.h"
#import "CategoryObject.h"
#import "AdvertiseType.h"
#import "SelectImageModal.h"
#import "SharedData.h"

@protocol ServerManagerResponseDelegate <NSObject>
@required
- (void)requestFinished:(ServerManagerResult *)serverManagerResult
            ofRequestId:(NSString *)requestId
    withNumberOfRequest:(int)numberOfRequest;
@required
@end

// Delgate
@class ServerManager;

@interface ServerManager : NSObject {
}
//=========================
+ (void)getAllCompanyCategories:(NSString *)language
                   withDelegate:(id<ServerManagerResponseDelegate>)delegate
                  withRequestId:(NSString *)requestId;
//=========================
+ (void)setHostName:(BOOL)isFromImage;

//=============================================get push notifications
+ (void)getPushNotification:(NSString *)pushNotificationUrl;
//============================================================get category items
+ (void)getAllCategories:(NSString *)language
            withDelegate:(id<ServerManagerResponseDelegate>)delegate
           withRequestId:(NSString *)requestId;
+ (ServerManagerResult *)parseAllCategories:(NSMutableDictionary *)json
                    withServerManagerResult:
                        (ServerManagerResult *)serverManagerResult;
//=========================================================get AllBanners to write
+ (void)getAllBannersForCategories:(NSString *)language
                      withDelegate:(id<ServerManagerResponseDelegate>)delegate
                     withRequestId:(NSString *)requestId;
//=========================================================get Images to write
//text on it
+ (void)getImagesToWriteTextOnIt:(NSString *)language
                   andResolution:resolution
                    withDelegate:(id<ServerManagerResponseDelegate>)delegate
                  withCategoryId:(NSString *)categoryId
                   withRequestId:(NSString *)requestId
    withIsOpenedFromAddAdvertise:(NSString *)isOpenedFromAddAdvertise;

//============================================================get category items
+ (void)getCategoryItemsInLanguage:(NSString *)language
                     andResolution:resolution
                      withDelegate:(id<ServerManagerResponseDelegate>)delegate
                    withCategoryId:(NSString *)categoryId
                     withRequestId:(NSString *)requestId
                       countOfRows:(NSString *)countOfRows
      withIsOpenedFromAddAdvertise:(NSString *)isOpenedFromAddAdvertise;

//============================================================ Registeration
+ (void)registeUserWithName:(NSString *)userName
             AndCountryCode:(NSString *)userCountryCode
                  AndNumber:(NSString *)userNumber
                   AndEmail:(NSString *)userEmail
               andUserToken:(NSString *)userToken
                AndDelegate:(id<ServerManagerResponseDelegate>)delegate
              withRequestId:(NSString *)requestId;

//============================================================Activation
+ (void)checkActivationCode:(NSString *)activationCode
             AndCountryCode:(NSString *)userCountryCode
                  AndNumber:(NSString *)userNumber
                AndDelegate:(id<ServerManagerResponseDelegate>)delegate
              withRequestId:(NSString *)requestId;

//============================================================mk network engine
+ (void)sendJsonToUrlUsingMk:(NSString *)url
                    withJson:(NSMutableDictionary *)json
                 AndDelegate:(id<ServerManagerResponseDelegate>)delegate
               withRequestId:(NSString *)requestId
      withJsonParserSelector:(SEL)jsonParserSelector
         withNumberOfRequest:(int)requestNumber;

+ (void)initMkNetworkEngine;

//============================================================get category
//products

+ (NSMutableDictionary *)
         encodeGetCategoryProductJson:(NSString *)categoryId
                     withTypeOfUpdate:(NSString *)typeOfUpdate
        withCategoryProductPageNumber:(int)categoryProductPageNumber
    withCategoryProductLastUpdateTime:(NSString *)categoryProductLastUpdateTime
     withCategoryProductNumberOfPages:(int)categoryProductNumberOfPages
                        withSearchStr:(NSString *)searchStr
                withAdvertiseTypeLang:(NSString *)advertiseLanguageType
                    withSubCategoryId:(NSString *)subCategoryId
                         withSortById:(NSString *)sortById
                withAdvertiseLanguage:(NSString *)advertiseLang
                        isAdSupported:(NSString *)isAdSupported
                          countOfRows:(NSString *)countOfRows
              withAdvertiseResolution:(NSString *)advertiseResolution
          withCategoryAdvertiseTypeId:(NSString *)categoryAdvertiseTypeId
                           withCityId:(NSString *)cityId
                         withRegionId:(NSString *)regionId
                 withSubSubCategoryId:(NSString *)subsubCategoryId
               withManfactureYearFrom:(NSString *)manfactureYearFrom
                     withManfactureTo:(NSString *)manfactureYearTo
                           withKmFrom:(NSString *)kmFrom
                             withKmTo:(NSString *)kmTo
                 withNumberOfRoomFrom:(NSString *)numberOfRoomfrom
                  withNumberOfRoomsTo:(NSString *)numberOfRoomsTo
                        withPriceFrom:(NSString *)priceFrom
                          withPriceTo:(NSString *)priceTo
withFurnishedTypeId:(NSString*)furnishedTypeId;

+ (void)getCategoryProductsItems:(NSString *)categoryId
                         withDelegate:
                             (id<ServerManagerResponseDelegate>)delegate
                       withNewSession:(bool)isNewSession
                     withTypeOfUpdate:(NSString *)typeOfUpdate
        withCategoryProductPageNumber:(int)categoryProductPageNumber
    withCategoryProductLastUpdateTime:(NSString *)categoryProductLastUpdateTime
     withCategoryProductNumberOfPages:(int)categoryProductNumberOfPages
                        withSearchStr:(NSString *)searchStr
                        withRequestId:(NSString *)requestId
                withAdvertiseLangType:(NSString *)advertiseLangType
                    withSubCategoryId:(NSString *)subCategoryId
                         withSortById:(NSString *)sortById
                withAdvertiseLanguage:(NSString *)advertiseLang
                        isAdSupported:(NSString *)isAdSupported
                          countOfRows:(NSString *)countOfRows
              withAdvertiseResolution:(NSString *)advertiseResolution
          withCategoryAdvertiseTypeId:(NSString *)categoryAdvertiseTypeId
                           withCityId:(NSString *)cityId
                         withRegionId:(NSString *)regionId
                 withSubSubCategoryId:(NSString *)subsubCategoryId
               withManfactureYearFrom:(NSString *)manfactureYearFrom
                     withManfactureTo:(NSString *)manfactureYearTo
                           withKmFrom:(NSString *)kmFrom
                             withKmTo:(NSString *)kmTo
                 withNumberOfRoomFrom:(NSString *)numberOfRoomfrom
                  withNumberOfRoomsTo:(NSString *)numberOfRoomsTo
                        withPriceFrom:(NSString *)priceFrom
                          withPriceTo:(NSString *)priceTo withFurnishedTypeId:(NSString*)furnishedTypeId
              andNumberOfRequest:(int)numberOfRequest;

+ (ServerManagerResult *)decodeJsonIntoProductItems:(NSData *)json
                                     withCategoryId:(NSString *)categoryId;
+ (ProductObject *)getProductObject:(NSDictionary *)productDictionary
                       inCategoryId:(NSString *)categoryId
                      withRequestId:(NSString *)requestId;

+ (NSString *)GET_UPDATES_AFTER_LAST_UPDATE_TIME;
+ (NSString *)GET_UPDATES_BEFORE_LAST_UPDATE_TIME;

//============================================================get product detail

+ (NSMutableDictionary *)encodeGetProductDetailJson:(NSString *)productId
                                         byLanguage:(NSString *)lang
                                        isAdvertise:(NSString *)isAdvertise
                                 andUserCountryCode:(NSString *)userCountryCode
                                      andUserNumber:(NSString *)userNumber
                                 andIsEditadvertise:(NSString*)isEditAdvertise;
//+ (ServerManagerResult*) decodeProductDetail:(NSData*)json;

+ (ServerManagerResult *)decodeProductDetail:(NSMutableDictionary *)json
                     withServerManagerResult:
                         (ServerManagerResult *)serverManagerResult;

+ (void)getProductDetails:(NSString *)productId
             withDelegate:(id<ServerManagerResponseDelegate>)delegate
            withRequestId:(NSString *)requestId
               byLanguage:(NSString *)lang
              isAdvertise:(NSString *)isAdvertise
       andUserCountryCode:(NSString *)userCountryCode
            andUserNumber:(NSString *)userNumber
       andIsEditAdvertise:(NSString*)isEditAdvertise
;

//============================================================Add View to Advertise

+ (void)addViewToAdvertise:(NSString *)productId
              withDelegate:(id<ServerManagerResponseDelegate>)delegate
             withRequestId:(NSString *)requestId;

//============================================================add product

+ (NSMutableDictionary *)encodeAddProductDetailWithoutImage:
        (ProductObject *)productObject;
+ (ServerManagerResult *)
    decodeAddProductDetailOfProduct:(NSMutableDictionary *)json
            withServerManagerResult:(ServerManagerResult *)serverManagerResult;
+ (void)addProductInfoWithoutImages:(ProductObject *)productObject
                       withDelegate:(id<ServerManagerResponseDelegate>)delegate
                      withRequestId:(NSString *)requestId;

+ (void)addImageToProductId:(NSString *)productId
                isMainImage:(NSString *)isMainImage
                  withImage:(UIImage *)productImage
               withDelegate:(id<ServerManagerResponseDelegate>)delegate
              withRequestId:(NSString *)requestId
        withNumberOfRequest:(int)numberOfRequest;

+ (void)addImageUrlToProductId:(NSString *)productId
                   isMainImage:(NSString *)isMainImage
                     withImage:(NSString *)productImageUrl
                  withDelegate:(id<ServerManagerResponseDelegate>)delegate
                 withRequestId:(NSString *)requestId
           withNumberOfRequest:(int)numberOfRequest;
//============================================================add productto
//favorite
//{"user_country_code":"00974","user_number":"66524147","productId":"3"}
+ (void)addProductToFavorite:(ProductObject *)productObject
                 AndDelegate:(id<ServerManagerResponseDelegate>)delegate
               withRequestId:(NSString *)requestId
                   withIsAdd:(NSString *)isAd;

//============================================================Get user favorite
+ (void)getUserFavoritewithDelegate:(id<ServerManagerResponseDelegate>)delegate
         withUserProductsPageNumber:(int)userFavoritePageNumber
      withUserProductsNumberOfPages:(int)userFavoriteNumberOfPages
                      withRequestId:(NSString *)requestId
                      withUpdatTime:(NSString *)lastUpdateTime
                         byLanguage:(NSString *)lang
                      isAdSupported:(NSString *)isAdSupported
                        countOfRows:(NSString *)countOfRows
            withAdvertiseResolution:(NSString *)advertiseResolution;

//============================================================Get user products
+ (void)getUserProductsWithDelegate:(id<ServerManagerResponseDelegate>)delegate
         withUserProductsPageNumber:(int)userProductsPageNumber
      withUserProductsNumberOfPages:(int)userProductsNumberOfPages
                      withRequestId:(NSString *)requestId
                      withUpdatTime:(NSString *)lastUpdateTime
                         byLanguage:(NSString *)lang
                      isAdSupported:(NSString *)isAdSupported
                        countOfRows:(NSString *)countOfRows
            withAdvertiseResolution:(NSString *)advertiseResolution
              andVisitorCountryCode:(NSString *)visitorCountryCode
                   andVisitorNumber:(NSString *)visitorNumber;

//============================================================parse products
//array
+ (ServerManagerResult *)parseProductsFromJson:(NSMutableDictionary *)json
                       withServerManagerResult:
                           (ServerManagerResult *)serverManagerResult;
//============================================================table like

+ (void)addLikeToProduct:(ProductObject *)product
             AndDelegate:(id<ServerManagerResponseDelegate>)delegate
           withRequestId:(NSString *)requestId;

//============================================================table comment

+ (void)addCommentToProduct:(ProductObject *)product
               fromUserName:(NSString *)userName
         andUserCountryCode:(NSString *)userCountryCode
              andUserNumber:(NSString *)userNumber
                withComment:(NSString *)userComment
                AndDelegate:(id<ServerManagerResponseDelegate>)delegate
              withRequestId:(NSString *)requestId;

+ (void)getCommentOfProduct:(ProductObject *)product
       withProductCommentsPageNumber:(int)productCommentsPageNumber
    withProductCommentsNumberOfPages:(int)productCommentsNumberOfPages
    withProductcommentLastUpdateTime:(NSString *)productCommentLastUpdateTime
                         AndDelegate:(id<ServerManagerResponseDelegate>)delegate
                       withRequestId:(NSString *)requestId;

+ (ServerManagerResult *)parseGetCommentOfProduct:(NSMutableDictionary *)json
                          withServerManagerResult:
                              (ServerManagerResult *)serverManagerResult;

//============================================================delete product
//item
+ (void)deleteProduct:(ProductObject *)productObject
          AndDelegate:(id<ServerManagerResponseDelegate>)delegate
        withRequestId:(NSString *)requestId;

//============================================================refresh advertise

+ (void)refreshAdvertise:(ProductObject *)productObject
          andCountryCode:(NSString *)userCountryCode
           andUserNumber:(NSString *)userNumber
         andUserLanguage:(NSString *)language
             AndDelegate:(id<ServerManagerResponseDelegate>)delegate
           withRequestId:(NSString *)requestId;

//============================================================delete user
//favorite
+ (void)deleteUserFavorite:(ProductObject *)productObject
               AndDelegate:(id<ServerManagerResponseDelegate>)delegate
             withRequestId:(NSString *)requestId;

//=================================add device token
+ (void)addDeviceToken:(NSString *)DeviceToken
           withLangauge:(NSString *)language
    withUserCountryCode:(NSString *)userCountryCode
         withUserNumber:(NSString *)userNumber
            AndDelegate:(id<ServerManagerResponseDelegate>)delegate
          withRequestId:(NSString *)requestId;

//================================report comment
+ (void)reportComment:(ProductCommentObject *)productCommentObject
          AndDelegate:(id<ServerManagerResponseDelegate>)delegate
        withRequestId:(NSString *)requestId;
//================================== Block User From Commnet
+ (void)blockUserFromComment:(ProductCommentObject *)productCommentObject andProductObj:(ProductObject *)productObject
                 AndDelegate:(id<ServerManagerResponseDelegate>)delegate
               withRequestId:(NSString *)requestId;
//================================report Advertise

+ (void)reportAdvertise:(ProductObject *)productObject
            AndDelegate:(id<ServerManagerResponseDelegate>)delegate
          withRequestId:(NSString *)requestId;

//================================delete comment
+ (void)deleteComment:(ProductCommentObject *)commentObject
          AndDelegate:(id<ServerManagerResponseDelegate>)delegate
        withRequestId:(NSString *)requestId
        withProductId:(NSString *)productId;

//================================Get Notification
+ (void)getNotificationWithCountryCode:(NSString *)userCountry
                             andNumber:(NSString *)userNumber
                     andLastUpdateTime:(NSString *)lastUpdateTime
                               andPage:(NSString *)page
                      andNumberPerPage:(NSString *)numberPerPage
                           andLanguage:(NSString *)language
                          andRequestId:(NSString *)requestId
                           andDelegate:
                               (id<ServerManagerResponseDelegate>)delegate;

+ (void)stopNotificationOfProductId:(NSString *)productId
                  andNotificationId:(NSString *)notificationId
                       andRequestId:(NSString *)requestId
                        andDelegate:(id<ServerManagerResponseDelegate>)delegate;

//======================================edit user profile
+ (void)updateUserProfile:(ProductObject *)productObject
             andRequestId:(NSString *)requestId
              andDelegate:(id<ServerManagerResponseDelegate>)delegate;

//================================Get sort by filters
+ (void)getSortByFilters:(id<ServerManagerResponseDelegate>)delegate
     withUserCountryCode:(NSString *)userCountryCode
              withNumber:(NSString *)userNumber
            withLanguage:(NSString *)language
           withRequestId:(NSString *)requestId;
+ (ServerManagerResult *)parseSortByFIlters:(NSMutableDictionary *)json
                    withServerManagerResult:
                        (ServerManagerResult *)serverManagerResult;
//================================Get cities and region
+ (void)getCitiesAndRegion:(id<ServerManagerResponseDelegate>)delegate
       withUserCountryCode:(NSString *)userCountryCode
                withNumber:(NSString *)userNumber
              withLanguage:(NSString *)language
             withRequestId:(NSString *)requestId;

+ (void)getRelateAds:(ProductObject *)productObject
         AndDelegate:(id<ServerManagerResponseDelegate>)delegate
       withRequestId:(NSString *)requestId;

+ (ServerManagerResult *)parseCitiesAndRegion:(NSMutableDictionary *)json
                      withServerManagerResult:
                          (ServerManagerResult *)serverManagerResult;
+ (void)getMoreInfo:(ProductObject *)productObject
        AndDelegate:(id<ServerManagerResponseDelegate>)delegate
      withRequestId:(NSString *)requestId;

+ (void)setNotificationRead:(id<ServerManagerResponseDelegate>)delegate withNotificationId:(NSString*)notificationId withRequestId:(NSString *)requestId;
+ (void)readAllNotification:(id<ServerManagerResponseDelegate>)delegate
              withRequestId:(NSString *)requestId;

+ (void)deleteNotification:(NSString *)notificationId
              andRequestId:(NSString *)requestId
               andDelegate:(id<ServerManagerResponseDelegate>)delegate;
+ (void)deleteAllNotification:(id<ServerManagerResponseDelegate>)delegate
                withRequestId:(NSString *)requestId;
+ (void)registeCompanyRequest:(NSString *)companyName
        AndCompanyPhoneNumber:(NSString *)companyPhoneNumber
              AndCompanyEmail:(NSString *)companyEmail
            AndCompanyDetails:(NSString *)companyDetails
                  AndDelegate:(id<ServerManagerResponseDelegate>)delegate
                withRequestId:(NSString *)requestId;



+ (void)getCompaniesDirectoriesRequest:(NSString *)lastUpdateTime
        AndPage:(int)page
              AndNumberOfPage:(int)numberOfPage
            AndLanguage:(NSString *)userDeviceLanguage
            withCategoryId:(NSString *)categoryId
          AndSearchKey:(NSString *)searchKey
                  AndDelegate:(id<ServerManagerResponseDelegate>)delegate
                withRequestId:(NSString *)requestId;

//GET Trending Search
+ (void)getTrendingSearch:(id<ServerManagerResponseDelegate>)delegate withLanguage:(NSString *)language
            withRequestId:(NSString *)requestId;
+ (void)getTextSearch:(id<ServerManagerResponseDelegate>)delegate withSearchText:(NSString *)strText withCategoryId:(NSString *)strId
         withLanguage:(NSString *)language
        withRequestId:(NSString *)requestId;

+ (NSString *)SUCCESS_OPERATION;
+ (NSString *)FAILED_OPERATION;
+ (NSString *)ALREADED_ADDED_OPERATION;
+ (NSString *)ERROR_IN_CONNECTION_OPERATION;
+ (NSString *)ERROR_USER_BLOCKED_FROM_ADMIN;
+ (NSString *)ERROR_USER_BLOCKED_BY_ANOTHER_USER ;
+ (NSString *)LANGUAGE_ARABIC;
+ (NSString *)LANGUAGE_ENGLISH;
+ (NSString *)RESOLUTION_LOW;
+ (NSString *)RESOLUTION_MEDIUM;
+ (NSString *)RESOLUTION_LARGE;
+ (NSString *)REGISTER_AS_COMPANY;
@end
