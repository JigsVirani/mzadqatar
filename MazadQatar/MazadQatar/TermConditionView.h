//
//  TermConditionView.h
//  Mzad Qatar
//
//  Created by GuruUgam on 5/19/17.
//  Copyright © 2017 Mohammed abdulfattah attia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TermConditionView : UIViewController
{
    IBOutlet UITextView *txvTermCondition;
    IBOutlet UILabel *tittleLbl;
}
@end
