//
//  RelatedAdsTableViewCell.h
//  Mzad Qatar
//
//  Created by Paresh Kacha on 15/11/15.
//  Copyright © 2015 Mohammed abdulfattah attia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProductObject.h"
#import "MWPhotoBrowser.h"
#import "CameraView.h"
#import "CameraViewDelegate.h"
#import "MWPhotoBrowser.h"
#import "ImageCollectionFooterCell.h"
#import "ImageCollectionViewCell.h"
#import <QuartzCore/QuartzCore.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import "DisplayUtility.h"
#import "SharedGraphicInfo.h"
#import "DialogManager.h"
#import "AppDelegate.h"
#import "GridView.h"
#import "SelectImageDialogueViewController.h"

@class RelatedAdsTableViewCell;
@protocol RelatedAdsTableViewCellDelegate <NSObject>
@optional
- (void)imageAddedToProductObject:(UIImage *)newImage
                  isEditAdvertise:(BOOL)isEditAdvertise;
@optional
@end

@interface RelatedAdsTableViewCell : UITableViewCell
<MWPhotoBrowserDelegate, UICollectionViewDataSource,
UICollectionViewDelegate,
UICollectionViewDelegateFlowLayout,
UIImagePickerControllerDelegate, CameraViewDelegate,
UINavigationControllerDelegate, SelectImageDelegate>
{
    bool isAddCell;
    bool isEditAdvertise;
    ProductObject *productObject;
    UIViewController *parentView;
    CGSize footerSize;
    NSMutableArray * adArray;
    id<RelatedAdsTableViewCellDelegate> relatedViewerDelegate;
    CameraView *cameraView;
}

@property(strong, nonatomic) FDTakeController *takeController;
@property(strong, nonatomic) IBOutlet UICollectionView *collectionView;
@property (nonatomic, strong) IBOutlet UICollectionViewFlowLayout *flowLayout;
@property(strong, nonatomic) IBOutlet UIActivityIndicatorView *loadingProductDetailsIndicator;


- (void)loadCell:(UIViewController *)parentViewParam
       isAddCell:(bool)isAddCellParam
 isEditAdvertise:(BOOL)isEditAdvertiseParam
withProductObject:(ProductObject *)productObjectParam
isloadingProductDetail:(BOOL)isLoading withArray:(NSMutableArray *)adsArray
withImageViewerDelegate:(id<RelatedAdsTableViewCellDelegate>)imageViewerDelegateParam;
@end
