//
//  UserProductsViewController.m
//  MazadQatar
//
//  Created by samo on 3/28/13.
//  Copyright (c) 2013 Mohammed abdulfattah attia. All rights reserved.
//

#import "UserProducts.h"
#import "ServerManager.h"
#import "ServerManagerResult.h"
#import "SharedViewManager.h"
#import "RegisterScreen.h"
#import "ProductDetailsViewController.h"
#import "EditUserProfileViewController.h"
#import "MyProductsCollectionReusableView.h"
#import "LocationVC.h"
#import "ProfileScreen.h"

@interface UserProducts ()<ConfirmationDialogueViewControllerDelegate>

@end


static int userProductsPageNumber = 1;
static int userProductsNumberOfPages = 50;

@implementation UserProducts

NSString *requestInitialUserProducts = @"requestUserProducts";
NSString *requestMoreUserProducts = @"requestMoreUserProducts";
NSString *requestDeleteAdvertise = @"requestDeleteAdvertise";
NSString *requestRefreshAdvertise = @"requestRefreshAdvertise";
NSString *requestRefreshUserProducts = @"requestRefreshUserProducts";
//NSString *requestCallAdvertiser = @"requestCallAdvertiser";

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    self.screenName = @"User Products Screen";
    [[_AdvertiseNowButton layer] setBorderWidth:1.0f];
    [[_AdvertiseNowButton layer] setBorderColor:[UIColor whiteColor].CGColor];
    
    isShowUserProfileBar=false;
    
    isCatgoryProductScreen = false;
    
    loadMoreWhereAvailable = YES;
    
    ipadCellWidth = 242;
    iphoneCellWidth = ([UIScreen mainScreen].bounds.size.width-24)/2.0;
    
    [self addRefreshControlToGrid];
    
    // add background listner
    [self addBackgroundListner];
    
    if (self.userProfileProductObject == nil) {
        self.userProfileProductObject = [[ProductObject alloc] init];
    }
}

- (void)showBackButton:(BOOL)isShown {
        
    self.backBtnArrow.hidden = !isShown;
    self.backBtnText.hidden = !isShown;
    self.sideMenuBtn.hidden = isShown;
    self.AdvertiseNowButton.hidden = isShown;
    self.notificationBtn.hidden = isShown;
    self.notificationCountLbl.hidden = isShown;
    self.searchIconBtn.hidden = isShown;
    self.advertiseNowFreeBtn.hidden = isShown;
}

- (void)requestFinished:(ServerManagerResult *)serverManagerResult
            ofRequestId:(NSString *)requestId
    withNumberOfRequest:(int)numberOfRequest {
    if ([serverManagerResult.opertationResult
         isEqualToString:[ServerManager ERROR_IN_CONNECTION_OPERATION]]) {
        [self showOverlayWithIndicator:false];
        [DialogManager showErrorInConnection];
        return;
    }
    // error in server
    if ([serverManagerResult.opertationResult
         isEqualToString:[ServerManager FAILED_OPERATION]]) {
        [self showOverlayWithIndicator:false];
       // [DialogManager showErrorInServer];
        [DialogManager showErrorInServerbyMessage:serverManagerResult.opertationMsg];
        return;
    }
    if (requestId == requestInitialUserProducts ||
        requestId == requestMoreUserProducts ||
        requestId == requestRefreshUserProducts) {
        if (refreshControl.isRefreshing) {
            [refreshControl endRefreshing];
        }
        
        [self showOverlayWithIndicator:false];

        loading = NO;
        
        [SharedData
         setUserProductsLastUpdateTime:serverManagerResult.lastUpdateTime];
        
        if (requestId == requestInitialUserProducts ||
            requestId == requestRefreshUserProducts) {
            // error happen in server and returened jsonarray nil
            //  if(serverManagerResult.jsonArray.count==0)
            //  {
            // [ErrorManager showErrorInServer];
            
            // return;
            //  }
            self.userProfileProductObject = serverManagerResult.userProfileObject;
           
            isShowUserProfileBar=true;
            productItems = serverManagerResult.jsonArray;
            
            [self uploadProductsIfAvailable];
            
            // show no ads available if products item 0
            if (productItems.count == 0) {
                //[self
                //addLabelToMiddleWithText:NSLocalizedString(@"NO_USER_PRODUCTS_AVAILABLE",
                //nil) inCollectionView:NO];
                [self.lblNoAdsAvailable setHidden:NO];
                [self.collectionViewGridView setScrollEnabled:false];
            } else {
                //[self removeLabelfromMiddleOfView];
                [self.lblNoAdsAvailable setHidden:YES];
                [self.collectionViewGridView setScrollEnabled:true];
            }
            
            [self.collectionViewGridView reloadData];
            
           // [[NotificationViewController getInstance] refreshNowNotificationCount];
        } else if (requestId == requestMoreUserProducts) {
            if (serverManagerResult.jsonArray.count > 0) {
                [productItems addObjectsFromArray:serverManagerResult.jsonArray];
            } else {
                loadMoreWhereAvailable = NO;
            }
        }
        
        if (requestId != requestRefreshUserProducts) {
            userProductsPageNumber++;
        }
        
        [self.collectionViewGridView reloadData];
    } else if (requestId == requestDeleteAdvertise) {
        if ([serverManagerResult.opertationResult
             isEqualToString:[ServerManager SUCCESS_OPERATION]]) {
            //[self.collectionViewGridView reloadData];
            [self startLoadingUserProductsWithRequestId:requestInitialUserProducts
                                        andIsNewSession:YES];
        } else if ([serverManagerResult.opertationResult
                    isEqualToString:[ServerManager FAILED_OPERATION]]) {
            // start loading again the deleted item
            [DialogManager showErrorInDeletingAdvertise];
        } else if ([serverManagerResult.opertationResult
                    isEqualToString:[ServerManager ALREADED_ADDED_OPERATION]]) {
            [DialogManager showErrorProductNotFound];
            
            [self refreshCurrentGrid];
        }
        
    } else if (requestId == requestRefreshAdvertise) {
        if ([serverManagerResult.opertationResult
             isEqualToString:[ServerManager SUCCESS_OPERATION]]) {
            [DialogManager showDialogWithMessage:serverManagerResult.opertationMsg];
            
            [self startLoadingUserProductsWithRequestId:requestInitialUserProducts
                                        andIsNewSession:YES];
        } else if ([serverManagerResult.opertationResult
                    isEqualToString:[ServerManager FAILED_OPERATION]]) {
            // start loading again the deleted item
            [DialogManager showErrorInRefreshingAdvertise];
        } else if ([serverManagerResult.opertationResult
                    isEqualToString:[ServerManager ALREADED_ADDED_OPERATION]]) {
            [DialogManager showErrorWithMessage:serverManagerResult.opertationMsg];
            
            [self refreshCurrentGrid];
        }
    }
    //    else if(requestId==requestRefreshUserProducts)
    //    {
    //        loading=NO;
    //
    //        [self.collectionViewGridView reloadData];
    //    }
    
    // UserImage Not showing so reload collectionView with delay
    [self performSelector:@selector(ReloadCollectionView) withObject:self afterDelay:1.0];
}
#pragma mark UICollectionview Reload
-(void)ReloadCollectionView
{
    [self.collectionViewGridView reloadData];
}
- (void)uploadProductsIfAvailable {
    uploadingListnerManager = [UploadingListenerManager getManager];
    
    NSMutableArray *uploadingProductList =
    [SharedGraphicInfo getcurrentUploadingProductList];
    
    if (uploadingProductList.count > 0) {
        
        [uploadingListnerManager registerUploadingListner:self];
        
        for (int i = 0; i < uploadingProductList.count; i++) {
            
            ProductObject *productObject = [uploadingProductList objectAtIndex:i];
            
            if (productObject.productDescription != nil) {
                productObject.productDescriptionSizePortrait =
                [StringUtility getSize:productObject.productDescription
                           andFontSize:16
                                isBold:YES
                            isPortrait:YES];
                productObject.productDescriptionSizeLandscape =
                [StringUtility getSize:productObject.productDescription
                           andFontSize:16
                                isBold:YES
                            isPortrait:NO];
            }
            
            // if not started before
            if (productObject.isUploading == NO) {
                
                UploadingProductsManager *uploadingProdictManager =
                [[UploadingProductsManager alloc] init] ;
                [uploadingProdictManager startProductObjectUploadloading:productObject];
                
                [self showOverlayWithIndicator:YES];
                
                if (productObject.isEditingAdvertise == false) {
                    // animate to top in case of adding advertise
                    [self.collectionViewGridView
                     scrollRectToVisible:CGRectMake(0, 0, 1, 1)
                     animated:YES];
                    
                    [productItems insertObject:productObject atIndex:0];
                }
            }
        }
    }
}
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    

    if ([UserSetting isUserRegistered] == false && isProfileOwner == true) {
        [SharedViewManager showLoginScreen:self];
        self.view.hidden = true;
        return;
    }else{
        self.view.hidden = false;
    }
    
    if (productItems.count > 0) {
        NSMutableArray *uploadingList =
        [SharedGraphicInfo getcurrentUploadingProductList];
        if (uploadingList.count > 0) {
            [self uploadProductsIfAvailable];
            return;
        }
    }
    isPullNewData = false;
    [self startLoadingUserProductsWithRequestId:requestInitialUserProducts
                                andIsNewSession:YES];
}
- (void)viewWillAppear:(BOOL)animated
{
    self.tabBarController.tabBar.hidden = _hideTabBar;
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(registerCompany:)
                                                 name:@"REGISTERCOMPANY"
                                               object:nil];
    isProfileOwner = false;
    
    // will be empty in case user not registered and its the owner of profile , as
    // vistor number only set in the product detail
    if ((([self.userProfileProductObject.productCountryCode
           isEqualToString:@" "] &&
          [self.userProfileProductObject.productUserNumber
           isEqualToString:@" "]) ||
         (self.userProfileProductObject.productCountryCode == nil &&
          self.userProfileProductObject.productUserNumber == nil)) ||
        ([self.userProfileProductObject.productCountryCode
          isEqualToString:[UserSetting getUserCountryCode]] &&
         [self.userProfileProductObject.productUserNumber
          isEqualToString:[UserSetting getUserNumber]]))
    {
        isProfileOwner = true;
        self.userProfileProductObject.productCountryCode =
        [UserSetting getUserCountryCode];
        self.userProfileProductObject.productUserNumber =
        [UserSetting getUserNumber];
        self.tabBarController.tabBar.hidden = NO;
        [self.tabBarController.tabBar setTranslucent:NO];
    }
    
   
}
-(void)registerCompany:(NSNotification *)notification {
    if (self.loadCompany == NO) {
        self.loadCompany = YES;
        CompanyRegistration  *companyRegistration = [self.storyboard
                                                     instantiateViewControllerWithIdentifier:@"RegisterAsCompany"];
        
        [self.navigationController pushViewController:companyRegistration animated:YES];
    }
}
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [uploadingListnerManager unRegisterUploadingListner:self];
    self.loadCompany = NO;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:[SharedViewManager registerSegue]]) {
        RegisterScreen *registerScreen = [segue destinationViewController];
        
        [registerScreen setSuccessScreenSegueName:nil];
        registerScreen.navigationItem.hidesBackButton = YES;
    } else if ([[segue identifier] isEqualToString:@"ImageViewClicked"] ||
               [[segue identifier] isEqualToString:@"ImageAdViewClicked"] ||
               [[segue identifier] isEqualToString:@"vistorImageViewClicked"]) {
        [self openProductDetail:segue sender:sender isEditAdvertise:NO];
    } else if ([[segue identifier] isEqualToString:@"EditAdvertiseClicked"]) {
        [self openProductDetail:segue
                         sender:[[[sender superview] superview]superview]
                isEditAdvertise:YES];
    } else if ([[segue identifier] isEqualToString:@"EditUserProfileSegue"]) {
        EditUserProfileViewController *editUserProfileViewController =
        [segue destinationViewController];
        
        [editUserProfileViewController
         setProductObject:self.userProfileProductObject];
    }else if ([[segue identifier] isEqualToString:@"ProfileSegue"]) {
        ProfileScreen *userProfileViewController = [segue destinationViewController];
        [userProfileViewController setUserProfileProductObject:self.userProfileProductObject];
    }
    
    [super prepareForSegue:segue sender:sender];
}

- (void)advertiseEditBtnClicked:(id)sender {
    [self performSegueWithIdentifier:@"EditAdvertiseClicked" sender:sender];
}

- (void)refreshAdvertiseClicked:(id)sender {
    NSIndexPath *index = [self.collectionViewGridView
                          indexPathForCell:(UICollectionViewCell *)[[[sender superview] superview]superview]];
    
    ProductObject *productToBeRefresh = [productItems objectAtIndex:index.row];
    
    productToBeRefresh.isRefreshingAdvertise = YES;
    
    [self.collectionViewGridView reloadData];
    
    [ServerManager
     refreshAdvertise:productToBeRefresh
     andCountryCode:[UserSetting getUserCountryCode]
     andUserNumber:[UserSetting getUserNumber]
     andUserLanguage:[UserSetting
                      getAdvertiseLanguageServerParam:
                      [UserSetting getUserFilterViewAdvertiseLanguage]]
     AndDelegate:self
     withRequestId:requestRefreshAdvertise];
}

- (void)cellDeleted:(UICollectionViewCell *)cell {
    
    NSIndexPath *index = [self.collectionViewGridView indexPathForCell:cell];
    
    ProductObject *productToBeDeleted = [productItems objectAtIndex:index.row];
    
    productToBeDeleted.isDeleting = YES;
    
    [self.collectionViewGridView reloadData];
    
    [ServerManager deleteProduct:productToBeDeleted
                     AndDelegate:self
                   withRequestId:requestDeleteAdvertise];
}

- (void)openProductDetail:(UIStoryboardSegue *)segue
                   sender:(id)sender
          isEditAdvertise:(BOOL)isEditAdvertise {
    // get the index clicked
    UICollectionViewCell *cell = (UICollectionViewCell *)sender;
    NSIndexPath *indexPath = [self.collectionViewGridView indexPathForCell:cell];
//    [DialogManager showPressEditAdvertiseDialog];

    ProductObject *productObjectSelected =
    [productItems objectAtIndex:indexPath.row];
    // open category products and set the index
    ProductDetailsViewController *productDetail =
    [segue destinationViewController];
    productDetail.userEmail = self.userProfileProductObject.productUserEmail;
    [productDetail setProductId:productObjectSelected.productId];
    if (isEditAdvertise) {
        [productDetail setIsAddProductDetail:isEditAdvertise];
        [productDetail setIsEditAdvertise:isEditAdvertise];
    }
}

- (void)updateProductProgress:(ProductObject *)productObject {
    //    int productIndex=[productItems indexOfObject:productObject];
    //    [productItems removeObjectAtIndex:productIndex];
    //    [productItems insertObject:productObject atIndex:productIndex];
    //    [self performSelectorInBackground:@selector(loadData:)
    //    withObject:passedValue];
    
    [self.collectionViewGridView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // [SharedData clearSdImageMemory];
}

- (void)loadData:(NSNumber *)isNewSession {
    [self startLoadingUserProductsWithRequestId:requestMoreUserProducts
                                andIsNewSession:NO];
}

- (void)uploadingStarted {
    if ([SharedData isAllowLog]) {
        /*NSLog(@"===================================================");
         NSLog(@"GridView uploadingStarted");
         NSLog(@"===================================================");*/
    }
}

- (void)uploadingProgressOfProduct:(ProductObject *)productObjectToUpload;
{
    if ([SharedData isAllowLog]) {
        /* NSLog(@"===================================================");
         NSLog(@"GridView uploadingProgress
         is=%f",productObjectToUpload.productUploadingPercent);
         NSLog(@"==================================================="); */
    }
    
    // [self updateProductProgress:productObjectToUpload];
    [self performSelectorOnMainThread:@selector(updateProductProgress:)
                           withObject:productObjectToUpload
                        waitUntilDone:NO];
}

- (void)uploadingFinished:(ProductObject *)productObjectToUpload;
{
    if ([SharedData isAllowLog]) {
        /* NSLog(@"===================================================");
         NSLog(@"GridView uploadingFinished" );
         NSLog(@"GridView updating the product item list list of product
         id=%p",productObjectToUpload);
         NSLog(@"==================================================="); */
    }
    
    // if(productObjectToUpload.isEditingAdvertise)
    // {
    [self showOverlayWithIndicator:NO];
    // }
    // int productIndex=[productItems indexOfObject:productObjectToUpload];
    //[productItems removeObject:productObjectToUpload];
    // productObjectToUpload.isNeedToUploadedOnServer=NO;
    //[productItems insertObject:productObjectToUpload atIndex:productIndex];
    
    [self performSelectorOnMainThread:@selector(updateProductProgress:)
                           withObject:productObjectToUpload
                        waitUntilDone:NO];
    
    NSMutableArray *uploadingProductList =
    [SharedGraphicInfo getcurrentUploadingProductList];
    if (uploadingProductList.count == 1) {
        isPullNewData = true;
        [self startLoadingUserProductsWithRequestId:requestInitialUserProducts
                                    andIsNewSession:YES];
    }
    
    //[self updateProductProgress:productObjectToUpload];
}
#pragma mark Top UIButtons Events
- (IBAction)profileArrowClicked:(id)sender
{
    [self performSegueWithIdentifier:@"ProfileSegue" sender:nil];
}
- (IBAction)locationBtnClicked:(id)sender {
    
 
    if(self.userProfileProductObject.productUserAddress == nil || [self.userProfileProductObject.productUserAddress  isEqual: @""] || [self.userProfileProductObject.productUserAddress  isEqualToString:@"0"]){
        self.userProfileProductObject.productUserAddress = NSLocalizedString(@"No Location Name Found", nil);
         [DialogManager showLocationDialogue:self.userProfileProductObject.productUserAddress andDelegate:self andRequestId:@"requestLocation"];
    }else{
//        BOOL canHandle = [[UIApplication sharedApplication] canOpenURL: [NSURL URLWithString:@"comgooglemaps://"]];
//
//         if (canHandle) {
         // Google maps installed
              [DialogManager showMapDialogue:self.userProfileProductObject.productUserAddress andDelegate:self andRequestId:@"requestLocation"];
//         }else {
//
//             [self displayMapScreen:1];
//         }
        
    }
}
- (void)cancelBtnClicked:(NSString *)requestId{
    NSLog(@"Dismiss Location Dialog");
}
-(void)OkBtnClicked:(NSString *)requestId{
    if ([requestId  isEqual: @"requestLocation"]) {

    }
}

-(void) mapTypeBtnClicked:(int)index{

      [self displayMapScreen:index];

}

- (void) displayMapScreen:(int) type {
    NSURL *mapUrl = nil;
    switch (type) {
        case 0:
            if ([[UIApplication sharedApplication] canOpenURL:
                 [NSURL URLWithString:@"comgooglemaps://"]]) {
                
                mapUrl = [NSURL URLWithString:[NSString stringWithFormat:@"comgooglemaps://?q=%f,%f",[self.userProfileProductObject.productUserlang floatValue],[self.userProfileProductObject.productUserlong floatValue]]];
                [[UIApplication sharedApplication] openURL:mapUrl];
            }else{
                [DialogManager showSuccessMessage:NSLocalizedString(@"Google Maps not installed in your device", nil)];
            }
            break;
        case 1:
            mapUrl= [NSURL URLWithString:[NSString stringWithFormat:@"http://maps.apple.com/?q=%f,%f",[self.userProfileProductObject.productUserlang floatValue],[self.userProfileProductObject.productUserlong floatValue]]];
            [[UIApplication sharedApplication] openURL:mapUrl];
            break;
            
        default:
            break;
    }
    
    
    
}
- (IBAction)callBtnClicked:(id)sender {
    [DialogManager
     showConfirmationDialogueWithDescription:
     [NSString
      stringWithFormat:@"%@ %@ %@",
      NSLocalizedString(@"CONFIRMATION_CALL_MSG", nil),
      self.userProfileProductObject
      .productUserNumber, NSLocalizedString(@"?",nil)]
     andConfirmBtnText:NSLocalizedString(
                                         @"CONFIRMATION_CALL_BTN_TEXT",
                                         nil)
     andDelegate:self
     andRequestId:@"requestCallAdvertiser"];
}

- (void)confirmBtnClicked:(NSString *)requestId {
    if ([requestId  isEqual: @"requestCallAdvertiser"]) {
        //[SystemUtility callNumber: self.userProfileProductObject.productUserNumber];
        [SystemUtility callNumberWithCountryCode:@"00974" withNumber:self.userProfileProductObject.productUserNumber];
    }
}
- (IBAction)shareOnOthersAppsClicked:(UIButton*)sender {
    
    /*if (self.userProfileProductObject.productShareImageUrl==nil) {
        [DialogManager showErrorProductNotFound];
        return;
    }*/
    
    NSString *text = nil;
    text = [NSString stringWithFormat:@"%@\n\n%@\n\n%@\n\n%@",self.userProfileProductObject.productUserName,self.userProfileProductObject.productUserShortDescription,NSLocalizedString(@"FOR_MORE_DETAILS", nil),self.userProfileProductObject.productUserAddress];
    
    UIImage *image = [SharedViewManager
                      writeOnImageTitle:self.userProfileProductObject.productUserName
                      andDescription:self.userProfileProductObject.productUserShortDescription
                      andNumber:[NSString
                                 stringWithFormat:@"%@%@",
                                 self.userProfileProductObject.productCountryCode,
                                 self.userProfileProductObject.productUserNumber]
                      onImage:[self downloadShareImage]];
    
    CGRect buttonFrameInDetailView= [self.view convertRect:sender.frame fromView:self.view];
    
    socialShareUtility = [[SocialShareUtility alloc] init];
    [socialShareUtility shareOnOtherAppsInView:self
                            andFrameToSHowFrom:buttonFrameInDetailView
                                      withText:text
                                     withImage:image
                                       withUrl:self.userProfileProductObject.productUserUrl];
}
-(UIImage*)downloadShareImage
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NSURL *url = [NSURL URLWithString:self.userProfileProductObject.productUserImageUrl];
    UIImage *image = [UIImage imageWithData: [NSData dataWithContentsOfURL:url]];
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    return image;
}

- (void)startLoadingUserProductsWithRequestId:(NSString *)requestId
                              andIsNewSession:(bool)isNewSession {
    if (requestId == requestInitialUserProducts) {
        loadMoreWhereAvailable = YES;
        // when switching between tabs and there is 100 item loaded , all items are
        // gone because we load only 50
        if ([self.productItems count] != 0 && isPullNewData == false) {
            [self refreshCurrentGrid];
            return;
        } else {
            [SharedData setUserProductsLastUpdateTime:@"0"];
        }
        [self showOverlayWithIndicator:TRUE];
        userProductsPageNumber = 1;
    }
    loading = YES;
    [ServerManager
     getUserProductsWithDelegate:self
     withUserProductsPageNumber:userProductsPageNumber
     withUserProductsNumberOfPages:userProductsNumberOfPages
     withRequestId:requestId
     withUpdatTime:[SharedData getUserProductsLastUpdateTime]
     byLanguage:
     [UserSetting
      getAdvertiseLanguageServerParam:
      [UserSetting
       getUserFilterViewAdvertiseLanguage]]
     isAdSupported:@"true"
     countOfRows:self.advertiseInOneRowCountString
     withAdvertiseResolution:self.advertiseResolution
     andVisitorCountryCode:self.userProfileProductObject
     .productCountryCode
     andVisitorNumber:self.userProfileProductObject
     .productUserNumber];
}

- (void)refreshCurrentGrid {
    [ServerManager
     getUserProductsWithDelegate:self
     withUserProductsPageNumber:1
     withUserProductsNumberOfPages:(userProductsPageNumber - 1) *
     userProductsNumberOfPages
     withRequestId:requestRefreshUserProducts
     withUpdatTime:@"0"
     byLanguage:
     [UserSetting
      getAdvertiseLanguageServerParam:
      [UserSetting
       getUserFilterViewAdvertiseLanguage]]
     isAdSupported:@"true"
     countOfRows:self.advertiseInOneRowCountString
     withAdvertiseResolution:self.advertiseResolution
     andVisitorCountryCode:self.userProfileProductObject
     .productCountryCode
     andVisitorNumber:self.userProfileProductObject
     .productUserNumber];
}

- (void)getNewData:(NSString *)searchStr {
    if ([searchStr isEqualToString:@""] == false) {
        [SharedViewManager showCategoryProductsWithSearch:self
                                             andProductId:@"0"
                                             andSearchStr:searchStr
                                         andSubCategoryId:@""
                                       andSubCategoryName:@""
                                              andSortById:@""
                                            andSortByName:@""
                                           andsortByIndex:-1
                                            andTabBarName:  [self getTabBarSearchAllCategories] andDefaultTabSelected:0
         ];
        
        return;
    }
    
    [self startLoadingUserProductsWithRequestId:requestInitialUserProducts
                                andIsNewSession:YES];
}

#pragma mark collection view cell layout
- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    ProductObject *productObject = [productItems objectAtIndex:indexPath.row];
    if (productObject.isAd) {
        if ([DisplayUtility isPad]) {
            return CGSizeMake(768, 227);
        } else {
            return CGSizeMake(320, (226*[DisplayUtility changeLaoutAsPerHeight]));
        }
    } else {
        if ([DisplayUtility isPad]) {
            if (isProfileOwner) {
                return CGSizeMake(ipadCellWidth, 373);
            } else {
                return CGSizeMake(ipadCellWidth, 335);
            }
            
        } else {
            if (isProfileOwner) {
                return CGSizeMake(iphoneCellWidth, iphoneCellWidth + 117);
            } else {
                return CGSizeMake(iphoneCellWidth, (228*[DisplayUtility changeLaoutAsPerHeight]));
            }
        }
    }
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView
           viewForSupplementaryElementOfKind:(NSString *)kind
                                 atIndexPath:(NSIndexPath *)indexPath {
    
    if (kind == UICollectionElementKindSectionHeader) {
        
        MyProductsCollectionReusableView *headerView =
        [collectionView dequeueReusableSupplementaryViewOfKind:
         UICollectionElementKindSectionHeader
                                           withReuseIdentifier:@"HeaderView"
                                                  forIndexPath:indexPath];
        
        // edit btn
        if ([self.userProfileProductObject.productCountryCode isEqualToString:[UserSetting getUserCountryCode]] &&
            [self.userProfileProductObject.productUserNumber isEqualToString:[UserSetting getUserNumber]]) {
            
                headerView.btnEditInfo.hidden = NO;
                headerView.btnCall.hidden = NO;
                headerView.btnLocation.hidden = NO;
                headerView.btnShare.hidden = NO;
            
            } else {
                
                /*headerView.editInfoConstant.constant = 0.0;
                headerView.editleadingConstant.constant = 0.0;
                headerView.editBtnWidthConstant.constant = 0.0;*/
                headerView.btnEditInfo.hidden = YES;
                headerView.btnCall.hidden = NO;
                headerView.btnLocation.hidden = NO;
                headerView.btnShare.hidden = NO;
                
                /*if (![DisplayUtility isPad]){
                    headerView.btnCallHeight.constant = -0.0f;
                    headerView.btnLocationHeight.constant = -0.0f;
                    headerView.btnEditHeight.constant = -0.0f;
                }*/
            }
        
        // user info
        
        if(![[SharedData getDeviceLanguage]isEqualToString:@"en"]){
            
            if([DisplayUtility isPad]){
                
                [headerView.btnShare setTitleEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 7)];
                [headerView.btnShare setImageEdgeInsets:UIEdgeInsetsMake(0, 7, 0, 0)];
                [headerView.btnEditInfo setTitleEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 7)];
                [headerView.btnEditInfo setImageEdgeInsets:UIEdgeInsetsMake(0, 7, 0, 0)];
                [headerView.btnLocation setTitleEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 7)];
                [headerView.btnLocation setImageEdgeInsets:UIEdgeInsetsMake(0, 7, 0, 0)];
                [headerView.btnCall setTitleEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 7)];
                [headerView.btnCall setImageEdgeInsets:UIEdgeInsetsMake(0, 7, 0, 0)];
                
            }else{
                if([[UIScreen mainScreen]bounds].size.width == 320){
                [headerView.btnShare.titleLabel setFont:[UIFont systemFontOfSize:11]];
                [headerView.btnEditInfo.titleLabel setFont:[UIFont systemFontOfSize:11]];
                [headerView.btnLocation.titleLabel setFont:[UIFont systemFontOfSize:11]];
                [headerView.btnCall.titleLabel setFont:[UIFont systemFontOfSize:11]];
                }
                
                [headerView.btnShare setTitleEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 5)];
                [headerView.btnEditInfo setTitleEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 5)];
                [headerView.btnLocation setTitleEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 5)];
                [headerView.btnCall setTitleEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 5)];
                
                [headerView.btnShare setImageEdgeInsets:UIEdgeInsetsMake(0, 5, 0, 0)];
                [headerView.btnEditInfo setImageEdgeInsets:UIEdgeInsetsMake(0, 5, 0, 0)];
                [headerView.btnLocation setImageEdgeInsets:UIEdgeInsetsMake(0, 5, 0, 0)];
                [headerView.btnCall setImageEdgeInsets:UIEdgeInsetsMake(0, 0, 5, 0)];
            }
            
        }else{
            
            if([DisplayUtility isPad]){
                [headerView.btnShare setTitleEdgeInsets:UIEdgeInsetsMake(0, 7, 0, 0)];
                [headerView.btnShare setImageEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 7)];
                [headerView.btnEditInfo setTitleEdgeInsets:UIEdgeInsetsMake(0, 7, 0, 0)];
                [headerView.btnEditInfo setImageEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 7)];
                [headerView.btnLocation setTitleEdgeInsets:UIEdgeInsetsMake(0, 7, 0, 0)];
                [headerView.btnLocation setImageEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 7)];
                [headerView.btnCall setTitleEdgeInsets:UIEdgeInsetsMake(0, 7, 0, 0)];
                [headerView.btnCall setImageEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 7)];
                
            }else
                if([[UIScreen mainScreen]bounds].size.width == 320){
                    [headerView.btnShare.titleLabel setFont:[UIFont systemFontOfSize:11]];
                    [headerView.btnEditInfo.titleLabel setFont:[UIFont systemFontOfSize:11]];
                    [headerView.btnLocation.titleLabel setFont:[UIFont systemFontOfSize:11]];
                    [headerView.btnCall.titleLabel setFont:[UIFont systemFontOfSize:11]];
                }
                [headerView.btnShare setImageEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 5)];
                [headerView.btnEditInfo setImageEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 5)];
                [headerView.btnLocation setImageEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 5)];
                [headerView.btnCall setImageEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 5)];
            
                [headerView.btnShare setTitleEdgeInsets:UIEdgeInsetsMake(0, 5, 0, 0)];
                [headerView.btnEditInfo setTitleEdgeInsets:UIEdgeInsetsMake(0, 5, 0, 0)];
                [headerView.btnLocation setTitleEdgeInsets:UIEdgeInsetsMake(0, 5, 0, 0)];
                [headerView.btnCall setTitleEdgeInsets:UIEdgeInsetsMake(0, 5, 0, 0)];
             }
        
        if([self.userProfileProductObject.productUserisCompany isEqualToString:@"0"]){
            headerView.isCompanyView.hidden = NO;
            headerView.isCompanyImage.hidden = NO;
        }else{
            headerView.isCompanyView.hidden = YES;
            headerView.isCompanyImage.hidden = YES;
        }
        
        NSString *imageName = [[self.userProfileProductObject productId]stringByAppendingString:@".png"];
        
        headerView.imageUserProfile.layer.cornerRadius = headerView.imageUserProfile.bounds.size.width/2.0;
        headerView.imageUserProfile.layer.borderWidth = 2.0;
        headerView.imageUserProfile.layer.borderColor = [UIColor darkGrayColor].CGColor;
        headerView.imageUserProfile.clipsToBounds = YES;
        
        [headerView.imageUserProfile sd_setImageWithURL:[NSURL URLWithString:self.userProfileProductObject.productUserImageUrl]placeholderImage:[UIImage imageNamed:imageName]completed:nil];
        
        [headerView.nameUserProfile setText:self.userProfileProductObject.productUserName];
        
        if([[SharedData getDeviceLanguage]isEqualToString:@"en"]){
            [headerView.nameUserProfile setTextAlignment:NSTextAlignmentLeft];
        }else{
            [headerView.nameUserProfile setTextAlignment:NSTextAlignmentRight];
        }
        
        [headerView.numberOfAdsUserProfile setText:[self.userProfileProductObject.productUserNumberOfAds stringByReplacingOccurrencesOfString:@"ads posted" withString:@"Adv."]];
        
        NSString *countryCodePlusNumber = [NSString
                                           stringWithFormat:@"%@%@",
                                           self.userProfileProductObject.productCountryCode,
                                           self.userProfileProductObject.productUserNumber];
        [headerView.numberUserProfile setText:countryCodePlusNumber];
        
        [headerView.shortDescriptionUserProfile
         setText:self.userProfileProductObject.productUserShortDescription];
        UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(profileArrowClicked:)];
        tapGestureRecognizer.numberOfTapsRequired = 1;
        [headerView.shortDescriptionUserProfile addGestureRecognizer:tapGestureRecognizer];
        headerView.shortDescriptionUserProfile.userInteractionEnabled = YES;
        return headerView;
    }
    
    return nil;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section {
    if(isShowUserProfileBar==false)
    {
        return CGSizeZero;
    }
    if ([DisplayUtility isPad])
    {
        NSAttributedString *attributedText = [[NSAttributedString alloc]
                                              initWithString:self.userProfileProductObject.productUserShortDescription.length == 0?@"No Description":self.userProfileProductObject.productUserShortDescription
                                              attributes:@{
                                                           NSFontAttributeName : [UIFont boldSystemFontOfSize:15.00]
                                                           }];
        
        CGRect rect = [attributedText
                       boundingRectWithSize:CGSizeMake(self.view.frame.size.width-16.0, CGFLOAT_MAX)
                       options:NSStringDrawingUsesLineFragmentOrigin
                       context:nil];
        
        return CGSizeMake(50, 96+rect.size.height+26+25+10);
    }
    else
    {
        /*NSAttributedString *attributedText = [[NSAttributedString alloc]
                                              initWithString:self.userProfileProductObject.productUserShortDescription.length == 0?@"No Description":self.userProfileProductObject.productUserShortDescription
                                              attributes:@{NSFontAttributeName : [UIFont boldSystemFontOfSize:14.00]
                                                           }];
        
        CGRect rect = [attributedText boundingRectWithSize:CGSizeMake(self.view.frame.size.width-16.0, CGFLOAT_MAX)
                       options:NSStringDrawingUsesLineFragmentOrigin
                       context:nil];*/
        if ([self.userProfileProductObject.productCountryCode isEqualToString:[UserSetting getUserCountryCode]] &&
            [self.userProfileProductObject.productUserNumber isEqualToString:[UserSetting getUserNumber]]){
            //return CGSizeMake(50, 58+rect.size.height+26+40);
            return CGSizeMake(50, 58+16+26+40+10);
        }else{
            //return CGSizeMake(50, 58+rect.size.height+26+19);
            return CGSizeMake(50, 58+16+26+40+10);
        }
    }
}

- (void)viewDidUnload {
    
    [super viewDidUnload];
}

@end
