//
//  CategoryChooser.m
//  MazadQatar
//
//  Created by samo on 5/13/13.
//  Copyright (c) 2013 Mohammed abdulfattah attia. All rights reserved.
//

#import "CategoryChooser.h"
#import "ServerManager.h"

@interface CategoryChooser ()

@end

static NSString *categoryChooserCellIdentifier = @"CategoryChooserCell";

@implementation CategoryChooser

- (void)viewDidLoad {
  [super viewDidLoad];
    
    self.screenName = @"Category Chooser";
  // Do any additional setup after loading the view.
  // self.selectedIndex=0;

  //UIImage *backgroundPattern = [UIImage imageNamed:@"bg.png"];
  //[self.view setBackgroundColor:[UIColor colorWithPatternImage:backgroundPattern]];
    [self.view setBackgroundColor:[UIColor blackColor]];
    
    if([LanguageManager currentLanguageIndex]==1){
        self.title = @"قسم الاعلان";
    }else{
        self.title = @"Category";
    }
    
  //categories = [SharedData getCategories];
    categories = [[NSMutableArray alloc]init];
    
    //For hide AllCategory if user comes from Addadvertise
    if(self.isFromAddAdvts == false){
        ProductObject *categoryObject = [[ProductObject alloc] init];
        categoryObject.productTilte = NSLocalizedString(@"ALL_SUBCATEGORIES", Nil);
        categoryObject.productId = @"0";
        [categories addObject:categoryObject];
    }
    for(int i=0;i<[[SharedData getCategories]count];i++){
        ProductObject *categoryObject = [[SharedData getCategories]objectAtIndex:i];
        NSLog(@"%@",categoryObject.productTilte);
        if(categoryObject.isAd==false){
            [categories addObject:categoryObject];
        }
    }
    //[self.navigationController.navigationBar.topItem setTitle:NSLocalizedString(@"BACK", nil)];
}

- (void)didReceiveMemoryWarning {
  [super didReceiveMemoryWarning];
  // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView
    numberOfRowsInSection:(NSInteger)section {
  return [categories count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
  // comments of product
  UITableViewCell *cell =
      [tableView dequeueReusableCellWithIdentifier:categoryChooserCellIdentifier
                                      forIndexPath:indexPath];

  ProductObject *categoryObject = [categories objectAtIndex:indexPath.row];

  cell.textLabel.text = [categoryObject productTilte];
  cell.textLabel.textColor = [UIColor whiteColor];

  if (categoryObject.productId == self.selectedCategoryId) {
    cell.accessoryType = UITableViewCellAccessoryCheckmark;
  } else {
    cell.accessoryType = UITableViewCellAccessoryNone;
  }

  // ios 7 background issue
  [self setBackgroundClear:cell];

  return cell;
}

- (void)setBackgroundClear:(UITableViewCell *)cell {
  cell.backgroundColor = [UIColor clearColor];
  // cell.backgroundView = [[UIView new] autorelease];
  // cell.selectedBackgroundView = [[UIView new] autorelease];
}

- (void)tableView:(UITableView *)tableView
    didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
  /*ProductObject *categoryObject =
      [[SharedData getCategories] objectAtIndex:indexPath.row];

  [self.delegate categoryChoosed:(int)indexPath.row
                   andCategoryId:categoryObject.productId];*/
    ProductObject *categoryObject =[categories objectAtIndex:indexPath.row];
    
    [self.delegate categoryChoosed:(int)indexPath.row andCategoryId:categoryObject.productId withArray:categories];

  [self.navigationController popViewControllerAnimated:YES];
}

- (void)viewDidUnload {

  [super viewDidUnload];
}

@end
