//
//  textFieldWithoutPaste.m
//  Mzad Qatar
//
//  Created by samo on 2/23/14.
//  Copyright (c) 2014 Mohammed abdulfattah attia. All rights reserved.
//

#import "textFieldWithoutPaste.h"

@implementation textFieldWithoutPaste

- (id)initWithFrame:(CGRect)frame {
  self = [super initWithFrame:frame];
  if (self) {
    // Initialization code
  }
  return self;
}

- (BOOL)canPerformAction:(SEL)action withSender:(id)sender {

  if ([UIMenuController sharedMenuController])
  {
    [UIMenuController sharedMenuController].menuVisible = NO;
  }
  return NO;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
