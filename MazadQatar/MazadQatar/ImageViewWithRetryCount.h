//
//  ImageViewWithRetryCount.h
//  Mzad Qatar
//
//  Created by samo on 1/29/14.
//  Copyright (c) 2014 Mohammed abdulfattah attia. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ImageViewWithRetryCount : NSObject

@property(weak) UIImage *imageView;
@property(assign) int retrycount;

@end
