//
//  ProductObjectComment.h
//  MazadQatar
//
//  Created by samo on 4/13/13.
//  Copyright (c) 2013 Mohammed abdulfattah attia. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ProductCommentObject : NSObject

@property(strong) NSString *commentId;
@property(strong) NSString *commentUserName;
@property(strong) NSString *commentDescription;
@property(strong) NSString *commentDate;
@property(strong) NSString *commenterCountryCode;
@property(strong) NSString *commenterNumber;
@property(strong) NSString *userImageUrl;

@property(assign) CGSize commentUserNameSizeCurrent;
@property(assign) CGSize commentUserNameSizePortrait;
@property(assign) CGSize commentUserNameSizeLandscape;

@property(assign) CGSize commentdescriptionSizeCurrent;
@property(assign) CGSize commentDescriptionSizePortrait;
@property(assign) CGSize commentDescriptionSizeLandscape;

@property(assign) CGSize dateSizeCurrent;
@property(assign) CGSize commentDateSizePortrait;
@property(assign) CGSize commentDateSizeLandscape;

// for comment report
@property(assign) BOOL isReportingAdvertise;
@property(strong) NSString *reportReasonId;
@property(strong) NSString *reportReasonText;

@end
