//
//  CompanyRegistration.h
//  Mzad Qatar
//
//  Created by AHMED HEGAB on 11/3/16.
//  Copyright © 2016 Mohammed abdulfattah attia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ServerManager.h"
#import "UserSetting.h"
#import "TextFieldViewController.h"
#import "DialogManager.h"
#import "TextViewTableCell.h"

@interface CompanyRegistration : UIViewController<UITextViewDelegate>
@property (weak, nonatomic) IBOutlet UITextField *companyName;
@property (weak, nonatomic) IBOutlet UITextField *companyPhoneNumber;
@property (weak, nonatomic) IBOutlet UITextField *companyEmail;
@property (weak, nonatomic) IBOutlet UITextView *companyDetails;

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@end
