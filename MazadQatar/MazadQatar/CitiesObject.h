//
//  CitiesAndRegionsObject.h
//  Mzad Qatar
//
//  Created by samo on 8/28/14.
//  Copyright (c) 2014 Mohammed abdulfattah attia. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CitiesObject : NSObject <NSCoding>

@property(nonatomic, strong) NSString *cityId;
@property(nonatomic, strong) NSString *cityName;
@property(strong) NSMutableArray *regions;

@end
