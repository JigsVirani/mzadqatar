//
//  CategoryTypesBarCell.h
//  Mzad Qatar
//
//  Created by Mohammed AbdulFattah on 2/9/15.
//  Copyright (c) 2015 Mohammed abdulfattah attia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AdvertiseType.h"

// Delgate
@class CategoryTypesBarCell;
@protocol CategoryTypesBarCellDelegate <NSObject>
@required
- (void)btnCategoryTypeNameClickedWithId:(NSString *)categoryTypeID
                                andIndex:(int)tabIndex;
@required
@end

@interface CategoryTypesBarCell : UICollectionViewCell

@property(strong, nonatomic)
    id<CategoryTypesBarCellDelegate> categoryTypesBarCellDelegate;
@property(strong, nonatomic) AdvertiseType *advertiseType;
@property(assign, nonatomic) NSInteger tabIndex;
@property(strong, nonatomic) IBOutlet UIButton *btnCategoryTypeName;
@property(strong, nonatomic) IBOutlet UIImageView *seperatorImage;
@property(strong, nonatomic) IBOutlet UIImageView *selectedTypeBarImage;
- (IBAction)btnCategoryTypeNameClicked:(id)sender;

@end
