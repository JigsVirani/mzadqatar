//
//  FileUploadEngine.h
//  MazadQatar
//
//  Created by samo on 3/24/13.
//  Copyright (c) 2013 Mohammed abdulfattah attia. All rights reserved.
//

#import "MKNetworkEngine.h"

@interface MkNetworkEngine : MKNetworkEngine
- (MKNetworkOperation *)postDataToServer:(NSDictionary *)params
                                    path:(NSString *)path;
@end
