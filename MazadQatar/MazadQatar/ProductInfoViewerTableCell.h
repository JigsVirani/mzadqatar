//
//  ProductInfoViewerTableCell.h
//  MazadQatar
//
//  Created by samo on 5/27/13.
//  Copyright (c) 2013 Mohammed abdulfattah attia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProductObject.h"

@interface ProductInfoViewerTableCell : UITableViewCell {
  ProductObject *productObject;
}
@property(strong, nonatomic) IBOutlet UITextView *productItemName;
@property(strong, nonatomic) IBOutlet UITextView *productPrice;
@property(strong, nonatomic) IBOutlet UITextView *productDescription;
@property(strong, nonatomic) IBOutlet UITextView *productPhoneNumber;
@property(strong, nonatomic) IBOutlet UITextView *productDate;

@property(strong, nonatomic) IBOutlet UIButton *totalViewsBtn;
- (IBAction)viewInWebsiteBtn:(UIButton *)sender;
- (void)setViewProductInfoUi:(ProductObject *)productObjectParam;
@end
