//
//  EnterTextViewController.m
//  Mzad Qatar
//
//  Created by Mohammed AbdulFattah on 1/7/15.
//  Copyright (c) 2015 Mohammed abdulfattah attia. All rights reserved.
//

#import "EnterTextViewController.h"
#import "DisplayUtility.h"

@interface EnterTextViewController ()

@end

@implementation EnterTextViewController

- (void)viewDidLoad {
  [super viewDidLoad];
    
    self.screenName = @"Enter Text On Image Dialogue";
  if ([DisplayUtility isPad]) {
    self.movementDistance = 50; // tweak as needed
  } else {
    self.movementDistance = 70; // tweak as needed
  }
    self.btnDone.layer.cornerRadius=20;
    self.btnDone.layer.borderWidth=1.0;
    self.btnDone.layer.borderColor=[UIColor colorWithRed:160/255.0 green:27/255.0 blue:88/255.0 alpha:1.0].CGColor;
  [self.textToBeWrittenField becomeFirstResponder];
}

- (void)didReceiveMemoryWarning {
  [super didReceiveMemoryWarning];
  // Dispose of any resources that can be recreated.
}

- (IBAction)closeDialogClicked:(id)sender {
  [self.view removeFromSuperview];
}

- (IBAction)doneClicked:(id)sender {
  [self.view removeFromSuperview];

  [self.delegate textEntered:self.textToBeWrittenField.text];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    // Prevent crashing undo bug – see note below.
    if(range.length + range.location > textField.text.length)
    {
        return NO;
    }
    
    NSUInteger newLength = [textField.text length] + [string length] - range.length;
    return (newLength > self.maxCharacterLimit) ? NO : YES;
}

@end
