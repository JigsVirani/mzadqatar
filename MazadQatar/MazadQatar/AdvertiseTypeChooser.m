//
//  AdvertiseTypeChooser.m
//  Mzad Qatar
//
//  Created by samo on 9/3/14.
//  Copyright (c) 2014 Mohammed abdulfattah attia. All rights reserved.
//

#import "AdvertiseTypeChooser.h"

@interface AdvertiseTypeChooser ()

@end

@implementation AdvertiseTypeChooser

- (void)viewDidLoad {
    
    self->screenName = @"Advertise Type Chooser";
  [super viewDidLoad];
    
  //UIImage *backgroundPattern = [UIImage imageNamed:@"bg.png"];
  //[self.furnishedTypeTable setBackgroundColor:[UIColor colorWithPatternImage:backgroundPattern]];

    [self.view setBackgroundColor:[UIColor blackColor]];
    
    if([LanguageManager currentLanguageIndex]==1){
        self.title = @"نوع الإعلان";
    }else{
        self.title = @"Advertise Type";
    }
    
  self.tableView.tableFooterView =
      [[UIView alloc] initWithFrame:CGRectZero];

  for (ProductObject *categoryObject in [SharedData getCategories]) {

    if ([categoryObject.productId isEqualToString:selectedCategoryId]) {
      self.advertiseTypesArray = categoryObject.productAdvertiseTypes;
      break;
    }
  }
    //[self.navigationController.navigationBar.topItem setTitle:NSLocalizedString(@"BACK", nil)];
}

- (NSInteger)tableView:(UITableView *)tableView
    numberOfRowsInSection:(NSInteger)section {
  return self.advertiseTypesArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
  UITableViewCell *cell = nil;

  AdvertiseType *advertiseType =
      [self.advertiseTypesArray objectAtIndex:indexPath.row];

  cell = [tableView dequeueReusableCellWithIdentifier:@"advertiseTYpeIdentifier"
                                         forIndexPath:indexPath];

  if (advertiseType.advertiseTypeId == selectedAdvertiseTypeId) {
    cell.accessoryType = UITableViewCellAccessoryCheckmark;
  } else {
    cell.accessoryType = UITableViewCellAccessoryNone;
  }

  cell.backgroundColor = [UIColor clearColor];

  AdvertiseType *advertiseTypeObject =
      [self.advertiseTypesArray objectAtIndex:indexPath.row];
  cell.textLabel.text = advertiseTypeObject.advertiseTypeName;

  return cell;
}

- (CGFloat)tableView:(UITableView *)tableView
    heightForRowAtIndexPath:(NSIndexPath *)indexPath {
  return 46;
}

- (void)tableView:(UITableView *)tableView
    didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
  AdvertiseType *advertiseType =
      [self.advertiseTypesArray objectAtIndex:indexPath.row];

  [delegate AdvertiseTypeChoosed:advertiseType.advertiseTypeId
      andAdvertiseTypeChooserName:advertiseType.advertiseTypeName];

  [self.navigationController popViewControllerAnimated:YES];
}

- (void)
    setAdvertiseTypeParamsWithCatId:(NSString *)categoryId
         andSelectedAdvertiseTypeId:(NSString *)currentSelectedAdvertiseTypeId
                        andDelegate:
                            (id<AdvertiseTypeChooserDelegate>)delegateParam {
  selectedCategoryId = categoryId;
  selectedAdvertiseTypeId = currentSelectedAdvertiseTypeId;
  delegate = delegateParam;
}


@end
