//
//  SoryByObject.m
//  Mzad Qatar
//
//  Created by samo on 8/27/14.
//  Copyright (c) 2014 Mohammed abdulfattah attia. All rights reserved.
//

#import "SortByObject.h"

@implementation SortByObject

NSString *SORT_BY_ID_KEY = @"SORT_BY_ID_KEY";
NSString *SORT_BY_NAMES_KEY = @"SORT_BY_NAMES_KEY";
NSString *SORT_BY_IMAGE_KEY = @"SORT_BY_Image_KEY";

- (void)encodeWithCoder:(NSCoder *)aCoder {

  [aCoder encodeObject:self.sortByFilterId forKey:SORT_BY_ID_KEY];
  [aCoder encodeObject:self.SortByFilterName forKey:SORT_BY_NAMES_KEY];
  [aCoder encodeObject:self.SortByFilterImageUrl forKey:SORT_BY_IMAGE_KEY];
}

- (id)initWithCoder:(NSCoder *)aDecoder {
  self = [super init];
  if (!self) {
    return nil;
  }

  self.sortByFilterId = [aDecoder decodeObjectForKey:SORT_BY_ID_KEY];
  self.SortByFilterName = [aDecoder decodeObjectForKey:SORT_BY_NAMES_KEY];
  self.SortByFilterImageUrl = [aDecoder decodeObjectForKey:SORT_BY_IMAGE_KEY];

  return self;
}
@end
