//
//  ProductInfoViewerTableCell.h
//  MazadQatar
//
//  Created by samo on 5/27/13.
//  Copyright (c) 2013 Mohammed abdulfattah attia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProductObject.h"
#import "ServerManager.h"
#import "SharedViewManager.h"
#import "SocialShareUtility.h"
#import "ReportItemViewController.h"

@interface NewProductInfoViewerTableCell : UITableViewCell<ServerManagerResponseDelegate,
ReportItemViewControllerDelegate> {
    ProductObject *productObject;
    UIViewController *parentView;
    UITableView *tableView;
    SocialShareUtility *socialShareUtility;
}
@property(strong, nonatomic) IBOutlet UILabel *productItemName;
@property(strong, nonatomic) IBOutlet UILabel *productPricez;
@property(strong, nonatomic) IBOutlet UILabel *totalViewsBtn;
@property(strong, nonatomic) IBOutlet UILabel *userName;
@property(strong, nonatomic) IBOutlet UITextView *userNumber;
@property(strong, nonatomic) IBOutlet UILabel *productDate;
@property(strong, nonatomic) IBOutlet UIImageView *userAvtarImage;
@property(strong, nonatomic) IBOutlet UIImageView *isCompany;
@property(strong, nonatomic) IBOutlet UILabel *commentsCountLabel;
@property(strong, nonatomic) IBOutlet UIActivityIndicatorView * loadingIndicator;
@property(strong, nonatomic) IBOutlet UIActivityIndicatorView * loadAllDetailIndicator;
- (IBAction)viewInWebsiteBtn:(UIButton *)sender;
- (void)setViewProductInfoUi:(ProductObject *)productObjectParam;
@property(strong, nonatomic) IBOutlet UIButton *viewInWebSite;
@property(strong, nonatomic) IBOutlet UIButton *addToFavoriteBtn;
@property(strong, nonatomic) IBOutlet UIButton *reportButton;

@property(strong, nonatomic) IBOutlet NSLayoutConstraint *bottomViewHeight;
@property(strong, nonatomic) IBOutlet NSLayoutConstraint *accessoryBtnHeight;
@property(strong, nonatomic) IBOutlet NSLayoutConstraint *accessoryBtnWidth;

- (IBAction)addToFavoriteBtnClicked:(id)sender;
- (IBAction)reportBtnClicked:(id)sender;

@property(strong, nonatomic)
IBOutlet UIActivityIndicatorView *addToFavoriteIndicator;
@property(strong, nonatomic) IBOutlet UIActivityIndicatorView *reportIndicator;
@property(strong, nonatomic) IBOutlet UITextField *resultTextField;

- (void)setProductObject:(ProductObject *)productObjectParam
            InParentView:(UIViewController *)parentViewParam
             inTableView:(UITableView *)tableViewParam;
-(IBAction)GotoProfileScreen:(id)sender;



@property (weak, nonatomic)IBOutlet UIButton *callBtn,*chatBtn,*emailBtn,*numberCopyBtn;
@property (weak, nonatomic)IBOutlet UILabel *byLbl;


@property (weak, nonatomic) IBOutlet UIStackView *iPhoneStackView;
@property (weak, nonatomic) IBOutlet UIStackView *iPadStackView;

@end
