//
//  Category.m
//  Mzad Qatar
//
//  Created by Waris Ali on 04/06/2014.
//  Copyright (c) 2014 Mohammed abdulfattah attia. All rights reserved.
//

#import "CategoryObject.h"

@implementation CategoryObject


NSString *PRODUCT_WEBSITE_URL_KEY = @"PRODUCT_WEBSITE_URL_KEY";
NSString *PRODUCT_ID_KEY = @"PRODUCT_ID_KEY";
NSString *CATEGORY_ID_KEY = @"CATEGORY_ID_KEY";
NSString *CATEGORY_NAME_KEY = @"CATEGORY_NAME_KEY";
NSString *CATEGORY_IMAGE_URL_KEY = @"CATEGORY_IMAGE_URL_KEY";
NSString *CATEGORY_SUBCATEGORIES_KEY = @"CATEGORY_SUBCATEGORIES_KEY";
NSString *CATEGORY_ADVERTISES_TYPE_KEY = @"CATEGORY_ADVERTISES_TYPE_KEY";
NSString *IS_CATEGORY = @"IS_CATEGORY";
NSString *SUBCAT_CATEGORY = @"SUBCAT_CATEGORY";
NSString *CATEGORY_FULL_IMAGE_WIDTH =@"CATEGORY_FULLWITH_URL_KEY";

- (void)encodeWithCoder:(NSCoder *)aCoder {
    
    [aCoder encodeObject:self.productWebsiteUrl forKey:PRODUCT_WEBSITE_URL_KEY];
    [aCoder encodeObject:self.productID forKey:PRODUCT_ID_KEY];
    [aCoder encodeObject:self.categoryID forKey:CATEGORY_ID_KEY];
    [aCoder encodeObject:self.categoryName forKey:CATEGORY_NAME_KEY];
    [aCoder encodeObject:self.categoryImageURL forKey:CATEGORY_IMAGE_URL_KEY];
    [aCoder encodeObject:self.isCategory forKey:IS_CATEGORY];
    [aCoder encodeObject:self.categorySubcategories
                  forKey:CATEGORY_SUBCATEGORIES_KEY];
    [aCoder encodeObject:self.productAdvertiseTypes
                  forKey:CATEGORY_ADVERTISES_TYPE_KEY];
    [aCoder encodeObject:self.categoryFullWidthImageURL forKey:CATEGORY_FULL_IMAGE_WIDTH];
    [aCoder encodeObject:self.subCategoryParent forKey:SUBCAT_CATEGORY];
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];
    if (!self) {
        return nil;
    }
    
    self.productWebsiteUrl = [aDecoder decodeObjectForKey:PRODUCT_WEBSITE_URL_KEY];
    self.productID = [aDecoder decodeObjectForKey:PRODUCT_ID_KEY];
    self.categoryID = [aDecoder decodeObjectForKey:CATEGORY_ID_KEY];
    self.categoryName = [aDecoder decodeObjectForKey:CATEGORY_NAME_KEY];
    self.categoryImageURL = [aDecoder decodeObjectForKey:CATEGORY_IMAGE_URL_KEY];
    self.isCategory = [aDecoder decodeObjectForKey:IS_CATEGORY];
    self.categorySubcategories =
    [aDecoder decodeObjectForKey:CATEGORY_SUBCATEGORIES_KEY];
    self.subCategoryParent = [aDecoder decodeObjectForKey:SUBCAT_CATEGORY];
    self.productAdvertiseTypes=[aDecoder decodeObjectForKey:CATEGORY_ADVERTISES_TYPE_KEY];
    self.categoryFullWidthImageURL=[aDecoder decodeObjectForKey:CATEGORY_FULL_IMAGE_WIDTH];
    return self;
}

@end
