//
//  SelectAdvertiseLanguageViewController.m
//  Mzad Qatar
//
//  Created by Mohammed AbdulFattah on 1/14/15.
//  Copyright (c) 2015 Mohammed abdulfattah attia. All rights reserved.
//

#import "SelectAdvertiseLanguageViewController.h"
#import "UserSetting.h"

@interface SelectAdvertiseLanguageViewController ()

@end

@implementation SelectAdvertiseLanguageViewController

- (void)viewDidLoad {
  [super viewDidLoad];
    
    self.screenName = @"Select Advertise Language Dialogue";
  // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
  [super didReceiveMemoryWarning];
  // Dispose of any resources that can be recreated.
}

- (IBAction)englishAndArabicChoosed:(id)sender {
  [UserSetting saveFilterViewAdvertiseLanguage:0];

  [self.view removeFromSuperview];
}

- (IBAction)englishChoosed:(id)sender {
  [UserSetting saveFilterViewAdvertiseLanguage:1];

  [self.view removeFromSuperview];
}

- (IBAction)arabicChoosed:(id)sender {
  [UserSetting saveFilterViewAdvertiseLanguage:2];

  [self.view removeFromSuperview];
}

@end
