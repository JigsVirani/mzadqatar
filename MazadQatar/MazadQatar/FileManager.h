//
//  FileManager.h
//  Darefeud
//
//  Created by Waris Ali on 18/02/2013.
//  Copyright (c) 2013 auradynamic. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FileManager : NSObject

// Initting
+ (void)initPlistByName:(NSString *)fileName;

// Getting
+ (NSMutableDictionary *)getDictFromFileName:(NSString *)fileName;
+ (NSMutableArray *)getArrayFromFileName:(NSString *)fileName;
+ (NSString *)getValueForKey:(NSString *)key fromPlist:(NSString *)fileName;

// Writing
+ (void)writeArray:(NSArray *)array toPlist:(NSString *)fileName;
+ (void)writeDictionary:(NSDictionary *)dict toPlist:(NSString *)fileName;
@end