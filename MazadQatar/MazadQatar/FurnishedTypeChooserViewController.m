//
//  FurnishedTypeChooserViewController.m
//  Mzad Qatar
//
//  Created by samo on 9/2/14.
//  Copyright (c) 2014 Mohammed abdulfattah attia. All rights reserved.
//

#import "FurnishedTypeChooserViewController.h"

@interface FurnishedTypeChooserViewController ()

@end

@implementation FurnishedTypeChooserViewController

NSString *AllTypesfurnishedIdentifier = @"AllTypesFurnishedIdentifier";
NSString *furnishedIdentifier = @"furnishedIdentifier";
NSString *semiFurnishedIdentifier = @"semiFurnishedIdentifier";
NSString *notFurnishedIdentifier = @"notFurnishedIdentifier";

NSString *AllTypesfurnishedValue = @"0";
NSString *furnishedValue = @"1";
NSString *semiFurnishedValue = @"2";
NSString *notFurnishedValue = @"3";

- (void)viewDidLoad {
    
    self->screenName = @"Furnished Type Chooser";

    [super viewDidLoad];
    //UIImage *backgroundPattern = [UIImage imageNamed:@"bg.png"];
    //[self.furnishedTypeTable setBackgroundColor:[UIColor colorWithPatternImage:backgroundPattern]];
    
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    //[self.navigationController.navigationBar.topItem setTitle:NSLocalizedString(@"BACK", nil)];
    
    if([LanguageManager currentLanguageIndex]==1){
        self.title = @"مفروشة، مد، زود";
    }else{
        self.title = @"Furnished";
    }
}

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section {
    if(isAddAdvertise)
    {
        return 3;
    }
    else
    {
        return 4;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = nil;
    NSString *currentFurnishedValue = nil;
    
    if(isAddAdvertise==true)
    {
        // comments of product
        if (indexPath.row == 0) {
            cell = [tableView dequeueReusableCellWithIdentifier:furnishedIdentifier
                                                   forIndexPath:indexPath];
            currentFurnishedValue = furnishedValue;
        } else if (indexPath.row == 1) {
            cell = [tableView dequeueReusableCellWithIdentifier:semiFurnishedIdentifier
                                                   forIndexPath:indexPath];
            currentFurnishedValue = semiFurnishedValue;
        } else if (indexPath.row == 2) {
            cell = [tableView dequeueReusableCellWithIdentifier:notFurnishedIdentifier
                                                   forIndexPath:indexPath];
            currentFurnishedValue = notFurnishedValue;
        }
    }
    else
    {
        if (indexPath.row == 0) {
            cell = [tableView dequeueReusableCellWithIdentifier:AllTypesfurnishedIdentifier
                                                   forIndexPath:indexPath];
            currentFurnishedValue = AllTypesfurnishedValue;
        }
        else if (indexPath.row == 1) {
            cell = [tableView dequeueReusableCellWithIdentifier:furnishedIdentifier
                                                   forIndexPath:indexPath];
            currentFurnishedValue = furnishedValue;
        } else if (indexPath.row == 2) {
            cell = [tableView dequeueReusableCellWithIdentifier:semiFurnishedIdentifier
                                                   forIndexPath:indexPath];
            currentFurnishedValue = semiFurnishedValue;
        } else if (indexPath.row == 3) {
            cell = [tableView dequeueReusableCellWithIdentifier:notFurnishedIdentifier
                                                   forIndexPath:indexPath];
            currentFurnishedValue = notFurnishedValue;
        }
        
    }
    if ([selectedFurnishedTypeID isEqualToString:currentFurnishedValue]) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    } else {
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    cell.backgroundColor = [UIColor clearColor];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView
heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 46;
}

- (void)tableView:(UITableView *)tableView
didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    NSString *furnishedName = cell.textLabel.text;
    
    if(isAddAdvertise==true)
    {
        if (indexPath.row == 0) {
            selectedFurnishedTypeID = furnishedValue;
            
        } else if (indexPath.row == 1) {
            selectedFurnishedTypeID = semiFurnishedValue;
        } else {
            selectedFurnishedTypeID = notFurnishedValue;
        }
    }
    else
    {
        if (indexPath.row ==0) {
            selectedFurnishedTypeID = AllTypesfurnishedValue;
            
        }
        else if (indexPath.row ==1) {
            selectedFurnishedTypeID = furnishedValue;
            
        } else if (indexPath.row == 2) {
            selectedFurnishedTypeID = semiFurnishedValue;
        } else {
            selectedFurnishedTypeID = notFurnishedValue;
        }
    }
    
    [delegate furnishedTypeChoosedInList:selectedFurnishedTypeID
                        andFurnishedName:furnishedName];
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)setFurnishedParamsWithFurnishedTypeId:(NSString *)furnishedTypeIdParam
                                  andDelegate:(id<FurnishedTypeChooserDelegate>)
delegateParam andIsAddAdvertise:(BOOL)isAddAdvertiseParam {
    selectedFurnishedTypeID = furnishedTypeIdParam;
    delegate = delegateParam;
    isAddAdvertise=isAddAdvertiseParam;
}



@end
