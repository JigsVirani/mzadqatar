//
//  FilterPropertyChooser.h
//  Mzad Qatar
//
//  Created by samo on 9/7/14.
//  Copyright (c) 2014 Mohammed abdulfattah attia. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "CitiesViewController.h"
#import "SubCategoryChooser.h"
#import "DialogManager.h"
#import "RegionListChooser.h"
#import "FurnishedTypeChooserViewController.h"
#import "TextFieldViewController.h"

@class FilterPropertyChooser;
@protocol FilterPropertyChooserDelegate <NSObject>
@required
- (void)propertyFiltersChoosedWithProductObject:
        (ProductObject *)productObjectParam;
@required
@end

@interface FilterPropertyChooser
    : TextFieldViewController <
          SubCategoryChooserDelegate, RegionViewControllerDelegate,
          FurnishedTypeChooserDelegate, UITextFieldDelegate> {
  NSString *currentCategoryId;
  NSString *currentSubCategoryId;
  NSString *currentSubCategoryIndex;
  NSString *currentCityIdSelected;
  NSString *currentCityIndexSelected;
  NSString *currentRegionId;
  int currentRegionIndexSelected;
  NSString *currentFurnishedId;
  int *currentFurnishedIndex;
}

@property(strong) ProductObject *productObject;
@property(nonatomic, strong) id<FilterPropertyChooserDelegate> delegate;

@property(strong, nonatomic) IBOutlet UIButton *citiesChooserBtn;
@property(strong, nonatomic) IBOutlet UIButton *regionChooserBtn;
@property(strong, nonatomic) IBOutlet UIButton *subCateogryChooserBtn;
@property(strong, nonatomic) IBOutlet UIButton *furnishedChooserBtn;
@property(strong, nonatomic) IBOutlet UITextField *priceFromTextField;
@property(strong, nonatomic) IBOutlet UITextField *priceToTextField;
@property(strong, nonatomic) IBOutlet UITextField *roomsFromTextFIeld;
@property(strong, nonatomic) IBOutlet UITextField *roomsToTextFIeld;

@property(strong, nonatomic) IBOutlet UIImageView *citiesChooserImg;
@property(strong, nonatomic) IBOutlet UIImageView *regionChooserImg;
@property(strong, nonatomic) IBOutlet UIImageView *subCateogryChooserImg;
@property(strong, nonatomic) IBOutlet UIImageView *furnishedChooserImg;

- (IBAction)searchBtnClicked:(id)sender;

@end
