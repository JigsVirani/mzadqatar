//
//  TextViewTableCell.m
//  MazadQatar
//
//  Created by samo on 4/21/13.
//  Copyright (c) 2013 Mohammed abdulfattah attia. All rights reserved.
//

#import "TextViewTableCell.h"
#import "DisplayUtility.h"

@implementation TextViewTableCell

static const CGFloat KEYBOARD_ANIMATION_DURATION = 0.3;
static const CGFloat MINIMUM_SCROLL_FRACTION = 0.3;
static const CGFloat MAXIMUM_SCROLL_FRACTION = 0.7;

static const CGFloat PORTRAIT_KEYBOARD_HEIGHT = 216;
static const CGFloat LANDSCAPE_KEYBOARD_HEIGHT = 162;

static const CGFloat PORTRAIT_KEYBOARD_HEIGHT_IPAD = 265;
static const CGFloat LANDSCAPE_KEYBOARD_HEIGHT_IPAD = 360;

- (id)initWithStyle:(UITableViewCellStyle)style
    reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

- (void)textViewDidBeginEditing:(UITextView *)textView {
    
    CGRect textFieldRect =
    [parentView.view.window convertRect:textView.bounds fromView:textView];
    CGRect viewRect = [parentView.view.window convertRect:parentView.view.bounds
                                                 fromView:parentView.view];
    
    CGFloat midline = textFieldRect.origin.y + 0.5 * textFieldRect.size.height;
    CGFloat numerator = midline - viewRect.origin.y -
    MINIMUM_SCROLL_FRACTION * viewRect.size.height;
    CGFloat denominator = (MAXIMUM_SCROLL_FRACTION - MINIMUM_SCROLL_FRACTION) *
    viewRect.size.height;
    CGFloat heightFraction = numerator / denominator;
    
    if (heightFraction < 0.0) {
        
        heightFraction = 0.0;
        
    } else if (heightFraction > 1.0) {
        
        heightFraction = 1.0;
    }
    
    UIInterfaceOrientation orientation =
    [[UIApplication sharedApplication] statusBarOrientation];
    
    if ([DisplayUtility isPad]) {
        
        if (orientation == UIInterfaceOrientationPortrait ||
            orientation == UIInterfaceOrientationPortraitUpsideDown) {
            
            animatedDistance = floor(PORTRAIT_KEYBOARD_HEIGHT_IPAD * heightFraction);
            
        } else {
            
            animatedDistance = floor(LANDSCAPE_KEYBOARD_HEIGHT_IPAD * heightFraction);
        }
    } else {
        if (orientation == UIInterfaceOrientationPortrait ||
            orientation == UIInterfaceOrientationPortraitUpsideDown) {
            
            animatedDistance = floor(PORTRAIT_KEYBOARD_HEIGHT * heightFraction);
            
        } else {
            
            animatedDistance = floor(LANDSCAPE_KEYBOARD_HEIGHT * heightFraction);
        }
    }
    CGRect viewFrame = parentView.view.frame;
    viewFrame.origin.y -= animatedDistance;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    
    [parentView.view setFrame:viewFrame];
    
    [UIView commitAnimations];
}

- (void)textViewDidEndEditing:(UITextView *)textView {
    
    if (isRotating == false) {
        CGRect viewFrame = parentView.view.frame;
        viewFrame.origin.y += animatedDistance;
        
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationBeginsFromCurrentState:YES];
        [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
        
        [parentView.view setFrame:viewFrame];
        [UIView commitAnimations];
    } else {
        isRotating = false;
    }
}



//- (BOOL)textView:(UITextView *)textView
//shouldChangeTextInRange:(NSRange)range
// replacementText:(NSString *)text {
//    if ([text isEqualToString:@"\n"]) {
//        [textView resignFirstResponder];
//        return NO;
//    }
//    return YES;
//}


- (void)createInputAccessoryView {
    
    CGSize screenSize = [DisplayUtility getScreenSize];
    
    if (inputAccView == nil) {
        inputAccView = [[UIView alloc]
                        initWithFrame:CGRectMake(10.0, 0.0, screenSize.width, 40.0)];
        // Set the view’s background color. We’ ll set it here to gray. Use any
        // color you want.
        [inputAccView setBackgroundColor:[UIColor colorWithRed:(87.0 / 255.0)
                                                         green:(95.0 / 255.0)
                                                          blue:(109.0 / 255.0)
                                                         alpha:0.8]];
        
        NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
        [nc addObserver:self
               selector:@selector(orientationChanged:)
                   name:UIApplicationDidChangeStatusBarOrientationNotification
                 object:nil];
    }
    
    if (btnDone == nil) {
        btnDone = [UIButton buttonWithType:UIButtonTypeCustom];
        [btnDone setFrame:CGRectMake(screenSize.width - 80, 3, 80.0f, 40.0f)];
        [btnDone setTitle:NSLocalizedString(@"DONE", nil)
                 forState:UIControlStateNormal];
        
        UIImage *backgroundPattern = [UIImage imageNamed:@"btn_advertise.png"];
        [btnDone setBackgroundImage:backgroundPattern
                           forState:UIControlStateNormal];
        
        // [btnDone setBackgroundColor:[UIColor blackColor]];
        [btnDone setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [btnDone addTarget:self
                    action:@selector(doneTyping)
          forControlEvents:UIControlEventTouchUpInside];
        
        [inputAccView addSubview:btnDone];
    }
}

- (void)textViewDidChange:(UITextView *)textView
{
    textView.enablesReturnKeyAutomatically =NO;
//    CGFloat fixedWidth = textView.frame.size.width;
//    CGSize newSize = [textView sizeThatFits:CGSizeMake(fixedWidth, MAXFLOAT)];
//    NSLog(@"%f", newSize.height);
//    if (newSize.height < 50)
//    {
//        
//        CGRect newFrame = textView.frame;
//        newFrame.size = CGSizeMake(fmaxf(newSize.width, fixedWidth), newSize.height);
//        
//        textView.frame = newFrame;
//    }
//    NSLog(@"%f" ,textView.frame.size.height);
//    NSLog(@"%@", textView.text);
}


@end
