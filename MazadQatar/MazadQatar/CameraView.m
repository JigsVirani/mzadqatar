//
//  CameraViewViewController.m
//  MazadQatar
//
//  Created by samo on 3/24/13.
//  Copyright (c) 2013 Mohammed abdulfattah attia. All rights reserved.
//

#import "CameraView.h"
#import <AssetsLibrary/ALAssetsLibrary.h>

@interface CameraView ()

@end

@implementation CameraView

@synthesize imageCaptureDelegate;

//- (void)viewDidLoad
//{
//    [super viewDidLoad];
//
//    UIImage * backgroundPattern = [UIImage imageNamed:@"bg.png"];
//    [self.view setBackgroundColor:[UIColor
//    colorWithPatternImage:backgroundPattern]];
//
//    [self startCameraCapturing];
//}

- (void)startCameraCapturing:(UIViewController *)parentViewParam;
{
  parentView = parentViewParam;

  //    UIImagePickerController *picker = [[UIImagePickerController alloc]
  //    init];
  //    picker.delegate = self;
  //    if ([UIImagePickerController
  //    isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
  //        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
  //    }
  //    picker.allowsEditing = YES;
  //    double delayInSeconds = 0.1;
  //    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW,
  //    delayInSeconds * NSEC_PER_SEC);
  //    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
  //        [parentView presentModalViewController:picker animated:YES];
  //    });
  //
  imagePicker = [[UIImagePickerController alloc] init];
  imagePicker.delegate = self;
  if ([UIImagePickerController
          isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
    imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
  }
  imagePicker.allowsEditing = YES;
  [parentView presentViewController:imagePicker animated:YES completion:nil];
}

- (void)imagePickerController:(UIImagePickerController *)picker
    didFinishPickingMediaWithInfo:(NSDictionary *)info {
  UIImage *image = [info objectForKey:UIImagePickerControllerEditedImage];
  if (!image)
    image = [info objectForKey:UIImagePickerControllerOriginalImage];
  if (!image)
    return;

  ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
  NSDictionary *metadata =
      [info objectForKey:UIImagePickerControllerMediaMetadata];
  [library writeImageToSavedPhotosAlbum:[image CGImage]
                               metadata:metadata
                        completionBlock:nil];

  [imagePicker dismissViewControllerAnimated:YES completion:nil];

  NSData *data = UIImageJPEGRepresentation(image, 0.40);

  [self.imageCaptureDelegate
      didFinishPickingMediaWithInfo:[UIImage imageWithData:data]];
}

//- (void)useImage:(UIImage *)image {
//    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
//
//    // Create a graphics image context
//    CGSize newSize = CGSizeMake(320, 480);
//    UIGraphicsBeginImageContext(newSize);
//    // Tell the old image to draw in this new context, with the desired
//    // new size
//    [image drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
//    // Get the new image from the context
//    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
//    // End the context
//    UIGraphicsEndImageContext();
//
//    //[userPhotoView setImage:newImage];
//    [self.imageCaptureDelegate didFinishPickingMediaWithInfo:image
//    withImageToDraw:image];
//
//    [pool release];
//}

//- (void)didReceiveMemoryWarning
//{
//    [super didReceiveMemoryWarning];
//    // Dispose of any resources that can be recreated.
//}

@end
