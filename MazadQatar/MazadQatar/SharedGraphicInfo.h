//
//  SharedInfo.h
//  MazadQatar
//
//  Created by samo on 3/5/13.
//  Copyright (c) 2013 Mohammed abdulfattah attia. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ProductObject.h"

@interface SharedGraphicInfo : NSObject

+ (NSMutableArray *)getcurrentUploadingProductList;
+ (void)addToUploadingProductList:(ProductObject *)productObject;

@end
