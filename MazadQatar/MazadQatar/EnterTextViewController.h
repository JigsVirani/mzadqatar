//
//  EnterTextViewController.h
//  Mzad Qatar
//
//  Created by Mohammed AbdulFattah on 1/7/15.
//  Copyright (c) 2015 Mohammed abdulfattah attia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TextFieldViewController.h"

@protocol EnterTextViewControllerDelegate <NSObject>
- (void)textEntered:(NSString *)text;
@end

@interface EnterTextViewController
    : TextFieldViewController <UITextFieldDelegate>

- (IBAction)closeDialogClicked:(id)sender;
- (IBAction)doneClicked:(id)sender;
@property(strong, nonatomic) IBOutlet UITextField *textToBeWrittenField;
@property (weak, nonatomic) IBOutlet UIButton *btnDone;

@property(nonatomic, unsafe_unretained)
    id<EnterTextViewControllerDelegate> delegate;
@property int maxCharacterLimit;
@end
