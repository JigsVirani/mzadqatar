//
//  SharedViewManager.m
//  MazadQatar
//
//  Created by samo on 4/3/13.
//  Copyright (c) 2013 Mohammed abdulfattah attia. All rights reserved.
//

#import "SharedViewManager.h"
#import "CategoryProducts.h"
#import "ProductDetailsViewController.h"

@implementation SharedViewManager

+ (void)showLoginScreen:(UIViewController *)view {
    [view performSegueWithIdentifier:[self registerSegue] sender:self];
}

+ (void)showAddAdvertiseScreen:(UIViewController *)view
{
    [view performSegueWithIdentifier:[self addAdvertisSegue] sender:self];
}

+ (void)showCategoryProductsWithSearch:(UIViewController *)view
                          andProductId:(NSString *)productId
                          andSearchStr:(NSString *)searchStr
                      andSubCategoryId:(NSString *)subcategoryServerId
                    andSubCategoryName:(NSString *)subCategoryName
                           andSortById:(NSString *)sortById
                         andSortByName:(NSString *)sortByName
                        andsortByIndex:(int)sortByIndex
                         andTabBarName:(NSMutableArray *)productAdvertiseTypes andDefaultTabSelected:(int)defautlTabSelected;

{
    
    CategoryProducts *categoryProducts = [[SharedData getCurrentStoryBoard]
                                          instantiateViewControllerWithIdentifier:@"CategoryProducts"];
    
    [categoryProducts setCategoryProductsCategoryId:productId
                                   andSubcategoryId:subcategoryServerId
                                 andSubcategoryName:subCategoryName];
    [categoryProducts setSearchString:searchStr];
    [categoryProducts setSoryById:sortById];
    [categoryProducts setSoryByName:sortByName];
    [categoryProducts setSortByIndex:sortByIndex];
    [categoryProducts setTabBarNames: productAdvertiseTypes];
    [categoryProducts setCurrentCategoryTypeBarIndex:defautlTabSelected];
    
    [view.navigationController pushViewController:categoryProducts animated:YES];
}

+ (void)showProductDetailView:(NSString *)productId fromView:(UIViewController*)view {
    
    ProductDetailsViewController *controller = [[SharedData getCurrentStoryBoard]
                                                instantiateViewControllerWithIdentifier:@"ProductDetailsViewController"];
    [controller setProductId:productId];
    
    [view.navigationController pushViewController:controller animated:NO];
}

+ (NSString *)successAddAdvertiseSugeue {
    return @"successAddProduct";
}

+ (NSString *)successUserProductSugeue {
    return @"successUserProducts";
}

+ (NSString *)registerSegue {
    return @"RegisterSegue";
}

+ (NSString *)chooseAddAdvertiseLanguageSegue {
    return @"ChooseAddAdvertiseLanguageSegue";
}

+ (NSString *)addAdvertisSegue {
    return @"addAdvertise";
}

+(UIImage *)writeOnImageTitle:(NSString *)title
                andDescription:(NSString *)description
                     andNumber:(NSString *)number
                       onImage:(UIImage *)image {
    CGSize size = CGSizeMake(image.size.width, image.size.height);
    UIGraphicsBeginImageContextWithOptions(size, YES, 0.0);
    
    // draw base image
    [image drawInRect:CGRectMake(0, 0, size.width, size.height)];
    
    // white background
    UIImage *whiteBackground = [UIImage imageNamed:@"share_background_White"];
    float whiteBackgroundYPostion = size.height - whiteBackground.size.height;
    [whiteBackground drawAtPoint:CGPointMake(0, whiteBackgroundYPostion)];
    
    // gray backgroud
    UIImage *grayBackground = [UIImage imageNamed:@"share_background_gray"];
    float grayBackgroundYPosition =
    size.height - whiteBackground.size.height - grayBackground.size.height;
    [grayBackground drawAtPoint:CGPointMake(0, grayBackgroundYPosition)];
    
    // mzad logo
    UIImage *mzadLogo = [UIImage imageNamed:@"share_mzad_logo"];
    float mzadLogoXposition = 10;
    float mzadLogoYposition = whiteBackgroundYPostion + 8;
    [mzadLogo drawAtPoint:CGPointMake(mzadLogoXposition, mzadLogoYposition)];
    
    // product title
    UIFont *titleFont = [UIFont systemFontOfSize:26.0];
    NSDictionary *titleAttrsDictionary = [NSDictionary
                                          dictionaryWithObjectsAndKeys:titleFont, NSFontAttributeName,
                                          [NSNumber numberWithFloat:1.0],
                                          NSBaselineOffsetAttributeName,
                                          [UIColor colorWithRed:(152 / 255.0)
                                                          green:(41 / 255.0)
                                                           blue:(92 / 255.0)
                                                          alpha:1],
                                          NSForegroundColorAttributeName, nil];
    [title drawAtPoint:CGPointMake(mzadLogoXposition + mzadLogo.size.width + 10,
                                   mzadLogoYposition + 5)
        withAttributes:titleAttrsDictionary];
    
    // product title
    UIFont *numberfont = [UIFont systemFontOfSize:24.0];
    NSDictionary *numberAttrsDictionary = [NSDictionary
                                           dictionaryWithObjectsAndKeys:numberfont, NSFontAttributeName,
                                           [NSNumber numberWithFloat:1.0],
                                           NSBaselineOffsetAttributeName,
                                           [UIColor colorWithRed:(83 / 255.0)
                                                           green:(83 / 255.0)
                                                            blue:(83 / 255.0)
                                                           alpha:1],
                                           NSForegroundColorAttributeName, nil];
    [number drawAtPoint:CGPointMake(mzadLogoXposition + mzadLogo.size.width + 10,
                                    mzadLogoYposition + 40)
         withAttributes:numberAttrsDictionary];
    
    // description
    NSMutableParagraphStyle *style = [[NSMutableParagraphStyle alloc] init];
    [style setAlignment:NSTextAlignmentCenter];
    [style setLineBreakMode:NSLineBreakByWordWrapping];
    
    UIFont *descriptionfont = [UIFont systemFontOfSize:22.0];
    NSDictionary *descriptionAttrsDictionary = [NSDictionary
                                                dictionaryWithObjectsAndKeys:descriptionfont, NSFontAttributeName,
                                                [NSNumber numberWithFloat:1.0],
                                                NSBaselineOffsetAttributeName,
                                                [UIColor colorWithRed:(255 / 255.0)
                                                                green:(255 / 255.0)
                                                                 blue:(255 / 255.0)
                                                                alpha:1],
                                                NSForegroundColorAttributeName, style,
                                                NSParagraphStyleAttributeName, nil];
    [description drawInRect:CGRectMake(0, grayBackgroundYPosition + 10,
                                       grayBackground.size.width,
                                       grayBackground.size.height - 15)
             withAttributes:descriptionAttrsDictionary];
    
    // mzad download text
    UIImage *mzadDownloadText = [UIImage imageNamed:@"Share_download_text"];
    [mzadDownloadText
     drawAtPoint:CGPointMake(whiteBackground.size.width -
                             mzadDownloadText.size.width - 10,
                             whiteBackgroundYPostion + 5)];
    
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return img;
}

@end
