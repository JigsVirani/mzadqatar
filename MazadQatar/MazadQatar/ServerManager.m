//
//  ServerManager.m
//  MazadQatar
//
//  Created by samo on 3/4/13.
//  Copyright (c) 2013 Mohammed abdulfattah attia. All rights reserved.
//

#define kHostName @"mzadqatar.com"
//#define kHostNameSearch @"mzad.mzadqatar.com"

//#define kServerUrl @"http://mzadqatar.com/cdn/ios_new.php"
//#define kCategoryUrl kServerUrl@"?action=cat"
//#define kCategoryProductsUrl kServerUrl@"?action=getcategorydata"
//#define kGetProductDetailUrl kServerUrl@"?action=getproductdetails"
//#define kAddAdvertiseUrl kServerUrl@"?action=addadvertise"

/*#define kGetImagesToWriteTextOnItUrl                                           \
@"/cdn/ios_api1.php?action=getWriteOnImages"
//#define kGetCategoriesUrl @"/cdn/ios_new.php?action=cat"
#define kGetCategoriesUrl @"/cdn/ios_api1.php?action=cat"
#define kGetAllCategoriesAndSubcategories                                      \
@"/cdn/ios_api1.php?action=getCatNames"
#define kGetAllBannersForCategories                                      \
@"/cdn/api/getCategoriesBanners.php"
#define kGetCategoriesProductUrl @"/cdn/ios_api1.php?action=getcategorydata"

#define kGetPushNotification @"/cdn/simplepush1.php"

#define kAddadvertiseData @"/cdn/ios_api1.php?action=addadvertise"

#define kAddImageAdvertiseUrl @"/cdn/ios_api1.php?action=AddImageToProduct"
#define kRegisterUrl @"/cdn/ios_api1.php?action=register"
#define kActivationUrl @"/cdn/ios_api1.php?action=active"
#define kaddLikeToProductUrl @"/cdn/ios_api1.php?action=addLike"
#define kaddCommentToProductUrl @"/cdn/ios_api1.php?action=addComment"
#define kaddProductToFavorite @"/cdn/ios_api1.php?action=addUserFavorite"
#define kgetCommentsOfProductUrl @"/cdn/ios_api1.php?action=getComments"
#define kGetProductDetailUrl @"/cdn/ios_api1.php?action=getproductdetails"
#define kGetUserFavorites @"/cdn/ios_api1.php?action=getUserFavorite"
#define kGetUserProducts @"/cdn/ios_api1.php?action=getUserProducts"
#define kDeleteAdvertiseUrl @"/cdn/ios_api1.php?action=deleteAdvertise"
#define kDeleteUserFavorite @"/cdn/ios_api1.php?action=deleteFavorite"
#define kRefreshAdvertise @"/cdn/ios_api1.php?action=refreshAdvertise"
#define kaddDeviceToken @"/cdn/ios_api1.php?action=addDeviceToken"
#define kreportComment @"/cdn/ios_api1.php?action=reportComment"
#define kblockUser @"/cdn/ios_api1.php?action=blockUserFromMyProfile"
#define kRelatedAds @"/cdn/ios_api1.php?action=getRelatedAds"
#define kMoreInfo @"/cdn/ios_api1.php?action=getMorePropertiesForAdvertise"
#define kreportAdvertise @"/cdn/ios_api1.php?action=reportAdvertise"
#define kdeleteComment @"/cdn/ios_api1.php?action=deleteComment"
#define kGetNotification @"/cdn/ios_api1.php?action=getUserNotificaitons"
#define kStopNotification @"/cdn/ios_api1.php?action=stopUserPushNotificaitonsAds"
#define kUpdateUserProfile @"/cdn/ios_api1.php?action=updateUserProfile"
#define kGetSortByFilters @"/cdn/ios_api1.php?action=getSortByFilters"
#define kGetCitiesAndRegion @"/cdn/ios_api1.php?action=getCitiesAndRegions"
#define kSetNotificationRead @"/cdn/ios_api1.php?action=setNotificationRead"
#define kAllNotificationRead @"/cdn/ios_api1.php?action=setAllNotificationRead"
#define kDeleteNotification @"/cdn/ios_api1.php?action=deleteNotification"
#define kDeleteAllNotification @"/cdn/ios_api1.php?action=delelteAllNotification"
#define kCompanyRegisterUrl @"/cdn/ios_api1.php?action=registerAsCompany"
#define kGetCompaniesDirectoriesUrl @"/cdn/ios_api1.php?action=getCompaniesDirectory"*/
//=====  ios_api1  ====


//=====  ios_new1  ====
#define kGetImagesToWriteTextOnItUrl                                           \
@"/cdn/ios_new1.php?action=getWriteOnImages"
//#define kGetCategoriesUrl @"/cdn/ios_new.php?action=cat"
#define kGetCategoriesUrl @"/cdn/ios_new1.php?action=cat"
#define kGetAllCategoriesAndSubcategories                                      \
@"/cdn/ios_new1.php?action=getCatNames"
#define kGetAllCompanyCategory                                      \
@"/cdn/ios_new1.php?action=getCompaniesCategories"
#define kGetAllBannersForCategories                                      \
@"/cdn/api/getCategoriesBanners.php"
#define kGetCategoriesProductUrl @"/cdn/ios_new1.php?action=getcategorydata"
//#define kGetCategoriesProductUrl @"/cdn/api1/getcategorydata1.php"

#define kGetPushNotification @"/cdn/simplepush1.php"

#define kAddadvertiseData @"/cdn/ios_new1.php?action=addadvertise"

#define kAddImageAdvertiseUrl @"/cdn/ios_new1.php?action=AddImageToProduct"
#define kRegisterUrl @"/cdn/ios_new1.php?action=register"
#define kActivationUrl @"/cdn/ios_new1.php?action=active"
#define kaddLikeToProductUrl @"/cdn/ios_new1.php?action=addLike"
#define kaddCommentToProductUrl @"/cdn/ios_new1.php?action=addComment"
#define kaddProductToFavorite @"/cdn/ios_new1.php?action=addUserFavorite"
#define kgetCommentsOfProductUrl @"/cdn/ios_new1.php?action=getComments"
#define kGetProductDetailUrl @"/cdn/ios_new1.php?action=getproductdetails"
#define kAddViewToAdvertise @"/cdn/ios_new1.php?action=addViewToAdvertise"
#define kGetUserFavorites @"/cdn/ios_new1.php?action=getUserFavorite"
#define kGetUserProducts @"/cdn/ios_new1.php?action=getUserProducts"
#define kDeleteAdvertiseUrl @"/cdn/ios_new1.php?action=deleteAdvertise"
#define kDeleteUserFavorite @"/cdn/ios_new1.php?action=deleteFavorite"
#define kRefreshAdvertise @"/cdn/ios_new1.php?action=refreshAdvertise"
#define kaddDeviceToken @"/cdn/ios_new1.php?action=addDeviceToken"
#define kreportComment @"/cdn/ios_new1.php?action=reportComment"
#define kblockUser @"/cdn/ios_new1.php?action=blockUserFromMyProfile"
#define kRelatedAds @"/cdn/ios_new1.php?action=getRelatedAds"
#define kMoreInfo @"/cdn/ios_new1.php?action=getMorePropertiesForAdvertise"
#define kreportAdvertise @"/cdn/ios_new1.php?action=reportAdvertise"
#define kdeleteComment @"/cdn/ios_new1.php?action=deleteComment"
#define kGetNotification @"/cdn/ios_new1.php?action=getUserNotificaitons"
#define kStopNotification @"/cdn/ios_new1.php?action=stopUserPushNotificaitonsAds"
#define kUpdateUserProfile @"/cdn/ios_new1.php?action=updateUserProfile"
#define kGetSortByFilters @"/cdn/ios_new1.php?action=getSortByFilters"
#define kGetCitiesAndRegion @"/cdn/ios_new1.php?action=getCitiesAndRegions"
#define kSetNotificationRead @"/cdn/ios_new1.php?action=setNotificationRead"
#define kAllNotificationRead @"/cdn/ios_new1.php?action=setAllNotificationRead"
#define kDeleteNotification @"/cdn/ios_new1.php?action=deleteNotification"
#define kDeleteAllNotification @"/cdn/ios_new1.php?action=delelteAllNotification"
#define kCompanyRegisterUrl @"/cdn/ios_new1.php?action=registerAsCompany"
#define kGetCompaniesDirectoriesUrl @"/cdn/ios_new1.php?action=getCompaniesDirectory"
#define kGetTrendingSearch @"/search_trending"
#define kGetTextSearch @"/cdn/ios_new1.php?action=gettextsearch"

//=====  ios_new1  ====

#define kPage @"page"

#define kLanguage @"language"
#define kIsShowAllText @"isShowAllText"
#define kResolution @"resolution"

//#TODO: will be changed insh2allah
#define kPerPage @"numberperpage"
#define kPerPageCapital @"numberPerPage"

//#TODO: will be changed insh2allah
#define kLastUpdateTime @"lastupdatetime"

#define KLastUpdateTimeCapital @"lastUpdateTime"
#define kLastUpdateTimeForCategories @"lastUpdateTimeForCategories"
#define kLastUpdateTimeForCities @"lastUpdateTimeForCities"
#define kLastUpdateTimeForSortBy @"lastUpdateTimeForSortBy"

#define kshouldUpdateTime @"shouldUpdateTime"

#define kProducts @"products"
#define kProperties @"properties"
#define kCategories @"categories"
#define kNotifications @"notifications"
#define kImages @"images"
#define KuserDeviceLanguage @"userDeviceLanguage"
#define kProductId @"productId"
#define kMessage @"message"
#define kClickType @"clicktype"
#define kreportReadonID @"reportReadonID"
#define KuserCountryToBeBlocked @"userCountryToBeBlocked"
#define kReportReasonText @"ReportReasonText"
#define KuserNumberToBeBlocked @"userNumberToBeBlocked"
#define kProductName @"productName"
#define kProductNameEnglish @"productNameEnglish"
#define kProductNameArabic @"productNameArabic"
#define kProductDescription @"productDescription"
#define kProductDescriptionWithTitles @"productDescriptionWithTitles"
#define kProductDescriptionEnglish @"productDescriptionEnglish"
#define kProductDescriptionArabic @"productDescriptionArabic"
#define kProductLanguage @"lang"
#define kProductMainImage @"productMainImage"
#define kProductShareImage @"productShareImage"
#define kProductPrice @"productPrice"
#define kProductImages @"productImages"
#define kProductIsMainImage @"productIsMainImage"
#define kProductImage @"productImage"
#define kProductComments @"productComments"
#define KProductCommentCount @"commentCount"
#define KProductLikeCount @"likeCount"
#define kSearchStr @"searchStr"
#define kproductsLanguage @"productsLang"
#define Kcatid @"catid"
#define kProductCategoryId @"categoryId"
#define kProductCategoryName @"categoryName"
#define kproductSubCategoryId @"subCategoryId"
#define kproductSubCategoryName @"subCategoryName"
#define kProductFilterType @"productFilterType"

// company cell info
#define kNumberOfAds @"numberOfAds"
#define kUserAds @"userAds"


#define kProductManfactureYear @"manfactureYear"
#define kProductKm @"km"
#define kProductFurnishedId @"furnishedTypeId"
#define kProductFurnishedName @"furnishedTypeName"
#define kProductNumberOfRooms @"numberOfRooms"

#define ksortBy @"sortBy"
#define kCommentId @"commentId"
#define kIsAddAdvertise @"isAddAdvertise"
#define kIsEditAdvertise @"isEditAdvertise"
#define kadvertiseLanguage @"SetAdvertiseLanguage"
#define kIsResetImages @"isResetImages"
#define kisAdsSupported @"isAdsSupported"
#define kcountOfRow @"countOfRow"
#define kadvertiseResolution @"advertiseResolution"
#define kproductAdvertiseUrl @"productAdvertiseUrl"
#define kisShowAddCommentAndLikes @"isShowAddCommentAndLikes"
#define kisAd @"isAd"
#define KIsNewUpdate @"isNewUpdate"

//{"advertiseResolution":"medium","countOfRow":"2","isAdsSupported":"true”,”visitorCountryCode”:”00974”,”visitorNumber”:”66524147","lastUpdateTime":"0","page":"1","numberPerPage":"50","productsLang":"en"}
#define kVisitorCountryCode @"visitorCountryCode"
#define KVisitorNumber @"visitorNumber"
#define kUserCountryCode @"userCountry"
#define kUserCountry @"userCountryCode"
#define KUserCompany @"isCompany"
#define KUserLang @"lang"
#define KUserLong @"long"
#define KUserAddress @"locationName"
#define KUserNumber @"userNumber"
#define KUserName @"userName"
#define KUserEmail @"userEmail"
#define kUserPhoto @"userPhoto"
#define KUserProfileUrl @"userProfileUrl"

#define KCompanyNumber @"companyNumber"
#define KCompanyName @"companyName"
#define KCompanyEmail @"companyEmail"
#define kCompanyDetails @"companyDetails"


#define kUserSmallDescription @"userSmallDescription"
#define kUserNumberOfAds @"userNumberOfAds"
#define kUserAdvertiseOwner @"userNumberAdv"
#define kUserComment @"userComment"
#define kNumberOfViews @"numberOfViews"
#define KDateOfAdvertise @"advertiseTimeFormatted"
#define KDateOfAdvertiseInMilliSecond @"dateOfAdvertise"
#define kProductUrl @"productUrl"
#define kUserProfileLink @"userProfileLink"

// category advertise types
#define kcategoryAdvertiseTypes @"categoryAdvertiseTypes"
#define kcategoryAdvertiseTypesId @"categoryAdvertiseTypeId"
#define kcategoryAdvertiseTypesName @"categoryAdvertiseTypeName"
#define kcategoryAdvertiseTypeDefaultView @"categoryAdvertiseTypeDefaultView"

// coment
#define KUserCommentJson @"comment"
#define KUserCommentDate @"commentTimeFormatted"
#define KUserCommentCountryCode @"userCountryCode"
#define KUserCommentNumber @"userNumber"
#define KUserCommentName @"userName"
#define KUserCommentuserImageUrl @"userImageUrl"
#define kUserActivationCode @"user_registered_code"

#define kRequestStatus @"status"
#define kRequestStatusMsg @"statusMsg"

// add device token
#define KUserToken @"userToken"
#define KDeviceType @"deviceType"

// sort by filter
#define kSortByfilter @"sortByFilters"
#define kSortByfilterId @"filterId"
#define kSortByfilterName @"filterName"
#define kSortByfilterImage @"filterImage"

// get cities and regions
#define kCities @"cities"
#define kCityId @"cityId"
#define KCityName @"cityName"
#define KRegions @"regions"
#define KRegionId @"regionId"
#define KRegionName @"regionName"

// get all category
#define kProductWebsiteUrl @"productWebsiteUrl"
#define kCategoryId @"categoryId"
#define kCategoryName @"categoryName"
#define kCategorySmallImage @"categorySmallImage"
#define kSubCategories @"subCategories"
#define kSubCategoriesId @"subcategoryId"
#define kSubCategoriesName @"subcategoryName"
#define kSubCategoriesImage @"subCategoryImage"
#define kSubSubCategoryImageLongWidth  @"subsubCategoryImageLongWidth"
#define kSubSubCategories @"subsubCategories"
#define kSubSubCategoriesId @"subsubCategoryId"
#define kSubSubCategoriesName @"subsubCategoryName"
#define kSubSubCategoriesImageUrl @"subsubCategoryImage"
#define KManfactureYearFrom @"manfactureYearFrom"
#define KmanfactureYearTo @"manfactureYearTo"
#define KkmFrom @"kmFrom"
#define KKmTo @"kmTo"
#define kPriceFrom @"priceFrom"
#define KPriceTo @"priceTo"
#define KNumberOfRoomsFrom @"numberOfRoomsFrom"
#define KNumberOfRoomsTo @"numberOfRoomsTo"

#define REQUEST_ID_ADD_IMAGE_ADVERTISE @"addImageAdvertise"
#define kGetCategoryLoadingString @"Loading Cats"

// notifications
#define KNewNotificationCount @"newNotificationCount"
#define KNotificationInfo @"notificationInfo"
#define KIsRead @"isRead"
#define KNotificationId @"notificationId"
#define KNotificationImage @"notificationImage"
#define KNotificationTime @"notificationTime"

#define KAllNotificationRead @"readAllNotification"
#define KDeleteNotification @"deleteNotification"

// companies directory

#define kCompanies@"companies"

//Search
#define kTrendingKeywords @"keywords"
#define kSearchKey @"key"
#define kSearchId @"id"


#import "ServerManager.h"
#import "Base64.h"

static BOOL isGettingImages;
@implementation ServerManager

// static int categoryProductPageNumber = 1;
// static int categoryProductLastUpdateTime= 0;
// static int categoryProductNumberOfPages= 50;

// static int productCommentsPageNumber = 1;
// static int productCommentsNumberOfPages= 10;

// static int userProductsPageNumber = 1;
// static int userProductsNumberOfPages= 50;

// static int userFavoritePageNumber = 1;
// static int userFavoriteNumberOfPages= 50;

static MkNetworkEngine *mkNetworkEngine;
static MKNetworkOperation *mkNetworkOperation;

int viewSpaceInTheCell = 50;

/////////////////////////////////
+ (void)setHostName:(BOOL)isFromImage {
    isGettingImages = isFromImage;
}

//============================================================get all category
// items
+ (void)getAllCategories:(NSString *)language
            withDelegate:(id<ServerManagerResponseDelegate>)delegate
           withRequestId:(NSString *)requestId {
    
    NSArray *keys = [NSArray arrayWithObjects:kLanguage, nil];
    NSArray *objects = [NSArray arrayWithObjects:language, nil];
    
    NSMutableDictionary *jsonDictionary =
    [NSMutableDictionary dictionaryWithObjects:objects forKeys:keys];
    
    [self sendJsonToUrlUsingMk:kGetAllCategoriesAndSubcategories
                      withJson:jsonDictionary
                   AndDelegate:delegate
                 withRequestId:requestId
        withJsonParserSelector:@selector(parseAllCategories:
                                         withServerManagerResult:)
           withNumberOfRequest:0];
}
+ (void)getAllCompanyCategories:(NSString *)language
            withDelegate:(id<ServerManagerResponseDelegate>)delegate
           withRequestId:(NSString *)requestId {
    
    NSArray *keys = [NSArray arrayWithObjects:kLanguage, nil];
    NSArray *objects = [NSArray arrayWithObjects:language, nil];
    
    NSMutableDictionary *jsonDictionary =
    [NSMutableDictionary dictionaryWithObjects:objects forKeys:keys];
    
    [self sendJsonToUrlUsingMk:kGetAllCompanyCategory
                      withJson:jsonDictionary
                   AndDelegate:delegate
                 withRequestId:requestId
        withJsonParserSelector:@selector(parseAllCompanyCategories:
                                         withServerManagerResult:)
           withNumberOfRequest:0];
}
+ (ServerManagerResult *)parseAllCompanyCategories:(NSMutableDictionary *)json
                    withServerManagerResult:
(ServerManagerResult *)serverManagerResult {
    [serverManagerResult setJsonDict:json];
    return serverManagerResult;
}
+ (ServerManagerResult *)parseAllCategories:(NSMutableDictionary *)json
                    withServerManagerResult:
(ServerManagerResult *)serverManagerResult {
    NSMutableArray *categoriesArray = [[NSMutableArray alloc] init];
    
    NSArray *jsonObjects = [json objectForKey:kCategories];
    
    for (NSDictionary *categoryDictionary in jsonObjects) {
        
        CategoryObject *categoryObject = [[CategoryObject alloc] init];
        
        categoryObject.categoryID = [categoryDictionary objectForKey:kCategoryId];
        categoryObject.categoryName = [categoryDictionary objectForKey:kCategoryName];
        categoryObject.categoryImageURL = [categoryDictionary objectForKey:kCategorySmallImage];
        categoryObject.isCategory = @"YES";
        
        NSMutableArray *subcategoriesArray = [[NSMutableArray alloc] init];
        
        if ([[categoryDictionary valueForKey:kcategoryAdvertiseTypes]
             isKindOfClass:[NSMutableArray class]]||[[categoryDictionary valueForKey:kcategoryAdvertiseTypes]
                                                     isKindOfClass:[NSArray class]]) {
            
            NSMutableArray *categoryAdvertiseTypesArray = [[NSMutableArray alloc] init];
            
            for (NSDictionary *regionDictionary in [categoryDictionary valueForKey:kcategoryAdvertiseTypes]) {
                
                AdvertiseType *advertiseType = [[AdvertiseType alloc] init];
                
                advertiseType.advertiseTypeId = [regionDictionary valueForKey:kcategoryAdvertiseTypesId];
                advertiseType.advertiseTypeName = [regionDictionary valueForKey:kcategoryAdvertiseTypesName];
                advertiseType.advertiseTypeDefaultView = [regionDictionary valueForKey:kcategoryAdvertiseTypeDefaultView];
                
                [categoryAdvertiseTypesArray addObject:advertiseType];
            }
            categoryObject.productAdvertiseTypes = categoryAdvertiseTypesArray;
        }
        
        for (NSDictionary *subCategoryDictionary in [categoryDictionary valueForKey:kSubCategories]) {
            
            NSMutableArray *subSubcategoriesArray = [[NSMutableArray alloc] init];
            CategoryObject *subCategoryObject = [[CategoryObject alloc] init];
            
            if([subCategoryDictionary objectForKey:kProductWebsiteUrl]){
                subCategoryObject.productWebsiteUrl = [subCategoryDictionary objectForKey:kProductWebsiteUrl];
            }
            if([subCategoryDictionary objectForKey:@"productID"]){
                subCategoryObject.productID = [subCategoryDictionary objectForKey:@"productID"];
            }
            subCategoryObject.categoryID = [subCategoryDictionary objectForKey:kSubCategoriesId];
            subCategoryObject.categoryName = [subCategoryDictionary objectForKey:kSubCategoriesName];
            subCategoryObject.categoryImageURL = [subCategoryDictionary objectForKey:kSubCategoriesImage];
            subCategoryObject.categoryFullWidthImageURL = [subCategoryDictionary objectForKey:kSubSubCategoryImageLongWidth];
            subCategoryObject.subCategoryParent = categoryObject.categoryID;
            
            
            for (NSDictionary *subsubCategoryDictionary in [subCategoryDictionary valueForKey:kSubSubCategories]) {
                
                CategoryObject *subsubCategoryObject = [[CategoryObject alloc] init];
                
                subsubCategoryObject.categoryID = [subsubCategoryDictionary objectForKey:kSubSubCategoriesId];
                subsubCategoryObject.categoryName = [subsubCategoryDictionary objectForKey:kSubSubCategoriesName];
                subsubCategoryObject.categoryImageURL = [subsubCategoryDictionary objectForKey:kSubSubCategoriesImageUrl];
                
                [subSubcategoriesArray addObject:subsubCategoryObject];
                subCategoryObject.categoryFullWidthImageURL = [subCategoryDictionary objectForKey:kSubSubCategoryImageLongWidth];
            }
            
            subCategoryObject.categorySubcategories = subSubcategoriesArray;
            
            [subcategoriesArray addObject:subCategoryObject];
        }
        
        categoryObject.categorySubcategories = subcategoriesArray;
        
        [categoriesArray addObject:categoryObject];
    }
    
    [serverManagerResult setJsonArray:categoriesArray];
    
    
    return serverManagerResult;
}
//============================================================category Banners
//============================================================get category Banners
+ (void)getAllBannersForCategories:(NSString *)language
            withDelegate:(id<ServerManagerResponseDelegate>)delegate
           withRequestId:(NSString *)requestId {
    
    NSArray *keys = [NSArray arrayWithObjects:kLanguage, nil];
    NSArray *objects = [NSArray arrayWithObjects:language, nil];
    
    NSMutableDictionary *jsonDictionary =
    [NSMutableDictionary dictionaryWithObjects:objects forKeys:keys];
    
    [self sendJsonToUrlUsingMk:kGetAllBannersForCategories
                      withJson:jsonDictionary
                   AndDelegate:delegate
                 withRequestId:requestId
        withJsonParserSelector:@selector(parseAllBannersForCategories:
                                         withServerManagerResult:)
           withNumberOfRequest:0];
}
+(ServerManagerResult *)parseAllBannersForCategories:(NSMutableDictionary *)json withServerManagerResult:(ServerManagerResult *)serverManagerResult
{
    [serverManagerResult setJsonArray:[json objectForKey:kCategories]];
    return serverManagerResult;
}

//============================================================category items
//============================================================get category items
+ (void)getCategoryItemsInLanguage:(NSString *)language
                     andResolution:resolution
                      withDelegate:(id<ServerManagerResponseDelegate>)delegate
                    withCategoryId:(NSString *)categoryId
                     withRequestId:(NSString *)requestId
                       countOfRows:(NSString *)countOfRows
      withIsOpenedFromAddAdvertise:(NSString *)isOpenedFromAddAdvertise {
    
    NSArray *keys = [NSArray arrayWithObjects:kLanguage, kResolution, kProductId,kcountOfRow,
                     kIsAddAdvertise, nil];
    
    NSArray *objects = [NSArray arrayWithObjects:language, resolution, categoryId,countOfRows,
                        isOpenedFromAddAdvertise, nil];
    
    NSMutableDictionary *jsonDictionary =
    [NSMutableDictionary dictionaryWithObjects:objects forKeys:keys];
    
    [self sendJsonToUrlUsingMk:kGetCategoriesUrl
                      withJson:jsonDictionary
                   AndDelegate:delegate
                 withRequestId:requestId
        withJsonParserSelector:@selector(parseProductsFromJson:
                                         withServerManagerResult:)
           withNumberOfRequest:0];
}

//==================================Get images from server to Write Ttxt on
// it=================
+ (void)getImagesToWriteTextOnIt:(NSString *)language
                   andResolution:resolution
                    withDelegate:(id<ServerManagerResponseDelegate>)delegate
                  withCategoryId:(NSString *)categoryId
                   withRequestId:(NSString *)requestId
    withIsOpenedFromAddAdvertise:(NSString *)isOpenedFromAddAdvertise {
    NSArray *keys = [NSArray arrayWithObjects:kLanguage, kResolution, kProductId,
                     kIsAddAdvertise, nil];
    
    NSArray *objects = [NSArray arrayWithObjects:language, resolution, categoryId,
                        isOpenedFromAddAdvertise, nil];
    
    NSMutableDictionary *jsonDictionary =
    [NSMutableDictionary dictionaryWithObjects:objects forKeys:keys];
    
    [self sendJsonToUrlUsingMk:kGetImagesToWriteTextOnItUrl
                      withJson:jsonDictionary
                   AndDelegate:delegate
                 withRequestId:requestId
        withJsonParserSelector:@selector(parseImagesfromjson:
                                         withServerManagerResult:)
           withNumberOfRequest:0];
}

+ (NSString *)LANGUAGE_ARABIC {
    return @"ar";
}
+ (NSString *)LANGUAGE_ENGLISH {
    return @"en";
}
+ (NSString *)RESOLUTION_LOW {
    return @"low";
}

+ (NSString *)RESOLUTION_MEDIUM {
    return @"medium";
}

+ (NSString *)RESOLUTION_LARGE {
    return @"large";
}
//============================================================get category
// porducts

+ (ServerManagerResult *)decodeJsonIntoProductItems:(NSData *)json
                                     withCategoryId:(NSString *)categoryId {
    ServerManagerResult *serverManagerResult =
    [[ServerManagerResult alloc] init];
    
    NSMutableArray *productArray = [[NSMutableArray alloc] init];
    
    NSDictionary *jsonObjectsDictionary =
    [NSJSONSerialization JSONObjectWithData:json
                                    options:NSJSONReadingMutableContainers
                                      error:nil];
    
    NSArray *jsonObjects = [jsonObjectsDictionary objectForKey:kProducts];
    
    // categoryProductLastUpdateTime = [jsonObjectsDictionary
    // objectForKey:kLastUpdateTime];
    
    for (int i = 0; i < jsonObjects.count; i++) {
        
        // get dictionary of eac
        NSDictionary *productDictionary = [jsonObjects objectAtIndex:i];
        
        ProductObject *productObject =
        [self getProductObject:productDictionary
                  inCategoryId:categoryId
                 withRequestId:serverManagerResult.requestId];
        
        [productArray addObject:productObject];
    }
    
    [serverManagerResult setJsonArray:productArray];
    
    
    return serverManagerResult;
}

+ (ProductObject *)getProductObject:(NSMutableDictionary *)productDictionary
                       inCategoryId:(NSString *)categoryId
                      withRequestId:(NSString *)requestId {
    ProductObject *productObject = [[ProductObject alloc] init];
    
    if ([SharedData isAllowLog]) {
        // get the product values from json
        NSLog(@"===================================================");
        NSLog(@"product id:%@", [productDictionary valueForKey:kProductId]);
        NSLog(@"product name:%@", [productDictionary valueForKey:kProductName]);
        NSLog(@"product name english:%@",
              [productDictionary valueForKey:kProductNameEnglish]);
        NSLog(@"product name arabic:%@",
              [productDictionary valueForKey:kProductNameArabic]);
        NSLog(@"product description:%@",
              [productDictionary valueForKey:kProductDescription]);
        NSLog(@"product description english:%@",
              [productDictionary valueForKey:kProductDescriptionEnglish]);
        NSLog(@"product description arabic:%@",
              [productDictionary valueForKey:kProductDescriptionArabic]);
        NSLog(@"product price:%@", [productDictionary valueForKey:kProductPrice]);
        NSLog(@"product main image url:%@",
              [productDictionary valueForKey:kProductMainImage]);
        NSLog(@"product images:%@", [productDictionary valueForKey:kProductImages]);
        NSLog(@"product categoryId:%@", [productDictionary valueForKey:categoryId]);
        NSLog(@"product comments:%@",
              [productDictionary valueForKey:kProductComments]);
        NSLog(@"product comments count:%@",
              [productDictionary valueForKey:KProductCommentCount]);
        NSLog(@"product comments like count:%@",
              [productDictionary valueForKey:KProductLikeCount]);
        NSLog(@"advertise resolution:%@",
              [productDictionary valueForKey:kadvertiseResolution]);
        NSLog(@"count of row:%@", [productDictionary valueForKey:kcountOfRow]);
        NSLog(@"is add supported%@",
              [productDictionary valueForKey:kisAdsSupported]);
        NSLog(@"===================================================");
    }
    
    productObject.isLoadedAdd = false;
    productObject.isLoadedAddInList = false;
    
    productObject.productUserisCompany = [productDictionary valueForKey:KUserCompany];
    
    productObject.productId = [productDictionary valueForKey:kProductId];
    
    productObject.productAdvClickType = [productDictionary valueForKey:kClickType];
    
    productObject.productTilte = [productDictionary valueForKey:kProductName];
    productObject.productTilteEnglish =
    [productDictionary valueForKey:kProductNameEnglish];
    productObject.productTilteArabic =
    [productDictionary valueForKey:kProductNameArabic];
    
    productObject.productDescription =
    [productDictionary valueForKey:kProductDescription];
    productObject.productDescriptionWithTitles =
    [productDictionary valueForKey:kProductDescriptionWithTitles];
    productObject.productDescriptionEnglish =
    [productDictionary valueForKey:kProductDescriptionEnglish];
    productObject.productDescriptionArabic =
    [productDictionary valueForKey:kProductDescriptionArabic];
    
    productObject.productPrice = [productDictionary valueForKey:kProductPrice];
    productObject.productMainImageUrl =
    [productDictionary valueForKey:kProductMainImage];
    productObject.productShareImageUrl =
    [productDictionary valueForKey:kProductShareImage];
    productObject.productCategoryId = categoryId;
    productObject.productCommentCount =
    [productDictionary valueForKey:KProductCommentCount];
    productObject.productLikeCount =
    [productDictionary valueForKey:KProductLikeCount];
    productObject.productLanguage =
    [productDictionary valueForKey:kProductLanguage];
    productObject.productCategoryId =
    [productDictionary valueForKey:kProductCategoryId];
    productObject.productCategoryName =
    [productDictionary valueForKey:kProductCategoryName];
    productObject.productSubCategoryId =
    [productDictionary valueForKey:kproductSubCategoryId];
    productObject.productSubCategoryName =
    [productDictionary valueForKey:kproductSubCategoryName];
    productObject.productSubsubCategoryId =
    [productDictionary valueForKey:kSubSubCategoriesId];
    productObject.productSubsubCategoryName =
    [productDictionary valueForKey:kSubSubCategoriesName];
    productObject.productFilterType =
    [productDictionary valueForKey:kProductFilterType];
    productObject.productUserNumberOfAds =
    [productDictionary valueForKey:kUserNumberOfAds];
    productObject.productUserName = [productDictionary valueForKey:KUserName];
    productObject.productUserEmail = [productDictionary valueForKey:KUserEmail];
    productObject.productUserImageUrl =
    [productDictionary valueForKey:kUserPhoto];
    productObject.productUserShortDescription =
    [productDictionary valueForKey:kUserSmallDescription];
    productObject.productCityId =
    [productDictionary valueForKey:kCityId];
    productObject.productCityName =
    [productDictionary valueForKey:KCityName];
    productObject.productRegionId =
    [productDictionary valueForKey:KRegionId];
    productObject.productRegionName =
    [productDictionary valueForKey:KRegionName];
    productObject.productAdvertiseTypeId =
    [productDictionary valueForKey:kcategoryAdvertiseTypesId];
    productObject.productAdvertiseTypeName =
    [productDictionary valueForKey:kcategoryAdvertiseTypesName];
    productObject.productFurnishedTypeId =
    [productDictionary valueForKey:kProductFurnishedId];
    productObject.productFurnishedTypeName =
    [productDictionary valueForKey:kProductFurnishedName];
    productObject.productKm =
    [productDictionary valueForKey:kProductKm];
    productObject.productManfactureYear =
    [productDictionary valueForKey:kProductManfactureYear];
    productObject.productNumberOfRooms =
    [productDictionary valueForKey:kProductNumberOfRooms];
    productObject.productCompanyNumberOfAds =
    [productDictionary valueForKey:kNumberOfAds];
    //    productObject.productAdvertiseTypes=[productDictionary
    //    valueForKey:kCategoryAdvertiseTypes];
    
    if ([productDictionary valueForKey:kcategoryAdvertiseTypes]!= nil) {
        NSMutableArray *categoryAdvertiseTypesArray = [[NSMutableArray alloc] init];
        
        for (NSDictionary *regionDictionary in
             [productDictionary valueForKey:kcategoryAdvertiseTypes]) {
            AdvertiseType *advertiseType = [[AdvertiseType alloc] init];
            advertiseType.advertiseTypeId =
            [regionDictionary valueForKey:kcategoryAdvertiseTypesId];
            advertiseType.advertiseTypeName =
            [regionDictionary valueForKey:kcategoryAdvertiseTypesName];
            advertiseType.advertiseTypeDefaultView =
            [regionDictionary valueForKey:kcategoryAdvertiseTypeDefaultView];
            [categoryAdvertiseTypesArray addObject:advertiseType];
        }
        productObject.productAdvertiseTypes = categoryAdvertiseTypesArray;
    }
  
    // advertise param
    productObject.productAdvertiseUrl =
    [productDictionary valueForKey:kproductAdvertiseUrl];
    
    productObject.isAd =
    [[productDictionary valueForKey:kisAd] isEqualToString:@"0"];
    
    productObject.isHideAddCommentsAndLikes =
    [[productDictionary valueForKey:kisShowAddCommentAndLikes]
     isEqualToString:@"1"];
    
    if ([productDictionary valueForKey:KUserNumber] != nil)
        productObject.productUserNumber =
        [productDictionary valueForKey:KUserNumber];
    
    if ([productDictionary valueForKey:kUserCountry] != nil)
        productObject.productCountryCode =
        [productDictionary valueForKey:kUserCountry];
    
    if ([productDictionary valueForKey:kNumberOfViews] != nil)
        productObject.productNumberOfViews =
        [productDictionary valueForKey:kNumberOfViews];
    
    if ([productDictionary valueForKey:kProductUrl] != nil)
        productObject.productWebsiteUrl = [productDictionary valueForKey:kProductUrl];
    
    if ([productDictionary valueForKey:kUserProfileLink] != nil)
        productObject.productUserUrl = [productDictionary valueForKey:kUserProfileLink];
    
    if ([requestId isEqualToString:@"requestGetProductComments"] ||
        [requestId isEqualToString:@"requestDeleteProductComment"] ||
        [requestId isEqualToString:@"requestMoreCommentsBtn"] |
        [requestId isEqualToString:@"requestGetProductDetails"] ||
        [requestId isEqualToString:@"requestInitialCategoryProducts"] ) {
        if ([productDictionary valueForKey:KDateOfAdvertise] != nil) {
//            NSString *dateMillisecond =
//            [productDictionary valueForKey:KDateOfAdvertise];
//            ;requestInitialCategoryProducts
//            NSDate *date = [NSDate
//                            dateWithTimeIntervalSince1970:[dateMillisecond longLongValue] /
//                            1000.];
//            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
//            [dateFormatter setDateFormat:@"dd-MM-yyyy"];
//            [dateFormatter setLocale:[NSLocale currentLocale]];
//            
            productObject.productDate = [productDictionary valueForKey:KDateOfAdvertise];        }
        
        if ([productDictionary valueForKey:KDateOfAdvertiseInMilliSecond] != nil) {
            productObject.productDateInMillisecond = [productDictionary valueForKey:KDateOfAdvertiseInMilliSecond];
        }
        
        
        if (productObject.productDescription != nil) {
            productObject.productDescriptionSizePortrait =
            [StringUtility getSize:productObject.productDescription
                       andFontSize:16
                            isBold:YES
                        isPortrait:YES];
            productObject.productDescriptionSizeLandscape =
            [StringUtility getSize:productObject.productDescription
                       andFontSize:16
                            isBold:YES
                        isPortrait:NO];
        }
        
        if (productObject.productDescriptionWithTitles != nil) {
            productObject.productDescriptionWithTitlesSizePortrait =
            [StringUtility getSizeWithNewSize:productObject.productDescriptionWithTitles
                                  andFontSize:17
                            isBold:NO
                        isPortrait:YES];
            productObject.productDescriptionWithTitlesSizeLandscape =
            [StringUtility getSizeWithNewSize:productObject.productDescriptionWithTitles
                       andFontSize:17
                            isBold:NO
                        isPortrait:NO];
        }
        
        if (productObject.productDescriptionEnglish != nil) {
            productObject.productDescriptionEnglishSizePortrait =
            [StringUtility getSize:productObject.productDescriptionEnglish
                       andFontSize:16
                            isBold:YES
                        isPortrait:YES];
            productObject.productDescriptionEnglishSizeLandscape =
            [StringUtility getSize:productObject.productDescriptionEnglish
                       andFontSize:16
                            isBold:YES
                        isPortrait:NO];
        }
        
        if (productObject.productDescriptionArabic != nil) {
            productObject.productDescriptionArabicSizePortrait =
            [StringUtility getSize:productObject.productDescriptionArabic
                       andFontSize:16
                            isBold:YES
                        isPortrait:YES];
            productObject.productDescriptionArabicSizeLandscape =
            [StringUtility getSize:productObject.productDescriptionArabic
                       andFontSize:16
                            isBold:YES
                        isPortrait:NO];
        }
        
        // add product comments
        NSArray *productCommentsFromServer =
        [productDictionary valueForKey:kProductComments];
        for (int i = 0; i < productCommentsFromServer.count; i++) {
            
            if (i == 0) {
                productObject.productComments =
                [[NSMutableArray alloc] init];
            }
            
            ProductCommentObject *productCommentObject =
            [[ProductCommentObject alloc] init];
            
            productCommentObject.commentId =
            [[productCommentsFromServer objectAtIndex:i] valueForKey:kCommentId];
            
            productCommentObject.commentDescription =
            [[productCommentsFromServer objectAtIndex:i]
             valueForKey:KUserCommentJson];
            productCommentObject.commentDescriptionSizePortrait =
            [StringUtility getSizeForComment:productCommentObject.commentDescription
                       andFontSize:14
                            isBold:NO
                        isPortrait:YES];
            productCommentObject.commentDescriptionSizeLandscape =
            [StringUtility getSizeForComment:productCommentObject.commentDescription
                       andFontSize:14
                            isBold:NO
                        isPortrait:NO];

            productCommentObject.commentDate =[[productCommentsFromServer objectAtIndex:i]        objectForKey:KUserCommentDate];;
            
            
            // productCommentObject.commentDateSizePortrait=[StringUtility
            // getSize:productCommentObject.commentDate andFontSize:16 isBold:NO
            // isPortrait:YES];
            // productCommentObject.commentDateSizeLandscape=[StringUtility
            // getSize:productCommentObject.commentDate andFontSize:16 isBold:NO
            // isPortrait:NO];
            productCommentObject.userImageUrl = [[productCommentsFromServer objectAtIndex:i]
                                                 valueForKey:KUserCommentuserImageUrl];
            
            productCommentObject.commentUserName =
            [[productCommentsFromServer objectAtIndex:i]
             valueForKey:KUserCommentName];
            // productCommentObject.commentUserNameSizePortrait=[StringUtility
            // getSize:productCommentObject.commentUserName andFontSize:16 isBold:YES
            // isPortrait:YES];
            // productCommentObject.commentUserNameSizeLandscape=[StringUtility
            // getSize:productCommentObject.commentUserName andFontSize:16 isBold:YES
            // isPortrait:NO];
            
            productCommentObject.commenterCountryCode =
            [[productCommentsFromServer objectAtIndex:i]
             valueForKey:KUserCommentCountryCode];
            
            productCommentObject.commenterNumber =
            [[productCommentsFromServer objectAtIndex:i] valueForKey:KUserNumber];
            
            [productObject.productComments addObject:productCommentObject];
            
        }
        
        if ([[productDictionary valueForKey:kProductImages]isKindOfClass:[NSArray class]]){
            
            NSMutableArray *productImages = [NSMutableArray arrayWithArray:[productDictionary valueForKey:kProductImages]];
            productObject.productImagesUrls = productImages;
        } else {
            productObject.productImagesUrls = [[NSMutableArray alloc] init];
        }
        
        /*if ([[productDictionary valueForKey:kProductImages]
             isKindOfClass:[NSMutableArray class]]) {
            NSMutableArray *productImages = [[NSMutableArray alloc]initWithArray:[productDictionary valueForKey:kProductImages]];
            productObject.productImagesUrls = productImages;
        }*/
    }
    
    if (productObject.productImagesUrls == nil) {
        productObject.productImagesUrls = [[NSMutableArray alloc] init];
    }
    
//    if (productObject.productMainImageUrl != nil) {
//        [productObject.productImagesUrls insertObject:productObject.productMainImageUrl atIndex:0];
//    }
    
    if([requestId isEqualToString:@"requestInitialCategoryProducts"] || [requestId isEqualToString:@"requestInitialCompanies"]){
        if ([[productDictionary valueForKey:kUserAds] isKindOfClass:[NSMutableArray class]] ||
            [[productDictionary valueForKey:kUserAds]isKindOfClass:[NSArray class]]) {
            NSMutableArray *productImages = [[NSMutableArray alloc] initWithArray:[productDictionary valueForKey:kUserAds]];
            productObject.productImagesUrls = productImages;
        } else {
//            productObject.productImagesUrls = [[NSMutableArray alloc] init];
        }
    }
    return productObject;
}

+ (void)getCategoryProductsItems:(NSString *)categoryId
                    withDelegate:
(id<ServerManagerResponseDelegate>)delegate
                  withNewSession:(bool)isNewSession
                withTypeOfUpdate:(NSString *)typeOfUpdate
   withCategoryProductPageNumber:(int)categoryProductPageNumber
withCategoryProductLastUpdateTime:(NSString *)categoryProductLastUpdateTime
withCategoryProductNumberOfPages:(int)categoryProductNumberOfPages
                   withSearchStr:(NSString *)searchStr
                   withRequestId:(NSString *)requestId
           withAdvertiseLangType:(NSString *)advertiseLangType
               withSubCategoryId:(NSString *)subCategoryId
                    withSortById:(NSString *)sortById
           withAdvertiseLanguage:(NSString *)advertiseLang
                   isAdSupported:(NSString *)isAdSupported
                     countOfRows:(NSString *)countOfRows
         withAdvertiseResolution:(NSString *)advertiseResolution
     withCategoryAdvertiseTypeId:(NSString *)categoryAdvertiseTypeId
                      withCityId:(NSString *)cityId
                    withRegionId:(NSString *)regionId
            withSubSubCategoryId:(NSString *)subsubCategoryId
          withManfactureYearFrom:(NSString *)manfactureYearFrom
                withManfactureTo:(NSString *)manfactureYearTo
                      withKmFrom:(NSString *)kmFrom
                        withKmTo:(NSString *)kmTo
            withNumberOfRoomFrom:(NSString *)numberOfRoomfrom
             withNumberOfRoomsTo:(NSString *)numberOfRoomsTo
                   withPriceFrom:(NSString *)priceFrom
                     withPriceTo:(NSString *)priceTo
             withFurnishedTypeId:(NSString*)furnishedTypeId
              andNumberOfRequest:(int)numberOfRequest{
    
    //===============================
    //    if(isNewSession)
    //    {
    //        [SharedData setCategoryDataLastUpdateTime:@"0"];
    //        //categoryProductPageNumber = 1;
    //    }
    
    NSMutableDictionary *jsonDictionary =
    [self encodeGetCategoryProductJson:categoryId
                      withTypeOfUpdate:typeOfUpdate
         withCategoryProductPageNumber:categoryProductPageNumber
     withCategoryProductLastUpdateTime:categoryProductLastUpdateTime
      withCategoryProductNumberOfPages:categoryProductNumberOfPages
                         withSearchStr:searchStr
                 withAdvertiseTypeLang:advertiseLangType
                     withSubCategoryId:subCategoryId
                          withSortById:sortById
                 withAdvertiseLanguage:advertiseLang
                         isAdSupported:isAdSupported
                           countOfRows:countOfRows
               withAdvertiseResolution:advertiseResolution
           withCategoryAdvertiseTypeId:categoryAdvertiseTypeId
                            withCityId:cityId
                          withRegionId:regionId
                  withSubSubCategoryId:subsubCategoryId
                withManfactureYearFrom:manfactureYearFrom
                      withManfactureTo:manfactureYearTo
                            withKmFrom:kmFrom
                              withKmTo:kmTo
                  withNumberOfRoomFrom:numberOfRoomfrom
                   withNumberOfRoomsTo:numberOfRoomsTo
                         withPriceFrom:priceFrom
                           withPriceTo:priceTo withFurnishedTypeId:furnishedTypeId
     ];
    
    // send json
    [self sendJsonToUrlUsingMk:kGetCategoriesProductUrl
                      withJson:jsonDictionary
                   AndDelegate:delegate
                 withRequestId:requestId
        withJsonParserSelector:@selector(parseProductsFromJson:
                                         withServerManagerResult:)
           withNumberOfRequest:numberOfRequest];
}

+ (NSMutableDictionary *)
encodeGetCategoryProductJson:(NSString *)categoryId
withTypeOfUpdate:(NSString *)typeOfUpdate
withCategoryProductPageNumber:(int)categoryProductPageNumber
withCategoryProductLastUpdateTime:(NSString *)categoryProductLastUpdateTime
withCategoryProductNumberOfPages:(int)categoryProductNumberOfPages
withSearchStr:(NSString *)searchStr
withAdvertiseTypeLang:(NSString *)advertiseLanguageType
withSubCategoryId:(NSString *)subCategoryId
withSortById:(NSString *)sortById
withAdvertiseLanguage:(NSString *)advertiseLang
isAdSupported:(NSString *)isAdSupported
countOfRows:(NSString *)countOfRows
withAdvertiseResolution:(NSString *)advertiseResolution
withCategoryAdvertiseTypeId:(NSString *)categoryAdvertiseTypeId
withCityId:(NSString *)cityId
withRegionId:(NSString *)regionId
withSubSubCategoryId:(NSString *)subsubCategoryId
withManfactureYearFrom:(NSString *)manfactureYearFrom
withManfactureTo:(NSString *)manfactureYearTo
withKmFrom:(NSString *)kmFrom
withKmTo:(NSString *)kmTo
withNumberOfRoomFrom:(NSString *)numberOfRoomfrom
withNumberOfRoomsTo:(NSString *)numberOfRoomsTo
withPriceFrom:(NSString *)priceFrom
withPriceTo:(NSString *)priceTo
withFurnishedTypeId:(NSString*)furnishedTypeId{
    NSArray *keys = [NSArray
                     arrayWithObjects:kProductId, KIsNewUpdate, kPage, kPerPage,
                     kLastUpdateTime, kSearchStr, kproductsLanguage,
                     kproductSubCategoryId, ksortBy, kadvertiseLanguage,
                     kisAdsSupported, kcountOfRow, kadvertiseResolution,
                     kcategoryAdvertiseTypesId, kCityId, KRegionId,
                     kSubSubCategoriesId, KManfactureYearFrom,
                     KmanfactureYearTo, KkmFrom, KKmTo, KNumberOfRoomsFrom,
                     KNumberOfRoomsTo, kPriceFrom, KPriceTo,kProductFurnishedId, nil];
    
    NSString *productPageNumber =
    [NSString stringWithFormat:@"%d", categoryProductPageNumber];
    NSString *productNumberOfPages =
    [NSString stringWithFormat:@"%d", categoryProductNumberOfPages];
    /////
    NSString *productLastUpdateTime =
    [NSString stringWithFormat:@"%@", categoryProductLastUpdateTime];
    
    NSArray *objects = [NSArray
                        arrayWithObjects:categoryId, typeOfUpdate, productPageNumber,
                        productNumberOfPages, productLastUpdateTime, searchStr,
                        advertiseLanguageType, subCategoryId, sortById,
                        advertiseLang, isAdSupported, countOfRows,
                        advertiseResolution, categoryAdvertiseTypeId, cityId,
                        regionId, subsubCategoryId, manfactureYearFrom,
                        manfactureYearTo, kmFrom, kmTo, numberOfRoomfrom,
                        numberOfRoomsTo, priceFrom, priceTo,furnishedTypeId, nil];
    
    NSMutableDictionary *jsonDictionary = nil;
    if (objects.count > 0)
        jsonDictionary =
        [NSMutableDictionary dictionaryWithObjects:objects forKeys:keys];
    
    return jsonDictionary;
}

+ (NSString *)GET_UPDATES_AFTER_LAST_UPDATE_TIME {
    return @"0";
}

+ (NSString *)GET_UPDATES_BEFORE_LAST_UPDATE_TIME {
    return @"1";
}
//============================================================get product
// details

+ (void)getProductDetails:(NSString *)productId
             withDelegate:(id<ServerManagerResponseDelegate>)delegate
            withRequestId:(NSString *)requestId
               byLanguage:(NSString *)lang
              isAdvertise:(NSString *)isAdvertise
       andUserCountryCode:(NSString *)userCountryCode
            andUserNumber:(NSString *)userNumber
       andIsEditAdvertise:(NSString*)isEditAdvertise{
    NSMutableDictionary *jsonDictionary =
    [self encodeGetProductDetailJson:productId
                          byLanguage:lang
                         isAdvertise:isAdvertise
                  andUserCountryCode:userCountryCode
                       andUserNumber:userNumber andIsEditadvertise:isEditAdvertise];
    
    [self sendJsonToUrlUsingMk:kGetProductDetailUrl
                      withJson:jsonDictionary
                   AndDelegate:delegate
                 withRequestId:requestId
        withJsonParserSelector:@selector(decodeProductDetail:
                                         withServerManagerResult:)
           withNumberOfRequest:0];
}

+ (ServerManagerResult *)decodeProductDetail:(NSMutableDictionary *)json
                     withServerManagerResult:
(ServerManagerResult *)serverManagerResult {
    // ServerManagerResult *serverManagerResult=[[[ServerManagerResult alloc]init]
    // autorelease];
    
    NSMutableArray *productArray = [[NSMutableArray alloc] init];
    
    //   NSDictionary *jsonObjectsDictionary = [NSJSONSerialization
    //   JSONObjectWithData:json options:NSJSONReadingMutableContainers
    //   error:nil];
    
    ProductObject *productObject =
    [self getProductObject:json
              inCategoryId:nil
             withRequestId:serverManagerResult.requestId];
    
    [productArray addObject:productObject];
    
    [serverManagerResult setJsonArray:productArray];
    
    
    return nil;
}

+ (NSMutableDictionary *)encodeGetProductDetailJson:(NSString *)productId
                                         byLanguage:(NSString *)lang
                                        isAdvertise:(NSString *)isAdvertise
                                 andUserCountryCode:(NSString *)userCountryCode
                                      andUserNumber:(NSString *)userNumber
                                 andIsEditadvertise:(NSString*)isEditAdvertise{
    NSArray *keys =
    [NSArray arrayWithObjects:kProductId, kproductsLanguage, kisAd,
     kUserCountryCode, KUserNumber,kIsEditAdvertise, nil];
    
    NSArray *objects =
    [NSArray arrayWithObjects:productId, lang, isAdvertise, userCountryCode,
     userNumber,isEditAdvertise, nil];
    
    NSMutableDictionary *jsonDictionary =
    [NSMutableDictionary dictionaryWithObjects:objects forKeys:keys];
    
    return jsonDictionary;
}

//============================================================Add View to Advertise

+ (void)addViewToAdvertise:(NSString *)productId
             withDelegate:(id<ServerManagerResponseDelegate>)delegate
            withRequestId:(NSString *)requestId{
    
    NSMutableDictionary *jsonDictionary = [[NSMutableDictionary alloc] initWithObjectsAndKeys:@"id", productId, nil];
    
    [self sendJsonToUrlUsingMk:kGetProductDetailUrl
                      withJson:jsonDictionary
                   AndDelegate:delegate
                 withRequestId:requestId
        withJsonParserSelector:nil
           withNumberOfRequest:0];
}

//============================================================add advetise

+ (void)addProductInfoWithoutImages:(ProductObject *)productObject
                       withDelegate:(id<ServerManagerResponseDelegate>)delegate
                      withRequestId:(NSString *)requestId;
{
    NSMutableDictionary *jsonDictionary =
    [self encodeAddProductDetailWithoutImage:productObject];
    
    // send json
    [self sendJsonToUrlUsingMk:kAddadvertiseData
                      withJson:jsonDictionary
                   AndDelegate:delegate
                 withRequestId:requestId
        withJsonParserSelector:@selector(decodeAddProductDetailOfProduct:
                                         withServerManagerResult:)
           withNumberOfRequest:0];
}

+ (ServerManagerResult *)
decodeAddProductDetailOfProduct:(NSMutableDictionary *)json
withServerManagerResult:(ServerManagerResult *)serverManagerResult {
    ProductObject *productObject = [[ProductObject alloc] init];
    
    productObject.productId = [json objectForKey:kProductId];
    
    if (serverManagerResult.jsonArray == nil) {
        serverManagerResult.jsonArray = [[NSMutableArray alloc] init];
    }
    [serverManagerResult.jsonArray addObject:productObject];
    return serverManagerResult;
}

//{"productId":"","userCountryCode":"00974","userNumber":"66524147","categoryId":"5","productName":"productName","productNameEnglish":"english","productNameArabic":"","productPrice":"88","productDescription":"productDescription","productDescriptionEnglish":"description","productDescriptionArabic":"","lang":"ar","isResetImages":"false"
//,
//"categoryAdvertiseTypeId":"0","cityId":"0","regionId":"0","subsubCategoryId":"0","manfactureYear":"2009","km":"123123","furnishedType":"full
// or semi or not","numberOfRooms":"2"}
+ (NSMutableDictionary *)encodeAddProductDetailWithoutImage:
(ProductObject *)productObject {
    
    NSArray *keys = [NSArray
                     arrayWithObjects:kProductId, KUserNumber, KUserCommentCountryCode,
                     kProductCategoryId, kproductSubCategoryId, kProductName,
                     kProductNameEnglish, kProductNameArabic, kProductPrice,
                     kProductDescription, kProductDescriptionEnglish,
                     kProductDescriptionArabic, kProductLanguage,
                     kIsResetImages, kcategoryAdvertiseTypesId, kCityId,
                     KRegionId, kSubSubCategoriesId, kProductManfactureYear,
                     kProductKm, kProductFurnishedId, kProductNumberOfRooms,
                     nil];
    
    NSArray *objects = [NSArray
                        arrayWithObjects:
                        productObject.productId, [UserSetting getUserNumber],
                        [UserSetting getUserCountryCode], productObject.productCategoryId,
                        productObject.productSubCategoryId, productObject.productTilte,
                        productObject.productTilteEnglish, productObject.productTilteArabic,
                        productObject.productPrice, productObject.productDescription,
                        productObject.productDescriptionEnglish,
                        productObject.productDescriptionArabic, productObject.productLanguage,
                        [productObject isResetProductImages],
                        productObject.productAdvertiseTypeId, productObject.productCityId,
                        productObject.productRegionId, productObject.productSubsubCategoryId,
                        productObject.productManfactureYear, productObject.productKm,
                        productObject.productFurnishedTypeId,
                        productObject.productNumberOfRooms, nil];
    
    NSMutableDictionary *jsonDictionary =
    [NSMutableDictionary dictionaryWithObjects:objects forKeys:keys];
    
    return jsonDictionary;
}

+ (NSString *)encodeImage:(UIImage *)image {
    NSData *data = UIImageJPEGRepresentation(image, 1.0f);
    [Base64 initialize];
    NSString *strEncoded = [Base64 encode:data];
    return strEncoded;
}

+ (void)addImageToProductId:(NSString *)productId
                isMainImage:(NSString *)isMainImage
                  withImage:(UIImage *)productImage
               withDelegate:(id<ServerManagerResponseDelegate>)delegate
              withRequestId:(NSString *)requestId
        withNumberOfRequest:(int)numberOfRequest {
    NSData *data = UIImageJPEGRepresentation(productImage, 1.0f);
    [Base64 initialize];
    NSString *strEncoded = [Base64 encode:data];
    
    NSArray *keys = [NSArray
                     arrayWithObjects:kProductId, kProductIsMainImage, kProductImage, nil];
    NSArray *objects =
    [NSArray arrayWithObjects:productId, isMainImage, strEncoded, nil];
    
    NSMutableDictionary *postParams =
    [NSMutableDictionary dictionaryWithObjects:objects forKeys:keys];
    
    // send json
    [self sendJsonToUrlUsingMk:kAddImageAdvertiseUrl
                      withJson:postParams
                   AndDelegate:delegate
                 withRequestId:requestId
        withJsonParserSelector:nil
           withNumberOfRequest:numberOfRequest];
}

+ (void)addImageUrlToProductId:(NSString *)productId
                   isMainImage:(NSString *)isMainImage
                     withImage:(NSString *)productImageUrl
                  withDelegate:(id<ServerManagerResponseDelegate>)delegate
                 withRequestId:(NSString *)requestId
           withNumberOfRequest:(int)numberOfRequest {
    NSArray *keys = [NSArray
                     arrayWithObjects:kProductId, kProductIsMainImage, kProductImage, nil];
    NSArray *objects =
    [NSArray arrayWithObjects:productId, isMainImage, productImageUrl, nil];
    
    NSMutableDictionary *postParams =
    [NSMutableDictionary dictionaryWithObjects:objects forKeys:keys];
    
    // send json
    [self sendJsonToUrlUsingMk:kAddImageAdvertiseUrl
                      withJson:postParams
                   AndDelegate:delegate
                 withRequestId:requestId
        withJsonParserSelector:nil
           withNumberOfRequest:numberOfRequest];
}

//============================================================Registeration

+ (void)registeUserWithName:(NSString *)userName
             AndCountryCode:(NSString *)userCountryCode
                  AndNumber:(NSString *)userNumber
                   AndEmail:(NSString *)userEmail
               andUserToken:(NSString *)userToken
                AndDelegate:(id<ServerManagerResponseDelegate>)delegate
              withRequestId:(NSString *)requestId {
    //==============================================set up json to
    // send================================================
    NSArray *keys =
    [NSArray arrayWithObjects:KUserName, kUserCountryCode, KUserNumber,
     KUserEmail, KUserToken, nil];
    NSArray *objects =
    [NSArray arrayWithObjects:userName, userCountryCode, userNumber,
     userEmail, userToken, nil];
    NSMutableDictionary *postParams =
    [NSMutableDictionary dictionaryWithObjects:objects forKeys:keys];
    
    // send json
    [self sendJsonToUrlUsingMk:kRegisterUrl
                      withJson:postParams
                   AndDelegate:delegate
                 withRequestId:requestId
        withJsonParserSelector:nil
           withNumberOfRequest:0];
}
+ (void)registeCompanyRequest:(NSString *)companyName
             AndCompanyPhoneNumber:(NSString *)companyPhoneNumber
                  AndCompanyEmail:(NSString *)companyEmail
                   AndCompanyDetails:(NSString *)companyDetails
                AndDelegate:(id<ServerManagerResponseDelegate>)delegate
              withRequestId:(NSString *)requestId {
    //==============================================set up json to
    // send================================================
    NSArray *keys =  [NSArray arrayWithObjects:KCompanyName, KCompanyNumber, KCompanyEmail,
     kCompanyDetails,nil];
    NSArray *objects =
    [NSArray arrayWithObjects:companyName, companyPhoneNumber, companyEmail,
     companyDetails,nil];
    NSMutableDictionary *postParams =
    [NSMutableDictionary dictionaryWithObjects:objects forKeys:keys];
    
    // send json
    [self sendJsonToUrlUsingMk:kCompanyRegisterUrl
                      withJson:postParams
                   AndDelegate:delegate
                 withRequestId:requestId
        withJsonParserSelector:nil
           withNumberOfRequest:0];
}

//============================================================activation code

+ (void)checkActivationCode:(NSString *)activationCode
             AndCountryCode:(NSString *)userCountryCode
                  AndNumber:(NSString *)userNumber
                AndDelegate:(id<ServerManagerResponseDelegate>)delegate
              withRequestId:(NSString *)requestId {
    //==============================================set up json to
    // send================================================
    NSArray *keys = [NSArray
                     arrayWithObjects:kUserActivationCode, kUserCountryCode, KUserNumber, nil];
    NSArray *objects = [NSArray
                        arrayWithObjects:activationCode, userCountryCode, userNumber, nil];
    NSMutableDictionary *postParams =
    [NSMutableDictionary dictionaryWithObjects:objects forKeys:keys];
    
    // send json
    [self sendJsonToUrlUsingMk:kActivationUrl
                      withJson:postParams
                   AndDelegate:delegate
                 withRequestId:requestId
        withJsonParserSelector:nil
           withNumberOfRequest:0];
}

//============================================================mk netowrk engine

+ (void)initMkNetworkEngine {
    // if(mkNetworkEngine==nil)
    {
        if (isGettingImages) {
            mkNetworkEngine =
            [[MkNetworkEngine alloc] initWithHostName:@"http://192.168.100.6"
                                   customHeaderFields:nil];
        } else {
            mkNetworkEngine = [[MkNetworkEngine alloc] initWithHostName:kHostName
                                                     customHeaderFields:nil];
        }
    }
}

+ (void)sendJsonToUrlUsingMk:(NSString *)url
                    withJson:(NSMutableDictionary *)json
                 AndDelegate:(id<ServerManagerResponseDelegate>)delegate
               withRequestId:(NSString *)requestId
      withJsonParserSelector:(SEL)jsonParserSelector
         withNumberOfRequest:(int)requestNumber {
    // add shared data
    [json addEntriesFromDictionary:[SharedData getSharedServerParams]];
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:json
                                                       options:NSJSONWritingPrettyPrinted
                                                         error:&error];
    NSString * str = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    NSLog(@"Json String :- %@",str);
    if ([SharedData isAllowLog]) {
        //        for (id key in json) {
        //            NSLog(@"key: %@, value: %@ \n", key, [json objectForKey:key]);
        //        }
        NSLog(@"request %@ json=%@", requestId, json);
    }
    [self initMkNetworkEngine];
    
    ServerManagerResult *serverManagerResult = [[ServerManagerResult alloc] init];
    if (isGettingImages) {
        mkNetworkOperation = [mkNetworkEngine postDataToServer:nil path:url];
        
    } else {
        mkNetworkOperation = [mkNetworkEngine postDataToServer:json path:url];
    }
    
    mkNetworkOperation.postDataEncoding = MKNKPostDataEncodingTypeJSON;
    
    [mkNetworkOperation addCompletionHandler:^(MKNetworkOperation *operation)
    {
        
        //   id<ServerManagerResponseDelegate> delegate=[[self arrayDelegate]
        //   objectAtIndex:0];
        //  [delegate requestFinished:serverManagerResult ofRequestId:[self
        //  addImageAdvertiseId]];
      
        NSMutableDictionary *jsonObjectsDictionary = [operation responseJSON];
        
        if ([SharedData isAllowLog]) {
            NSLog(@"===================================================");
            NSLog(@"request finished is %@", requestId);
            NSLog(@"server respnose is =%@", jsonObjectsDictionary);
            NSLog(@"===================================================");
        }
        
        NSInteger statusCode =   [operation HTTPStatusCode];
        NSLog(@"Status Code :- %ld",(long)statusCode);
        // response status
        int status = [[jsonObjectsDictionary objectForKey:kRequestStatus] intValue];
        NSString *str = [NSString stringWithFormat:@"%d", status];
        serverManagerResult.opertationResult = str;
        
        NSString *statusMsg =
        [jsonObjectsDictionary objectForKey:kRequestStatusMsg];
        serverManagerResult.opertationMsg = statusMsg;
        
        // last update time if available
        NSString *lastUpdateTimeObject =
        [jsonObjectsDictionary objectForKey:kLastUpdateTime];
        serverManagerResult.lastUpdateTime = lastUpdateTimeObject;
        
        serverManagerResult.requestId = requestId;
        // parse the json result into server manager result
        if (jsonParserSelector != nil) {
            [self performSelector:jsonParserSelector
                       withObject:jsonObjectsDictionary
                       withObject:serverManagerResult];
        }
        
        [delegate requestFinished:serverManagerResult
                      ofRequestId:requestId
              withNumberOfRequest:requestNumber];
    } errorHandler:^(MKNetworkOperation *errorOp, NSError *error) {
        
        // response status
        
        serverManagerResult.opertationResult = [self ERROR_IN_CONNECTION_OPERATION];
        
        [delegate requestFinished:serverManagerResult
                      ofRequestId:requestId
              withNumberOfRequest:requestNumber];
        
    }];
    
    [mkNetworkEngine enqueueOperation:mkNetworkOperation];
}

+ (void)sendJsonToUrlUsingMk:(NSString *)url
                    withHost:(NSString *)strHost
                    withJson:(NSMutableDictionary *)json
                 AndDelegate:(id<ServerManagerResponseDelegate>)delegate
               withRequestId:(NSString *)requestId
      withJsonParserSelector:(SEL)jsonParserSelector
         withNumberOfRequest:(int)requestNumber {
    // add shared data
    [json addEntriesFromDictionary:[SharedData getSharedServerParams]];
    
    if ([SharedData isAllowLog]) {
        NSLog(@"request %@ json=%@", requestId, json);
    }
    mkNetworkEngine = [[MkNetworkEngine alloc] initWithHostName:strHost customHeaderFields:nil];
    ServerManagerResult *serverManagerResult = [[ServerManagerResult alloc] init];
    mkNetworkOperation = [mkNetworkEngine postDataToServer:json path:url];
    mkNetworkOperation.postDataEncoding = MKNKPostDataEncodingTypeJSON;
    [mkNetworkOperation addCompletionHandler:^(MKNetworkOperation *operation)
     {
         NSMutableDictionary *jsonObjectsDictionary = [operation responseJSON];
         
         if ([SharedData isAllowLog]) {
             NSLog(@"===================================================");
             NSLog(@"request finished is %@", requestId);
             NSLog(@"server respnose is =%@", jsonObjectsDictionary);
             NSLog(@"===================================================");
         }
         
         // response status
         int status = [[jsonObjectsDictionary objectForKey:kRequestStatus] intValue];
         NSString *str = [NSString stringWithFormat:@"%d", status];
         serverManagerResult.opertationResult = str;
         
         NSString *statusMsg =
         [jsonObjectsDictionary objectForKey:kRequestStatusMsg];
         serverManagerResult.opertationMsg = statusMsg;
         
         // last update time if available
         NSString *lastUpdateTimeObject =
         [jsonObjectsDictionary objectForKey:kLastUpdateTime];
         serverManagerResult.lastUpdateTime = lastUpdateTimeObject;
         
         serverManagerResult.requestId = requestId;
         
         // parse the json result into server manager result
         if (jsonParserSelector != nil) {
             [self performSelector:jsonParserSelector
                        withObject:jsonObjectsDictionary
                        withObject:serverManagerResult];
         }
         
         [delegate requestFinished:serverManagerResult
                       ofRequestId:requestId
               withNumberOfRequest:requestNumber];
     } errorHandler:^(MKNetworkOperation *errorOp, NSError *error) {
         
         // response status
         
         serverManagerResult.opertationResult = [self ERROR_IN_CONNECTION_OPERATION];
         
         [delegate requestFinished:serverManagerResult
                       ofRequestId:requestId
               withNumberOfRequest:requestNumber];
         
     }];
    
    [mkNetworkEngine enqueueOperation:mkNetworkOperation];
}


//============================================================table like

+ (void)addLikeToProduct:(ProductObject *)product
             AndDelegate:(id<ServerManagerResponseDelegate>)delegate
           withRequestId:(NSString *)requestId {
    //==============================================set up json to
    // send================================================
    NSArray *keys = [NSArray
                     arrayWithObjects:kProductId, @"userCountryCode", KUserNumber,kUserAdvertiseOwner, nil];
    NSArray *objects = [NSArray
                        arrayWithObjects:product.productId, [UserSetting getUserCountryCode],
                        [UserSetting getUserNumber],product.productUserNumber, nil];
    NSMutableDictionary *postParams =
    [NSMutableDictionary dictionaryWithObjects:objects forKeys:keys];
    
    // send json
    [self sendJsonToUrlUsingMk:kaddLikeToProductUrl
                      withJson:postParams
                   AndDelegate:delegate
                 withRequestId:requestId
        withJsonParserSelector:nil
           withNumberOfRequest:0];
}

//============================================================add device token

+ (void)addDeviceToken:(NSString *)DeviceToken
          withLangauge:(NSString *)language
   withUserCountryCode:(NSString *)userCountryCode
        withUserNumber:(NSString *)userNumber
           AndDelegate:(id<ServerManagerResponseDelegate>)delegate
         withRequestId:(NSString *)requestId {
    //==============================================set up json to
    // send================================================
    NSArray *keys =
    [NSArray arrayWithObjects:KUserToken, kLanguage, KUserCommentCountryCode,
     KUserNumber, KDeviceType, nil];
    
    NSArray *objects =
    [NSArray arrayWithObjects:DeviceToken, language, userCountryCode,
     userNumber, @"ios", nil];
    
    NSMutableDictionary *postParams =
    [NSMutableDictionary dictionaryWithObjects:objects forKeys:keys];
    
    // send json
    [self sendJsonToUrlUsingMk:kaddDeviceToken
                      withJson:postParams
                   AndDelegate:delegate
                 withRequestId:requestId
        withJsonParserSelector:nil
           withNumberOfRequest:0];
}

//============================================================table comment

+ (void)addCommentToProduct:(ProductObject *)product
               fromUserName:(NSString *)userName
         andUserCountryCode:(NSString *)userCountryCode
              andUserNumber:(NSString *)userNumber
                withComment:(NSString *)userComment
                AndDelegate:(id<ServerManagerResponseDelegate>)delegate
              withRequestId:(NSString *)requestId {
    //==============================================set up json to
    // send================================================
    NSArray *keys =
    [NSArray arrayWithObjects:kProductId, kUserCountryCode, KUserNumber,
     KUserName, kUserComment,kUserAdvertiseOwner, nil];
    NSArray *objects =
    [NSArray arrayWithObjects:product.productId, userCountryCode, userNumber,
     userName, userComment,product.productUserNumber, nil];
    NSMutableDictionary *postParams =
    [NSMutableDictionary dictionaryWithObjects:objects forKeys:keys];
    
    // send json
    [self sendJsonToUrlUsingMk:kaddCommentToProductUrl
                      withJson:postParams
                   AndDelegate:delegate
                 withRequestId:requestId
        withJsonParserSelector:nil
           withNumberOfRequest:0];
}

+ (void)getCommentOfProduct:(ProductObject *)product
withProductCommentsPageNumber:(int)productCommentsPageNumber
withProductCommentsNumberOfPages:(int)productCommentsNumberOfPages
withProductcommentLastUpdateTime:(NSString *)productCommentLastUpdateTime
                AndDelegate:(id<ServerManagerResponseDelegate>)delegate
              withRequestId:(NSString *)requestId {
    //    if(isNewSession)
    //    {
    //        [SharedData setproductCommentsLastUpdateTime:@"0"];
    //        productCommentsPageNumber = 1;
    //    }
    
    //==============================================set up json to
    // send================================================
    NSArray *keys = [NSArray arrayWithObjects:@"productId", @"lastUpdateTime",
                     @"page", @"numberPerPage", nil];
    
    NSString *productPageNumber =
    [NSString stringWithFormat:@"%d", productCommentsPageNumber];
    NSString *productNumberOfPages =
    [NSString stringWithFormat:@"%d", productCommentsNumberOfPages];
    
    NSArray *objects =
    [NSArray arrayWithObjects:product.productId, productCommentLastUpdateTime,
     productPageNumber, productNumberOfPages, nil];
    NSMutableDictionary *postParams =
    [NSMutableDictionary dictionaryWithObjects:objects forKeys:keys];
    
    // send json
    [self sendJsonToUrlUsingMk:kgetCommentsOfProductUrl
                      withJson:postParams
                   AndDelegate:delegate
                 withRequestId:requestId
        withJsonParserSelector:@selector(parseGetCommentOfProduct:
                                         withServerManagerResult:)
           withNumberOfRequest:0];
    
    // productCommentsPageNumber++;
}

+ (ServerManagerResult *)parseGetCommentOfProduct:(NSMutableDictionary *)json
                          withServerManagerResult:
(ServerManagerResult *)serverManagerResult {
    NSMutableArray *productArray = [[NSMutableArray alloc] init];
    
    ProductObject *productObject =
    [self getProductObject:json
              inCategoryId:nil
             withRequestId:serverManagerResult.requestId];
    
    [productArray addObject:productObject];
    
    [serverManagerResult setJsonArray:productArray];
    return serverManagerResult;
}

//============================================================Get user favorite
+ (void)getUserFavoritewithDelegate:(id<ServerManagerResponseDelegate>)delegate
         withUserProductsPageNumber:(int)userFavoritePageNumber
      withUserProductsNumberOfPages:(int)userFavoriteNumberOfPages
                      withRequestId:(NSString *)requestId
                      withUpdatTime:(NSString *)lastUpdateTime
                         byLanguage:(NSString *)lang
                      isAdSupported:(NSString *)isAdSupported
                        countOfRows:(NSString *)countOfRows
            withAdvertiseResolution:(NSString *)advertiseResolution {
    //    if(isNewSession)
    //    {
    //        [SharedData setUserFavoriteLastUpdateTime:@"0"];
    //        userFavoritePageNumber = 1;
    //    }
    
    //==============================================set up json to
    // send================================================
    NSArray *keys = [NSArray
                     arrayWithObjects:kUserCountryCode, KUserNumber, KLastUpdateTimeCapital,
                     kPage, kPerPageCapital, kproductsLanguage,
                     kisAdsSupported, kcountOfRow, kadvertiseResolution, nil];
    
    NSString *userFavoritePageNumberStr =
    [NSString stringWithFormat:@"%d", userFavoritePageNumber];
    NSString *userFavoriteNumberOfPagesStr =
    [NSString stringWithFormat:@"%d", userFavoriteNumberOfPages];
    
    NSArray *objects = [NSArray
                        arrayWithObjects:[UserSetting getUserCountryCode],
                        [UserSetting getUserNumber], lastUpdateTime,
                        userFavoritePageNumberStr, userFavoriteNumberOfPagesStr,
                        lang, isAdSupported, countOfRows, advertiseResolution,
                        nil];
    NSMutableDictionary *postParams =
    [NSMutableDictionary dictionaryWithObjects:objects forKeys:keys];
    
    // send json
    [self sendJsonToUrlUsingMk:kGetUserFavorites
                      withJson:postParams
                   AndDelegate:delegate
                 withRequestId:requestId
        withJsonParserSelector:@selector(parseProductsFromJson:
                                         withServerManagerResult:)
           withNumberOfRequest:0];
    
    // userFavoritePageNumber++;
}

//============================================================Get user products
+ (void)getUserProductsWithDelegate:(id<ServerManagerResponseDelegate>)delegate
         withUserProductsPageNumber:(int)userProductsPageNumber
      withUserProductsNumberOfPages:(int)userProductsNumberOfPages
                      withRequestId:(NSString *)requestId
                      withUpdatTime:(NSString *)lastUpdateTime
                         byLanguage:(NSString *)lang
                      isAdSupported:(NSString *)isAdSupported
                        countOfRows:(NSString *)countOfRows
            withAdvertiseResolution:(NSString *)advertiseResolution
              andVisitorCountryCode:(NSString *)visitorCountryCode
                   andVisitorNumber:(NSString *)visitorNumber {
    
    // if(isNewSession)
    //{
    //[SharedData setUserProductsLastUpdateTime:@"0"];
    // userProductsPageNumber = 1;
    //}
    
    //==============================================set up json to
    // send================================================
    NSArray *keys =
    [NSArray arrayWithObjects:kVisitorCountryCode, KVisitorNumber,
     KLastUpdateTimeCapital, kPage, kPerPageCapital,
     kproductsLanguage, kisAdsSupported, kcountOfRow,
     kadvertiseResolution, nil];
    
    NSString *userProductsPageNumberStr =
    [NSString stringWithFormat:@"%d", userProductsPageNumber];
    NSString *userProductsNumberOfPagesStr =
    [NSString stringWithFormat:@"%d", userProductsNumberOfPages];
    
    if ([SharedData isAllowLog]) {
        NSLog(@"=====================================================");
        NSLog(@"country code=%@", visitorCountryCode);
        NSLog(@"user number=%@", visitorNumber);
        NSLog(@"user last update time=%@", lastUpdateTime);
        NSLog(@"count of row=%@", countOfRows);
        NSLog(@"advertise resolution=%@", advertiseResolution);
        NSLog(@"is AdSupported=%@", isAdSupported);
        NSLog(@"=====================================================");
    }
    if(visitorCountryCode == nil)
        visitorCountryCode = @"";
    NSArray *objects = [NSArray
                        arrayWithObjects:visitorCountryCode, visitorNumber, lastUpdateTime,
                        userProductsPageNumberStr, userProductsNumberOfPagesStr,
                        lang, isAdSupported, countOfRows, advertiseResolution,
                        nil];
    NSMutableDictionary *postParams =
    [NSMutableDictionary dictionaryWithObjects:objects forKeys:keys];
    
    // send json
    [self sendJsonToUrlUsingMk:kGetUserProducts
                      withJson:postParams
                   AndDelegate:delegate
                 withRequestId:requestId
        withJsonParserSelector:@selector(parseProductsFromJson:
                                         withServerManagerResult:)
           withNumberOfRequest:0];
    // userProductsPageNumber++;
}

+ (ServerManagerResult *)parseNotificationsFromJson:(NSMutableDictionary *)json
                            withServerManagerResult:
(ServerManagerResult *)serverManagerResult {
    //    NSLog(@"JSON : %@",json);
    
    NSMutableArray *notificationArray = [[NSMutableArray alloc] init];
    
    NSArray *jsonObjects = [json objectForKey:kNotifications];
    
    [serverManagerResult
     setNotifcationCount:[json objectForKey:KNewNotificationCount]];
    
    for (int i = 0; i < jsonObjects.count; i++) {
        
        // get dictionary of eac
        NSDictionary *productDictionary = [jsonObjects objectAtIndex:i];
        
        NotificationModal *notificationObject = [[NotificationModal alloc] init];
        notificationObject.notificationInfo =
        [productDictionary valueForKey:KNotificationInfo];
        notificationObject.isRead = [productDictionary valueForKey:KIsRead];
        notificationObject.notificationId =
        [productDictionary valueForKey:KNotificationId];
        notificationObject.notificationImage =
        [productDictionary valueForKey:KNotificationImage];
        notificationObject.notificationTime =
        [productDictionary valueForKey:KNotificationTime];
        notificationObject.productId = [productDictionary valueForKey:kProductId];
        notificationObject.notificationCategoryId=[productDictionary valueForKey:kCategoryId];
        [notificationArray addObject:notificationObject];
    }
    
    [serverManagerResult setJsonArray:notificationArray];
    [serverManagerResult setLastUpdateTime:[json objectForKey:kLastUpdateTime]];
    
    return serverManagerResult;
}

//============================================================parse products
+ (ServerManagerResult *)parseProductsFromJsonForMoreInfo:(NSMutableDictionary *)json
                       withServerManagerResult:
(ServerManagerResult *)serverManagerResult{
    [serverManagerResult setJsonArray:[json objectForKey:kProperties]];
    return serverManagerResult;
}

+ (ServerManagerResult *)parseProductsFromJson:

                               (NSMutableDictionary *)json
                               withServerManagerResult:
(ServerManagerResult *)serverManagerResult{
    
    NSMutableArray *productArray = [[NSMutableArray alloc] init];
   
    NSArray *jsonObjects = [json objectForKey:kProducts];
    
    
    if(jsonObjects == nil){
    
    
        jsonObjects = [json objectForKey:@""];
    
    }
    
   
    [self parseProductsArray:jsonObjects andResult: productArray andJson: json withServerManagerResult: serverManagerResult];
    
    [serverManagerResult setJsonArray:productArray];
    [serverManagerResult setLastUpdateTimeForCategories:
     [json objectForKey:kLastUpdateTimeForCategories]];
    [serverManagerResult
     setLastUpdateTimeForSortBy:[json objectForKey:kLastUpdateTimeForSortBy]];
    [serverManagerResult
     setLastUpdateTimeForCities:[json objectForKey:kLastUpdateTimeForCities]];
    
    // user profile
    serverManagerResult.userProfileObject.productCountryCode =
    [json objectForKey:kUserCountryCode];
    serverManagerResult.userProfileObject.productUserNumber =
    [json objectForKey:KUserNumber];
    serverManagerResult.userProfileObject.productUserNumberOfAds =
    [json objectForKey:kUserNumberOfAds];
    serverManagerResult.userProfileObject.productUserName =
    [json objectForKey:KUserName];
    serverManagerResult.userProfileObject.productUserImageUrl =
    [json objectForKey:kUserPhoto];
    serverManagerResult.userProfileObject.productUserShortDescription =
    [json objectForKey:kUserSmallDescription];
    serverManagerResult.userProfileObject.productUserEmail =
    [json objectForKey:KUserEmail];
    serverManagerResult.userProfileObject.productUserUrl=[json objectForKey:KUserProfileUrl];
    
    serverManagerResult.userProfileObject.productUserisCompany = [json objectForKey:KUserCompany];
    serverManagerResult.userProfileObject.productUserlang =
    [json objectForKey:KUserLang];
    serverManagerResult.userProfileObject.productUserlong =
    [json objectForKey:KUserLong];
    serverManagerResult.userProfileObject.productUserAddress = [json objectForKey:KUserAddress];
    
    if ([json objectForKey:KUserNumber]) {
        
        if ([[json objectForKey:KUserNumber] isEqualToString:[UserSetting getUserNumber]]) {
            [UserSetting saveUserSettingWithName:[json objectForKey:KUserName] withCountryCode:[json objectForKey:kUserCountryCode] withNumber:[json objectForKey:KUserNumber] withEmail:[json objectForKey:KUserEmail]];
            [UserSetting saveUserPhoto:[json objectForKey:kUserPhoto]];
        }
    }
    
    return serverManagerResult;
}

+ (void) parseProductsArray:(NSArray *) jsonObjects andResult:(NSMutableArray *) productArray andJson:(NSMutableDictionary *) json withServerManagerResult:(ServerManagerResult *) serverManagerResult{

    for (int i = 0; i < jsonObjects.count; i++) {
        
        // get dictionary of eac
        NSDictionary *productDictionary = [jsonObjects objectAtIndex:i];
        
        ProductObject *productObject =
        [self getProductObject:productDictionary
                  inCategoryId:nil
                 withRequestId:serverManagerResult.requestId];
       
        productObject.productUserlang = [json objectForKey:KUserLang];
        productObject.productUserlong = [json objectForKey:KUserLong];
        productObject.productUserAddress = [json objectForKey:KUserAddress];
    
        [productArray addObject:productObject];
    }
}

+ (ServerManagerResult *)parseImagesfromjson:(NSMutableDictionary *)json
                     withServerManagerResult:
(ServerManagerResult *)serverManagerResult {
    //    NSLog(@"JSON : %@",json);
    
    NSMutableArray *imagesArray = [[NSMutableArray alloc] init];
    
    NSArray *jsonObjects = [json objectForKey:kImages];
    
    for (int i = 0; i < jsonObjects.count; i++) {
        
        // get dictionary of eac
        NSDictionary *imageDictionary = [jsonObjects objectAtIndex:i];
        
        SelectImageModal *imageObject = [[SelectImageModal alloc] init];
        imageObject.imageId = [imageDictionary valueForKey:@"imageID"];
        imageObject.imageUrl = [imageDictionary valueForKey:@"ImageUrl"];
        imageObject.imageCharacterLimit =
        [imageDictionary valueForKey:@"imageCharacterLimit"];
        imageObject.imageTextColor =
        [imageDictionary valueForKey:@"imageTextColor"];
        imageObject.imageXPosition =
        [imageDictionary valueForKey:@"imageXPosition"];
        imageObject.imageYPosition =
        [imageDictionary valueForKey:@"imageYPosition"];
        imageObject.imageTextSize = [imageDictionary valueForKey:@"imageFontSize"];
        
        [imagesArray addObject:imageObject];
    }
    
    [serverManagerResult setJsonArray:imagesArray];
    [serverManagerResult setLastUpdateTime:[json objectForKey:kLastUpdateTime]];
    
    return serverManagerResult;
}

//============================================================add productto
// favorite
//{"user_country_code":"00974","user_number":"66524147","productId":"3"}
+ (void)addProductToFavorite:(ProductObject *)productObject
                 AndDelegate:(id<ServerManagerResponseDelegate>)delegate
               withRequestId:(NSString *)requestId
                   withIsAdd:(NSString *)isAd {
    //==============================================set up json to
    // send================================================
    NSArray *keys = [NSArray
                     arrayWithObjects:kProductId, kUserCountryCode, KUserNumber, kisAd, nil];
    NSArray *objects =
    [NSArray arrayWithObjects:productObject.productId,
     [UserSetting getUserCountryCode],
     [UserSetting getUserNumber], isAd, nil];
    NSMutableDictionary *postParams =
    [NSMutableDictionary dictionaryWithObjects:objects forKeys:keys];
    
    // send json
    [self sendJsonToUrlUsingMk:kaddProductToFavorite
                      withJson:postParams
                   AndDelegate:delegate
                 withRequestId:requestId
        withJsonParserSelector:nil
           withNumberOfRequest:0];
}

//============================================================delete product
// item
+ (void)deleteProduct:(ProductObject *)productObject
          AndDelegate:(id<ServerManagerResponseDelegate>)delegate
        withRequestId:(NSString *)requestId {
    //==============================================set up json to
    // send================================================
    NSArray *keys = [NSArray arrayWithObjects:kProductId, nil];
    NSArray *objects = [NSArray arrayWithObjects:productObject.productId, nil];
    NSMutableDictionary *postParams =
    [NSMutableDictionary dictionaryWithObjects:objects forKeys:keys];
    
    // send json
    [self sendJsonToUrlUsingMk:kDeleteAdvertiseUrl
                      withJson:postParams
                   AndDelegate:delegate
                 withRequestId:requestId
        withJsonParserSelector:nil
           withNumberOfRequest:0];
}

//============================================================refresh advertise
+ (void)refreshAdvertise:(ProductObject *)productObject
          andCountryCode:(NSString *)userCountryCode
           andUserNumber:(NSString *)userNumber
         andUserLanguage:(NSString *)language
             AndDelegate:(id<ServerManagerResponseDelegate>)delegate
           withRequestId:(NSString *)requestId {
    //==============================================set up json to
    // send================================================
    NSArray *keys = [NSArray arrayWithObjects:kUserCountryCode, KUserNumber,
                     kProductId, kLanguage, nil];
    NSArray *objects =
    [NSArray arrayWithObjects:userCountryCode, userNumber,
     productObject.productId, language, nil];
    NSMutableDictionary *postParams =
    [NSMutableDictionary dictionaryWithObjects:objects forKeys:keys];
    
    // send json
    [self sendJsonToUrlUsingMk:kRefreshAdvertise
                      withJson:postParams
                   AndDelegate:delegate
                 withRequestId:requestId
        withJsonParserSelector:nil
           withNumberOfRequest:0];
}

//============================================================delete user
// favorite
+ (void)deleteUserFavorite:(ProductObject *)productObject
               AndDelegate:(id<ServerManagerResponseDelegate>)delegate
             withRequestId:(NSString *)requestId {
    //==============================================set up json to
    // send================================================
    NSArray *keys = [NSArray
                     arrayWithObjects:KUserCommentCountryCode, KUserNumber, kProductId, nil];
    NSArray *objects = [NSArray arrayWithObjects:[UserSetting getUserCountryCode],
                        [UserSetting getUserNumber],
                        productObject.productId, nil];
    NSMutableDictionary *postParams =
    [NSMutableDictionary dictionaryWithObjects:objects forKeys:keys];
    
    // send json
    [self sendJsonToUrlUsingMk:kDeleteUserFavorite
                      withJson:postParams
                   AndDelegate:delegate
                 withRequestId:requestId
        withJsonParserSelector:nil
           withNumberOfRequest:0];
}

//================================report comment
+ (void)reportComment:(ProductCommentObject *)productCommentObject
          AndDelegate:(id<ServerManagerResponseDelegate>)delegate
        withRequestId:(NSString *)requestId{
    
    //==============================================set up json to
    // send================================================
    NSArray *keys = [NSArray
                     arrayWithObjects:kCommentId, kreportReadonID, kReportReasonText, nil];
    
    NSArray *objects =
    [NSArray arrayWithObjects:productCommentObject.commentId,
     productCommentObject.reportReasonId,
     productCommentObject.reportReasonText, nil];
    NSMutableDictionary *postParams =
    [NSMutableDictionary dictionaryWithObjects:objects forKeys:keys];
    
    // send json
    [self sendJsonToUrlUsingMk:kreportComment
                      withJson:postParams
                   AndDelegate:delegate
                 withRequestId:requestId
        withJsonParserSelector:nil
           withNumberOfRequest:0];
}
//================================== Block User From Commnet
+ (void)blockUserFromComment:(ProductCommentObject *)productCommentObject andProductObj:(ProductObject *)productObject
          AndDelegate:(id<ServerManagerResponseDelegate>)delegate
        withRequestId:(NSString *)requestId {
    
    //==============================================set up json to
    // send================================================
    NSArray *keys = [NSArray
                     arrayWithObjects:KuserCountryToBeBlocked, KuserNumberToBeBlocked, nil];
    NSArray *objects =
    @[productCommentObject.commenterCountryCode,
     productCommentObject.commenterNumber];
    NSMutableDictionary *postParams =
    [NSMutableDictionary dictionaryWithObjects:objects forKeys:keys];
    
    // send json
    [self sendJsonToUrlUsingMk:kblockUser
                      withJson:postParams
                   AndDelegate:delegate
                 withRequestId:requestId
        withJsonParserSelector:nil
           withNumberOfRequest:0];
}
//================================== get related Ads
+ (void)getRelateAds:(ProductObject *)productObject
                 AndDelegate:(id<ServerManagerResponseDelegate>)delegate
               withRequestId:(NSString *)requestId {
    
    //==============================================set up json to
    // send================================================
    
    NSArray *keys = [NSArray
                     arrayWithObjects:kProductId,Kcatid,kproductsLanguage,kproductSubCategoryId,kcategoryAdvertiseTypesId,kSubSubCategoriesId, nil];
    
    if (productObject.productCategoryId == nil || productObject.productCategoryId == (id)[NSNull null]) {
        productObject.productCategoryId = @"";
    }
    NSArray *objects =
    @[productObject.productId,productObject.productCategoryId,productObject.productLanguage,productObject.productSubCategoryId,productObject.productAdvertiseTypeId,productObject.productSubsubCategoryId];
    NSMutableDictionary *postParams =
    [NSMutableDictionary dictionaryWithObjects:objects forKeys:keys];
    
    // send json
    [self sendJsonToUrlUsingMk:kRelatedAds
                      withJson:postParams
                   AndDelegate:delegate
                 withRequestId:requestId
                          withJsonParserSelector:@selector(parseProductsFromJson:
                                                           withServerManagerResult:)
           withNumberOfRequest:0];
    
}
+ (void)readAllNotification:(id<ServerManagerResponseDelegate>)delegate
       withRequestId:(NSString *)requestId {
    
    //==============================================set up json to
    // send================================================
    NSArray *keys = [NSArray
                     arrayWithObjects:kUserCountryCode,KUserNumber, nil];
    NSArray *objects =
    @[[UserSetting getUserCountryCode],[UserSetting getUserNumber]];
    NSMutableDictionary *postParams =
    [NSMutableDictionary dictionaryWithObjects:objects forKeys:keys];
    
    // send json
    [self sendJsonToUrlUsingMk:kAllNotificationRead
                                        withJson:postParams
                                     AndDelegate:delegate
                                   withRequestId:requestId
                          withJsonParserSelector:nil withNumberOfRequest:0];
}
+ (void)deleteNotification:(NSString *)notificationId
                       andRequestId:(NSString *)requestId
                        andDelegate:(id<ServerManagerResponseDelegate>)delegate{
//    NSArray *keys = [NSArray
//                     arrayWithObjects:kUserCountryCode,KUserNumber, nil];
//    NSArray *objects =
//    @[[UserSetting getUserCountryCode],[UserSetting getUserNumber]];
//    NSMutableDictionary *postParams =
//    [NSMutableDictionary dictionaryWithObjects:objects forKeys:keys];
    NSMutableDictionary * post =[[NSMutableDictionary alloc]init];
    [post setValue:notificationId forKey:@"notificationId"];
    [self sendJsonToUrlUsingMk:kDeleteNotification
                      withJson:post
                   AndDelegate:delegate
                 withRequestId:requestId
        withJsonParserSelector:nil
           withNumberOfRequest:0];
}
+ (void)deleteAllNotification:(id<ServerManagerResponseDelegate>)delegate
                withRequestId:(NSString *)requestId
{
    NSArray *keys = [NSArray
                     arrayWithObjects:kUserCountryCode,KUserNumber, nil];
    NSArray *objects =
    @[[UserSetting getUserCountryCode],[UserSetting getUserNumber]];
    NSMutableDictionary *postParams =
    [NSMutableDictionary dictionaryWithObjects:objects forKeys:keys];
    
    // send json
    [self sendJsonToUrlUsingMk:kDeleteAllNotification
                                        withJson:postParams
                                     AndDelegate:delegate
                                   withRequestId:requestId
                          withJsonParserSelector:nil
                             withNumberOfRequest:0];
}
//================================== Block User From Commnet
+ (void)getMoreInfo:(ProductObject *)productObject
         AndDelegate:(id<ServerManagerResponseDelegate>)delegate
       withRequestId:(NSString *)requestId {
    
    //==============================================set up json to
    // send================================================
    NSArray *keys = [NSArray
                     arrayWithObjects:kUserCountryCode,KUserNumber,kProductId,kproductsLanguage,KuserDeviceLanguage, nil];
    if (productObject.productLanguage == nil)
    {
        productObject.productLanguage = @"";
    }
    NSArray *objects = [NSArray new];
        objects =
        @[[UserSetting getUserCountryCode],[UserSetting getUserNumber],productObject.productId,productObject.productLanguage,[UserSetting getUserCurrentLanguage]];

    NSMutableDictionary *postParams =
    [NSMutableDictionary dictionaryWithObjects:objects forKeys:keys];
    
    // send json
    [self sendJsonToUrlUsingMk:kMoreInfo
                                        withJson:postParams
                                     AndDelegate:delegate
                                   withRequestId:requestId
                          withJsonParserSelector:@selector(parseProductsFromJsonForMoreInfo:
                                                           withServerManagerResult:)
                             withNumberOfRequest:0];
}
+ (void)reportAdvertise:(ProductObject *)productObject
            AndDelegate:(id<ServerManagerResponseDelegate>)delegate
          withRequestId:(NSString *)requestId {
    
    //==============================================set up json to
    // send================================================
    NSArray *keys = [NSArray
                     arrayWithObjects:kProductId, kreportReadonID, kReportReasonText, nil];
    
    NSArray *objects = [NSArray
                        arrayWithObjects:productObject.productId, productObject.reportReasonId,
                        productObject.reportReasonText, nil];
    NSMutableDictionary *postParams =
    [NSMutableDictionary dictionaryWithObjects:objects forKeys:keys];
    
    // send json
    [self sendJsonToUrlUsingMk:kreportAdvertise
                      withJson:postParams
                   AndDelegate:delegate
                 withRequestId:requestId
        withJsonParserSelector:nil
           withNumberOfRequest:0];
}

//================================delete comment
+ (void)deleteComment:(ProductCommentObject *)commentObject
          AndDelegate:(id<ServerManagerResponseDelegate>)delegate
        withRequestId:(NSString *)requestId
        withProductId:(NSString *)productId {
    //==============================================set up json to
    // send================================================
    NSArray *keys = [NSArray arrayWithObjects:kCommentId, kProductId, nil];
    NSArray *objects =
    [NSArray arrayWithObjects:commentObject.commentId, productId, nil];
    NSMutableDictionary *postParams =
    [NSMutableDictionary dictionaryWithObjects:objects forKeys:keys];
    
    // send json
    [self sendJsonToUrlUsingMk:kdeleteComment
                      withJson:postParams
                   AndDelegate:delegate
                 withRequestId:requestId
        withJsonParserSelector:nil
           withNumberOfRequest:0];
}

//======================================Get Push Notification

+ (void)getPushNotification:(NSString *)pushNotificationUrl {
    [self sendJsonToUrlUsingMk:kGetPushNotification
                      withJson:nil
                   AndDelegate:nil
                 withRequestId:nil
        withJsonParserSelector:nil
           withNumberOfRequest:nil];
}

//======================================Get Notification
+ (void)getNotificationWithCountryCode:(NSString *)userCountry
                             andNumber:(NSString *)userNumber
                     andLastUpdateTime:(NSString *)lastUpdateTime
                               andPage:(NSString *)page
                      andNumberPerPage:(NSString *)numberPerPage
                           andLanguage:(NSString *)language
                          andRequestId:(NSString *)requestId
                           andDelegate:
(id<ServerManagerResponseDelegate>)delegate {
    NSArray *keys = [NSArray arrayWithObjects:kUserCountryCode, KUserNumber,
                     KLastUpdateTimeCapital, kPage,
                     kPerPageCapital, nil];
    NSArray *objects =
    [NSArray arrayWithObjects:userCountry, userNumber, lastUpdateTime, page,
     numberPerPage, nil];
    NSMutableDictionary *jsonDictionary =
    [NSMutableDictionary dictionaryWithObjects:objects forKeys:keys];
    
    [self sendJsonToUrlUsingMk:kGetNotification
                      withJson:jsonDictionary
                   AndDelegate:delegate
                 withRequestId:requestId
        withJsonParserSelector:@selector(parseNotificationsFromJson:
                                         withServerManagerResult:)
           withNumberOfRequest:0];
}

+ (void)stopNotificationOfProductId:(NSString *)productId
                  andNotificationId:(NSString *)notificationId
                       andRequestId:(NSString *)requestId
                        andDelegate:(id<ServerManagerResponseDelegate>)delegate {
    NSArray *keys = [NSArray arrayWithObjects:kProductId, KNotificationId, nil];
    NSArray *objects = [NSArray arrayWithObjects:productId, notificationId, nil];
    NSMutableDictionary *jsonDictionary =
    [NSMutableDictionary dictionaryWithObjects:objects forKeys:keys];
    
    [self sendJsonToUrlUsingMk:kStopNotification
                      withJson:jsonDictionary
                   AndDelegate:delegate
                 withRequestId:requestId
        withJsonParserSelector:nil
           withNumberOfRequest:0];
}

//{"userCountry":"00974","userNumber":"66524147","userPhoto":"123124413","userSmallDescription":"asdasdas","userName":"mohammed","userEmail":"userEmail"}
//======================================edit user profile
+ (void)updateUserProfile:(ProductObject *)productObject
             andRequestId:(NSString *)requestId
              andDelegate:(id<ServerManagerResponseDelegate>)delegate {
    NSData *data =
    UIImageJPEGRepresentation(productObject.productUserImage, 1.0f);
    [Base64 initialize];
    NSString *strEncoded = [Base64 encode:data];
    
    NSArray *keys = [NSArray arrayWithObjects:KUserNumber,kUserPhoto, kUserSmallDescription,
                     KUserName, KUserEmail,KUserLang,KUserLong,KUserAddress, nil];
    NSArray *objects = [NSArray
                        arrayWithObjects:productObject.productUserNumber,strEncoded, productObject.productUserShortDescription,
                        productObject.productUserName,
                        productObject.productUserEmail,productObject.productUserlang==nil?@"":productObject.productUserlang,productObject.productUserlong==nil?@"":productObject.productUserlong,productObject.productUserAddress==nil?@"":productObject.productUserAddress, nil];
    NSMutableDictionary *jsonDictionary =
    [NSMutableDictionary dictionaryWithObjects:objects forKeys:keys];
    
    [self sendJsonToUrlUsingMk:kUpdateUserProfile
                      withJson:jsonDictionary
                   AndDelegate:delegate
                 withRequestId:requestId
        withJsonParserSelector:nil
           withNumberOfRequest:0];
}

//======================================Get sortBy filters
+ (void)getSortByFilters:(id<ServerManagerResponseDelegate>)delegate
     withUserCountryCode:(NSString *)userCountryCode
              withNumber:(NSString *)userNumber
            withLanguage:(NSString *)language
           withRequestId:(NSString *)requestId {
    //==============================================set up json to
    // send================================================
    NSArray *keys = [NSArray
                     arrayWithObjects:KUserCommentCountryCode, KUserNumber, kLanguage, nil];
    NSArray *objects =
    [NSArray arrayWithObjects:userCountryCode, userNumber, language, nil];
    NSMutableDictionary *postParams =
    [NSMutableDictionary dictionaryWithObjects:objects forKeys:keys];
    
    // send json
    [self sendJsonToUrlUsingMk:kGetSortByFilters
                      withJson:postParams
                   AndDelegate:delegate
                 withRequestId:requestId
        withJsonParserSelector:@selector(parseSortByFIlters:
                                         withServerManagerResult:)
           withNumberOfRequest:0];
}

//============================================================parse sortBy
// filters
+ (ServerManagerResult *)parseSortByFIlters:(NSMutableDictionary *)json
                    withServerManagerResult:
(ServerManagerResult *)serverManagerResult {
    NSMutableArray *sortByArray = [[NSMutableArray alloc] init];
    
    NSArray *jsonObjects = [json objectForKey:kSortByfilter];
    
    for (int i = 0; i < jsonObjects.count; i++) {
        
        // get dictionary of eac
        NSDictionary *productDictionary = [jsonObjects objectAtIndex:i];
        
        SortByObject *soryByObject = [[SortByObject alloc] init];
        soryByObject.sortByFilterId =
        [productDictionary valueForKey:kSortByfilterId];
        soryByObject.SortByFilterName =
        [productDictionary valueForKey:kSortByfilterName];
        soryByObject.SortByFilterImageUrl =
        [productDictionary valueForKey:kSortByfilterImage];
        
        [sortByArray addObject:soryByObject];
    }
    
    [serverManagerResult setJsonArray:sortByArray];
    
    return serverManagerResult;
}

//======================================Get sortBy filters
+ (void)getCitiesAndRegion:(id<ServerManagerResponseDelegate>)delegate
       withUserCountryCode:(NSString *)userCountryCode
                withNumber:(NSString *)userNumber
              withLanguage:(NSString *)language
             withRequestId:(NSString *)requestId {
    //==============================================set up json to
    // send================================================
    NSArray *keys =
    [NSArray arrayWithObjects:KUserCommentCountryCode, KUserNumber, kLanguage, nil];
    NSArray *objects = [NSArray arrayWithObjects:userCountryCode, userNumber,
                        language, nil];
    NSMutableDictionary *postParams =
    [NSMutableDictionary dictionaryWithObjects:objects forKeys:keys];
    
    // send json
    [self sendJsonToUrlUsingMk:kGetCitiesAndRegion
                      withJson:postParams
                   AndDelegate:delegate
                 withRequestId:requestId
        withJsonParserSelector:@selector(parseCitiesAndRegion:
                                         withServerManagerResult:)
           withNumberOfRequest:0];
}

+ (void)getCompaniesDirectoriesRequest:(NSString *)lastUpdateTime
                               AndPage:(int)page
                       AndNumberOfPage:(int)numberOfPage
                           AndLanguage:(NSString *)userDeviceLanguage
                        withCategoryId:(NSString *)categoryId
                        AndSearchKey:(NSString *)searchKey
                           AndDelegate:(id<ServerManagerResponseDelegate>)delegate
                         withRequestId:(NSString *)requestId{

    NSArray *keys = [NSArray
                     arrayWithObjects:KLastUpdateTimeCapital,
                     kPage, kPerPageCapital, KuserDeviceLanguage,
                     kProductCategoryId,kSearchStr, nil];
    
    NSString *companiesPageNumber =
    [NSString stringWithFormat:@"%d", page];
    NSString *companiesNumberOfPages =
    [NSString stringWithFormat:@"%d", numberOfPage];
    /////
    NSArray *objects = [NSArray arrayWithObjects:lastUpdateTime, companiesPageNumber,
                        companiesNumberOfPages,userDeviceLanguage,categoryId,searchKey, nil];
    NSMutableDictionary *postParams =
    [NSMutableDictionary dictionaryWithObjects:objects forKeys:keys];
    
    // send json
    [self sendJsonToUrlUsingMk:kGetCompaniesDirectoriesUrl
                      withJson:postParams
                   AndDelegate:delegate
                 withRequestId:requestId
        withJsonParserSelector:@selector(parseCompaniesDirectories:
                                         withServerManagerResult:)
           withNumberOfRequest:0];

}
+ (ServerManagerResult *)parseCompaniesDirectories:(NSMutableDictionary *)json
                      withServerManagerResult:
(ServerManagerResult *)serverManagerResult {
    
    NSArray *jsonObjects = [json objectForKey:kCompanies];
    
    NSMutableArray *productArray = [[NSMutableArray alloc] init];
    
    if(jsonObjects == nil){
        
        
        jsonObjects = [json objectForKey:@""];
        
    }
    
    
   [self parseProductsArray:jsonObjects andResult: productArray andJson: json withServerManagerResult: serverManagerResult];
    
    [serverManagerResult setJsonArray:productArray];
    
    return serverManagerResult;
}
//============================================================parse sortBy
// filters
+ (ServerManagerResult *)parseCitiesAndRegion:(NSMutableDictionary *)json
                      withServerManagerResult:
(ServerManagerResult *)serverManagerResult {
    NSMutableArray *citiesArry = [[NSMutableArray alloc] init];
    
    NSArray *jsonObjects = [json objectForKey:kCities];
    
    for (int i = 0; i < jsonObjects.count; i++) {
        
        NSMutableArray *regionsArray = [[NSMutableArray alloc] init];
        
        // get dictionary of eac
        NSDictionary *productDictionary = [jsonObjects objectAtIndex:i];
        
        CitiesObject *citiesObject = [[CitiesObject alloc] init];
        citiesObject.cityId = [productDictionary valueForKey:kCityId];
        citiesObject.cityName = [productDictionary valueForKey:KCityName];
        
        for (NSDictionary *regionDictionary in
             [productDictionary valueForKey:KRegions]) {
            
            RegionsObject *regionsObject = [[RegionsObject alloc] init];
            regionsObject.regionId = [regionDictionary valueForKey:KRegionId];
            regionsObject.regionName = [regionDictionary valueForKey:KRegionName];
            [regionsArray addObject:regionsObject];
        }
        
        citiesObject.regions = regionsArray;
        
        [citiesArry addObject:citiesObject];
    }
    
    [serverManagerResult setJsonArray:citiesArry];
    
    return serverManagerResult;
}

//============================================================set notification id read
+ (void)setNotificationRead:(id<ServerManagerResponseDelegate>)delegate withNotificationId:(NSString*)notificationId withRequestId:(NSString *)requestId {
    //==============================================set up json to
    // send================================================
    NSArray *keys = [NSArray
                     arrayWithObjects:KNotificationId, nil];
    NSArray *objects = [NSArray arrayWithObjects:notificationId, nil];
    NSMutableDictionary *postParams =
    [NSMutableDictionary dictionaryWithObjects:objects forKeys:keys];
    
    // send json
    [self sendJsonToUrlUsingMk:kSetNotificationRead
                      withJson:postParams
                   AndDelegate:delegate
                 withRequestId:requestId
        withJsonParserSelector:nil
           withNumberOfRequest:0];
}

#pragma mark -
#pragma mark - Search API Call

+ (void)getTrendingSearch:(id<ServerManagerResponseDelegate>)delegate withLanguage:(NSString *)language
            withRequestId:(NSString *)requestId
{
    NSArray *keys = [NSArray arrayWithObjects:kLanguage, nil];
    NSArray *objects = [NSArray arrayWithObjects:language, nil];
    
    NSMutableDictionary *jsonDictionary =
    [NSMutableDictionary dictionaryWithObjects:objects forKeys:keys];
    [self sendJsonToUrlUsingMk:kGetTrendingSearch withHost:kHostName withJson:jsonDictionary AndDelegate:delegate withRequestId:requestId withJsonParserSelector:@selector(parseTrendingSearch:withServerManagerResult:) withNumberOfRequest:0];
}

+ (void)getTextSearch:(id<ServerManagerResponseDelegate>)delegate withSearchText:(NSString *)strText withCategoryId:(NSString *)strId
         withLanguage:(NSString *)language
        withRequestId:(NSString *)requestId
{
    NSArray *keys;
    NSArray *objects;
    if (strId.length) {
        keys = [NSArray arrayWithObjects:kLanguage,kSearchKey,kSearchId, nil];
        objects = [NSArray arrayWithObjects:language,strText,strId, nil];
    }else{
        keys = [NSArray arrayWithObjects:kLanguage,kSearchKey, nil];
        objects = [NSArray arrayWithObjects:language,strText, nil];
    }
    
    
    NSMutableDictionary *jsonDictionary =
    [NSMutableDictionary dictionaryWithObjects:objects forKeys:keys];
    [self sendJsonToUrlUsingMk:kGetTextSearch withHost:kHostName withJson:jsonDictionary AndDelegate:delegate withRequestId:requestId withJsonParserSelector:@selector(parseTrendingSearch:withServerManagerResult:) withNumberOfRequest:0];
}

+ (ServerManagerResult *)parseTrendingSearch:(NSMutableDictionary *)json
                     withServerManagerResult:(ServerManagerResult *)serverManagerResult{
    
    NSMutableArray *trendingArray = [[NSMutableArray alloc] init];
    trendingArray = [json objectForKey:kTrendingKeywords];
    [serverManagerResult setJsonArray:trendingArray];
    return serverManagerResult;
    
}


+ (NSString *)SUCCESS_OPERATION {
    return @"0";
}

+ (NSString *)FAILED_OPERATION {
    return @"1";
}

+ (NSString *)ALREADED_ADDED_OPERATION {
    return @"2";
}

+ (NSString *)ERROR_IN_CONNECTION_OPERATION {
    return @"10";
}

+ (NSString *)ERROR_USER_BLOCKED_FROM_ADMIN {
    return @"3";
}

+ (NSString *)ERROR_USER_BLOCKED_BY_ANOTHER_USER {
    return @"4";
}
+ (NSString *)REGISTER_AS_COMPANY {
    return @"5";
}
@end
