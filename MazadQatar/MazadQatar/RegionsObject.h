//
//  RegionsObject.h
//  Mzad Qatar
//
//  Created by samo on 8/28/14.
//  Copyright (c) 2014 Mohammed abdulfattah attia. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RegionsObject : NSObject <NSCoding>

@property(nonatomic, strong) NSString *regionId;
@property(nonatomic, strong) NSString *regionName;

@end
