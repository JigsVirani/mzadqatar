//
//  SharedInfo.m
//  MazadQatar
//
//  Created by samo on 3/5/13.
//  Copyright (c) 2013 Mohammed abdulfattah attia. All rights reserved.
//

#import "SharedGraphicInfo.h"
#import "DisplayUtility.h"

@implementation SharedGraphicInfo

+ (NSMutableArray *)getcurrentUploadingProductList {
  static NSMutableArray *currentUploadingProductList;

  if (!currentUploadingProductList) {
    currentUploadingProductList = [[NSMutableArray alloc] init];
  }

  return currentUploadingProductList;
}

+ (void)addToUploadingProductList:(ProductObject *)productObject {
  [[self getcurrentUploadingProductList] addObject:productObject];
}

@end
