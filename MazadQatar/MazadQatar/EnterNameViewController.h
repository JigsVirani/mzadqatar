//
//  EnterNameViewController.h
//  Mzad Qatar
//
//  Created by Paresh Kacha on 30/11/15.
//  Copyright © 2015 Mohammed abdulfattah attia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TextFieldViewController.h"
@protocol EnterNameViewControllerDelegate <NSObject>
- (void)textEntered:(NSString *)text;
@end

@interface EnterNameViewController: TextFieldViewController <UITextFieldDelegate>

- (IBAction)closeDialogClicked:(id)sender;
- (IBAction)sendClicked:(id)sender;
@property(strong, nonatomic) IBOutlet UITextField *textToBeWrittenField;


@property(nonatomic, unsafe_unretained)
id<EnterNameViewControllerDelegate> delegate;
@property int maxCharacterLimit;
@end
