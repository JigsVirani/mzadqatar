//
//  MoreDescriptionViewController.h
//  Mzad Qatar
//
//  Created by Paresh Kacha on 01/12/15.
//  Copyright © 2015 Mohammed abdulfattah attia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ServerManager.h"
#import "MBProgressHUD.h"

#import "GAITrackedViewController.h"

@interface MoreDescriptionViewController : GAITrackedViewController<UITableViewDataSource,UITableViewDelegate,ServerManagerResponseDelegate>
{
    NSMutableArray * propertyArry;
}
@property(strong,nonatomic) ProductObject *productObject;
@property(strong,nonatomic) IBOutlet UITableView * tableView;
@end
