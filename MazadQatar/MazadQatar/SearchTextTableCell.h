//
//  SearchTextTableCell.h
//  Mzad Qatar
//
//  Created by Sandeep Gangajaliya on 18/03/17.
//  Copyright © 2017 Mohammed abdulfattah attia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchTextTableCell : UITableViewCell

@property (nonatomic, retain) IBOutlet UILabel *lblSearchFor;
@property (nonatomic, retain) IBOutlet UILabel *lblCategoryType;
@property (nonatomic, retain) IBOutlet UIImageView *imgArrow;

@end
