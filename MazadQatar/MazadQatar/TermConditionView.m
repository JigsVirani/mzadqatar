//
//  TermConditionView.m
//  Mzad Qatar
//
//  Created by GuruUgam on 5/19/17.
//  Copyright © 2017 Mohammed abdulfattah attia. All rights reserved.
//

#import "TermConditionView.h"
#import "SharedData.h"
#import "DisplayUtility.h"
@interface TermConditionView ()

@end

@implementation TermConditionView

- (void)viewDidLoad {
    [super viewDidLoad];
    //self.title = NSLocalizedString(@"Terms of use", nil);
    self.tabBarController.tabBar.hidden = YES;
    [self.tabBarController.tabBar setTranslucent:YES];
    if ([DisplayUtility isPad]) {
        //self->txvTermCondition.font = [UIFont systemFontOfSize:22.0];
        self->tittleLbl.font = [UIFont boldSystemFontOfSize:22.0];
    }
    self->tittleLbl.text = NSLocalizedString(@"Terms of use", nil);
    [self setTextViewTearmConditon];
    [txvTermCondition setEditable:false];
    // Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated
{
    
   
}
-(void)setTextViewTearmConditon{
    
    if([[SharedData getDeviceLanguage]isEqualToString:@"en"]){
        NSString *path = [[NSBundle mainBundle] pathForResource:@"terms" ofType:@"txt"];
        NSString *content = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:nil];
        NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithData:[content dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
        if ([DisplayUtility isPad]) {
            UIFont *font = [UIFont systemFontOfSize:22.0];
            [attributedString addAttribute:NSFontAttributeName value:font range:NSMakeRange(0, attributedString.string.length)];
            //content = [NSString stringWithFormat:@"<font size=\"%d\"> %@ </font>",6,content];
        }else
        {
            UIFont *font = [UIFont systemFontOfSize:18.0];
            [attributedString addAttribute:NSFontAttributeName value:font range:NSMakeRange(0, attributedString.string.length)];
        }
        txvTermCondition.attributedText = attributedString;
    }else
    {
        NSString *path = [[NSBundle mainBundle] pathForResource:@"terms-ar" ofType:@"txt"];
        NSString *content = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:nil];
        NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithData:[content dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
        
        if ([DisplayUtility isPad]) {
            UIFont *font = [UIFont systemFontOfSize:22.0];
            [attributedString addAttribute:NSFontAttributeName value:font range:NSMakeRange(0, attributedString.string.length)];
            //content = [NSString stringWithFormat:@"<font size=\"%d\"> %@ </font>",6,content];
        }else
        {
            UIFont *font = [UIFont systemFontOfSize:18.0];
            [attributedString addAttribute:NSFontAttributeName value:font range:NSMakeRange(0, attributedString.string.length)];
        }
        NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
        [paragraphStyle setAlignment:NSTextAlignmentRight];
        [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, attributedString.string.length)];
        txvTermCondition.attributedText = attributedString;
        
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
