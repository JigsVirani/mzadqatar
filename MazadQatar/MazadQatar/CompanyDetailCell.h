//
//  CompanyDetailCell.h
//  Mzad Qatar
//
//  Created by GuruUgam on 10/14/16.
//  Copyright © 2016 Mohammed abdulfattah attia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CompanyDetailCell : UITableViewCell

@property(strong,nonatomic)IBOutlet UILabel *lblNum;
@property(strong,nonatomic)IBOutlet UILabel *lblTtitle;
@property(strong,nonatomic)IBOutlet UILabel *lblLine;

@property(strong,nonatomic)IBOutlet UIButton *btnCall;
@property(strong,nonatomic)IBOutlet UIButton *btnEmail;
@property (weak, nonatomic) IBOutlet UIButton *registerAsCompany;
@end
