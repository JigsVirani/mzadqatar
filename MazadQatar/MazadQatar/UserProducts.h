//
//  UserProductsViewController.h
//  MazadQatar
//
//  Created by samo on 3/28/13.
//  Copyright (c) 2013 Mohammed abdulfattah attia. All rights reserved.
//

#import "GridView.h"
#import "ProductObject.h"
#import "ProductGridCell.h"
#import "ServerManager.h"
#import "UploadingListenerManager.h"
#import "DialogManager.h"
#import "SocialShareUtility.h"
#import "CompanyRegistration.h"

@interface UserProducts
    : GridView <ServerManagerResponseDelegate, ProductsUploadingDelegate,LocationDialogueDelegate> {
  UploadingListenerManager *uploadingListnerManager;
        bool   isShowUserProfileBar;
        SocialShareUtility *socialShareUtility;
}
@property bool loadCompany;
@property BOOL *checkCompany;

@property(strong, nonatomic) IBOutlet UILabel *textViewUserName;

@property(strong, nonatomic) ProductObject *userProfileProductObject;

@property(strong, nonatomic) IBOutlet UILabel *lblNoAdsAvailable;
@property (weak, nonatomic) IBOutlet UIButton *AdvertiseNowButton;
- (IBAction)callBtnClicked:(id)sender;

- (void)startLoadingUserProductsWithRequestId:(NSString *)requestId
                              andIsNewSession:(bool)isNewSession;
- (void)updateProductProgress:(ProductObject *)productObject;
- (void)showBackButton:(BOOL)isShown;
@property BOOL hideTabBar;
//-(void)startUploaddingProducts;

@end
