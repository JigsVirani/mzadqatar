//
//  FilterCarsChooser.h
//  Mzad Qatar
//
//  Created by samo on 8/20/14.
//  Copyright (c) 2014 Mohammed abdulfattah attia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CitiesViewController.h"
#import "SubCategoryChooser.h"
#import "SubSubCategoriesChooser.h"
#import "DialogManager.h"
#import "TextFieldViewController.h"

@class FilterCarsChooser;
@protocol FilterCarsChooserDelegate <NSObject>
@required
- (void)carFiltersChoosedWithProductObject:(ProductObject *)productObjectParam;
@required
@end

@interface FilterCarsChooser
    : TextFieldViewController <SubCategoryChooserDelegate,
                               SubSubCategoriesChooserDelegate> {
//  NSString *currentCategoryId;
//  NSString *currentSubCategoryId;
//  NSString *currentSubSubCategoryId;
//  NSString *currentCityIdSelected;
}

@property(strong) ProductObject *productObject;

@property(strong, nonatomic) IBOutlet UIButton *citiesChooserBtn;
@property(strong, nonatomic) IBOutlet UIButton *motorChooserBtn;
@property(strong, nonatomic) IBOutlet UIButton *modelChooserBtn;
@property(strong, nonatomic) IBOutlet UIImageView *citiesChooserIMG;
@property(strong, nonatomic) IBOutlet UIImageView *motorChooserIMG;
@property(strong, nonatomic) IBOutlet UIImageView *modelChooserIMG;

@property(strong, nonatomic) IBOutlet UITextField *priceFromTextField;
@property(strong, nonatomic) IBOutlet UITextField *priceToTextField;
@property(strong, nonatomic) IBOutlet UITextField *yearFromTextField;
@property(strong, nonatomic) IBOutlet UITextField *yearToTextField;
@property(strong, nonatomic) IBOutlet UITextField *kmFromTextField;
@property(strong, nonatomic) IBOutlet UITextField *kmToTextField;

@property(nonatomic, strong) id<FilterCarsChooserDelegate> delegate;

- (IBAction)searchBtnClicked:(id)sender;

@end
