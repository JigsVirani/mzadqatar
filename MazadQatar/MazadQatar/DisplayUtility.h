//
//  DisplayUtility.h
//  MazadQatar
//
//  Created by samo on 3/7/13.
//  Copyright (c) 2013 Mohammed abdulfattah attia. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DisplayUtility : NSObject

+ (BOOL)isPad;
+ (BOOL)hasRetinaDisplay;
+ (bool)isLandscape;
+ (CGSize)getScreenSize;
+ (CGSize)getScreenSize:(bool)isPortrait;
+ (CGFloat) changeLaoutAsPerHeight;
@end
