//
//  ServerResponseObject.h
//  MazadQatar
//
//  Created by samo on 3/17/13.
//  Copyright (c) 2013 Mohammed abdulfattah attia. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ProductObject.h"

@interface ServerManagerResult : NSObject

@property(strong) NSString *opertationResult;
@property(strong) NSString *opertationMsg;
@property(strong) NSString *lastUpdateTime;
@property(strong) NSString *lastUpdateTimeForCategories;
@property(strong) NSString *lastUpdateTimeForSortBy;
@property(strong) NSString *lastUpdateTimeForCities;
@property(strong) NSString *requestId;
@property(strong) NSMutableArray *jsonArray;
@property(strong) NSMutableDictionary *jsonDict;
@property(strong) NSMutableArray *categoryAdvertiseTypes;
@property(strong) NSString *notifcationCount;
@property(strong) ProductObject *userProfileObject;

@end
