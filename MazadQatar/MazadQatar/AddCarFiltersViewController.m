//
//  AddCarFiltersViewController.m
//  Mzad Qatar
//
//  Created by samo on 9/11/14.
//  Copyright (c) 2014 Mohammed abdulfattah attia. All rights reserved.
//

#import "AddCarFiltersViewController.h"
#import "AdvertiseTypeChooser.h"
#import "YearChooserViewController.h"

@interface AddCarFiltersViewController ()

@end

@implementation AddCarFiltersViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.screenName = @"Add Car Filters Screen";
    self.title = NSLocalizedString(@"Filters of category", nil);
    
//    if ([DisplayUtility isPad]) {
//        self.movementDistance = 200; // tweak as needed
//    } else {
//        self.movementDistance = 180; // tweak as needed
//    }
    [self createKeyboardAccessoryView];
    [self.firstTextView setInputAccessoryView:self.inputAccView];
    [self.secondTextView setInputAccessoryView:self.inputAccView];
    [self.thirdTextView setInputAccessoryView:self.inputAccView];
    [self.forthTextView setInputAccessoryView:self.inputAccView];
    [self.fifthTextView setInputAccessoryView:self.inputAccView];
    [self.sixTextView setInputAccessoryView:self.inputAccView];
    [self.eightTextView setInputAccessoryView:self.inputAccView];
    
    if([[SharedData getDeviceLanguage]isEqualToString:@"en"]){
        
        [self.advertiseTypeChooserBtn setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
        [self.citiesChooserBtn setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
        [self.motorChooserBtn setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
        [self.modelChooserBtn setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
        [self.manfactureYearBtn setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
        [self.btnPriceAddInfo setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
        
        [self.advertiseTypeChooserImg setImage:[UIImage imageNamed:@"ic_english_arrow"]];
        [self.citiesChooserImg setImage:[UIImage imageNamed:@"ic_english_arrow"]];
        [self.motorChooserImg setImage:[UIImage imageNamed:@"ic_english_arrow"]];
        [self.modelChooserImg setImage:[UIImage imageNamed:@"ic_english_arrow"]];
        [self.manfactureYearImg setImage:[UIImage imageNamed:@"ic_english_arrow"]];
        
        /*[self.advertiseTypeChooserBtn setBackgroundImage:[UIImage imageNamed:@"bk_choose_list_btn_right.png"] forState:UIControlStateNormal];
        [self.citiesChooserBtn setBackgroundImage:[UIImage imageNamed:@"bk_choose_list_btn_right.png"] forState:UIControlStateNormal];
        [self.motorChooserBtn setBackgroundImage:[UIImage imageNamed:@"bk_choose_list_btn_right.png"] forState:UIControlStateNormal];
        [self.modelChooserBtn setBackgroundImage:[UIImage imageNamed:@"bk_choose_list_btn_right.png"] forState:UIControlStateNormal];
        [self.manfactureYearBtn setBackgroundImage:[UIImage imageNamed:@"bk_choose_list_btn_right.png"] forState:UIControlStateNormal];*/
    }else{
        
        [self.advertiseTypeChooserBtn setContentHorizontalAlignment:UIControlContentHorizontalAlignmentRight];
        [self.citiesChooserBtn setContentHorizontalAlignment:UIControlContentHorizontalAlignmentRight];
        [self.motorChooserBtn setContentHorizontalAlignment:UIControlContentHorizontalAlignmentRight];
        [self.modelChooserBtn setContentHorizontalAlignment:UIControlContentHorizontalAlignmentRight];
        [self.manfactureYearBtn setContentHorizontalAlignment:UIControlContentHorizontalAlignmentRight];
        [self.btnPriceAddInfo setContentHorizontalAlignment:UIControlContentHorizontalAlignmentRight];
        
        [self.advertiseTypeChooserImg setImage:[UIImage imageNamed:@"ic_arabic_arrow"]];
        [self.citiesChooserImg setImage:[UIImage imageNamed:@"ic_arabic_arrow"]];
        [self.motorChooserImg setImage:[UIImage imageNamed:@"ic_arabic_arrow"]];
        [self.modelChooserImg setImage:[UIImage imageNamed:@"ic_arabic_arrow"]];
        [self.manfactureYearImg setImage:[UIImage imageNamed:@"ic_arabic_arrow"]];
        
        [self.advertiseTypeChooserBtn.titleLabel setFont:[UIFont fontWithName:@"FSAlbertArabicWeb-Regular" size:14.0]];
        [self.citiesChooserBtn.titleLabel setFont:[UIFont fontWithName:@"FSAlbertArabicWeb-Regular" size:14.0]];
        [self.motorChooserBtn.titleLabel setFont:[UIFont fontWithName:@"FSAlbertArabicWeb-Regular" size:14.0]];
        [self.modelChooserBtn.titleLabel setFont:[UIFont fontWithName:@"FSAlbertArabicWeb-Regular" size:14.0]];
        [self.manfactureYearBtn.titleLabel setFont:[UIFont fontWithName:@"FSAlbertArabicWeb-Regular" size:14.0]];
        [self.btnPriceAddInfo.titleLabel setFont:[UIFont fontWithName:@"FSAlbertArabicWeb-Regular" size:15.0]];
        [self.btnDone.titleLabel setFont:[UIFont fontWithName:@"FSAlbertArabicWeb-Regular" size:15.0]];
        
        [self.lblAdvertiseTypeChooser setFont:[UIFont fontWithName:@"FSAlbertArabicWeb-Regular" size:17.0]];
        [self.lblCitiesChooser setFont:[UIFont fontWithName:@"FSAlbertArabicWeb-Regular" size:17.0]];
        [self.lblMotorChooser setFont:[UIFont fontWithName:@"FSAlbertArabicWeb-Regular" size:17.0]];
        [self.lblModelChooser setFont:[UIFont fontWithName:@"FSAlbertArabicWeb-Regular" size:17.0]];
        [self.lblManfactureYear setFont:[UIFont fontWithName:@"FSAlbertArabicWeb-Regular" size:17.0]];
                
        [self.advertiseTypeChooserBtn setTitleEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 8)];
        [self.citiesChooserBtn setTitleEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 8)];
        [self.motorChooserBtn setTitleEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 8)];
        [self.modelChooserBtn setTitleEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 8)];
        [self.manfactureYearBtn setTitleEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 8)];
        [self.btnPriceAddInfo setTitleEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 8)];
        
        /*[self.advertiseTypeChooserBtn setBackgroundImage:[UIImage imageNamed:@"bk_choose_list_btn_left"] forState:UIControlStateNormal];
        [self.citiesChooserBtn setBackgroundImage:[UIImage imageNamed:@"bk_choose_list_btn_left"] forState:UIControlStateNormal];
        [self.motorChooserBtn setBackgroundImage:[UIImage imageNamed:@"bk_choose_list_btn_left"] forState:UIControlStateNormal];
        [self.modelChooserBtn setBackgroundImage:[UIImage imageNamed:@"bk_choose_list_btn_left"] forState:UIControlStateNormal];
        [self.manfactureYearBtn setBackgroundImage:[UIImage imageNamed:@"bk_choose_list_btn_left"] forState:UIControlStateNormal];*/
    }
    //[self.navigationController.navigationBar.topItem setTitle:NSLocalizedString(@"BACK", nil)];
}

-(void)viewDidAppear:(BOOL)animated
{
    [self resetUiValues];
}


-(void)resetUiValues
{
    if (self.productObject.productCityName != nil) {
        [self.citiesChooserBtn setTitle:self.productObject.productCityName
                               forState:UIControlStateNormal];
    }
    
    if (self.productObject.productSubCategoryName != nil) {
        [self.motorChooserBtn setTitle:self.productObject.productSubCategoryName
                              forState:UIControlStateNormal];
    }
    
    if (self.productObject.productSubsubCategoryName != nil) {
        [self.modelChooserBtn setTitle:self.productObject.productSubsubCategoryName
                              forState:UIControlStateNormal];
    }
    
     if (self.productObject.productManfactureYear != nil && self.productObject.productManfactureYear.length > 0 ) {
     [self.manfactureYearBtn setTitle:self.productObject.productManfactureYear forState:UIControlStateNormal];
     }
     
    
    
    if (self.productObject.productKm != nil && self.productObject.productKm.length > 0 ) {
      
        NSInteger len = self.productObject.productKm.length;
        
        NSUInteger virtualIndex = 7 - len;
        NSInteger i = len-1;
        
        for(  ; i >= 0 ; i--){
            
            NSString *s = [NSString stringWithFormat:@"%c", [self.productObject.productKm characterAtIndex:i]];

            NSInteger value;
            if (![[[NSLocale preferredLanguages] objectAtIndex:0] containsString:@"en"]){
                value = (8-(i + virtualIndex));
            }else
            {
                value = (i + virtualIndex);
            }
            switch (value) {
                case 0:
                    [_firstTextView setText:s];
                    break;
                case 1:
                    [_secondTextView setText:s];
                    break;
                case 2:
                    [_thirdTextView setText:s];
                    break;
                case 3:
                    [_forthTextView setText:s];
                    break;
                case 4:
                    [_fifthTextView setText:s];
                    break;
                case 5:
                    [_sixTextView setText:s];
                    break;
                case 6:
                    [_eightTextView setText:s];
                    break;
                    
                default:
                    break;
            }
        }
    }
    
    
    if (self.productObject.productAdvertiseTypeName != nil) {
        [self.advertiseTypeChooserBtn
         setTitle:self.productObject.productAdvertiseTypeName
         forState:UIControlStateNormal];
    }
}
-(void)textViewDidEndEditing:(UITextView *)textView
{    
//    if(textView == self.manfactureYearTextField)
//    {
//        self.productObject.productManfactureYear=textView.text;
//    }
//    if(textView == self.kmTextField)
//    {
//        self.productObject.productKm=textView.text;
//    }
    if (textView == self.firstTextView ||
        textView == self.secondTextView ||
        textView == self.thirdTextView||
        textView == self.forthTextView||
        textView == self.fifthTextView ||
        textView == self.sixTextView ||
        textView == self.eightTextView
        ) {
        if ([textView.text length]==0) {
            textView.text=@"0";
        }
    }
}

- (void)setProductObjectUi:(ProductObject *)productObject {
    self.productObject = productObject;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier
                                  sender:(id)sender {
    if ([identifier isEqualToString:@"SubSubCategorySegue"] ||
        [identifier isEqualToString:@"SubSubCategorySegueArrow"]) {
        if ([self.productObject.productSubCategoryId isEqualToString:@""]) {
            [DialogManager showErrorSubCategoryNotChosen];
            
            return false;
        }
    }
    
    return true;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    [super prepareForSegue:segue sender:sender];
    
    if ([[segue identifier] isEqualToString:@"SubCategorySegue"]) {
        SubCategoryChooser *subCategoryChooser = [segue destinationViewController];
        
        [subCategoryChooser
         setSubCategoryParamsWithCatId:self.productObject.productCategoryId
         andCurrentSubCategoryId:self.productObject.productSubCategoryId
         andIsOpenedFromAddAdvertise:true
         andDelegate:self];
        
    } else if ([[segue identifier] isEqualToString:@"SubSubCategorySegue"]) {
        SubSubCategoriesChooser *subSubCategoriesChooser =
        [segue destinationViewController];
        
        [subSubCategoriesChooser
         setSubSubCategoryParamsWithCatId:self.productObject.productCategoryId
         andCurrentSubCategoryId:self.productObject.productSubCategoryId
         andCurrentSubSubCategoryId:self.productObject
         .productSubsubCategoryId
         andIsOpenedFormAdvertise:YES
         andDelegate:self];
    } else if ([[segue identifier] isEqualToString:@"CitiesSegue"]) {
        CitiesViewController *citiesViewController =
        [segue destinationViewController];
        
        [citiesViewController
         setCityParamsWithCityId:self.productObject.productCityId
         andIsOpenedFromAddAdvertise:true
         andDelegate:self];
    } else if ([[segue identifier]
                isEqualToString:@"AdvertiseTypeChooserSegue"]) {
        AdvertiseTypeChooser *advertiseTypeChooser =
        [segue destinationViewController];
        
        [advertiseTypeChooser
         setAdvertiseTypeParamsWithCatId:self.productObject.productCategoryId
         andSelectedAdvertiseTypeId:self.productObject
         .productAdvertiseTypeId
         andDelegate:self];
    }else if([[segue identifier] isEqualToString:@"pushYearChooser"]){
    
        YearChooserViewController *yearViewController = [segue destinationViewController];
        
        [yearViewController setYearDelegate:self];
        
        
    
    
    }
}

- (void)subCategoryChoosedInList:(NSString *)subCategoryId
              andSubCategoryName:(NSString *)subCategorName {
    [self.motorChooserBtn setTitle:subCategorName forState:UIControlStateNormal];
    self.productObject.productSubCategoryId = subCategoryId;
    self.productObject.productSubCategoryName = subCategorName;
    
    // reset
    [self.modelChooserBtn
     setTitle:NSLocalizedString(@"FILTER_CHOSE_MODEL_TEXT", nil)
     forState:UIControlStateNormal];
    self.productObject.productSubsubCategoryId = nil;
    self.productObject.productSubsubCategoryName =
    NSLocalizedString(@"FILTER_CHOSE_MODEL_TEXT", nil);
}

- (void)subsubCategoryChoosedInList:(NSString *)subsubCategoryId
              andSubsubCategoryName:(NSString *)subsubCategoryName {
    [self.modelChooserBtn setTitle:subsubCategoryName
                          forState:UIControlStateNormal];
    self.productObject.productSubsubCategoryId = subsubCategoryId;
    self.productObject.productSubsubCategoryName = subsubCategoryName;
}

- (void)citiesChoosedInList:(NSString *)cityId
                andCityName:(NSString *)cityName {
    [self.citiesChooserBtn setTitle:cityName forState:UIControlStateNormal];
    self.productObject.productCityId = cityId;
    self.productObject.productCityName = cityName;
}

- (void)AdvertiseTypeChoosed:(NSString *)advertiseTypeChooserId
 andAdvertiseTypeChooserName:(NSString *)advertiseTypeChooserName {
    [self.advertiseTypeChooserBtn setTitle:advertiseTypeChooserName
                                  forState:UIControlStateNormal];
    self.productObject.productAdvertiseTypeId = advertiseTypeChooserId;
    self.productObject.productAdvertiseTypeName = advertiseTypeChooserName;
}

- (void)yearChoosed:(NSString *)yearChooser{
    
    [self.manfactureYearBtn setTitle:yearChooser
                                  forState:UIControlStateNormal];
    self.productObject.productManfactureYear = yearChooser;


}


- (void)createKeyboardAccessoryViewInChild {
}

- (void)doneTyping {
    
    [self.firstTextView resignFirstResponder];
    [self.secondTextView resignFirstResponder];
    [self.thirdTextView resignFirstResponder];
    [self.forthTextView resignFirstResponder];
    [self.fifthTextView resignFirstResponder];
    [self.sixTextView resignFirstResponder];
    [self.eightTextView resignFirstResponder];
}


- (bool)validateCarFilters {
    if (self.productObject.productCityId.length == 0) {
        [DialogManager showErrorCitiesNotChosen];
        
        return false;
    }
    
    if (self.productObject.productAdvertiseTypeId.length == 0) {
        [DialogManager showErrorAdvertiseTypeNotChoosen];
        
        return false;
    }
    
    if (self.productObject.productSubCategoryId.length == 0) {
        [DialogManager showErrorSubCategoryNotChosen];
        
        return false;
    }
    
    if (self.productObject.productSubsubCategoryId.length == 0) {
        [DialogManager showErrorSubSubCagegoryNotChosen];
        
        return false;
    }
   
    if (self.manfactureYearBtn.currentTitle.length != 4) {
        [DialogManager showErrorManfactureYearNotChosen];
        
        return false;
    }
    
    
    
    
    /*if ([self getKMValueFromTextViews].length == 0) {
        [DialogManager showErrorKmNotChosen];
        
        return false;
    }*/
    
    return true;
}
- (BOOL)checkHasIntegerValue:(NSString *) text {
    
    int value = [text intValue];
    
    if(value > 0)
        return true;
    
    return false;
    
}

- (NSString* )getKMValueFromTextViews {
    
    NSString *priceValue = @"";
    
    BOOL hasPreviousNumber = false;
    if ( _firstTextView.text.length > 0 && [self checkHasIntegerValue:_firstTextView.text]){
        hasPreviousNumber = true;
        priceValue =  [priceValue stringByAppendingString:_firstTextView.text];
    }
    if ( _secondTextView.text.length > 0 && [self checkHasIntegerValue:_secondTextView.text]){
        hasPreviousNumber = true;
        priceValue = [priceValue stringByAppendingString:_secondTextView.text];
    }else{
        if(hasPreviousNumber){
            priceValue = [priceValue stringByAppendingString:@"0"];
        }
    }
    if ( _thirdTextView.text.length > 0 && [self checkHasIntegerValue:_thirdTextView.text]){
        hasPreviousNumber = true;
        priceValue =[priceValue stringByAppendingString:_thirdTextView.text];
    }else{
        if(hasPreviousNumber){
            priceValue = [priceValue stringByAppendingString:@"0"];
        }
    }
    if ( _forthTextView.text.length > 0 && [self checkHasIntegerValue:_forthTextView.text]){
        hasPreviousNumber = true;
        priceValue = [priceValue stringByAppendingString:_forthTextView.text];
    }else{
        if(hasPreviousNumber){
            priceValue = [priceValue stringByAppendingString:@"0"];
        }
    }
    if ( _fifthTextView.text.length > 0 && [self checkHasIntegerValue:_fifthTextView.text]){
        hasPreviousNumber = true;
        priceValue = [priceValue stringByAppendingString:_fifthTextView.text];
    }else{
        if(hasPreviousNumber){
            priceValue = [priceValue stringByAppendingString:@"0"];
        }
    }
    if ( _sixTextView.text.length > 0 && [self checkHasIntegerValue:_sixTextView.text]){
        hasPreviousNumber = true;
        priceValue = [priceValue stringByAppendingString:_sixTextView.text];
    }else{
        if(hasPreviousNumber){
            priceValue = [priceValue stringByAppendingString:@"0"];
        }
    }
    if ( _eightTextView.text.length > 0 && [self checkHasIntegerValue:_eightTextView.text]){
        hasPreviousNumber = true;
        priceValue = [priceValue stringByAppendingString:_eightTextView.text];
    }else{
        if(hasPreviousNumber){
            priceValue = [priceValue stringByAppendingString:@"0"];
        }
    }

    return priceValue;
}

- (BOOL)textView:(UITextView *)textView
shouldChangeTextInRange:(NSRange)range
 replacementText:(NSString *)text {
    // you only need this check if you have more than one textfield on the view:
    if (textView == self.firstTextView ||
        textView == self.secondTextView ||
        textView == self.thirdTextView||
        textView == self.forthTextView||
        textView == self.fifthTextView ||
        textView == self.sixTextView ||
        textView == self.eightTextView
        ) {
        /*NSUInteger newLength =
        [textView.text length] + [text length] - range.length;
        return (newLength > 1) ? NO : YES;*/
        
//        if([text length] == 1){
//            textView.text = @"";
//            return YES;
//        }else{
//            return NO;
//        }
        if([text length] == 0 || [text isEqualToString:@"0"]){
            textView.text = @"";
            return YES;
        }else{
            return YES;
        }
    }
    else{
    return YES;
    }
}
//=================================================keyboard accessor view
-(BOOL)textViewShouldBeginEditing:(UITextView *)textView {
    if (textView == self.firstTextView ||
        textView == self.secondTextView ||
        textView == self.thirdTextView||
        textView == self.forthTextView||
        textView == self.fifthTextView ||
        textView == self.sixTextView ||
        textView == self.eightTextView
        ) {
        if ([textView.text isEqualToString:@"0"]) {
            textView.text=@"";
        }
    }
    return YES;
}

- (IBAction)doneBtnClicked:(id)sender {
    if ([self validateCarFilters] == false) {
        return;
    }
    
    self.productObject.productKm = [self getKMValueFromTextViews];
    
    self.productObject.isAllFiltersSelected=true;
    
    [self.delegate carFiltersChoosedWithProductObject:self.productObject];
    
    [self.navigationController popViewControllerAnimated:YES];
}


@end
