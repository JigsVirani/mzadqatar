//
//  NewSendCoomentTableViewCell.m
//  Mzad Qatar
//
//  Created by Paresh Kacha on 03/11/15.
//  Copyright © 2015 Mohammed abdulfattah attia. All rights reserved.
//

#import "NewSendCommentTableCell.h"
#import "GAI.h"
#import "GAIDictionaryBuilder.h"

//@implementation NSObject
//
////+ (void)load {
////    NSLog(@"in Superclass load");
////}
//
//@end

@implementation NewSendCommentTableCell


NSString *NewrequestAddProductComments = @"requestAddProductComments";



- (id)initWithStyle:(UITableViewCellStyle)style
    reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}
-(UIView *)roundCornersOnView:(UIView *)view onTopLeft:(BOOL)tl topRight:(BOOL)tr bottomLeft:(BOOL)bl bottomRight:(BOOL)br radius:(float)radius {
    
    if (tl || tr || bl || br) {
        UIRectCorner corner = 0; //holds the corner
        //Determine which corner(s) should be changed
        if (tl) {
            corner = corner | UIRectCornerTopLeft;
        }
        if (tr) {
            corner = corner | UIRectCornerTopRight;
        }
        if (bl) {
            corner = corner | UIRectCornerBottomLeft;
        }
        if (br) {
            corner = corner | UIRectCornerBottomRight;
        }
        
        UIView *roundedView = view;
        UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:roundedView.bounds byRoundingCorners:corner cornerRadii:CGSizeMake(radius, radius)];
        CAShapeLayer *maskLayer = [CAShapeLayer layer];
        maskLayer.frame = roundedView.bounds;
        maskLayer.path = maskPath.CGPath;
        roundedView.layer.mask = maskLayer;
        return roundedView;
    } else
    {
        return view;
    }

}
- (void)setProductOfComment:(ProductObject *)productObjectParam
               withDelegate:(id<NewSendCommentDelegate>)delegateParam
           inViewController:(UIViewController *)view;
{

    delegate = delegateParam;
    productObject = productObjectParam;
    parentView = view;
//    [self roundCornersOnView:self.sendBtn onTopLeft:YES topRight:YES bottomLeft:YES bottomRight:YES radius:5.0];
//    [self roundCornersOnView:self.userComment onTopLeft:YES topRight:YES bottomLeft:YES bottomRight:YES radius:5.0];

    self.sendBtn.layer.cornerRadius = 5.0;
    self.userComment.layer.cornerRadius = 5.0;
    
    [self createKeyboardAccessoryView];
    
//    if ([[UserSetting getUserName] isEqualToString:@" "] ||
//        [[UserSetting getUserName] length] == 0) {
//        self.userCommentName.text = NSLocalizedString(@"WRITE_COMMENT_NAME", nil);
        // self.userCommentName.textColor = [UIColor lightGrayColor];
//    } else {
//        self.userCommentName.text = [UserSetting getUserName];
//    }
    
    self.userComment.text = [NSString stringWithFormat:@"%@",NSLocalizedString(@"WRITE_COMMENT", nil)];
    self.userComment.textColor = [UIColor lightGrayColor];
//
    
    //self.userComment.delegate = self;
    self.sendBtn.enabled = YES;
    [self.sendBtn setAlpha:1.0];

    
    if (productObject.isHideAddCommentsAndLikes == true) {
        [self.sendBtn setEnabled:false];
        [self.sendBtnResultText setHidden:false];
        [self.sendBtnResultText
         setText:NSLocalizedString(@"SEND_COMMENT_NOT_ENABLED_IN_ADVERTISE",
                                   nil)];
    }
}
- (IBAction)sendBtnClicked:(id)sender {
//    self.sendBtn.enabled = NO;
//    [self.sendBtn setAlpha:0.7];
    [self endEditing:YES];
    if ([self.userComment.text
         isEqualToString:NSLocalizedString(@"WRITE_COMMENT", nil)]) {
        self.userComment.text = @"";
    }
    NSString *trimString = self.userComment.text;
    trimString = [trimString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    if (trimString.length == 0)
    {
        [DialogManager showErrorUserCommentEmpty];
        self.userComment.text = @"";
    }
    else
    {

    if ([UserSetting isUserRegistered] == false)
    {
        [SharedViewManager showLoginScreen:parentView];
        return;
    }
    else
    {
        NSString *name =[UserSetting getUserName];
        if([UserSetting getUserName].length > 0)
        {
            self.sendBtnResultText.hidden = YES;
            [self.sendCommentIndicator startAnimating];
            [ServerManager addCommentToProduct:productObject
                                  fromUserName:[UserSetting getUserName]
                            andUserCountryCode:[UserSetting getUserCountryCode]
                                 andUserNumber:[UserSetting getUserNumber]
                                   withComment:self.userComment.text
                                   AndDelegate:self
                                 withRequestId:NewrequestAddProductComments];
            
            
            //=================================================
            // Log setting open event with category="ui", action="open", and label="settings".
            id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
            [tracker send:
             [[GAIDictionaryBuilder createEventWithCategory:@"ui"
                                                     action:@"Comment Sent"
                                                      label:@"Add Comment"
                                                      value:nil] build]];
            //=================================================
        }
        else
        {
            EnterNameViewController *controller = [[SharedData getCurrentStoryBoard]
                                                   instantiateViewControllerWithIdentifier:@"EnterNameViewController"];
            controller.delegate = self;
            [parentView.view addSubview:controller.view];
            [parentView addChildViewController:controller];
       
        }
    
    }
    }

}

- (void)textEntered:(NSString *)text
{
    if ([self validateInput:text] == false) {
        return;
    }
    [UserSetting saveUserName:text];
    
    self.sendBtnResultText.hidden = YES;
    [self.sendCommentIndicator startAnimating];
    [ServerManager addCommentToProduct:productObject
                          fromUserName:text
                    andUserCountryCode:[UserSetting getUserCountryCode]
                         andUserNumber:[UserSetting getUserNumber]
                           withComment:self.userComment.text
                           AndDelegate:self
                         withRequestId:NewrequestAddProductComments];
    
    //=================================================
    // Log setting open event with category="ui", action="open", and label="settings".
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker send:
     [[GAIDictionaryBuilder createEventWithCategory:@"ui"
                                             action:@"Comment Sent"
                                              label:@"Add Comment"
                                              value:nil] build]];
    //=================================================
    
   @try {
        [ServerManager updateUserProfile:productObject
                            andRequestId:@"requestEditUserProfile"
                             andDelegate:self];
    } @catch (NSException *exception) {
       
    } 
}

- (bool)validateInput:(NSString *)name {
    if ([self validateCommentInput] == false) {
        [DialogManager showErrorUserCommentEmpty];
        return false;
    }
    
    if ([self validateCommentNameInputwithString:name] == false) {
        [DialogManager showErrorUserCommentNameEmpty];
        return false;
    }
    
    return true;
}

- (bool)validateCommentInput {
    NSCharacterSet *set = [NSCharacterSet whitespaceCharacterSet];
    if ([[self.userComment.text stringByTrimmingCharactersInSet:set] length] ==
        0 ||
        [self.userComment.text
         isEqualToString:NSLocalizedString(@"WRITE_COMMENT", nil)]) {
            return false;
        }
    
    return true;
}

- (bool)validateCommentNameInputwithString:(NSString *)text {
    NSCharacterSet *set = [NSCharacterSet whitespaceCharacterSet];
    
    if ([[text
          stringByTrimmingCharactersInSet:set] length] == 0 ||
        [text
         isEqualToString:NSLocalizedString(@"WRITE_COMMENT_NAME", nil)]) {
            return false;
        }
    
    return true;
}

- (void)requestFinished:(ServerManagerResult *)serverManagerResult
            ofRequestId:(NSString *)requestId
    withNumberOfRequest:(int)numberOfRequest {

    
    if (requestId == NewrequestAddProductComments) {
        //[ self.sendCommentIndicator stopAnimating];
        [self.sendCommentIndicator stopAnimating];
        
        if ([serverManagerResult.opertationResult
             isEqualToString:[ServerManager ERROR_IN_CONNECTION_OPERATION]]) {
            [DialogManager showErrorInConnection];
            return;
        }
        else if ([serverManagerResult.opertationResult
                  isEqualToString:[ServerManager ERROR_USER_BLOCKED_BY_ANOTHER_USER]]) {
            
            [DialogManager showErrorWithMessage:NSLocalizedString(@"USER_BLOCKED_YOU", nil)];
            
            return;
        }
        else if ([serverManagerResult.opertationResult
                  isEqualToString:[ServerManager ERROR_USER_BLOCKED_FROM_ADMIN]]) {
            
            [DialogManager showErrorWithMessage:NSLocalizedString(@"ERROR_USER_IS_BLOCKED_FROM_ADMIN", nil)];
            
            return;
        }
        // set status text
        else if ([serverManagerResult.opertationResult
             isEqualToString:[ServerManager SUCCESS_OPERATION]]) {
            self.sendBtnResultText.text = NSLocalizedString(@"SUCCESS_ADD", nil);
            //[DialogManager showSuccessMessage:NSLocalizedString(@"SUCCESS_ADD", nil)];
        } else if ([serverManagerResult.opertationResult
                    isEqualToString:[ServerManager FAILED_OPERATION]]) {
            //[DialogManager showErrorWithMessage:NSLocalizedString(@"ERROR_ADD", nil)];
            self.sendBtnResultText.text = NSLocalizedString(@"ERROR_ADD", nil);
        } else if ([serverManagerResult.opertationResult
                    isEqualToString:[ServerManager ALREADED_ADDED_OPERATION]]) {
            //[DialogManager showErrorWithMessage:NSLocalizedString(@"ALREADY_ADD", nil)];
            self.sendBtnResultText.text = NSLocalizedString(@"ALREADY_ADD", nil);
        }
        
        self.sendBtnResultText.hidden = NO;
        
        [delegate commentSent];
    }
}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView {


    [self createKeyboardAccessoryView];
    
    if (textView == self.userComment) {
        self.userComment.textColor = [UIColor blackColor];
        if ([self.userComment.text
             isEqualToString:NSLocalizedString(@"WRITE_COMMENT", nil)]) {
            self.userComment.text = @"";
        }
    }
    return YES;
}

- (BOOL)textView:(UITextView *)textView
shouldChangeTextInRange:(NSRange)range
 replacementText:(NSString *)text {
    if (textView == self.userComment) {
        NSUInteger newLength =
        [textView.text length] + [text length] - range.length;
        return (newLength > 500) ? NO : YES;
    }
    
    return YES;
}

- (BOOL)textViewShouldEndEditing:(UITextView *)textView {
    if (textView == self.userComment) {
        if ([StringUtility isTextEmpty:self.userComment.text] == 0) {
            //  self.userCommentName.textColor = [UIColor lightGrayColor];
            self.userComment.text = NSLocalizedString(@"WRITE_COMMENT", nil);
            self.userComment.textColor = [UIColor lightGrayColor];
            [self.userComment resignFirstResponder];
        }
        else
        {
            self.userComment.textColor = [UIColor blackColor];
        }
    }
    
    return YES;
}

- (void)createKeyboardAccessoryView {
    if ([DisplayUtility isPad] == false) {
        NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
        [nc addObserver:self
               selector:@selector(orientationChanged:)
                   name:UIApplicationDidChangeStatusBarOrientationNotification
                 object:nil];
        
        // Call the createInputAccessoryView method we created earlier.
        // By doing that we will prepare the inputAccView.
        [self createInputAccessoryView];
        
        // Now add the view as an input accessory view to the selected textfield.
//        [_userCommentName setInputAccessoryView:inputAccView];
        [_userComment setInputAccessoryView:inputAccView];
    }
}


- (void)orientationChanged:(NSNotification *)note {
    isRotating = YES;
    
    [self recreateInputAccessoryView];
    // issue in keyboard when moving another screen
    [parentView.view endEditing:YES];
    //[self createKeyboardAccessoryView];
    //[self.view reloadInputViews];
    isRotating = NO;
}

- (void)recreateInputAccessoryView {
    
    CGSize screenSize = [DisplayUtility getScreenSize];
    
    [inputAccView setFrame:CGRectMake(10.0, 0.0, screenSize.width, 40.0)];
    
    [btnDone setFrame:CGRectMake(screenSize.width - 90, 3, 80.0f, 40.0f)];
}


- (void)doneTyping {
    // When the "done" button is tapped, the keyboard should go away.
    // That simply means that we just have to resign our first responder.
//    [_userCommentName resignFirstResponder];
    [_userComment resignFirstResponder];
}

- (void)dealloc {
    
    NSNotificationCenter *nc =
    [NSNotificationCenter defaultCenter]; // Get the notification centre for
    // the app
    [nc removeObserver:self
                  name:UIApplicationDidChangeStatusBarOrientationNotification
                object:nil];
    
}


@end




