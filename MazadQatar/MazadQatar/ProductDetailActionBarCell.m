//
//  ProductDetailActionBarCell.m
//  MazadQatar
//
//  Created by samo on 4/23/13.
//  Copyright (c) 2013 Mohammed abdulfattah attia. All rights reserved.
//

#import "ProductDetailActionBarCell.h"
#import "MBProgressHUD.h"

@implementation ProductDetailActionBarCell

NSString *requestAddLike = @"requestAddLike";
NSString *requestAddFavorite = @"requestAddFavorite";
NSString *requestReportAdvertise = @"requestReportAdvertise";

- (id)initWithStyle:(UITableViewCellStyle)style
    reuseIdentifier:(NSString *)reuseIdentifier {
  self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
  if (self) {
    // Initialization code
  }
  return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
  [super setSelected:selected animated:animated];

  // Configure the view for the selected state
}

- (void)setProductObject:(ProductObject *)productObjectParam
            InParentView:(UIViewController *)parentViewParam
             inTableView:(UITableView *)tableViewParam {
  productObject = productObjectParam;
  parentView = parentViewParam;
  tableView = tableViewParam;
  if (productObject.isHideAddCommentsAndLikes == true) {
    [self.likeBtn setEnabled:false];
    [self.resultTextField setHidden:false];
    [self.resultTextField
        setText:NSLocalizedString(@"LIKE_NOT_ENABLED_IN_ADVERTISE", nil)];
  }

  if (productObject.isReportingAdvertise) {
    self.resultTextField.hidden = YES;
    [self.reportIndicator startAnimating];
    [ServerManager reportAdvertise:productObject
                       AndDelegate:self
                     withRequestId:requestReportAdvertise];
  }
}

- (IBAction)addToFavoriteBtnClicked:(id)sender {
  if ([UserSetting isUserRegistered] == false) {
    [SharedViewManager showLoginScreen:parentView];

    self.resultTextField.hidden = NO;

    self.resultTextField.text =
        NSLocalizedString(@"ERROR_TRY_AGAIN_AFTER_REGISTER", nil);

  } else {
    self.resultTextField.hidden = YES;

    [self.addToFavoriteIndicator startAnimating];

    NSString *isAd;
    if (productObject.isAd == true) {
      isAd = @"0";
    } else {
      isAd = @"1";
    }

    [ServerManager addProductToFavorite:productObject
                            AndDelegate:self
                          withRequestId:requestAddFavorite
                              withIsAdd:isAd];
  }
}

- (IBAction)likeBtnClicked:(id)sender {
  if ([UserSetting isUserRegistered] == false) {
    [SharedViewManager showLoginScreen:parentView];

    self.resultTextField.hidden = NO;

    self.resultTextField.text =
        NSLocalizedString(@"ERROR_TRY_AGAIN_AFTER_REGISTER", nil);
  } else {
    self.resultTextField.hidden = YES;

    [self.likeIndicator startAnimating];

    [ServerManager addLikeToProduct:productObject
                        AndDelegate:self
                      withRequestId:requestAddLike];
  }
}

- (IBAction)reportBtnClicked:(id)sender {
  
    [self showReportAdvertiseDialogue];
  
}

#pragma mark - share btns

- (IBAction)shareOnFacebookClicked:(id)sender {
  if (productObject.productShareImageUrl == nil) {
    [DialogManager showErrorProductNotFound];
    return;
  }

    
  NSString *text = nil;

  text = [NSString stringWithFormat:@"%@\n\n%@\n\n%@\n\n%@",
                                    productObject.productTilte,
                                    productObject.productDescription,
                                    NSLocalizedString(@"FOR_MORE_DETAILS", nil),
                                    productObject.productWebsiteUrl];

  UIImage *image = [self
      writeOnImageTitle:productObject.productTilte
         andDescription:productObject.productDescription
              andNumber:[NSString
                            stringWithFormat:@"%@%@",
                                             productObject.productCountryCode,
                                             productObject.productUserNumber]
                onImage:[self downloadShareImage]];

   socialShareUtility = [[SocialShareUtility alloc] init];
  [socialShareUtility shareOnFacebookInView:parentView
                                   withText:text
                                  withImage:image
                                    withUrl:productObject.productWebsiteUrl];
}

- (IBAction)shareOnTwitterClicked:(id)sender {
  if (productObject.productShareImageUrl == nil) {
    [DialogManager showErrorProductNotFound];
    return;
  }

  NSString *text = nil;

  if (productObject.productDescription.length > 30) {
    text = [NSString
        stringWithFormat:@"%@\n\n%@\n\n%@\n\n%@", productObject.productTilte,
                         [productObject.productDescription substringToIndex:30],
                         NSLocalizedString(@"FOR_MORE_DETAILS", nil),
                         productObject.productWebsiteUrl];
  } else {
    text = [NSString
        stringWithFormat:@"%@\n\n%@\n\n%@\n\n%@", productObject.productTilte,
                         productObject.productDescription,
                         NSLocalizedString(@"FOR_MORE_DETAILS", nil),
                         productObject.productWebsiteUrl];
  }

  //    text = [NSString
  //    stringWithFormat:@"%@\n\n%@\n\n%@\n\n%@",productObject.productTilte,productObject.productDescription,NSLocalizedString(@"FOR_MORE_DETAILS",
  //    nil),productObject.productWebsiteUrl];

  UIImage *image = [self
      writeOnImageTitle:productObject.productTilte
         andDescription:productObject.productDescription
              andNumber:[NSString
                            stringWithFormat:@"%@%@",
                                             productObject.productCountryCode,
                                             productObject.productUserNumber]
                onImage:[self downloadShareImage]];

   socialShareUtility = [[SocialShareUtility alloc] init];
  [socialShareUtility shareOnTwitterInView:parentView
                                  withText:text
                                 withImage:image
                                   withUrl:productObject.productWebsiteUrl];
}

- (IBAction)shareOnWhatsAppClicked:(id)sender {
  NSString *text = nil;

  text = [NSString stringWithFormat:@"%@\n\n%@\n\n%@\n\n%@",
                                    productObject.productTilte,
                                    productObject.productDescription,
                                    NSLocalizedString(@"FOR_MORE_DETAILS", nil),
                                    productObject.productWebsiteUrl];

   socialShareUtility = [[SocialShareUtility alloc] init];
  [socialShareUtility shareOnWhatsAppInView:parentView withText:text];
}

- (IBAction)shareOnInstagramClicked:(id)sender {
  if (productObject.productShareImageUrl==nil) {
    [DialogManager showErrorProductNotFound];
    return;
  }

  UIImage *image = [self
      writeOnImageTitle:productObject.productTilte
         andDescription:productObject.productDescription
              andNumber:[NSString
                            stringWithFormat:@"%@%@",
                                             productObject.productCountryCode,
                                             productObject.productUserNumber]
                onImage:[self downloadShareImage]];

   socialShareUtility= [[SocialShareUtility alloc] init];
  [socialShareUtility shareOnInstagramInView:parentView withImage:image];
}

- (IBAction)shareOnOthersAppsClicked:(UIButton*)sender {
    if (productObject.productShareImageUrl==nil) {
        [DialogManager showErrorProductNotFound];
        return;
    }

  NSString *text = nil;

  text = [NSString stringWithFormat:@"%@\n\n%@\n\n%@\n\n%@",
                                    productObject.productTilte,
                                    productObject.productDescription,
                                    NSLocalizedString(@"FOR_MORE_DETAILS", nil),
                                    productObject.productWebsiteUrl];

  UIImage *image = [self
      writeOnImageTitle:productObject.productTilte
         andDescription:productObject.productDescription
              andNumber:[NSString
                            stringWithFormat:@"%@%@",
                                             productObject.productCountryCode,
                                             productObject.productUserNumber]
                onImage:[self downloadShareImage]];

   CGRect buttonFrameInDetailView= [parentView.view convertRect:sender.frame fromView:self];
    
   socialShareUtility = [[SocialShareUtility alloc] init];
  [socialShareUtility shareOnOtherAppsInView:parentView
                          andFrameToSHowFrom:buttonFrameInDetailView
                                    withText:text
                                   withImage:image
                                     withUrl:productObject.productWebsiteUrl];
}

- (UIImage *)writeOnImageTitle:(NSString *)title
                andDescription:(NSString *)description
                     andNumber:(NSString *)number
                       onImage:(UIImage *)image {
  CGSize size = CGSizeMake(image.size.width, image.size.height);
  UIGraphicsBeginImageContextWithOptions(size, YES, 0.0);

  // draw base image
  [image drawInRect:CGRectMake(0, 0, size.width, size.height)];

  // white background
  UIImage *whiteBackground = [UIImage imageNamed:@"share_background_White"];
  float whiteBackgroundYPostion = size.height - whiteBackground.size.height;
  [whiteBackground drawAtPoint:CGPointMake(0, whiteBackgroundYPostion)];

  // gray backgroud
  UIImage *grayBackground = [UIImage imageNamed:@"share_background_gray"];
  float grayBackgroundYPosition =
      size.height - whiteBackground.size.height - grayBackground.size.height;
  [grayBackground drawAtPoint:CGPointMake(0, grayBackgroundYPosition)];

  // mzad logo
  UIImage *mzadLogo = [UIImage imageNamed:@"share_mzad_logo"];
  float mzadLogoXposition = 10;
  float mzadLogoYposition = whiteBackgroundYPostion + 8;
  [mzadLogo drawAtPoint:CGPointMake(mzadLogoXposition, mzadLogoYposition)];

  // product title
  UIFont *titleFont = [UIFont systemFontOfSize:26.0];
  NSDictionary *titleAttrsDictionary = [NSDictionary
      dictionaryWithObjectsAndKeys:titleFont, NSFontAttributeName,
                                   [NSNumber numberWithFloat:1.0],
                                   NSBaselineOffsetAttributeName,
                                   [UIColor colorWithRed:(152 / 255.0)
                                                   green:(41 / 255.0)
                                                    blue:(92 / 255.0)
                                                   alpha:1],
                                   NSForegroundColorAttributeName, nil];
  [title drawAtPoint:CGPointMake(mzadLogoXposition + mzadLogo.size.width + 10,
                                 mzadLogoYposition + 5)
      withAttributes:titleAttrsDictionary];

  // product title
  UIFont *numberfont = [UIFont systemFontOfSize:24.0];
  NSDictionary *numberAttrsDictionary = [NSDictionary
      dictionaryWithObjectsAndKeys:numberfont, NSFontAttributeName,
                                   [NSNumber numberWithFloat:1.0],
                                   NSBaselineOffsetAttributeName,
                                   [UIColor colorWithRed:(83 / 255.0)
                                                   green:(83 / 255.0)
                                                    blue:(83 / 255.0)
                                                   alpha:1],
                                   NSForegroundColorAttributeName, nil];
  [number drawAtPoint:CGPointMake(mzadLogoXposition + mzadLogo.size.width + 10,
                                  mzadLogoYposition + 40)
       withAttributes:numberAttrsDictionary];

  // description
  NSMutableParagraphStyle *style = [[NSMutableParagraphStyle alloc] init];
  [style setAlignment:NSTextAlignmentCenter];
  [style setLineBreakMode:NSLineBreakByWordWrapping];

  UIFont *descriptionfont = [UIFont systemFontOfSize:22.0];
  NSDictionary *descriptionAttrsDictionary = [NSDictionary
      dictionaryWithObjectsAndKeys:descriptionfont, NSFontAttributeName,
                                   [NSNumber numberWithFloat:1.0],
                                   NSBaselineOffsetAttributeName,
                                   [UIColor colorWithRed:(255 / 255.0)
                                                   green:(255 / 255.0)
                                                    blue:(255 / 255.0)
                                                   alpha:1],
                                   NSForegroundColorAttributeName, style,
                                   NSParagraphStyleAttributeName, nil];
  [description drawInRect:CGRectMake(0, grayBackgroundYPosition + 10,
                                     grayBackground.size.width,
                                     grayBackground.size.height - 15)
           withAttributes:descriptionAttrsDictionary];

  // mzad download text
  UIImage *mzadDownloadText = [UIImage imageNamed:@"Share_download_text"];
  [mzadDownloadText
      drawAtPoint:CGPointMake(whiteBackground.size.width -
                                  mzadDownloadText.size.width - 10,
                              whiteBackgroundYPostion + 5)];

  UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
  UIGraphicsEndImageContext();
  return img;
}

- (void)requestFinished:(ServerManagerResult *)serverManagerResult
            ofRequestId:(NSString *)requestId
    withNumberOfRequest:(int)numberOfRequest {
  // stop animation
  if (requestId == requestAddLike) {
    [self.likeIndicator stopAnimating];
  } else if (requestId == requestAddFavorite) {
    [self.addToFavoriteIndicator stopAnimating];
  } else if (requestId == requestReportAdvertise) {
    [self.reportIndicator stopAnimating];

    productObject.isReportingAdvertise = false;
  }

  // show status text field
  self.resultTextField.hidden = NO;

  if ([serverManagerResult.opertationResult
          isEqualToString:[ServerManager ERROR_IN_CONNECTION_OPERATION]]) {
    [DialogManager showErrorInConnection];
  }

  // set status text
  if ([serverManagerResult.opertationResult
          isEqualToString:[ServerManager SUCCESS_OPERATION]]) {
    self.resultTextField.text = NSLocalizedString(@"SUCCESS_ADD", nil);
  } else if ([serverManagerResult.opertationResult
                 isEqualToString:[ServerManager FAILED_OPERATION]])

  {
    self.resultTextField.text = NSLocalizedString(@"ERROR_ADD", nil);
  } else if ([serverManagerResult.opertationResult
                 isEqualToString:[ServerManager ALREADED_ADDED_OPERATION]])

  {
    self.resultTextField.text = NSLocalizedString(@"ALREADY_ADD", nil);
  }
}

- (void)showReportAdvertiseDialogue {
    
  ReportItemViewController *controller = [[SharedData getCurrentStoryBoard]
      instantiateViewControllerWithIdentifier:@"AdvertiseReportViewController"];
  controller.delegate = self;
  [parentView.navigationController pushViewController:controller animated:YES];

}

-(UIImage*)downloadShareImage
{
    [MBProgressHUD showHUDAddedTo:parentView.view animated:YES];
    NSURL *url = [NSURL URLWithString:productObject.productShareImageUrl];
    UIImage *image = [UIImage imageWithData: [NSData dataWithContentsOfURL:url]];
    [MBProgressHUD hideAllHUDsForView:parentView.view animated:YES];

    return image;
}

- (void)reportAdWithReasonWithReasonID:(NSString *)reasonID
                         andReasonText:(NSString *)reasonText

{
  //[self.reportIndicator startAnimating];
  [productObject setIsReportingAdvertise:YES];
  [productObject setReportReasonId:reasonID];
  [productObject setReportReasonText:reasonText];

  [tableView reloadData];
}

@end
