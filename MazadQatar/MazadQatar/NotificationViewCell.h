//
//  NotificationViewCell.h
//  Mzad Qatar
//
//  Created by Perveen Akhtar on 02/07/2014.
//  Copyright (c) 2014 Mohammed abdulfattah attia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ServerManager.h"

@interface NotificationViewCell
    : UITableViewCell <ServerManagerResponseDelegate> {
  NotificationModal *notificationModel;
}
@property(strong, nonatomic) IBOutlet UIImageView *unReadNotificationImg;
@property(strong, nonatomic) IBOutlet UIImageView *imageOfNotification;
@property(strong, nonatomic) IBOutlet UILabel *dateOfNotification;
@property(strong, nonatomic) IBOutlet UILabel *titleOfNotification;
@property(strong, nonatomic)
    IBOutlet UIActivityIndicatorView *imageLoadingIndicatorOfNotification;
@property(strong, nonatomic) IBOutlet UIButton *stopNotificationButton;
@property(strong, nonatomic)
    IBOutlet UIActivityIndicatorView *stopNotifcationIndicator;
@property(strong, nonatomic) IBOutlet UIImageView *stopNotificationUnderline;
@property(strong, nonatomic) IBOutlet UILabel *stopNotificationResult;

- (void)setNotiifcationProductId:(NotificationModal *)notificationModelParam;

@end
