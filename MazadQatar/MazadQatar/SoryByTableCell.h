//
//  SoryByTableCell.h
//  Mzad Qatar
//
//  Created by Mohammed AbdulFattah on 12/12/14.
//  Copyright (c) 2014 Mohammed abdulfattah attia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SoryByTableCell : UITableViewCell

@property(strong, nonatomic) IBOutlet UILabel *sortByTextView;
@property(strong, nonatomic) IBOutlet UIImageView *sortByImageView;
@property(strong, nonatomic) IBOutlet UIActivityIndicatorView *sortByIndicator;

@end
