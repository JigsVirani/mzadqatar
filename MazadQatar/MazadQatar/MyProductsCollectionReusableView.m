//
//  MyProductsCollectionReusableView.m
//  Mzad Qatar
//
//  Created by Mohammed AbdulFattah on 1/29/15.
//  Copyright (c) 2015 Mohammed abdulfattah attia. All rights reserved.
//

#import "MyProductsCollectionReusableView.h"
#import "SharedData.h"
#import "DisplayUtility.h"

@implementation MyProductsCollectionReusableView

- (void) awakeFromNib
{
    [super awakeFromNib];
    if(![[SharedData getDeviceLanguage]isEqualToString:@"en"]){
        self.btnDetailArrow.transform = CGAffineTransformMakeRotation(M_PI);
    }
    if([DisplayUtility isPad]) {
       [_nameUserProfile setFont:[UIFont boldSystemFontOfSize:22.0]];
        [_numberOfAdsUserProfile setFont:[UIFont systemFontOfSize:18.0]];
        self.cmpLogoWidthConstant.constant = 120.0;
        self.cmpLogoHeightConstant.constant = 25.0;
        [_shortDescriptionUserProfile setFont:[UIFont systemFontOfSize:20.0]];
        [_numberUserProfile setFont:[UIFont systemFontOfSize:18.0]];
        _callBtnWidthConstant.constant=_callBtnWidthConstant.constant+_callBtnWidthConstant.constant/2.0;
        _shareBtnWidthConstant.constant=_shareBtnWidthConstant.constant+_shareBtnWidthConstant.constant/2.0;
        _editBtnWidthConstant.constant=_editBtnWidthConstant.constant+_editBtnWidthConstant.constant/2.0;
        _locationBtnWidthConstant.constant=_locationBtnWidthConstant.constant+_locationBtnWidthConstant.constant/2.0;
        
        self.topButtonsHeight.constant = 40;
        
        _callBtnHeightConstant.constant=30.0;
        _shareBtnHeightConstant.constant=30.0;
        _editBtnHeightConstant.constant=30.0;
        _locationBtnHeightConstant.constant=30.0;
        
        _btnCall.titleLabel.font = [UIFont systemFontOfSize:20.0];
        _btnLocation.titleLabel.font = [UIFont systemFontOfSize:20.0];
        _btnEditInfo.titleLabel.font = [UIFont systemFontOfSize:20.0];
        _btnShare.titleLabel.font = [UIFont systemFontOfSize:20.0];
    }
}
@end
