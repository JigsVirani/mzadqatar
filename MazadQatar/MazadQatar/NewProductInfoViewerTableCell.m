//
//  ProductInfoViewerTableCell.m
//  MazadQatar
//
//  Created by samo on 5/27/13.
//  Copyright (c) 2013 Mohammed abdulfattah attia. All rights reserved.
//

#import "NewProductInfoViewerTableCell.h"
#import "DisplayUtility.h"
#import "UserSetting.h"
#import "ServerManager.h"
@implementation NewProductInfoViewerTableCell

NSString *NewrequestAddFavorite = @"requestAddFavorite";
NSString *NewrequestReportAdvertise = @"requestReportAdvertise";

- (id)initWithStyle:(UITableViewCellStyle)style
    reuseIdentifier:(NSString *)reuseIdentifier {
  self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
  if (self) {
    // Initialization code
  }
  return self;
}

-(void) awakeFromNib
{
    [super awakeFromNib];
    if (IS_IPAD) {
        _iPadStackView.hidden = false;
        _iPhoneStackView.hidden = true;
        _callBtn.titleLabel.font = [UIFont systemFontOfSize:16.0];
        _chatBtn.titleLabel.font = [UIFont systemFontOfSize:16.0];
        _emailBtn.titleLabel.font = [UIFont systemFontOfSize:16.0];
        _numberCopyBtn.titleLabel.font = [UIFont systemFontOfSize:16.0];
        _addToFavoriteBtn.titleLabel.font = [UIFont systemFontOfSize:15.0];
        _reportButton.titleLabel.font = [UIFont systemFontOfSize:15.0];
        _viewInWebSite.titleLabel.font = [UIFont systemFontOfSize:15.0];
        _productDate.font = [UIFont systemFontOfSize:22.0];
        _productPricez.font = [UIFont systemFontOfSize:22.0];
        _byLbl.font = [UIFont systemFontOfSize:22.0];
        _totalViewsBtn.font = [UIFont systemFontOfSize:22.0];
        _resultTextField.font = [UIFont systemFontOfSize:22.0];
        _userNumber.font = [UIFont systemFontOfSize:22.0];
    }else{
        _iPadStackView.hidden = true;
        _iPhoneStackView.hidden = false;
    }
    _userNumber.contentInset = UIEdgeInsetsMake(-7.0,0.0,0,0.0);
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
  [super setSelected:selected animated:animated];

  // Configure the view for the selected state
}

- (IBAction)viewInWebsiteBtn:(UIButton *)sender {
  [[UIApplication sharedApplication]
      openURL:[NSURL URLWithString:productObject.productWebsiteUrl]];
}

- (void)setViewProductInfoUi:(ProductObject *)productObjectParam {
  //[self changeAddProductsFieldsToReadingOnly];
  productObject = productObjectParam;

  [self setStringInTextView];
}
- (void)setStringInTextView {
    // iOS6 and above : Use NSAttributedStrings
    
    /*[self setUserInteractionEnabled:false];
    [self.loadingIndicator startAnimating];
    [self.reportIndicator setHidden:YES];
    if(!(productObject.productUserImageUrl == nil)){
        [self.userAvtarImage
         sd_setImageWithURL:[NSURL URLWithString:productObject.productUserImageUrl]
         placeholderImage:nil
         completed:^(UIImage *image, NSError *error,
                     SDImageCacheType cacheType, NSURL *imageURL) {
             [self setUserInteractionEnabled:true];
             [self.loadingIndicator stopAnimating];
             [self setNeedsLayout];
         }];
    }*/
	//dispatch_sync(dispatch_get_main_queue(), ^{
		self.userAvtarImage.layer.cornerRadius = self.userAvtarImage.bounds.size.height/2.0;
		self.userAvtarImage.clipsToBounds = YES;
        [self.userAvtarImage layoutIfNeeded];
	//});
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0ul);
    dispatch_async(queue, ^{
        
        //[self setUserInteractionEnabled:false];
        [self.loadingIndicator startAnimating];
        [self.reportIndicator setHidden:YES];
        
        [self.userAvtarImage
         sd_setImageWithURL:[NSURL URLWithString:productObject.productUserImageUrl]
         placeholderImage:nil
         completed:^(UIImage *image, NSError *error,
                     SDImageCacheType cacheType, NSURL *imageURL) {
             [self setUserInteractionEnabled:true];
             [self.loadingIndicator stopAnimating];
         }];
		
    });
    
    if([productObject.productUserisCompany isEqualToString:@"0"]){
        [self.isCompany setHidden:false];
    }else{
        [self.isCompany setHidden:true];
    }
    self.userAvtarImage.layer.cornerRadius = self.userAvtarImage.bounds.size.height/2.0;
    self.userAvtarImage.clipsToBounds = YES;
    [self.userAvtarImage layoutIfNeeded];
    const CGFloat fontSize = UI_USER_INTERFACE_IDIOM() != UIUserInterfaceIdiomPad?15:20;
    UIFont *boldFont = [UIFont boldSystemFontOfSize:10.0];
    UIFont *regularFont = [UIFont boldSystemFontOfSize:fontSize];
    UIFont *smallFont = [UIFont boldSystemFontOfSize:UI_USER_INTERFACE_IDIOM() != UIUserInterfaceIdiomPad ? 12.0:20.0];
    
    UIColor *foregroundColor = [UIColor whiteColor];
    
    _addToFavoriteBtn.layer.cornerRadius = 4.0;
    _reportButton.layer.cornerRadius = 4.0;
    _viewInWebSite.layer.cornerRadius = 4.0;
    
    if(![productObject.productTilte isEqualToString:NSLocalizedString(@"LOADING", nil)])
    {
        [self.loadAllDetailIndicator stopAnimating];
    }
    // Create the attributes
    NSDictionary *attrsForTitle = [NSDictionary
                                   dictionaryWithObjectsAndKeys:regularFont, NSFontAttributeName,
                                   foregroundColor,
                                   NSForegroundColorAttributeName, nil];
    NSDictionary *subAttrs = [NSDictionary
                              dictionaryWithObjectsAndKeys:boldFont, NSFontAttributeName,
                              foregroundColor,
                              NSForegroundColorAttributeName, nil];
    NSDictionary *attrs = [NSDictionary
                           dictionaryWithObjectsAndKeys:smallFont, NSFontAttributeName,
                           foregroundColor,
                           NSForegroundColorAttributeName, nil];
    
    NSDictionary *attrsForNumber = [NSDictionary
                                    dictionaryWithObjectsAndKeys:smallFont, NSFontAttributeName, nil];
    
    //Comment new position
    
    NSString* string = NSLocalizedString(@"Comments_Lable_Text",nil);
    
    NSString* string2 =[NSString stringWithFormat:@"(%@)",productObject.productCommentCount];
    self.commentsCountLabel.text =[string stringByAppendingString:string2];
    
    
    
    // total views btn
    if (productObject.productNumberOfViews != nil) {
        
        NSString* string = NSLocalizedString(@"View_Count_For_adv",nil);
        self.totalViewsBtn.text = [string stringByAppendingString:productObject.productNumberOfViews];
        
    }
    
    // Create the attributed string (text + attributes)
    //  const NSRange productItemRange =
    //      NSMakeRange(0, NSLocalizedString(@"PRODUCT_ITEM_NAME", nil).length);
    NSMutableAttributedString *productItemAttributedText = [
                                                            [NSMutableAttributedString alloc]
                                                            initWithString:[NSString stringWithFormat:@"%@",
                                                                            productObject.productTilte]
                                                            attributes:attrsForTitle];
    //  [productItemAttributedText setAttributes:subAttrs range:productItemRange];
    // Set it in our UILabel and we are done!
    [self.productItemName setAttributedText:productItemAttributedText];
    
    //NSString *countryCodePlusNumber = [NSString stringWithFormat:@" +974%@",productObject.productUserNumber];
    NSString *countryCodePlusNumber = [NSString stringWithFormat:@" %@%@",productObject.productCountryCode,productObject.productUserNumber];
    [self.userNumber setText:countryCodePlusNumber];
    
    if([LanguageManager currentLanguageIndex]==1){
        [self.userNumber setTextAlignment:NSTextAlignmentRight];
    }else{
        [self.userNumber setTextAlignment:NSTextAlignmentLeft];
    }
    NSMutableAttributedString *productUserNameAttributedText = [
                                                                [NSMutableAttributedString alloc]
                                                                initWithString:[NSString stringWithFormat:@"%@",
                                                                                productObject.productUserName]
                                                                attributes:attrs];
    
    [self.userName setAttributedText:productUserNameAttributedText];
    //  const NSRange productPriceRange =
    //      NSMakeRange(0, NSLocalizedString(@"PRODUCT_PRICE", nil).length);
    //  NSMutableAttributedString *productPriceAttributedText =
    //      [[NSMutableAttributedString alloc]
    //          initWithString:[NSString stringWithFormat:@"Price : %@",
    //                                                    productObject.productPrice]
    //              attributes:attrs];
    //  [productPriceAttributedText setAttributes:subAttrs range:productPriceRange];
    //  // Set it in our UILabel and we are done!
    //    [self.productPricez setText:[NSString stringWithFormat:@"Price:%@QAR",
    //                                 productObject.productPrice]];
    
    const NSRange productPriceRange =
    NSMakeRange(0, NSLocalizedString(@"PRODUCT_PRICE", nil).length);
    NSMutableAttributedString *productPriceAttributedText =
    [[NSMutableAttributedString alloc]
     initWithString:[NSString stringWithFormat:@"%@%@%@",
                     NSLocalizedString(
                                       @"PRODUCT_PRICE", nil),
                     productObject.productPrice,
                     NSLocalizedString(
                                       @"QAR", nil)]
     attributes:attrs];
    [productPriceAttributedText setAttributes:[NSDictionary
                                               dictionaryWithObjectsAndKeys:
                                               foregroundColor,
                                               NSForegroundColorAttributeName, nil] range:productPriceRange];
    // Set it in our UILabel and we are done!
    [self.productPricez setAttributedText:productPriceAttributedText];
    
    //
    //  const NSRange productPhoneNumberRange =
    //      NSMakeRange(0, NSLocalizedString(@"PRODUCT_PHONE_NUMBER", nil).length);
    //  NSMutableAttributedString *productPhoneNumberAttributedText =
    //      [[NSMutableAttributedString alloc]
    //          initWithString:[NSString
    //                             stringWithFormat:@"%@ %@%@",
    //                                              NSLocalizedString(
    //                                                  @"PRODUCT_PHONE_NUMBER", nil),
    //                                              @"00974",
    //                                              productObject.productUserNumber]
    //              attributes:attrs];
    //  [productPhoneNumberAttributedText setAttributes:subAttrs
    //                                            range:productPhoneNumberRange];
    self.productDate.text = productObject.productDate;
    
    //  const NSRange productDateRange =
    //      NSMakeRange(0, NSLocalizedString(@"PRODUCT_DATE", nil).length);
    //  NSMutableAttributedString *productDateAttributedText =
    //      [[NSMutableAttributedString alloc]
    //          initWithString:[NSString stringWithFormat:@"%@",
    //                                                    productObject.productDate]
    //              attributes:attrs];
    //  [productDateAttributedText setAttributes:subAttrs range:productDateRange];
    // Set it in our UILabel and we are done!
    
    
    
    // const NSRange productDescriptionRange =
    //     NSMakeRange(0, NSLocalizedString(@"PRODUCT_Description", nil).length);
    NSMutableAttributedString *productDescriptionAttributedText =
    [[NSMutableAttributedString alloc]
     initWithString:[NSString
                     stringWithFormat:@"%@",
                     productObject.productDescriptionWithTitles]
     attributes:attrs];
    
    productDescriptionAttributedText= [self boldTextInDescription:productDescriptionAttributedText andStyle:subAttrs];
    
    //[productDescriptionAttributedText setAttributes:subAttrs
    //                                          range:productDescriptionRange];
    // Set it in our UILabel and we are done!
    
    
}

-(NSMutableAttributedString*)boldTextInDescription:(NSMutableAttributedString*)text andStyle: (NSDictionary *)subAttrs
{
    NSRange range = [productObject.productDescriptionWithTitles rangeOfString:@"%B"];
   // [text setAttributes:subAttrs
   //                                           range:range];
    
    NSRange startRangeBoldKey=range;
    NSRange endRangeBoldKey=NSMakeRange(0, 0);
    while(range.location!=NSNotFound)
    {
        productObject.productDescriptionWithTitles=[text string];

        if(range.location+1>productObject.productDescriptionWithTitles.length)
        {
            break;
        }
        range = [productObject.productDescriptionWithTitles rangeOfString:@"%B" options:0 range:NSMakeRange(range.location+1,productObject.productDescriptionWithTitles.length-(range.location+1))];
        
        if(startRangeBoldKey.length==0)
        {
            startRangeBoldKey=range;
        }
        else
        {
            endRangeBoldKey=range;
        }
        
        if(startRangeBoldKey.length!=0&endRangeBoldKey.length!=0)
        {
            [text setAttributes:subAttrs
                          range:NSMakeRange(startRangeBoldKey.location+1, endRangeBoldKey.location-(startRangeBoldKey.location+1))];
            [text replaceCharactersInRange:startRangeBoldKey withString:@""];
            [text replaceCharactersInRange:NSMakeRange(endRangeBoldKey.location-2, endRangeBoldKey.length) withString:@""];

            range=NSMakeRange(endRangeBoldKey.location-2, endRangeBoldKey.length);
            startRangeBoldKey=NSMakeRange(0, 0);
            endRangeBoldKey=NSMakeRange(0, 0);
            
        }
        
    }

    return text;
}
- (void)setProductObject:(ProductObject *)productObjectParam
            InParentView:(UIViewController *)parentViewParam
             inTableView:(UITableView *)tableViewParam {
    productObject = productObjectParam;
    parentView = parentViewParam;
    tableView = tableViewParam;
        
    if (productObject.isReportingAdvertise) {
        self.resultTextField.hidden = YES;
        [self.reportIndicator startAnimating];
        [ServerManager reportAdvertise:productObject
                           AndDelegate:self
                         withRequestId:NewrequestReportAdvertise];
    }
}

- (IBAction)addToFavoriteBtnClicked:(id)sender {
    //self.addToFavoriteBtn.hidden = YES;
    self.addToFavoriteBtn.frame = CGRectMake(0, 0, self.addToFavoriteBtn.frame.size.width, 0);
    if ([UserSetting isUserRegistered] == false) {
        [SharedViewManager showLoginScreen:parentView];
        self.resultTextField.hidden = NO;
        self.resultTextField.text =
        NSLocalizedString(@"ERROR_TRY_AGAIN_AFTER_REGISTER", nil);
    } else {
        self.resultTextField.hidden = YES;
        [self.addToFavoriteIndicator startAnimating];
        NSString *isAd;
        if (productObject.isAd == true) {
            isAd = @"0";
        } else {
            isAd = @"1";
        }
        [ServerManager addProductToFavorite:productObject
                                AndDelegate:self
                              withRequestId:NewrequestAddFavorite
                                  withIsAdd:isAd];
    }
}
- (IBAction)reportBtnClicked:(id)sender {
    //self.reportButton.hidden = YES;
    self.reportIndicator.hidden = NO;
    [self.reportIndicator startAnimating];
    [self showReportAdvertiseDialogue];
    
}
- (void)requestFinished:(ServerManagerResult *)serverManagerResult
            ofRequestId:(NSString *)requestId
    withNumberOfRequest:(int)numberOfRequest {
    // stop animation
    if (requestId == NewrequestAddFavorite) {
        self.addToFavoriteBtn.hidden = NO;
        
        if([[LanguageManager currentLanguageCode]isEqualToString:@"en"]){
            self.addToFavoriteBtn.frame = CGRectMake(0, 0, self.addToFavoriteBtn.frame.size.width, self.addToFavoriteBtn.superview.frame.size.height);
        }else{
            self.addToFavoriteBtn.frame = CGRectMake(self.addToFavoriteBtn.superview.frame.size.width-self.addToFavoriteBtn.frame.size.width,0, self.addToFavoriteBtn.frame.size.width, self.addToFavoriteBtn.superview.frame.size.height);
        }
        
        [self.addToFavoriteIndicator stopAnimating];
    } else if (requestId == NewrequestReportAdvertise) {
        [self.reportIndicator stopAnimating];
        self.reportIndicator.hidden = YES;
        self.reportButton.hidden = NO;
        productObject.isReportingAdvertise = false;
    }
    // show status text field
    self.resultTextField.hidden = NO;
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        self.resultTextField.hidden = YES;
    });
    
    if ([serverManagerResult.opertationResult
         isEqualToString:[ServerManager ERROR_IN_CONNECTION_OPERATION]]) {
        [DialogManager showErrorInConnection];
    }
    
    // set status text
    if ([serverManagerResult.opertationResult
         isEqualToString:[ServerManager SUCCESS_OPERATION]]) {
        self.resultTextField.text = NSLocalizedString(@"SUCCESS_ADD", nil);
    } else if ([serverManagerResult.opertationResult
                isEqualToString:[ServerManager FAILED_OPERATION]])
        
    {
        self.resultTextField.text = NSLocalizedString(@"ERROR_ADD", nil);
    } else if ([serverManagerResult.opertationResult
                isEqualToString:[ServerManager ALREADED_ADDED_OPERATION]])
        
    {
        self.resultTextField.text = NSLocalizedString(@"ALREADY_ADD", nil);
    }
    else if ([serverManagerResult.opertationResult
              isEqualToString:[ServerManager ERROR_USER_BLOCKED_BY_ANOTHER_USER]])
    {
        self.resultTextField.text = NSLocalizedString(@"USER_BLOCKED_YOU", nil);
    }
}

- (void)showReportAdvertiseDialogue {
    ReportItemViewController *controller = [[SharedData getCurrentStoryBoard]
                                            instantiateViewControllerWithIdentifier:@"AdvertiseReportViewController"];
    
    controller.delegate = self;
    
    [parentView.navigationController pushViewController:controller animated:YES];
}
- (void)reportAdWithReasonWithReasonID:(NSString *)reasonID
                         andReasonText:(NSString *)reasonText

{
    
    //[self.reportIndicator startAnimating];
    [productObject setIsReportingAdvertise:YES];
    [productObject setReportReasonId:reasonID];
    [productObject setReportReasonText:reasonText];
   

    [tableView reloadData];
}
-(IBAction)GotoProfileScreen:(id)sender
{
    
    
}
@end
