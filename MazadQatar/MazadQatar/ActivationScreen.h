//
//  ActivationScreen.h
//  MazadQatar
//
//  Created by samo on 4/1/13.
//  Copyright (c) 2013 Mohammed abdulfattah attia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ServerManager.h"
#import "TextFieldViewController.h"
#import "UserSetting.h"
#import "DisplayUtility.h"

@interface ActivationScreen
    : TextFieldViewController <ServerManagerResponseDelegate>

@property(weak) NSString *successScreenSegueName;
@property(strong,nonatomic) NSString *userRegisteredNumber;

@property(strong, nonatomic) IBOutlet UITextField *activationNumberField;
- (IBAction)activateBtnClicked:(id)sender;
- (IBAction)resendBtnClicked:(id)sender;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activateIndicator;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *resendIndicator;


@property (weak,nonatomic)IBOutlet UILabel *activationLbl;
@property (weak,nonatomic)IBOutlet UITextView *detailTxtView;
@property (weak,nonatomic)IBOutlet UIButton *activateBtn,*resendBtn;
@property(weak, nonatomic)IBOutlet NSLayoutConstraint *btnActivateHeightConstant,*btnResendHeightConstant;
@end
