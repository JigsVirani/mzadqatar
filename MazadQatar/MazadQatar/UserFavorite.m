//
//  UserFavoriteViewController.m
//  MazadQatar
//
//  Created by samo on 4/28/13.
//  Copyright (c) 2013 Mohammed abdulfattah attia. All rights reserved.
//

#import "UserFavorite.h"
#import "ProductDetailsViewController.h"
@interface UserFavorite ()

@end

static int userFavoritePageNumber = 1;
static int userFavoriteNumberOfPages = 50;
@implementation UserFavorite

NSString *requestInitialUserFavorite = @"requestUserFavorite";
NSString *requestMoreUserFavorite = @"requestMoreUserFavorite";
NSString *requestDeleteUserFavorite = @"requestDeleteUserFavorite";
NSString *requestRefreshUserFavorites = @"requestRefreshUserFavorites";

- (void)viewDidLoad {
    [super viewDidLoad];
    self.screenName = @"User Favorite Screen";
    
    loadMoreWhereAvailable = YES;
    
    ipadCellWidth = 242;
    iphoneCellWidth = ([UIScreen mainScreen].bounds.size.width-30.0)/2;
    
    [self addRefreshControlToGrid];
    
    // add background listner
    [self addBackgroundListner];
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    [self.navigationController.tabBarController.tabBar setHidden:NO];
}
- (void)requestFinished:(ServerManagerResult *)serverManagerResult
            ofRequestId:(NSString *)requestId
    withNumberOfRequest:(int)numberOfRequest {
    if ([serverManagerResult.opertationResult
         isEqualToString:[ServerManager ERROR_IN_CONNECTION_OPERATION]]) {
        [DialogManager showErrorInConnection];
    }
    
    if (requestId == requestInitialUserFavorite) {
        [self showOverlayWithIndicator:false];
    }
    // error in server
    if ([serverManagerResult.opertationResult
         isEqualToString:[ServerManager FAILED_OPERATION]]) {
        //Comment [DialogManager showErrorInServer];
        [DialogManager showErrorWithMessage:serverManagerResult.opertationMsg];
        return;
    }
    
    if (requestId == requestInitialUserFavorite ||
        requestId == requestMoreUserFavorite ||
        requestId == requestRefreshUserFavorites) {
        loading = NO;
        
        [SharedData
         setUserFavoriteLastUpdateTime:serverManagerResult.lastUpdateTime];
        
        if (requestId == requestInitialUserFavorite ||
            requestId == requestRefreshUserFavorites) {
            // error happen in server and returened jsonarray nil
            // if(serverManagerResult.jsonArray.count==0)
            // {
            // [ErrorManager showErrorInServer];
            
            // return;
            // }
            
            if (refreshControl.isRefreshing) {
                [refreshControl endRefreshing];
            }
            
            productItems = serverManagerResult.jsonArray;
            
            if (productItems.count == 0) {
                [self addLabelToMiddleWithText:NSLocalizedString(
                                                                 @"NO_USER_FAVORITES_AVAILABLE", nil)
                              inCollectionView:NO];
            } else {
                [self removeLabelfromMiddleOfView];
            }
            
           // [[NotificationViewController getInstance] refreshNowNotificationCount];
            
        } else if (requestId == requestMoreUserFavorite) {
            if (serverManagerResult.jsonArray.count > 0) {
                [productItems addObjectsFromArray:serverManagerResult.jsonArray];
            } else {
                loadMoreWhereAvailable = NO;
            }
        }
        
        if (requestId != requestRefreshUserFavorites) {
            userFavoritePageNumber++;
        }
        
        [self.collectionViewGridView reloadData];
    }
    
    else if (requestId == requestDeleteUserFavorite) {
        if ([serverManagerResult.opertationResult
             isEqualToString:[ServerManager SUCCESS_OPERATION]]) {
            [self startLoadingUserFavoriteWithRequestId:requestInitialUserFavorite
                                        andIsNewSession:YES];
            
        } else if ([serverManagerResult.opertationResult
                    isEqualToString:[ServerManager FAILED_OPERATION]]) {
            // start loading again the deleted item
            [DialogManager showErrorInDeletingAdvertise];
            
        } else if ([serverManagerResult.opertationResult
                    isEqualToString:[ServerManager ALREADED_ADDED_OPERATION]]) {
            [DialogManager showErrorProductNotFound];
            
            [self refreshCurrentGrid];
        }
    }
    //    else if(requestId==requestRefreshUserFavorites)
    //    {
    //        loading=NO;
    //
    //        [self.collectionViewGridView reloadData];
    //    }
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [self doneBtnClicked:nil];
    
    if ([UserSetting isUserRegistered] == false) {
        [SharedViewManager showLoginScreen:self];
        return;
    } else {
        
        [self startLoadingUserFavoriteWithRequestId:requestInitialUserFavorite
                                    andIsNewSession:YES];
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:[SharedViewManager registerSegue]]) {
        RegisterScreen *registerScreen = [segue destinationViewController];
        
        [registerScreen setSuccessScreenSegueName:nil];
        registerScreen.navigationItem.hidesBackButton = YES;
    } else if ([[segue identifier] isEqualToString:@"ImageViewClicked"] ||
               [[segue identifier] isEqualToString:@"ImageAdViewClicked"]) {
        // get the index clicked
        UICollectionViewCell *cell = (UICollectionViewCell *)sender;
        NSIndexPath *indexPath =
        [self.collectionViewGridView indexPathForCell:cell];
        
        ProductObject *productObjectSelected =
        [productItems objectAtIndex:indexPath.row];
        // open category products and set the index
        ProductDetailsViewController *productDetail =
        [segue destinationViewController];
        [productDetail setProductId:productObjectSelected.productId];
        
        // [productDetail setProductObject:productObjectSelected];
    }
    
    [super prepareForSegue:segue sender:sender];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    //[SharedData clearSdImageMemory];
}

- (void)loadData:(NSNumber *)isNewSession {
    [self startLoadingUserFavoriteWithRequestId:requestMoreUserFavorite
                                andIsNewSession:NO];
}


- (void)cellDeleted:(UICollectionViewCell *)cell {
    NSIndexPath *index = [self.collectionViewGridView indexPathForCell:cell];
    
    ProductObject *productToBeDeleted = [productItems objectAtIndex:index.row];
    
    productToBeDeleted.isDeleting = YES;
    
    [self.collectionViewGridView reloadData];
    
    [ServerManager deleteUserFavorite:productToBeDeleted
                          AndDelegate:self
                        withRequestId:requestDeleteUserFavorite];
}

- (void)startLoadingUserFavoriteWithRequestId:(NSString *)requestId
                              andIsNewSession:(bool)isNewSession {
    if (requestId == requestInitialUserFavorite) {
        
        loadMoreWhereAvailable=YES;
        
        loading = YES;
        
        // when switching between tabs and there is 100 item loaded , all items are
        // gone because we load only 50
        if ([self.productItems count] != 0 && isPullNewData == false) {
            [self refreshCurrentGrid];
            
            return;
        }
        
        [self showOverlayWithIndicator:true];
        
        [SharedData setUserFavoriteLastUpdateTime:@"0"];
        userFavoritePageNumber = 1;
    }
    
    [ServerManager
     getUserFavoritewithDelegate:self
     withUserProductsPageNumber:userFavoritePageNumber
     withUserProductsNumberOfPages:userFavoriteNumberOfPages
     withRequestId:requestId
     withUpdatTime:[SharedData getUserFavoriteLastUpdateTime]
     byLanguage:
     [UserSetting
      getAdvertiseLanguageServerParam:
      [UserSetting
       getUserFilterViewAdvertiseLanguage]]
     isAdSupported:@"true"
     countOfRows:self.advertiseInOneRowCountString
     withAdvertiseResolution:self.advertiseResolution];
}

- (void)refreshCurrentGrid {
    [ServerManager
     getUserFavoritewithDelegate:self
     withUserProductsPageNumber:1
     withUserProductsNumberOfPages:userFavoriteNumberOfPages *
     (userFavoritePageNumber - 1)
     withRequestId:requestRefreshUserFavorites
     withUpdatTime:@"0"
     byLanguage:
     [UserSetting
      getAdvertiseLanguageServerParam:
      [UserSetting
       getUserFilterViewAdvertiseLanguage]]
     isAdSupported:@"true"
     countOfRows:self.advertiseInOneRowCountString
     withAdvertiseResolution:self.advertiseResolution];
}

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    ProductObject *productObject = [productItems objectAtIndex:indexPath.row];
    
    if (productObject.isAd) {
        if ([DisplayUtility isPad]) {
            return CGSizeMake(768, 227);
        } else {
            return CGSizeMake(320, 226);
        }
    } else {
        if ([DisplayUtility isPad]) {
            return CGSizeMake(ipadCellWidth, 373);
        } else {
            return CGSizeMake(iphoneCellWidth, iphoneCellWidth + 114);
        }
    }
}

- (void)getNewData:(NSString *)searchStr {
    if ([searchStr isEqualToString:@""] == false) {
        [SharedViewManager showCategoryProductsWithSearch:self
                                             andProductId:@"0"
                                             andSearchStr:searchStr
                                         andSubCategoryId:@""
                                       andSubCategoryName:@""
                                              andSortById:@""
                                            andSortByName:@""
                                           andsortByIndex:-1
                                            andTabBarName:  [self getTabBarSearchAllCategories] andDefaultTabSelected:0];
        
        return;
    }
    
    [self startLoadingUserFavoriteWithRequestId:requestInitialUserFavorite
                                andIsNewSession:YES];
}

- (void)viewDidUnload {
    [super viewDidUnload];
}

@end
