//
//  SharedViewManager.h
//  MazadQatar
//
//  Created by samo on 4/3/13.
//  Copyright (c) 2013 Mohammed abdulfattah attia. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SharedViewManager : NSObject

+ (void)showLoginScreen:(UIViewController *)view;
+ (void)showAddAdvertiseScreen:(UIViewController *)view;
+ (void)showCategoryProductsWithSearch:(UIViewController *)view
                          andProductId:(NSString *)productId
                          andSearchStr:(NSString *)searchStr
                      andSubCategoryId:(NSString *)subcategoryServerId
                    andSubCategoryName:(NSString *)subCategoryName
                           andSortById:(NSString *)sortById
                         andSortByName:(NSString *)sortByName
                        andsortByIndex:(int)sortByIndex
                         andTabBarName:(NSMutableArray *)productAdvertiseTypes andDefaultTabSelected:(int)defautlTabSelected;

+ (void)showProductDetailView:(NSString *)productId fromView:(UIViewController*)view ;
+ (NSString *)successAddAdvertiseSugeue;
+ (NSString *)successUserProductSugeue;
+ (NSString *)registerSegue;
+ (NSString *)chooseAddAdvertiseLanguageSegue;
+ (NSString *)addAdvertisSegue;
+(UIImage *)writeOnImageTitle:(NSString *)title
               andDescription:(NSString *)description
                    andNumber:(NSString *)number
                      onImage:(UIImage *)image;
@end
