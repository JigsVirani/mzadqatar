//
//  RateViewController.m
//  Mzad Qatar
//
//  Created by Waris Ali on 23/12/2014.
//  Copyright (c) 2014 Mohammed abdulfattah attia. All rights reserved.
//

#import "RateViewController.h"
#import "SharedData.h"

@interface RateViewController ()

@end

@implementation RateViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.screenName = @"Rate Dialogue";
    
    rateController = [[iRate alloc] init];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little
 preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (IBAction)btnClose:(UIButton *)sender {
    [SharedData setIsRateShareDialogShown:false];
    [self.view removeFromSuperview];
    [rateController remindLater];
}

- (IBAction)rateBtnPressed:(UIButton *)sender {
    [SharedData setIsRateShareDialogShown:false];
    [self.view removeFromSuperview];
    [rateController openRatingsPageInAppStore];
}

- (IBAction)laterbtnPressed:(UIButton *)sender {
    [SharedData setIsRateShareDialogShown:false];
    [self.view removeFromSuperview];
    [rateController remindLater];
}

- (IBAction)noThanksBtnPressed:(UIButton *)sender {
    [SharedData setIsRateShareDialogShown:false];
    [self.view removeFromSuperview];
    [rateController declineThisVersion];
}
@end
