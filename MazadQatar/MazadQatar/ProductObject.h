//
//  ProductObject.h
//  MazadQatar
//
//  Created by samo on 3/3/13.
//  Copyright (c) 2013 Mohammed abdulfattah attia. All rights reserved.
//

#import <Foundation/Foundation.h>

//@class ProductObject;
//@protocol UploadProductDelegate <NSObject>
//@required
//- (void)UploadStarted;
//- (void)uploadProgress:(int)precent;
//- (void)uploadFinished;
//@required
//@end

@interface ProductObject : NSObject

@property(strong) NSString *productId;
@property(strong) NSString *productTilte;
@property(strong) NSString *productTilteEnglish;
@property(strong) NSString *productTilteArabic;
@property(strong) NSString *productMainImageUrl;
@property(strong) NSMutableArray *productImagesUrls;
@property(strong) NSMutableArray *productImagesOnDevices;
@property(strong) NSMutableArray *productComments;
@property(strong) NSMutableArray *categorySubcategories;

@property(strong) NSString *productPrice;
@property(strong) NSString *productPriceFrom;
@property(strong) NSString *productPriceTo;
@property(strong) NSString *productDescription;
@property(strong) NSString *productDescriptionWithTitles;
@property(strong) NSString *productDescriptionEnglish;
@property(strong) NSString *productDescriptionArabic;
@property(strong) NSString *productCategoryId;
@property(strong) NSString *productCategoryName;
@property(strong) NSString *productSubCategoryId;
@property(strong) NSString *productSubCategoryName;
@property(strong) NSString *productLikeCount;
@property(strong) NSString *productCommentCount;
@property(strong) NSString *productCountryCode;
@property(strong) NSString *productUserNumber;

@property(strong) NSString *productUserisCompany;
@property(strong) NSString *productUserlang;
@property(strong) NSString *productUserlong;
@property(strong) NSString *productUserlat;
@property(strong) NSString *productUserAddress;

@property(strong) NSString *productCompanyNumberOfAds;

@property(strong) NSString *productUserName;
@property(strong) NSString *productUserEmail;
@property(strong) NSString *productUserShortDescription;
@property(strong) NSString *productUserImageUrl;
@property(strong) UIImage *productUserImage;
@property(strong) NSString *productShareImageUrl;
@property(strong) NSString *productUserNumberOfAds;
@property(strong) NSString *productNumberOfViews;
@property(strong) NSString *productDate;
@property(strong) NSString *productDateInMillisecond;
@property(strong) NSString *productWebsiteUrl;

@property(strong) NSString *productUserUrl;

@property(strong) NSString *productLanguage;
@property(strong) NSMutableArray *productAdvertiseTypes;
@property(strong) NSString *productAdvertiseTypeId;
@property(strong) NSString *productAdvertiseTypeName;
@property(strong) NSString *productCityId;
@property(strong) NSString *productCityName;
@property(strong) NSString *productRegionId;
@property(strong) NSString *productRegionName;
@property(strong) NSString *productSubsubCategoryId;
@property(strong) NSString *productSubsubCategoryName;
@property(strong) NSString *productManfactureYear;
@property(strong) NSString *productManfactureYearFrom;
@property(strong) NSString *productManfactureYearTo;
@property(strong) NSString *productKm;
@property(strong) NSString *productKmFrom;
@property(strong) NSString *productKmTo;
@property(strong) NSString *productFurnishedTypeId;
@property(strong) NSString *productFurnishedTypeName;
@property(strong) NSString *productNumberOfRooms;
@property(strong) NSString *productNumberOfRoomsFrom;
@property(strong) NSString *productNumberOfRoomsTo;
@property(strong) NSString *productFilterType;
@property(assign) bool isAllFiltersSelected;

//Product clickType
@property(strong) NSString *productAdvClickType;
// sizes
@property(assign) CGSize productDescriptionSizeCurrent;
@property(assign) CGSize productDescriptionEnglishSizeCurrent;
@property(assign) CGSize productDescriptionArabicSizeCurrent;

@property(assign) CGSize productDescriptionSizePortrait;
@property(assign) CGSize productDescriptionSizeLandscape;
@property(assign) CGSize productDescriptionEnglishSizePortrait;
@property(assign) CGSize productDescriptionEnglishSizeLandscape;
@property(assign) CGSize productDescriptionArabicSizePortrait;
@property(assign) CGSize productDescriptionArabicSizeLandscape;
@property(assign) CGSize productDescriptionWithTitlesSizePortrait;
@property(assign) CGSize productDescriptionWithTitlesSizeLandscape;

@property(assign) BOOL isNeedToUploadedAdvertiseOnServer;
@property(assign) BOOL isUploading;
@property(assign) BOOL isDeleting;
@property(assign) BOOL isRefreshingAdvertise;
@property(assign) BOOL isEditingAdvertise;
@property(assign) BOOL isLoadedAdd;
@property(assign) BOOL isLoadedAddInList;

// for report advertise
@property(assign) BOOL isReportingAdvertise;
@property(strong) NSString *reportReasonId;
@property(strong) NSString *reportReasonText;

@property(assign) float productUploadingPercent;

@property(assign) BOOL isUploadAdvertiseImages;
@property(strong) NSString *isResetProductImages;

// ads
@property(strong) NSString *productAdvertiseUrl;
@property(assign) BOOL isAd;
@property(assign) BOOL isHideAddCommentsAndLikes;



@property (strong) NSString *productName;
@property (strong) NSString *productMainImage;
@property (strong) NSString *companiesCategoryImage;
-(id) copyWithZ;

@end
