//
//  AppDelegate.m
//  MazadQatar
//
//  Created by samo on 2/28/13.
//  Copyright (c) 2013 Mohammed abdulfattah attia. All rights reserved.
//

#import "AppDelegate.h"
#import "UserSetting.h"
#import "iRate.h"
#import "ServerManager.h"
#import "SharedOperation.h"
#import "ProductDetailsViewController.h"
#import "CategoryScreen.h"
#import "GAI.h"
#import <FirebaseAnalytics/FirebaseAnalytics.h>
#import <FirebaseMessaging/FirebaseMessaging.h>
#import <FirebaseInstanceID/FirebaseInstanceID.h>
#import <FirebaseDynamicLinks/FirebaseDynamicLinks.h>
#import <Crashlytics/Crashlytics.h>
#import <Fabric/Fabric.h>
#import "AGPushNoteView.h"

#import "ALUserDefaultsHandler.h"
#import "ALRegisterUserClientService.h"
#import "ALPushNotificationService.h"
#import "ALUtilityClass.h"
#import "ALDBHandler.h"
#import "ALMessagesViewController.h"
#import "ALPushAssist.h"
#import "ALMessageService.h"
#import <UserNotifications/UserNotifications.h>
#import "ALAppLocalNotifications.h"
#import "ALChatManager.h"

#if defined(__IPHONE_10_0) && __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_10_0
#import <UserNotifications/UserNotifications.h>
#endif


//
// Google Analytics configuration constants
//

// Property ID (provided by https://www.google.com/analytics/web/ ) used to initialize a tracker.
static NSString *const kCutePetsPropertyId = @"UA-40508787-3";

// Dispatch interval for automatic dispatching of hits to Google Analytics.
// Values 0.0 or less will disable periodic dispatching. The default dispatch interval is 120 secs.
static NSTimeInterval const kCutePetsDispatchInterval = 120.0;

// Set log level to have the Google Analytics SDK report debug information only in DEBUG mode.
#if DEBUG
static GAILogLevel const kCutePetsLogLevel = kGAILogLevelVerbose;
#else
static GAILogLevel const kCutePetsLogLevel = kGAILogLevelWarning;
#endif

//#import "TestFlight.h"
//

@interface AppDelegate ()<UNUserNotificationCenterDelegate, FIRMessagingDelegate>

@property(nonatomic, copy) void (^dispatchHandler)(GAIDispatchResult result);

- (void)initializeGoogleAnalytics;
- (void)sendHitsInBackground;

@end


@implementation AppDelegate
@synthesize isFromPushNotificationClicked;
bool isShowingLandscapeView = NO;

+(AppDelegate*) app
{
    return (AppDelegate*) [[UIApplication sharedApplication] delegate];
}

- (void)initializeGoogleAnalytics {
    // Automatically send uncaught exceptions to Google Analytics.
    [GAI sharedInstance].trackUncaughtExceptions = YES;
    
    // Set the dispatch interval for automatic dispatching.
    [GAI sharedInstance].dispatchInterval = kCutePetsDispatchInterval;
    
    // Set the appropriate log level for the default logger.
    [GAI sharedInstance].logger.logLevel = kCutePetsLogLevel;
    
    // Initialize a tracker using a Google Analytics property ID.
    [[GAI sharedInstance] trackerWithTrackingId:kCutePetsPropertyId];
}

// This method sends any queued hits when the app enters the background.
- (void)sendHitsInBackground {
    __block BOOL taskExpired = NO;
    
    __block UIBackgroundTaskIdentifier taskId =
    [[UIApplication sharedApplication] beginBackgroundTaskWithExpirationHandler:^{
        taskExpired = YES;
    }];
    
    if (taskId == UIBackgroundTaskInvalid) {
        return;
    }
    
    __weak AppDelegate *weakSelf = self;
    self.dispatchHandler = ^(GAIDispatchResult result) {
        // Dispatch hits until we have none left, we run into a dispatch error,
        // or the background task expires.
        if (result == kGAIDispatchGood && !taskExpired) {
            [[GAI sharedInstance] dispatchWithCompletionHandler:weakSelf.dispatchHandler];
        } else {
            [[UIApplication sharedApplication] endBackgroundTask:taskId];
        }
    };
    
    [[GAI sharedInstance] dispatchWithCompletionHandler:self.dispatchHandler];
}



- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    [[UINavigationBar appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor], NSForegroundColorAttributeName, nil]];

    
    self.dateFormatter = [[NSDateFormatter alloc]init];
    [self.dateFormatter setDateStyle:NSDateFormatterMediumStyle];
    [self.dateFormatter setTimeStyle:NSDateFormatterMediumStyle];
    
    [self initializeGoogleAnalytics];
    
    [UserSetting loadUserSetting];
    //com.MzadQatar.MzadQatar
    // self.window.tintColor = [UIColor blackColor];
    
    /*[[UIApplication sharedApplication] openURL:[NSURL URLWithString: @"MzadQatar://"]];
     if (![launchOptions objectForKey:UIApplicationLaunchOptionsURLKey]) {
     UIAlertView *alertView;
     alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"This app was launched without any text. Open this app using the Sender app to send text." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
     [alertView show];
     }*/
    [Fabric with:@[[Crashlytics class]]];
    [[Fabric sharedSDK] setDebug: YES];
    if ([[UIApplication sharedApplication]
         respondsToSelector:@selector(registerUserNotificationSettings:)]) {
        [[UIApplication sharedApplication]
         registerUserNotificationSettings:
         [UIUserNotificationSettings
          settingsForTypes:(UIUserNotificationTypeSound |
                            UIUserNotificationTypeAlert |
                            UIUserNotificationTypeBadge)
          categories:nil]];
        [[UIApplication sharedApplication] registerForRemoteNotifications];
        
        if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_9_x_Max) {
            
            UIUserNotificationType allNotificationTypes = (UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge);
            UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:allNotificationTypes categories:nil];
            [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
            
        } else {
            // iOS 10 or later
#if defined(__IPHONE_10_0) && __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_10_0
            
            UNAuthorizationOptions authOptions = UNAuthorizationOptionAlert | UNAuthorizationOptionSound | UNAuthorizationOptionBadge;
            [[UNUserNotificationCenter currentNotificationCenter]requestAuthorizationWithOptions:authOptions completionHandler:^(BOOL granted, NSError * _Nullable error) {
                NSLog(@"jayesh");
            }
             ];
            // For iOS 10 display notification (sent via APNS)
            [[UNUserNotificationCenter currentNotificationCenter] setDelegate:self];
            // For iOS 10 data message (sent via FCM)
            [[FIRMessaging messaging] setRemoteMessageDelegate:self];
#endif
        }
        [[UIApplication sharedApplication] registerForRemoteNotifications];
    } else {
        
    }
    [FIROptions defaultOptions].deepLinkURLScheme = @"com.MzadQatar.MzadQatar";
    //[FIROptions defaultOptions].deepLinkURLScheme = @"https://s9dzp.app.goo.gl/?link=http://mzadqatar.com/products/*&apn=com.MzadQatar.MzadQatar&amv=3&ad=1";
    [FIRApp configure];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(tokenRefreshNotification:)
                                                 name:kFIRInstanceIDTokenRefreshNotification object:nil];
    
    if ([UserSetting isUserRegistered] == true) {
        
        if ([ALUserDefaultsHandler isLoggedIn]){
            
            [ALPushNotificationService userSync];
        }else{
        }
        [UserSetting registerUserToApplozic];
    }
    
    [ALRegisterUserClientService isAppUpdated];
    
    ALAppLocalNotifications *localNotification = [ALAppLocalNotifications appLocalNotificationHandler];
    [localNotification dataConnectionNotificationHandler];
    
    if (launchOptions != nil)
    {
        NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] initWithDictionary:[launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey]];
//         NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] initWithDictionary:[[NSUserDefaults standardUserDefaults]valueForKey:@"TempPush"]];
        if (dictionary != nil)
        {
            if ([dictionary valueForKey:@"AL_KEY"] && [[dictionary valueForKey:@"AL_KEY"] isEqualToString:@"APPLOZIC_01"]) {
                
                [dictionary setValue:@"true" forKey:@"FromLocalNotification"];
                
                self.dictInfo = [[NSMutableDictionary alloc] initWithDictionary:dictionary];
            }
            
            [self application:application didReceiveRemoteNotification:dictionary];
        }
    }
    [[UIApplication sharedApplication]setStatusBarStyle:UIStatusBarStyleLightContent];
    return YES;
}

#pragma mark Applications Methods
- (void)application:(UIApplication *)application
didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    //   [TestFlight passCheckpoint:@"Success register for token"];
    NSString *refreshedToken = [[FIRInstanceID instanceID] token];
    NSString *deviceTokenformatted =
    [[[[deviceToken description] stringByReplacingOccurrencesOfString:@"<"
                                                           withString:@""]
      stringByReplacingOccurrencesOfString:@">"
      withString:@""]
     stringByReplacingOccurrencesOfString:@" "
     withString:@""];
    
    [[SharedOperation sharedInstance]
     checkTokenOrLanguageChange:deviceTokenformatted];
    
    const unsigned *tokenBytes = [deviceToken bytes];
    NSString *hexToken = [NSString stringWithFormat:@"%08x%08x%08x%08x%08x%08x%08x%08x",
                          ntohl(tokenBytes[0]), ntohl(tokenBytes[1]), ntohl(tokenBytes[2]),
                          ntohl(tokenBytes[3]), ntohl(tokenBytes[4]), ntohl(tokenBytes[5]),
                          ntohl(tokenBytes[6]), ntohl(tokenBytes[7])];
    
    NSString *apnDeviceToken = hexToken;
    NSLog(@"APN_DEVICE_TOKEN :: %@", hexToken);
    
    if ([[ALUserDefaultsHandler getApnDeviceToken] isEqualToString:apnDeviceToken])
    {
        return;
    }
    
    ALRegisterUserClientService *registerUserClientService = [[ALRegisterUserClientService alloc] init];
    [registerUserClientService updateApnDeviceTokenWithCompletion:apnDeviceToken withCompletion:^(ALRegistrationResponse *rResponse, NSError *error) {
        
        if (error)
        {
            NSLog(@"REGISTRATION ERROR :: %@",error.description);
            return;
        }
        
        NSLog(@"Registration response from server : %@", rResponse);
    }];
    
}
- (void)application:(UIApplication *)application
didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    //      [TestFlight passCheckpoint:[NSString stringWithFormat:@"failed to
    //      register with error %@",error]];
    
    if ([SharedData isAllowLog]) {
        NSLog(@"Failed to get token, error: %@", error);
    }
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    
    /*NSDictionary *apsInfo = [userInfo objectForKey:@"aps"];
     
     self.notificationProductId = [apsInfo objectForKey:@"productId"];
     self.categoryProductId = [apsInfo objectForKey:@"categoryId"];
     
     if(self.categoryProductId==nil)
     {
     self.categoryProductId=@"";
     }
     NSString *alertMsg = [apsInfo objectForKey:@"alert"];
     
     if (application.applicationState == UIApplicationStateActive) {
     [[NotificationViewController getInstance] refreshNowNotificationCount];
     } else {
     if(self.categoryProductId!=nil&& [self.categoryProductId isEqualToString:@""]==false)
     {
     [self showCategortProducts:self.categoryProductId];
     }
     else if (self.notificationProductId != nil&& [self.notificationProductId isEqualToString:@""]==false) {
     [self showProductDetailView:self.notificationProductId];
     } else {
     [self showPushMsg:alertMsg];
     }
     }*/
//    NSString *strSubcategoryId = @"";
//    NSString *strTabID = @"";
    
    self.categoryProductId=@"";
    self.notificationProductId=@"";
    self.strSubcategoryId = @"";
    self.strTabID = @"";
    
    BOOL isHassanNotificationMessage = false;
    
    NSDictionary *apsInfo = [userInfo objectForKey:@"aps"];
    
    if([apsInfo objectForKey:@"productId"]||[apsInfo objectForKey:@"categoryId"])
    {
        /* Hassan push notification message handling
         Hassan is the backend developer of Mzad
         */
        isHassanNotificationMessage = true;
        
        self.notificationProductId = [apsInfo objectForKey:@"productId"];
        self.categoryProductId = [apsInfo objectForKey:@"categoryId"];
        
        self.strSubcategoryId = [apsInfo objectForKey:@"subcategoryId"];
        self.strTabID = [apsInfo objectForKey:@"tabId"];
    }
    else if([userInfo objectForKey:@"productId"]||[userInfo objectForKey:@"categoryId"])
    {
        // Firebase push notification message handling
        isHassanNotificationMessage = false;
        self.notificationProductId = [userInfo objectForKey:@"productId"];
        self.categoryProductId = [userInfo objectForKey:@"categoryId"];
        
        self.strSubcategoryId = [userInfo objectForKey:@"subcategoryId"];
        self.strTabID = [userInfo objectForKey:@"tabId"];
        
    }else if([userInfo valueForKey:@"AL_KEY"]){
        
        NSLog(@"RECEIVED_NOTIFICATION :: %@", userInfo);
        ALPushNotificationService *pushNotificationService = [[ALPushNotificationService alloc] init];
        [pushNotificationService notificationArrivedToApplication:application withDictionary:userInfo];
    }
    
    //reset values if null
    if(self.categoryProductId==nil){
        self.categoryProductId=@"";
    }
    if(self.notificationProductId==nil){
        self.notificationProductId=@"";
    }
    if(self.strSubcategoryId==nil){
        self.strSubcategoryId=@"";
    }
    if(self.strTabID==nil){
        self.strTabID=@"0";
    }

    NSString *alertMsg = [apsInfo objectForKey:@"alert"];
    if (application.applicationState == UIApplicationStateActive) {
        if([userInfo valueForKey:@"AL_KEY"]){
            if ([[userInfo valueForKey:@"AL_KEY"] isEqualToString:@"APPLOZIC_01"]) {
                self.dictInfo = [[NSMutableDictionary alloc] initWithDictionary:userInfo];
                if ([userInfo valueForKey:@"FromLocalNotification"]) {
                    [self performSelector:@selector(showChatDetail) withObject:nil afterDelay:0.5];
                }else{
//                    [AGPushNoteView showWithNotificationMessage:alertMsg];
//                    [AGPushNoteView setMessageAction:^(NSString *message) {
//                        [self showChatDetail];
//                    }];
                }
            }
            
        }
//        else if(alertMsg != nil){
//            [AGPushNoteView showWithNotificationMessage:alertMsg];
//            [AGPushNoteView setMessageAction:^(NSString *message) {
//                //[self showChatDetail];
//            }];
//        }
        else{
            if(isHassanNotificationMessage){
                [[NotificationViewController getInstance] refreshNowNotificationCount];
            }else{
                [[NotificationViewController getInstance] displayNotificationMessageOfFirebase:alertMsg andProductAdvertiseId:self.notificationProductId andCategoryId:self.categoryProductId andSubCategoryId:self.strSubcategoryId andTabId:self.strTabID];
            }
        }
        
    } else {
        if(self.categoryProductId!=nil&& [self.categoryProductId isEqualToString:@""]==false)
        {
            //[self showCategortProducts:self.categoryProductId];
            if (application.applicationState == UIApplicationStateBackground){
                
                [self performSelector:@selector(showCategortProducts) withObject:nil afterDelay:0];
            }else{
                [self performSelector:@selector(showCategortProducts) withObject:nil afterDelay:4.0f];
            }
//            [self showCategortProducts:self.categoryProductId withSubcatgoryID:self.strSubcategoryId withTabID:self.strTabID];
        }
        else if (self.notificationProductId != nil&& [self.notificationProductId isEqualToString:@""]==false) {
            [self performSelector:@selector(showProductDetailView) withObject:nil afterDelay:0.1f];
        } else if([userInfo valueForKey:@"AL_KEY"]){
            
            if(application.applicationState == UIApplicationStateInactive){
                self.dictInfo = [[NSMutableDictionary alloc] initWithDictionary:userInfo];
                
                if ([[userInfo valueForKey:@"AL_KEY"] isEqualToString:@"APPLOZIC_01"]) {
                    
                    if ([userInfo valueForKey:@"FromLocalNotification"]) {
                        [self performSelector:@selector(showChatDetail) withObject:nil afterDelay:0.5];
                    }else{
                        [self showChatDetail];
                    }
                }
            }
        }else {
            //[self showPushMsg:alertMsg];
        }
    }
    //TODO: Fcm Code
    NSLog(@"Message ID: %@", userInfo[@"gcm.message_id"]);
    // Print full message.
    NSLog(@"%@", userInfo);
}

- (void)showPushMsg:(NSString *)msg {
    if (msg != nil) {
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle:NSLocalizedString(@"NOTIFICATION_ALERT", nil)
                              message:msg
                              delegate:nil
                              cancelButtonTitle:NSLocalizedString(@"DISMISS", nil)
                              otherButtonTitles:nil];
        [alert show];
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 1) {
        if (self.notificationProductId != nil) {
            [self showProductDetailView];
        }
    }
}

- (void)showProductDetailView {
    
    ProductDetailsViewController *controller = [[SharedData getCurrentStoryBoard]
                                                instantiateViewControllerWithIdentifier:@"ProductDetailsViewController"];
    [controller setProductId:self.notificationProductId];
    
    
    SWRevealViewController *swController =
    (SWRevealViewController *)self.window.rootViewController;
    UITabBarController *tbController =
    (UITabBarController *)swController.frontViewController;
    UINavigationController *nvController =
    (UINavigationController *)[tbController.viewControllers objectAtIndex:tbController.selectedIndex];
    [nvController pushViewController:controller animated:NO];
    
    
}

- (void)showCategortProducts {
    
    CategoryProducts *categoryProducts = [[SharedData getCurrentStoryBoard]
                                          instantiateViewControllerWithIdentifier:@"CategoryProducts"];

    [categoryProducts setCategoryProductsCategoryId:self.categoryProductId andSubcategoryId:self.strSubcategoryId andTabId:self.strTabID];

    
    SWRevealViewController *swController =
    (SWRevealViewController *)self.window.rootViewController;
    UITabBarController *tbController =
    (UITabBarController *)swController.frontViewController;
    UINavigationController *nvController =
    (UINavigationController *)[tbController.viewControllers objectAtIndex:tbController.selectedIndex];
    [nvController pushViewController:categoryProducts animated:NO];
}

-(void)showChatDetail{
    
    SWRevealViewController *swController = (SWRevealViewController *)self.window.rootViewController;
    UITabBarController *tbController = (UITabBarController *)swController.frontViewController;
    [tbController setSelectedIndex:4];
    UINavigationController *nvController = (UINavigationController *)[tbController.viewControllers objectAtIndex:tbController.selectedIndex];
    
    UIViewController *parentViewController = [[nvController viewControllers]lastObject];
    Class parentVCClass = [parentViewController class];
    
    NSString *className = NSStringFromClass(parentVCClass);
    if (![className isEqualToString:@"ALChatViewController"]){
        
        NSString *type = (NSString *)[self.dictInfo valueForKey:@"AL_KEY"];
        
        NSString *alValueJson = (NSString *)[self.dictInfo valueForKey:@"AL_VALUE"];
        NSData* data = [alValueJson dataUsingEncoding:NSUTF8StringEncoding];
        
        NSError *error = nil;
        NSDictionary *theMessageDict = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
        NSString *notificationId = (NSString *)[theMessageDict valueForKey:@"id"];
        
        if(notificationId && [ALUserDefaultsHandler isNotificationProcessd:notificationId])
        {
            NSLog(@"Returning from ALPUSH because notificationId is already processed... %@",notificationId);
            //BOOL isInactive = ([[UIApplication sharedApplication] applicationState] == UIApplicationStateInactive);
            if(/*isInactive && */([type isEqualToString:MT_SYNC] || [type isEqualToString:MT_MESSAGE_SENT]))
            {
                NSLog(@"ALAPNs : APP_IS_INACTIVE");
                
                ALChatViewController *detailChatViewController = [[SharedData getCurrentStoryBoard] instantiateViewControllerWithIdentifier:@"ALChatViewController"];
                
                detailChatViewController.contactIds = [theMessageDict valueForKey:@"message"];
                detailChatViewController.conversationId = nil;
                detailChatViewController.chatViewDelegate = self;
                
                NSArray *arrTemp = [[theMessageDict valueForKey:@"message"] componentsSeparatedByString:@":"];
                
                if (arrTemp.count > 1) {
                    detailChatViewController.channelKey = [NSNumber numberWithInteger:[arrTemp[1] integerValue]];
                }
                
                
                [nvController pushViewController:detailChatViewController animated:NO];
                
                /*ALChatViewController *detailChatViewController = [[SharedData getCurrentStoryBoard] instantiateViewControllerWithIdentifier:@"ALChatViewController"];
                 detailChatViewController.refresh = YES;
                 
                 NSArray *arrTemp = [[theMessageDict valueForKey:@"message"] componentsSeparatedByString:@":"];
                 
                 if (arrTemp.count > 0) {
                 detailChatViewController.contactIds = arrTemp[0];//;[NSString stringWithFormat:@"%@", [arrTemp objectAtIndex:0]];
                 }
                 if (arrTemp.count > 1) {
                 detailChatViewController.conversationId = [NSNumber numberWithInteger:[arrTemp[1] integerValue]];
                 }
                 
                 SWRevealViewController *swController = (SWRevealViewController *)self.window.rootViewController;
                 UITabBarController *tbController = (UITabBarController *)swController.frontViewController;
                 [tbController setSelectedIndex:4];
                 UINavigationController *nvController = (UINavigationController *)[tbController.viewControllers objectAtIndex:tbController.selectedIndex];
                 [nvController pushViewController:detailChatViewController animated:NO];*/
            }
            else
            {
                NSLog(@"ALAPNs : APP_IS_ACTIVE");
            }
        }
    }
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state.
    // This can occur for certain types of temporary interruptions (such as an
    // incoming phone call or SMS message) or when the user quits the application
    // and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down
    // OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate
    // timers, and store enough application state information to restore your
    // application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called
    // instead of applicationWillTerminate: when the user quits.
    [self sendHitsInBackground];
    
    // TODO: Fcm Code
    [[FIRMessaging messaging] disconnect];
    NSLog(@"Disconnected from FCM");
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"APP_ENTER_IN_BACKGROUND" object:nil];
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state;
    // here you can undo many of the changes made on entering the background.
    
    // Restore the dispatch interval since dispatchWithCompletionHandler:
    // disables automatic dispatching.
    [GAI sharedInstance].dispatchInterval = kCutePetsDispatchInterval;
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"APP_ENTER_IN_FOREGROUND" object:nil];
    
    if ([UserSetting isUserRegistered] == true) {
        
        if ([ALUserDefaultsHandler isLoggedIn]){
            
            [ALPushNotificationService userSync];
            
            [ALMessageService getLatestMessageForUser:[ALUserDefaultsHandler getDeviceKeyString] withCompletion:^(NSMutableArray  * messageList, NSError *error) {
                
                if(error)
                {
                    NSLog(@"ERROR: IN REFRESH MSG VC :: %@",error);
                    return;
                }
                NSLog(@"REFRESH MSG VC");
            }];
        }else{
            
            [UserSetting registerUserToApplozic];
        }
    }
}
#pragma mark - OpenUrl Handling Mehthods
- (BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<NSString *, id> *)options {
    
    return [self application:app openURL:url sourceApplication:nil annotation:@{}];
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    
    FIRDynamicLink *dynamicLink = [[FIRDynamicLinks dynamicLinks] dynamicLinkFromCustomSchemeURL:url];
    if (dynamicLink) {
        // Handle the deep link. For example, show the deep-linked content or
        // apply a promotional offer to the user's account.
        // ...
        return YES;
    }
    return NO;
}
- (BOOL)application:(UIApplication *)application continueUserActivity:(NSUserActivity *)userActivity restorationHandler:(void (^)(NSArray *))restorationHandler {
    
    BOOL handled = [[FIRDynamicLinks dynamicLinks] handleUniversalLink:userActivity.webpageURL completion:^(FIRDynamicLink * _Nullable dynamicLink,NSError * _Nullable error) {
        // ...
        NSLog(@"%@",dynamicLink.url);
        NSArray *array = [[NSString stringWithFormat:@"%@",dynamicLink.url] componentsSeparatedByString:@"/"];
        if([array containsObject:@"products"]){
            NSLog(@"%@",array.lastObject);
            if([array.lastObject isEqualToString:@""]||array.lastObject == nil){
                return;
            }
            self.notificationProductId = array.lastObject;
            [self showProductDetailView];
        }
    }];
    return handled;
}
- (void)applicationDidBecomeActive:(UIApplication *)application {
    
    //    NSUserDefaults * userDefaults = [NSUserDefaults standardUserDefaults];
    //    NSInteger appLaunchAmounts = [userDefaults
    //    integerForKey:@"LaunchAmounts"];
    //    if (appLaunchAmounts == 5)
    //    {
    //        NSLog(@"",appLaunchAmounts);
    //    }
    //    [userDefaults setInteger:appLaunchAmounts+1 forKey:@"LaunchAmounts"];
    
    //    int d;
    //    if(d!=nil)
    //        d=0;
    //
    //    [SharedData setAppDidBecomeActiveCountMethod:d];
    // int t=data.appDidBecomeActiveCountMethod;
    
    // Restart any tasks that were paused (or not yet started) while the
    // application was inactive. If the application was previously in the
    // background, optionally refresh the user interface.
    
    // TODO: Fcm Code
    [self connectToFcm];
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if
    // appropriate. See also applicationDidEnterBackground:.
    
    [[ALDBHandler sharedInstance] saveContext];
}

+ (void)initialize {
    //#TODO: change in deployment
    // set the bundle ID. normally you wouldn't need to do this
    // as it is picked up automatically from your Info.plist file
    // but we want to test with an app that's actually on the store
    //[iRate sharedInstance].applicationBundleID =
    //@"com.charcoaldesign.rainbowblocks-free";
    //[iRate sharedInstance].onlyPromptIfLatestVersion = NO;
    
    // enable preview mode
    
    // Testing. To show everytime.
    [iRate sharedInstance].previewMode = NO;
    
    [iRate sharedInstance].appStoreID = 680268461;
    [iRate sharedInstance].appStoreCountry = @"QA";
    // [iRate sharedInstance].applicationBundleID = @"com.MzadQatar.MzadQatar";
}

//- (void)initializeNotificationController {
//  NotificationViewController *controller =
//      [NotificationViewController getInstance];
//
//  [controller refreshNowNotificationCount];
//}
#pragma mark - Add24Hours To CurrentDate
-(void)Add24HoursToDate:(NSDate *)date withKey:(NSString *)strKey withCurrentDateKey:(NSString *)strCurrentKey{
    
    [[NSUserDefaults standardUserDefaults]setObject:date forKey:strCurrentKey];
    
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *components = [[NSDateComponents alloc] init];
    [components setHour:24];
    
    NSDate *newDate= [calendar dateByAddingComponents:components toDate:date options:0];
    [[NSUserDefaults standardUserDefaults]setObject:newDate forKey:strKey];
}

#pragma mark FCM Methods
- (void)tokenRefreshNotification:(NSNotification *)notification {
    // Note that this callback will be fired everytime a new token is generated, including the first
    // time. So if you need to retrieve the token as soon as it is available this is where that
    // should be done.
    NSString *refreshedToken = [[FIRInstanceID instanceID] token];
    NSLog(@"InstanceID token: %@", refreshedToken);
    // Connect to FCM since connection may have failed when attempted before having a token.
    [self connectToFcm];
    // TODO: If necessary send token to application server.
}
- (void)connectToFcm {
    [[FIRMessaging messaging] connectWithCompletion:^(NSError * _Nullable error) {
        if (error != nil) {
            NSLog(@"Unable to connect to FCM. %@", error);
        } else {
            NSLog(@"Connected to FCM.");
        }
    }];
}
// Receive displayed notifications for iOS 10 devices.
#if defined(__IPHONE_10_0) && __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_10_0
- (void)userNotificationCenter:(UNUserNotificationCenter *)center willPresentNotification:(UNNotification *)notification
         withCompletionHandler:(void (^)(UNNotificationPresentationOptions))completionHandler {
    // Print message ID.
    NSDictionary *userInfo = notification.request.content.userInfo;
    NSLog(@"Message ID: %@", userInfo[@"gcm.message_id"]);
    // Print full message.
    NSLog(@"%@", userInfo);
    
    [self application:nil didReceiveRemoteNotification:userInfo];
}

// Receive data message on iOS 10 devices.
- (void)applicationReceivedRemoteMessage:(FIRMessagingRemoteMessage *)remoteMessage {
    // Print full message
    NSLog(@"%@", [remoteMessage appData]);
}
#endif



@end
