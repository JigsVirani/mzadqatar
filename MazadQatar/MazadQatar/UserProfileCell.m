//
//  UserProfileCell.m
//  Mzad Qatar
//
//  Created by Mohammed AbdulFattah on 1/26/15.
//  Copyright (c) 2015 Mohammed abdulfattah attia. All rights reserved.
//

#import "UserProfileCell.h"
#import <SDWebImage/UIImageView+WebCache.h>

@implementation UserProfileCell

- (id)initWithStyle:(UITableViewCellStyle)style
    reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

- (IBAction)moreAdsClicked:(id)sender {
}

- (void)setUserProfileInfoUi:(ProductObject *)productObjectParam {
    productObject = productObjectParam;
    
    [self setUiData];
}

- (void)setUiData {
    // user info
    
    NSString *imageName =
    [[productObject productId] stringByAppendingString:@".png"];
    
    self.userImageView.layer.cornerRadius = 5.0;
    self.userImageView.clipsToBounds = YES;
    [self.userImageView
     sd_setImageWithURL:[NSURL URLWithString:productObject.productUserImageUrl]
     placeholderImage:[UIImage imageNamed:imageName]
     completed:nil];
    
    [self.userName setText:productObject.productUserName];
    
    [self.userNumberOfAds setText:productObject.productUserNumberOfAds];
    
    NSString *countryCodePlusNumber =
    [NSString stringWithFormat:@"%@%@", productObject.productCountryCode,
     productObject.productUserNumber];
    [self.userNumber setText:countryCodePlusNumber];
    
    [self.userDescription setText:productObject.productUserShortDescription];
}

@end
