//
//  CameraViewViewController.h
//  MazadQatar
//
//  Created by samo on 3/24/13.
//  Copyright (c) 2013 Mohammed abdulfattah attia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CameraViewDelegate.h"

//@interface CameraView : UIViewController <UIImagePickerControllerDelegate,
//UINavigationControllerDelegate>
@interface CameraView : NSObject <UIImagePickerControllerDelegate,
                                  UINavigationControllerDelegate> {
  UIViewController *parentView;
  UIImagePickerController *imagePicker;
  UIPopoverController *popover;
}
@property(nonatomic, weak) id<CameraViewDelegate> imageCaptureDelegate;

- (void)startCameraCapturing:(UIViewController *)parentViewParam;
@end
