//
//  SuccessPopView.m
//  Mzad Qatar
//
//  Created by ashish agrawal on 04/12/16.
//  Copyright © 2016 Mohammed abdulfattah attia. All rights reserved.
//

#import "SuccessPopView.h"

@implementation SuccessPopView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self = [[[NSBundle mainBundle] loadNibNamed:@"SuccessPopView" owner:self options:nil] objectAtIndex:0];
        CGRect frame1 = frame;
        frame1.origin.y = 64;
        frame1.size.height = frame1.size.height - 64;
        self.frame = frame1;
        [self sentIntialValue];
        
    }
    return self;
}

-(void)sentIntialValue {

//    NSString *language = [[NSLocale preferredLanguages] objectAtIndex:0];
//    NSDictionary *languageDic = [NSLocale componentsFromLocaleIdentifier:language];
//    NSString *languageCode = [languageDic objectForKey:@"kCFLocaleLanguageCodeKey"];
//
//    if ([languageCode isEqual: @"en"]) {
//        
//    } else {
//        
//    }
//    
//    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad) {
//        
//    }

    [self.lblDetail1
     setText:NSLocalizedString(@"COMPANYSUCCESS_DETAIL1", nil)];
    [self.lblDetail2
     setText:NSLocalizedString(@"COMPANYSUCCESS_DETAIL2", nil)];
    [self.lblRegister
     setText:NSLocalizedString(@"COMPANYSUCCESS_REGISTER_AS_COMPANY", nil)];
    [self.btnRegister
     setTitle:NSLocalizedString(@"DONE", nil) forState:UIControlStateNormal];
}


- (IBAction)btnClickDone:(id)sender {
    [self removeFromSuperview];
    
    if ([_delegate respondsToSelector:(@selector(dneClicked))]) {
        [_delegate dneClicked];
    }
}


@end
