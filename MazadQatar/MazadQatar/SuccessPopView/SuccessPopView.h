//
//  SuccessPopView.h
//  Mzad Qatar
//
//  Created by ashish agrawal on 04/12/16.
//  Copyright © 2016 Mohammed abdulfattah attia. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SuccessPopViewDelegate <NSObject>

-(void)dneClicked;

@end

@interface SuccessPopView : UIView

@property (weak, nonatomic) IBOutlet UILabel *lblRegister;
@property (weak, nonatomic) IBOutlet UILabel *lblDetail1;
@property (weak, nonatomic) IBOutlet UILabel *lblDetail2;
@property (weak, nonatomic) IBOutlet UIButton *btnRegister;

@property (weak, nonatomic)  id<SuccessPopViewDelegate> delegate;

@end
