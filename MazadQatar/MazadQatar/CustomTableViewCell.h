//
//  CustomTableViewCell.h
//  Mzad Qatar
//
//  Created by iOS Developer on 23/09/2014.
//  Copyright (c) 2014 Mohammed abdulfattah attia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GoogleMobileAds/GoogleMobileAds.h>

@interface CustomTableViewCell : UITableViewCell
@property(nonatomic, strong) IBOutlet UIImageView *customImageView;
@property(weak, nonatomic)  IBOutlet DFPBannerView  *bannerView;
@property(strong, nonatomic) IBOutlet UILabel *title;
@property(strong, nonatomic) IBOutlet UILabel *desc;
@property(strong, nonatomic) IBOutlet UILabel *qr;

@property(strong, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property(strong, nonatomic) IBOutlet UIButton *comments;
@property(strong, nonatomic) IBOutlet UIButton *likes;
@property(strong, nonatomic) IBOutlet UIView *isCompanyView;
@property(strong, nonatomic) IBOutlet NSLayoutConstraint *constViewCompanyWidth;

@end
