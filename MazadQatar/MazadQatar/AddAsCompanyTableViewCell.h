//
//  AddAsCompanyTableViewCell.h
//  Mzad Qatar
//
//  Created by MacBook Pro on 6/22/16.
//  Copyright © 2016 Mohammed abdulfattah attia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddAsCompanyTableViewCell : UITableViewCell
@property(strong, nonatomic) IBOutlet UIButton *btnAddDetail;
@property(strong, nonatomic) IBOutlet UIButton *btnArrow;
@end
