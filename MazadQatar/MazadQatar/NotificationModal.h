//
//  NotificationModal.h
//  Mzad Qatar
//
//  Created by Perveen Akhtar on 04/07/2014.
//  Copyright (c) 2014 Mohammed abdulfattah attia. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NotificationModal : NSObject

@property(strong) NSString *lastUpdateTime;
@property(strong) NSString *notificationInfo;
@property(strong) NSString *notificationTime;
@property(strong) NSString *isRead;
@property(strong) NSString *numberPerPage;
@property(strong) NSString *productId;
@property(strong) NSString *notificationImage;
@property(strong) NSString *notificationCategoryId;
@property(strong) NSString *notificationId;
@property(assign) bool isStoppingNotification;
@property(strong) NSString *stoppingNotificationResult;

@end
