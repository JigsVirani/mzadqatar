//
//  SocialShareUtility.m
//  Mzad Qatar
//
//  Created by Mohammed AbdulFattah on 11/12/14.
//  Copyright (c) 2014 Mohammed abdulfattah attia. All rights reserved.
//

#import "SocialShareUtility.h"
#import "WhatsAppKit.h"
#import "DialogManager.h"
#import "MWCommon.h"

@implementation SocialShareUtility



- (void)shareOnFacebookInView:(UIViewController *)parentView
                     withText:(NSString *)shareText
                    withImage:(UIImage *)shareImage
                      withUrl:(NSString *)shareUrl {

  if ([SLComposeViewController
          isAvailableForServiceType:SLServiceTypeFacebook]) {
    SLComposeViewController *controller = [SLComposeViewController
        composeViewControllerForServiceType:SLServiceTypeFacebook];

    [controller setInitialText:shareText];
    [controller addURL:[NSURL URLWithString:shareUrl]];
    [controller addImage:shareImage];

      [parentView presentViewController:controller animated:YES completion:^{
          //controller.view.tintColor = [UIColor whiteColor];
      }];
  }
  else
  {
      [DialogManager showDialogWithMessage:NSLocalizedString(@"FACEBOOK_INSTALLED", nil)];
  }
}

- (void)shareOnTwitterInView:(UIViewController *)parentView
                    withText:(NSString *)shareText
                   withImage:(UIImage *)shareImage
                     withUrl:(NSString *)shareUrl {
  if ([SLComposeViewController
          isAvailableForServiceType:SLServiceTypeTwitter]) {
    SLComposeViewController *controller = [SLComposeViewController
        composeViewControllerForServiceType:SLServiceTypeTwitter];
    [controller setInitialText:shareText];
    [controller addURL:[NSURL URLWithString:shareUrl]];
    [controller addImage:shareImage];
    [parentView presentViewController:controller animated:YES completion:nil];
  }
  else
  {
      [DialogManager showDialogWithMessage:NSLocalizedString(@"TWITTER_NOT_INSTALLED", nil)];
  }
}

- (void)shareOnWhatsAppInView:(UIViewController *)parentView
                     withText:(NSString *)shareText {
  if ([WhatsAppKit isWhatsAppInstalled]) {
    [WhatsAppKit launchWhatsAppWithMessage:shareText];
  }
  else
  {
      [DialogManager showDialogWithMessage:NSLocalizedString(@"WHATS_APP_NOT_INSTALLED", nil)];
  }
}

- (void)shareOnInstagramInView:(UIViewController *)parentView
                     withImage:(UIImage *)shareImage {
  NSString *savePath = [NSHomeDirectory()
      stringByAppendingPathComponent:@"Documents/MzadImage.igo"];

  [UIImagePNGRepresentation(shareImage) writeToFile:savePath atomically:YES];

  NSURL *instagramURL = [NSURL URLWithString:@"instagram://app"];

  if ([[UIApplication sharedApplication] canOpenURL:instagramURL])

  {

    self.documentInteractionController = [UIDocumentInteractionController
        interactionControllerWithURL:
            [NSURL fileURLWithPath:savePath]];

    self.documentInteractionController.UTI = @"com.instagram.exclusivegram";
      UIView *view=[parentView view];
      [view setTintColor:[UIColor whiteColor]];
    [self.documentInteractionController
        presentOpenInMenuFromRect:CGRectZero
                           inView:view
                         animated:YES];
  }
  else
  {
      [DialogManager showDialogWithMessage:NSLocalizedString(@"INSTAGRAM_NOT_INSTALLED", nil)];
  }
}

- (void)shareOnOtherAppsInView:(UIViewController *)parentView
            andFrameToSHowFrom:(CGRect)frame
                      withText:(NSString *)shareText
                     withImage:(UIImage *)shareImage
                       withUrl:(NSString *)shareUrl {
  NSArray *activityItems =
      [NSArray arrayWithObjects: shareText, shareImage,[NSURL URLWithString:shareUrl], nil];
  UIActivityViewController *avc =
      [[UIActivityViewController alloc] initWithActivityItems:activityItems
                                        applicationActivities:nil];
   
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {

      [avc.view setTintColor:[UIColor grayColor]];
    }
    
  // for ipad we should show popover
  if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
    [parentView presentViewController:avc animated:YES completion:nil];
  } else {
    // Change Rect to position Popover

    UIPopoverController *popup = [[UIPopoverController alloc]
        initWithContentViewController:avc];

    [popup presentPopoverFromRect:frame
                           inView:parentView.view
         permittedArrowDirections:UIPopoverArrowDirectionAny
                         animated:YES];
  }

}

@end
