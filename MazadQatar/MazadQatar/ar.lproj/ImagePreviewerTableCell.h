//
//  ImagePreviewerTableCell.h
//  Mzad Qatar
//
//  Created by Paresh Kacha on 02/11/15.
//  Copyright © 2015 Mohammed abdulfattah attia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProductObject.h"
#import "ServerManager.h"
#import "MWPhotoBrowser.h"
#import "CameraView.h"
#import "CameraViewDelegate.h"
#import "MWPhotoBrowser.h"
#import "ImageCollectionFooterCell.h"
#import "ImageCollectionViewCell.h"
#import <QuartzCore/QuartzCore.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import "DisplayUtility.h"
#import "SharedGraphicInfo.h"
#import "DialogManager.h"
#import "AppDelegate.h"
#import "GridView.h"
#import "SelectImageDialogueViewController.h"

// Delgate
@class ImagePreviewerTableCell;
@protocol ImagePreviewerDelegate <NSObject>
@optional
- (void)imageAddedToProductObject:(UIImage *)newImage
                  isEditAdvertise:(BOOL)isEditAdvertise;
@optional
@end

@interface ImagePreviewerTableCell
: UITableViewCell <MWPhotoBrowserDelegate, UICollectionViewDataSource,
UICollectionViewDelegate,
UICollectionViewDelegateFlowLayout,
UIImagePickerControllerDelegate, CameraViewDelegate,
UINavigationControllerDelegate, SelectImageDelegate> {
    bool isAddCell;
    bool isEditAdvertise;
    ProductObject *productObject;
    UIViewController *parentView;
    CGSize footerSize;
    id<ImagePreviewerDelegate> imageViewerDelegate;
    CameraView *cameraView;
}
@property(strong, nonatomic) FDTakeController *takeController;
//@property(strong, nonatomic) IBOutlet UIButton *btnCommentsCount;
//@property(strong, nonatomic) IBOutlet UIButton *btnLikeCount;
@property(strong, nonatomic) IBOutlet UICollectionView *collectionView;
//@property(strong, nonatomic) IBOutlet UIButton *imageInitialAddImage;
//@property(strong, nonatomic) IBOutlet UILabel *labelInitialAddImage;
//@property(strong, nonatomic) IBOutlet UITextView *labelAtLeastImages;
//@property(strong, nonatomic) IBOutlet UIButton *smallAddImage;
//@property(strong, nonatomic) IBOutlet UILabel *smallAddImageText;
@property(strong, nonatomic) IBOutlet UIPageControl *pageControlCount;

//@property(strong, nonatomic)
//IBOutlet UIActivityIndicatorView *loadingProductDetailsIndicator;

//@property(strong, nonatomic) IBOutlet UIButton *addImageInEditBtn;
//@property(strong, nonatomic) IBOutlet UIButton *setAsDefaultInEditBtn;
//@property(strong, nonatomic) IBOutlet UIButton *deleteInEditBtn;
//@property(strong, nonatomic) IBOutlet UIImageView *addImageInEditIcon;
//@property(strong, nonatomic) IBOutlet UIImageView *setAsDefaultInEditIcon;
//@property(strong, nonatomic) IBOutlet UIImageView *deleteInEditIcon;
//@property(strong, nonatomic) IBOutlet UITextField *editButtonsResultTextField;
//- (IBAction)addImageButtonClicked:(id)sender;
- (void)loadCell:(UIViewController *)parentViewParam
       isAddCell:(bool)isAddCellParam
 isEditAdvertise:(BOOL)isEditAdvertiseParam
withProductObject:(ProductObject *)productObjectParam
isloadingProductDetail:(BOOL)isLoading
withImageViewerDelegate:(id<ImagePreviewerDelegate>)imageViewerDelegateParam;

- (void)hideAddProductView:(BOOL)isHide;
- (void)hideAddImage:(bool)isHide;
- (void)hideEditAdButtons:(bool)isHide;
- (void)hideCommentLikeCount:(bool)isHide;
- (void)showSelectImageDialog:(UIButton *)sender;
@end
