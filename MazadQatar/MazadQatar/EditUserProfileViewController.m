//
//  UserProfileViewController.m
//  Mzad Qatar
//
//  Created by Mohammed AbdulFattah on 1/28/15.
//  Copyright (c) 2015 Mohammed abdulfattah attia. All rights reserved.
//

#import "EditUserProfileViewController.h"
#import "EditUserProfileCell.h"
#import "LocationVC.h"

@interface EditUserProfileViewController ()<SelectLatLong,EditUserProfileDelegate>

@end

@implementation EditUserProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.screenName = @"Edit User Profile Screen";
    
    //[self.tableView setBackgroundView:[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"bg.png"]] ];

    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    [DialogManager showPressEditInfoDialog];
}

-(void)viewDidAppear:(BOOL)animated
{
    
    //[self.view endEditing:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    EditUserProfileCell *cell = nil;
    // comments of product
    if (indexPath.row == 0) {
        cell = [tableView dequeueReusableCellWithIdentifier:@"EditProfileIndentifier" forIndexPath:indexPath];
    }
    
    [cell setProductInfoCell:self.productObject andParentView:self];
    cell.delegate = self;
    cell.backgroundColor = [UIColor clearColor];
    
    return cell;
}

- (IBAction)updateButtonClicked:(id)sender {
}
#pragma mark -----------------------
#pragma mark  Latitude & Longitude Methods
#pragma mark -----------------------

-(void)tappedLocationBtn{
    [self performSegueWithIdentifier:@"LocationVC" sender:nil];
}
- (void)selectedLatitude:(NSString *)strLatitude withLongitude:(NSString *)strLongitude withAddress:(NSString *)strAddress{
    
    EditUserProfileCell *theCell = (id)[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    
    self.productObject.productUserlang = strLatitude;
    self.productObject.productUserlong = strLongitude;
    self.productObject.productUserAddress = strAddress;
    [theCell.btnLocation setTitle:strAddress forState:UIControlStateNormal];
    [self.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObjects:[NSIndexPath indexPathForRow:0 inSection:0], nil] withRowAnimation:UITableViewRowAnimationNone];
}
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if([segue.identifier isEqualToString:@"LocationVC"]){
        LocationVC *obj = [segue destinationViewController];
        obj.delegate = self;
    }
}
@end
