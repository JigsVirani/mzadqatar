
#import "LocationVC.h"
#import "UserSetting.h"
//#import "MyAnnotation.h"
@interface LocationVC ()

@end

@implementation LocationVC
#pragma mark -----------------------
#pragma mark  LifeCycle Methods
#pragma mark -----------------------

- (void)viewDidLoad
{    
    [loader stopAnimating];
    loader.hidden = YES;
    [_btnLocation setTitle:NSLocalizedString(@"MAP_SELECT_LOCATION", nil) forState:UIControlStateNormal];
    [_btnRecentLocation setTitle:NSLocalizedString(@"MAP_RECENTLOCATION", nil) forState:UIControlStateNormal];

    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.distanceFilter = kCLDistanceFilterNone; //whenever we move
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    
    [locationManager startUpdatingLocation];
    [locationManager requestWhenInUseAuthorization]; // Add This Line
    
    [_mapLocation setShowsUserLocation:NO];
    [_mapLocation setScrollEnabled:YES];
    
    _mapLocation.delegate = self;
    CLLocationCoordinate2D location = CLLocationCoordinate2DMake(locationManager.location.coordinate.latitude, locationManager.location.coordinate.longitude);
    MKCoordinateRegion region;
    selected_latitude = locationManager.location.coordinate.latitude;
    selected_longitude = locationManager.location.coordinate.longitude;
    current_lat = locationManager.location.coordinate.latitude;
    current_long = locationManager.location.coordinate.longitude;

    region = MKCoordinateRegionMake(location, MKCoordinateSpanMake(0.1, 0.1));
    MKCoordinateRegion adjustedRegion = [_mapLocation regionThatFits:region];
    [_mapLocation setRegion:adjustedRegion animated:YES];
    
    pinCustom = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"pin.png"]];
    pinCustom.center = self.view.center;
    [self.view addSubview:pinCustom];
    [_mapLocation setCenterCoordinate:locationManager.location.coordinate animated:YES];
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated{
    _mapLocation.delegate = self;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark -----------------------
#pragma mark  MapView Delegate Methods
#pragma mark -----------------------

-(void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation {
    [_mapLocation setCenterCoordinate:userLocation.coordinate animated:YES];
    selected_latitude = locationManager.location.coordinate.latitude;
    selected_longitude = locationManager.location.coordinate.longitude;
    current_lat = locationManager.location.coordinate.latitude;
    current_long = locationManager.location.coordinate.longitude;
    [self getAddressFromLatLon:selected_latitude withLongitude:selected_longitude andCompletionHandler:^(BOOL result) {
        if(result){
            
        }
    }];
}
- (void)mapView:(MKMapView *)mapView
 annotationView:(MKAnnotationView *)annotationView
didChangeDragState:(MKAnnotationViewDragState)newState
   fromOldState:(MKAnnotationViewDragState)oldState
{
    if (newState == MKAnnotationViewDragStateEnding)
    {
        CLLocationCoordinate2D droppedAt = annotationView.annotation.coordinate;
        NSLog(@"Pin dropped at %f,%f", droppedAt.latitude, droppedAt.longitude);
        selected_latitude = droppedAt.latitude;
        selected_longitude = droppedAt.longitude;
        [self getAddressFromLatLon:selected_latitude withLongitude:selected_longitude andCompletionHandler:^(BOOL result) {
            if(result){
                
            }
        }];
    }
}
- (void)mapView:(MKMapView *)mapView regionWillChangeAnimated:(BOOL)animated{

}
- (void)mapView:(MKMapView *)mapView regionDidChangeAnimated:(BOOL)animated{
    NSLog(@"%f %f",mapView.centerCoordinate.latitude,mapView.centerCoordinate.longitude);
    selected_latitude = mapView.centerCoordinate.latitude;
    selected_longitude = mapView.centerCoordinate.longitude;
    [self getAddressFromLatLon:selected_latitude withLongitude:selected_longitude andCompletionHandler:^(BOOL result) {
        if(result){
            
        }
    }];
}
- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation{
    CLLocation *currentLocation = newLocation;
    if (currentLocation != nil) {
    }
}
-(void)selectedLatitude:(NSString *)strLat andLongitude:(NSString *)strLong andAddress:(NSString *)strAdd{
    selected_latitude = [strLat floatValue];
    selected_longitude =[strLong floatValue];
    [self.delegate selectedLatitude:strLat withLongitude:strLong withAddress:strAdd];
    [self.navigationController popViewControllerAnimated:YES];

//    CLLocationCoordinate2D droppedAt = CLLocationCoordinate2DMake(selected_latitude, selected_longitude);
//    MKCoordinateRegion region;
//    region = MKCoordinateRegionMake(droppedAt, MKCoordinateSpanMake(0.1, 0.1));
//    MKCoordinateRegion adjustedRegion = [_mapLocation regionThatFits:region];
//    [_mapLocation setRegion:adjustedRegion animated:YES];
//    _mapLocation.showsUserLocation = NO;
//    [_mapLocation removeAnnotations:_mapLocation.annotations];
//    [_mapLocation addAnnotation:[[MyAnnotation alloc] initWithCoordinate:droppedAt]];
//    strAddress = strAdd;
}
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
/*- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"go_recentlocation"]) {
        RecentLocationVC *object = [segue destinationViewController];
        object.delegate = self;
    }
}*/

#pragma mark -----------------------
#pragma mark  Action Methods
#pragma mark -----------------------

- (IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)selectLocation:(id)sender {
    loader.hidden = NO;
    [loader startAnimating];
    if(strAddress.length != 0){
        NSString *strLat = [NSString stringWithFormat:@"%f",selected_latitude];
        NSString *strLon = [NSString stringWithFormat:@"%f",selected_longitude];
        [self.delegate selectedLatitude:strLat withLongitude:strLon withAddress:strAddress];
        [self.navigationController popViewControllerAnimated:YES];
    }
    else{
        [self getAddressFromLatLon:selected_latitude withLongitude:selected_longitude andCompletionHandler:^(BOOL result) {
            if(result){
                [self.navigationController popViewControllerAnimated:YES];
            }
        }];
    }
}

-(void)getAddressFromLatLon:(double)pdblLatitude withLongitude:(double)pdblLongitude andCompletionHandler:(void (^)(BOOL result))completionHandler
{
    CLGeocoder *ceo = [[CLGeocoder alloc]init];
    CLLocation *loc = [[CLLocation alloc]initWithLatitude:pdblLatitude longitude:pdblLongitude]; //insert your coordinates
    
    [ceo reverseGeocodeLocation:loc
              completionHandler:^(NSArray *placemarks, NSError *error) {
                  CLPlacemark *placemark = [placemarks objectAtIndex:0];
                  if (placemark) {
                      [loader stopAnimating];
                      loader.hidden = YES;
                      NSLog(@"placemark %@",placemark);
                      //String to hold address
                      strAddress = [[placemark.addressDictionary valueForKey:@"FormattedAddressLines"] componentsJoinedByString:@", "];
                      _lblCurrentLocation.text = strAddress;
                      
                      NSLog(@"addressDictionary %@", placemark.addressDictionary);
                      NSString *strLat = [NSString stringWithFormat:@"%f",selected_latitude];
                      NSString *strLon = [NSString stringWithFormat:@"%f",selected_longitude];
                      [self.delegate selectedLatitude:strLat withLongitude:strLon withAddress:strAddress];
                      completionHandler(YES);
                  }
                  else {
                      [loader stopAnimating];
                      loader.hidden = YES;
                  }
              }
     ];
}

- (IBAction)getCurrentLocation:(id)sender {
    
    selected_latitude = current_lat;
    selected_longitude = current_long;

    CLLocationCoordinate2D startCoord = CLLocationCoordinate2DMake(current_lat, current_long);
    MKCoordinateRegion adjustedRegion = [_mapLocation regionThatFits:MKCoordinateRegionMakeWithDistance(startCoord, 3000, 3000)];
    _mapLocation.showsUserLocation = YES;
    [_mapLocation setRegion:adjustedRegion animated:YES];
}
@end
