//
//  TextFieldViewController.h
//  MazadQatar
//
//  Created by samo on 4/2/13.
//  Copyright (c) 2013 Mohammed abdulfattah attia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GAITrackedViewController.h"
@interface TextFieldViewController

    : GAITrackedViewController <UITextViewDelegate, UITextFieldDelegate> {
  CGFloat animatedDistance;
}

@property(assign) int movementDistance;

@property(nonatomic, strong) UIView *currentController;
@property(nonatomic)         UIEdgeInsets                 currentContentInset;                   
@property(nonatomic, strong) UIView *inputAccView;
@property(nonatomic, strong) UIButton *btnDone;
@property(readwrite, nonatomic) BOOL isCommnetController;


@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;

- (void)createKeyboardAccessoryView;
- (void)createKeyboardAccessoryViewInChild;
- (void)doneTyping;
- (void)createInputAccessoryView ;
- (void)registerForKeyboardNotifications;

@end
