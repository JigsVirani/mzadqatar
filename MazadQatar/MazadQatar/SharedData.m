//
//  SharedData.m
//  MazadQatar
//
//  Created by samo on 4/14/13.
//  Copyright (c) 2013 Mohammed abdulfattah attia. All rights reserved.
//

#import "SharedData.h"
#import "UserSetting.h"
#import "ServerManager.h"
#import "NotificationViewController.h"
#include <ifaddrs.h>
#include <arpa/inet.h>
#import "ProductDetailsViewController.h"

// static NSString *productCommentlastUpdateTime =@"0";
static NSString *USER_IP_ADDRESS = @"userIpAddress";
static NSString *USER_DEVICE_TYPE = @"userDeviceType";
static NSString *USER_DEVICE_MODEL = @"userDeviceModel";
static NSString *USER_COUNTRY_CODE = @"userCountryCode";
static NSString *USER_REGISTERED_NUMBER = @"userRegisteredNumber";
static NSString *USER_DEVICE_LANGUAGE = @"userDeviceLanguage";
static NSString *USER_MAC_ADDRESS = @"userMacAddress";
static NSString *USER_VERSION_NUMBER = @"versionNumber";

static NSString *userProductslastUpdateTime = @"0";
static NSString *userFavoritelastUpdateTime = @"0";
static NSString *categoryDatalastUpdateTime = @"0";
static NSString *notificationlastUpdateTime = @"0";

static int maxImageSize = 200 * 1024;
static int compressImageSizeToReduce = 50 * 1024;
static int numberOfAdsPerRow = 0;
static bool isContinueCountintRows;
static NSString *sharedNotificationCount=nil;
static BOOL *isFromSelectImageViewControllerBool;

NSString *carFiltersKey = @"1";
NSString *propertyFiltersKey = @"2";
NSString *otherFiltersKey = @"0";

static NSMutableArray *categories = nil;

static bool isDialogRateShown = false;

@implementation SharedData

//+(NSString*) getproductCommentsLastUpdateTime
//{
//    return productCommentlastUpdateTime;
//}
//
//+(void) setproductCommentsLastUpdateTime
//:(NSString*)productCommentLastUpdateTimeParam
//{
//    productCommentlastUpdateTime=productCommentLastUpdateTimeParam;
//}

+ (NSString *)getUserProductsLastUpdateTime {
  return userProductslastUpdateTime;
}

+ (void)setUserProductsLastUpdateTime:
        (NSString *)userProductsLastUpdateTimeParam {
  userProductslastUpdateTime = userProductsLastUpdateTimeParam;
}

+ (NSString *)getUserFavoriteLastUpdateTime {
  return userFavoritelastUpdateTime;
}

+ (void)setUserFavoriteLastUpdateTime:
        (NSString *)userFavoritesLastUpdateTimeParam {
  userFavoritelastUpdateTime = userFavoritesLastUpdateTimeParam;
}

+ (NSString *)getCategoryDataLastUpdateTime {
  return categoryDatalastUpdateTime;
}
+ (void)setCategoryDataLastUpdateTime:
        (NSString *)categoryDataLastUpdateTimeParam {
  categoryDatalastUpdateTime = categoryDataLastUpdateTimeParam;
}

+ (NSString *)getNotificationLastUpdateTime {
  return notificationlastUpdateTime;
}
+ (void)setNotificationLastUpdateTime:
        (NSString *)categoryDatalastUpdateTimeParam {
  notificationlastUpdateTime = categoryDatalastUpdateTimeParam;
}

+ (NSMutableArray *)getCategories {
  return categories;
}

+ (void)setCategories:(NSMutableArray *)categoriesParam {
  categories = categoriesParam;
}

+ (bool)isAllowLog {
  return true;
}

+ (void)clearSdImageMemory {
  SDImageCache *imageCache = [SDImageCache sharedImageCache];
  [imageCache clearMemory];

  [imageCache clearDisk];
  [imageCache cleanDisk];
}

+ (int)maxImageSize {
  return maxImageSize;
}

+ (int)compressImageSizeToReduce {
  return compressImageSizeToReduce;
}

+ (NSString *)openedFromAddAdvdrtise {
  return @"0";
}

+ (NSString *)notOpenedFromAddAdvdrtise {
  return @"1";
}

+ (int)numberOfAdvertiseInOneRow {
  return numberOfAdsPerRow;
}

+ (void)setNumberOfAdvertiseInOneRow:(int)numberOfAdsPerRowParam {
  numberOfAdsPerRow = numberOfAdsPerRowParam;
}

+ (BOOL)isContinueCountintRows {
  return isContinueCountintRows;
}

+ (BOOL)getIsFromSelectImageViewControllerBool {
  return isFromSelectImageViewControllerBool;
}

+ (void)setIsContinueCountintRows:(BOOL)isContinueCountintRowsParam {
  isContinueCountintRows = isContinueCountintRowsParam;
}
+ (void)setIsFromSelectImageViewController:(BOOL)isFromSelectImageView {
  isFromSelectImageViewControllerBool = isFromSelectImageView;
}

+ (void)setNotificationCountInSharedData:
        (NSString *)sharedDataNotificationCountParam {
  sharedNotificationCount = sharedDataNotificationCountParam;
}
+ (NSString *)getNotificationCountFromSharedData {
  return sharedNotificationCount;
}

+ (NSString *)getDeviceLanguage {
    //NSString *language = [[[NSBundle mainBundle] preferredLocalizations] objectAtIndex:0];
    NSString *language = [LanguageManager currentLanguageCode];

  if ([language isEqualToString:[ServerManager LANGUAGE_ARABIC]]) {
    return @"ar";
  } else {
    return @"en";
  }
}

+ (NSString *)CITIES_AND_REGION_KEY {
  return @"CITIES_AND_REGION_KEY";
}

+ (NSString *)CATEGORY_ARRAY_FILE_KEY {
  return @"categoriesArrayFile";
}

+ (NSString *)CITIES_AND_REGION_ARRAY_FILE_KEY {
  return @"CitiesAndRegionArrayFile";
}

+ (NSString *)CAR_FILTER_VALUE {
  return carFiltersKey;
}

+ (NSString *)PROPERTY_FILTER_VALUE {
  return propertyFiltersKey;
}

+ (NSString *)OTHER_FILTER_VALUE {
  return otherFiltersKey;
}

+ (NSMutableDictionary *)getSharedServerParams {

    NSString * version = [[NSBundle mainBundle] objectForInfoDictionaryKey: @"CFBundleShortVersionString"];

  NSArray *keys = [NSArray
      arrayWithObjects:USER_MAC_ADDRESS, USER_VERSION_NUMBER, USER_IP_ADDRESS,
                       USER_DEVICE_TYPE, USER_DEVICE_MODEL, USER_COUNTRY_CODE,
                       USER_REGISTERED_NUMBER, USER_DEVICE_LANGUAGE, nil];

  NSArray *objects = [NSArray
      arrayWithObjects:
          [[[UIDevice currentDevice] identifierForVendor] UUIDString],
          version, [self getUserIPAddress], @"IOS",
          [[UIDevice currentDevice] model], [UserSetting getUserCountryCode],
          [UserSetting getUserNumber], [UserSetting getUserCurrentLanguage],
          nil];

  NSMutableDictionary *jsonDictionary =
      [NSMutableDictionary dictionaryWithObjects:objects forKeys:keys];

  return jsonDictionary;
}

+ (NSString *)getUserIPAddress {
  NSString *address = @"error";
  struct ifaddrs *interfaces = NULL;
  struct ifaddrs *temp_addr = NULL;
  int success = 0;

  // retrieve the current interfaces - returns 0 on success
  success = getifaddrs(&interfaces);
  if (success == 0) {
    // Loop through linked list of interfaces
    temp_addr = interfaces;
    while (temp_addr != NULL) {
      if (temp_addr->ifa_addr->sa_family == AF_INET) {
        // Check if interface is en0 which is the wifi connection on the iPhone
        if ([[NSString stringWithUTF8String:temp_addr->ifa_name]
                isEqualToString:@"en0"]) {
          // Get NSString from C String
          address = [NSString
              stringWithUTF8String:inet_ntoa(((struct sockaddr_in *)temp_addr
                                                  ->ifa_addr)->sin_addr)];
        }
      }

      temp_addr = temp_addr->ifa_next;
    }
  }

  // Free memory
  freeifaddrs(interfaces);

  return address;
}

+ (UIStoryboard *)getCurrentStoryBoard {
  NSString *storyBoard;

  if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
    //storyBoard = @"MainStoryboard_iPad";
      storyBoard = @"MainStoryboard_iPhone";
  } else {
    storyBoard = @"MainStoryboard_iPhone";
  }
  UIStoryboard *mainStoryboard =
      [UIStoryboard storyboardWithName:storyBoard bundle:nil];

  return mainStoryboard;
}

+ (void)setIsRateShareDialogShown:(BOOL)isDialogRateShownParam {

    isDialogRateShown = isDialogRateShownParam;
}

+ (bool)isRateShareDialogShown {
    return isDialogRateShown;
}

@end
