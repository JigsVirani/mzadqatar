//
//  SubSubCategoriesChooser.m
//  Mzad Qatar
//
//  Created by samo on 9/2/14.
//  Copyright (c) 2014 Mohammed abdulfattah attia. All rights reserved.
//

#import "SubSubCategoriesChooser.h"

@implementation SubSubCategoriesChooser

static NSString *subSubcategoryChooserCellIdentifier =
@"SubSubCategoryChooserCell";

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.screenName = @"Sub SubCategories Chooser";
    [self startloadingData];
    
    if([LanguageManager currentLanguageIndex]==1){
        self.title = @"نموذج";
    }else{
        self.title = @"Model";
    }
    //[self.navigationController.navigationBar.topItem setTitle:NSLocalizedString(@"BACK", nil)];
}

- (void)startloadingData {
    NSData *arrayData = [[NSUserDefaults standardUserDefaults]
                         objectForKey:[SharedData CATEGORY_ARRAY_FILE_KEY]];
    NSMutableArray *array = [NSKeyedUnarchiver unarchiveObjectWithData:arrayData];
    
    
    [self loadDataInTableFromArray:array];
}

- (void)loadDataInTableFromArray:(NSMutableArray *)array {
    
    
    for (CategoryObject *categoryObject in array) {
        if ([categoryObject.categoryID isEqualToString:currentCategoryId]) {
            for (CategoryObject *subCategoryObject in categoryObject
                 .categorySubcategories) {
                
                if ([subCategoryObject.categoryID
                     isEqualToString:currentSubCategoryId]) {
                    
                    //remove first element if add advertise which all subsubcategories
                    if(isOpenedFromAddAdvertise==true)
                    {
                        if(subCategoryObject
                           .categorySubcategories.count>0)
                        {
                            [subCategoryObject
                             .categorySubcategories removeObjectAtIndex:0];
                        }
                    }
                    
                    for (CategoryObject *subSubCategoryObject in subCategoryObject
                         .categorySubcategories) {
                        {
                            [self.tableData addObject:subSubCategoryObject.categoryName];
                        
                        }
                        
                        productItems = subCategoryObject.categorySubcategories;
                    }
                }
            }
        }
    }
    
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section {
    return [self getTableCount];
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    // comments of product
    SubcategoryTableCell *cell = [tableView
                                  dequeueReusableCellWithIdentifier:subSubcategoryChooserCellIdentifier
                                  forIndexPath:indexPath];
    
    CategoryObject *categoryObject = [productItems objectAtIndex:indexPath.row];
    
    cell.subcatgoryTitle.text = [self getRowText:indexPath];
    cell.textLabel.textColor = [UIColor whiteColor];
    
    NSString *imageName =
    [[categoryObject categoryName] stringByAppendingString:@".png"];
    NSString *url = [categoryObject categoryImageURL];
    
    [cell.subcategoryImageLoadingIndicator startAnimating];
    if ([url isKindOfClass:[NSString class]])
    {
        [cell.subcategoryImageView
         sd_setImageWithURL:[NSURL
                             URLWithString:url]
         placeholderImage:[UIImage imageNamed:imageName]
         completed:^(UIImage *image, NSError *error,
                     SDImageCacheType cacheType, NSURL *imageURL) {
             [cell.subcategoryImageLoadingIndicator stopAnimating];
         }];
        
    }
    
    if (categoryObject.categoryID == currentSelectedSubSubCategoryId) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    } else {
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    // ios 7 background issue
    [self setBackgroundClear:cell];
    
    return cell;
}

- (void)setBackgroundClear:(UITableViewCell *)cell {
    cell.backgroundColor = [UIColor clearColor];
    // cell.backgroundView = [[UIView new] autorelease];
    // cell.selectedBackgroundView = [[UIView new] autorelease];
}

- (void)tableView:(UITableView *)tableView
didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    CategoryObject *subsubCategoryObject;
    
    if([self.searchResult count]>0)
    {
        NSString* subsubcategoryNameSearched=[self.searchResult objectAtIndex:indexPath.row];
        
        for (CategoryObject *subsubCategoryObjectInArray in productItems)
        {
            if([subsubCategoryObjectInArray.categoryName isEqualToString:subsubcategoryNameSearched])
            {
                subsubCategoryObject=subsubCategoryObjectInArray;
                break;
            }
        }
    }
    else
    {
        
    subsubCategoryObject =
    [productItems objectAtIndex:indexPath.row];
    }
    
    [delegate subsubCategoryChoosedInList:subsubCategoryObject.categoryID
                    andSubsubCategoryName:subsubCategoryObject.categoryName];
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)setSubSubCategoryParamsWithCatId:(NSString *)categoryIdParam
                 andCurrentSubCategoryId:(NSString *)subCategoryIDParam
              andCurrentSubSubCategoryId:(NSString *)subSubCategoryIdParam
                andIsOpenedFormAdvertise:(BOOL)isOpenedFromAdvertiseParam
                             andDelegate:(id<SubSubCategoriesChooserDelegate>)
delegateParam {
    currentCategoryId = categoryIdParam;
    currentSubCategoryId = subCategoryIDParam;
    currentSelectedSubSubCategoryId = subSubCategoryIdParam;
    isOpenedFromAddAdvertise = isOpenedFromAdvertiseParam;
    delegate = delegateParam;
}


- (void)viewDidUnload {
    
    [super viewDidUnload];
}

@end
