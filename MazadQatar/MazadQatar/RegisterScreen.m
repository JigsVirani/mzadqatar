//
//  RegisterScreen.m
//  MazadQatar
//
//  Created by samo on 4/1/13.
//  Copyright (c) 2013 Mohammed abdulfattah attia. All rights reserved.
//

#import "RegisterScreen.h"
#import "SearchViewController.h"
#import "ActivationScreen.h"
//#import "TestFlight.h"
#import "CategoryScreen.h"
NSString *requestRegisterBtnClicked = @"registerBtnClicked";

@interface RegisterScreen () <ServerManagerResponseDelegate>

@end

@implementation RegisterScreen

@synthesize successScreenSegueName;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.screenName = @"Register Screen";
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"BACK", nil) style:UIBarButtonItemStylePlain target:nil action:nil];

    if([[SharedData getDeviceLanguage]isEqualToString:@"en"]){
        self.lblTitle.text = @"Registration";
    }else{
        self.lblTitle.text = @"التسجيل";
    }
    
    if ([DisplayUtility isPad]) {
        self.movementDistance = 180; // tweak as needed
    } else {
        self.movementDistance = 100; // tweak as needed
    }
    _userNumberField.textColor = [UIColor whiteColor];
    _userNumberFieldAr.textColor = [UIColor whiteColor];
    
    _qatarCodeTextField.textColor = [UIColor whiteColor];
    
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(notificationbtn:)];
    tapGestureRecognizer.numberOfTapsRequired = 1;
    [self.notificationCountLbl addGestureRecognizer:tapGestureRecognizer];
    self.notificationCountLbl.userInteractionEnabled = YES;
    
    self.detailTxtView.text = NSLocalizedString(@"Register_Description", nil);
    
    if([DisplayUtility isPad]){
        self.registerLbl.font = [UIFont systemFontOfSize:22.0];
        self.mobileLbl.font = [UIFont systemFontOfSize:20.0];
        self.mobileLblAr.font = [UIFont systemFontOfSize:20.0];
        self.detailTxtView.font = [UIFont systemFontOfSize:20.0];
        self.userNumberField.font = [UIFont systemFontOfSize:20.0];
        self.userNumberFieldAr.font = [UIFont systemFontOfSize:20.0];
        self.registerBtn.titleLabel.font = [UIFont systemFontOfSize:22.0];
        self.btnRegisterHeightConstant.constant = 40.0;
        [self.detailTxtView setTextColor:[UIColor whiteColor]];
    }
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    /*if([UserSetting isUserRegistered] == false){
     self.navigationItem.hidesBackButton = false;
     }*/
    
    [self.view endEditing:YES];
    
    if ([UserSetting isUserRegistered] == true) {
        [self.navigationController popViewControllerAnimated:NO];
        return;
    }
    
    /*if([[UIScreen mainScreen]bounds].size.height <= 667){
        CGSize displaySize = [DisplayUtility getScreenSize];
        self.scrollView.contentSize = CGSizeMake(displaySize.width, displaySize.height + 150);
    }*/
    CGSize displaySize = [DisplayUtility getScreenSize];
    self.scrollView.contentSize = CGSizeMake(displaySize.width, displaySize.height);
}

- (void)viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:animated];
    [self.registerIndicator stopAnimating];
    [self.view endEditing:YES];
    //self.tabBarController.tabBar.hidden = NO;
    //[self.tabBarController.tabBar setTranslucent:NO];
}
- (void) viewWillAppear:(BOOL)animated
{
    //self.tabBarController.tabBar.hidden = true;
    [super viewWillAppear:animated];
    
    if([[LanguageManager currentLanguageCode] isEqualToString:@"en"]){
        [self.viewAr setHidden:true];
    }else{
        [self.viewAr setHidden:false];
    }
    
    NSLog(@"%@",self.navigationController.viewControllers);
    for (UIViewController *obj in self.navigationController.viewControllers) {
        if ([obj isKindOfClass:[CategoryScreen class]]) {
            self.tabBarController.tabBar.hidden = YES;
            [self.tabBarController.tabBar setTranslucent:YES];
        }
    }
 
    NSUInteger numberOfViewControllersOnStack = [self.navigationController.viewControllers count];
    UIViewController *parentViewController = self.navigationController.viewControllers[numberOfViewControllersOnStack - 2];
    Class parentVCClass = [parentViewController class];
    
    if ([NSStringFromClass(parentVCClass) isEqualToString:@"ALMessagesViewController"]) {
        self.navigationItem.hidesBackButton = YES;
    }
    
    NSString *strNotificationCount = [SharedData getNotificationCountFromSharedData];
    if([strNotificationCount isEqualToString:@"0"]||[strNotificationCount isEqualToString:@"٠"] || strNotificationCount==nil){
        [self.notificationCountLbl setHidden:YES];
    }else{
        [self.notificationCountLbl setHidden:NO];
        self.notificationCountLbl.text = strNotificationCount;
    }
}

- (void)didReceiveMemoryWarning {
  [super didReceiveMemoryWarning];
  // Dispose of any resources that can be recreated.
}

- (IBAction)registerBtnClicked {
    //self.navigationItem.hidesBackButton = YES;
    
    NSString *userNumber = [[LanguageManager currentLanguageCode] isEqualToString:@"en"] ? self.userNumberField.text : self.userNumberFieldAr.text;

    if ([userNumber length] == 0) {
        [DialogManager showErrorRegisterationNumberEmtpy];
        return;
    }
    
    [self.view endEditing:YES];
    
    [self.registerIndicator startAnimating];
    
    //test mode
    [self performSegueWithIdentifier:@"ActivateSegue" sender:self];
    //allow this only when publish
    
//   [ServerManager registeUserWithName:@"" AndCountryCode:@"00974" AndNumber:userNumber AndEmail:@"" andUserToken:[UserSetting getuserToken] AndDelegate:self withRequestId:requestRegisterBtnClicked];
}

- (void)requestFinished:(ServerManagerResult *)serverManagerResult
            ofRequestId:(NSString *)requestId
    withNumberOfRequest:(int)numberOfRequest {
    
    [self.registerIndicator stopAnimating];

  if ([serverManagerResult.opertationResult
          isEqualToString:[ServerManager ERROR_IN_CONNECTION_OPERATION]]) {
    [DialogManager showErrorInConnection];
      self.navigationItem.hidesBackButton = NO;

        return;
  }
  //    else if([serverManagerResult.opertationResult
  //    isEqualToString:[ServerManager ALREADED_ADDED_OPERATION]])
  //    {
  //        //[TestFlight passCheckpoint:@"Sms limit in registeration screen"];
  //        [DialogManager showErrorResendSmSLimit];
  //    }
  else if ([serverManagerResult.opertationResult
               isEqualToString:[ServerManager FAILED_OPERATION]]) {
    //[DialogManager showErrorWithMessage:serverManagerResult.opertationMsg];
      self.navigationItem.hidesBackButton = NO;
      
      if (self.isViewLoaded && self.view.window){
          // viewController is visible
          [self performSegueWithIdentifier:@"ActivateSegue" sender:self];
          
      }
  } else if ([serverManagerResult.opertationResult
                 isEqualToString:[ServerManager SUCCESS_OPERATION]]) {

            //   [DialogManager showErrorWithMessage:serverManagerResult.opertationMsg];
      self.navigationItem.hidesBackButton = NO;

      if (self.isViewLoaded && self.view.window){
          // viewController is visible
          [self performSegueWithIdentifier:@"ActivateSegue" sender:self];
          
      }
  }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([[segue identifier] isEqualToString:@"ActivateSegue"]) {
        
        ActivationScreen *activationScreen = [segue destinationViewController];
        [activationScreen setSuccessScreenSegueName:[self successScreenSegueName]];
        
        if([[LanguageManager currentLanguageCode] isEqualToString:@"en"]){
            [activationScreen setUserRegisteredNumber:[self.userNumberField.text copy]];
        }else{
            [activationScreen setUserRegisteredNumber:[self.userNumberFieldAr.text copy]];
        }
    }
}

- (void)viewDidUnload {
  // reset setting
  [UserSetting setIsUserRegistered:NO];
  [UserSetting saveUserSettingWithName:@""
                       withCountryCode:@""
                            withNumber:@""
                             withEmail:@""];

 // [self setUserNum؛berField:nil];
  //[self setScrollView:nil];
  [super viewDidUnload];
}
-(void)textFieldDidBeginEditing:(UITextField *)textField{
    
    if([[LanguageManager currentLanguageCode] isEqualToString:@"en"]){
        if([[UIScreen mainScreen]bounds].size.height <= 568){
            [self.scrollView setContentOffset:CGPointMake(0, textField.frame.origin.y-50) animated:YES];
        }else if([[UIScreen mainScreen]bounds].size.height <= 667){
            [self.scrollView setContentOffset:CGPointMake(0, textField.frame.origin.y-100) animated:YES];
        }else if([[UIScreen mainScreen]bounds].size.height <= 736){
            [self.scrollView setContentOffset:CGPointMake(0, textField.frame.origin.y-150) animated:YES];
        }
    }else{
        if([[UIScreen mainScreen]bounds].size.height <= 568){
            [self.scrollView setContentOffset:CGPointMake(0, textField.superview.frame.origin.y-50) animated:YES];
        }else if([[UIScreen mainScreen]bounds].size.height <= 667){
            [self.scrollView setContentOffset:CGPointMake(0, textField.superview.frame.origin.y-100) animated:YES];
        }else if([[UIScreen mainScreen]bounds].size.height <= 736){
            [self.scrollView setContentOffset:CGPointMake(0, textField.superview.frame.origin.y-150) animated:YES];
        }
    }
}
-(IBAction)btnSearchTapped:(id)sender{
    
    SearchViewController *searchVC =
    (SearchViewController *)[[SharedData getCurrentStoryBoard]
                                   instantiateViewControllerWithIdentifier:@"SearchViewController"];
    [self.navigationController pushViewController:searchVC animated:YES];
}
- (IBAction)notificationbtn:(id)sender {
    
    NotificationViewController *controller =
    (NotificationViewController *)[[SharedData getCurrentStoryBoard]
                                   instantiateViewControllerWithIdentifier:@"NotificationView"];
    [self.navigationController pushViewController:controller animated:YES];
}
- (void)willRotateToInterfaceOrientation:
            (UIInterfaceOrientation)toInterfaceOrientation
                                duration:(NSTimeInterval)duration {
  [self.view endEditing:YES];
  [super willRotateToInterfaceOrientation:toInterfaceOrientation
                                 duration:duration];
}

@end
