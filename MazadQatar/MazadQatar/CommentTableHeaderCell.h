//
//  CommentTableHeaderCell.h
//  MazadQatar
//
//  Created by samo on 4/16/13.
//  Copyright (c) 2013 Mohammed abdulfattah attia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CommentTableHeaderCell : UITableViewCell
@property(strong, nonatomic) IBOutlet UILabel *noCommentLabel;
@property(strong, nonatomic)
    IBOutlet UIActivityIndicatorView *loadingCommentsAnimator;

@end
