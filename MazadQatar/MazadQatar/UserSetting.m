//
//  UserSetting.m
//  MazadQatar
//
//  Created by samo on 3/19/13.
//  Copyright (c) 2013 Mohammed abdulfattah attia. All rights reserved.
//

#import "UserSetting.h"
#import "ServerManager.h"

static NSString *userName = @" ";
static NSString *userCountryCode = @" ";
static NSString *userNumber = @" ";
static NSString *userEmail = @" ";
static NSString *userPhoto = @"";
static NSString *userToken = @" ";
static NSString *userPushNotificationLanguage = @" ";
static NSString *allCategoriesFileLanguage = @" ";
static NSString *sortBYFileLanguage = @" ";
static NSString *citiesAndRegoinsFileLanguage = @" ";

static int userAddAdvertiseLanguage = 3;
static int userEnglishAndArabicLanguage = 0;
static int userFileterViewAdvertiseLanguage = -1;

static bool isUserRegistered = false;
static bool isShowPressEditAdvertiseAlert = true;

#define kUserName @"UserName"
#define kUserCountryCode @"UserCountryCode"
#define kUserNumber @"UserNumber"
#define kUserEmail @"UserEmail"
#define kUserPhoto @"UserPhoto"
#define kIsUserRegistered @"isUserRegistered"
#define kuserPushNotificationLanguage @"userLanguage"
#define kAllCategoriesFileLanguage @"allCategoriesFileLanguage"
#define kSortByFileLanguage @"sortByFileLanguage"
#define kCitiesAndRegionsFileLanguage @"citiesAndRegionsFileLanguage"

#define kuserFilterViewAdvertiseLanguage @"userFilterViewAdvertiseLanguage"
#define kIsShowPressEditAdvertiseAlert @"IsShowPressEditAdvertiseAlert"

#define kLastUpdateTimeForCategoriesFromServer                                 \
@"LastUpdateTimeForCategoriesFromServer"
#define kLastUpdateTimeForCategoriesFromLocal                                  \
@"LastUpdateTimeForCategoriesFromLocal"
static NSString *lastUpdateTimeForCategoriesFromServer = @"1";
static NSString *lastUpdateTimeForCategoriesFromLocal = @"0";

#define kLastUpdateTimeForSortByFromServer @"LastUpdateTimeForSortByFromServer"
#define kLastUpdateTimeForSortByFromLocal @"LastUpdateTimeForSortByFromLocal"
static NSString *lastUpdateTimeForSortByFromServer = @"1";
static NSString *lastUpdateTimeForSortByFromLocal = @"0";

#define kLastUpdateTimeForCitiesFromServer @"LastUpdateTimeForCitiesFromServer"
#define kLastUpdateTimeForCitiesFromLocal @"LastUpdateTimeForCitiesFromLocal"
static NSString *lastUpdateTimeForCitiesFromServer = @"1";
static NSString *lastUpdateTimeForCitiesFromLocal = @"0";

#define kRateDialgoueShown @"rateDialogueShown"
#define kShareAppDialogueShown @"ShareAppDialogueShown"
#define kReportDialogueShown @"ReportDialogueShown"

#define kuserToken @"userToken"

NSString *languagesNamesArrays[] = {@"English and Arabic", @"English",
    @"Arabic",@"Please select ad language"};
NSString *languagesNamesArraysArabic[] = {
    @"الإنجليزية و العربية", @"الإنجليزية",@"العربية",
    @"اختار لغة الاعلان"};

@implementation UserSetting

+ (void)loadUserSetting {
    if ([[NSUserDefaults standardUserDefaults] objectForKey:kUserName] != nil) {
        userName = [[NSUserDefaults standardUserDefaults] objectForKey:kUserName];
    }
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey:kUserEmail] != nil) {
        userEmail = [[NSUserDefaults standardUserDefaults] objectForKey:kUserEmail];
    }
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey:kUserCountryCode] !=
        nil) {
        userCountryCode =
        [[NSUserDefaults standardUserDefaults] objectForKey:kUserCountryCode];
    }
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey:kUserPhoto] != nil) {
        userPhoto = [[NSUserDefaults standardUserDefaults] objectForKey:kUserPhoto];
    }
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey:kUserNumber] != nil) {
        if ([[[NSUserDefaults standardUserDefaults]
              objectForKey:kUserNumber] length] != 8) {
            userNumber = [StringUtility
                          initDecryptedString:[[NSUserDefaults standardUserDefaults]
                                               objectForKey:kUserNumber]];
        } else {
            userNumber =
            [[NSUserDefaults standardUserDefaults] objectForKey:kUserNumber];
            
            // so it can be encrypted
            [UserSetting saveUserSettingWithName:userName
                                 withCountryCode:userCountryCode
                                      withNumber:userNumber
                                       withEmail:userEmail];
            
        }
    }
    
    if ([[NSUserDefaults standardUserDefaults] valueForKey:kIsUserRegistered] !=
        nil) {
        isUserRegistered =
        [[NSUserDefaults standardUserDefaults] boolForKey:kIsUserRegistered];
    }
    
    if ([[NSUserDefaults standardUserDefaults]
         valueForKey:kIsShowPressEditAdvertiseAlert] != nil) {
        isShowPressEditAdvertiseAlert = [[NSUserDefaults standardUserDefaults]
                                         boolForKey:kIsShowPressEditAdvertiseAlert];
    }
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey:kuserToken] != nil) {
        userToken = [[NSUserDefaults standardUserDefaults] objectForKey:kuserToken];
    }
    
    if ([[NSUserDefaults standardUserDefaults]
         objectForKey:kuserPushNotificationLanguage] != nil) {
        userPushNotificationLanguage = [[NSUserDefaults standardUserDefaults]
                                        objectForKey:kuserPushNotificationLanguage];
    }
    
    if ([[NSUserDefaults standardUserDefaults]
         objectForKey:kAllCategoriesFileLanguage] != nil) {
        allCategoriesFileLanguage = [[NSUserDefaults standardUserDefaults]
                                     objectForKey:kAllCategoriesFileLanguage];
    }
    
    if ([[NSUserDefaults standardUserDefaults]
         objectForKey:kSortByFileLanguage] != nil) {
        sortBYFileLanguage = [[NSUserDefaults standardUserDefaults]
                              objectForKey:kSortByFileLanguage];
    }
    
    if ([[NSUserDefaults standardUserDefaults]
         objectForKey:kCitiesAndRegionsFileLanguage] != nil) {
        citiesAndRegoinsFileLanguage = [[NSUserDefaults standardUserDefaults]
                                        objectForKey:kCitiesAndRegionsFileLanguage];
    }
    
    if ([[NSUserDefaults standardUserDefaults]
         objectForKey:kuserFilterViewAdvertiseLanguage] != nil) {
        userFileterViewAdvertiseLanguage = [[[NSUserDefaults standardUserDefaults]
                                             objectForKey:kuserFilterViewAdvertiseLanguage] intValue];
    }
    
    if ([[NSUserDefaults standardUserDefaults]
         objectForKey:kLastUpdateTimeForCategoriesFromLocal] != nil) {
        lastUpdateTimeForCategoriesFromLocal =
        [[NSUserDefaults standardUserDefaults]
         objectForKey:kLastUpdateTimeForCategoriesFromLocal];
    }
    
    if ([[NSUserDefaults standardUserDefaults]
         objectForKey:kLastUpdateTimeForCategoriesFromServer] != nil) {
        lastUpdateTimeForCategoriesFromServer =
        [[NSUserDefaults standardUserDefaults]
         objectForKey:kLastUpdateTimeForCategoriesFromServer];
    }
    
    if ([[NSUserDefaults standardUserDefaults]
         objectForKey:kLastUpdateTimeForCitiesFromLocal] != nil) {
        lastUpdateTimeForCitiesFromLocal = [[NSUserDefaults standardUserDefaults]
                                            objectForKey:kLastUpdateTimeForCitiesFromLocal];
    }
    
    if ([[NSUserDefaults standardUserDefaults]
         objectForKey:kLastUpdateTimeForCitiesFromServer] != nil) {
        lastUpdateTimeForCitiesFromServer = [[NSUserDefaults standardUserDefaults]
                                             objectForKey:kLastUpdateTimeForCitiesFromServer];
    }
    
    if ([[NSUserDefaults standardUserDefaults]
         objectForKey:kLastUpdateTimeForSortByFromLocal] != nil) {
        lastUpdateTimeForSortByFromLocal = [[NSUserDefaults standardUserDefaults]
                                            objectForKey:kLastUpdateTimeForSortByFromLocal];
    }
    
    if ([[NSUserDefaults standardUserDefaults]
         objectForKey:kLastUpdateTimeForSortByFromServer] != nil) {
        lastUpdateTimeForSortByFromServer = [[NSUserDefaults standardUserDefaults]
                                             objectForKey:kLastUpdateTimeForSortByFromServer];
    }
}

+ (void)saveUserSettingWithName:(NSString *)name
                withCountryCode:(NSString *)countryCode
                     withNumber:(NSString *)number
                      withEmail:(NSString *)email {
    [[NSUserDefaults standardUserDefaults] setValue:name forKey:kUserName];
    
    NSData *encryptedData = [StringUtility encryptString:number];
    [[NSUserDefaults standardUserDefaults] setValue:encryptedData
                                             forKey:kUserNumber];
    
    [[NSUserDefaults standardUserDefaults] setValue:email forKey:kUserEmail];
    [[NSUserDefaults standardUserDefaults] setValue:countryCode
                                             forKey:kUserCountryCode];
    
    // Return the results of attempting to write preferences to system
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [self loadUserSetting];
}

+ (void)saveUserPhoto:(NSString *)strImageName{
    
    [[NSUserDefaults standardUserDefaults] setValue:strImageName forKey:kUserPhoto];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    userPhoto = [[NSUserDefaults standardUserDefaults] objectForKey:kUserName];
    [self loadUserSetting];
}

+ (void)saveUserName:(NSString *)userNameParam {
    [[NSUserDefaults standardUserDefaults] setValue:userNameParam
                                             forKey:kUserName];
    [[NSUserDefaults standardUserDefaults] synchronize];
 
    userName = [[NSUserDefaults standardUserDefaults] objectForKey:kUserName];
    [self loadUserSetting];
}

+ (void)saveUserToken:(NSString *)userTokenParam {
    [[NSUserDefaults standardUserDefaults] setValue:userTokenParam
                                             forKey:kuserToken];
    [[NSUserDefaults standardUserDefaults] synchronize];
    userToken = [[NSUserDefaults standardUserDefaults] objectForKey:kuserToken];
}

+ (void)saveUserPushNotificationLanguage:(NSString *)userLanguageParam {
    [[NSUserDefaults standardUserDefaults]
     setValue:userLanguageParam
     forKey:kuserPushNotificationLanguage];
    [[NSUserDefaults standardUserDefaults] synchronize];
    userPushNotificationLanguage = [[NSUserDefaults standardUserDefaults]
                                    objectForKey:kuserPushNotificationLanguage];
}

+ (void)saveAllCategoriesFileLanguage:(NSString *)allCategoriesLanguageParam {
    [[NSUserDefaults standardUserDefaults] setValue:allCategoriesLanguageParam
                                             forKey:kAllCategoriesFileLanguage];
    [[NSUserDefaults standardUserDefaults] synchronize];
    allCategoriesFileLanguage = [[NSUserDefaults standardUserDefaults]
                                 objectForKey:kAllCategoriesFileLanguage];
}

+ (void)saveSortByFileLanguage:(NSString *)soryByLangParam {
    [[NSUserDefaults standardUserDefaults] setValue:soryByLangParam
                                             forKey:kSortByFileLanguage];
    [[NSUserDefaults standardUserDefaults] synchronize];
    sortBYFileLanguage =
    [[NSUserDefaults standardUserDefaults] objectForKey:kSortByFileLanguage];
}

+ (void)saveCitiesAndRegionFileLanguage:
(NSString *)citiesAndRegionLanguageParam {
    [[NSUserDefaults standardUserDefaults]
     setValue:citiesAndRegionLanguageParam
     forKey:kCitiesAndRegionsFileLanguage];
    [[NSUserDefaults standardUserDefaults] synchronize];
    citiesAndRegoinsFileLanguage = [[NSUserDefaults standardUserDefaults]
                                    objectForKey:kCitiesAndRegionsFileLanguage];
}

+ (void)saveFilterViewAdvertiseLanguage:(int)userLanguageParamIndex {
    [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithInt:userLanguageParamIndex]
     forKey:kuserFilterViewAdvertiseLanguage];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    userFileterViewAdvertiseLanguage = [[[NSUserDefaults standardUserDefaults]
                                         objectForKey:kuserFilterViewAdvertiseLanguage] intValue];
}

+ (void)setIsShowPressEditAdvertiseAlert:
(BOOL)isShowPressEditAdvertiseAlertParam {
    [[NSUserDefaults standardUserDefaults]
     setBool:isShowPressEditAdvertiseAlertParam
     forKey:kIsShowPressEditAdvertiseAlert];
    
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    isShowPressEditAdvertiseAlert = isShowPressEditAdvertiseAlertParam;
}

+ (bool)isShowPressEditAdvertise {
    return isShowPressEditAdvertiseAlert;
}

+ (NSString *)getUserName {
    return userName;
}

+ (NSString *)getUserCountryCode {
    return userCountryCode;
}

+ (NSString *)getUserNumber {
    return userNumber;
}

+ (NSString *)getUserEmail {
    return userEmail;
}

+ (NSString *)getUserPhoto {
    return userPhoto;
}

+ (NSString *)getuserToken {
    return userToken;
}

+ (NSString *)getUserLanguage {
    return userPushNotificationLanguage;
}

+ (NSString *)getAllCategoriesFileLanguage {
    return [[NSUserDefaults standardUserDefaults]
            objectForKey:kAllCategoriesFileLanguage];
}

+ (NSString *)getSortByFileLanguage {
    return sortBYFileLanguage;
}

+ (NSString *)getCitiesAndRegionFileLanguage {
    return citiesAndRegoinsFileLanguage;
}

+ (int)getUserFilterViewAdvertiseLanguage {
    
    
    return userFileterViewAdvertiseLanguage;
}

+ (void)setIsUserRegistered:(BOOL)isRegistered {
    [[NSUserDefaults standardUserDefaults] setBool:isRegistered
                                            forKey:kIsUserRegistered];
    
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    isUserRegistered = isRegistered;
}

+ (bool)isUserRegistered {
    return isUserRegistered;
}

+ (void)saveUserAddAdvertiseLanguage:(int)userLanguageParam {
    userAddAdvertiseLanguage = userLanguageParam;
}

+ (int)getUserAddAdvertiseLanguage {
    return userAddAdvertiseLanguage;
}

+ (int)EnglishAndArabicLanguageKey {
    return userEnglishAndArabicLanguage;
}

+ (NSString *)getUserAddAdvertiseLanguageName:(int)index {
    
    // set language needed
    //NSString *language = [[[NSBundle mainBundle] preferredLocalizations] objectAtIndex:0];
    NSString *language = [LanguageManager currentLanguageCode];
    
    if ([language isEqualToString:[ServerManager LANGUAGE_ARABIC]]) {
        return languagesNamesArraysArabic[index];
    } else {
        return languagesNamesArrays[index];
    }
}

+ (NSString *)getUserFilterViewAdvertiseLanguageBtnParam:(int)index {
    //NSString *language = [[[NSBundle mainBundle] preferredLocalizations] objectAtIndex:0];
    NSString *language = [LanguageManager currentLanguageCode];
    
    if (index == 0) {
        if ([language isEqualToString:[ServerManager LANGUAGE_ARABIC]]) {
            return @"الكل";
        } else {
            return @"All";
        }
    } else if (index == 1) {
        if ([language isEqualToString:[ServerManager LANGUAGE_ARABIC]]) {
            return @"انجليزي";
        } else {
            return @"En";
        }
    } else if (index == 2) {
        if ([language isEqualToString:[ServerManager LANGUAGE_ARABIC]]) {
            return @"عربي";
        } else {
            return @"Ar";
        }
    }
    
    return @"aren";
}

// return the type of language , arabic and english , arabic , english
// this language type of advertise
+ (NSString *)getAdvertiseTypeServerParam:(int)index {
    if (index == 0) {
        return @"aren";
    } else if (index == 1) {
        return @"en";
    } else if (index == 2) {
        return @"ar";
    }
    
    return @"aren";
}

// return the type of language , arabic and english , arabic , english
// this language type of advertise
+ (int)getAdvertiseTypeServerIndex:(NSString *)serverLangParam {
    if ([serverLangParam isEqualToString:@"aren"]) {
        return 0;
    } else if ([serverLangParam isEqualToString:@"en"]) {
        return 1;
    } else if ([serverLangParam isEqualToString:@"ar"]) {
        return 2;
    }
    
    return 0;
}

// save the last update time for categories
+ (NSString *)getLastUpdateTimeForCategoriesFromLocal {
    return [[NSUserDefaults standardUserDefaults]
            objectForKey:kLastUpdateTimeForCategoriesFromLocal];
}
+ (void)saveLastUpdateTimeForCategoriesFromLocal:(NSString *)lastUpdateTime {
    [[NSUserDefaults standardUserDefaults]
     setObject:lastUpdateTime
     forKey:kLastUpdateTimeForCategoriesFromLocal];
    
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    lastUpdateTimeForCategoriesFromLocal = lastUpdateTime;
}
+ (NSString *)getLastUpdateTimeForCategoriesFromServer {
    return [[NSUserDefaults standardUserDefaults]
            objectForKey:kLastUpdateTimeForCategoriesFromServer];
}

// save the last update time for categories
+ (NSString *)getLastUpdateTimeForSortByFromLocal {
    return [[NSUserDefaults standardUserDefaults]
            objectForKey:kLastUpdateTimeForSortByFromLocal];
}
+ (void)saveLastUpdateTimeForSortByFromLocal:(NSString *)lastUpdateTime {
    [[NSUserDefaults standardUserDefaults]
     setObject:lastUpdateTime
     forKey:kLastUpdateTimeForSortByFromLocal];
    
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    lastUpdateTimeForSortByFromLocal = lastUpdateTime;
}
+ (NSString *)getLastUpdateTimeForSortByFromServer {
    return [[NSUserDefaults standardUserDefaults]
            objectForKey:kLastUpdateTimeForSortByFromServer];
}

// save the last update time for categories
+ (NSString *)getLastUpdateTimeForCitiesFromLocal {
    return [[NSUserDefaults standardUserDefaults]
            objectForKey:kLastUpdateTimeForCitiesFromLocal];
}
+ (void)saveLastUpdateTimeForCitiesFromLocal:(NSString *)lastUpdateTime {
    [[NSUserDefaults standardUserDefaults]
     setObject:lastUpdateTime
     forKey:kLastUpdateTimeForCitiesFromLocal];
    
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    lastUpdateTimeForCitiesFromLocal = lastUpdateTime;
}
+ (NSString *)getLastUpdateTimeForCitiesFromServer {
    
    NSLog( @"%@",[[NSUserDefaults standardUserDefaults]
                  objectForKey:kLastUpdateTimeForCitiesFromServer]);
    
    return [[NSUserDefaults standardUserDefaults]
            objectForKey:kLastUpdateTimeForCitiesFromServer];
}

+ (void)saveAllLastUpdateTimeFromServer:(NSString *)lastUpdateTimeForCategories
                    andSoryByUpdateTime:(NSString *)lastUpdateTimeForSortBy
                    andCitiesUpdateTime:(NSString *)lastUpdateTimeForCities {
    [[NSUserDefaults standardUserDefaults]
     setValue:lastUpdateTimeForCategories
     forKey:kLastUpdateTimeForCategoriesFromServer];
    [[NSUserDefaults standardUserDefaults]
     setValue:lastUpdateTimeForSortBy
     forKey:kLastUpdateTimeForSortByFromServer];
    [[NSUserDefaults standardUserDefaults]
     setValue:lastUpdateTimeForCities
     forKey:kLastUpdateTimeForCitiesFromServer];
    
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    lastUpdateTimeForCategoriesFromServer =  [[NSUserDefaults standardUserDefaults]
                                              objectForKey:kLastUpdateTimeForCategoriesFromServer];
    lastUpdateTimeForSortByFromServer = [[NSUserDefaults standardUserDefaults]
                                         objectForKey:kLastUpdateTimeForSortByFromServer];
    lastUpdateTimeForCitiesFromServer =  [[NSUserDefaults standardUserDefaults]
                                          objectForKey:kLastUpdateTimeForCitiesFromServer];
}

// return the language that will appear in thea advertise
// language or arabic
+ (NSString *)getAdvertiseLanguageServerParam:(int)index {
    if (index == 0) {
        //NSString *language = [[[NSBundle mainBundle] preferredLocalizations] objectAtIndex:0];
        NSString *language = [LanguageManager currentLanguageCode];
        
        if ([language isEqualToString:[ServerManager LANGUAGE_ARABIC]]) {
            return @"ar";
        } else {
            return @"en";
        }
    } else if (index == 1) {
        return @"en";
    } else if (index == 2) {
        return @"ar";
    }
    
    return @"en";
}

+ (int)ENGLISH_ARABIC_CONSTANT {
    return 0;
}

+ (int)ENGLISH_CONSTANT {
    return 1;
}

+ (int)ARABIC_CONSTANT {
    return 2;
}

+ (NSString *)ENGLISH_ARABIC_CONSTANT_STRING {
    return @"aren";
}

+ (NSString *)ENGLISH_CONSTANT_STRING {
    return @"em";
}

+ (NSString *)ARABIC_CONSTANT_STRING {
    return @"ar";
}

+ (BOOL)shouldShowRateDialogue {
    if ([[NSUserDefaults standardUserDefaults] objectForKey:kRateDialgoueShown] ==
        nil) {
        if ([SharedData isAllowLog]) {
            NSLog(@"Running for first time.. Show Rate Dialogue...");
        }
        [[NSUserDefaults standardUserDefaults]
         setObject:[NSNumber numberWithBool:YES]
         forKey:kRateDialgoueShown];
        return YES;
    } else if ([[[NSUserDefaults standardUserDefaults]
                 objectForKey:kRateDialgoueShown] boolValue] == YES) {
        if ([SharedData isAllowLog]) {
            NSLog(@"Show Rate Dialogue...");
        }
        [[NSUserDefaults standardUserDefaults]
         setObject:[NSNumber numberWithBool:NO]
         forKey:kRateDialgoueShown];
        return YES;
    } else {
        if ([SharedData isAllowLog]) {
            NSLog(@"Show Share Dialogue...");
        }
        [[NSUserDefaults standardUserDefaults]
         setObject:[NSNumber numberWithBool:YES]
         forKey:kRateDialgoueShown];
        return NO;
    }
}

+ (NSString *)getUserCurrentLanguage {
    //NSString *language = [[[NSBundle mainBundle] preferredLocalizations] objectAtIndex:0];
    NSString *language = [LanguageManager currentLanguageCode];
    
    if ([language isEqualToString:[ServerManager LANGUAGE_ARABIC]]) {
        return [ServerManager LANGUAGE_ARABIC];
    } else {
        return [ServerManager LANGUAGE_ENGLISH];
    }
}

+(ALUser*)generateUserForApplozic{
    
    ALUser *user = [[ALUser alloc] init];
    [user setUserId:[UserSetting getUserNumber]];
    [user setDisplayName:[UserSetting getUserName]];
    [user setContactNumber:[UserSetting getUserNumber]];
    [user setPassword:[UserSetting getUserNumber]];
    [user setImageLink:[UserSetting getUserPhoto]];
    [user setEmail:[UserSetting getUserEmail]];
    [ALUserDefaultsHandler setUserId:user.userId];
    [ALUserDefaultsHandler setDisplayName:[UserSetting getUserName]];
    [ALUserDefaultsHandler setPassword:user.password];
    
    return user;
}

+(void)registerUserToApplozic{
    
    ALChatManager * chatManager = [[ALChatManager alloc] initWithApplicationKey:APPLOZIC_APPLICATION_ID];
    [chatManager registerUser:[UserSetting generateUserForApplozic]];
}

@end
