//
//  ChooseListWithSearch.m
//  Mzad Qatar
//
//  Created by samo on 8/22/14.
//  Copyright (c) 2014 Mohammed abdulfattah attia. All rights reserved.
//

#import "ChooseListWithSearch.h"

@interface ChooseListWithSearch ()

@end

@implementation ChooseListWithSearch

- (void)viewDidLoad {
  [super viewDidLoad];

  //UIImage *backgroundPattern = [UIImage imageNamed:@"bg.png"];
  //[self.tableView setBackgroundColor:[UIColor colorWithPatternImage:backgroundPattern]];
    [self.tableView setBackgroundColor:[UIColor blackColor]];
    
  self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
  self.tableData = [[NSMutableArray alloc] init];

  self.searchResult = [NSMutableArray arrayWithCapacity:[self.tableData count]];

  [self addLoadingIndicatorToView];
    //[self.navigationController.navigationBar.topItem setTitle:NSLocalizedString(@"BACK", nil)];
}

- (void)didReceiveMemoryWarning {
  [super didReceiveMemoryWarning];
  // Dispose of any resources that can be recreated.
}

- (NSString *)getRowText:(NSIndexPath *)indexPath {
  if ([searchString isEqualToString:@""] == false && searchString != nil) {
    return [self.searchResult objectAtIndex:indexPath.row];
  } else {
    return self.tableData[indexPath.row];
  }
}

- (NSMutableArray *)getCurrentArrayData {
    if ([searchString isEqualToString:@""] == false && searchString != nil) {
        return self.searchResult;
    } else {
        return self.tableData;
    }
}

- (NSInteger)getTableCount {
  if ([searchString isEqualToString:@""] == false && searchString != nil) {
    return [self.searchResult count];
  } else {
    return [self.tableData count];
  }
}

- (void)searchBar:(UISearchBar *)searchBar
    textDidChange:(NSString *)searchText {
  searchString = [searchText copy];

  NSPredicate *resultPredicate =
      [NSPredicate predicateWithFormat:@"SELF contains[c] %@", searchText];

  self.searchResult = [NSMutableArray
      arrayWithArray:[self.tableData
                         filteredArrayUsingPredicate:resultPredicate]];

  [self.tableView reloadData];
}

- (void)addLoadingIndicatorToView {
  activityIndicator = [[UIActivityIndicatorView alloc]
      initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
  activityIndicator.center = self.navigationController.view.center;

  [self.navigationController.view addSubview:activityIndicator];
}

- (void)showOverlayWithIndicator:(BOOL)isShow {
  if (isShow) {
    isOverlayShown = true;
    [activityIndicator startAnimating];

  } else {
    isOverlayShown = false;
    [activityIndicator stopAnimating];
  }
}
- (IBAction)searchTextFieldChnage:(UITextField *)textfield
                         forEvent:(UIEvent *)event {
  searchString = [textfield.text copy];

  NSPredicate *resultPredicate =
      [NSPredicate predicateWithFormat:@"SELF contains[c] %@", searchString];

  self.searchResult = [NSMutableArray
      arrayWithArray:[self.tableData
                         filteredArrayUsingPredicate:resultPredicate]];

  [self.tableView reloadData];
}

@end
