//
//  FurnishedTypeChooserViewController.h
//  Mzad Qatar
//
//  Created by samo on 9/2/14.
//  Copyright (c) 2014 Mohammed abdulfattah attia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GoogleAnalyticsTableViewController.h"
// Delgate
@class FurnishedTypeChooserViewController;
@protocol FurnishedTypeChooserDelegate <NSObject>
@required
- (void)furnishedTypeChoosedInList:(NSString *)furnishedTypeId
                  andFurnishedName:(NSString *)furnishedName;
@required
@end

@interface FurnishedTypeChooserViewController
    : GoogleAnalyticsTableViewController {
  NSString *selectedFurnishedTypeID;
  NSString *isSHowAllText;
  id<FurnishedTypeChooserDelegate> delegate;
        BOOL isAddAdvertise;
}

- (void)setFurnishedParamsWithFurnishedTypeId:(NSString *)furnishedTypeIdParam
                                  andDelegate:(id<FurnishedTypeChooserDelegate>)
delegateParam andIsAddAdvertise:(BOOL)isAddAdvertiseParam;

//@property (nonatomic,retain) id<ChoseLanguageChooserDelegate> delegate;
@property(strong, nonatomic) IBOutlet UITableView *furnishedTypeTable;


@end
