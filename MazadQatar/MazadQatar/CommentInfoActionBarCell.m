//
//  CommentInfoActionBarCell.m
//  MazadQatar
//
//  Created by samo on 4/23/13.
//  Copyright (c) 2013 Mohammed abdulfattah attia. All rights reserved.
//

#import "CommentInfoActionBarCell.h"

@implementation CommentInfoActionBarCell

NSString *requestMoreBtnClicked = @"requestMoreBtnClicked";

- (id)initWithStyle:(UITableViewCellStyle)style
    reuseIdentifier:(NSString *)reuseIdentifier {
  self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
  if (self) {
    // Initialization code
  }
  return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
  [super setSelected:selected animated:animated];

  // Configure the view for the selected state
}

- (void)setMoreBtnDelegate:(id<CommentInfoActionBarDelegate>)delegateParam {
  delegate = delegateParam;
}

- (IBAction)moreBtnClikced:(id)sender {
  // self.resultTextField.hidden=NO;

  // self.resultTextField.text=NSLocalizedString(@"ERROR_TRY_AGAIN_AFTER_REGISTER",
  // nil);

  [delegate moreClickedFinishedWithLoadingIndicator:self.moreCommentIndicator
                                 andResultTextField:self.resultTextField];
}


@end
