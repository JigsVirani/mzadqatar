//
//  CitiesViewController.h
//  Mzad Qatar
//
//  Created by samo on 8/28/14.
//  Copyright (c) 2014 Mohammed abdulfattah attia. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ChooseListWithSearch.h"
#import "ServerManager.h"
#import "SharedData.h"

// Delgate
@class CitiesViewController;
@protocol CitiesViewControllerDelegate <NSObject>
@required
- (void)citiesChoosedInList:(NSString *)cityId andCityName:(NSString *)cityName;
@required
@end

@interface CitiesViewController
    : ChooseListWithSearch <UITableViewDataSource, UITableViewDelegate> {
  NSMutableArray *citiesAndRegionArray;

  bool isOpenedFromAddAdvertise;
  NSString *currentCityId;
  id<CitiesViewControllerDelegate> delegate;
}

- (void)setCityParamsWithCityId:(NSString *)cityIdParam
               andIsOpenedFromAddAdvertise:(bool)isOpenedFromAddAdvertiseParam
                    andDelegate:(id<CitiesViewControllerDelegate>)delegateParam;

- (void)startloadingData;

@end
