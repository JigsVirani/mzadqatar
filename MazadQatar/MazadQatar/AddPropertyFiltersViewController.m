//
//  AddPropertyFiltersViewController.m
//  Mzad Qatar
//
//  Created by samo on 9/18/14.
//  Copyright (c) 2014 Mohammed abdulfattah attia. All rights reserved.
//

#import "AddPropertyFiltersViewController.h"

@interface AddPropertyFiltersViewController ()

@end

@implementation AddPropertyFiltersViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.screenName = @"Add Property Filters Screen";
    self.title = NSLocalizedString(@"Filters of category", nil);
    
    if([[SharedData getDeviceLanguage]isEqualToString:@"en"]){
        
        [self.chooseAdvertiseTypeBtn setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
        [self.chooseCityBtn setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
        [self.chooseRegionBtn setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
        [self.chooseSubcategoryBtn setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
        [self.isFurnishedBtn setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
        
        [self.chooseAdvertiseTypeImg setImage:[UIImage imageNamed:@"ic_english_arrow"]];
        [self.chooseCityImg setImage:[UIImage imageNamed:@"ic_english_arrow"]];
        [self.chooseRegionImg setImage:[UIImage imageNamed:@"ic_english_arrow"]];
        [self.chooseSubcategoryImg setImage:[UIImage imageNamed:@"ic_english_arrow"]];
        [self.isFurnishedImg setImage:[UIImage imageNamed:@"ic_english_arrow"]];
        
        /*[self.chooseAdvertiseTypeBtn setBackgroundImage:[UIImage imageNamed:@"bk_choose_list_btn_right.png"] forState:UIControlStateNormal];
        [self.chooseCityBtn setBackgroundImage:[UIImage imageNamed:@"bk_choose_list_btn_right.png"] forState:UIControlStateNormal];
        [self.chooseRegionBtn setBackgroundImage:[UIImage imageNamed:@"bk_choose_list_btn_right.png"] forState:UIControlStateNormal];
        [self.chooseSubcategoryBtn setBackgroundImage:[UIImage imageNamed:@"bk_choose_list_btn_right.png"] forState:UIControlStateNormal];
        [self.isFurnishedBtn setBackgroundImage:[UIImage imageNamed:@"bk_choose_list_btn_right.png"] forState:UIControlStateNormal];*/
    }else{
        
        [self.chooseAdvertiseTypeBtn setContentHorizontalAlignment:UIControlContentHorizontalAlignmentRight];
        [self.chooseCityBtn setContentHorizontalAlignment:UIControlContentHorizontalAlignmentRight];
        [self.chooseRegionBtn setContentHorizontalAlignment:UIControlContentHorizontalAlignmentRight];
        [self.chooseSubcategoryBtn setContentHorizontalAlignment:UIControlContentHorizontalAlignmentRight];
        [self.isFurnishedBtn setContentHorizontalAlignment:UIControlContentHorizontalAlignmentRight];
        
        [self.chooseAdvertiseTypeImg setImage:[UIImage imageNamed:@"ic_arabic_arrow"]];
        [self.chooseCityImg setImage:[UIImage imageNamed:@"ic_arabic_arrow"]];
        [self.chooseRegionImg setImage:[UIImage imageNamed:@"ic_arabic_arrow"]];
        [self.chooseSubcategoryImg setImage:[UIImage imageNamed:@"ic_arabic_arrow"]];
        [self.isFurnishedImg setImage:[UIImage imageNamed:@"ic_arabic_arrow"]];
        
        [self.chooseAdvertiseTypeBtn setTitleEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 8)];
        [self.chooseCityBtn setTitleEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 8)];
        [self.chooseRegionBtn setTitleEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 8)];
        [self.chooseSubcategoryBtn setTitleEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 8)];
        [self.isFurnishedBtn setTitleEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 8)];
        
        /*[self.chooseAdvertiseTypeBtn setBackgroundImage:[UIImage imageNamed:@"bk_choose_list_btn_left"] forState:UIControlStateNormal];
        [self.chooseCityBtn setBackgroundImage:[UIImage imageNamed:@"bk_choose_list_btn_left"] forState:UIControlStateNormal];
        [self.chooseRegionBtn setBackgroundImage:[UIImage imageNamed:@"bk_choose_list_btn_left"] forState:UIControlStateNormal];
        [self.chooseSubcategoryBtn setBackgroundImage:[UIImage imageNamed:@"bk_choose_list_btn_left"] forState:UIControlStateNormal];
        [self.isFurnishedBtn setBackgroundImage:[UIImage imageNamed:@"bk_choose_list_btn_left"] forState:UIControlStateNormal];*/
    }
    //[self.navigationController.navigationBar.topItem setTitle:NSLocalizedString(@"BACK", nil)];
}

-(void)viewDidAppear:(BOOL)animated
{
    [self resetUiValues];
}

-(void)resetUiValues
{
    if (self.productObject.productAdvertiseTypeName != nil) {
        [self.chooseAdvertiseTypeBtn
         setTitle:self.productObject.productAdvertiseTypeName
         forState:UIControlStateNormal];
    }
    
    if (self.productObject.productCityName != nil) {
        [self.chooseCityBtn setTitle:self.productObject.productCityName
                            forState:UIControlStateNormal];
    }
    
    if (self.productObject.productRegionName != nil) {
        [self.chooseRegionBtn setTitle:self.productObject.productRegionName
                              forState:UIControlStateNormal];
    }
    
    if (self.productObject.productSubCategoryName != nil) {
        [self.chooseSubcategoryBtn
         setTitle:self.productObject.productSubCategoryName
         forState:UIControlStateNormal];
    }
    
    if (self.productObject.productFurnishedTypeName != nil) {
        [self.isFurnishedBtn setTitle:self.productObject.productFurnishedTypeName
                             forState:UIControlStateNormal];
    }
    
    if (self.productObject.productNumberOfRooms != nil) {
        [self.numberOfRoomsTextField
         setText:self.productObject.productNumberOfRooms];
    }
}

-(void)textViewDidEndEditing:(UITextView *)textView
{    
    if(textView == self.numberOfRoomsTextField)
    {
        self.productObject.productNumberOfRooms=textView.text;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier
                                  sender:(id)sender {
    if ([identifier isEqualToString:@"RegionSegue"]) {
        if ([self.productObject.productCityId isEqualToString:@""]) {
            [DialogManager showErrorRegionChoosedBeforeCities];
            
            return false;
        }
    }
    
    return true;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    [super prepareForSegue:segue sender:sender];
    
    if ([[segue identifier] isEqualToString:@"SubCategorySegue"]) {
        SubCategoryChooser *subCategoryChooser = [segue destinationViewController];
        
        [subCategoryChooser
         setSubCategoryParamsWithCatId:self.productObject.productCategoryId
         andCurrentSubCategoryId:self.productObject.productSubCategoryId
         andIsOpenedFromAddAdvertise:true
         andDelegate:self];
    } else if ([[segue identifier] isEqualToString:@"CitiesSegue"]) {
        CitiesViewController *citiesViewController =
        [segue destinationViewController];
        
        [citiesViewController
         setCityParamsWithCityId:self.productObject.productCityId
         andIsOpenedFromAddAdvertise:true
         andDelegate:self];
    } else if ([[segue identifier] isEqualToString:@"RegionSegue"]) {
        RegionListChooser *regionListChooser = [segue destinationViewController];
        
        [regionListChooser
         setRegionParamsWithRegionId:self.productObject.productRegionId
         andSelectedCityId:self.productObject.productCityId andIsOpenedFromAddAdvertise:YES andDelegate:self];
    } else if ([[segue identifier]
                isEqualToString:@"AdvertiseTypeChooserSegue"]) {
        AdvertiseTypeChooser *advertiseTypeChooser =
        [segue destinationViewController];
        
        [advertiseTypeChooser
         setAdvertiseTypeParamsWithCatId:self.productObject.productCategoryId
         andSelectedAdvertiseTypeId:self.productObject
         .productAdvertiseTypeId
         andDelegate:self];
    } else if ([[segue identifier] isEqualToString:@"IsFurnishedSegue"]) {
        FurnishedTypeChooserViewController *furnishedTypeChooserViewController =
        [segue destinationViewController];
        
        [furnishedTypeChooserViewController
         setFurnishedParamsWithFurnishedTypeId:self.productObject
         .productFurnishedTypeId
         andDelegate:self andIsAddAdvertise:true];
    }
}

- (void)subCategoryChoosedInList:(NSString *)subCategoryId
              andSubCategoryName:(NSString *)subCategorName {
    [self.chooseSubcategoryBtn setTitle:subCategorName
                               forState:UIControlStateNormal];
    self.productObject.productSubCategoryId = subCategoryId;
    self.productObject.productSubCategoryName = subCategorName;
}

- (void)citiesChoosedInList:(NSString *)cityId
                andCityName:(NSString *)cityName {
    [self.chooseCityBtn setTitle:cityName forState:UIControlStateNormal];
    self.productObject.productCityId = cityId;
    self.productObject.productCityName = cityName;
    
    // reset
    [self.chooseRegionBtn
     setTitle:NSLocalizedString(@"FILTER_CHOSE_REGION_TEXT", nil)
     forState:UIControlStateNormal];
    self.productObject.productRegionId = nil;
    self.productObject.productRegionName =
    NSLocalizedString(@"FILTER_CHOSE_REGION_TEXT", nil);
}

- (void)createKeyboardAccessoryViewInChild {
    [self.numberOfRoomsTextField setInputAccessoryView:self.inputAccView];
}

- (void)doneTyping {
    // When the "done" button is tapped, the keyboard should go away.
    // That simply means that we just have to resign our first responder.
    [self.numberOfRoomsTextField resignFirstResponder];
}

- (void)AdvertiseTypeChoosed:(NSString *)advertiseTypeChooserId
 andAdvertiseTypeChooserName:(NSString *)advertiseTypeChooserName {
    [self.chooseAdvertiseTypeBtn setTitle:advertiseTypeChooserName
                                 forState:UIControlStateNormal];
    self.productObject.productAdvertiseTypeId = advertiseTypeChooserId;
    self.productObject.productAdvertiseTypeName = advertiseTypeChooserName;
}

- (void)regionChoosedInList:(NSString *)regionId
              andRegionName:(NSString *)regionName {
    [self.chooseRegionBtn setTitle:regionName forState:UIControlStateNormal];
    self.productObject.productRegionId = regionId;
    self.productObject.productRegionName = regionName;
}

- (void)furnishedTypeChoosedInList:(NSString *)furnishedTypeId
                  andFurnishedName:(NSString *)furnishedName {
    [self.isFurnishedBtn setTitle:furnishedName forState:UIControlStateNormal];
    self.productObject.productFurnishedTypeId = furnishedTypeId;
    self.productObject.productFurnishedTypeName = furnishedName;
}

- (IBAction)doneBtnCLicked:(id)sender {
    if ([self validatePropertyFilters] == false) {
        return;
    }
    
    self.productObject.productNumberOfRooms = self.numberOfRoomsTextField.text;
    self.productObject.isAllFiltersSelected=true;
    [self.delegate propertyFiltersChoosedWithProductObject:self.productObject];
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (bool)validatePropertyFilters {
    if (self.productObject.productCityId.length == 0) {
        [DialogManager showErrorCitiesNotChosen];
        
        return false;
    }
    
    if (self.productObject.productAdvertiseTypeId.length == 0) {
        [DialogManager showErrorAdvertiseTypeNotChoosen];
        
        return false;
    }
    
    if (self.productObject.productSubCategoryId.length == 0) {
        [DialogManager showErrorSubCategoryNotChosen];
        
        return false;
    }
    
    if (self.productObject.productRegionId.length == 0) {
        [DialogManager showErrorRegionNotChosen];
        
        return false;
    }
    
    if (self.productObject.productFurnishedTypeId.length == 0) {
        [DialogManager showErrorFurnishedTypeNotChosen];
        
        return false;
    }
    
    if (self.numberOfRoomsTextField.text.length == 0) {
        [DialogManager showErrorNumberOfRoomsNotChosen];
        
        return false;
    }
    
    return true;
}

@end
