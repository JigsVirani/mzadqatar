//
//  RegionListChooser.m
//  Mzad Qatar
//
//  Created by samo on 9/2/14.
//  Copyright (c) 2014 Mohammed abdulfattah attia. All rights reserved.
//

#import "RegionListChooser.h"

@implementation RegionListChooser

- (void)viewDidLoad {
    [super viewDidLoad];
    self.screenName = @"Region List Chooser";
    [self startloadingData];
    
    if([LanguageManager currentLanguageIndex]==1){
        self.title = @"منطقة";
    }else{
        self.title = @"Region";
    }
    //[self.navigationController.navigationBar.topItem setTitle:NSLocalizedString(@"BACK", nil)];
}

- (void)startloadingData {
    [self loadDataInTableFromStoredArray];
}

- (void)loadDataInTableFromStoredArray {
    
    NSData *arrayData = [[NSUserDefaults standardUserDefaults]
                         objectForKey:[SharedData CITIES_AND_REGION_ARRAY_FILE_KEY]];
    NSMutableArray *cityArray =
    [NSKeyedUnarchiver unarchiveObjectWithData:arrayData];
    
    
    for (CitiesObject *cityObject in cityArray) {
        if ([cityObject.cityId isEqualToString:currentCitySelectedID]) {
            
            //dont add first element
            if(isOpenedFromAddAdvertise==true)
            {
                if(cityObject.regions.count>0)
                {
                    [cityObject.regions removeObjectAtIndex:0];
                }
            }
            for (RegionsObject *regionObect in cityObject.regions) {
                
                [self.tableData addObject:regionObect.regionName];
            }
            
            citiesAndRegionArray = cityObject.regions;
            
            break;
        }
    }
    
    [self.tableView reloadData];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section {
    return [self getTableCount];
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell =
    [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    RegionsObject *regionObject =
    [citiesAndRegionArray objectAtIndex:indexPath.row];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier:CellIdentifier];
    }
    
    if (regionObject.regionId == currentRegionSelectedID) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    } else {
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    cell.textLabel.text = [self getRowText:indexPath];
    
    cell.backgroundColor = [UIColor clearColor];
    cell.textLabel.textColor = [UIColor whiteColor];
    return cell;
}

- (void)tableView:(UITableView *)tableView
didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    RegionsObject *regionsObject;
    
    if([self.searchResult count]>0)
    {
        NSString* regionNameSearched=[self.searchResult objectAtIndex:indexPath.row];
        
        for (RegionsObject *regionObjectInArray in citiesAndRegionArray)
        {
            if([regionObjectInArray.regionName isEqualToString:regionNameSearched])
            {
                regionsObject=regionObjectInArray;
                break;
            }
        }
    }
    else
    {
        regionsObject =
        [citiesAndRegionArray objectAtIndex:indexPath.row];
    }
    [delegate regionChoosedInList:regionsObject.regionId
                    andRegionName:regionsObject.regionName];
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)setRegionParamsWithRegionId:(NSString *)regionIdParam
                  andSelectedCityId:(NSString *)selectedCityIdParam
        andIsOpenedFromAddAdvertise:(BOOL)isOpenedFromAddAdvertiseParam      andDelegate:
(id<RegionViewControllerDelegate>)delegateParam {
    
    isOpenedFromAddAdvertise=isOpenedFromAddAdvertiseParam;
    currentRegionSelectedID = regionIdParam;
    currentCitySelectedID = selectedCityIdParam;
    delegate = delegateParam;
    
}

@end
