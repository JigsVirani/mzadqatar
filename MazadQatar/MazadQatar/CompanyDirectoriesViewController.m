//
//  CompanyDirectoriesViewController.m
//  Mzad Qatar
//
//  Created by AHMED HEGAB on 11/7/16.
//  Copyright © 2016 Mohammed abdulfattah attia. All rights reserved.
//

#import "CompanyDirectoriesViewController.h"

#import "ServerManager.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "DisplayUtility.h"
#import "ServerManagerResult.h"
#import "ProductDetailsViewController.h"
#import "DisplayUtility.h"
#import "SWRevealViewController.h"
#import "CitiesViewController.h"
#import "CategoryTypesBarCell.h"
#import "GAIDictionaryBuilder.h"
#import "ProfileScreen.h"

#import "CompanyRegistration.h"

@interface CompanyDirectoriesViewController () <ServerManagerResponseDelegate,GADInterstitialDelegate,CategoryChooserDelegate,UITabBarControllerDelegate>

@property(nonatomic, strong) NSString *language;
@end
static int companiesPageNumber = 1;
// static NSString* categoryProductLastUpdateTime= @"0";
static int companiesNumberOfPages = 50;


NSString *CategoryTypesCellIdentifier = @"CategoryTypesBarCell";

@implementation CompanyDirectoriesViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
 
    self.search_View.layer.cornerRadius=5.0;
    self.search_View.layer.borderWidth=1.0;
    self.search_View.layer.borderColor=[UIColor colorWithRed:85/255.0 green:85/255.0 blue:85/255.0 alpha:1.0].CGColor;
    [[UITextField appearanceWhenContainedIn:[UISearchBar class], nil] setDefaultTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    
    _lblPlaceHlder.hidden = true;
      selectedSubCategoryId = @"0";
    
    self.tabBarController.delegate = self;
    
    
    [self addRefreshControlToGrid];
    
    //self.language = [[[NSBundle mainBundle] preferredLocalizations] objectAtIndex:0];
    self.language = [LanguageManager currentLanguageCode];
    
    self.screenName = @"Category Products Screen";
    
    [self initVariables];
    
    if (_search == YES) {
        [self startLoadingCompaniesWithRequestId:requestInitialCompanies andSearchKey:_searchTxt andCategoryId:@"" andIsNewSession:YES];
    }else
    {
       [self startLoadingCompaniesWithRequestId:requestInitialCompanies andSearchKey:@"" andCategoryId:self.productId andIsNewSession:YES];
    }
    NSLog(@"%@",NSLocalizedString(@"Add Company", nil));
    
    
    [_addCompanyBtn.layer setBorderColor:[UIColor whiteColor].CGColor];
    [_addCompanyBtn.layer setBorderWidth:1.5f];
    [_addCompanyBtn setTitle:NSLocalizedString(@"Add Company", nil) forState:UIControlStateNormal];
    // Do any additional setup after loading the view.
    if (IS_IPAD) {
        _addCompanyBtn.titleLabel.font = [UIFont systemFontOfSize:22.0];
        _addCompanyBtnWidthConstant.constant = 200.0;
        [_headerTitleLbl setFont:[UIFont boldSystemFontOfSize:22.0]];
    }
   self.tabBarController.tabBar.hidden = true;
}

-(void)viewWillDisappear:(BOOL)animated{
    
    [super viewWillDisappear:animated];
    [self.customSearchBar resignFirstResponder];
}
-(void)setContentSizeSubview
{
    self.viewConstraints.constant = self.companyCollectionView.contentSize.height;
}
- (void)initVariables {
    isCatgoryProductScreen = true;
    [self resetArrayOfProducts];
    if (self.searchString == nil) {
        self.searchString = @"";
    }
    
    
    if ([categoryProductObject.productSubCategoryId isEqualToString:@"0"]) {
        categoryProductObject.productSubCategoryName =
        NSLocalizedString(@"ALL_SUBCATEGORIES", Nil);
    }
    else if ([categoryProductObject.productSubCategoryId isEqualToString:@""] ||
             categoryProductObject.productSubCategoryId == nil) {
        categoryProductObject.productSubCategoryId = @"0";
        categoryProductObject.productSubCategoryName =
        NSLocalizedString(@"ALL_SUBCATEGORIES", Nil);
    }

    
    
    self.productsLanguage =
    [UserSetting getAdvertiseTypeServerParam:
     [UserSetting getUserFilterViewAdvertiseLanguage]];
    
    loadMoreWhereAvailable = YES;
    
    ipadCellWidth = 242;
    iphoneCellWidth = 149;
    
    [self calculateNumberOfAdvertiseBasedOnRowCount];
    
    [self addRefreshControlToGrid];
    
    //    UIImage *backgroundPattern = [UIImage imageNamed:@"bg.png"];
    //    [self.tableView
    //     setBackgroundColor:[UIColor colorWithPatternImage:backgroundPattern]];
    
    // init the grid or list initial view
    AdvertiseType *tabBarName = [_tabBarNames objectAtIndex:0];
    
    categoryProductObject.productAdvertiseTypeId = tabBarName.advertiseTypeId;
    
    //init the collection view tab current index
    [self resetCategoriesTabCollectionViewInCategoryTypeId:[[self.tabBarNames
                                                             objectAtIndex:self.currentCategoryTypeBarIndex] advertiseTypeId] andIndex:self.currentCategoryTypeBarIndex];
    
    [self setCategoryAdvertiseType: tabBarName];
    
    
    
}

- (void)viewWillAppear:(BOOL)animated {
    // i=0;
    [super viewWillAppear:YES];
    //[self.tabBarController.tabBar setHidden:NO];
}



#pragma mark Button - Action
- (IBAction)btnSearchAction:(id)sender
{
    SearchViewController *controller = [[SharedData getCurrentStoryBoard]
                                        instantiateViewControllerWithIdentifier:@"SearchViewController"];
    [self.navigationController pushViewController:controller animated:YES];
}

- (void)btnCategoryTypeNameClickedWithId:(NSString *)categoryTypeID
                                andIndex:(int)tabIndex;
{
    [self resetCategoriesTabCollectionViewInCategoryTypeId:categoryTypeID andIndex:tabIndex];
    
    [self loadFromLocalOrServer];
}

-(void)resetCategoriesTabCollectionViewInCategoryTypeId:(NSString *)categoryTypeID andIndex:(int)tabIndex
{
    AdvertiseType *tabBarName = [_tabBarNames objectAtIndex:tabIndex];
    // set the default view based on server parap
    [self setCategoryAdvertiseType: tabBarName];
    self.currentCategoryTypeBarIndex = tabIndex;
    categoryProductObject.productAdvertiseTypeId = categoryTypeID;
}
- (void) setCategoryAdvertiseType: (AdvertiseType *) tabBarName{
    

        [self showCompany];
    
    
}

- (void)loadFromLocalOrServer {
    
 
        isPullNewData = true;
        
        [self
         startLoadingCompaniesWithRequestId:requestInitialCompanies andSearchKey:@"" andCategoryId:self.productId
         andIsNewSession:YES];
        
        isPullNewData = false;
    
}

-(void)reloadListOrGrid:(NSString*)requestId
{
    dispatch_async(dispatch_get_main_queue(), ^{
                //scroll to top
                [self.companyCollectionView reloadData];
        [self performSelector:@selector(setContentSizeSubview) withObject:self afterDelay:0.2];
    });
}

-(void) openRegisterScreen{
    
    
    UINavigationController *navcon = (UINavigationController*)self.tabBarController.selectedViewController;
    
    UIViewController *currentViewController = [navcon.viewControllers lastObject];
    Class currentVCClass = [currentViewController class];
    
    if (![NSStringFromClass(currentVCClass) isEqualToString:@"RegisterScreen"]) {
        
        RegisterScreen *controller = [[SharedData getCurrentStoryBoard] instantiateViewControllerWithIdentifier:@"RegisterScreen"];
        
        [navcon pushViewController:controller animated:NO];
    }
}


#pragma mark - WebService functions

- (void)calculateNumberOfAdvertiseBasedOnRowCount {
    // we need advertises to fill all the row ,so we must send the count right ,
    // we calculate it by dividing number of row by current count per page , then
    // we multiply it by row count we get the right total.
    int numberToMultiplyByRowCount =
    ceil((double)companiesNumberOfPages /
         (double)[SharedData numberOfAdvertiseInOneRow]);
    companiesNumberOfPages =
    [SharedData numberOfAdvertiseInOneRow] * numberToMultiplyByRowCount;
}

- (void)requestFinished:(ServerManagerResult *)serverManagerResult
            ofRequestId:(NSString *)requestId
    withNumberOfRequest:(int)numberOfRequest {
    
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    
    
    if ([serverManagerResult.opertationResult
         isEqualToString:[ServerManager ERROR_IN_CONNECTION_OPERATION]]) {
        [DialogManager showErrorInConnection];
        
    }
    
    if ([requestId isEqual:requestInitialCompanies] ||
        [requestId isEqual:requestMoreCompanies] ||
        [requestId isEqual:requestRefreshCompanies]) {
        if (refreshControl.isRefreshing) {
            [refreshControl endRefreshing];
        }
        
        // error in server
        if ([serverManagerResult.opertationResult
             isEqualToString:[ServerManager FAILED_OPERATION]]) {
           //Comment [DialogManager showErrorInServer];
            [DialogManager showErrorWithMessage:serverManagerResult.opertationMsg];
            return;
        }
        
        if ([requestId isEqual:requestInitialCompanies] ||
            [requestId isEqual:requestRefreshCompanies]) {
            loading = NO;
            
            // error happen in server and returened jsonarray nil
            //            if(serverManagerResult.jsonArray.count==0)
            //            {
            //                [ErrorManager showErrorInServer];
            //
            //                return;
            //            }
            
            if(serverManagerResult.jsonArray !=nil)
            {
               
                productItems = serverManagerResult.jsonArray;
                
                
            }
            if (productItems.count == 0) {
                _lblPlaceHlder.hidden = false;
                
            }else
            _lblPlaceHlder.hidden = true;
            
            // [[NotificationViewController getInstance] refreshNowNotificationCount];
            
        } else if ([requestId isEqual:requestMoreCompanies]) {
            loading = NO;
         
        }
        [self reloadListOrGrid:requestId];
    }
}

- (IBAction)btnchangeView:(UIButton *)sender {

            [self showCompany];
 
    
}
- (void)showCompany {
    self.companyCollectionView.hidden = NO;
    [self.companyCollectionView reloadData];
}



- (void)startLoadingCompaniesWithRequestId:(NSString *)requestId andSearchKey: (NSString *) searchKey
andCategoryId:(NSString *) catgeoryId andIsNewSession:(bool)isNewSession {
    
    loading = YES;
    
    if ([requestId isEqual:requestInitialCompanies]) {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [hud setLabelText:NSLocalizedString(@"LOADING_CATEGORYPRODUCTS", Nil)];
    
        
        loadMoreWhereAvailable = true;
        
        // reset it for next call
        isPullNewData = false;
        
        // categoryProductLastUpdateTime=0;
        [SharedData setCategoryDataLastUpdateTime:@"0"];
        
        // [self showOverlayWithIndicator:true];
        
        companiesPageNumber = 1;
    }
    else
    {
        companiesPageNumber=ceilf((double)((float)self.productItems.count/(float)companiesNumberOfPages))+1;
    }
    
    [ServerManager
     getCompaniesDirectoriesRequest:@"0" AndPage:companiesPageNumber AndNumberOfPage:companiesNumberOfPages AndLanguage:self.language withCategoryId:catgeoryId AndSearchKey:searchKey AndDelegate:self withRequestId:requestInitialCompanies];
}


- (IBAction)cancelSearchBar:(id)sender {
    [super cancelSearchBar:sender];
    [self startLoadingCompaniesWithRequestId:requestInitialCompanies andSearchKey:@"" andCategoryId:self.productId andIsNewSession:YES];

}

//-(void)refreshCurrentGrid
//{
//    [ServerManager getCategoryProductsItems:productId withDelegate:self
//    withNewSession:NO withTypeOfUpdate:[ServerManager
//    GET_UPDATES_BEFORE_LAST_UPDATE_TIME] withCategoryProductPageNumber:1
//    withCategoryProductLastUpdateTime:@"0"
//    withCategoryProductNumberOfPages:(categoryProductPageNumber-1)*categoryProductNumberOfPages
//    withSearchStr:self.searchString
//    withRequestId:requestRefreshCategoryProducts
//    withAdvertiseLangType:self.productsLanguage
//    withSubCategoryId:productsSubCategoryId withSortById:soryById
//    withAdvertiseLanguage: [UserSetting
//    getAdvertiseLanguageServerParam:[UserSetting
//    getUserFilterViewAdvertiseLanguage]]];
//
//}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


- (void)getNewData:(NSString *)searchStr {
    [self.view endEditing:true];
    if ([searchStr isEqualToString:@""] == true && isPullNewData == false) {
        return;
    }
    [self startLoadingCompaniesWithRequestId:requestInitialCompanies andSearchKey:searchStr andCategoryId:selectedSubCategoryId
                                    andIsNewSession:YES];
}
- (IBAction)displayCompanyScreen:(UIButton *)sender {
    
    CompanyRegistration  *companyRegistration = [[SharedData getCurrentStoryBoard]
                                                 instantiateViewControllerWithIdentifier:@"RegisterAsCompany"];
    
    [self.navigationController pushViewController:companyRegistration animated:YES];
}

- (void)viewDidUnload {
    [super viewDidUnload];
    [self setContentSizeSubview];
}

-(void)filtersChanged:(ProductObject *)productObjectParam
{
   
}

- (void)carFiltersChoosedWithProductObject:(ProductObject *)productObjectParam {
    
    
    [self filtersChanged:productObjectParam];
    
}

- (void)propertyFiltersChoosedWithProductObject:
(ProductObject *)productObjectParam {
    
    [self filtersChanged:productObjectParam];
}

- (void)otherFiltersChoosedWithProductObject:
(ProductObject *)productObjectParam {
    
    [self filtersChanged:productObjectParam];
    
}

- (void)setCategoryProductsCategoryId:(NSString *)categoryIdParam
                     andSubcategoryId:(NSString *)subcategoryIdParam
                   andSubcategoryName:(NSString *)subcategoryName {
    if (categoryProductObject == nil) {
        categoryProductObject = [[ProductObject alloc] init];
    }
    
    categoryProductObject.productCategoryId = categoryIdParam;
    categoryProductObject.productSubCategoryId = subcategoryIdParam;
    categoryProductObject.productSubCategoryName = subcategoryName;
}
- (void)selectedTabFromSubMenu:(NSString *)strCategoryID withTabID:(NSString *)strTabID
{
    [self btnCategoryTypeNameClickedWithId:strCategoryID andIndex:[strTabID intValue]];
}


- (void) setImageIndicatorForImageView:(UIImageView*) imageView imageURL:(NSString*) url imageName:(NSString*)imageName imageIndicator:(UIActivityIndicatorView*) imageIndicator rounded:(BOOL) isRounded{
    imageView.contentMode = UIViewContentModeScaleToFill;
    [imageView setAlpha:1];
    [imageIndicator startAnimating];
    if ([url isKindOfClass:[NSString class]]) {
        [imageView
         sd_setImageWithURL:[NSURL
                             URLWithString:url]
         placeholderImage:[UIImage imageNamed:imageName]
         completed:^(UIImage *image, NSError *error,
                     SDImageCacheType cacheType, NSURL *imageURL) {
             [imageIndicator stopAnimating];
          
             if(isRounded){
                //[self roundCornersOnView:imageView onTopLeft:true topRight:true bottomLeft:true bottomRight:true radius:50.0f];
             }
         }];
    } else {
        [imageIndicator stopAnimating];
    }
}
-(UIView *)roundCornersOnView:(UIView *)view onTopLeft:(BOOL)tl topRight:(BOOL)tr bottomLeft:(BOOL)bl bottomRight:(BOOL)br radius:(float)radius {
    
    if (tl || tr || bl || br) {
        UIRectCorner corner = 0; //holds the corner
        //Determine which corner(s) should be changed
        if (tl) {
            corner = corner | UIRectCornerTopLeft;
        }
        if (tr) {
            corner = corner | UIRectCornerTopRight;
        }
        if (bl) {
            corner = corner | UIRectCornerBottomLeft;
        }
        if (br) {
            corner = corner | UIRectCornerBottomRight;
        }
        
        UIView *roundedView = view;
        UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:roundedView.bounds byRoundingCorners:corner cornerRadii:CGSizeMake(radius, radius)];
        CAShapeLayer *maskLayer = [CAShapeLayer layer];
        maskLayer.frame = roundedView.bounds;
        maskLayer.path = maskPath.CGPath;
        roundedView.layer.mask = maskLayer;
        return roundedView;
    } else {
        return view;
    }
}


#pragma mark - segue methods

- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender {
    
    if ([identifier isEqualToString:@"ImageViewClicked"] || [identifier isEqualToString:@"ImageAdViewClicked"]) {
        
        UICollectionViewCell *cell = (UICollectionViewCell *)sender;
        NSIndexPath *indexPath = [self.collectionViewGridView indexPathForCell:cell];
        ProductObject *productObjectSelected = [productItems objectAtIndex:indexPath.row];
        
        if([identifier isEqualToString:@"ImageViewClicked"]){
            if([productObjectSelected.productAdvClickType isEqualToString:@"1"]){
                [[UIApplication sharedApplication]openURL:[NSURL URLWithString:productObjectSelected.productAdvertiseUrl]];
                return false;
            }else{
                return true;
            }
        }else{
            if ([productObjectSelected.productAdvertiseUrl isEqualToString:@""] == false) {
                
                [[UIApplication sharedApplication]openURL:[NSURL URLWithString:productObjectSelected.productAdvertiseUrl]];
                return false;
            }
        }
    }
    return true;
}
#pragma mark - Scrollview Delegate -
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    
}
-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    [_customSearchBar resignFirstResponder];
}
#pragma mark SearchBar Delegate
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    
    [searchBar resignFirstResponder];
    isPullNewData = true;
//    [self getNewData:searchBar.text];
    [self getResultBasedOnSearch:searchBar.text];
    isPullNewData = false;
}
- (void)getResultBasedOnSearch:(NSString*)searchText {
    if (productItems.count>0) {
        NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"productTilte BEGINSWITH[cd] %@", searchText];
        productItems = [[productItems filteredArrayUsingPredicate:resultPredicate] mutableCopy];
    }
    [self.companyCollectionView reloadData];
}
-(void)searchBarTextDidEndEditing:(UISearchBar *)searchBar{
    [searchBar resignFirstResponder];
}
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    // This method has been called when u enter some text on search or Cancel the search.
    if([searchText isEqualToString:@""] || searchText==nil) {
        // Nothing to search, empty result.
        if (_search == YES) {
            [self startLoadingCompaniesWithRequestId:requestInitialCompanies andSearchKey:_searchTxt andCategoryId:@"" andIsNewSession:YES];
        }else
        {
            [self startLoadingCompaniesWithRequestId:requestInitialCompanies andSearchKey:@"" andCategoryId:self.productId andIsNewSession:YES];
        }
    }else
    {
    [self getResultBasedOnSearch:searchText];
    }
}

-(void)resetArrayOfProducts
{

}

- (void)listMethod {
   
}
-(void)categoryChoosed:(int)index andCategoryId:(NSString *)categoryId withArray:(NSMutableArray *)arrCategory{
  
    selectedSubCategoryId = categoryId;
    productItems = nil;
    [self.companyCollectionView reloadData];
    [self startLoadingCompaniesWithRequestId:requestInitialCompanies andSearchKey:@"" andCategoryId:selectedSubCategoryId andIsNewSession:YES];

}


#pragma mark Collection Delegate
#pragma mark- UICollectionViewDataSource

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    if (productItems.count == 0) {
        // _lblPlaceHlder.hidden = false;
        return  0;
    }
    //_lblPlaceHlder.hidden = true;
    return productItems.count;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
	
	if (IS_IPAD) {
		return CGSizeMake([UIScreen mainScreen].bounds.size.width/4.0-3, 180);
	}else
		return CGSizeMake([UIScreen mainScreen].bounds.size.width/2.0-20, 130);
	
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    //CompanyGridiPadCell
    CompanyGridCell *cell;
    if (IS_IPAD) {
        cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"CompanyGridiPadCell" forIndexPath:indexPath];
    }else
    {
       cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"CompanyGridCell" forIndexPath:indexPath];
    }
    return  [self returnCompanyCollectionViewCell:indexPath collectionCell:cell];
    return cell;
}

- (UICollectionViewCell *)returnCompanyCollectionViewCell:(NSIndexPath *)indexPath collectionCell:(CompanyGridCell *)cell {
    
    ProductObject *productObject;
    productObject = [productItems objectAtIndex:indexPath.item];
    cell.layer.borderWidth = 1.0;
    cell.layer.borderColor = [[UIColor whiteColor] colorWithAlphaComponent:0.05].CGColor;
    cell.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.05];
    if (indexPath.item < productItems.count) {
        ProductObject *productObject = [productItems objectAtIndex:indexPath.item];
        cell.productObject = productObject;
        [cell.collView reloadData];
        // load image using sdwebimage framework handles all tasks of image download
        NSString *imageName =
        [[productObject productId] stringByAppendingString:@".png"];
        [self setImageIndicatorForImageView:cell.companyImage imageURL:productObject.productMainImageUrl imageName: imageName imageIndicator:cell.companyImageIndicator rounded:true];
        
        if(productObject.productImagesUrls != nil && productObject.productImagesUrls.count > 0){
            cell.lbl.hidden = true;
            cell.collView.hidden = false;
            
        } else {
            cell.lbl.hidden = false;
            cell.collView.hidden = true;
        }
    }
    if(!([productObject.productNumberOfViews isEqual: nil]|| [productObject.productNumberOfViews isEqualToString: @"null"]) ){
        [cell.viewsCount
         setText:[NSString
                  stringWithFormat:@"%@", productObject.productNumberOfViews]];
    }else{
        [cell.viewsCount
         setText:[NSString
                  stringWithFormat:@"%@", @"0"]];
    }
    
    if(!([productObject.productCompanyNumberOfAds isEqual: nil]|| [productObject.productCompanyNumberOfAds isEqualToString: @"null"]) ){
        
        [cell.adsCount
         setText:[NSString
                  stringWithFormat:@"%@", productObject.productCompanyNumberOfAds]];
    }else{
        [cell.adsCount
         setText:[NSString
                  stringWithFormat:@"%@", @"0"]];
    }
    
    [cell.companyTitle setText:productObject.productTilte];
    [cell.companyDetails setText:productObject.productDescription];
    return cell;
    
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    ProductObject *productObjectSelected = [productItems objectAtIndex:indexPath.item];
    if([productObjectSelected.productAdvClickType isEqualToString:@"1"]){
        
        [[UIApplication sharedApplication]openURL:[NSURL URLWithString:productObjectSelected.productAdvertiseUrl]];
        return;
    }else if([productObjectSelected.productAdvClickType isEqualToString:@"2"]){
        
        UserProducts *userProducts = [[SharedData getCurrentStoryBoard]instantiateViewControllerWithIdentifier:@"UserProducts"];
        [userProducts setUserProfileProductObject:productObjectSelected];
        [userProducts showBackButton:YES];
        [self.navigationController pushViewController:userProducts animated:YES];
        
    }else if([productObjectSelected.productAdvClickType isEqualToString:@"0"]){
        
        ProductDetailsViewController *productDetail = [[SharedData getCurrentStoryBoard]
                                                       instantiateViewControllerWithIdentifier:@"ProductDetailsViewController"];
        [productDetail setProductId:productObjectSelected.productId];
        [self.navigationController pushViewController:productDetail animated:YES];
    }else{
        UserProducts *userProfileViewController = [[SharedData getCurrentStoryBoard]instantiateViewControllerWithIdentifier:@"UserProducts"];
        [userProfileViewController setUserProfileProductObject:productObjectSelected];
        [userProfileViewController showBackButton:YES];
        [self.navigationController pushViewController:userProfileViewController animated:YES];
       // [self performSegueWithIdentifier:@"ProfileSegue" sender:productObjectSelected];
    }
}

@end

