//
//  LocationDialogue.m
//  Mzad Qatar
//
//  Created by GuruUgam on 7/23/16.
//  Copyright © 2016 Mohammed abdulfattah attia. All rights reserved.
//

#import "LocationDialogue.h"

@interface LocationDialogue ()

@end

@implementation LocationDialogue

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.screenName = @"Location Dialogue";
    
    self.lblLoactionName.text = _strLocationNameText;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)googleMapsBtnClicked:(id)sender {
    [self.delegate mapTypeBtnClicked:0];
    [self.view removeFromSuperview];
}
- (IBAction)OkBtnClicked:(id)sender {
    [self.delegate OkBtnClicked:self.requestId];
    [self.view removeFromSuperview];
}
- (IBAction)appleMapsBtnClicked:(id)sender {
   [self.delegate mapTypeBtnClicked:1];
    [self.view removeFromSuperview];
}

- (IBAction)cancelBtnClicked:(id)sender {
    [self.delegate cancelBtnClicked:self.requestId];
    [self.view removeFromSuperview];
}
@end
