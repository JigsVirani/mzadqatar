//
//  SendCommentTableCell.h
//  MazadQatar
//
//  Created by samo on 4/17/13.
//  Copyright (c) 2013 Mohammed abdulfattah attia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ServerManager.h"
#import "ProductObject.h"
#import "TextViewTableCell.h"
#import "SharedViewManager.h"
#import "DialogManager.h"
#import "SendCommentTableCell.h"
#import "StringUtility.h"

// Delgate
@class SendCommentTableCell;
@protocol SendCommentDelegate <NSObject>
@required
- (void)commentSent;
@required
@end

@interface SendCommentTableCell
    : TextViewTableCell <ServerManagerResponseDelegate> {
  ProductObject *productObject;
  id<SendCommentDelegate> delegate;
}
@property(strong, nonatomic) IBOutlet UITextView *userCommentName;
@property(strong, nonatomic) IBOutlet UITextView *userComment;
@property(strong, nonatomic)
    IBOutlet UIActivityIndicatorView *sendCommentIndicator;
@property(strong, nonatomic) IBOutlet UIButton *sendBtn;
@property(strong, nonatomic) IBOutlet UIView *background;
@property(strong, nonatomic) IBOutlet UITextField *sendBtnResultText;

- (IBAction)sendBtnClicked:(id)sender;
- (void)setProductOfComment:(ProductObject *)productObjectParam
               withDelegate:(id<SendCommentDelegate>)delegateParam
           inViewController:(UIViewController *)view;

- (bool)validateInput;
- (bool)validateCommentInput;
- (bool)validateCommentNameInput;

@end
