//
//  UpdateUserProfileCell.m
//  Mzad Qatar
//
//  Created by Mohammed AbdulFattah on 1/28/15.
//  Copyright (c) 2015 Mohammed abdulfattah attia. All rights reserved.
//

#import "EditUserProfileCell.h"
#import "DisplayUtility.h"

@implementation EditUserProfileCell

NSString *requestEditUserProfile = @"requestEditUserProfile";
NSString *getUpdatedUserProfile = @"getUpdatedUserProfile";



- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

- (void)setProductInfoCell:(ProductObject *)productObjectParam andParentView:(UIViewController *)parentViewParam {
    
    productObject = productObjectParam;
    parentView = parentViewParam;
    
    NSString *imageName = [[productObject productUserName] stringByAppendingString:@".png"];
    
    self.imageOfUser.layer.cornerRadius = 5.0;
    self.imageOfUser.clipsToBounds = YES;
    
    [self.imageOfUser sd_setImageWithURL:[NSURL URLWithString:productObject.productUserImageUrl]placeholderImage:[UIImage imageNamed:imageName]
     completed:nil];
    
    self.textViewName.text = productObject.productUserName;
    self.textViewEmail.text = productObject.productUserEmail;
    self.textViewShortDescription.text =
    productObject.productUserShortDescription;
    if(productObject.productUserAddress == nil ||[productObject.productUserAddress isEqualToString:@""]||[productObject.productUserAddress isEqualToString:@"0"]){
        productObject.productUserAddress = NSLocalizedString(@"PLEASE_ENTER_YOUR_LOCATION", nil);
    }
    [self.btnLocation setTitle:productObject.productUserAddress forState:UIControlStateNormal];

    [self createKeyboardAccessoryView];
}

- (void)createKeyboardAccessoryView {
    if ([DisplayUtility isPad] == false) {
        
        // Call the createInputAccessoryView method we created earlier.
        // By doing that we will prepare the inputAccView.
        [self createInputAccessoryView];
        
        // Now add the view as an input accessory view to the selected textfield.
        [self.textViewName setInputAccessoryView:inputAccView];
        [self.textViewEmail setInputAccessoryView:inputAccView];
        [self.textViewShortDescription setInputAccessoryView:inputAccView];
    }
}

- (void)createInputAccessoryView {
    
    CGSize screenSize = [DisplayUtility getScreenSize];
    
    if (inputAccView == nil) {
        
        inputAccView = [[UIView alloc]initWithFrame:CGRectMake(10.0, 0.0, screenSize.width, 40.0)];
        // Set the view’s background color. We’ ll set it here to gray. Use any
        // color you want.
        [inputAccView setBackgroundColor:[UIColor colorWithRed:(87.0 / 255.0)green:(95.0 / 255.0) blue:(109.0 / 255.0) alpha:0.8]];
        
        NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
        [nc addObserver:self selector:@selector(orientationChanged:) name:UIApplicationDidChangeStatusBarOrientationNotification object:nil];
    }
    
    if (btnDone == nil) {
        
        btnDone = [UIButton buttonWithType:UIButtonTypeCustom];
        [btnDone setFrame:CGRectMake(screenSize.width - 80, 3, 80.0f, 40.0f)];
        [btnDone setTitle:NSLocalizedString(@"DONE", nil) forState:UIControlStateNormal];
        
        UIImage *backgroundPattern = [UIImage imageNamed:@"btn_advertise.png"];
        [btnDone setBackgroundImage:backgroundPattern forState:UIControlStateNormal];
        
        // [btnDone setBackgroundColor:[UIColor blackColor]];
        [btnDone setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [btnDone addTarget:self action:@selector(doneTyping) forControlEvents:UIControlEventTouchUpInside];
        
        [inputAccView addSubview:btnDone];
    }
}
#pragma mark UITextField Delegate
//====================================================limit the characters
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    
    if (textView == self.textViewName) {
        NSUInteger newLength =
        [textView.text length] + [text length] - range.length;
        return (newLength > 30) ? NO : YES;
    }
    
//    if (textView == self.textViewShortDescription) {
//        
//        NSUInteger newLength =
//        [textView.text length] + [text length] - range.length;
//        return (newLength > 92) ? NO : YES;
//    }
    
    return YES;
}

- (void)doneTyping {
    // When the "done" button is tapped, the keyboard should go away.
    // That simply means that we just have to resign our first responder.
    [self.textViewName resignFirstResponder];
    [self.textViewEmail resignFirstResponder];
    [self.textViewShortDescription resignFirstResponder];
}
- (IBAction)locationButtonClicked:(id)sender {
    [self.delegate tappedLocationBtn];
}
- (IBAction)updateButtonClicked:(id)sender {
    
    [self endEditing:YES];
    
    if ([self validateNameInput] == false) {
        return;
    }
    
    [self.indicatorUpdateProfile startAnimating];
    
    productObject.productUserName = self.textViewName.text;
    productObject.productUserImage = self.imageOfUser.image;
    productObject.productUserEmail = self.textViewEmail.text;
    productObject.productUserShortDescription =
    self.textViewShortDescription.text;
    
    [ServerManager updateUserProfile:productObject andRequestId:requestEditUserProfile andDelegate:self];
}

- (bool)validateNameInput {
    NSCharacterSet *set = [NSCharacterSet whitespaceCharacterSet];
    
    if ([[self.textViewName.text stringByTrimmingCharactersInSet:set] length] == 0) {
        [DialogManager showErrorUpdateUserNameEmpty];
        return false;
    }
    
    if(self.textViewEmail.text.length>0)
    {
       if( [StringUtility NSStringIsValidEmail:self.textViewEmail.text]==false)
       {
           [DialogManager showErrorWithMessage:NSLocalizedString(@"ERROR_EMAIL_WRONG_FORMAT", nil)];
           return false;
       }
    }
    /*if([self.btnLocation.titleLabel.text isEqualToString:NSLocalizedString(@"PLEASE_ENTER_YOUR_LOCATION", nil)]||[productObject.productUserAddress isEqualToString:NSLocalizedString(@"PLEASE_ENTER_YOUR_LOCATION", nil)]){
        [DialogManager showErrorUpdateUserLocationEmpty];
        return false;
    }*/
    return true;
}

- (IBAction)changeImageClicked:(id)sender {
    
    SelectImageDialogueViewController *controller = [[SharedData getCurrentStoryBoard] instantiateViewControllerWithIdentifier:
                                                     @"SelectImageDialogViewController"];
    controller.delegate = self;
    
    [parentView.view addSubview:controller.view];
    [parentView addChildViewController:controller];
}

- (void)requestFinished:(ServerManagerResult *)serverManagerResult
            ofRequestId:(NSString *)requestId
    withNumberOfRequest:(int)numberOfRequest {
    
    [self.indicatorUpdateProfile stopAnimating];
    if (requestId == requestEditUserProfile) {
        
        
        if ([serverManagerResult.opertationResult
             isEqualToString:[ServerManager ERROR_IN_CONNECTION_OPERATION]]) {
            [DialogManager showErrorInConnection];
        }
        
        if ([serverManagerResult.opertationResult
             isEqualToString:[ServerManager SUCCESS_OPERATION]]) {
            
            [self.indicatorUpdateProfile startAnimating];
            [ServerManager getUserProductsWithDelegate:self withUserProductsPageNumber:0 withUserProductsNumberOfPages:0 withRequestId:getUpdatedUserProfile withUpdatTime:[SharedData getUserProductsLastUpdateTime] byLanguage: [UserSetting getAdvertiseLanguageServerParam: [UserSetting getUserFilterViewAdvertiseLanguage]] isAdSupported:@"true" countOfRows:@"0" withAdvertiseResolution:@"" andVisitorCountryCode:[UserSetting getUserCountryCode] andVisitorNumber:[UserSetting getUserNumber]];
        } else if ([serverManagerResult.opertationResult
                    isEqualToString:[ServerManager FAILED_OPERATION]])
        {
            [self.lblStatusOfUpdate setHidden:false];
            
            //Comment [DialogManager showErrorInServer];
            [DialogManager showErrorWithMessage:serverManagerResult.opertationMsg];
        }
    }
    else if (requestId == getUpdatedUserProfile) {
        
        [UserSetting registerUserToApplozic];
        
        [parentView.navigationController popViewControllerAnimated:YES];
    }
}

- (void)SelectImageDelegatePhoto:(UIImage *)photo {
    self.imageOfUser.image = photo;
}

@end
