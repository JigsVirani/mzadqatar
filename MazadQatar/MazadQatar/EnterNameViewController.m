//
//  EnterNameViewController.m
//  Mzad Qatar
//
//  Created by Paresh Kacha on 30/11/15.
//  Copyright © 2015 Mohammed abdulfattah attia. All rights reserved.
//

#import "EnterNameViewController.h"
#import "DisplayUtility.h"
#import "UserSetting.h"


@interface EnterNameViewController ()

@end

@implementation EnterNameViewController
- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.screenName = @"Enter Name Screen";
    if ([DisplayUtility isPad]) {
        self.movementDistance = 50; // tweak as needed
    } else {
        self.movementDistance = 70; // tweak as needed
    }
    self.textToBeWrittenField.delegate = self;
    [self.textToBeWrittenField becomeFirstResponder];
    
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    CGRect aFrame = self.view.frame;
    aFrame.origin.y = -30;
    self.view.frame = aFrame;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)closeDialogClicked:(id)sender {
    [self.view removeFromSuperview];
}


- (IBAction)sendClicked:(id)sender {

        [self.view removeFromSuperview];
        [self.delegate textEntered:self.textToBeWrittenField.text];

   }





@end
