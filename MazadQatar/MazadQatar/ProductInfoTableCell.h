//
//  ProductInfoTableCell.h
//  MazadQatar
//
//  Created by samo on 4/17/13.
//  Copyright (c) 2013 Mohammed abdulfattah attia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProductObject.h"
#import "SharedGraphicInfo.h"
#import "TextViewTableCell.h"
#import "SharedData.h"
#import "CategoryChooser.h"
#import "DialogManager.h"
#import "LanguageChooserViewController.h"
#import "SubCategoryChooser.h"
#import "AddCarFiltersViewController.h"
#import "AddPropertyFiltersViewController.h"
#import "AddOtherFiltersViewController.h"

// Delgate
@class ProductInfoTableCell;
@protocol ProductInfoTableCellDelegate <NSObject>
@optional
- (void)addAdvertiseClickedInCategory:(NSString *)categoryId
                            withTitle:(NSString *)productTitle
                     withTitleEnglish:(NSString *)productTitleEnglish
                      withTitleArabic:(NSString *)productTitleArabic
                     withProductPrice:(NSString *)productPrice
               withProductDescription:(NSString *)productDescription
        withProductDescriptionEnglish:(NSString *)productDescriptionEnglish
         withProductDescriptionArabic:(NSString *)productDescriptionArabic
                  withProductLanguage:(NSString *)productLang
         withIsCategoryFiltersChoosed:(BOOL)isCategoryFiltersChoosed;
- (void)addAdvertiseLanguageChanged:(int)languageIndex;
- (void)productInfoChanged:(ProductObject *)productObjectParam;
@optional
@end

@interface ProductInfoTableCell
: TextViewTableCell <CategoryChooserDelegate, ChoseLanguageChooserDelegate,
AddCarFiltersViewControllerDelegate,
AddPropertyFiltersViewControllerDelgate,
AddOtherFiltersViewControllerDelegate> {
    ProductObject *productObject;
    
    NSString *selectedCategoryId;
    
    int selectedLanguageIndex;
    
    NSString *selectedSubCategoryId;
    
    id<ProductInfoTableCellDelegate> productInfoDelegate;
}



@property(strong, nonatomic) IBOutlet UILabel *productTitleLabel;
@property(strong, nonatomic) IBOutlet UILabel *productPriceLabel;
@property(strong, nonatomic) IBOutlet UILabel *productDescriptionLabel;
@property(strong, nonatomic) IBOutlet UILabel *productCateogryLabel;


@property(strong, nonatomic) IBOutlet UITextView *productTitleTextView;
@property(strong, nonatomic) IBOutlet UITextView *productTitleEnglish;
@property(strong, nonatomic) IBOutlet UITextView *productTitleArabic;

@property(strong, nonatomic) IBOutlet UITextView *productDescriptionTextView;
@property(strong, nonatomic) IBOutlet UITextView *productDescriptionEnglish;
@property(strong, nonatomic) IBOutlet UITextView *productDescriptionArabic;

@property(strong, nonatomic) IBOutlet UIButton *categoryBtn;


@property(weak, nonatomic) IBOutlet UITextView *currentSelectedTextView;

@property (weak, nonatomic) IBOutlet UITextView *firstNumber;

@property (weak, nonatomic) IBOutlet UITextView *secondNumber;

@property (weak, nonatomic) IBOutlet UITextView *thirdNumber;

@property (weak, nonatomic) IBOutlet UITextView *forthNumber;

@property (weak, nonatomic) IBOutlet UITextView *fifthNumber;

@property (weak, nonatomic) IBOutlet UITextView *sixNumber;

@property (weak, nonatomic) IBOutlet UITextView *sevenNumber;

@property (weak, nonatomic) IBOutlet UITextView *eightNumber;

@property (weak, nonatomic) IBOutlet UITextView *nineNumber;


@property(strong, nonatomic) IBOutlet UIButton *btnLangArrow;
@property(strong, nonatomic) IBOutlet UIButton *btnCatArrow;
@property(strong, nonatomic) IBOutlet UIButton *btnFilterArrow;


@property(strong, nonatomic) IBOutlet UIButton *subCategoryBtn;

@property(strong, nonatomic) IBOutlet UIButton *languageOfAdvertiseButton;
@property(strong, nonatomic) IBOutlet UIButton *btnPriceInfoDetail;
@property(strong, nonatomic) IBOutlet UIButton *btnAddAdvertise;
@property(strong, nonatomic) IBOutlet UITextView *lblAddAdvertiseRight;


@property (weak, nonatomic) IBOutlet UITextField *productPriceTextField1;
@property (weak, nonatomic) IBOutlet UITextField *productPriceTextField2;

@property (weak, nonatomic) IBOutlet UITextField *productPriceTextField3;

@property (weak, nonatomic) IBOutlet UITextField *productPriceTextField4;

@property (weak, nonatomic) IBOutlet UITextField *productPriceTextField5;

@property (weak, nonatomic) IBOutlet UITextField *productPriceTextField6;

@property (weak, nonatomic) IBOutlet UITextField *productPriceTextField7;

@property (weak, nonatomic) IBOutlet UITextField *productPriceTextField8;

@property (weak, nonatomic) IBOutlet UITextField *productPriceTextField9;



- (IBAction)btnAdvertiseClicked:(id)sender;
- (IBAction)btnCategoryClicked:(id)sender;
//- (IBAction)btnSubCategoryClicked:(id)sender;
- (void)setProductInfoCell:(ProductObject *)productObjectParam
             andParentView:(UIViewController *)parentViewParam
               andDelegate:(id<ProductInfoTableCellDelegate>)delegate
         fromEditAdvertise:(BOOL)isEditAdvertise;
//-(void)setViewProductInfoUi;
//-(bool)validateInfoToAdd;

//-(CGFloat)getCellHeight:(ProductObject*)productObjectPram;

@end
