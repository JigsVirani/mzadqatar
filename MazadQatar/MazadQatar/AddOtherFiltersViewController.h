//
//  AddOtherFiltersViewController.h
//  Mzad Qatar
//
//  Created by samo on 9/20/14.
//  Copyright (c) 2014 Mohammed abdulfattah attia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TextFieldViewController.h"
#import "CitiesViewController.h"
#import "SubCategoryChooser.h"
#import "SubSubCategoriesChooser.h"
#import "DialogManager.h"
#import "TextFieldViewController.h"
#import "AdvertiseTypeChooser.h"
#import "RegionListChooser.h"
#import "FurnishedTypeChooserViewController.h"
#import "GAITrackedViewController.h"
// Delgate
@class AddOtherFiltersViewController;
@protocol AddOtherFiltersViewControllerDelegate <NSObject>
@required
- (void)otherFiltersChoosedWithProductObject:
(ProductObject *)productObjectParam ;
@required
@end

@interface AddOtherFiltersViewController
    : GAITrackedViewController <SubCategoryChooserDelegate,
                        CitiesViewControllerDelegate,
                        AdvertiseTypeChooserDelegate> {
  NSString *currentCategoryId;
  NSString *currentSubCategoryId;
  NSString *currentCityIdSelected;
  NSString *currentAdvertiseTypeIdSelected;
}

@property(strong,nonatomic) ProductObject *productObject;

@property(strong, nonatomic) IBOutlet UIButton *chooseAdvertiseTypeBtn;
@property(strong, nonatomic) IBOutlet UIButton *chooseCityBtn;
@property(strong, nonatomic) IBOutlet UIButton *chooseSubcategoryBtn;

@property(strong, nonatomic) IBOutlet UIImageView *chooseAdvertiseTypeImg;
@property(strong, nonatomic) IBOutlet UIImageView *chooseCityImg;
@property(strong, nonatomic) IBOutlet UIImageView *chooseSubcategoryImg;

@property(nonatomic, strong) id<AddOtherFiltersViewControllerDelegate> delegate;

- (IBAction)doneBtnClicked:(id)sender;
- (void)setProductObjectUi:(ProductObject *)productObject;
@end
