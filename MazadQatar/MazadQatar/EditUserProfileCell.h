//
//  UpdateUserProfileCell.h
//  Mzad Qatar
//
//  Created by Mohammed AbdulFattah on 1/28/15.
//  Copyright (c) 2015 Mohammed abdulfattah attia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProductObject.h"
#import "TextViewTableCell.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "ServerManager.h"
#import "SelectImageDialogueViewController.h"

@protocol EditUserProfileDelegate <NSObject>

@optional
-(void)tappedLocationBtn;
@end
@interface EditUserProfileCell
    : TextViewTableCell <ServerManagerResponseDelegate, SelectImageDelegate,UITextFieldDelegate> {
  ProductObject *productObject;
  //UIViewController *parentView;
}

//@property(nonatomic, retain) UIView *inputAccView;
//@property(nonatomic, retain) UIButton *btnDone;

@property(strong, nonatomic) IBOutlet UITableView *tableEditProfile;
@property(strong, nonatomic) IBOutlet UITextView *textViewName;
@property(strong, nonatomic) IBOutlet UITextView *textViewEmail;
@property(strong, nonatomic) IBOutlet UITextView *textViewShortDescription;
@property(strong, nonatomic) IBOutlet UILabel *lblStatusOfUpdate;
@property(strong, nonatomic) IBOutlet UIButton *btnLocation;
@property(strong, nonatomic)
    IBOutlet UIActivityIndicatorView *indicatorUpdateProfile;
@property(strong, nonatomic) IBOutlet UIImageView *imageOfUser;

- (IBAction)updateButtonClicked:(id)sender;
- (IBAction)changeImageClicked:(id)sender;

- (void)setProductInfoCell:(ProductObject *)productObjectParam
             andParentView:(UIViewController *)parentViewParam;
@property(nonatomic, strong) id<EditUserProfileDelegate> delegate;
@end
