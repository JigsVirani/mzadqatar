//
//  AdvertiseTypeChooser.h
//  Mzad Qatar
//
//  Created by samo on 9/3/14.
//  Copyright (c) 2014 Mohammed abdulfattah attia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SharedData.h"
#import "ProductObject.h"
#import "AdvertiseType.h"
#import "GoogleAnalyticsTableViewController.h"
@class AdvertiseTypeChooser;
@protocol AdvertiseTypeChooserDelegate <NSObject>
@required
- (void)AdvertiseTypeChoosed:(NSString *)advertiseTypeChooserId
    andAdvertiseTypeChooserName:(NSString *)advertiseTypeChooserName;
@required
@end

@interface AdvertiseTypeChooser
    : GoogleAnalyticsTableViewController <UITableViewDataSource, UITableViewDelegate> {
  id<AdvertiseTypeChooserDelegate> delegate;
  NSString *selectedAdvertiseTypeId;
  NSString *selectedCategoryId;
}

- (void)setAdvertiseTypeParamsWithCatId:(NSString *)categoryId
             andSelectedAdvertiseTypeId:
                 (NSString *)currentSelectedAdvertiseTypeId
                            andDelegate:
                                (id<AdvertiseTypeChooserDelegate>)delegateParam;

//@property (nonatomic,retain) id<ChoseLanguageChooserDelegate> delegate;
@property(strong, nonatomic) IBOutlet UITableView *furnishedTypeTable;

@property(strong, nonatomic) NSMutableArray *advertiseTypesArray;

@end
