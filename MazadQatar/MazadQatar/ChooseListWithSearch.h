//
//  ChooseListWithSearch.h
//  Mzad Qatar
//
//  Created by samo on 8/22/14.
//  Copyright (c) 2014 Mohammed abdulfattah attia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GAITrackedViewController.h"
@interface ChooseListWithSearch : GAITrackedViewController {
  NSString *searchString;
  BOOL isOverlayShown;
  UIActivityIndicatorView *activityIndicator;
}

@property(strong, nonatomic) IBOutlet UITableView *tableView;

@property(nonatomic, strong) NSMutableArray *tableData;

@property(nonatomic, strong) NSMutableArray *searchResult;

- (void)addLoadingIndicatorToView;
- (void)showOverlayWithIndicator:(BOOL)isShow;

- (NSString *)getRowText:(NSIndexPath *)indexPath;
- (NSMutableArray *)getCurrentArrayData;
- (NSInteger)getTableCount;

@end
