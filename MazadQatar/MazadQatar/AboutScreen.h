//
//  AboutScreen.h
//  MazadQatar
//
//  Created by samo on 5/24/13.
//  Copyright (c) 2013 Mohammed abdulfattah attia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import "GoogleAnalyticsTableViewController.h"

@interface AboutScreen: GoogleAnalyticsTableViewController <MFMailComposeViewControllerDelegate,
                             MFMessageComposeViewControllerDelegate>
- (IBAction)websiteClicked:(id)sender;
- (IBAction)contactUsClicked:(id)sender;

@end
