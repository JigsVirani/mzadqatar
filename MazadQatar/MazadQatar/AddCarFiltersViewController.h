//
//  AddCarFiltersViewController.h
//  Mzad Qatar
//
//  Created by samo on 9/11/14.
//  Copyright (c) 2014 Mohammed abdulfattah attia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CitiesViewController.h"
#import "SubCategoryChooser.h"
#import "SubSubCategoriesChooser.h"
#import "DialogManager.h"
#import "TextFieldViewController.h"
#import "AdvertiseTypeChooser.h"

#import "YearChooserViewController.h"

// Delgate
@class AddCarFiltersViewController;
@protocol AddCarFiltersViewControllerDelegate <NSObject>
@required
- (void)carFiltersChoosedWithProductObject:(ProductObject *)productObjectParam;
@required
@end

@interface AddCarFiltersViewController
    : TextFieldViewController <
          SubCategoryChooserDelegate, SubSubCategoriesChooserDelegate,
          CitiesViewControllerDelegate, AdvertiseTypeChooserDelegate, YearChooserDelegate>

@property(weak) ProductObject *productObject;

@property(strong, nonatomic) IBOutlet UIButton *citiesChooserBtn;
@property(strong, nonatomic) IBOutlet UIButton *motorChooserBtn;
@property(strong, nonatomic) IBOutlet UIButton *modelChooserBtn;
@property(strong, nonatomic) IBOutlet UIButton *manfactureYearBtn;

@property (weak, nonatomic) IBOutlet UITextView *firstTextView;
@property (weak, nonatomic) IBOutlet UITextView *secondTextView;
@property (weak, nonatomic) IBOutlet UITextView *thirdTextView;
@property (weak, nonatomic) IBOutlet UITextView *forthTextView;
@property (weak, nonatomic) IBOutlet UITextView *fifthTextView;
@property (weak, nonatomic) IBOutlet UITextView *sixTextView;
@property (weak, nonatomic) IBOutlet UITextView *eightTextView;

@property(weak, nonatomic) IBOutlet UITextView *currentSelectedTextView;

@property(strong, nonatomic) IBOutlet UIButton *advertiseTypeChooserBtn;
@property(strong, nonatomic) IBOutlet UIButton *btnPriceAddInfo;
@property(strong, nonatomic) IBOutlet UIButton *btnDone;

@property(strong, nonatomic) IBOutlet UIImageView *advertiseTypeChooserImg;
@property(strong, nonatomic) IBOutlet UIImageView *citiesChooserImg;
@property(strong, nonatomic) IBOutlet UIImageView *motorChooserImg;
@property(strong, nonatomic) IBOutlet UIImageView *modelChooserImg;
@property(strong, nonatomic) IBOutlet UIImageView *manfactureYearImg;

@property(strong, nonatomic) IBOutlet UILabel *lblAdvertiseTypeChooser;
@property(strong, nonatomic) IBOutlet UILabel *lblCitiesChooser;
@property(strong, nonatomic) IBOutlet UILabel *lblMotorChooser;
@property(strong, nonatomic) IBOutlet UILabel *lblModelChooser;
@property(strong, nonatomic) IBOutlet UILabel *lblManfactureYear;
@property(strong, nonatomic) IBOutlet UILabel *lblKM;

@property(nonatomic, strong) id<AddCarFiltersViewControllerDelegate> delegate;

- (void)setProductObjectUi:(ProductObject *)productObject;
- (IBAction)doneBtnClicked:(id)sender;

@end
