//
//  SearchTableCell.m
//  Mzad Qatar
//
//  Created by Paresh Vasoya on 31/01/17.
//  Copyright © 2017 Mohammed abdulfattah attia. All rights reserved.
//

#import "SearchTableCell.h"

@implementation SearchTableCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
