//
//  SectionView.h
//  RevealView
//
//  Created by Waris Ali on 03/06/2014.
//  Copyright (c) 2014 Waris Ali. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SectionViewProtocol <NSObject>
- (void)sectionTapped:(int)section;
@end

@interface SectionView : UIView

// Attributes
@property(nonatomic, strong) id<SectionViewProtocol> delegate;

@property(nonatomic, strong) UIImageView *sectionView;

// Functions
- (id)initWithFrame:(CGRect)frame
              label:(NSString *)label
                tag:(NSInteger)tag
          collapsed:(BOOL)isCollapsed
           imageURL:(NSString *)imageURL;

@end
