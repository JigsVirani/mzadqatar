//
//  YearChooserViewController.m
//  Mzad Qatar
//
//  Created by AHMED HEGAB on 7/12/16.
//  Copyright © 2016 Mohammed abdulfattah attia. All rights reserved.
//

#import "YearChooserViewController.h"

@interface YearChooserViewController ()

@end


@implementation YearChooserViewController

- (void) viewDidLoad {
    
    self->screenName = @"Year Chooser";
    
    [super viewDidLoad];
    
    //UIImage *backgroundImage = [UIImage imageNamed:@"bg.png"];
    //[self.yearTableView setBackgroundColor:[UIColor colorWithPatternImage:backgroundImage]];
    
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];

    NSInteger firstYear = 1920;

    
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components = [gregorian components:NSYearCalendarUnit fromDate:[NSDate date]];
    NSInteger lastYear =  [components year];
    
    
    //NSString *language = [[[NSBundle mainBundle] preferredLocalizations] objectAtIndex:0];
    NSString *language = [LanguageManager currentLanguageCode];
    
    
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    NSLocale *currentLanguage = [[NSLocale alloc] initWithLocaleIdentifier:language];
    [formatter setLocale:currentLanguage];

    
     self.yearArrayList = [NSMutableArray array];
    
    
    for (int i = lastYear; i >= firstYear ; i-- ) {
        
        NSDecimalNumber *someNumber = [NSDecimalNumber numberWithInt:i];
        
        [self.yearArrayList addObject:[formatter stringFromNumber:someNumber]];

//        [self.yearArrayList addObject:[NSString stringWithFormat:@"%ld", (long)i ]];
    }
    
    self.TempArrYear = [self.yearArrayList mutableCopy];
    //[self.navigationController.navigationBar.topItem setTitle:NSLocalizedString(@"BACK", nil)];
    
    if([LanguageManager currentLanguageIndex]==1){
        self.title = @"تصنيع السنة";
    }else{
        self.title = @"Manufacture Year";
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    UITableViewCell *cell = nil;
    
    //NSString *year = [self.yearArrayList objectAtIndex:indexPath.row];
    NSString *year = [self.TempArrYear objectAtIndex:indexPath.row];
    
    cell = [tableView dequeueReusableCellWithIdentifier:@"yearIdentifier" forIndexPath:indexPath];
    
    cell.accessoryType = UITableViewCellAccessoryNone;
    
    cell.backgroundColor = [UIColor clearColor];
    cell.textLabel.text = year;
    
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 46;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    //NSString *year = [self.yearArrayList objectAtIndex:indexPath.row];
    NSString *year = [self.TempArrYear objectAtIndex:indexPath.row];
    
    [delegate yearChoosed:year];
    
    [self.navigationController popViewControllerAnimated:YES];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    //return self.yearArrayList.count;
    return self.TempArrYear.count;
}

- (void) setYearDelegate:(id<YearChooserDelegate>)delegateParam {
    delegate = delegateParam;    
}
#pragma mark - UITextFiled Delegate
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *strSearch = [textField.text stringByAppendingString:string];
    strSearch = [strSearch stringByReplacingCharactersInRange:range withString:@""];
    
    if(strSearch.length){
        
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(SELF contains[cd] %@)",strSearch];
        self.TempArrYear = [[self.yearArrayList filteredArrayUsingPredicate:predicate] mutableCopy];
    }else{
        self.TempArrYear = [self.yearArrayList mutableCopy];
    }
    [self.tableView reloadData];
    return YES;
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    return [self.txtSearch resignFirstResponder];
}
@end
