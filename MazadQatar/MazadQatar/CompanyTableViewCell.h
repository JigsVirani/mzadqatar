//
//  CompanyTableViewCell.h
//  Mzad Qatar
//
//  Created by AHMED HEGAB on 7/24/16.
//  Copyright © 2016 Mohammed abdulfattah attia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProductObject.h"

@interface CompanyTableViewCell : UITableViewCell <UICollectionViewDataSource>


@property (weak, nonatomic) IBOutlet UILabel *companyTitle;

@property (weak, nonatomic) IBOutlet UILabel *companyDetails;


@property (weak, nonatomic) IBOutlet UIImageView *companyImage;

@property (weak, nonatomic) IBOutlet UILabel *adsCount;

@property (weak, nonatomic) IBOutlet UILabel *viewsCount;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *companyImageIndicator;

@property (weak, nonatomic) IBOutlet UIImageView *adImage1;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *adImage1Indicator;

@property (weak, nonatomic) IBOutlet UIImageView *adImage2;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *adImage2Indicator;

@property (weak, nonatomic) IBOutlet UIImageView *adImage3;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *adImage3Indicator;

@property (weak, nonatomic) IBOutlet UILabel *lbl;
@property (weak, nonatomic) IBOutlet UICollectionView *collView;
@property (weak, nonatomic) ProductObject *productObject;



/** Set Layout For Ipad **/
@property (weak ,nonatomic)IBOutlet UILabel *adLbl,*viewLbl;
@end
