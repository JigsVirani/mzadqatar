//
//  ImagePreviewerTableCell.m
//  Mzad Qatar
//
//  Created by Paresh Kacha on 02/11/15.
//  Copyright © 2015 Mohammed abdulfattah attia. All rights reserved.
//

#import "ImagePreviewerTableCell.h"
#import "SelectImageDialogueViewController.h"

@implementation ImagePreviewerTableCell

static NSString *CellIdentifier = @"GridCell";
// when sdwebimage finished and view deallocated , crash happen
// so we must have flag if view finished or not
// static bool isViewFinished=false;

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

- (void)loadCell:(UIViewController *)parentViewParam
       isAddCell:(bool)isAddCellParam isEditAdvertise:(BOOL)isEditAdvertiseParam
withProductObject:(ProductObject *)productObjectParam isloadingProductDetail:(BOOL)isLoading
isFromCategoryProduct:(BOOL)isCategoryProduct withImageViewerDelegate:(id<ImagePreviewerDelegate>)imageViewerDelegateParam;
{
    //issue to solve right to left issue in related ads
    NSString * language = [[NSLocale preferredLanguages] objectAtIndex:0];
    if([language isEqualToString:[ServerManager LANGUAGE_ARABIC]])
    {
        //[self.collectionView setTransform:CGAffineTransformMakeScale(-1, 1)];
    }
    
    /*Vish Put this below three linecode for image productObjectParam.productImagesUrls.count get zero
     as per discussion with mohammed on 05/07/2017.
    */
    if(isCategoryProduct){
        
        if(productObjectParam.productImagesUrls.count == 0){
            productObjectParam.productImagesUrls = [[NSMutableArray alloc]init];
            if(productObjectParam.productMainImageUrl != nil){
                [productObjectParam.productImagesUrls addObject:productObjectParam.productMainImageUrl];
            }
        }
    }else{
        
        NSMutableArray *temArray = [NSMutableArray new];
        
        if(productObjectParam.productMainImageUrl != nil){
            
            [temArray addObject:productObjectParam.productMainImageUrl];
            for (int i=0; i<productObjectParam.productImagesUrls.count; i++) {
                [temArray addObject:[productObjectParam.productImagesUrls objectAtIndex:i]];
            }
            
            productObjectParam.productImagesUrls = [temArray mutableCopy];
            
            if(productObjectParam.productImagesUrls.count > 1){
                if([productObjectParam.productImagesUrls containsObject:productObjectParam.productMainImageUrl]){
                    [productObjectParam.productImagesUrls removeObject:productObjectParam.productMainImageUrl];
                    [productObjectParam.productImagesUrls addObject:productObjectParam.productMainImageUrl];
                }
            }
        }
    }
    
    
    // isViewFinished=false;
    parentView = parentViewParam;
    isAddCell = isAddCellParam;
    imageViewerDelegate = imageViewerDelegateParam;
    isEditAdvertise = isEditAdvertiseParam;
    
    if (isEditAdvertise) {
        [self startSettingImageViewerUi:productObjectParam
                              isLoading:isLoading
                              isAddCell:isAddCell];
        
        if (productObject.productImagesOnDevices.count == 0) {
            productObject.productImagesOnDevices = productObject.productImagesUrls;
        }
        
        [self.pageControlCount
         setNumberOfPages:productObject.productImagesOnDevices.count];
        if (productObject.productImagesOnDevices.count == 0 && isLoading == false) {
            [self hideAddImage:NO];
        }
        [self hideCommentLikeCount:YES];
    } else if (isAddCell) {
        //#TODO: commented the under line because when changing lang image doesnt
        //download agian , check if the under line issue will appear
        // not to renew with scrolling in table view
        // if(productObject==nil||)
        //{
        
        [self hideAddProductView:YES];
        productObject = productObjectParam;
        [self.pageControlCount
         setNumberOfPages:productObject.productImagesOnDevices.count];
        [self.collectionView reloadData];
        //}
        if (productObject.productImagesOnDevices.count == 0) {
            [self hideAddImage:NO];
        }
        
        [self hideCommentLikeCount:YES];
    } else {
        [self startSettingImageViewerUi:productObjectParam
                              isLoading:isLoading
                              isAddCell:isAddCell];
        [self.pageControlCount
         setNumberOfPages:productObject.productImagesUrls.count];
        if([language isEqualToString:[ServerManager LANGUAGE_ARABIC]])
        {
            [self.pageControlCount setTransform:CGAffineTransformMakeScale(-1, 1)];
        }
    }
}

- (void)startSettingImageViewerUi:(ProductObject *)productObjectParam
                        isLoading:(BOOL)isLoading
                        isAddCell:(bool)isAddCellParam {
    
    if (isLoading) {
        [self.loadingProductDetailsIndicator startAnimating];
    } else {
        [self.loadingProductDetailsIndicator stopAnimating];
        
        //  if(productObject==nil)
        //  {
        productObject = productObjectParam;
        //      NSLog(@"count=%lu",(unsigned long)[productObject retainCount]);
        //  }
        
        isAddCell = isAddCellParam;
        
//        [self.btnLikeCount setTitle:productObject.productLikeCount
//                           forState:UIControlStateNormal];
//        [self.btnCommentsCount setTitle:productObject.productCommentCount
//                               forState:UIControlStateNormal];
        
        [self.collectionView reloadData];
    }
    
    [self hideAddProductView:YES];
}

//========================collection view cell

//- (UICollectionReusableView *)collectionView:(UICollectionView
//*)collectionView viewForSupplementaryElementOfKind:(NSString *)kind
//atIndexPath:(NSIndexPath *)indexPath
//{
//    if(isAddCell&&productObject.productImagesDownloaded.count>0)
//    {
//        UICollectionReusableView *reusableview = nil;
//
//        ImageCollectionFooterCell *footerview = [collectionView
//        dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader
//        withReuseIdentifier:@"HeaderView" forIndexPath:indexPath];
//
//        reusableview = footerview;
//
//        footerSize=footerview.frame.size;
//
//        return reusableview;
//    }
//    else
//    {
//        ImageCollectionViewCell *emptyCell=[[[ImageCollectionViewCell alloc]
//        init] autorelease];
//        [emptyCell setFrame:CGRectZero];
//        return emptyCell;
//    }
//
//    //return nil;
//}

- (NSInteger)collectionView:(UICollectionView *)collectionView
     numberOfItemsInSection:(NSInteger)section {
    if (isEditAdvertise) {
        return productObject.productImagesOnDevices.count;
    } else if (isAddCell == true) {
        return [productObject.productImagesOnDevices count];
    } else {
        return [productObject.productImagesUrls count];
    }
}

//- (CGSize)collectionView:(UICollectionView *)collectionView
//layout:(UICollectionViewLayout*)collectionViewLayout
//referenceSizeForHeaderInSection:(NSInteger)section
//{
//    if(isAddCell==false)
//    {
//        return CGSizeMake(0, 0);
//    }
//    else
//    {
//        if(productObject.productImagesDownloaded.count>0)
//        {
//            return CGSizeMake(collectionView.frame.size.width/3,
//            collectionView.frame.size.height);
//
//        }
//        else
//        {
//            return CGSizeMake(0, 0);
//        }
//    }
//}
-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(self.frame.size.width, collectionView.frame.size.height);
}
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    CGFloat pageWidth = self.collectionView.frame.size.width - 20;
    self.pageControlCount.currentPage =
    self.collectionView.contentOffset.x / pageWidth;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)cv
                  cellForItemAtIndexPath:(NSIndexPath *)indexPath;
{
    //issue to solve right to left issue in related ads
    ImageCollectionViewCell *cell =
    [cv dequeueReusableCellWithReuseIdentifier:CellIdentifier
                                  forIndexPath:indexPath];

    NSIndexPath * newIndexPath;
    NSString * language = [[NSLocale preferredLanguages] objectAtIndex:0];
    if([language isEqualToString:[ServerManager LANGUAGE_ARABIC]])
    {
        NSUInteger count;
        if (isEditAdvertise) {
            count = productObject.productImagesOnDevices.count;
        } else if (isAddCell == true) {
            count = [productObject.productImagesOnDevices count];
        } else {
            count = [productObject.productImagesUrls count];
        }
        newIndexPath = [NSIndexPath indexPathForRow:(count-1)-indexPath.row inSection:0];
        
    }
    else
    {
        newIndexPath = indexPath;
    }
    // reuse cell
//    cell.imageViewCell.layer.cornerRadius = 5.0;
//    cell.imageViewCell.clipsToBounds = YES;
    
    if (isEditAdvertise == true) {
        //        if(indexPath.row>=productObject.productImagesDownloaded.count||productObject.productImagesDownloaded.count==0)
        //        {
        //            cell=[self startSettingImageFromServer:cell
        //            withIndexPath:indexPath
        //            fromArray:productObject.productImagesUrls];
        //        }
        //        else
        //        {
        //            cell=[self startLoadingFromImagesDownloaded:cell
        //            withIndexPath:indexPath
        //            fromArray:productObject.productImagesDownloaded];
        //        }
        
        if ([[productObject.productImagesOnDevices objectAtIndex:newIndexPath.row]
             isKindOfClass:[UIImage class]]) {
            cell =
            [self startLoadingFromImagesDownloaded:cell
                                     withIndexPath:newIndexPath
                                         fromArray:productObject
             .productImagesOnDevices];
        } else {
            cell = [self
                    startSettingImageFromServer:cell
                    withIndexPath:newIndexPath
                    fromArray:productObject.productImagesOnDevices];
        }
    } else if (isAddCell == true) {
        cell = [self
                startLoadingFromImagesDownloaded:cell
                withIndexPath:newIndexPath
                fromArray:productObject.productImagesOnDevices];
    } else {
        cell = [self startSettingImageFromServer:cell
                                   withIndexPath:newIndexPath
                                       fromArray:productObject.productImagesUrls];
    }
    
    //issue to solve right to left issue in related ads
    if([language isEqualToString:[ServerManager LANGUAGE_ARABIC]])
    {
        [cell.imageViewCell setTransform:CGAffineTransformMakeScale(-1, 1)];
    }
    cell.backgroundColor = [UIColor clearColor];
    cell.contentView.backgroundColor = [UIColor clearColor];
    return cell;
}

- (ImageCollectionViewCell *)
startLoadingFromImagesDownloaded:(ImageCollectionViewCell *)cell
withIndexPath:(NSIndexPath *)indexPath
fromArray:(NSMutableArray *)imageArray {
    [cell.loadingIndicator startAnimating];
    [cell.imageViewCell setImage:[imageArray objectAtIndex:indexPath.row]];
    [cell.loadingIndicator stopAnimating];
    return cell;
}
- (ImageCollectionViewCell *)
startSettingImageFromServer:(ImageCollectionViewCell *)cell
withIndexPath:(NSIndexPath *)indexPath
fromArray:(NSMutableArray *)imageArray {
    NSString *imageName =
    [[productObject productId] stringByAppendingString:@".png"];
    NSString *url = [imageArray objectAtIndex:indexPath.row];
    
    [cell setUserInteractionEnabled:false];
    [cell.loadingIndicator startAnimating];
    if ([url isKindOfClass:[NSString class]]) {
        
        [cell.imageViewCell
         sd_setImageWithURL:[NSURL URLWithString:url]
         placeholderImage:[UIImage imageNamed:imageName]
         completed:^(UIImage *image, NSError *error,
                     SDImageCacheType cacheType, NSURL *imageURL) {
             [cell setUserInteractionEnabled:true];
             [cell.loadingIndicator stopAnimating];       }];
        
    }
    return cell;
}

//===========================image navigation browser
- (void)collectionView:(UICollectionView *)collectionView
didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    // Create & present browser
    NSIndexPath * newIndexPath;
    NSString * language = [[NSLocale preferredLanguages] objectAtIndex:0];
    if([language isEqualToString:[ServerManager LANGUAGE_ARABIC]])
    {
        NSUInteger count;
        if (isEditAdvertise) {
            count = productObject.productImagesOnDevices.count;
        } else if (isAddCell == true) {
            count = [productObject.productImagesOnDevices count];
        } else {
            count = [productObject.productImagesUrls count];
        }
        newIndexPath = [NSIndexPath indexPathForRow:(count-1)-indexPath.row inSection:0];
    }
    else
    {
        newIndexPath = indexPath;
    }
    MWPhotoBrowser *browser = [[MWPhotoBrowser alloc] initWithDelegate:self];
    // Set options
    //  browser.wantsFullScreenLayout = YES; // Decide if you want the photo
    //  browser full screen, i.e. whether the status bar is affected (defaults to
    //  YES)
    if (isAddCell) {
        
        browser.displayActionButton = YES; // Show action button to save, copy or
        // email photos (defaults to NO)
        
    }
    [browser setCurrentPhotoIndex:newIndexPath.row]; // Example: allows second image
    // to be presented first
    // Present
    [parentView.navigationController pushViewController:browser animated:YES];
    
}

- (NSUInteger)numberOfPhotosInPhotoBrowser:(MWPhotoBrowser *)photoBrowser {
    if (isEditAdvertise == true) {
        return productObject.productImagesOnDevices.count;
    } else if (isAddCell == true) {
        return productObject.productImagesOnDevices.count;
    } else {
        return productObject.productImagesUrls.count;
    }
}

- (id<MWPhoto>)photoBrowser:(MWPhotoBrowser *)photoBrowser
               photoAtIndex:(NSUInteger)index {
    // if image is not downloaded return url
    if (index < productObject.productImagesOnDevices.count) {
        if ([[productObject.productImagesOnDevices objectAtIndex:index]
             isKindOfClass:[UIImage class]]) {
            return [MWPhoto photoWithImage:[productObject.productImagesOnDevices
                                            objectAtIndex:index]];
        } else {
            return [MWPhoto
                    photoWithURL:[NSURL URLWithString:[productObject.productImagesUrls
                                                       objectAtIndex:index]]];
        }
    } else {
        return [MWPhoto
                photoWithURL:[NSURL URLWithString:[productObject.productImagesUrls
                                                   objectAtIndex:index]]];
    }
}

- (void)imageDeleted:(int)imageIndex {
    if (isEditAdvertise) {
        [productObject setIsResetProductImages:@"true"];
    }
    
    [productObject setIsUploadAdvertiseImages:YES];
    
    [productObject.productImagesOnDevices removeObjectAtIndex:imageIndex];
    
    [self.pageControlCount
     setNumberOfPages:productObject.productImagesOnDevices.count];
    
    // show only result in case of images more than one as it will be noticed if
    // no images
    if (productObject.productImagesOnDevices.count > 0) {
//        [_editButtonsResultTextField setHidden:false];
//        [_editButtonsResultTextField
//         setText:NSLocalizedString(@"EDIT_DELETE_IMAGE_SUCCESS", nil)];
    } else {
        [self hideAddImage:NO];
    }
    
    [self.collectionView reloadData];
}

- (void)imageDefaultImageChanged:(int)imageIndex {
    if (isEditAdvertise) {
        [productObject setIsResetProductImages:@"true"];
    }
    
    [productObject setIsUploadAdvertiseImages:YES];
    
    [productObject.productImagesOnDevices
     insertObject:[productObject.productImagesOnDevices
                   objectAtIndex:imageIndex]
     atIndex:0];
    
    // add 1 because we add element at index 0
    [productObject.productImagesOnDevices removeObjectAtIndex:imageIndex + 1];
    
    // show only result in case of images more than one as it will be noticed if
    // no images
    if (productObject.productImagesOnDevices.count > 0) {
//        [_editButtonsResultTextField setHidden:false];
//        [_editButtonsResultTextField
//         setText:NSLocalizedString(@"EDIT_SET_DEFAULT_IMAGE_SUCCESS", nil)];
    }
    
    [self.collectionView reloadData];
}

// camera image
- (void)didFinishPickingMediaWithInfo:(UIImage *)imageCaptured {
    if (isEditAdvertise) {
        [productObject setIsResetProductImages:@"true"];
    }
    
    
    if (productObject.productImagesOnDevices.count > 14) {
        [DialogManager showErrorMaxImagesLimit];
        return;
    }
    
    [imageViewerDelegate imageAddedToProductObject:imageCaptured
                                   isEditAdvertise:isEditAdvertise];
}

- (void)SelectImageDelegatePhoto:(UIImage *)photo {
    if (productObject.productImagesOnDevices.count > 14) {
        [DialogManager showErrorMaxImagesLimit];
        return;
    }
    
    UIImage *imageCompressed = [self compreessImage:photo];
    
    UIImage *imageResized =
    [self resizeImage:imageCompressed toSize:CGSizeMake(600, 600)];
    
    NSData *imgData = UIImageJPEGRepresentation(imageResized, 0);
    
    if (imgData.length > [SharedData maxImageSize]) {
        [DialogManager showErrorImageSizeLimit];
        return;
    }
    
    if (isEditAdvertise) {
        [productObject setIsResetProductImages:@"true"];
    }
    
    [productObject setIsUploadAdvertiseImages:YES];
    
    [imageViewerDelegate imageAddedToProductObject:imageResized
                                   isEditAdvertise:isEditAdvertise];
}

- (UIImage *)resizeImage:(UIImage *)image toSize:(CGSize)newSize;
{
    UIGraphicsBeginImageContext(newSize);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}

- (UIImage *)compreessImage:(UIImage *)photo {
    NSData *data = UIImageJPEGRepresentation(photo, 0.40);
    
    CGFloat compression = 0.4f;
    CGFloat maxCompression = 0.1f;
    
    while ([data length] > [SharedData compressImageSizeToReduce] &&
           compression > maxCompression) {
        compression -= 0.1;
        data = UIImageJPEGRepresentation(photo, compression);
    }
    
    return [UIImage imageWithData:data];
}
//=====================prepare the segue

- (IBAction)addImageButtonClicked:(UIButton *)sender {
    // issue in keyboard when moving anotther screen
    [parentView.view endEditing:YES];
    
    [self showSelectImageDialog:sender];
}

- (void)showSelectImageDialog:(UIButton *)sender {
    SelectImageDialogueViewController *controller = [
                                                     [SharedData getCurrentStoryBoard] instantiateViewControllerWithIdentifier:
                                                     @"SelectImageDialogViewController"];
    controller.delegate = self;
    
    [parentView.view addSubview:controller.view];
    [parentView addChildViewController:controller];
}

- (void)hideAddProductView:(BOOL)isHide;
{
    [self hideAddImage:isHide];
    if (productObject.isHideAddCommentsAndLikes == true) {
        [self hideCommentLikeCount:YES];
    } else {
        [self hideCommentLikeCount:!isHide];
    }
}

- (void)hideAddImage:(bool)isHide {
//    [self.imageInitialAddImage setHidden:isHide];
//    [self.labelInitialAddImage setHidden:isHide];
//    [self.labelAtLeastImages setHidden:isHide];
    
    if (isEditAdvertise == true) {
        //        [self.smallAddImage setHidden:isHide];
        //        [self.smallAddImageText setHidden:isHide];
        [self hideEditAdButtons:!isHide];
    } else if (isAddCell) {
        //[self.smallAddImage setHidden:!isHide];
        // [self.smallAddImageText setHidden:!isHide];
        [self hideEditAdButtons:!isHide];
        
    } else {
//        [self.smallAddImage setHidden:isHide];
//        [self.smallAddImageText setHidden:isHide];
    }
}

- (void)hideEditAdButtons:(bool)isHide {
//    [self.setAsDefaultInEditBtn setHidden:isHide];
//    [self.setAsDefaultInEditIcon setHidden:isHide];
//    [self.deleteInEditBtn setHidden:isHide];
//    [self.deleteInEditIcon setHidden:isHide];
//    [self.addImageInEditBtn setHidden:isHide];
//    [self.addImageInEditIcon setHidden:isHide];
}

- (void)hideCommentLikeCount:(bool)isHide {
//    [self.btnCommentsCount setHidden:isHide];
//    [self.btnLikeCount setHidden:isHide];
}

- (IBAction)deleteInEditClicked:(id)sender {
    [self imageDeleted:(int)_pageControlCount.currentPage];
}

- (IBAction)setAsDefaultInEditClicked:(id)sender {
    [self imageDefaultImageChanged:(int)_pageControlCount.currentPage];
}

- (void)dealloc {
    
    if (isAddCell == false) {
        //      [productObject release];
    }
    // isViewFinished=true;
}
@end
