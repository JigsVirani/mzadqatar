//
//  CameraViewDelegate.h
//  MazadQatar
//
//  Created by samo on 3/24/13.
//  Copyright (c) 2013 Mohammed abdulfattah attia. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol CameraViewDelegate <NSObject>

- (void)didFinishPickingMediaWithInfo:(UIImage *)imageCaptured;

@end
