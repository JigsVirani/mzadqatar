//
//  CategoryTabsType.m
//  Mzad Qatar
//
//  Created by samo on 9/3/14.
//  Copyright (c) 2014 Mohammed abdulfattah attia. All rights reserved.
//

#import "AdvertiseType.h"

@implementation AdvertiseType 

NSString *ADVERTISE_TYPE_ID_KEY = @"ADVERTISE_TYPE_ID_KEY";
NSString *ADVERTISE_TYPE_NAME_KEY = @"ADVERTISE_TYPE_NAME_KEY";
NSString *ADVERTISE_TYPE_DEFAULT_VIEW_KEY = @"ADVERTISE_TYPE_DEFAULT_VIEW_KEY";


- (void)encodeWithCoder:(NSCoder *)aCoder {
    
    [aCoder encodeObject:self.advertiseTypeId forKey:ADVERTISE_TYPE_ID_KEY];
    [aCoder encodeObject:self.advertiseTypeName forKey:ADVERTISE_TYPE_NAME_KEY];
    [aCoder encodeObject:self.advertiseTypeDefaultView forKey:ADVERTISE_TYPE_DEFAULT_VIEW_KEY];
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];
    if (!self) {
        return nil;
    }
    
    self.advertiseTypeId = [aDecoder decodeObjectForKey:ADVERTISE_TYPE_ID_KEY];
    self.advertiseTypeName = [aDecoder decodeObjectForKey:ADVERTISE_TYPE_NAME_KEY];
    self.advertiseTypeDefaultView = [aDecoder decodeObjectForKey:ADVERTISE_TYPE_DEFAULT_VIEW_KEY];
    
    return self;
}
@end
