//
//  CustomCell.h
//  Mzad Qatar
//
//  Created by Perveen Akhtar on 26/06/2014.
//  Copyright (c) 2014 Mohammed abdulfattah attia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomCell : UITableViewCell

@property(nonatomic, strong)IBOutlet NSLayoutConstraint *arrowMargin;
@property (weak, nonatomic) IBOutlet UIImageView *imgView;
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UIImageView *imgArrw;
@property (weak, nonatomic) IBOutlet UIImageView *imgArrwBG;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activity;




@end
