//
//  SearchViewController.m
//  Mzad Qatar
//
//  Created by Paresh Vasoya on 31/01/17.
//  Copyright © 2017 Mohammed abdulfattah attia. All rights reserved.
//

#import "SearchViewController.h"
#import "SearchTableCell.h"
#import "SharedViewManager.h"
#import "AdvertiseType.h"
#import "ServerManager.h"
#import "SearchTextTableCell.h"

#define TrendingSearchKey @"getTrendingSearch"
#define TextSearchKey @"getTextSearch"

@interface SearchViewController () <UITextFieldDelegate,ServerManagerResponseDelegate, UIScrollViewDelegate>
{
    IBOutlet UITextField *txtSearch;
    IBOutlet UILabel *lblEmptySearchResult;
    IBOutlet UITableView *tblSearchResult;
    IBOutlet UIView *viewSearch;
    NSMutableArray *arrTrendingSearch;
    NSMutableArray *arrCategories;
    
//    NSDictionary *dicCurrentRecord;
    CategoryObject *categoryMain;
    NSString *language;
}
@end

@implementation SearchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.tabBarController.tabBar.hidden = true;
    // Do any additional setup after loading the view.
    tblSearchResult.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
    if ([DisplayUtility isPad]) {
        txtSearch.layer.cornerRadius = 15.0;
    }else{
        txtSearch.layer.cornerRadius = 15.0;
    }
    
    UIView *fieldSpace = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 10)];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        txtSearch.leftViewMode = UITextFieldViewModeAlways;
        txtSearch.leftView = fieldSpace;
    });
    if([LanguageManager currentLanguageIndex] == 1){
        [txtSearch setTextAlignment:NSTextAlignmentRight];
    }else{
        [txtSearch setTextAlignment:NSTextAlignmentLeft];
    }
    
    viewSearch.layer.borderColor = [UIColor whiteColor].CGColor;
    viewSearch.layer.borderWidth = 2.0;
    viewSearch.layer.masksToBounds = YES;
    
    // set language needed
    
    if ([UserSetting getUserFilterViewAdvertiseLanguage] == 0) {
        language= @"aren";
    }else if ([UserSetting getUserFilterViewAdvertiseLanguage] == 1) {
        language= @"en";
    }
    else if ([UserSetting getUserFilterViewAdvertiseLanguage] == 2) {
        language= @"ar";
    }else{
        //language = [[[NSBundle mainBundle] preferredLocalizations] objectAtIndex:0];
        language = [LanguageManager currentLanguageCode];
        
        if ([language isEqualToString:[ServerManager LANGUAGE_ARABIC]]) {
            language = [ServerManager LANGUAGE_ARABIC];
        } else {
            language = [ServerManager LANGUAGE_ENGLISH];
        }
    }
    
    
    if (self.strId.length) {
        txtSearch.placeholder = NSLocalizedString(@"Search this category", nil);
    }
    
    [self getTrendingSearch];
    
    arrCategories = [[NSMutableArray alloc] init];
    [self getTabBar];
    
    [txtSearch becomeFirstResponder];
    //[self.navigationController.navigationBar.topItem setTitle:NSLocalizedString(@"BACK", nil)];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.title = NSLocalizedString(@"SEARCH", nil);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark -
#pragma mark - API Call

- (void)getTrendingSearch
{
   // [ServerManager getTrendingSearch:self withLanguage:language withRequestId:TrendingSearchKey];
}

- (void)getTextSearchWithText:(NSString *)strText
{
    [ServerManager getTextSearch:self withSearchText:strText withCategoryId:self.strId withLanguage:language withRequestId:TextSearchKey];
}

- (void)requestFinished:(ServerManagerResult *)serverManagerResult
            ofRequestId:(NSString *)requestId
    withNumberOfRequest:(int)numberOfRequest {
    if ([requestId isEqualToString:TrendingSearchKey]) {
        arrTrendingSearch = [[NSMutableArray alloc] initWithArray:serverManagerResult.jsonArray];
        [tblSearchResult reloadData];
    }
    else if ([requestId isEqualToString:TextSearchKey])
    {
        arrTrendingSearch = [[NSMutableArray alloc] initWithArray:serverManagerResult.jsonArray];
        [tblSearchResult reloadData];
    }
    
    if (arrTrendingSearch.count != 0) {
        lblEmptySearchResult.hidden = true;
    }else{
        lblEmptySearchResult.hidden = false;
    }
}

#pragma mark -
#pragma mark - UITextField Delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField.text.length) {
         [textField resignFirstResponder];
        if ([_delegate respondsToSelector:@selector(refreshProductListPage:withCategoryId:withCategoryId:withTypeId:)]) {
            [_delegate refreshProductListPage:textField.text withCategoryId:@"" withCategoryId:@"" withTypeId:@""];
            [self.navigationController popViewControllerAnimated:YES];
        }else{
            [self openProductListPage:textField.text withCategoryId:@"" withCategoryId:@"" withTypeId:@"" withSubCatName:@""];
        }
    }else{
         [textField resignFirstResponder];
    }
    
    return TRUE;
}

- (IBAction)textFieldDidChangeText:(UITextField *)textField
{
    if (textField.text.length != 0) {
        //lblTrendingSearch.hidden = TRUE;
        [self getTextSearchWithText:textField.text];
        
        [tblSearchResult reloadData];
    }else{
        //lblTrendingSearch.hidden = FALSE;
        //[self getTrendingSearch];
        arrTrendingSearch = [[NSMutableArray alloc] init];
        [tblSearchResult reloadData];
        
        lblEmptySearchResult.hidden = false;
    }
}

#pragma mark - 
#pragma mark - UITableView DataSource and Delegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (txtSearch.text.length == 0) {
        return 0;
    }else{
        return arrTrendingSearch.count+1;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row == 0) {
        
        static NSString *CellIdentifier = @"";
        SearchTextTableCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SearchTextTableCell"];
        
        if (cell == nil) {
            cell = [[SearchTextTableCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        }
        
        cell.backgroundColor = [UIColor blackColor];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        cell.lblSearchFor.text = [NSString stringWithFormat:@"%@ @%@", NSLocalizedString(@"Search for", nil), txtSearch.text];
        
        if (self.strId.length) {
            
            cell.lblCategoryType.text = self.strCategories;
            
        }else{
            cell.lblCategoryType.text = NSLocalizedString(@"All category", nil);
        }
        
        
        if([LanguageManager currentLanguageIndex] == 1){
            [cell.lblSearchFor setTextAlignment:NSTextAlignmentRight];
            [cell.lblCategoryType setTextAlignment:NSTextAlignmentRight];
            [cell.imgArrow setImage:[UIImage imageNamed:@"next_ar.png"]];
        }else{
            [cell.imgArrow setImage:[UIImage imageNamed:@"next_en.png"]];
            [cell.lblSearchFor setTextAlignment:NSTextAlignmentLeft];
            [cell.lblCategoryType setTextAlignment:NSTextAlignmentLeft];
        }
        
        return cell;
    }else{
        
        static NSString *strCellIdentifier = @"SearchTableCell";
        SearchTableCell *cell = (SearchTableCell *)[tableView dequeueReusableCellWithIdentifier:strCellIdentifier];
        NSDictionary *dicRecord = [arrTrendingSearch objectAtIndex:indexPath.row-1];
        cell.lblSerchTitle.text = [NSString stringWithFormat:@"%@",[dicRecord valueForKey:@"title"]];
        
        cell.contentView.backgroundColor = [UIColor clearColor];
        cell.backgroundColor = [UIColor clearColor];
        
        [self getSubCategoriesArray:dicRecord];
        
        NSString *strTabName = @"";
        for (int i=0; i < categoryMain.productAdvertiseTypes.count; i++) {
            
            AdvertiseType *adName = [categoryMain.productAdvertiseTypes objectAtIndex:i];
            
            if ([adName.advertiseTypeId isEqualToString:[dicRecord valueForKey:@"type_id"]]) {
                strTabName = adName.advertiseTypeName;
                break;
            }
            if (i == categoryMain.productAdvertiseTypes.count-1 && strTabName.length == 0) {
                
                adName = [categoryMain.productAdvertiseTypes objectAtIndex:0];
                strTabName = adName.advertiseTypeName;
            }
        }
        
        if([LanguageManager currentLanguageIndex] == 1){
            [cell.lblSerchTitle setTextAlignment:NSTextAlignmentRight];
            [cell.lblSearchCategory setTextAlignment:NSTextAlignmentRight];
            [cell.imgArrow setImage:[UIImage imageNamed:@"next_ar.png"]];
        }else{
            [cell.imgArrow setImage:[UIImage imageNamed:@"next_en.png"]];
            [cell.lblSerchTitle setTextAlignment:NSTextAlignmentLeft];
            [cell.lblSearchCategory setTextAlignment:NSTextAlignmentLeft];
        }
        
        cell.lblSearchCategory.text = [NSString stringWithFormat:NSLocalizedString(@"SearchSubTitle", nil), [dicRecord valueForKey:@"category_name"],[dicRecord valueForKey:@"sub_category_name"],strTabName];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [txtSearch resignFirstResponder];
    
    if (indexPath.row == 0) {
        
        if ([_delegate respondsToSelector:@selector(refreshProductListPage:withCategoryId:withCategoryId:withTypeId:)]) {
            
            [_delegate refreshProductListPage:txtSearch.text withCategoryId:@"" withCategoryId:@"" withTypeId:@""];
            [self.navigationController popViewControllerAnimated:YES];
        }else{
            
            [self openProductListPage:txtSearch.text withCategoryId:@"" withCategoryId:@"" withTypeId:@"" withSubCatName:@""];
        }
    }else{
        
        self.title = NSLocalizedString(@"BACK", nil);
        
        NSDictionary *dicRecord = [arrTrendingSearch objectAtIndex:indexPath.row-1];
//        dicCurrentRecord = dicRecord;
        /*if (lblTrendingSearch.hidden) {
         NSString *strTypeId = [NSString stringWithFormat:@"%@",[dicRecord valueForKey:@"type_id"]];
         NSString *strSubCategoryId = [NSString stringWithFormat:@"%@",[dicRecord valueForKey:@"sub_category_id"]];
         NSString *strCategoryId = [NSString stringWithFormat:@"%@",[dicRecord valueForKey:@"category_id"]];
         
         if ([_delegate respondsToSelector:@selector(refreshProductListPage:withCategoryId:withCategoryId:withTypeId:)]) {
         [_delegate refreshProductListPage:[dicRecord valueForKey:@"title"] withCategoryId:strSubCategoryId withCategoryId:strCategoryId withTypeId:strTypeId];
         [self.navigationController popViewControllerAnimated:YES];
         }else{
         [self openProductListPage:[dicRecord valueForKey:@"title"] withCategoryId:strSubCategoryId withCategoryId:strCategoryId withTypeId:strTypeId withSubCatName:[dicRecord valueForKey:@"sub_category_name"]];
         }
         
         }else{*/
        
        
//        [self getTabBar];
        
        [self getSubCategoriesArray:dicRecord];

        NSString *strTabIndex = @"0";
        for (int i=0; i < categoryMain.productAdvertiseTypes.count; i++) {
            
            AdvertiseType *adName = [categoryMain.productAdvertiseTypes objectAtIndex:i];
            
            if ([adName.advertiseTypeId isEqualToString:[dicRecord valueForKey:@"type_id"]]) {
                strTabIndex = [NSString stringWithFormat:@"%d", i];
            }
        }
        
        if ([_delegate respondsToSelector:@selector(refreshProductListPage:withCategoryId:withCategoryId:withTypeId:)]) {
            //        [_delegate refreshProductListPage:[dicRecord valueForKey:@"title"] withCategoryId:[dicRecord valueForKey:@"sub_category_id"] withCategoryId:[dicRecord valueForKey:@"category_id"] withTypeId:[dicRecord valueForKey:@"type_id"]];
            
            /*[_delegate refreshProductListPage:[dicRecord valueForKey:@"title"] withCategoryId:[dicRecord valueForKey:@"sub_category_id"] withCategoryId:[dicRecord valueForKey:@"category_id"] withTypeId:strTabIndex];
            [self.navigationController popViewControllerAnimated:YES];*/
            [self openProductListPage:[dicRecord valueForKey:@"title"] withCategoryId:[dicRecord valueForKey:@"sub_category_id"] withCategoryId:[dicRecord valueForKey:@"category_id"] withTypeId:strTabIndex withSubCatName:[dicRecord valueForKey:@"sub_category_name"]];
        }else{
            //[self openProductListPage:[dicRecord valueForKey:@"title"] withCategoryId:[dicRecord valueForKey:@"sub_category_id"] withCategoryId:[dicRecord valueForKey:@"category_id"] withTypeId:[dicRecord valueForKey:@"type_id"] withSubCatName:[dicRecord valueForKey:@"sub_category_name"]];
            
            [self openProductListPage:[dicRecord valueForKey:@"title"] withCategoryId:[dicRecord valueForKey:@"sub_category_id"] withCategoryId:[dicRecord valueForKey:@"category_id"] withTypeId:strTabIndex withSubCatName:[dicRecord valueForKey:@"sub_category_name"]];
        }
    }
}

- (void)openProductListPage:(NSString *)strTitle withCategoryId:(NSString *)subCategoryId withCategoryId:(NSString *)strCategoryId withTypeId:(NSString *)strTypeId withSubCatName:(NSString *)strSubCatName
{
    [SharedViewManager showCategoryProductsWithSearch:self
                                         andProductId:strCategoryId
                                         andSearchStr:strTitle
                                     andSubCategoryId:subCategoryId
                                   andSubCategoryName:strSubCatName
                                          andSortById:@""
                                        andSortByName:@""
                                       andsortByIndex:-1
                                        andTabBarName:  subCategoryId.length == 0 ? [self getTabBarSearchAllCategories:strTypeId] : categoryMain.productAdvertiseTypes
                                andDefaultTabSelected:[strTypeId intValue]
     ];
}

-(NSMutableArray*)getTabBarSearchAllCategories:(NSString *)strTypeId
{
    AdvertiseType *allTypes=[[AdvertiseType alloc]init];
    allTypes.advertiseTypeId=strTypeId;
    allTypes.advertiseTypeName=NSLocalizedString(@"ALL_TYPES", nil);
    allTypes.advertiseTypeDefaultView=@"grid";
    
    return  [NSMutableArray arrayWithObject:allTypes];
}

- (void)getTabBar
{
    NSData *arrayData = [[NSUserDefaults standardUserDefaults]objectForKey:[SharedData CATEGORY_ARRAY_FILE_KEY]];
    arrCategories = [NSKeyedUnarchiver unarchiveObjectWithData:arrayData];
//    [self getSubCategoriesArray:arrCategories];
}

#pragma mark - SetInitiate Data
-(void)getSubCategoriesArray: (NSDictionary*)dicCurrentRecord
{
    categoryMain = [[CategoryObject alloc]init];
    for (CategoryObject *category in arrCategories) {
        if([category.categoryID isEqualToString:[dicCurrentRecord valueForKey:@"category_id"]]){
            categoryMain = category;
            break;
        }
    }
    
}

#pragma mark - Scroll View Delegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    if (scrollView == tblSearchResult) {
        [self.view endEditing:true];
    }
}

@end
