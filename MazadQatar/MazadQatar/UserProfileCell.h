//
//  UserProfileCell.h
//  Mzad Qatar
//
//  Created by Mohammed AbdulFattah on 1/26/15.
//  Copyright (c) 2015 Mohammed abdulfattah attia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProductObject.h"

@interface UserProfileCell : UITableViewCell {
  ProductObject *productObject;
}
@property(strong, nonatomic) IBOutlet UIImageView *userImageView;
@property(strong, nonatomic) IBOutlet UILabel *userName;
@property(strong, nonatomic) IBOutlet UITextView *userNumber;
@property(strong, nonatomic) IBOutlet UILabel *userNumberOfAds;
@property(strong, nonatomic) IBOutlet UILabel *userDescription;
@property(strong, nonatomic) IBOutlet UIView *background;

- (IBAction)moreAdsClicked:(id)sender;

- (void)setUserProfileInfoUi:(ProductObject *)productObjectParam;

@end
