//
//  CategoryChooser.h
//  MazadQatar
//
//  Created by samo on 5/13/13.
//  Copyright (c) 2013 Mohammed abdulfattah attia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SharedData.h"
#import "ProductObject.h"
#import "GAITrackedViewController.h"

// Delgate
@class CategoryChooser;
@protocol CategoryChooserDelegate <NSObject>
@required
//- (void)categoryChoosed:(int)index andCategoryId:(NSString *)categoryId;
- (void)categoryChoosed:(int)index andCategoryId:(NSString *)categoryId withArray:(NSMutableArray *)arrCategory;
@required
@end

@interface CategoryChooser
    : GAITrackedViewController <UITableViewDataSource, UITableViewDelegate> {
        NSMutableArray *categories;
}
@property(weak) NSString *selectedCategoryId;
@property(nonatomic,assign) BOOL isFromAddAdvts;
@property(nonatomic, strong) id<CategoryChooserDelegate> delegate;

@end
