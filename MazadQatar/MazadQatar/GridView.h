//
//  GridView.h
//  MazadQatar
//
//  Created by samo on 3/8/13.
//  Copyright (c) 2013 Mohammed abdulfattah attia. All rights reserved.
//
#import <UIKit/UIKit.h>
#import "ProductObject.h"
#import "UploadingProductsManager.h"
#import "SharedGraphicInfo.h"
#import "UploadingListenerManager.h"
#import "UserSetting.h"
#import "SharedViewManager.h"
#import "RegisterScreen.h"
#import "ProductGridCell.h"
#import "DialogManager.h"
#import "NotificationViewController.h"
#import "SortByChooserViewController.h"
#import "SWRevealViewController.h"

#import "GAITrackedViewController.h"

@interface GridView
    : GAITrackedViewController <UICollectionViewDataSource, UICollectionViewDelegate,
                        UICollectionViewDelegateFlowLayout,
                        ProductGridCellDelegate, UISearchBarDelegate,
                        NotificationViewControllerDelegate,SWRevealViewControllerDelegateMenu> {
  NSMutableArray *productItems;

  BOOL loadMoreWhereAvailable;

  BOOL loading;
  BOOL noMoreResultsAvail;

  BOOL isHideDeleteBtn;

  UIRefreshControl *refreshControl;

  bool isPullNewData;

  UIActivityIndicatorView *activityIndicator;

  BOOL isOverlayShown;
  UIView *overlayView;

  UILabel *collectionMiddleLabel;
  // NSMutableArray *productsToUpload;

  bool isCatgoryProductScreen;
  CGSize normalAdvertiseCellSize;

  CGFloat prevouseYPointValue;

  int ipadCellWidth;
  int iphoneCellWidth;
  ProductObject *categoryProductObject;
  BOOL isProfileOwner;
}
@property(assign) int advertiseInOneRowCount;

@property(strong) NSString *advertiseResolution;
@property(strong) NSString *advertiseInOneRowCountString;

@property(strong) NSString *searchString;

@property(assign) int sortByIndex;
@property(strong) NSString *soryById;
@property(strong) NSString *soryByName;

@property(nonatomic, strong) NSMutableArray *productItems;

@property(strong, nonatomic) IBOutlet UICollectionView *collectionViewGridView;

- (IBAction)addAdvertiseClicked:(id)sender;
- (void)loadData:(NSNumber *)isNewSession;
- (void)getNewData:(NSString *)searchStr;

//==============================animating grid
- (void)startAnimatingGrid;
- (void)stopAnimatingGrid;
- (void)addRefreshControlToGrid;

//====================================edit btn
- (IBAction)editBtnClicked:(id)sender;
@property(strong, nonatomic) IBOutlet UIButton *editBtn;
- (IBAction)doneBtnClicked:(id)sender;
@property(strong, nonatomic) IBOutlet UIButton *doneBtn;

//====================================header bar
@property(strong, nonatomic) IBOutlet UISearchBar *searchAdsBar;
@property(strong, nonatomic) IBOutlet UIButton *cancelSearchBtn;
@property(strong, nonatomic) IBOutlet UIButton *searchIconBtn;
@property(strong, nonatomic) IBOutlet UIView *viewOfSearchBar;
@property(strong, nonatomic) IBOutlet UILabel *notificationCountLbl;
@property(strong, nonatomic) IBOutlet UIButton *backBtnText;
@property(strong, nonatomic) IBOutlet UIButton *backBtnArrow;
// upper bar
@property(strong, nonatomic) IBOutlet UIButton *notificationBtn;
@property(strong, nonatomic) IBOutlet UIButton *sideMenuBtn;
@property(strong, nonatomic) IBOutlet UIButton *advertiseNowFreeBtn;
//=====================================refresh view

- (void)pullNewData;

// show overlay with animating
- (void)showOverlayWithIndicator:(BOOL)isShow;

- (void)addLabelToMiddleWithText:(NSString *)text
                inCollectionView:(BOOL)isInCollectionView;

- (void)removeLabelfromMiddleOfView;

- (void)addBackgroundListner;

- (IBAction)menuButtonPressed:(id)sender;

-(NSMutableArray*)getTabBarSearchAllCategories;


- (void)showSearchBar:(BOOL)isHideSearchBar;

- (IBAction)cancelSearchBar:(id)sender ;


/** Set width constant of Addvertise now free **/
@property (weak,nonatomic)IBOutlet NSLayoutConstraint *advertiseBtnWidthConstant;
@end
