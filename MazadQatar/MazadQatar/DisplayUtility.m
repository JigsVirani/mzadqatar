//
//  DisplayUtility.m
//  MazadQatar
//
//  Created by samo on 3/7/13.
//  Copyright (c) 2013 Mohammed abdulfattah attia. All rights reserved.
//

#import "DisplayUtility.h"

@implementation DisplayUtility

+ (BOOL)isPad {
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
    {
        return YES;
    }
    else
    {
        return NO;
    };
}
+ (CGFloat) changeLaoutAsPerHeight
{
    return ([UIScreen mainScreen].bounds.size.width/320);
}

+ (BOOL)hasRetinaDisplay {
    if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)] &&
        [[UIScreen mainScreen] scale] == 2)
        return YES;
    else
        return NO;
}

+ (bool)isLandscape {
    if (UIInterfaceOrientationIsLandscape(
                                          [UIApplication sharedApplication].statusBarOrientation)) {
        return YES;
    }
    return NO;
}

+ (CGSize)getScreenSize {
    //    CGRect screenBounds = [[UIScreen mainScreen] bounds];
    //    // CGFloat screenScale = [[UIScreen mainScreen] scale];
    //    CGSize screenSize = CGSizeMake(screenBounds.size.width ,
    //    screenBounds.size.height );
    //
    //    return screenSize;
    CGSize size = [UIScreen mainScreen].bounds.size;
    UIApplication *application = [UIApplication sharedApplication];
    if (UIInterfaceOrientationIsLandscape(
                                          [UIApplication sharedApplication].statusBarOrientation)) {
        size = CGSizeMake(size.height, size.width);
    }
    if (application.statusBarHidden == NO) {
        size.height -= MIN(application.statusBarFrame.size.width,
                           application.statusBarFrame.size.height);
    }
    return size;
}

+ (CGSize)getScreenSize:(bool)isPortrait {
    CGSize size = [UIScreen mainScreen].bounds.size;
    UIApplication *application = [UIApplication sharedApplication];
    if (isPortrait == false) {
        size = CGSizeMake(size.height, size.width);
    }
    if (application.statusBarHidden == NO) {
        size.height -= MIN(application.statusBarFrame.size.width,
                           application.statusBarFrame.size.height);
    }
    return size;
}
@end
