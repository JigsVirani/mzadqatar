//
//  CustomTableViewCell.m
//  Mzad Qatar
//
//  Created by iOS Developer on 23/09/2014.
//  Copyright (c) 2014 Mohammed abdulfattah attia. All rights reserved.
//

#import "CustomTableViewCell.h"
#import "DisplayUtility.h"

@implementation CustomTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
  // Initialization code
    if ([DisplayUtility isPad]) {
        [_title setFont:[UIFont boldSystemFontOfSize:24.0]];
        [_qr setFont:[UIFont systemFontOfSize:22.0]];
        [_desc setFont:[UIFont systemFontOfSize:18.0]];
        _likes.titleLabel.font = [UIFont systemFontOfSize:18.0];
        _comments.titleLabel.font = [UIFont systemFontOfSize:18.0];
    }
}

- (id)initWithStyle:(UITableViewCellStyle)style
    reuseIdentifier:(NSString *)reuseIdentifier {
  self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
  if (self) {
    // Initialization code
  }
  return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
  [super setSelected:selected animated:animated];

  // Configure the view for the selected state
}

@end
