//
//  LanguageChooserViewController.h
//  Mzad Qatar
//
//  Created by samo on 12/19/13.
//  Copyright (c) 2013 Mohammed abdulfattah attia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GoogleAnalyticsTableViewController.h"

@interface LanguageCell:UITableViewCell

@property (weak, nonatomic) IBOutlet UIStackView *stackViewEng;
@property (weak, nonatomic) IBOutlet UIButton *btnEnglishApp;
@property (weak, nonatomic) IBOutlet UIButton *btnArabicApp;

@property (weak, nonatomic) IBOutlet UIStackView *stackViewAr;
@property (weak, nonatomic) IBOutlet UIButton *btnEnglishAppAr;
@property (weak, nonatomic) IBOutlet UIButton *btnArabicAppAr;

@property (weak, nonatomic) IBOutlet UIButton *btnEnglishAdv;
@property (weak, nonatomic) IBOutlet UIButton *btnArabicAdv;
@property (weak, nonatomic) IBOutlet UIButton *btnEnglishArabic;

@property (weak, nonatomic) IBOutlet UIButton *btnContinue;
@end


@class ViewAdvertiseLanguageChooserViewController;
@protocol ChoseViewAdvertiseLanguageChooserDelegate <NSObject>
@required
- (void)ViewAdvertiselanguageChanged:(int)index
                    withLanguageName:(NSString *)languageName;
@required
@end

@interface ViewAdvertiseLanguageChooserViewController
    : GoogleAnalyticsTableViewController

@property(assign) int selectedIndex;
@property(assign) BOOL isHideBackButton;
@property(nonatomic, strong)
    id<ChoseViewAdvertiseLanguageChooserDelegate> delegate;
@property(strong, nonatomic) IBOutlet UITableView *selectLanguageTable;

@end
