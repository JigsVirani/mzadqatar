//
//  NewDescriptionTableViewCell.m
//  Mzad Qatar
//
//  Created by Paresh Kacha on 05/11/15.
//  Copyright © 2015 Mohammed abdulfattah attia. All rights reserved.
//
#import "DisplayUtility.h"
#import "UserSetting.h"
#import "ServerManager.h"
#import "MBProgressHUD.h"
#import "NewDescriptionTableViewCell.h"

NSString *NewrequestAddLike = @"requestAddLike";

@implementation NewDescriptionTableViewCell
@synthesize likeCoundUpdate;


- (void)awakeFromNib
{
    [super awakeFromNib];
    if ([DisplayUtility isPad]) {
        self.likeWidthConstant.constant = 120.0;
        self.likeHeightConstant.constant = 50.0;
        self.sharelbl.font = [UIFont systemFontOfSize:20.0];
        self.descLbl.font = [UIFont systemFontOfSize:22.0];
        self.likeBtn.titleLabel.font = [UIFont systemFontOfSize:20.0];
    }
}
- (void)requestFinished:(ServerManagerResult *)serverManagerResult
            ofRequestId:(NSString *)requestId
    withNumberOfRequest:(int)numberOfRequest {
    
    if (requestId == NewrequestAddLike) {
        [self.likeIndicator stopAnimating];
         self.likeBtn.hidden = NO;
        [self sendSubviewToBack:self.likeIndicator];
        
        if ([serverManagerResult.opertationResult
             isEqualToString:[ServerManager ERROR_IN_CONNECTION_OPERATION]]) {
            
            [DialogManager showErrorInConnection];
            
            return;
        }
        else if ([serverManagerResult.opertationResult
             isEqualToString:[ServerManager ERROR_USER_BLOCKED_BY_ANOTHER_USER]]) {
            
            [DialogManager showErrorWithMessage:NSLocalizedString(@"USER_BLOCKED_YOU", nil)];
            
            return;
        }
        else if ([serverManagerResult.opertationResult
                  isEqualToString:[ServerManager ERROR_USER_BLOCKED_FROM_ADMIN]]) {
            
            [DialogManager showErrorWithMessage:NSLocalizedString(@"ERROR_USER_IS_BLOCKED_FROM_ADMIN", nil)];
            
            return;
        }
        else if ([serverManagerResult.opertationResult
             isEqualToString:[ServerManager SUCCESS_OPERATION]]) {
            if (self.likeCoundUpdate) {
                
                int aLikeCount = [productObject.productLikeCount intValue];
                aLikeCount++;
                productObject.productLikeCount = [NSString stringWithFormat:@"%d",aLikeCount];
                self.likeCoundUpdate();
            }
            self.resultTextField.text = NSLocalizedString(@"SUCCESS_ADD", nil);
        } else if ([serverManagerResult.opertationResult
                    isEqualToString:[ServerManager FAILED_OPERATION]])
            
        {
            self.resultTextField.text = NSLocalizedString(@"ERROR_ADD", nil);
        } else if ([serverManagerResult.opertationResult
                    isEqualToString:[ServerManager ALREADED_ADDED_OPERATION]])
            
        {
            self.resultTextField.text = NSLocalizedString(@"ALREADY_ADD", nil);
        }
        
        self.resultTextField.hidden = NO;
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            self.resultTextField.hidden = YES;
        });
    }
}

- (id)initWithStyle:(UITableViewCellStyle)style
    reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}
- (void)setViewDescriptionInfoUi:(ProductObject *)productObjectParam InParentView:(UIViewController *)parentViewParam {
    //[self changeAddProductsFieldsToReadingOnly];
   
    productObject = productObjectParam;
    if (productObject.isHideAddCommentsAndLikes == true) {
        
        [self.resultTextField setHidden:false];
        [self.resultTextField
         setText:NSLocalizedString(@"LIKE_NOT_ENABLED_IN_ADVERTISE", nil)];
    }

    parentView = parentViewParam;
    self.moreButton.layer.cornerRadius = 4;
    self.likeBtn.layer.cornerRadius = 4;
    self.moreButton.layer.borderWidth = 2;
    self.moreButton.layer.borderColor = [UIColor grayColor].CGColor;
    [self setStringInTextView];
}

- (void)setStringInTextView {
    // iOS6 and above : Use NSAttributedStrings
    CGFloat fontSize = 20;
    if ([DisplayUtility isPad]) {
         fontSize = 25;
    }
    
    UIFont *boldFont = [UIFont boldSystemFontOfSize:fontSize];
    UIFont *regularFont = [UIFont systemFontOfSize:fontSize];
    UIColor *foregroundColor = [UIColor whiteColor];
    
    // Create the attributes
    NSDictionary *attrs = [NSDictionary
                           dictionaryWithObjectsAndKeys:regularFont, NSFontAttributeName,
                           foregroundColor,
                           NSForegroundColorAttributeName, nil];
    NSDictionary *subAttrs = [NSDictionary
                              dictionaryWithObjectsAndKeys:boldFont, NSFontAttributeName,
                              foregroundColor,
                              NSForegroundColorAttributeName, nil];
    
    // const NSRange productDescriptionRange =
    //     NSMakeRange(0, NSLocalizedString(@"PRODUCT_Description", nil).length);
    NSMutableAttributedString *productDescriptionAttributedText =
    [[NSMutableAttributedString alloc]
     initWithString:[NSString
                     stringWithFormat:@"%@",
                     productObject.productDescription]
     attributes:attrs];
    
    productDescriptionAttributedText= [self boldTextInDescription:productDescriptionAttributedText andStyle:subAttrs];
    
    //[productDescriptionAttributedText setAttributes:subAttrs
    //                                          range:productDescriptionRange];
    // Set it in our UILabel and we are done!
    [self.descriptionField setAttributedText:productDescriptionAttributedText];
    
    if([LanguageManager currentLanguageIndex] == 1){
        self.descriptionField.textAlignment = NSTextAlignmentRight;
    }else{
        self.descriptionField.textAlignment = NSTextAlignmentLeft;
    }
    NSString* string = NSLocalizedString(@"Like_Button_Text",nil);
    NSString* string2 =[NSString stringWithFormat:@"(%@)",productObject.productLikeCount];
    [self.likeBtn setTitle:[string stringByAppendingString:string2] forState:UIControlStateNormal];
    
    if([[LanguageManager currentLanguageCode]isEqualToString:@"ar"]){
        
        self.likeBtn.titleLabel.font = [UIFont systemFontOfSize:12.0];
        [self.likeBtn setImageEdgeInsets:UIEdgeInsetsMake(0, 1, 0, 0)];
        [self.likeBtn setTitleEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 1)];
        
        [self.likeBtn.titleLabel setLineBreakMode:NSLineBreakByClipping];
    }else{
        [self.likeBtn setImageEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 3)];
        [self.likeBtn setTitleEdgeInsets:UIEdgeInsetsMake(0, 3, 0, 0)];
    }
    
}

-(NSMutableAttributedString*)boldTextInDescription:(NSMutableAttributedString*)text andStyle: (NSDictionary *)subAttrs
{
    NSRange range = [productObject.productDescriptionWithTitles rangeOfString:@"%B"];
    // [text setAttributes:subAttrs
    //                                           range:range];
    
    NSRange startRangeBoldKey=range;
    NSRange endRangeBoldKey=NSMakeRange(0, 0);
    while(range.location!=NSNotFound)
    {
        productObject.productDescriptionWithTitles=[text string];
        
        if(range.location+1>productObject.productDescriptionWithTitles.length)
        {
            break;
        }
        range = [productObject.productDescriptionWithTitles rangeOfString:@"%B" options:0 range:NSMakeRange(range.location+1,productObject.productDescriptionWithTitles.length-(range.location+1))];
        
        if(startRangeBoldKey.length==0)
        {
            startRangeBoldKey=range;
        }
        else
        {
            endRangeBoldKey=range;
        }
        
        if(startRangeBoldKey.length!=0&endRangeBoldKey.length!=0)
        {
            [text setAttributes:subAttrs
                          range:NSMakeRange(startRangeBoldKey.location+1, endRangeBoldKey.location-(startRangeBoldKey.location+1))];
            [text replaceCharactersInRange:startRangeBoldKey withString:@""];
            [text replaceCharactersInRange:NSMakeRange(endRangeBoldKey.location-2, endRangeBoldKey.length) withString:@""];
            
            range=NSMakeRange(endRangeBoldKey.location-2, endRangeBoldKey.length);
            startRangeBoldKey=NSMakeRange(0, 0);
            endRangeBoldKey=NSMakeRange(0, 0);
            
        }
        
    }
    
    return text;
}
-(BOOL)textView:(UITextView *)textView shouldInteractWithURL:(NSURL *)URL inRange:(NSRange)characterRange{
    
    if([[[NSString stringWithFormat:@"%@",URL] componentsSeparatedByString:@":"]count]>0){
        if([[[[NSString stringWithFormat:@"%@",URL] componentsSeparatedByString:@":"]objectAtIndex:0]isEqualToString:@"tel"]){
            return true;
        }
    }
    [self.delegate setUrlToDeepLink:[NSString stringWithFormat:@"%@",URL]];
    return false;
}
- (IBAction)likeBtnClicked:(id)sender {
    self.likeBtn.hidden = YES;
    if ([UserSetting isUserRegistered] == false) {
        [SharedViewManager showLoginScreen:parentView];
        
        self.resultTextField.hidden = NO;
        
        self.resultTextField.text =
        NSLocalizedString(@"ERROR_TRY_AGAIN_AFTER_REGISTER", nil);
    } else {
        self.resultTextField.hidden = YES;
        
        [self.likeIndicator startAnimating];
        [self bringSubviewToFront:self.likeIndicator];
        
        [ServerManager addLikeToProduct:productObject
                            AndDelegate:self
                          withRequestId:NewrequestAddLike];
    }
}
-(UIImage*)downloadShareImage
{
    [MBProgressHUD showHUDAddedTo:parentView.view animated:YES];
    NSURL *url = [NSURL URLWithString:productObject.productShareImageUrl];
    UIImage *image = [UIImage imageWithData: [NSData dataWithContentsOfURL:url]];
    [MBProgressHUD hideAllHUDsForView:parentView.view animated:YES];
    
    return image;
}
- (UIImage *)writeOnImageTitle:(NSString *)title
                andDescription:(NSString *)description
                     andNumber:(NSString *)number
                       onImage:(UIImage *)image {
    CGSize size = CGSizeMake(image.size.width, image.size.height);
    UIGraphicsBeginImageContextWithOptions(size, YES, 0.0);
    
    // draw base image
    [image drawInRect:CGRectMake(0, 0, size.width, size.height)];
    
    // white background
    UIImage *whiteBackground = [UIImage imageNamed:@"share_background_White"];
    float whiteBackgroundYPostion = size.height - whiteBackground.size.height;
    [whiteBackground drawAtPoint:CGPointMake(0, whiteBackgroundYPostion)];
    
    // gray backgroud
    UIImage *grayBackground = [UIImage imageNamed:@"share_background_gray"];
    float grayBackgroundYPosition =
    size.height - whiteBackground.size.height - grayBackground.size.height;
    [grayBackground drawAtPoint:CGPointMake(0, grayBackgroundYPosition)];
    
    // mzad logo
    UIImage *mzadLogo = [UIImage imageNamed:@"share_mzad_logo"];
    float mzadLogoXposition = 10;
    float mzadLogoYposition = whiteBackgroundYPostion + 8;
    [mzadLogo drawAtPoint:CGPointMake(mzadLogoXposition, mzadLogoYposition)];
    
    // product title
    UIFont *titleFont = [UIFont systemFontOfSize:26.0];
    NSDictionary *titleAttrsDictionary = [NSDictionary
                                          dictionaryWithObjectsAndKeys:titleFont, NSFontAttributeName,
                                          [NSNumber numberWithFloat:1.0],
                                          NSBaselineOffsetAttributeName,
                                          [UIColor colorWithRed:(152 / 255.0)
                                                          green:(41 / 255.0)
                                                           blue:(92 / 255.0)
                                                          alpha:1],
                                          NSForegroundColorAttributeName, nil];
    [title drawAtPoint:CGPointMake(mzadLogoXposition + mzadLogo.size.width + 10,
                                   mzadLogoYposition + 5)
        withAttributes:titleAttrsDictionary];
    
    // product title
    UIFont *numberfont = [UIFont systemFontOfSize:24.0];
    NSDictionary *numberAttrsDictionary = [NSDictionary
                                           dictionaryWithObjectsAndKeys:numberfont, NSFontAttributeName,
                                           [NSNumber numberWithFloat:1.0],
                                           NSBaselineOffsetAttributeName,
                                           [UIColor colorWithRed:(83 / 255.0)
                                                           green:(83 / 255.0)
                                                            blue:(83 / 255.0)
                                                           alpha:1],
                                           NSForegroundColorAttributeName, nil];
    [number drawAtPoint:CGPointMake(mzadLogoXposition + mzadLogo.size.width + 10,
                                    mzadLogoYposition + 40)
         withAttributes:numberAttrsDictionary];
    
    // description
    NSMutableParagraphStyle *style = [[NSMutableParagraphStyle alloc] init];
    [style setAlignment:NSTextAlignmentCenter];
    [style setLineBreakMode:NSLineBreakByWordWrapping];
    
    UIFont *descriptionfont = [UIFont systemFontOfSize:22.0];
    NSDictionary *descriptionAttrsDictionary = [NSDictionary
                                                dictionaryWithObjectsAndKeys:descriptionfont, NSFontAttributeName,
                                                [NSNumber numberWithFloat:1.0],
                                                NSBaselineOffsetAttributeName,
                                                [UIColor colorWithRed:(255 / 255.0)
                                                                green:(255 / 255.0)
                                                                 blue:(255 / 255.0)
                                                                alpha:1],
                                                NSForegroundColorAttributeName, style,
                                                NSParagraphStyleAttributeName, nil];
    [description drawInRect:CGRectMake(0, grayBackgroundYPosition + 10,
                                       grayBackground.size.width,
                                       grayBackground.size.height - 15)
             withAttributes:descriptionAttrsDictionary];
    
    // mzad download text
    UIImage *mzadDownloadText = [UIImage imageNamed:@"Share_download_text"];
    [mzadDownloadText
     drawAtPoint:CGPointMake(whiteBackground.size.width -
                             mzadDownloadText.size.width - 10,
                             whiteBackgroundYPostion + 5)];
    
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return img;
}

#pragma mark - share btns
- (IBAction)shareOnFacebookClicked:(id)sender {
    if (productObject.productShareImageUrl == nil) {
        [DialogManager showErrorProductNotFound];
        return;
    }
    
    
    NSString *text = nil;
    
    text = [NSString stringWithFormat:@"%@\n\n%@\n\n%@\n\n%@",
            productObject.productTilte,
            productObject.productDescription,
            NSLocalizedString(@"FOR_MORE_DETAILS", nil),
            productObject.productWebsiteUrl];
    
    UIImage *image = [self
                      writeOnImageTitle:productObject.productTilte
                      andDescription:productObject.productDescription
                      andNumber:[NSString
                                 stringWithFormat:@"%@%@",
                                 productObject.productCountryCode,
                                 productObject.productUserNumber]
                      onImage:[self downloadShareImage]];
    
    socialShareUtility = [[SocialShareUtility alloc] init];
    [socialShareUtility shareOnFacebookInView:parentView
                                     withText:text
                                    withImage:image
                                      withUrl:productObject.productWebsiteUrl];
}

- (IBAction)shareOnTwitterClicked:(id)sender {
    if (productObject.productShareImageUrl == nil) {
        [DialogManager showErrorProductNotFound];
        return;
    }
    
    NSString *text = nil;
    
    if (productObject.productDescription.length > 30) {
        text = [NSString
                stringWithFormat:@"%@\n\n%@\n\n%@\n\n%@", productObject.productTilte,
                [productObject.productDescription substringToIndex:30],
                NSLocalizedString(@"FOR_MORE_DETAILS", nil),
                productObject.productWebsiteUrl];
    } else {
        text = [NSString
                stringWithFormat:@"%@\n\n%@\n\n%@\n\n%@", productObject.productTilte,
                productObject.productDescription,
                NSLocalizedString(@"FOR_MORE_DETAILS", nil),
                productObject.productWebsiteUrl];
    }
    
    //    text = [NSString
    //    stringWithFormat:@"%@\n\n%@\n\n%@\n\n%@",productObject.productTilte,productObject.productDescription,NSLocalizedString(@"FOR_MORE_DETAILS",
    //    nil),productObject.productWebsiteUrl];
    
    UIImage *image = [self
                      writeOnImageTitle:productObject.productTilte
                      andDescription:productObject.productDescription
                      andNumber:[NSString
                                 stringWithFormat:@"%@%@",
                                 productObject.productCountryCode,
                                 productObject.productUserNumber]
                      onImage:[self downloadShareImage]];
    
    socialShareUtility = [[SocialShareUtility alloc] init];
    [socialShareUtility shareOnTwitterInView:parentView
                                    withText:text
                                   withImage:image
                                     withUrl:productObject.productWebsiteUrl];
}

- (IBAction)shareOnWhatsAppClicked:(id)sender {
    NSString *text = nil;
    
    text = [NSString stringWithFormat:@"%@\n\n%@\n\n%@\n\n%@",
            productObject.productTilte,
            productObject.productDescription,
            NSLocalizedString(@"FOR_MORE_DETAILS", nil),
            productObject.productWebsiteUrl];
    
    socialShareUtility = [[SocialShareUtility alloc] init];
    [socialShareUtility shareOnWhatsAppInView:parentView withText:text];
}

- (IBAction)shareOnInstagramClicked:(id)sender {
    if (productObject.productShareImageUrl==nil) {
        [DialogManager showErrorProductNotFound];
        return;
    }
    
    UIImage *image = [self
                      writeOnImageTitle:productObject.productTilte
                      andDescription:productObject.productDescription
                      andNumber:[NSString
                                 stringWithFormat:@"%@%@",
                                 productObject.productCountryCode,
                                 productObject.productUserNumber]
                      onImage:[self downloadShareImage]];
    
    socialShareUtility= [[SocialShareUtility alloc] init];
    [socialShareUtility shareOnInstagramInView:parentView withImage:image];
}

- (IBAction)shareOnOthersAppsClicked:(UIButton*)sender {
    if (productObject.productShareImageUrl==nil) {
        [DialogManager showErrorProductNotFound];
        return;
    }
    
    NSString *text = nil;
    
    text = [NSString stringWithFormat:@"%@\n\n%@\n\n%@\n\n%@",
            productObject.productTilte,
            productObject.productDescription,
            NSLocalizedString(@"FOR_MORE_DETAILS", nil),
            productObject.productUserUrl];//productObject.productWebsiteUrl
    
    UIImage *image = [self
                      writeOnImageTitle:productObject.productTilte
                      andDescription:productObject.productDescription
                      andNumber:[NSString
                                 stringWithFormat:@"%@%@",
                                 productObject.productCountryCode,
                                 productObject.productUserNumber]
                      onImage:[self downloadShareImage]];
    
    CGRect buttonFrameInDetailView= [parentView.view convertRect:sender.frame fromView:self];
    
    socialShareUtility = [[SocialShareUtility alloc] init];
    [socialShareUtility shareOnOtherAppsInView:parentView
                            andFrameToSHowFrom:buttonFrameInDetailView
                                      withText:text
                                     withImage:image
                                       withUrl:productObject.productUserUrl];//productObject.productWebsiteUrl
}
- (IBAction)moreInfoButton:(id)sender {
}
@end
