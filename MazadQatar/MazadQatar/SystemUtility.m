//
//  SystemUtility.m
//  Mzad Qatar
//
//  Created by samo on 10/24/13.
//  Copyright (c) 2013 Mohammed abdulfattah attia. All rights reserved.
//

#import "SystemUtility.h"

@implementation SystemUtility

+ (void)callNumberWithCountryCode:(NSString *)CountryCode withNumber:(NSString *)number {
  NSString *phoneNumber = [@"tel://"
      stringByAppendingString:[NSString
                                  stringWithFormat:@"%@%@", CountryCode, number]];

  if ([[UIApplication sharedApplication]
          canOpenURL:[NSURL URLWithString:phoneNumber]]) {
    [[UIApplication sharedApplication]
        openURL:[NSURL URLWithString:phoneNumber]];
  } else {
    UIAlertView *alert = [[UIAlertView alloc]
            initWithTitle:NSLocalizedString(@"PHONE_NUMBER_TO_CALL", nil)
                  message:[NSString stringWithFormat:@"%@%@", CountryCode, number]
                 delegate:nil
        cancelButtonTitle:NSLocalizedString(@"DISMISS", nil)
        otherButtonTitles:NSLocalizedString(@"COPY_NUMBER", nil), nil];
    [alert setDelegate:self];

    [alert show];
  }
}

+ (void)smsNumberInView:(UIViewController *)currentView andCountryCode:(NSString *)CountryCode andNumber:(NSString *)number andDelegate:(id<MFMessageComposeViewControllerDelegate>)delegate {
  MFMessageComposeViewController *controller =
      [[MFMessageComposeViewController alloc] init];
  if ([MFMessageComposeViewController canSendText]) {
    //  NSString *userFullNumber=[[UserSetting
    //  getUserCountryCode]stringByAppendingString:[UserSetting getUserNumber]];

    controller.body = @"";
    controller.recipients = [NSArray arrayWithObjects:[NSString stringWithFormat:@"%@%@", CountryCode, number],nil];
    controller.messageComposeDelegate = delegate;
    [currentView presentViewController:controller animated:YES completion:nil];
  } else {
    UIAlertView *alert = [[UIAlertView alloc]
            initWithTitle:NSLocalizedString(@"PHONE_NUMBER_TO_CALL", nil)
                  message:[NSString stringWithFormat:@"%@%@", CountryCode, number]
                 delegate:nil
        cancelButtonTitle:NSLocalizedString(@"DISMISS", nil)
        otherButtonTitles:NSLocalizedString(@"COPY_NUMBER", nil), nil];
    [alert setDelegate:self];

    [alert show];
  }
}

+ (bool)checkConnection {
  Reachability *networkReachability =
      [Reachability reachabilityForInternetConnection];
  NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
  if (networkStatus == NotReachable) {
    return false;
  } else {
    return true;
  }
}

@end
