//
//  ConfirmationDialogueViewController.m
//  Mzad Qatar
//
//  Created by Waris Ali on 20/01/2015.
//  Copyright (c) 2015 Mohammed abdulfattah attia. All rights reserved.
//

#import "ConfirmationDialogueViewController.h"

@interface ConfirmationDialogueViewController ()

@end

@implementation ConfirmationDialogueViewController

- (void)viewDidLoad {
  [super viewDidLoad];
    self.screenName = @"Confirmation Dialogue";
  self.firstBtnText.text = _confirmButtonText;

  self.confirmationDescriptionLabel.text = _confirmationDescriptionText;
}

- (void)didReceiveMemoryWarning {
  [super didReceiveMemoryWarning];
  // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation

 // In a storyboard-based application, you will often want to do a little
 preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */


- (IBAction)confirmBtnClicked:(id)sender {
  [self.delegate confirmBtnClicked:self.requestId];

  [self.view removeFromSuperview];
}

- (IBAction)cancelBtnClicked:(id)sender {
  //[self.delegate cancelBtnClicked:self.requestId];

  [self.view removeFromSuperview];
}

@end
