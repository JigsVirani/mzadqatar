//
//  ViewController.m
//  MazadQatar
//
//  Created by samo on 2/28/13.
//  Copyright (c) 2013 Mohammed abdulfattah attia. All rights reserved.
//

#import "CategoryScreen.h"


#import "ServerManager.h"
#import "CategoryProducts.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "ServerManagerResult.h"
#import "ProductObject.h"
#import "SharedGraphicInfo.h"
#import "UserSetting.h"
#import "SWRevealViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "SubcategoriesController.h"
#import "MyProductsCollectionReusableView.h"

#define g_iphoneCellWidth 159
#define g_ipadCellWidth 255

@interface CategoryScreen () <UITabBarControllerDelegate>
@property(nonatomic, strong) NSString *language;
@end
@implementation CategoryScreen
- (void)viewDidLoad {
    [SharedData setIsContinueCountintRows:true];
    
    [super viewDidLoad];
    //self.language = [[[NSBundle mainBundle] preferredLocalizations] objectAtIndex:0];
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"BACK", nil) style:UIBarButtonItemStylePlain target:nil action:nil];
    
    self.language = [LanguageManager currentLanguageCode];
    
    self.screenName = @"Category Screen";
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    SWRevealViewController *revealController = [self revealViewController];
    [revealController panGestureRecognizer];
    [revealController tapGestureRecognizer];
    self.advertiseInOneRowCount = 1;
    
    UIView *headerView1;
    DFPBannerView *bannerViewGrid;
    UIActivityIndicatorView *loadingIndicatorCl = [[UIActivityIndicatorView alloc]init];
    
    if([DisplayUtility isPad]){
        [loadingIndicatorCl setFrame:CGRectMake(384, 40, 20, 20)];
        headerView1 = [[UIView alloc]initWithFrame:CGRectMake(0,-50,self.view.frame.size.width,100)];
        bannerViewGrid = [[DFPBannerView alloc]initWithFrame:CGRectMake(20,5,728,90)];
        [self.collectionViewGridView setContentInset:UIEdgeInsetsMake(50, 0, 0, 0)];
        
        if([self.language isEqualToString:[ServerManager LANGUAGE_ARABIC]]) {
            bannerViewGrid.adUnitID = @"/271990408/HomepageiOSTabletAppTopBannerArabic";
        }else{
            bannerViewGrid.adUnitID = @"/271990408/HomepageiOSTabletAppTopBannerEnglish";
        }
    }else{
        
        [loadingIndicatorCl setFrame:CGRectMake(self.view.center.x-10, 20, 20, 20)];
        headerView1 = [[UIView alloc]initWithFrame:CGRectMake(0,-5,self.view.frame.size.width,65)];
        bannerViewGrid = [[DFPBannerView alloc]initWithFrame:CGRectMake(self.view.center.x-160,3,320,50)];
        [self.collectionViewGridView setContentInset:UIEdgeInsetsMake(5, 0, 0, 0)];
        
        if([self.language isEqualToString:[ServerManager LANGUAGE_ARABIC]]) {
            bannerViewGrid.adUnitID = @"/271990408/HomepageiOSMobileAppTopBannerArabic";
        }else{
            bannerViewGrid.adUnitID = @"/271990408/HomepageiOSMobileAppTopBannerEnglish";
        }
    }
    bannerViewGrid.adSize = kGADAdSizeSmartBannerPortrait;
    bannerViewGrid.rootViewController = self;
    [bannerViewGrid loadRequest:[DFPRequest request]];
    [headerView1 addSubview:loadingIndicatorCl];
    [loadingIndicatorCl startAnimating];
    [headerView1 addSubview:bannerViewGrid];
    [self.collectionViewGridView addSubview:headerView1];
    
    SWRevealViewController *swController = (SWRevealViewController *)[AppDelegate app].window.rootViewController;
    UITabBarController *tbController = (UITabBarController *)swController.frontViewController;
    UITabBarItem *tbItem = (UITabBarItem *)[tbController.tabBar.items objectAtIndex:4];
    
    ALUserService * alUserService = [[ALUserService alloc] init];
    NSNumber * totalUnreadCount = [alUserService getTotalUnreadCount];
    if([totalUnreadCount intValue] == 0){
        tbItem.badgeValue = nil;
    }else{
        tbItem.badgeValue = [NSString stringWithFormat:@"%@", totalUnreadCount];
    }
}

- (void)pushTempTestings {
    [appDelegate application:nil didReceiveRemoteNotification:nil];
}

- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
    ////To disable swipe left right functionality
    self.revealViewController.panGestureRecognizer.enabled = NO;
    
    //if (productItems.count == 0) {
        [self startLoadingUserCategoryWithRequestId:requestGetCategory
                                    andIsNewSession:YES];
    //}
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *currentAppVersion = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
    NSString *currentBuildVersion = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleVersion"];
    NSString *currentAppVersionStore = [defaults objectForKey:@"appVersion"];
    
    if(currentAppVersion.floatValue > currentAppVersionStore.floatValue){
        
        [defaults setObject:currentAppVersion forKey:@"appVersion"];
        [defaults setObject:currentBuildVersion forKey:@"appBuildVersion"];
        [defaults synchronize];
        
        ViewAdvertiseLanguageChooserViewController *controller = [[SharedData getCurrentStoryBoard]
                                                                  instantiateViewControllerWithIdentifier:
                                                                  @"ViewAdvertiseLanguageChooserViewController"];
        controller.isHideBackButton = YES;
        [self.navigationController pushViewController:controller animated:false];
        
    }else if(currentAppVersion == [defaults objectForKey:@"appVersion"]){
        
        if(currentBuildVersion > [defaults objectForKey:@"appBuildVersion"]){
            
            [defaults setObject:currentAppVersion forKey:@"appVersion"];
            [defaults setObject:currentBuildVersion forKey:@"appBuildVersion"];
            [defaults synchronize];
            ViewAdvertiseLanguageChooserViewController *controller = [[SharedData getCurrentStoryBoard]
                                                                      instantiateViewControllerWithIdentifier:
                                                                      @"ViewAdvertiseLanguageChooserViewController"];
            controller.isHideBackButton = YES;
            [self.navigationController pushViewController:controller animated:false];
        }
    }
    
    /*if ([UserSetting getUserFilterViewAdvertiseLanguage] == -1) {
        //[DialogManager showUserFilterViewAdvertiseLanguageDialog];
        ViewAdvertiseLanguageChooserViewController *controller = [[SharedData getCurrentStoryBoard]
                                                                  instantiateViewControllerWithIdentifier:
                                                                  @"ViewAdvertiseLanguageChooserViewController"];
        controller.isHideBackButton = YES;
        [self.navigationController pushViewController:controller animated:false];
    }*/
    
    [self.searchIconBtn bringSubviewToFront:self.view];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    
    self.tabBarController.delegate = self;
    self.tabBarController.tabBar.hidden = false;
    //[self.navigationController.tabBarController.tabBar setHidden:NO];
}

- (void)startLoadingUserCategoryWithRequestId:(NSString *)requestId
                              andIsNewSession:(bool)isNewSession {
    
    if(productItems.count==0)
    {
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [hud setLabelText:NSLocalizedString(@"LOADING_CATEGORIES", Nil)];
    }
    //[self showOverlayWithIndicator:true];
    
    loadMoreWhereAvailable = NO;
    loading = YES;
    
    // set language needed
    //NSString *language = [[[NSBundle mainBundle] preferredLocalizations] objectAtIndex:0];
    NSString *language = [LanguageManager currentLanguageCode];
    
    if ([language isEqualToString:[ServerManager LANGUAGE_ARABIC]]) {
        language = [ServerManager LANGUAGE_ARABIC];
    } else {
        language = [ServerManager LANGUAGE_ENGLISH];
    }
    
    // get the category based on the resolution
    if ([DisplayUtility isPad]) {
        int rowCount = [[UIScreen mainScreen]bounds].size.width/g_ipadCellWidth;
        self.advertiseInOneRowCountString = [NSString stringWithFormat:@"%d",rowCount];
        [ServerManager
         getCategoryItemsInLanguage:language
         andResolution:[ServerManager RESOLUTION_MEDIUM]
         withDelegate:self
         withCategoryId:@""
         withRequestId:requestGetCategory
         countOfRows:self.advertiseInOneRowCountString
         withIsOpenedFromAddAdvertise:[SharedData notOpenedFromAddAdvdrtise]];
    } else {
        int rowCount = [[UIScreen mainScreen]bounds].size.width/g_iphoneCellWidth;
        self.advertiseInOneRowCountString = [NSString stringWithFormat:@"%d",rowCount];
        [ServerManager
         getCategoryItemsInLanguage:language
         andResolution:[ServerManager RESOLUTION_LOW]
         withDelegate:self
         withCategoryId:@""
         withRequestId:requestGetCategory
         countOfRows:self.advertiseInOneRowCountString
         withIsOpenedFromAddAdvertise:[SharedData notOpenedFromAddAdvdrtise]];
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    [super prepareForSegue:segue sender:sender];
    
    if ([[segue identifier] isEqualToString:@"ImageViewClicked"]) {
        // get the index clicked
        
        UICollectionViewCell *cell = (UICollectionViewCell *)sender;
        NSIndexPath *indexPath = [self.collectionViewGridView indexPathForCell:cell];
        
        ProductObject *productObjectSelected = [productItems objectAtIndex:indexPath.row];
        // open category products and set the index
        SubcategoriesController *SubcategoriesVC = [segue destinationViewController];
        SubcategoriesVC.strCategoryID = productObjectSelected.productId;
        SubcategoriesVC.strCategoryName = productObjectSelected.productTilte;
        SubcategoriesVC.hidesBottomBarWhenPushed = true;
        //[self.navigationController.tabBarController.tabBar setHidden:YES];
        
        /*CategoryProducts *categoryProducts = [segue destinationViewController];
        [categoryProducts setCategoryProductsCategoryId:productObjectSelected.productId andSubcategoryId:@"0" andSubcategoryName:@""];
        categoryProducts.ƒ = productObjectSelected.productAdvertiseTypes;*/
    }
}

- (void)requestFinished:(ServerManagerResult *)serverManagerResult
            ofRequestId:(NSString *)requestId
    withNumberOfRequest:(int)numberOfRequest {
    if ([requestId isEqual:requestGetCategory]) {
        loading = NO;
        
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        
       // [self showOverlayWithIndicator:false];
        
        if ([serverManagerResult.opertationResult
             isEqualToString:[ServerManager ERROR_IN_CONNECTION_OPERATION]]) {
            [DialogManager showErrorInConnection];
        }
        
        if ([serverManagerResult.opertationResult
             isEqualToString:[ServerManager SUCCESS_OPERATION]]) {
            //[self.view setUserInteractionEnabled:true];
            
            [UserSetting
             saveAllLastUpdateTimeFromServer:serverManagerResult
             .lastUpdateTimeForCategories
             andSoryByUpdateTime:serverManagerResult
             .lastUpdateTimeForSortBy
             andCitiesUpdateTime:serverManagerResult
             .lastUpdateTimeForCities];
            
            if(productItems.count==0)
            {
                [[NotificationViewController getInstance] refreshNowNotificationCount];
            }
            
            productItems = serverManagerResult.jsonArray;
            
            [SharedData setCategories:productItems];
            if ([UserSetting isUserRegistered]) {
                [ServerManager getUserProductsWithDelegate:self withUserProductsPageNumber:0 withUserProductsNumberOfPages:0 withRequestId:@"getUserProfile" withUpdatTime:[SharedData getUserProductsLastUpdateTime] byLanguage: [UserSetting getAdvertiseLanguageServerParam: [UserSetting getUserFilterViewAdvertiseLanguage]] isAdSupported:@"true" countOfRows:@"0" withAdvertiseResolution:@"" andVisitorCountryCode:[UserSetting getUserCountryCode] andVisitorNumber:[UserSetting getUserNumber]];
            }
            [self.collectionViewGridView reloadData];
        } else if ([serverManagerResult.opertationResult
                    isEqualToString:[ServerManager FAILED_OPERATION]])
            
        {
           //Comment [DialogManager showErrorInServer];
            [DialogManager showErrorWithMessage:serverManagerResult.opertationMsg];
        }
        

    }
}

- (void)getNewData:(NSString *)searchStr {
    
    [SharedViewManager showCategoryProductsWithSearch:self
                                         andProductId:@"0"
                                         andSearchStr:searchStr
                                     andSubCategoryId:@""
                                   andSubCategoryName:@""
                                          andSortById:@""
                                        andSortByName:@""
                                       andsortByIndex:-1
                                        andTabBarName:  [self getTabBarSearchAllCategories]
                                andDefaultTabSelected:0
     ];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // [SharedData clearSdImageMemory];
}


- (void)viewDidUnload {
    
    [super viewDidUnload];
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    ProductObject *productObject = [productItems objectAtIndex:indexPath.item];
    
    if (productObject.isAd) {
        if ([DisplayUtility isPad]) {
            return CGSizeMake(768, 275);
        } else {
            return CGSizeMake(320, 275);
        }
    } else {
        if ([DisplayUtility isPad]) {
            return CGSizeMake(255, 256);
        } else {
            //return CGSizeMake([UIScreen mainScreen].bounds.size.width/2.0, 150);
            CGFloat size = ([UIScreen mainScreen].bounds.size.width-2)/2.0;
            return CGSizeMake(size, size);
        }
    }
}
/*- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    
    if (kind == UICollectionElementKindSectionHeader) {
        
        MyProductsCollectionReusableView *headerView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader
                                                                                          withReuseIdentifier:@"HeaderView"
                                                                                                 forIndexPath:indexPath];
        [headerView.loadingIndicator startAnimating];
        if ([DisplayUtility isPad]){
            
            if([self.language isEqualToString:[ServerManager LANGUAGE_ARABIC]]) {
                headerView.bannerView.adUnitID = @"/271990408/HomepageiOSTabletAppTopBannerArabic";
            }else{
                headerView.bannerView.adUnitID = @"/271990408/HomepageiOSTabletAppTopBannerEnglish";
            }
        }else{
            if([self.language isEqualToString:[ServerManager LANGUAGE_ARABIC]]) {
                headerView.bannerView.adUnitID = @"/271990408/HomepageiOSMobileAppTopBannerArabic";
            }else{
                headerView.bannerView.adUnitID = @"/271990408/HomepageiOSMobileAppTopBannerEnglish";
            }
        }
        //headerView.bannerView.frame = CGRectMake(self.view.center.x-160, 5, 320, 50);
        headerView.bannerView.adSize = kGADAdSizeSmartBannerPortrait;
        headerView.bannerView.rootViewController = self;
        [headerView.bannerView loadRequest:[DFPRequest request]];
        return headerView;
    }
    return nil;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section {
    
    if ([DisplayUtility isPad]){
        return CGSizeMake(768,95);
    }else{
        return CGSizeMake([UIScreen mainScreen].bounds.size.width,55);
    }
}*/

//- (void)alertView:(UIAlertView *)alertView
//clickedButtonAtIndex:(NSInteger)buttonIndex
//{
//    if(buttonIndex>0)
//    {
//        [UserSetting saveFilterViewAdvertiseLanguage:(int)buttonIndex-1];
//    }
//    else
//    {
//        [UserSetting saveFilterViewAdvertiseLanguage:0];
//    }
//}

#pragma mark - IBActions

- (IBAction)btnSearchAction:(id)sender
{
    [self performSegueWithIdentifier:@"segueSearchController" sender:nil];
}

#pragma mark - MenuTableView Delegate

- (void)requestBack:(NSString *)categoryID {
    CategoryProducts *catProduct = (CategoryProducts *)[self.storyboard
                                                        instantiateViewControllerWithIdentifier:@"CategoryProducts"];
    
    [catProduct setCategoryProductsCategoryId:categoryID
                             andSubcategoryId:@"0"
                           andSubcategoryName:@""];
    UINavigationController *navController = [[UINavigationController alloc]
                                              initWithRootViewController:catProduct];
    [self.revealViewController rightRevealToggleAnimated:YES];
    [self.revealViewController setFrontViewController:navController animated:YES];
}

#pragma mark - UITabBarControllerDelegate

- (void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController {
    
    if (tabBarController.selectedIndex == 1) {
        [self cancelSearchBar:nil];
    }
    else if(tabBarController.selectedIndex == 4){
        
        if ([UserSetting isUserRegistered] == false) {
            [self performSelector:@selector(openRegisterScreen) withObject:nil afterDelay:0.1f];
        }
    }
}

-(void) openRegisterScreen{
    
    /*AppDelegate *appdel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    SWRevealViewController *swController = (SWRevealViewController *)appdel.window.rootViewController;
    UITabBarController *tbController = (UITabBarController *)swController.frontViewController;
    UINavigationController *nvController = (UINavigationController *)[tbController.viewControllers objectAtIndex:tbController.selectedIndex];
    RegisterScreen *controller = [[SharedData getCurrentStoryBoard] instantiateViewControllerWithIdentifier:@"RegisterScreen"];
    [controller setSuccessScreenSegueName:nil];
    [nvController pushViewController:controller animated:true];*/
    
    UINavigationController *navcon = (UINavigationController*)self.tabBarController.selectedViewController;
    UIViewController *currentViewController = [navcon.viewControllers lastObject];
    Class currentVCClass = [currentViewController class];
    
    if (![NSStringFromClass(currentVCClass) isEqualToString:@"RegisterScreen"]) {
        
        RegisterScreen *controller = [[SharedData getCurrentStoryBoard] instantiateViewControllerWithIdentifier:@"RegisterScreen"];
        [navcon pushViewController:controller animated:NO];
    }
}

@end
