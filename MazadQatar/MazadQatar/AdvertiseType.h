//
//  CategoryTabsType.h
//  Mzad Qatar
//
//  Created by samo on 9/3/14.
//  Copyright (c) 2014 Mohammed abdulfattah attia. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AdvertiseType : NSObject <NSCoding>

@property(nonatomic, strong) NSString *advertiseTypeId;
@property(nonatomic, strong) NSString *advertiseTypeName;
@property(nonatomic, strong) NSString *advertiseTypeDefaultView;

@end
