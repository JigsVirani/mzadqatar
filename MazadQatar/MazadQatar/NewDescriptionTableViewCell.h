//
//  NewDescriptionTableViewCell.h
//  Mzad Qatar
//
//  Created by Paresh Kacha on 05/11/15.
//  Copyright © 2015 Mohammed abdulfattah attia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProductObject.h"
#import "ServerManager.h"
#import "SharedViewManager.h"
#import "SocialShareUtility.h"

@class NewDescriptionTableViewCell;
@protocol ProductNewDescriptionTableViewCell <NSObject>
@optional
- (void)setUrlToDeepLink:(NSString *)strURL;
@optional
@end


@interface NewDescriptionTableViewCell : UITableViewCell<ServerManagerResponseDelegate>
{
    ProductObject *productObject;
    UIViewController *parentView;
    SocialShareUtility *socialShareUtility;
}
@property id<ProductNewDescriptionTableViewCell> delegate;

@property (strong, nonatomic) IBOutlet UITextView * descriptionField;
@property (strong, nonatomic) IBOutlet UIButton * moreButton;
@property(strong, nonatomic) IBOutlet UIButton *likeBtn;
- (IBAction)likeBtnClicked:(id)sender;
@property(strong, nonatomic) IBOutlet UIActivityIndicatorView *likeIndicator;
@property(strong, nonatomic) IBOutlet UITextField *resultTextField;
- (IBAction)moreInfoButton:(id)sender;
- (void)setViewDescriptionInfoUi:(ProductObject *)productObjectParam InParentView:(UIViewController *)parentViewParam;
@property(nonatomic,copy) void(^likeCoundUpdate)(void);



@property (weak, nonatomic) IBOutlet NSLayoutConstraint *likeWidthConstant,*likeHeightConstant;
@property (weak, nonatomic) IBOutlet UILabel *sharelbl,*descLbl;
@end
