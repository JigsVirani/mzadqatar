//
//  CommentInfoTableCell.h
//  MazadQatar
//
//  Created by samo on 4/17/13.
//  Copyright (c) 2013 Mohammed abdulfattah attia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProductCommentObject.h"
#import "SystemUtility.h"
#import "ServerManager.h"
#import "DialogManager.h"
#import "ReportItemViewController.h"
#import "ConfirmationDialogueViewController.h"
#import "ProductObject.h"
#import "UserProducts.h"
// Delgate
@class ProductDetailsViewController;
@class NewCommentInfoTableCell;
@protocol NewCommentInfoTableCellDelegate <NSObject>
@required
- (void)deleteCommentCLicked:(UIActivityIndicatorView *)indicator
          andResultTextField:(UITextField *)resultTextFieldParam;
- (void)
moreClickedFinishedWithLoadingIndicator:(UIActivityIndicatorView *)indicator
andResultTextField:(UITextField *)resultTextFieldParam andButton:(UIButton *)moreButton;
@required
@end

@interface NewCommentInfoTableCell
    : UITableViewCell <ServerManagerResponseDelegate,
                       ReportItemViewControllerDelegate,
                       ConfirmationDialogueViewControllerDelegate> {
  ProductCommentObject *productCommentObject;
  ProductObject * productObject;
  NSString *advertiseNumber;
  NSString *productId;
  ProductDetailsViewController *currentView;
  id<MFMessageComposeViewControllerDelegate> smsDelegate;
  id<NewCommentInfoTableCellDelegate> commentInfoDelegate;
  UITableView *tableView;
                           BOOL isCommentOwner;
                           UIViewController *parentView;

}
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *moreButtonHeightCons
;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *commentLblHeightCons
;
@property(strong, nonatomic) IBOutlet UIActivityIndicatorView *loadingAvtarIndicator;
@property(strong, nonatomic) IBOutlet UIActivityIndicatorView *loadingCommentsIndicator;
@property(strong, nonatomic) IBOutlet UIActivityIndicatorView *blockIndicator;
@property(strong, nonatomic) IBOutlet UILabel *adOwnerLabel;
@property(strong, nonatomic) IBOutlet UILabel *commentsCountLabel;
@property(strong, nonatomic) IBOutlet UILabel *nameTextEdit;
@property(strong, nonatomic) IBOutlet UITextView *commentText;
@property(strong, nonatomic) IBOutlet UILabel *dateText;
@property(strong, nonatomic) IBOutlet UITextView *contactNumberLabel;
@property(strong, nonatomic) IBOutlet UILabel *lblContactNumber;
@property(strong, nonatomic) IBOutlet UIButton *moreAdBtn;
@property(strong, nonatomic) IBOutlet UITextField *lblCommentActionResult;

//===================AddOwner Constrain
@property (strong,nonatomic)IBOutlet NSLayoutConstraint *addOwnerHeight;

//===================report comment
@property(strong, nonatomic) IBOutlet UIButton *reportBtn;
@property(strong, nonatomic) IBOutlet UIImageView *avtarImage;

@property(strong, nonatomic)
    IBOutlet UIActivityIndicatorView *reportActivityIndicator;
- (IBAction)reportBtnClicked:(id)sender;

//===================delete comment
@property(strong, nonatomic) IBOutlet UIButton *deleteBtn;
@property(strong, nonatomic) IBOutlet UIButton *blockBtn;
@property(strong, nonatomic)
    IBOutlet UIActivityIndicatorView *deleteActivityIndicator;
@property(strong, nonatomic)
IBOutlet UIActivityIndicatorView *moreCommentIndicator;

- (IBAction)deleteBtnclicked:(id)sender;

- (IBAction)callBtnClicked:(UIButton *)sender;
- (IBAction)smsBtnClicked:(id)sender;
- (IBAction)moreBtnClikced:(id)sender;
- (void)setComment:(ProductCommentObject *)productCommentObjectParam productObj:(ProductObject *)productObjectfromCell andAdvertiseNumber:(NSString *)advertiseNumberParam inView:(ProductDetailsViewController *)currentViewParam andDelegate:
(id<MFMessageComposeViewControllerDelegate>)smsDelegateParam andCommentInfoDelegae:(id<NewCommentInfoTableCellDelegate>)commentInfoDelegateParam inProductId:(NSString *)productIdParam inTableView:(UITableView *)tableViewParam isAdOwnerOrCommentOwner:(BOOL)isAdOwnerOrCommentOwner;
- (void)setIsLoadingCommentView:(BOOL)isLoading;
- (void)setIsNoCommentLblHidden:(BOOL)isHidden;
-(UIView *)roundCornersOnView:(UIView *)view onTopLeft:(BOOL)tl topRight:(BOOL)tr bottomLeft:(BOOL)bl bottomRight:(BOOL)br radius:(float)radius;
-(void)setDelegate:(id<NewCommentInfoTableCellDelegate>)commentInfoDelegateParam;
//-(CGFloat)getCellHeight:(ProductCommentObject*)productCommentObjectParam;
@end
