//
//  ProfileScreen.m
//  Mzad Qatar
//
//  Created by dinesh on 04/12/16.
//  Copyright © 2016 Mohammed abdulfattah attia. All rights reserved.
//

#import "ProfileScreen.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface ProfileScreen ()

@end

@implementation ProfileScreen

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = NSLocalizedString(@"More information", nil);
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    NSString *imageName = [[self.userProfileProductObject productId]
                          stringByAppendingString:@".png"];
    
    self.profile_Img.layer.cornerRadius = self.profile_Img.frame.size.width/2.0;
    self.profile_Img.layer.borderWidth = 2.0;
    self.profile_Img.layer.borderColor = [UIColor darkGrayColor].CGColor;
    self.profile_Img.clipsToBounds = YES;
    [self.profile_Img sd_setImageWithURL:[NSURL URLWithString:self.userProfileProductObject
                         .productUserImageUrl]
     placeholderImage:[UIImage imageNamed:imageName]
     completed:nil];
    
    [self.numberOfAdsUserProfile setText:[self.userProfileProductObject.productUserNumberOfAds stringByReplacingOccurrencesOfString:@"ads posted" withString:@"Adv."]];
    [self.nameUserProfile setText:self.userProfileProductObject.productUserName];
    [self.description_TxtView
     setText:self.userProfileProductObject.productUserShortDescription];
    if([self.userProfileProductObject.productUserisCompany isEqualToString:@"0"]){
        self.isCompanyView.hidden = NO;
        self.isCompanyImage.hidden = NO;
    }else{
        self.strikeConstantWidth.constant = self.strikeConstantWidth.constant/2.0;
        self.isCompanyView.hidden = YES;
        self.isCompanyImage.hidden = YES;
    }
    // Do any additional setup after loading the view.
    //[self.navigationController.navigationBar.topItem setTitle:NSLocalizedString(@"BACK", nil)];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
