//
//  ProfileScreen.h
//  Mzad Qatar
//
//  Created by dinesh on 04/12/16.
//  Copyright © 2016 Mohammed abdulfattah attia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProductObject.h"

@interface ProfileScreen : UIViewController
@property (weak,nonatomic)IBOutlet NSLayoutConstraint *strikeConstantWidth;;
@property (weak,nonatomic)IBOutlet UIImageView *isCompanyImage;
@property (weak,nonatomic)IBOutlet UIView *isCompanyView;
@property (weak,nonatomic)IBOutlet UIImageView *profile_Img;
@property (weak,nonatomic)IBOutlet UILabel *numberOfAdsUserProfile;
@property(strong, nonatomic) IBOutlet UILabel *nameUserProfile;
@property (weak,nonatomic)IBOutlet UITextView *description_TxtView;
@property(strong, nonatomic) ProductObject *userProfileProductObject;
@end
