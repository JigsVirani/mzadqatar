//
//  AboutScreen.m
//  MazadQatar
//
//  Created by samo on 5/24/13.
//  Copyright (c) 2013 Mohammed abdulfattah attia. All rights reserved.
//

#import "AboutScreen.h"
#import "SystemUtility.h"
#import "DialogManager.h"

@interface AboutScreen ()
{
    __weak IBOutlet UIButton *btnCallus;
    __weak IBOutlet UIButton *btnSmsus;
    __weak IBOutlet UIButton *btnEmailus;
    __weak IBOutlet UIButton *btnTerm;
    __weak IBOutlet UIButton *btnWeb;
    __weak IBOutlet UILabel *lblCopyRightText;
    
    __weak IBOutlet UILabel *lblCallUs;
    __weak IBOutlet UILabel *lblSmsus;
    __weak IBOutlet UILabel *lblEmailus;
    __weak IBOutlet UILabel *lblTerm;
    __weak IBOutlet UILabel *lblWeb;
    
    __weak IBOutlet UIImageView *imgAppIcon;
    __weak IBOutlet UIImageView *imgCompanyLogo;
}
@end

@implementation AboutScreen

- (id)initWithStyle:(UITableViewStyle)style {
  self = [super initWithStyle:style];
  if (self) {
    // Custom initialization
  }
  return self;
}

- (void)viewDidLoad {
    
  self->screenName = @"About Screen";
  self.title = NSLocalizedString(@"About Us", nil);
    
  [super viewDidLoad];
    self.tabBarController.tabBar.hidden = YES;
    [self.tabBarController.tabBar setTranslucent:YES];
    imgAppIcon.clipsToBounds = true;
    imgAppIcon.layer.cornerRadius = 15.0;
    
    imgCompanyLogo.clipsToBounds = true;
    imgCompanyLogo.layer.cornerRadius = 5.0;
    
  // Uncomment the following line to preserve selection between presentations.
  // self.clearsSelectionOnViewWillAppear = NO;

  // Uncomment the following line to display an Edit button in the navigation
  // bar for this view controller.
  // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
}
- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    
    if([[SharedData getDeviceLanguage]isEqualToString:@"ar"]){
        
        lblCallUs.text = @"اتصل بنا‎";
        lblSmsus.text = @"آرسل لنا رساله‎";
        lblEmailus.text = @"ارسل لنا بريد‎";
        lblTerm.text = @"الشروط‎";
        lblWeb.text = @"الموقع‎";
        
//        [btnCallus setTitleEdgeInsets:UIEdgeInsetsMake(30, 0, 0, 0)];
//        [btnCallus setImage:nil forState:UIControlStateNormal];
//        [btnCallus setBackgroundImage:[UIImage imageNamed:@"CallusArabic"] forState:UIControlStateNormal];
//        [btnCallus setTitle:@"اتصل بنا‎" forState:UIControlStateNormal];
//        
//        [btnSmsus setTitleEdgeInsets:UIEdgeInsetsMake(30, 0, 0, 0)];
//        [btnSmsus setImage:nil forState:UIControlStateNormal];
//        [btnSmsus setBackgroundImage:[UIImage imageNamed:@"SMSusArabic"] forState:UIControlStateNormal];
//        [btnSmsus setTitle:@"آرسل لنا رساله‎" forState:UIControlStateNormal];
//        
//        [btnEmailus setTitleEdgeInsets:UIEdgeInsetsMake(30, 0, 0, 0)];
//        [btnEmailus setImage:nil forState:UIControlStateNormal];
//        [btnEmailus setBackgroundImage:[UIImage imageNamed:@"EmailusArabic"] forState:UIControlStateNormal];
//        [btnEmailus setTitle:@"ارسل لنا بريد‎" forState:UIControlStateNormal];
//        
//        [btnTerm setTitleEdgeInsets:UIEdgeInsetsMake(30, 0, 0, 0)];
//        [btnTerm setImage:nil forState:UIControlStateNormal];
//        [btnTerm setBackgroundImage:[UIImage imageNamed:@"TermOfUseArabic"] forState:UIControlStateNormal];
//        [btnTerm setTitle:@"الشروط‎" forState:UIControlStateNormal];
//        
//        [btnWeb setTitleEdgeInsets:UIEdgeInsetsMake(30, 0, 0, 0)];
//        [btnWeb setImage:nil forState:UIControlStateNormal];
//        [btnWeb setBackgroundImage:[UIImage imageNamed:@"websiteArabic"] forState:UIControlStateNormal];
//        [btnWeb setTitle:@"الموقع‎" forState:UIControlStateNormal];
        
        lblCopyRightText.text = @"كل الحقوق محفوظه للمزاد قطر";
    }
}
- (void) viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    self.tabBarController.tabBar.hidden = NO;
    [self.tabBarController.tabBar setTranslucent:NO];
}
- (void)didReceiveMemoryWarning {
  [super didReceiveMemoryWarning];
  // Dispose of any resources that can be recreated.
}

- (IBAction)websiteClicked:(id)sender {

  [[UIApplication sharedApplication]
      openURL:[NSURL URLWithString:@"http://mzadqatar.com/"]];
}
- (IBAction)contactUsClicked:(id)sender {
  // To address
  NSArray *toRecipents = [NSArray arrayWithObject:@"support@mzadqatar.com"];

  if ([MFMailComposeViewController canSendMail]) {

    MFMailComposeViewController *mc =
        [[MFMailComposeViewController alloc] init];
    [mc setToRecipients:toRecipents];
    mc.mailComposeDelegate = self;

    // Present mail view controller on screen
    [self presentViewController:mc animated:YES completion:NULL];
  } else {
    [DialogManager showErrorNoEmailAccountAvailable];
  }
}
- (IBAction)termsConditionAction:(id)sender
{
    [self performSegueWithIdentifier:@"Termscondition" sender:nil];
}
- (IBAction)callUsClicked:(id)sender {
  //[SystemUtility callNumber:@"33342920"];
    [SystemUtility callNumberWithCountryCode:@"00974" withNumber:@"33342920"];
}

- (IBAction)smsClicked:(id)sender {
  //[SystemUtility smsNumberInView:self andNumber:@"33342920" andDelegate:self];
    [SystemUtility smsNumberInView:self andCountryCode:@"00974" andNumber:@"33342920" andDelegate:self];
}

- (void)messageComposeViewController:
            (MFMessageComposeViewController *)controller
                 didFinishWithResult:(MessageComposeResult)result {
  [self dismissViewControllerAnimated:YES completion:Nil];
}

- (void)mailComposeController:(MFMailComposeViewController *)controller
          didFinishWithResult:(MFMailComposeResult)result
                        error:(NSError *)error {
  // Close the Mail Interface
  [self dismissViewControllerAnimated:YES completion:NULL];
}
- (void)viewDidUnload {
  [super viewDidUnload];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return self.view.frame.size.height-64;
}
@end
