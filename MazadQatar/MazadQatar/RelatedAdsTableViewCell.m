//
//  RelatedAdsTableViewCell.m
//  Mzad Qatar
//
//  Created by Paresh Kacha on 15/11/15.
//  Copyright © 2015 Mohammed abdulfattah attia. All rights reserved.
//

#import "RelatedAdsTableViewCell.h"
#import "ProductDetailsViewController.h"
@implementation RelatedAdsTableViewCell
static NSString *CellIdentifier = @"GridCell";


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}
- (void)getRight
{
    
    [self.collectionView reloadData];
    
    
}
- (void)loadCell:(UIViewController *)parentViewParam
       isAddCell:(bool)isAddCellParam
 isEditAdvertise:(BOOL)isEditAdvertiseParam
withProductObject:(ProductObject *)productObjectParam
isloadingProductDetail:(BOOL)isLoading withArray:(NSMutableArray *)adsArray
withImageViewerDelegate:(id<RelatedAdsTableViewCellDelegate>)imageViewerDelegateParam
{
    //issue to solve right to left issue in related ads
    NSString * language = [[NSLocale preferredLanguages] objectAtIndex:0];
    if([language isEqualToString:[ServerManager LANGUAGE_ARABIC]])
    {
        [self.collectionView setTransform:CGAffineTransformMakeScale(-1, 1)];
    }
    
    [self.loadingProductDetailsIndicator stopAnimating];
    parentView = parentViewParam;
    isAddCell = isAddCellParam;
    relatedViewerDelegate = imageViewerDelegateParam;
    isEditAdvertise = isEditAdvertiseParam;
    adArray = adsArray;
    [self startSettingImageViewerUi:productObjectParam
                          isLoading:isLoading
                          isAddCell:isAddCell];
}


- (void)startSettingImageViewerUi:(ProductObject *)productObjectParam
                        isLoading:(BOOL)isLoading
                        isAddCell:(bool)isAddCellParam {
    
    if (isLoading) {
        [self.loadingProductDetailsIndicator startAnimating];
    } else {
        productObject = productObjectParam;
        isAddCell = isAddCellParam;
    }
    [self.collectionView reloadData];
}
- (NSInteger)collectionView:(UICollectionView *)collectionView
     numberOfItemsInSection:(NSInteger)section
{
    return adArray.count ;
    
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView
                  cellForItemAtIndexPath:(NSIndexPath *)indexPath;
{
    ProductObject * obj = [adArray objectAtIndex:indexPath.item];
    ImageCollectionViewCell *cell =
    [collectionView dequeueReusableCellWithReuseIdentifier:CellIdentifier
                                              forIndexPath:indexPath];
    cell = [self startSettingImageFromServer:cell
                               withIndexPath:indexPath
                                   fromArray:obj.productMainImageUrl];
    cell.titleLabel.text = obj.productTilte;
    cell.priceLabel.text = [NSString stringWithFormat:@"%@ %@",obj.productPrice, NSLocalizedString(@"PRICE_QR", nil)];
    
    //issue to solve right to left issue in related ads
    NSString * language = [[NSLocale preferredLanguages] objectAtIndex:0];
    if([language isEqualToString:[ServerManager LANGUAGE_ARABIC]])
    {
        [cell.priceLabel setTransform:CGAffineTransformMakeScale(-1, 1)];
        [cell.titleLabel setTransform:CGAffineTransformMakeScale(-1, 1)];
    }
    
    return cell;
}



- (ImageCollectionViewCell *)
startSettingImageFromServer:(ImageCollectionViewCell *)cell
withIndexPath:(NSIndexPath *)indexPath
fromArray:(NSString *)imageurlStr {
    NSString *imageName =
    [[productObject productId] stringByAppendingString:@".png"];
    
    [cell setUserInteractionEnabled:false];
    [cell.loadingIndicator startAnimating];
    [cell.imageViewCell
     sd_setImageWithURL:[NSURL URLWithString:imageurlStr]
     placeholderImage:[UIImage imageNamed:imageName]
     completed:^(UIImage *image, NSError *error,
                 SDImageCacheType cacheType, NSURL *imageURL) {
         [cell setUserInteractionEnabled:true];
         [cell.loadingIndicator stopAnimating];       }];
    
    return cell;
}
- (NSUInteger)numberOfPhotosInPhotoBrowser:(MWPhotoBrowser *)photoBrowser {
    return productObject.productImagesUrls.count;
}
- (void)collectionView:(UICollectionView *)collectionView
didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    ProductObject * obj = [adArray objectAtIndex:indexPath.item];
    ProductDetailsViewController * vc = (ProductDetailsViewController *)relatedViewerDelegate;
    [vc showProductWithId:obj.productId];
}
- (id<MWPhoto>)photoBrowser:(MWPhotoBrowser *)photoBrowser
               photoAtIndex:(NSUInteger)index {
    if (index < productObject.productImagesOnDevices.count) {
        if ([[productObject.productImagesOnDevices objectAtIndex:index]
             isKindOfClass:[UIImage class]]) {
            return [MWPhoto photoWithImage:[productObject.productImagesOnDevices
                                            objectAtIndex:index]];
        } else {
            return [MWPhoto
                    photoWithURL:[NSURL URLWithString:[productObject.productImagesUrls
                                                       objectAtIndex:index]]];
        }
    } else {
        return [MWPhoto
                photoWithURL:[NSURL URLWithString:[productObject.productImagesUrls
                                                   objectAtIndex:index]]];
    }
}
- (void)didFinishPickingMediaWithInfo:(UIImage *)imageCaptured {
    if (productObject.productImagesOnDevices.count > 14) {
        [DialogManager showErrorMaxImagesLimit];
        return;
    }
    
    [relatedViewerDelegate imageAddedToProductObject:imageCaptured
                                     isEditAdvertise:isEditAdvertise];
}

- (void)SelectImageDelegatePhoto:(UIImage *)photo {
    if (productObject.productImagesOnDevices.count > 14) {
        [DialogManager showErrorMaxImagesLimit];
        return;
    }
    
    UIImage *imageCompressed = [self compreessImage:photo];
    
    UIImage *imageResized =
    [self resizeImage:imageCompressed toSize:CGSizeMake(600, 600)];
    
    NSData *imgData = UIImageJPEGRepresentation(imageResized, 0);
    
    if (imgData.length > [SharedData maxImageSize]) {
        [DialogManager showErrorImageSizeLimit];
        return;
    }
    
    if (isEditAdvertise) {
        [productObject setIsResetProductImages:@"true"];
    }
    
    [productObject setIsUploadAdvertiseImages:YES];
    
    [relatedViewerDelegate imageAddedToProductObject:imageResized
                                     isEditAdvertise:isEditAdvertise];
}

- (UIImage *)resizeImage:(UIImage *)image toSize:(CGSize)newSize;
{
    UIGraphicsBeginImageContext(newSize);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}


- (UIImage *)compreessImage:(UIImage *)photo {
    NSData *data = UIImageJPEGRepresentation(photo, 0.40);
    
    CGFloat compression = 0.4f;
    CGFloat maxCompression = 0.1f;
    
    while ([data length] > [SharedData compressImageSizeToReduce] &&
           compression > maxCompression) {
        compression -= 0.1;
        data = UIImageJPEGRepresentation(photo, compression);
    }
    
    return [UIImage imageWithData:data];
}
@end
