//
//  FilterOtherCategoriesChooser.h
//  Mzad Qatar
//
//  Created by samo on 9/7/14.
//  Copyright (c) 2014 Mohammed abdulfattah attia. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "CitiesViewController.h"
#import "SubCategoryChooser.h"
#import "DialogManager.h"
#import "TextFieldViewController.h"

@class FilterOtherCategoriesChooser;
@protocol FilterOtherCategoriesChooserDelegate <NSObject>
@required
- (void)otherFiltersChoosedWithProductObject:
        (ProductObject *)productObjectParam;
@required
@end

@interface FilterOtherCategoriesChooser
    : TextFieldViewController <SubCategoryChooserDelegate> {
  NSString *currentCategoryId;
  NSString *currentSubCategoryId;
  NSString *currentCityId;
}

@property(strong) ProductObject *productObject;

@property(strong, nonatomic) IBOutlet UIButton *citiesBtnChooser;
@property(strong, nonatomic) IBOutlet UIButton *subcategoriesBtnChooser;
@property(strong, nonatomic) IBOutlet UITextField *priceFromTextField;
@property(strong, nonatomic) IBOutlet UITextField *priceToTextField;

@property(strong, nonatomic) IBOutlet UIImageView *citiesImgChooser;
@property(strong, nonatomic) IBOutlet UIImageView *subcategoriesImgChooser;

@property(nonatomic, strong) id<FilterOtherCategoriesChooserDelegate> delegate;

- (IBAction)searchBtnClicked:(id)sender;

@end
