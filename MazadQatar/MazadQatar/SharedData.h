//
//  SharedData.h
//  MazadQatar
//
//  Created by samo on 4/14/13.
//  Copyright (c) 2013 Mohammed abdulfattah attia. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <SDWebImage/UIImageView+WebCache.h>
//#import "UIImageView+WebCache.h"

@interface SharedData : NSObject

//==============================================product comments
//+(NSString*) getproductCommentsLastUpdateTime;
//+(void) setproductCommentsLastUpdateTime
//:(NSString*)productCommentLastUpdateTimeParam;

#define SET_ALERTUI_FREQUENCY @"5"
#define SHARE_LABEL_TITILE @"Share this on Fb!"

+ (NSString *)getUserProductsLastUpdateTime;
+ (void)setUserProductsLastUpdateTime:
        (NSString *)userProductsLastUpdateTimeParam;

+ (NSString *)getUserFavoriteLastUpdateTime;
+ (void)setUserFavoriteLastUpdateTime:
        (NSString *)userFavoritesLastUpdateTimeParam;

+ (NSString *)getCategoryDataLastUpdateTime;
+ (void)setCategoryDataLastUpdateTime:
        (NSString *)categoryDataLastUpdateTimeParam;

+ (NSString *)getNotificationLastUpdateTime;
+ (void)setNotificationLastUpdateTime:
        (NSString *)categoryDatalastUpdateTimeParam;

+ (NSMutableArray *)getCategories;
+ (void)setCategories:(NSMutableArray *)categoriesParam;

+ (bool)isAllowLog;
+ (void)clearSdImageMemory;

+ (NSString *)openedFromAddAdvdrtise;
+ (NSString *)notOpenedFromAddAdvdrtise;

+ (int)numberOfAdvertiseInOneRow;
+ (void)setNumberOfAdvertiseInOneRow:(int)numberOfAdsPerRowParam;

+ (BOOL)isContinueCountintRows;

+ (void)setIsContinueCountintRows:(BOOL)isContinueCountintRowsParam;

+ (int)maxImageSize;
+ (int)compressImageSizeToReduce;

+ (BOOL)getIsFromSelectImageViewControllerBool;
+ (void)setIsFromSelectImageViewController:(BOOL)isFromSelectImageView;

+ (void)setNotificationCountInSharedData:
        (NSString *)sharedDataNotificationCount;
+ (NSString *)getNotificationCountFromSharedData;
+ (NSString *)getDeviceLanguage;

+ (NSString *)CITIES_AND_REGION_KEY;
+ (NSString *)CATEGORY_ARRAY_FILE_KEY;
+ (NSString *)CITIES_AND_REGION_ARRAY_FILE_KEY;

+ (NSString *)CAR_FILTER_VALUE;
+ (NSString *)PROPERTY_FILTER_VALUE;
+ (NSString *)OTHER_FILTER_VALUE;

+ (NSMutableDictionary *)getSharedServerParams;
+ (NSString *)getUserIPAddress;

+ (UIStoryboard *)getCurrentStoryBoard;

+ (void)setIsRateShareDialogShown:(BOOL)isDialogRateShownParam ;

+ (bool)isRateShareDialogShown ;
@end
