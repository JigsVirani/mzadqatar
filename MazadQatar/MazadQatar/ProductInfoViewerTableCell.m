//
//  ProductInfoViewerTableCell.m
//  MazadQatar
//
//  Created by samo on 5/27/13.
//  Copyright (c) 2013 Mohammed abdulfattah attia. All rights reserved.
//

#import "ProductInfoViewerTableCell.h"
#import "DisplayUtility.h"
#import "UserSetting.h"
#import "ServerManager.h"

@implementation ProductInfoViewerTableCell

- (id)initWithStyle:(UITableViewCellStyle)style
    reuseIdentifier:(NSString *)reuseIdentifier {
  self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
  if (self) {
    // Initialization code
  }
  return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
  [super setSelected:selected animated:animated];

  // Configure the view for the selected state
}

- (IBAction)viewInWebsiteBtn:(UIButton *)sender {
  [[UIApplication sharedApplication]
      openURL:[NSURL URLWithString:productObject.productWebsiteUrl]];
}

- (void)setViewProductInfoUi:(ProductObject *)productObjectParam {
  //[self changeAddProductsFieldsToReadingOnly];
  productObject = productObjectParam;

  [self setStringInTextView];
}

- (void)setStringInTextView {
  // iOS6 and above : Use NSAttributedStrings
  const CGFloat fontSize = 16;
  UIFont *boldFont = [UIFont boldSystemFontOfSize:fontSize];
  UIFont *regularFont = [UIFont systemFontOfSize:fontSize];
  UIColor *foregroundColor = [UIColor whiteColor];

  // Create the attributes
  NSDictionary *attrs = [NSDictionary
      dictionaryWithObjectsAndKeys:regularFont, NSFontAttributeName,
                                   foregroundColor,
                                   NSForegroundColorAttributeName, nil];
  NSDictionary *subAttrs = [NSDictionary
      dictionaryWithObjectsAndKeys:boldFont, NSFontAttributeName,
                                   foregroundColor,
                                   NSForegroundColorAttributeName, nil];

  // total views btn
  if (productObject.productNumberOfViews != nil) {
    [self.totalViewsBtn setTitle:productObject.productNumberOfViews
                        forState:UIControlStateNormal];
  }

  // Create the attributed string (text + attributes)
  const NSRange productItemRange =
      NSMakeRange(0, NSLocalizedString(@"PRODUCT_ITEM_NAME", nil).length);
  NSMutableAttributedString *productItemAttributedText = [
      [NSMutableAttributedString alloc]
      initWithString:[NSString stringWithFormat:@"%@ %@",
                                                NSLocalizedString(
                                                    @"PRODUCT_ITEM_NAME", nil),
                                                productObject.productTilte]
          attributes:attrs];
  [productItemAttributedText setAttributes:subAttrs range:productItemRange];
  // Set it in our UILabel and we are done!
  [self.productItemName setAttributedText:productItemAttributedText];


  const NSRange productPriceRange =
      NSMakeRange(0, NSLocalizedString(@"PRODUCT_PRICE", nil).length);
  NSMutableAttributedString *productPriceAttributedText =
      [[NSMutableAttributedString alloc]
          initWithString:[NSString stringWithFormat:@"%@ %@",
                                                    NSLocalizedString(
                                                        @"PRODUCT_PRICE", nil),
                                                    productObject.productPrice]
              attributes:attrs];
  [productPriceAttributedText setAttributes:subAttrs range:productPriceRange];
  // Set it in our UILabel and we are done!
  [self.productPrice setAttributedText:productPriceAttributedText];


  const NSRange productPhoneNumberRange =
      NSMakeRange(0, NSLocalizedString(@"PRODUCT_PHONE_NUMBER", nil).length);
  NSMutableAttributedString *productPhoneNumberAttributedText =
      [[NSMutableAttributedString alloc]
          initWithString:[NSString
                             stringWithFormat:@"%@ %@%@",
                                              NSLocalizedString(
                                                  @"PRODUCT_PHONE_NUMBER", nil),
                                              @"00974",
                                              productObject.productUserNumber]
              attributes:attrs];
  [productPhoneNumberAttributedText setAttributes:subAttrs
                                            range:productPhoneNumberRange];
  // Set it in our UILabel and we are done!
  [self.productPhoneNumber setAttributedText:productPhoneNumberAttributedText];


  const NSRange productDateRange =
      NSMakeRange(0, NSLocalizedString(@"PRODUCT_DATE", nil).length);
  NSMutableAttributedString *productDateAttributedText =
      [[NSMutableAttributedString alloc]
          initWithString:[NSString stringWithFormat:@"%@ %@",
                                                    NSLocalizedString(
                                                        @"PRODUCT_DATE", nil),
                                                    productObject.productDate]
              attributes:attrs];
  [productDateAttributedText setAttributes:subAttrs range:productDateRange];
  // Set it in our UILabel and we are done!
  [self.productDate setAttributedText:productDateAttributedText];


    
    // const NSRange productDescriptionRange =
 //     NSMakeRange(0, NSLocalizedString(@"PRODUCT_Description", nil).length);
  NSMutableAttributedString *productDescriptionAttributedText =
      [[NSMutableAttributedString alloc]
          initWithString:[NSString
                             stringWithFormat:@"%@",
                                              productObject.productDescriptionWithTitles]
              attributes:attrs];
    
  productDescriptionAttributedText= [self boldTextInDescription:productDescriptionAttributedText andStyle:subAttrs];

  //[productDescriptionAttributedText setAttributes:subAttrs
  //                                          range:productDescriptionRange];
  // Set it in our UILabel and we are done!
  [self.productDescription setAttributedText:productDescriptionAttributedText];

}

-(NSMutableAttributedString*)boldTextInDescription:(NSMutableAttributedString*)text andStyle: (NSDictionary *)subAttrs
{
    NSRange range = [productObject.productDescriptionWithTitles rangeOfString:@"%B"];
   // [text setAttributes:subAttrs
   //                                           range:range];
    
    NSRange startRangeBoldKey=range;
    NSRange endRangeBoldKey=NSMakeRange(0, 0);
    while(range.location!=NSNotFound)
    {
        productObject.productDescriptionWithTitles=[text string];

        if(range.location+1>productObject.productDescriptionWithTitles.length)
        {
            break;
        }
        range = [productObject.productDescriptionWithTitles rangeOfString:@"%B" options:0 range:NSMakeRange(range.location+1,productObject.productDescriptionWithTitles.length-(range.location+1))];
        
        if(startRangeBoldKey.length==0)
        {
            startRangeBoldKey=range;
        }
        else
        {
            endRangeBoldKey=range;
        }
        
        if(startRangeBoldKey.length!=0&endRangeBoldKey.length!=0)
        {
            [text setAttributes:subAttrs
                          range:NSMakeRange(startRangeBoldKey.location+1, endRangeBoldKey.location-(startRangeBoldKey.location+1))];
            [text replaceCharactersInRange:startRangeBoldKey withString:@""];
            [text replaceCharactersInRange:NSMakeRange(endRangeBoldKey.location-2, endRangeBoldKey.length) withString:@""];

            range=NSMakeRange(endRangeBoldKey.location-2, endRangeBoldKey.length);
            startRangeBoldKey=NSMakeRange(0, 0);
            endRangeBoldKey=NSMakeRange(0, 0);
            
        }
        
    }

    return text;
}

@end
