//
//  LanguageChooserViewController.m
//  Mzad Qatar
//
//  Created by samo on 12/19/13.
//  Copyright (c) 2013 Mohammed abdulfattah attia. All rights reserved.
//

#import "SortByChooserViewController.h"
#import "UserSetting.h"
#import "DisplayUtility.h"
#import "SortByObject.h"
#import "SoryByTableCell.h"

@interface SortByChooserViewController () <ServerManagerResponseDelegate>

@end

NSString *requestGetSortByFilters = @"requestGetSortByFilters";
NSString *sortByArrayFile = @"sortByArrayFile";

@implementation SortByChooserViewController

- (void)viewDidLoad {
    
    self->screenName = @"Sort By Chooser";
    self.title = NSLocalizedString(@"Sort By", nil);
    
    [super viewDidLoad];
    [self addLoadingIndicatorToView];
    
    //UIImage *backgroundPattern = [UIImage imageNamed:@"bg.png"];
    //[self.selectLanguageTable setBackgroundColor:[UIColor colorWithPatternImage:backgroundPattern]];

    self.tableView.tableFooterView =
    [[UIView alloc] initWithFrame:CGRectZero];
    
    [self startLoadingSortByFilters];
    //[self.navigationController.navigationBar.topItem setTitle:NSLocalizedString(@"BACK", nil)];
}

- (void)startLoadingSortByFilters {
    //NSString *language = [[[NSBundle mainBundle] preferredLocalizations] objectAtIndex:0];
    NSString *language = [LanguageManager currentLanguageCode];
    
    if ([[UserSetting getLastUpdateTimeForSortByFromServer] intValue] >
        [[UserSetting getLastUpdateTimeForSortByFromLocal] intValue] ||
        [language isEqualToString:[UserSetting getSortByFileLanguage]] == false) {
        [self loadDataInTableFromServer];
    } else {
        NSData *arrayData =
        [[NSUserDefaults standardUserDefaults] objectForKey:sortByArrayFile];
        NSMutableArray *array =
        [NSKeyedUnarchiver unarchiveObjectWithData:arrayData];
        
        [self loadDataInTableFromArray:array];
    }
}

- (void)loadDataInTableFromArray:(NSMutableArray *)array {
    if ([array count] < 1) {
        // error in local file
        [self loadDataInTableFromServer];
        return;
    }
    
    
    sortByArray = array;
    
    [self.selectLanguageTable reloadData];
}

- (void)loadDataInTableFromServer {
    [self showOverlayWithIndicator:true];
    
    [ServerManager getSortByFilters:self
                withUserCountryCode:[UserSetting getUserCountryCode]
                         withNumber:[UserSetting getUserNumber]
                       withLanguage:[SharedData getDeviceLanguage]
                      withRequestId:requestGetSortByFilters];
}

- (void)addLoadingIndicatorToView {
    activityIndicator = [[UIActivityIndicatorView alloc]
                         initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    activityIndicator.center = self.navigationController.view.center;
    
    [self.navigationController.view addSubview:activityIndicator];
}

- (void)showOverlayWithIndicator:(BOOL)isShow {
    if (isShow) {
        isOverlayShown = true;
        [activityIndicator startAnimating];
        
    } else {
        isOverlayShown = false;
        [activityIndicator stopAnimating];
    }
}

-(void)setSoryByDelegate:(id<SortByChooserDelegate>)delegateParam andSelectedIndex:(int)selectedIndexParam
{
    self.delegate=delegateParam;
    
    selectedIndex=selectedIndexParam;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section {
    return sortByArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    SoryByTableCell *cell = nil;
    
    cell = [tableView dequeueReusableCellWithIdentifier:@"sortByIdentifier"
                                           forIndexPath:indexPath];
    SortByObject *sortByObject = [sortByArray objectAtIndex:indexPath.row];
    
    [cell.sortByTextView setTextColor:[UIColor whiteColor]];
    [cell.sortByTextView setText:sortByObject.SortByFilterName];
    
    NSString *imageName =
    [[sortByObject SortByFilterName] stringByAppendingString:@".png"];
    [cell.sortByIndicator startAnimating];
    
    if ([sortByObject.SortByFilterImageUrl isKindOfClass:[NSString class]]) {
        [cell.sortByImageView
         sd_setImageWithURL:[NSURL
                             URLWithString:sortByObject.SortByFilterImageUrl]
         placeholderImage:[UIImage imageNamed:imageName]
         completed:^(UIImage *image, NSError *error,
                     SDImageCacheType cacheType, NSURL *imageURL) {
             [cell.sortByIndicator stopAnimating];
         }];
    }
    
    if (indexPath.row ==selectedIndex) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    } else {
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    cell.backgroundColor = [UIColor clearColor];
    

    return cell;
}

//- (CGFloat)tableView:(UITableView *)tableView
//heightForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    return 39;
//}

- (void)tableView:(UITableView *)tableView
didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    selectedIndex = (int)indexPath.row;
    
    SortByObject *sortByObject = [sortByArray objectAtIndex:indexPath.row];
    
    [self.delegate sortByChooserChanged:sortByObject.sortByFilterId
                        withSortByIndex:(int)indexPath.row
                         withSortByName:sortByObject.SortByFilterName];
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)requestFinished:(ServerManagerResult *)serverManagerResult
            ofRequestId:(NSString *)requestId
    withNumberOfRequest:(int)numberOfRequest {
    [self showOverlayWithIndicator:false];
    
    if ([requestId isEqualToString:requestGetSortByFilters]) {
        if ([serverManagerResult.opertationResult
             isEqualToString:[ServerManager ERROR_IN_CONNECTION_OPERATION]]) {
            [DialogManager showErrorInConnection];
        } else if ([serverManagerResult.opertationResult
                    isEqualToString:[ServerManager SUCCESS_OPERATION]]) {
            
            NSData *data = [NSKeyedArchiver
                            archivedDataWithRootObject:serverManagerResult.jsonArray];
            [[NSUserDefaults standardUserDefaults] setObject:data
                                                      forKey:sortByArrayFile];
            
            [self loadDataInTableFromArray:serverManagerResult.jsonArray];
            
            [UserSetting saveLastUpdateTimeForSortByFromLocal:
             [UserSetting getLastUpdateTimeForSortByFromServer]];
            
            // save language so whenever we change locale we load file again with
            // other language
            //NSString *language = [[[NSBundle mainBundle] preferredLocalizations] objectAtIndex:0];
            NSString *language = [LanguageManager currentLanguageCode];
            
            if ([language isEqualToString:[ServerManager LANGUAGE_ARABIC]]) {
                [UserSetting saveSortByFileLanguage:[ServerManager LANGUAGE_ARABIC]];
            } else {
                [UserSetting saveSortByFileLanguage:[ServerManager LANGUAGE_ENGLISH]];
            }
            
        } else if ([serverManagerResult.opertationResult
                    isEqualToString:[ServerManager FAILED_OPERATION]])
            
        {
            //Comment [DialogManager showErrorInServer];
            [DialogManager showErrorWithMessage:serverManagerResult.opertationMsg];
        }
    }
}

@end
