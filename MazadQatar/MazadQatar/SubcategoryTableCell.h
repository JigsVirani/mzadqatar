//
//  SubcategoryTableCell.h
//  Mzad Qatar
//
//  Created by samo on 1/25/14.
//  Copyright (c) 2014 Mohammed abdulfattah attia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SubcategoryTableCell : UITableViewCell

@property(strong, nonatomic) IBOutlet UIImageView *subcategoryImageView;
@property(strong, nonatomic) IBOutlet UILabel *subcatgoryTitle;
@property(strong, nonatomic)
    IBOutlet UIActivityIndicatorView *subcategoryImageLoadingIndicator;

@end
