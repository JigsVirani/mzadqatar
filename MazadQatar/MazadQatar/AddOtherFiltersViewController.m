//
//  AddOtherFiltersViewController.m
//  Mzad Qatar
//
//  Created by samo on 9/20/14.
//  Copyright (c) 2014 Mohammed abdulfattah attia. All rights reserved.
//

#import "AddOtherFiltersViewController.h"

@interface AddOtherFiltersViewController ()

@end

@implementation AddOtherFiltersViewController

- (void)viewDidLoad {
  
    [super viewDidLoad];

    self.screenName = @"Add Other Filters Screen";
    self.title = NSLocalizedString(@"Filters of category", nil);
    
    if([[SharedData getDeviceLanguage]isEqualToString:@"en"]){
        
        [self.chooseAdvertiseTypeBtn setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
        [self.chooseCityBtn setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
        [self.chooseSubcategoryBtn setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
        
        [self.chooseAdvertiseTypeImg setImage:[UIImage imageNamed:@"ic_english_arrow"]];
        [self.chooseCityImg setImage:[UIImage imageNamed:@"ic_english_arrow"]];
        [self.chooseSubcategoryImg setImage:[UIImage imageNamed:@"ic_english_arrow"]];
        
        /*[self.chooseAdvertiseTypeBtn setBackgroundImage:[UIImage imageNamed:@"bk_choose_list_btn_right.png"] forState:UIControlStateNormal];
        [self.chooseCityBtn setBackgroundImage:[UIImage imageNamed:@"bk_choose_list_btn_right.png"] forState:UIControlStateNormal];
        [self.chooseSubcategoryBtn setBackgroundImage:[UIImage imageNamed:@"bk_choose_list_btn_right.png"] forState:UIControlStateNormal];*/
        
    }else{
        
        [self.chooseAdvertiseTypeBtn setContentHorizontalAlignment:UIControlContentHorizontalAlignmentRight];
        [self.chooseCityBtn setContentHorizontalAlignment:UIControlContentHorizontalAlignmentRight];
        [self.chooseSubcategoryBtn setContentHorizontalAlignment:UIControlContentHorizontalAlignmentRight];
        
        [self.chooseAdvertiseTypeImg setImage:[UIImage imageNamed:@"ic_arabic_arrow"]];
        [self.chooseCityImg setImage:[UIImage imageNamed:@"ic_arabic_arrow"]];
        [self.chooseSubcategoryImg setImage:[UIImage imageNamed:@"ic_arabic_arrow"]];
        
        [self.chooseAdvertiseTypeBtn setTitleEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 8)];
        [self.chooseCityBtn setTitleEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 8)];
        [self.chooseSubcategoryBtn setTitleEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 8)];
        
        /*[self.chooseAdvertiseTypeBtn setBackgroundImage:[UIImage imageNamed:@"bk_choose_list_btn_left"] forState:UIControlStateNormal];
        [self.chooseCityBtn setBackgroundImage:[UIImage imageNamed:@"bk_choose_list_btn_left"] forState:UIControlStateNormal];
        [self.chooseSubcategoryBtn setBackgroundImage:[UIImage imageNamed:@"bk_choose_list_btn_left"] forState:UIControlStateNormal];*/
    }
    //[self.navigationController.navigationBar.topItem setTitle:NSLocalizedString(@"BACK", nil)];
}
- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}
- (void)setProductObjectUi:(ProductObject *)productObject {
    self.productObject = productObject;
    if (self.productObject.productAdvertiseTypeName != nil) {
        [self.chooseAdvertiseTypeBtn
         setTitle:self.productObject.productAdvertiseTypeName
         forState:UIControlStateNormal];
    }
    if (self.productObject.productCityName != nil) {
        [self.chooseCityBtn setTitle:self.productObject.productCityName
                            forState:UIControlStateNormal];
    }
    if (self.productObject.productSubCategoryName != nil) {
        [self.chooseSubcategoryBtn
         setTitle:self.productObject.productSubCategoryName
         forState:UIControlStateNormal];
    }
}
- (void)didReceiveMemoryWarning {
  [super didReceiveMemoryWarning];
  // Dispose of any resources that can be recreated.
}

- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier
                                  sender:(id)sender {
  if ([identifier isEqualToString:@"SubSubCategorySegue"] ||
      [identifier isEqualToString:@"SubSubCategorySegueArrow"]) {
    if ([self.productObject.productSubCategoryId isEqualToString:@""]) {
      [DialogManager showErrorSubCategoryNotChosen];

      return false;
    }
  }

  return true;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
  [super prepareForSegue:segue sender:sender];

  if ([[segue identifier] isEqualToString:@"SubCategorySegue"]) {
    SubCategoryChooser *subCategoryChooser = [segue destinationViewController];

    [subCategoryChooser
        setSubCategoryParamsWithCatId:self.productObject.productCategoryId
              andCurrentSubCategoryId:self.productObject.productSubCategoryId
                     andIsOpenedFromAddAdvertise:true
                          andDelegate:self];
  } else if ([[segue identifier] isEqualToString:@"CitiesSegue"]) {
    CitiesViewController *citiesViewController =
        [segue destinationViewController];

    [citiesViewController
        setCityParamsWithCityId:self.productObject.productCityId
               andIsOpenedFromAddAdvertise:true
                    andDelegate:self];
  } else if ([[segue identifier]
                 isEqualToString:@"AdvertiseTypeChooserSegue"]) {
    AdvertiseTypeChooser *advertiseTypeChooser =
        [segue destinationViewController];

    [advertiseTypeChooser
        setAdvertiseTypeParamsWithCatId:self.productObject.productCategoryId
             andSelectedAdvertiseTypeId:self.productObject
                                            .productAdvertiseTypeId
                            andDelegate:self];
  }
}

- (void)subCategoryChoosedInList:(NSString *)subCategoryId
              andSubCategoryName:(NSString *)subCategorName {
  [self.chooseSubcategoryBtn setTitle:subCategorName
                             forState:UIControlStateNormal];
  self.productObject.productSubCategoryId = subCategoryId;
  self.productObject.productSubCategoryName = subCategorName;
}

- (void)citiesChoosedInList:(NSString *)cityId
                andCityName:(NSString *)cityName {
  [self.chooseCityBtn setTitle:cityName forState:UIControlStateNormal];
  self.productObject.productCityId = cityId;
  self.productObject.productCityName = cityName;
}

- (void)AdvertiseTypeChoosed:(NSString *)advertiseTypeChooserId
    andAdvertiseTypeChooserName:(NSString *)advertiseTypeChooserName {
  [self.chooseAdvertiseTypeBtn setTitle:advertiseTypeChooserName
                               forState:UIControlStateNormal];
  self.productObject.productAdvertiseTypeId = advertiseTypeChooserId;
  self.productObject.productAdvertiseTypeName = advertiseTypeChooserName;
}

- (IBAction)doneBtnClicked:(id)sender;
{
  if ([self validateOtherFilters] == false) {
    return;
  }

    [self.productObject setIsAllFiltersSelected:YES];
  [self.delegate otherFiltersChoosedWithProductObject:self.productObject];

  [self.navigationController popViewControllerAnimated:YES];
}

- (bool)validateOtherFilters {
  if (self.productObject.productCityId.length == 0) {
    [DialogManager showErrorCitiesNotChosen];

    return false;
  }

  if (self.productObject.productAdvertiseTypeId.length == 0) {
    [DialogManager showErrorAdvertiseTypeNotChoosen];

    return false;
  }

  if (self.productObject.productSubCategoryId.length == 0) {
    [DialogManager showErrorSubCategoryNotChosen];

    return false;
  }

  return true;
}


@end
