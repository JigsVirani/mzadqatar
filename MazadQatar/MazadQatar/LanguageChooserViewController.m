//
//  LanguageChooserViewController.m
//  Mzad Qatar
//
//  Created by samo on 12/19/13.
//  Copyright (c) 2013 Mohammed abdulfattah attia. All rights reserved.
//

#import "LanguageChooserViewController.h"
#import "UserSetting.h"
#import "ServerManager.h"

@interface LanguageChooserViewController ()

@end

@implementation LanguageChooserViewController

- (id)initWithStyle:(UITableViewStyle)style {
  self = [super initWithStyle:style];
  if (self) {
    // Custom initialization
  }
  return self;
}

- (void)viewDidLoad {
    
    self->screenName = @"Language Chooser";
    [super viewDidLoad];
    
    if([LanguageManager currentLanguageIndex]==1){
        self.title = @"لغة الإعلان";
    }else{
        self.title = @"Ad language";
    }
    
    //UIImage *backgroundPattern = [UIImage imageNamed:@"bg.png"];
    //[self.selectLanguageTable setBackgroundColor:[UIColor colorWithPatternImage:backgroundPattern]];
    //self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    //[self.navigationController.navigationBar.topItem setTitle:NSLocalizedString(@"BACK", nil)];
}

- (void)didReceiveMemoryWarning {
  [super didReceiveMemoryWarning];
  // Dispose of any resources that can be recreated.
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
  return 3;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = nil;
    // comments of product
    
    if(indexPath.row == 0) {
        cell = [tableView dequeueReusableCellWithIdentifier:@"englishAndArabicIdentifier" forIndexPath:indexPath];
    }else if(indexPath.row == 1) {
        cell = [tableView dequeueReusableCellWithIdentifier:@"englishIdentifier" forIndexPath:indexPath];
    }else if(indexPath.row == 2) {
        cell = [tableView dequeueReusableCellWithIdentifier:@"arabicIdentifier" forIndexPath:indexPath];
    }
    UIButton *btnCheck = [cell viewWithTag:101];
    
    if(indexPath.row == self.selectedIndex) {
        //cell.accessoryType = UITableViewCellAccessoryCheckmark;
        [btnCheck setHidden:false];
    }else{
        //cell.accessoryType = UITableViewCellAccessoryNone;
        [btnCheck setHidden:true];
    }
    
    cell.backgroundColor = [UIColor clearColor];
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
  return 90;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    self.selectedIndex = (int)indexPath.row;
    [self.navigationController popViewControllerAnimated:YES];
    [self.delegate languageChanged:(int)indexPath.row withLanguageName:[UserSetting getUserAddAdvertiseLanguageName:(int)indexPath.row]];
}
@end
