//
//  AppDelegate.h
//  MazadQatar
//
//  Created by samo on 2/28/13.
//  Copyright (c) 2013 Mohammed abdulfattah attia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SharedData.h"
#import "NotificationViewController.h"
#import "SWRevealViewController.h"

@interface AppDelegate
: UIResponder <UIApplicationDelegate, UIAlertViewDelegate,UITabBarControllerDelegate,ALChatViewControllerDelegate>
//pod 'Google-Mobile-Ads-SDK', '~> 7.8'
@property(strong, nonatomic) UIWindow *window;
@property(strong, nonatomic) NSString *notificationProductId;
@property(strong, nonatomic) NSString *categoryProductId;
@property(strong, nonatomic) NSString *strSubcategoryId;
@property(strong, nonatomic) NSString *strTabID;

@property(assign, nonatomic) int notificationCount;
@property(strong, nonatomic) NSDateFormatter *dateFormatter;

@property(strong, nonatomic) UINavigationController *navigationController;

@property(strong, nonatomic) NSMutableDictionary *dictInfo;

@property(assign) BOOL isFromPushNotificationClicked;
@property (strong,nonatomic) UIStoryboard *storyboardName;
- (void)application:(UIApplication *)application
didReceiveRemoteNotification:(NSDictionary *)userInfo;
+(AppDelegate*) app;
-(void)Add24HoursToDate:(NSDate *)date withKey:(NSString *)strKey withCurrentDateKey:(NSString *)strCurrentKey;
@end
