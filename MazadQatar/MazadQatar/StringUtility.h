//
//  StringUtility.h
//  MazadQatar
//
//  Created by samo on 6/10/13.
//  Copyright (c) 2013 Mohammed abdulfattah attia. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DisplayUtility.h"

@interface StringUtility : NSObject

+ (int)isTextEmpty:(NSString *)text;

+ (CGSize)getSize:(NSString *)string
      andFontSize:(int)fontSize
           isBold:(bool)isBold
       isPortrait:(bool)isPortrait;
+ (CGSize)getSizeWithNewSize:(NSString *)string
                 andFontSize:(int)fontSize
                      isBold:(bool)isBold
                  isPortrait:(bool)isPortrait;
+ (CGSize)getSizeForComment:(NSString *)string
                andFontSize:(int)fontSize
                     isBold:(bool)isBold
                 isPortrait:(bool)isPortrait;
+ (NSData *)encryptString:(NSString *)encryptString;
+ (NSString *)initDecryptedString:(NSData *)decryptString;
+(BOOL) NSStringIsValidEmail:(NSString *)checkString;
@end
