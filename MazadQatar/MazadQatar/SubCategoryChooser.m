//
//  CategoryChooser.m
//  MazadQatar
//
//  Created by samo on 5/13/13.
//  Copyright (c) 2013 Mohammed abdulfattah attia. All rights reserved.
//

#import "SubCategoryChooser.h"
#import "SubcategoryTableCell.h"

@interface SubCategoryChooser () <ServerManagerResponseDelegate>

@end

@implementation SubCategoryChooser

NSString *requestGetSubCategory = @"requestGetSubCategory";
NSString *subcategoryChooserCellIdentifier = @"SubCategoryChooserCell";

- (void)viewDidLoad {
    [super viewDidLoad];
    self.screenName = @"Sub Category Chooser";
    [self startloadingData];
    self.tableView.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
    //[self.navigationController.navigationBar.topItem setTitle:NSLocalizedString(@"BACK", nil)];
    
    if([LanguageManager currentLanguageIndex]==1){
        self.title = @"التصنيف الفرعي";
    }else{
        self.title = @"SubCategory";
    }
}
-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    return YES;
}
- (void)startloadingData {
    //NSString *language = [[[NSBundle mainBundle] preferredLocalizations] objectAtIndex:0];
    NSString *language = [LanguageManager currentLanguageCode];
    
    if ([[UserSetting getLastUpdateTimeForCategoriesFromServer] intValue] >
        [[UserSetting getLastUpdateTimeForCategoriesFromLocal] intValue] ||
        [language isEqualToString:[UserSetting getAllCategoriesFileLanguage]] ==
        false) {
        [self loadDataInTableFromServer];
    } else {
        NSData *arrayData = [[NSUserDefaults standardUserDefaults]
                             objectForKey:[SharedData CATEGORY_ARRAY_FILE_KEY]];
        NSMutableArray *array =
        [NSKeyedUnarchiver unarchiveObjectWithData:arrayData];
        
        [self loadDataInTableFromArray:array];
    }
}

- (void)loadDataInTableFromArray:(NSMutableArray *)array {
    if ([array count] < 1) {
        // error in local file
        [self loadDataInTableFromServer];
        return;
    }
    
    
    for (CategoryObject *categoryObject in array) {
        if ([categoryObject.categoryID isEqualToString:currentCategoryId]) {
            
            //dont add first element
            if(isOpenedFromAddAdvertise==true)
            {
                if(categoryObject
                   .categorySubcategories.count>0)
                {
                    [categoryObject
                     .categorySubcategories removeObjectAtIndex:0];
                }
            }
            
            for (CategoryObject *subCategoryObject in categoryObject
                 .categorySubcategories) {
                [self.tableData addObject:subCategoryObject.categoryName];
            }
            productItems = categoryObject.categorySubcategories;
            
            break;
        }
    }
    
    [self.tableView reloadData];
}

- (void)loadDataInTableFromServer {
    [self showOverlayWithIndicator:true];
    
    // set language needed
    //NSString *language = [[[NSBundle mainBundle] preferredLocalizations] objectAtIndex:0];
    NSString *language = [LanguageManager currentLanguageCode];
    
    if ([language isEqualToString:[ServerManager LANGUAGE_ARABIC]]) {
        language = [ServerManager LANGUAGE_ARABIC];
    } else {
        language = [ServerManager LANGUAGE_ENGLISH];
    }
    
    //#TODO: is show all text will be changed based on screen opened
    [ServerManager getAllCategories:language
                       withDelegate:self
                      withRequestId:requestGetSubCategory];
}

- (void)requestFinished:(ServerManagerResult *)serverManagerResult
            ofRequestId:(NSString *)requestId
    withNumberOfRequest:(int)numberOfRequest {
    if ([requestId isEqual:requestGetSubCategory]) {
        [self showOverlayWithIndicator:false];
        
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        
        if ([serverManagerResult.opertationResult
             isEqualToString:[ServerManager ERROR_IN_CONNECTION_OPERATION]]) {
            [DialogManager showErrorInConnection];
        }
        
        if ([serverManagerResult.opertationResult
             isEqualToString:[ServerManager SUCCESS_OPERATION]]) {
            
            NSData *data = [NSKeyedArchiver
                            archivedDataWithRootObject:serverManagerResult.jsonArray];
            [[NSUserDefaults standardUserDefaults]
             setObject:data
             forKey:[SharedData CATEGORY_ARRAY_FILE_KEY]];
            
            
            [self loadDataInTableFromArray:serverManagerResult.jsonArray];
            
            [UserSetting saveLastUpdateTimeForCategoriesFromLocal:
             [UserSetting getLastUpdateTimeForCategoriesFromServer]];
            
            // save language so whenever we change locale we load file again with
            // other language
            //NSString *language = [[[NSBundle mainBundle] preferredLocalizations] objectAtIndex:0];
            NSString *language = [LanguageManager currentLanguageCode];
            
            if ([language isEqualToString:[ServerManager LANGUAGE_ARABIC]]) {
                [UserSetting
                 saveAllCategoriesFileLanguage:[ServerManager LANGUAGE_ARABIC]];
            } else {
                [UserSetting
                 saveAllCategoriesFileLanguage:[ServerManager LANGUAGE_ENGLISH]];
            }
        } else if ([serverManagerResult.opertationResult
                    isEqualToString:[ServerManager FAILED_OPERATION]])
            
        {
            //Comment [DialogManager showErrorInServer];
            [DialogManager showErrorWithMessage:serverManagerResult.opertationMsg];
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section {
    return [self getTableCount];
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    // comments of product
    SubcategoryTableCell *cell = [tableView
                                  dequeueReusableCellWithIdentifier:subcategoryChooserCellIdentifier
                                  forIndexPath:indexPath];
    
    CategoryObject *categoryObject = [productItems objectAtIndex:indexPath.row];
    
    cell.subcatgoryTitle.text = [self getRowText:indexPath];
    cell.textLabel.textColor = [UIColor whiteColor];
    
    NSString *imageName =
    [[categoryObject categoryName] stringByAppendingString:@".png"];
    NSString *url = [categoryObject categoryImageURL];
    
    [cell.subcategoryImageLoadingIndicator startAnimating];
    if ([url isKindOfClass:[NSString class]]) {
        
        [cell.subcategoryImageView
         sd_setImageWithURL:[NSURL
                             URLWithString:url]
         placeholderImage:[UIImage imageNamed:imageName]
         completed:^(UIImage *image, NSError *error,
                     SDImageCacheType cacheType, NSURL *imageURL) {
             [cell.subcategoryImageLoadingIndicator stopAnimating];
         }];
        
    }    
    
    if (categoryObject.categoryID == currentSelectedSubCategoryId) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    } else {
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    // ios 7 background issue
    [self setBackgroundClear:cell];
    
    return cell;
}

- (void)setBackgroundClear:(UITableViewCell *)cell {
    cell.backgroundColor = [UIColor clearColor];
    // cell.backgroundView = [[UIView new] autorelease];
    // cell.selectedBackgroundView = [[UIView new] autorelease];
}

- (void)tableView:(UITableView *)tableView
didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    CategoryObject *subCategoryObject;
    
    if([self.searchResult count]>0)
    {
        NSString* subcategoryNameSearched=[self.searchResult objectAtIndex:indexPath.row];
        
        for (CategoryObject *subCategoryObjectInArray in productItems)
        {
            if([subCategoryObjectInArray.categoryName isEqualToString:subcategoryNameSearched])
            {
                subCategoryObject=subCategoryObjectInArray;
                break;
            }
        }
    }
    else
    {
        subCategoryObject =
        [productItems objectAtIndex:indexPath.row];
    }
    
    [delegate subCategoryChoosedInList:subCategoryObject.categoryID
                    andSubCategoryName:subCategoryObject.categoryName];
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)setSubCategoryParamsWithCatId:(NSString *)categoryId
              andCurrentSubCategoryId:(NSString *)currentSelectedsubCategoryID
          andIsOpenedFromAddAdvertise:(BOOL)isOpenedFromAddAdvertiseParam
                          andDelegate:
(id<SubCategoryChooserDelegate>)delegateParam {
    currentCategoryId = categoryId;
    currentSelectedSubCategoryId = currentSelectedsubCategoryID;
    isOpenedFromAddAdvertise = isOpenedFromAddAdvertiseParam;
    delegate = delegateParam;
}


- (void)viewDidUnload {
    
    [super viewDidUnload];
}

@end
