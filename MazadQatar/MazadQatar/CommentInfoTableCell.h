//
//  CommentInfoTableCell.h
//  MazadQatar
//
//  Created by samo on 4/17/13.
//  Copyright (c) 2013 Mohammed abdulfattah attia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProductCommentObject.h"
#import "SystemUtility.h"
#import "ServerManager.h"
#import "DialogManager.h"
#import "ReportItemViewController.h"
#import "ConfirmationDialogueViewController.h"

// Delgate
@class CommentInfoTableCell;
@protocol CommentInfoTableCellDelegate <NSObject>
@required
- (void)deleteCommentCLicked:(UIActivityIndicatorView *)indicator
          andResultTextField:(UITextField *)resultTextFieldParam;
@required
@end

@interface CommentInfoTableCell
    : UITableViewCell <ServerManagerResponseDelegate,
                       ReportItemViewControllerDelegate,
                       ConfirmationDialogueViewControllerDelegate> {
  ProductCommentObject *productCommentObject;
  NSString *advertiseNumber;
  NSString *productId;
  UIViewController *currentView;
  id<MFMessageComposeViewControllerDelegate> smsDelegate;
  id<CommentInfoTableCellDelegate> commentInfoDelegate;
  UITableView *tableView;
}

@property(strong, nonatomic)
    IBOutlet UIActivityIndicatorView *loadingCommentsIndicator;

@property(strong, nonatomic) IBOutlet UILabel *noCommentLbl;

@property(strong, nonatomic) IBOutlet UITextView *nameTextEdit;
@property(strong, nonatomic) IBOutlet UITextView *commentText;
@property(strong, nonatomic) IBOutlet UITextView *dateText;
@property(strong, nonatomic) IBOutlet UIButton *callBtn;
@property(strong, nonatomic) IBOutlet UIButton *smsBtn;
@property(strong, nonatomic) IBOutlet UIImageView *callIcon;
@property(strong, nonatomic) IBOutlet UIImageView *smsIcon;
@property(strong, nonatomic) IBOutlet UILabel *lblAdvertiseOwner;
@property(strong, nonatomic) IBOutlet UITextField *lblCommentActionResult;

//===================report comment
@property(strong, nonatomic) IBOutlet UIButton *reportBtn;
@property(strong, nonatomic) IBOutlet UIImageView *reportImageView;
@property(strong, nonatomic)
    IBOutlet UIActivityIndicatorView *reportActivityIndicator;
- (IBAction)reportBtnClicked:(id)sender;

//===================delete comment
@property(strong, nonatomic) IBOutlet UIButton *deleteBtn;
@property(strong, nonatomic) IBOutlet UIImageView *deleteImageView;
@property(strong, nonatomic)
    IBOutlet UIActivityIndicatorView *deleteActivityIndicator;
- (IBAction)deleteBtnclicked:(id)sender;

- (IBAction)callBtnClicked:(UIButton *)sender;
- (IBAction)smsBtnClicked:(id)sender;

- (void)setComment:(ProductCommentObject *)productCommentObjectParam
       andAdvertiseNumber:(NSString *)advertiseNumberParam
                   inView:(UIViewController *)currentViewParam
              andDelegate:
                  (id<MFMessageComposeViewControllerDelegate>)smsDelegateParam
    andCommentInfoDelegae:
        (id<CommentInfoTableCellDelegate>)commentInfoDelegateParam
              inProductId:(NSString *)productIdParam
              inTableView:(UITableView *)tableViewParam;
- (void)setIsLoadingCommentView:(BOOL)isLoading;
- (void)setIsNoCommentLblHidden:(BOOL)isHidden;
//-(CGFloat)getCellHeight:(ProductCommentObject*)productCommentObjectParam;
@end
