//
//  CitiesViewController.m
//  Mzad Qatar
//
//  Created by samo on 8/28/14.
//  Copyright (c) 2014 Mohammed abdulfattah attia. All rights reserved.
//

#import "CitiesViewController.h"
#import "FileManager.h"

@interface CitiesViewController () <ServerManagerResponseDelegate>
@end

@implementation CitiesViewController

NSString *requestGetCitiesAndRegion = @"requestGetCitiesAndRegion";

- (void)viewDidLoad {
    [super viewDidLoad];
    self.screenName = @"Cities Screen";
    [self startloadingData];
    
    if([LanguageManager currentLanguageIndex]==1){
        self.title = @"مدن";
    }else{
        self.title = @"Cities";
    }
}

- (void)startloadingData {
    //NSString *language = [[[NSBundle mainBundle] preferredLocalizations] objectAtIndex:0];
    NSString *language = [LanguageManager currentLanguageCode];
    
    NSNumber *lastUpdateTimeForCitiesFromServerInt=[NSNumber numberWithInt:[UserSetting getLastUpdateTimeForCitiesFromServer]];
    
    NSNumber *lastUpdateTimeForCitiesFromLocalInt=[NSNumber numberWithInt:[UserSetting getLastUpdateTimeForCitiesFromLocal]];
    
    if (lastUpdateTimeForCitiesFromServerInt >
        lastUpdateTimeForCitiesFromLocalInt ||
        [language isEqualToString:[UserSetting getCitiesAndRegionFileLanguage]] ==
        false) {
        [self loadDataInTableFromServer];
    } else {
        NSData *arrayData = [[NSUserDefaults standardUserDefaults]
                             objectForKey:[SharedData CITIES_AND_REGION_ARRAY_FILE_KEY]];
        NSMutableArray *array =
        [NSKeyedUnarchiver unarchiveObjectWithData:arrayData];
        
        [self loadDataInTableFromArray:array];
    }
    //[self.navigationController.navigationBar.topItem setTitle:NSLocalizedString(@"BACK", nil)];
}

- (void)requestFinished:(ServerManagerResult *)serverManagerResult
            ofRequestId:(NSString *)requestId
    withNumberOfRequest:(int)numberOfRequest {
    [self showOverlayWithIndicator:false];
    
    if ([requestId isEqualToString:requestGetCitiesAndRegion]) {
        if ([serverManagerResult.opertationResult
             isEqualToString:[ServerManager ERROR_IN_CONNECTION_OPERATION]]) {
            [DialogManager showErrorInConnection];
        } else if ([serverManagerResult.opertationResult
                    isEqualToString:[ServerManager SUCCESS_OPERATION]]) {
            
            NSData *data = [NSKeyedArchiver
                            archivedDataWithRootObject:serverManagerResult.jsonArray];
            [[NSUserDefaults standardUserDefaults]
             setObject:data
             forKey:[SharedData CITIES_AND_REGION_ARRAY_FILE_KEY]];
            
            [self loadDataInTableFromArray:serverManagerResult.jsonArray];

            [UserSetting saveLastUpdateTimeForCitiesFromLocal:
             [UserSetting getLastUpdateTimeForCitiesFromServer]];
            
            // save language so whenever we change locale we load file again with
            // other language
            //NSString *language = [[[NSBundle mainBundle] preferredLocalizations] objectAtIndex:0];
            NSString *language = [LanguageManager currentLanguageCode];
            
            if ([language isEqualToString:[ServerManager LANGUAGE_ARABIC]]) {
                [UserSetting
                 saveCitiesAndRegionFileLanguage:[ServerManager LANGUAGE_ARABIC]];
            } else {
                [UserSetting
                 saveCitiesAndRegionFileLanguage:[ServerManager LANGUAGE_ENGLISH]];
            }
        } else if ([serverManagerResult.opertationResult
                    isEqualToString:[ServerManager FAILED_OPERATION]])
            
        {
           //Comment [DialogManager showErrorInServer];
            [DialogManager showErrorWithMessage:serverManagerResult.opertationMsg];
        }
    }
}

- (void)loadDataInTableFromArray:(NSMutableArray *)array {
    if ([array count] < 1) {
        // error in local file
        [self loadDataInTableFromServer];
        return;
    }
    
    
    citiesAndRegionArray = array;
    
    //dont add first element
    if(isOpenedFromAddAdvertise==true)
    {
        if(citiesAndRegionArray.count>0)
        {
            [citiesAndRegionArray removeObjectAtIndex:0];
        }
    }
    
    for (CitiesObject *cityObject in citiesAndRegionArray) {
        [self.tableData addObject:cityObject.cityName];
    }
    
    [self.tableView reloadData];
}

- (void)loadDataInTableFromServer {
    [self showOverlayWithIndicator:true];
    
    [ServerManager getCitiesAndRegion:self
                  withUserCountryCode:[UserSetting getUserCountryCode]
                           withNumber:[UserSetting getUserNumber]
                         withLanguage:[SharedData getDeviceLanguage]
                        withRequestId:requestGetCitiesAndRegion];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section {
    return [self getTableCount];
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell =
    [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    CitiesObject *citiesObject =
    [citiesAndRegionArray objectAtIndex:indexPath.row];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                       reuseIdentifier:CellIdentifier];
    }
    if (citiesObject.cityId == currentCityId) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    } else {
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    cell.textLabel.text = [self getRowText:indexPath];
    
    cell.backgroundColor = [UIColor clearColor];
    cell.textLabel.textColor = [UIColor whiteColor];
    return cell;
}

- (void)tableView:(UITableView *)tableView
didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    CitiesObject *cityObject;
    
    if([self.searchResult count]>0)
    {
        NSString* cityNameSearched=[self.searchResult objectAtIndex:indexPath.row];
        
        for (CitiesObject *cityObjectInArray in citiesAndRegionArray)
        {
            if([cityObjectInArray.cityName isEqualToString:cityNameSearched])
            {
                cityObject=cityObjectInArray;
                break;
            }
        }
    }
    else
    {
        cityObject = [citiesAndRegionArray objectAtIndex:indexPath.row];
    }
    [delegate citiesChoosedInList:cityObject.cityId
                      andCityName:cityObject.cityName];
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)setCityParamsWithCityId:(NSString *)cityIdParam
    andIsOpenedFromAddAdvertise:(bool)isOpenedFromAddAdvertiseParam
                    andDelegate:
(id<CitiesViewControllerDelegate>)delegateParam {
    currentCityId = cityIdParam;
    isOpenedFromAddAdvertise = isOpenedFromAddAdvertiseParam;
    delegate = delegateParam;
}

@end
