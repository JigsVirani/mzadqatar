//
//  RegionsObject.m
//  Mzad Qatar
//
//  Created by samo on 8/28/14.
//  Copyright (c) 2014 Mohammed abdulfattah attia. All rights reserved.
//

#import "RegionsObject.h"

@implementation RegionsObject

NSString *REGION_ID_KEY = @"REGION_ID_KEY";
NSString *REGION_NAMES_KEY = @"REGION_NAMES_KEY";

- (void)encodeWithCoder:(NSCoder *)aCoder {

  [aCoder encodeObject:self.regionId forKey:REGION_ID_KEY];
  [aCoder encodeObject:self.regionName forKey:REGION_NAMES_KEY];
}

- (id)initWithCoder:(NSCoder *)aDecoder {
  self = [super init];
  if (!self) {
    return nil;
  }

  self.regionId = [aDecoder decodeObjectForKey:REGION_ID_KEY];
  self.regionName = [aDecoder decodeObjectForKey:REGION_NAMES_KEY];

  return self;
}
@end
