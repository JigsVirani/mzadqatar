//
//  NotificationViewController.m
//  Mzad Qatar
//
//  Created by Perveen Akhtar on 02/07/2014.
//  Copyright (c) 2014 Mohammed abdulfattah attia. All rights reserved.
//

#import "NotificationViewController.h"
#import "ProductDetailsViewController.h"
#import "CategoryProducts.h"

@implementation NotificationViewController

NSString *requestDeleteNotification = @"requestDeleteNotification";
NSString *requestDeleteAllNotification = @"requestDeleteAllNotification";

static NSString *CellIdentifier = @"NotificationViewCell";
bool loadingFromServer = NO;

//@synthesize notificationCount,pageNumberCount,count;

static NotificationViewController *singletonNotificationViewInstance;

+ (NotificationViewController *)getInstance {
    if (singletonNotificationViewInstance == nil) {
        singletonNotificationViewInstance = [[super alloc] init];
    }
    return singletonNotificationViewInstance;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    self.screenName = @"Notification Screen";
    loadMoreWhereAvailable=YES;
     _NoNotificationLable.hidden = YES;
    //[[UIApplication sharedApplication]
    //setStatusBarStyle:UIStatusBarStyleLightContent];
    
    // for background and extra lines in table view
//    UIImage *backgroundPattern = [UIImage imageNamed:@"bg.png"];
//    [self.notificationTableView
//     setBackgroundColor:[UIColor colorWithPatternImage:backgroundPattern]];
    
    self.notificationTableView.tableFooterView =[[UIView alloc] initWithFrame:CGRectZero];
    [self initVariables];
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [hud setLabelText:NSLocalizedString(@"LOADING_NOTIFICATIONS", Nil)];
    //[self.navigationController.navigationBar.topItem setTitle:NSLocalizedString(@"BACK", nil)];
}

//-(void)viewWillAppear:(BOOL)animated{
//    [super viewWillAppear:true];
//    [self.navigationController.tabBarController.tabBar setHidden:NO];
//}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear: animated];
    loadMoreWhereAvailable=YES;
    self.navigationController.navigationBar.translucent = YES;
    self.tabBarController.tabBar.hidden = YES;
}
- (void)initVariables {
    if (notificationArray == nil) {
        notificationArray = [[NSMutableArray alloc] init];
    }
    self.pageNumber = 1;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
   
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
       return [notificationArray count];
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if(loadMoreWhereAvailable==YES)
    {
        if (indexPath.row >= (notificationArray.count * 0.40)) {
            
            if (!loadingFromServer ) {
                [self startLoadingNotificationWithRequestId:requestGetMoreNotification
                                            andIsNewSession:NO andNumberPerPage:@"20"];
            }
        }
    }
    // Configure the cell...
    NotificationViewCell *cell =
    [tableView dequeueReusableCellWithIdentifier:CellIdentifier
                                    forIndexPath:indexPath];
    
    
    NotificationModal *notificationObject =
    [notificationArray objectAtIndex:indexPath.row];
    [cell setNotiifcationProductId:notificationObject];
    
    if ([notificationObject.isRead isEqualToString:@"1"]) {
        cell.unReadNotificationImg.image = [UIImage imageNamed:@"bullect_active.png"];
    } else {
        cell.unReadNotificationImg.image = [UIImage imageNamed:@"bullet_deactive.png"];
    }
    
    cell.titleOfNotification.text = notificationObject.notificationInfo;
    cell.dateOfNotification.text = notificationObject.notificationTime;
    
    NSString *imageName =
    [[notificationObject productId] stringByAppendingString:@".png"];
    [cell.imageLoadingIndicatorOfNotification startAnimating];
    

    [cell.imageOfNotification
     sd_setImageWithURL:[NSURL
                         URLWithString:notificationObject.notificationImage]
     placeholderImage:[UIImage imageNamed:imageName]
     completed:^(UIImage *image, NSError *error,
                 SDImageCacheType cacheType, NSURL *imageURL) {
         [cell.imageLoadingIndicatorOfNotification stopAnimating];
     }];
    
    
    [cell setBackgroundColor:[UIColor clearColor]];
    
    return cell;
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    if(notificationArray.count==0)
    {
        [self startLoadingNotificationWithRequestId:requestGetInitialNotification
                                    andIsNewSession:YES andNumberPerPage:@"20"];
    }
    else
    {
    [self.notificationTableView reloadData];
    }
    self.view.frame = [UIScreen mainScreen].bounds;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NotificationModal *notificationObject =
    [notificationArray objectAtIndex:indexPath.row];
    
    if(notificationObject.notificationCategoryId!=nil&& [notificationObject.notificationCategoryId isEqualToString:@""]==false)
    {
        CategoryProducts *categoryProducts = [[SharedData getCurrentStoryBoard]
                                              instantiateViewControllerWithIdentifier:@"CategoryProducts"];
        [categoryProducts
         setCategoryProductsCategoryId:notificationObject.notificationCategoryId
         andSubcategoryId:@"0"
         andSubcategoryName:@""];
        
        for (ProductObject *product in [SharedData getCategories]) {
            if([product.productId isEqualToString:notificationObject.notificationCategoryId])
            {
                categoryProducts.tabBarNames = product.productAdvertiseTypes;
                break;
            }
        }
        //[categoryProducts selectedTabFromSubMenu:strSubCatID withTabID:strTabID];
        [self.navigationController pushViewController:categoryProducts animated:YES];
    }
    else if (notificationObject.productId != nil&&[notificationObject.productId isEqualToString:@""]==false) {
        
        ProductDetailsViewController *productDetail =
        [[SharedData getCurrentStoryBoard]
         instantiateViewControllerWithIdentifier:
         @"ProductDetailsViewController"];
        [productDetail setProductId:notificationObject.productId];
        [self.navigationController pushViewController:productDetail animated:YES];
    } else {
        [self.notificationTableView reloadData];

        [DialogManager showDialogWithMessage:notificationObject.notificationInfo];
    }
    
    if([notificationObject.isRead isEqualToString:@"1"])
    {
        [ServerManager setNotificationRead:self withNotificationId:notificationObject.notificationId withRequestId:requestSetNotificationRead];
        notificationObject.isRead=@"0";
        
        selectedNotification = (int)indexPath.row;
    }
}
- (void)displayNotificationMessageOfFirebase:(NSString*) message andProductAdvertiseId:(NSString *)productId andCategoryId:(NSString *)categoryId andSubCategoryId:(NSString *)subCategoryId andTabId:(NSString *)tabId{
    [self.delegate
     receivedNewNotificiation:@"1" withNotificationMsg:message andProductAdvertiseId:productId andCategoryId:categoryId andIsBackendMessage:false andSubCategoryId:subCategoryId andTabId:tabId];
    
}

- (void)showOverlayWithIndicator:(BOOL)isShow {
    if (isShow) {
        isOverlayShown = true;
        [overlayView setHidden:NO];
        [activityIndicator startAnimating];
        
    } else {
        isOverlayShown = false;
        [overlayView setHidden:YES];
        [activityIndicator stopAnimating];
    }
}

- (void)startLoadingNotificationWithRequestId:(NSString *)requestId
                              andIsNewSession:(bool)isNewSession andNumberPerPage:(NSString*)numberPerPage {
    //    if (loadingFromServer == YES) {
    //        return;
    //    }
    
    loadingFromServer = YES;
    
    NSString *lastUpdateTimeToServer = @"0";
    NSString *pageNumberToServer = @"1";
    
    if ([requestId isEqualToString:requestGetInitialNotification]) {
        // page number initialized and lastupdate time initialize , also shared data
        // initialized so we can next pages right
        [SharedData setNotificationLastUpdateTime:@"0"];
        lastUpdateTimeToServer = @"0";
        self.pageNumber = 1;
        pageNumberToServer = @"1";
    } else if ([requestId isEqualToString:requestGetMoreNotification]) {
        // get the current page number adn current last update time
        lastUpdateTimeToServer = [SharedData getNotificationLastUpdateTime];
        pageNumberToServer = [NSString stringWithFormat:@"%i", self.pageNumber];
    } else if ([requestId isEqualToString:requestGetNewNotificationcount]) {
        // page number initialie and last update time initialize , shared data value
        // nto changed as this only for getting count shoudn't change page sequence
        lastUpdateTimeToServer = @"0";
        pageNumberToServer = @"1";
    }
    
    [ServerManager
     getNotificationWithCountryCode:[UserSetting getUserCountryCode]
     andNumber:[UserSetting getUserNumber]
     andLastUpdateTime:lastUpdateTimeToServer
     andPage:pageNumberToServer
     andNumberPerPage:numberPerPage
     andLanguage:[UserSetting getUserCurrentLanguage]
     andRequestId:requestId
     andDelegate:self];
}

#pragma mark - ServerManagerResponse Delegate
- (void)requestFinished:(ServerManagerResult *)serverManagerResult
            ofRequestId:(NSString *)requestId
    withNumberOfRequest:(int)numberOfRequest
{
    
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    
    loadingFromServer = NO;
    
    if(serverManagerResult.jsonArray.count==0)
    {
        loadMoreWhereAvailable=NO;
       
    }
       if ([requestId isEqualToString:requestGetInitialNotification])
       {
       if ([serverManagerResult.opertationResult
             isEqualToString:[ServerManager ERROR_IN_CONNECTION_OPERATION]]) {
            [DialogManager showErrorInConnection];
            return;
        } else if ([serverManagerResult.opertationResult
                    isEqualToString:[ServerManager FAILED_OPERATION]])
            
        {
            //Comment [DialogManager showErrorInServer];
            [DialogManager showErrorWithMessage:serverManagerResult.opertationMsg];
            return;
        }
        if ([serverManagerResult.opertationResult
             isEqualToString:[ServerManager SUCCESS_OPERATION]])
        {
                if (serverManagerResult.jsonArray.count == 0)
                {
                    [notificationArray removeAllObjects];
                    [self.notificationTableView reloadData];
                    _NoNotificationLable.hidden = NO;
                    return;
                }
            _NoNotificationLable.hidden = YES;
            [notificationArray removeAllObjects];
            [notificationArray addObjectsFromArray:serverManagerResult.jsonArray];
            [SharedData setNotificationCountInSharedData:serverManagerResult
             .notifcationCount];
            [SharedData
             setNotificationLastUpdateTime:serverManagerResult.lastUpdateTime];
            self.pageNumber++;
            [self.notificationTableView reloadData];
        }
       }
           else if([requestId isEqualToString:requestGetMoreNotification])
           {
                    if ([serverManagerResult.opertationResult
                         isEqualToString:[ServerManager ERROR_IN_CONNECTION_OPERATION]]) {
                        
                        [DialogManager showErrorInConnection];
                        
                        return;
                        
                    } else if ([serverManagerResult.opertationResult
                                isEqualToString:[ServerManager FAILED_OPERATION]])
                        
                    {
                        //Comment [DialogManager showErrorInServer];
                        [DialogManager showErrorWithMessage:serverManagerResult.opertationMsg];
                        return;
                    }
                    
                    if ([serverManagerResult.opertationResult
                         isEqualToString:[ServerManager SUCCESS_OPERATION]])
                    {
                        
                        _NoNotificationLable.hidden = YES;
                        
                        [notificationArray addObjectsFromArray:serverManagerResult.jsonArray];
                        
                        
                        [SharedData setNotificationCountInSharedData:serverManagerResult
                         .notifcationCount];
                        
                        [SharedData
                         setNotificationLastUpdateTime:serverManagerResult.lastUpdateTime];
                        
                        self.pageNumber++;
                        
                        [self.notificationTableView reloadData];
                        
                    }
            
    } else if ([requestId isEqualToString:requestGetNewNotificationcount]) {
        
        NotificationModal *notification=nil;
        
        if([SharedData getNotificationCountFromSharedData]!=nil)
        {
            if(([serverManagerResult.notifcationCount intValue] > [[SharedData getNotificationCountFromSharedData] intValue] )==true)
            {
                if(serverManagerResult.jsonArray.count>0)
                {
                    notification=[serverManagerResult.jsonArray objectAtIndex:0];
                }
            }
        }
        [SharedData
         setNotificationCountInSharedData:serverManagerResult.notifcationCount];
        
        
        [self.delegate
         receivedNewNotificiation:serverManagerResult.notifcationCount withNotificationMsg:notification.notificationInfo andProductAdvertiseId:notification.productId andCategoryId:notification.notificationCategoryId andIsBackendMessage:true andSubCategoryId:@"" andTabId:@"0"];
    }
    else if ([requestId isEqualToString:requestReadAllNotification])
    {
        NSString * str;
        if ([serverManagerResult.opertationResult
             isEqualToString:[ServerManager SUCCESS_OPERATION]]) {
            str = NSLocalizedString(@"SUCCESS_ADD", nil);
            [self startLoadingNotificationWithRequestId:requestGetInitialNotification
                                        andIsNewSession:YES andNumberPerPage:@"20"];
            return;
        } else if ([serverManagerResult.opertationResult
                    isEqualToString:[ServerManager FAILED_OPERATION]]) {
            str = NSLocalizedString(@"ERROR_ADD", nil);
        } else if ([serverManagerResult.opertationResult
                    isEqualToString:[ServerManager ALREADED_ADDED_OPERATION]]) {
            str = NSLocalizedString(@"ALREADY_ADD", nil);
        }
        UIAlertController * alert = [UIAlertController alertControllerWithTitle:str message:nil preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
            [self refreshNowNotificationCount];
        }]];
        
        [self presentViewController: alert animated:YES completion:^{
            
        }];
    }
    else if ([requestId isEqualToString:requestDeleteAllNotification])
    {
        NSString * str;

        if ([serverManagerResult.opertationResult
             isEqualToString:[ServerManager SUCCESS_OPERATION]])
        {
            str = NSLocalizedString(@"SUCCESS_ADD", nil);
            if ([notificationArray count] > 0)
            {
                [notificationArray removeAllObjects];
                _NoNotificationLable.hidden = NO;
            }
            [self.notificationTableView reloadData];
            [self refreshNowNotificationCount];
            return;
        } else if ([serverManagerResult.opertationResult
                    isEqualToString:[ServerManager FAILED_OPERATION]]) {
            str = NSLocalizedString(@"ERROR_ADD", nil);
        } else if ([serverManagerResult.opertationResult
                    isEqualToString:[ServerManager ALREADED_ADDED_OPERATION]]) {
            str = NSLocalizedString(@"ALREADY_ADD", nil);
        }
        UIAlertController * alert = [UIAlertController alertControllerWithTitle:str message:nil preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
            [self refreshNowNotificationCount];
        }]];
        
        [self presentViewController: alert animated:YES completion:^{
            
        }];
    }
    else if ([requestId isEqualToString:requestDeleteNotification])
    {
        NSString * str;
        if ([serverManagerResult.opertationResult
             isEqualToString:[ServerManager SUCCESS_OPERATION]]) {
            str = NSLocalizedString(@"SUCCESS_ADD", nil);
            [self startLoadingNotificationWithRequestId:requestGetInitialNotification
                                        andIsNewSession:YES andNumberPerPage:@"20"];
                 return;
        } else if ([serverManagerResult.opertationResult
                    isEqualToString:[ServerManager FAILED_OPERATION]]) {
            str = NSLocalizedString(@"ERROR_ADD", nil);
        } else if ([serverManagerResult.opertationResult
                    isEqualToString:[ServerManager ALREADED_ADDED_OPERATION]]) {
            str = NSLocalizedString(@"ALREADY_ADD", nil);
        }
        UIAlertController * alert = [UIAlertController alertControllerWithTitle:str message:nil preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
            [self refreshNowNotificationCount];
        }]];
        [self presentViewController: alert animated:YES completion:^{
        }];
    }
        else if ([requestId isEqualToString:requeststopNotificationRequest])
        {
            NSString * str;
            if ([serverManagerResult.opertationResult
                 isEqualToString:[ServerManager SUCCESS_OPERATION]])
            {
                str = serverManagerResult.opertationMsg;
            } else if ([serverManagerResult.opertationResult
                        isEqualToString:[ServerManager FAILED_OPERATION]]) {
                str = NSLocalizedString(@"ERROR_ADD", nil);
            } else if ([serverManagerResult.opertationResult
                        isEqualToString:[ServerManager ALREADED_ADDED_OPERATION]]) {
                str = NSLocalizedString(@"ALREADY_ADD", nil);
            }
            UIAlertController * alert = [UIAlertController alertControllerWithTitle:str message:nil preferredStyle:UIAlertControllerStyleAlert];
            [alert addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
            }]];
            
            [self presentViewController: alert animated:YES completion:^{
                
            }];
        }
        else if ([requestId isEqualToString:requestSetNotificationRead])
        {
            NSString * str;
            if ([serverManagerResult.opertationResult
                 isEqualToString:[ServerManager SUCCESS_OPERATION]]) {
                str = NSLocalizedString(@"SUCCESS_ADD", nil);
                
                NotificationModal *notificationObject =
                [notificationArray objectAtIndex:selectedNotification];
                notificationObject.isRead = @"0";
                [self.notificationTableView reloadData];
                
                //[self startLoadingNotificationWithRequestId:requestGetInitialNotification andIsNewSession:YES andNumberPerPage:@"20"];
                return;
            } else if ([serverManagerResult.opertationResult
                        isEqualToString:[ServerManager FAILED_OPERATION]]) {
                str = NSLocalizedString(@"ERROR_ADD", nil);
            } else if ([serverManagerResult.opertationResult
                        isEqualToString:[ServerManager ALREADED_ADDED_OPERATION]]) {
                str = NSLocalizedString(@"ALREADY_ADD", nil);
            }
            UIAlertController * alert = [UIAlertController alertControllerWithTitle:str message:nil preferredStyle:UIAlertControllerStyleAlert];
            [alert addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
                [self refreshNowNotificationCount];
            }]];
            [self presentViewController: alert animated:YES completion:^{
            }];
        }

}
    
    
-(IBAction)showOption:(UIButton *)sender
{
    id superView = sender.superview;
    while (superView && ![superView isKindOfClass:[UITableViewCell class]]) {
        superView = [superView superview];
    }
    NotificationViewCell * cell = (NotificationViewCell *)superView;
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Select_Option",nil) message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    alertController.view.backgroundColor = [UIColor whiteColor];
   
   
    
    NSIndexPath * indexPath = [self.notificationTableView indexPathForCell:cell];

    selectedNotification = (int) indexPath.row;
    NotificationModal *notificationObject =
    [notificationArray objectAtIndex:indexPath.row];
    
    UIAlertAction *cancelAction = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(@"Cancel", nil)
                                   style:UIAlertActionStyleDestructive
                                   handler:^(UIAlertAction *action)
                                   {
                                   }];
    
    UIAlertAction *notificationReadAction = [UIAlertAction
                                    actionWithTitle:NSLocalizedString(@"Mark_as_Read", nil)
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction *action)
                                    {
                                        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                                        [hud setLabelText:NSLocalizedString(@"LOADING_PLEASE_WAIT", Nil)];
                                        [ServerManager setNotificationRead:self withNotificationId:notificationObject.notificationId withRequestId:requestSetNotificationRead ];
                                    }];
    

    
    UIAlertAction *allReadAction = [UIAlertAction
                               actionWithTitle:NSLocalizedString(@"Mark_all_as_Read", nil)
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action)
                               {
                                   MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                                   [hud setLabelText:NSLocalizedString(@"LOADING_PLEASE_WAIT", Nil)];
                                   [ServerManager readAllNotification:self withRequestId:requestReadAllNotification];
                               }];
    
    
    UIAlertAction *deleteAction = [UIAlertAction
                                  actionWithTitle:NSLocalizedString(@"Delete_Notification", nil)
                                  style:UIAlertActionStyleDefault
                                  handler:^(UIAlertAction *action)
                                  {
                                      
                                      // Confirmation Alert For Delete
                                      [DialogManager showConfirmationDialogueWithDescription:[NSString stringWithFormat:NSLocalizedString(@"Do_you_want_to_delete_this_notification", nil)] andConfirmBtnText:NSLocalizedString(@"CONFIRMATION_DELETE_BTN_TEXT",nil) andDelegate:self
                                                                                andRequestId:requestDeleteNotification];
                                      
                                      
                                      
                                  }];
    UIAlertAction *deleteAllAction = [UIAlertAction
                                  actionWithTitle:NSLocalizedString(@"Delete_all_Notifications", nil)
                                  style:UIAlertActionStyleDefault
                                  handler:^(UIAlertAction *action)
                                  {
                                      // Confirmation Alert For Delete All
                                      [DialogManager showConfirmationDialogueWithDescription:[NSString stringWithFormat:NSLocalizedString(@"Do_you_want_to_delete_All_notifications", nil)] andConfirmBtnText:NSLocalizedString(@"CONFIRMATION_DELETE_BTN_TEXT",nil) andDelegate:self
                                       andRequestId:requestDeleteAllNotification];
                                      
                                  }];
    UIAlertAction *stopAction = [UIAlertAction
                                  actionWithTitle:NSLocalizedString(@"Stop_Notifications", nil)
                                  style:UIAlertActionStyleDefault
                                  handler:^(UIAlertAction *action)
                                  {
                                      [ServerManager stopNotificationOfProductId:notificationObject.productId
                                        andNotificationId:notificationObject.notificationId
                                        andRequestId:requeststopNotificationRequest
                                        andDelegate:self];
                                  }];
    [alertController addAction:notificationReadAction];
    [alertController addAction:allReadAction];
    [alertController addAction:deleteAction];
    [alertController addAction:deleteAllAction];
    [alertController addAction:stopAction];
    [alertController addAction:cancelAction];
    // Remove arrow from action sheet.
    [alertController.popoverPresentationController setPermittedArrowDirections:UIPopoverArrowDirectionAny];
    //For set action sheet to middle of view.
    CGRect rect = self.view.frame;
    CGRect buttonFrame = [cell convertRect:cell.bounds toView:self.view];
//    NSIndexPath *indexPath = [self.notificationTableView indexPathForRowAtPoint:buttonFrame.origin];
    rect.origin.x = buttonFrame.origin.x;
    rect.origin.y = buttonFrame.origin.y;
    alertController.popoverPresentationController.sourceView = self.view;
    alertController.popoverPresentationController.sourceRect = buttonFrame;

    
    
    UIView *view =[[UIView alloc]initWithFrame:CGRectMake(0.0, 0.0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height-350)];
    [view bringSubviewToFront:self.notificationTableView];
    
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(DismissAlertByTab)];
    [singleTap setNumberOfTapsRequired:1];
    [view addGestureRecognizer:singleTap];
    [self presentViewController: alertController animated: YES completion:^{
        alertController.view.superview.userInteractionEnabled = YES;
        [alertController.view.superview addSubview:view];
        [alertController.view addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget: self action: @selector(DismissAlertByTab)]];
    }];
    
    //[self presentViewController:alertController animated:YES completion:nil];
    alertController.view.tintColor = [UIColor blackColor];
    
}
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer
       shouldReceiveTouch:(UITouch *)touch {
    return YES;
}
- (void)DismissAlertByTab
{
    [self dismissViewControllerAnimated: YES completion: nil];
}
- (void)refreshNowNotificationCount {
    [self startLoadingNotificationWithRequestId:requestGetNewNotificationcount
                                andIsNewSession:YES andNumberPerPage:@"1"];
}

#pragma confirmation dialog buttons clicked events

- (void)confirmBtnClicked:(NSString *)requestId {
    if (requestId == requestDeleteAllNotification) {
            MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            [hud setLabelText:NSLocalizedString(@"DELETING_NOTIFICATION", Nil)];
            [ServerManager deleteAllNotification:self withRequestId:requestDeleteAllNotification];
        
    }else if (requestId == requestDeleteNotification) {
        NotificationModal *notificationObject = [notificationArray objectAtIndex:selectedNotification];
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [hud setLabelText:NSLocalizedString(@"DELETING_NOTIFICATION", Nil)];
        [ServerManager deleteNotification:notificationObject.notificationId andRequestId:requestDeleteNotification andDelegate:self];
    }
}


@end
