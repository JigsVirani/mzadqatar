//
//  ViewController.h
//  MazadQatar
//
//  Created by samo on 2/28/13.
//  Copyright (c) 2013 Mohammed abdulfattah attia. All rights reserved.
//

#import "GridView.h"
#import "DisplayUtility.h"
#import "MenuTableViewController.h"
#import "AboutScreen.h"
#import "Define.h"
#import "ViewAdvertiseLanguageChooserViewController.h"
#import "AppDelegate.h"
#import "AboutScreen.h"
#import "SocialShareUtility.h"
#import "ShareAppViewController.h"

#define requestGetCategory @"requestGetCategory"

@interface CategoryScreen
    : GridView <ServerManagerResponseDelegate, UIGestureRecognizerDelegate> {
  AppDelegate *appDelegate;
  UIView *my_view;
  UIImage *crossImg;
}
@property(nonatomic) BOOL isCroosedPressed;
@property(nonatomic, strong) UIPopoverController *popOverController;

- (void)startLoadingUserCategoryWithRequestId:(NSString *)requestId
                              andIsNewSession:(bool)isNewSession;

// IBAction

@end
