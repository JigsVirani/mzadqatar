//
//  SelectImageCellCollectionViewCell.h
//  Mzad Qatar
//
//  Created by Mohammed AbdulFattah on 1/7/15.
//  Copyright (c) 2015 Mohammed abdulfattah attia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SelectImageCellCollectionViewCell : UICollectionViewCell

@property(strong, nonatomic) IBOutlet UIImageView *imageOfSelection;
@property(strong, nonatomic)
    IBOutlet UIActivityIndicatorView *imageLoadingIndicator;

@end
