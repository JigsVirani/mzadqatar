//
//  GridView.m
//  MazadQatar
//
//  Created by samo on 3/8/13.
//  Copyright (c) 2013 Mohammed abdulfattah attia. All rights reserved.
//

#import "GridView.h"
#import "CategoryScreen.h"
#import "ServerManager.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "SharedGraphicInfo.h"
#import "DisplayUtility.h"
#import "ServerManagerResult.h"
#import "ProductGridCell.h"
#import <QuartzCore/QuartzCore.h>
#import "ProductDetailsViewController.h"
#import "AGPushNoteView.h"
#import "GAI.h"
#import "GAIDictionaryBuilder.h"

@interface GridView ()
@property(nonatomic, strong) NSString *language;
@end

NSString *PlainCellIdentifier = @"GridCell";
NSString *GridCellForVistiorIdentifier = @"GridCellForVistior";
NSString *AdsCellIdentifier = @"GridCellAdvetrtise";
NSString *OVERLAY_TAG = @"OVERLAY_TAG";

@implementation GridView

#define RADIANS(degrees) ((degrees * M_PI) / 180.0)

@synthesize productItems;

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"BACK", nil) style:UIBarButtonItemStylePlain target:nil action:nil];

    //self.language = [[[NSBundle mainBundle] preferredLocalizations] objectAtIndex:0];
    self.language = [LanguageManager currentLanguageCode];
    
    [self initVaraibales];
    
    CGSize size = [DisplayUtility getScreenSize];
    overlayView =
    [[UIView alloc] initWithFrame:CGRectMake(0, 0, size.width, size.height)];
    overlayView.backgroundColor =
    [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];
    activityIndicator = [[UIActivityIndicatorView alloc]
                         initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    activityIndicator.center = overlayView.center;
    [overlayView addSubview:activityIndicator];
    [self.navigationController.view addSubview:overlayView];
    [overlayView setHidden:YES];
    
//    UIImage *backgroundPattern = [UIImage imageNamed:@"bg.png"];
//    [self.collectionViewGridView
//     setBackgroundColor:[UIColor colorWithPatternImage:backgroundPattern]];
    
    [[_advertiseNowFreeBtn layer] setBorderWidth:1.0f];
    [[_advertiseNowFreeBtn layer] setBorderColor:[UIColor whiteColor].CGColor];
    if(IS_IPAD){
        _advertiseNowFreeBtn.titleLabel.font = [UIFont systemFontOfSize:22.0];
        _advertiseBtnWidthConstant.constant = 250.0;
    }
}

-(void)viewDidAppear:(BOOL)animated
{
    NotificationViewController *notificationController =
    [NotificationViewController getInstance];
    notificationController.delegate = self;
    
    
    [self receivedNewNotificiation:[SharedData
                                    getNotificationCountFromSharedData] withNotificationMsg:nil andProductAdvertiseId:nil andCategoryId:nil andIsBackendMessage:true andSubCategoryId:@"" andTabId:@"0"];
}
- (void)initVaraibales {
    // all screens is public for all user , except for user profile we should
    // check on vistor number same or no
    isProfileOwner = true;
    noMoreResultsAvail = NO;
    isHideDeleteBtn = YES;
    isPullNewData = false;
    // as in first time will be not count bec prevoius will be -1 so we start by 1
    // isContinueCountintRows=true;
    prevouseYPointValue = -1;
    
    if (categoryProductObject == nil) {
        categoryProductObject = [[ProductObject alloc] init];
    }
    
    if ([DisplayUtility isPad]) {
        self.advertiseResolution = [ServerManager RESOLUTION_LARGE];
    } else {
        self.advertiseResolution = [ServerManager RESOLUTION_MEDIUM];
    }
    
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(notificationbtn:)];
    tapGestureRecognizer.numberOfTapsRequired = 1;
    [self.notificationCountLbl addGestureRecognizer:tapGestureRecognizer];
    self.notificationCountLbl.userInteractionEnabled = YES;
    
    self.advertiseInOneRowCountString =
    [NSString stringWithFormat:@"%d", [SharedData numberOfAdvertiseInOneRow]];
    
    // setting new notification count
}

- (void)showOverlayWithIndicator:(BOOL)isShow {
    if (isShow) {
        isOverlayShown = true;
        [overlayView setHidden:NO];
        [activityIndicator startAnimating];
        
    } else {
        isOverlayShown = false;
        [overlayView setHidden:YES];
        [activityIndicator stopAnimating];
    }
}

- (void)addRefreshControlToGrid {
    refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self
                       action:@selector(pullNewData)
             forControlEvents:UIControlEventValueChanged];
    [self.collectionViewGridView addSubview:refreshControl];
    self.collectionViewGridView.alwaysBounceVertical = YES;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView
     numberOfItemsInSection:(NSInteger)section {
    int count = (int)[productItems count];
    
    return count;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)cv
                  cellForItemAtIndexPath:(NSIndexPath *)indexPath;
{
    if (loadMoreWhereAvailable) {
        
        if (indexPath.row >= (productItems.count * 0.40)) {
            
            if (!loading) {
                loading = YES;
                // loadRequest is the method that loads the next batch of data.
                BOOL myBool = NO;
                NSNumber *passedValue = [NSNumber numberWithBool:myBool];
                [self loadData:passedValue];
            }
        }
    }
    
    if (indexPath.row < productItems.count) {
        
        // start setting the grid view
        ProductObject *productObject = [productItems objectAtIndex:indexPath.row];
        
        ProductGridCell *cell;
        
        // reuse cell
        if (productObject.isAd == true) {
            
            //=================================================
            // Log setting open event with category="ui", action="open", and label="settings".
            id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
            [tracker send:
             [[GAIDictionaryBuilder createEventWithCategory:@"ui"
                                                     action:productObject.productTilte
                                                      label:@"advertisement"
                                                      value:nil] build]];
            //=================================================
            
            /*cell = [cv dequeueReusableCellWithReuseIdentifier:AdsCellIdentifier
                                                 forIndexPath:indexPath];
            if ([productObject.productAdvertiseUrl isEqual:@""] == false &&
                productObject.productAdvertiseUrl != nil) {
                [self hideAdvertiseLikeCommentPrice:true inCell:cell];
            } else {
                [self hideAdvertiseLikeCommentPrice:false inCell:cell];
            }
            if (isProfileOwner != true) {
                    [cell.btnEditPaidAdvertise setHidden:true];
                    [cell.imageEditPaidAdvertise setHidden:true];
                } else {
                    [cell.btnEditPaidAdvertise setHidden:false];
                    [cell.imageEditPaidAdvertise setHidden:false];                }
//            if ([productObject.productUserNumber
//                 isEqual:[UserSetting getUserNumber]] == false) {
//               
//            } else {}*/
            
            NSString *identifier = [NSString stringWithFormat:@"Identifier_%d-%d-%d", (int)indexPath.section, (int)indexPath.row, (int)indexPath.item];
            [cv registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:identifier];
            UICollectionViewCell *cellNew = [cv dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
            
            if(productObject.isLoadedAdd==false){
                
                DFPBannerView *bannerView;
                UIActivityIndicatorView *loadingIndicator = [[UIActivityIndicatorView alloc]init];
                UILabel *lblAdText;
                
                if([DisplayUtility isPad]){
                    
                    bannerView = [[DFPBannerView alloc]initWithFrame:CGRectMake(234,20,300,250)];
                    lblAdText = [[UILabel alloc]initWithFrame:CGRectMake(234, 5, 300, 15)];
                    
                    if([self.language isEqualToString:[ServerManager LANGUAGE_ARABIC]]) {
                        bannerView.adUnitID = @"/271990408/ProductPageiOSTabletAppMPUArabic";
                        lblAdText.text = @"اعلان";
                        [lblAdText setTextAlignment:NSTextAlignmentRight];
                    }else{
                        bannerView.adUnitID = @"/271990408/ProductPageiOSTabletAppMPUEnglish";
                        lblAdText.text = @"ADVERTISEMENT";
                        [lblAdText setTextAlignment:NSTextAlignmentLeft];
                    }
                }else{
                    if(cellNew.frame.origin.x==0){
                        bannerView = [[DFPBannerView alloc]initWithFrame:CGRectMake(10,20,300,250)];
                        lblAdText = [[UILabel alloc]initWithFrame:CGRectMake(10, 5, 300, 15)];
                    }else{
                        
                        if([self.language isEqualToString:[ServerManager LANGUAGE_ARABIC]]) {
                            bannerView = [[DFPBannerView alloc]initWithFrame:CGRectMake(17,20,300,250)];
                            lblAdText = [[UILabel alloc]initWithFrame:CGRectMake(17, 5, 300, 15)];
                        }else{
                            bannerView = [[DFPBannerView alloc]initWithFrame:CGRectMake(3,20,300,250)];
                            lblAdText = [[UILabel alloc]initWithFrame:CGRectMake(3, 5, 300, 15)];
                        }
                    }
                    
                    if([self.language isEqualToString:[ServerManager LANGUAGE_ARABIC]]) {
                        bannerView.adUnitID = @"/271990408/ProductPageiOSMobileAppMPUArabic";
                        lblAdText.text = @"اعلان";
                        [lblAdText setTextAlignment:NSTextAlignmentRight];
                    }else{
                        bannerView.adUnitID = @"/271990408/ProductPageiOSMobileAppMPUEnglish";
                        lblAdText.text = @"ADVERTISEMENT";
                        [lblAdText setTextAlignment:NSTextAlignmentLeft];
                    }
                }
                
                [loadingIndicator startAnimating];
                
                bannerView.rootViewController = self;
                [bannerView loadRequest:[DFPRequest request]];
                bannerView.tag = 111;
                
                lblAdText.textColor = [UIColor lightGrayColor];
                lblAdText.font = [UIFont systemFontOfSize:12.0 weight:UIFontWeightHeavy];
                
                [loadingIndicator setFrame:CGRectMake(bannerView.frame.origin.x+150,bannerView.frame.size.height/2.0, 20, 20)];
                
                [cellNew.contentView addSubview:lblAdText];
                [cellNew.contentView addSubview:loadingIndicator];
                [cellNew.contentView addSubview:bannerView];
                
                productObject.isLoadedAdd = true;
            }
            return cellNew;
            
        } else {
            if (isProfileOwner == true) {
                cell = [cv dequeueReusableCellWithReuseIdentifier:PlainCellIdentifier
                                                     forIndexPath:indexPath];
            } else {
                cell = [cv
                        dequeueReusableCellWithReuseIdentifier:GridCellForVistiorIdentifier
                        forIndexPath:indexPath];
            }
        }
        id isCompany=productObject.productUserisCompany;
        if( [isCompany isKindOfClass:[NSString class]]){
            if ([isCompany isEqualToString:@"0"] ) {
            
            cell.isCompanyView.layer.cornerRadius = 5.0f;
            cell.isCompanyView.layer.borderWidth = 1.0f;
            cell.isCompanyView.layer.borderColor = [UIColor colorWithRed:0.98 green:0.66 blue:0.19 alpha:1.0].CGColor;
            cell.isCompanyView.layer.masksToBounds = YES;
            cell.isCompanyView.hidden = NO;
        }else{
            cell.isCompanyView.hidden = YES;
        }
        }
        // set the image roundend
        if(![cell isKindOfClass:[ProductGridCell class]])
        {
            cell.productImageView.layer.cornerRadius = 3.0;
        }
        else
        {
            cell.adEdit.layer.cornerRadius = 3.0;
            cell.btnDeleteUnderAd.layer.cornerRadius = 3.0;
            cell.refreshAdvertiseBtn.layer.cornerRadius = 3.0;
        }
        cell.productImageView.clipsToBounds = YES;
        [cell setUserInteractionEnabled:true];
        
        if (productObject.isDeleting) {
            
            cell.btnDeleteUnderAd.hidden = YES;
            cell.deleteImage.hidden = YES;
            cell.iconUnderAd.hidden = YES;
            [cell.loadingIndicatorUnderDeleteAd startAnimating];
        } else {
            cell.btnDeleteUnderAd.hidden = NO;
            cell.deleteImage.hidden = NO;
            cell.iconUnderAd.hidden = NO;
            [cell.loadingIndicatorUnderDeleteAd stopAnimating];
        }
        
        if (productObject.isRefreshingAdvertise) {
            
            cell.refreshAdvertiseBtn.hidden = YES;
            cell.refreshAdvertiseImage.hidden = YES;
            [cell.refreshAdvertiseIndicator startAnimating];
        } else {
            cell.refreshAdvertiseBtn.hidden = NO;
            cell.refreshAdvertiseImage.hidden = NO;
            [cell.refreshAdvertiseIndicator stopAnimating];
        }
        
        if (productObject.isNeedToUploadedAdvertiseOnServer) {
            if (productObject.productUploadingPercent < 0.99) {
                // uploading in progress
                [cell.uploadingProgressPar setHidden:NO];
                [cell.productImageView setAlpha:0.5];
                [cell.uploadingProgressPar
                 setProgress:productObject.productUploadingPercent];
                [cell setUserInteractionEnabled:false];
            } else {
                // uploading finished
                [cell.productImageView setAlpha:1];
                [cell.uploadingProgressPar setHidden:YES];
                [cell setUserInteractionEnabled:true];
            }
            
            [cell.productImageView setImage:[productObject.productImagesOnDevices objectAtIndex:0]];
        } else {
            // load image using sdwebimage framework handles all tasks of image
            // download
            NSString *imageName =
            [[productObject productId] stringByAppendingString:@".png"];
            NSString *url = [productObject productMainImageUrl];
            [cell.uploadingProgressPar setHidden:YES];
            [cell.productImageView setAlpha:1];
            [cell.loadingIndicator startAnimating];
            
            // if image is null , it came as array type object , so we check by type
            // if its string
            if ([url isKindOfClass:[NSString class]]) {
                
                [cell.productImageView sd_setImageWithURL:[NSURL URLWithString:url] placeholderImage:[UIImage imageNamed:imageName] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                    [cell.loadingIndicator stopAnimating];
                }];
                
            } else {
                [cell.loadingIndicator stopAnimating];
            }
        }
        
        [cell setDelegate:self];
        
        // set the title
        [cell.productTitle setText:productObject.productTilte];
        
        if (productObject.isAd == true) {
            [cell.productPrice
             setText:[NSString
                      stringWithFormat:@"%@", productObject.productPrice]];
        } else {
            [cell.productPrice
             setText:[NSString
                      stringWithFormat:@"%@ %@", productObject.productPrice,
                      NSLocalizedString(@"PRICE_QR", nil)]];
        }
        
        [cell.lblLike setText:productObject.productLikeCount];
        [cell.lblComment setText:productObject.productCommentCount];
        
        /*if (cell.btnLike.titleLabel.text == nil) {
            [cell.lblLike setText:productObject.productLikeCount];
            [cell.lblComment setText:productObject.productCommentCount];
        }else
        {
            [cell.btnLike setTitle:productObject.productLikeCount forState:UIControlStateNormal];
            [cell.btnComment setTitle:productObject.productCommentCount forState:UIControlStateNormal];
        }*/
        
        // get count of advertise in row by checking how much y axis will be
        // repeated
        if ([SharedData isContinueCountintRows] == true) {
            if (prevouseYPointValue != -1) {
                if (cell.frame.origin.y == prevouseYPointValue) {
                    _advertiseInOneRowCount++;
                } else {
                    [SharedData setNumberOfAdvertiseInOneRow:_advertiseInOneRowCount];
                    [SharedData setIsContinueCountintRows:false];
                }
            }
            prevouseYPointValue = cell.frame.origin.y;
        }
        return cell;
    }
    return nil;
}

- (void)hideAdvertiseLikeCommentPrice:(BOOL)isHide
                               inCell:(ProductGridCell *)cell {
    [cell.btnLike setHidden:isHide];
    [cell.btnComment setHidden:isHide];
    [cell.lblLike setHidden:isHide];
    [cell.lblComment setHidden:isHide];
    [cell.productPrice setHidden:isHide];
}

//==============================animating grid
- (void)startAnimatingGrid {
    self.view.transform =
    CGAffineTransformRotate(CGAffineTransformIdentity, RADIANS(-1));
    
    [UIView animateWithDuration:0.25
                          delay:0.0
                        options:(UIViewAnimationOptionAllowUserInteraction |
                                 UIViewAnimationOptionRepeat |
                                 UIViewAnimationOptionAutoreverse)
                     animations:^{
                         self.view.transform = CGAffineTransformRotate(
                                                                       CGAffineTransformIdentity, RADIANS(1));
                     }
                     completion:NULL];
}

- (void)stopAnimatingGrid {
    [UIView animateWithDuration:0.25
                          delay:0.0
                        options:(UIViewAnimationOptionAllowUserInteraction |
                                 UIViewAnimationOptionBeginFromCurrentState |
                                 UIViewAnimationOptionCurveLinear)
                     animations:^{
                         self.view.transform = CGAffineTransformIdentity;
                     }
                     completion:NULL];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


- (void)viewDidUnload {
    
    // [self setEditBtn:nil];
    // [self setDoneBtn:nil];
    // [self setProductItems:nil];
    // [self setCollectionViewGridView:nil];
    
    [super viewDidUnload];
}

- (IBAction)addAdvertiseClicked:(id)sender {
    if ([UserSetting isUserRegistered] == false) {
        [SharedViewManager showLoginScreen:self];
    } else {
        [SharedViewManager showAddAdvertiseScreen:self];
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier]
         isEqualToString:[SharedViewManager addAdvertisSegue]]) {
        ProductDetailsViewController *productDetails =
        [segue destinationViewController];
        [productDetails setIsAddProductDetail:YES];
    } else if ([[segue identifier]
                isEqualToString:[SharedViewManager registerSegue]]) {
        RegisterScreen *registerScreen = [segue destinationViewController];
        
        [registerScreen
         setSuccessScreenSegueName:[SharedViewManager addAdvertisSegue]];
    }
}

///=============================================Referesh pulling
- (void)pullNewData {
    
    isPullNewData = true;
    
    [self getNewData:@""];
    
    isPullNewData = false;
}

//=============================================edit and done btn

- (IBAction)editBtnClicked:(id)sender {
    
    self.editBtn.hidden = YES;
    self.doneBtn.hidden = NO;
    
    isHideDeleteBtn = NO;
    
    [self startAnimatingGrid];
    
    [self.collectionViewGridView reloadData];
}

- (void)getNewData:(NSString *)searchStr {
}

- (void)loadData:(NSNumber *)isNewSession {
}

- (void)cellDeleted:(UICollectionViewCell *)cell {
}

//- (UICollectionReusableView *)collectionView:(UICollectionView
//*)collectionView viewForSupplementaryElementOfKind:(NSString *)kind
//atIndexPath:(NSIndexPath *)indexPath
//{
//    UICollectionReusableView *reusableview = nil;
//
//    if (kind == UICollectionElementKindSectionHeader) {
//
//        UICollectionReusableView *headerView = [collectionView
//        dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader
//        withReuseIdentifier:@"HeaderView" forIndexPath:indexPath];
//
//        if([headerView isKindOfClass:[CategoryProductHeader class]])
//        {
//            [(CategoryProductHeader*)headerView
//            setCategoryProductHeaderDelegate:categoryProductHeaderDelegate];
//            [(CategoryProductHeader*)headerView setParentViewController:self];
//
//            [(CategoryProductHeader*)headerView
//            setProductsSubCategoryId:categoryProductObject.productSubCategoryId];
//            [(CategoryProductHeader*)headerView
//            setCurrentCategoryId:categoryProductObject.productCategoryId];
//
//            [(CategoryProductHeader*)headerView setSortById:_soryById];
//            [(CategoryProductHeader*)headerView setSortByIndex:_sortByIndex];
//
//            [[(CategoryProductHeader*)headerView productLangButton]
//            setTitle:[UserSetting
//            getUserFilterViewAdvertiseLanguageBtnParam:[UserSetting
//            getUserFilterViewAdvertiseLanguage]]
//            forState:UIControlStateNormal];
//            [[(CategoryProductHeader*)headerView subCategoryButton]
//            setTitle:categoryProductObject.productSubCategoryName
//            forState:UIControlStateNormal];
//            [[(CategoryProductHeader*)headerView sortByButton]
//            setTitle:_soryByName forState:UIControlStateNormal];
//
//            if(isDisableCategoryProductButton==true)
//            {
//                [[(CategoryProductHeader*)headerView subCategoryButton]
//                setEnabled:false];
//            }
//
//            [[(CategoryProductHeader*)headerView searchBar]
//            setText:self.searchString];
//        }
//        reusableview = headerView;
//    }
//
//    return reusableview;
//}

//- (CGSize)collectionView:(UICollectionView *)collectionView
//layout:(UICollectionViewLayout*)collectionViewLayout
//referenceSizeForHeaderInSection:(NSInteger)section
//{
//    if(isHideBar==false)
//    {
//        return CGSizeMake(collectionView.frame.size.width,
//        collectionViewHeaderHeight);
//    }
//    return CGSizeZero;
//}

- (IBAction)doneBtnClicked:(id)sender {
    self.editBtn.hidden = NO;
    self.doneBtn.hidden = YES;
    
    isHideDeleteBtn = YES;
    
    [self stopAnimatingGrid];
    
    [self.collectionViewGridView reloadData];
}

- (void)addLabelToMiddleWithText:(NSString *)text
                inCollectionView:(BOOL)isInCollectionView {
    if (collectionMiddleLabel != nil) {
        //[collectionMiddleLabel removeFromSuperview];
        [self removeLabelfromMiddleOfView];
    } else {
        collectionMiddleLabel = [[UILabel alloc]
                                 initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width,
                                                          self.view.bounds.size.height)];
        collectionMiddleLabel.textAlignment = NSTextAlignmentCenter;
        collectionMiddleLabel.backgroundColor = [UIColor clearColor];
        collectionMiddleLabel.font = [UIFont fontWithName:@"Helvetica" size:24];
        [collectionMiddleLabel setTextColor:[UIColor whiteColor]];
        [collectionMiddleLabel
         setFont:[UIFont fontWithName:@"HelveticaNeueLTStd-ThCn" size:34]];
    }
    
    if (isInCollectionView) {
        [self.collectionViewGridView addSubview:collectionMiddleLabel];
        //[self setLabelInTheMiddleOFView:true];
    } else {
        [self.view addSubview:collectionMiddleLabel];
        [self setLabelInTheMiddleOFView:false];
    }
    collectionMiddleLabel.text = text;
}

- (void)setLabelInTheMiddleOFView:(BOOL)isCollectionView {
    collectionMiddleLabel.translatesAutoresizingMaskIntoConstraints = NO;
    
    if (isCollectionView == false) {
        [self.view addConstraint:[NSLayoutConstraint
                                  constraintWithItem:collectionMiddleLabel
                                  attribute:NSLayoutAttributeCenterX
                                  relatedBy:NSLayoutRelationEqual
                                  toItem:self.collectionViewGridView
                                  attribute:NSLayoutAttributeCenterX
                                  multiplier:1.
                                  constant:0]];
        
        [self.view addConstraint:[NSLayoutConstraint
                                  constraintWithItem:collectionMiddleLabel
                                  attribute:NSLayoutAttributeCenterY
                                  relatedBy:NSLayoutRelationEqual
                                  toItem:self.collectionViewGridView
                                  attribute:NSLayoutAttributeCenterY
                                  multiplier:1.
                                  constant:0]];
    }
}

- (void)willRotateToInterfaceOrientation:
(UIInterfaceOrientation)toInterfaceOrientation
                                duration:(NSTimeInterval)duration {
    // only in case label is in collection view we will reposition it manually
    // as there is problem in constraint in collection view only
    if ([collectionMiddleLabel.superview
         isKindOfClass:[UICollectionView class]]) {
        if (toInterfaceOrientation == UIInterfaceOrientationPortrait ||
            toInterfaceOrientation == UIInterfaceOrientationPortraitUpsideDown) {
            if ([DisplayUtility isPad]) {
                // [collectionMiddleLabel setFrame:CGRectMake(-120, 50,
                // collectionMiddleLabel.frame.size.width,
                // collectionMiddleLabel.frame.size.height)];
                [collectionMiddleLabel
                 setCenter:CGPointMake(self.collectionViewGridView.center.x - 120,
                                       self.collectionViewGridView.center.y + 50)];
            } else {
                [collectionMiddleLabel
                 setCenter:CGPointMake(self.collectionViewGridView.center.x - 80,
                                       self.collectionViewGridView.center.y + 50)];
            }
        } else {
            if ([DisplayUtility isPad]) {
                // [collectionMiddleLabel setFrame:CGRectMake(130, -150,
                // collectionMiddleLabel.frame.size.width,
                // collectionMiddleLabel.frame.size.height)];
                [collectionMiddleLabel
                 setCenter:CGPointMake(self.collectionViewGridView.center.x + 130,
                                       self.collectionViewGridView.center.y - 150)];
            } else {
                
                [collectionMiddleLabel
                 setCenter:CGPointMake(self.collectionViewGridView.center.x + 80,
                                       self.collectionViewGridView.center.y - 100)];
            }
        }
    }
    
    [super willRotateToInterfaceOrientation:toInterfaceOrientation
                                   duration:duration];
}

- (void)removeLabelfromMiddleOfView {
    if (collectionMiddleLabel != nil) {
        [collectionMiddleLabel removeFromSuperview];
    }
}

//======================baackground listner
- (void)addBackgroundListner {
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(appWillResignActive:)
     name:UIApplicationWillResignActiveNotification
     object:nil];
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(appWillTerminate:)
     name:UIApplicationWillTerminateNotification
     object:nil];
}

- (void)appWillResignActive:(NSNotification *)note {
    [self doneBtnClicked:NULL];
}

- (void)appWillTerminate:(NSNotification *)note {
    [[NSNotificationCenter defaultCenter]
     removeObserver:self
     name:UIApplicationWillResignActiveNotification
     object:nil];
    [[NSNotificationCenter defaultCenter]
     removeObserver:self
     name:UIApplicationWillTerminateNotification
     object:nil];
}

#pragma mark - upper bar
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    isPullNewData = true;
    
    [self getNewData:searchBar.text];
    
    isPullNewData = false;
    [self showSearchBar:true];
}
- (IBAction)showSearchBarClicked:(id)sender {
    // in ipad , if the view already shown and click again on search icon , we
    // hide it
    
    if ([DisplayUtility isPad] == true) {
        if ([self.viewOfSearchBar isHidden] == false) {
            [self showSearchBar:true];
            return;
        }
    }
    
    [self showSearchBar:false];
}
- (IBAction)cancelSearchBar:(id)sender {
    [self showSearchBar:true];
}

- (void)showSearchBar:(BOOL)isHideSearchBar {
    
    if(isHideSearchBar)
    {
        //reset search in case of search inside category only when cacnel btn clicked
        if(isCatgoryProductScreen && [categoryProductObject.productCategoryId isEqualToString:@"0"]==false)
        {
            self.searchString=@"";
            [self getNewData:@""];
        }
        [self.searchAdsBar endEditing:YES];
    }
    else
    {
        if ([self.searchAdsBar isFirstResponder]) {
            [self.searchAdsBar resignFirstResponder];
        } else {
            [self.searchAdsBar becomeFirstResponder];
        }
    }
    if ([DisplayUtility isPad] == true) {
        self.searchAdsBar.hidden = YES;
        self.cancelSearchBtn.hidden = YES;
        // animation
        [UIView transitionWithView:self.viewOfSearchBar
                          duration:0.1
                           options:UIViewAnimationOptionTransitionCrossDissolve
                        animations:NULL
                        completion:NULL];
        
        self.viewOfSearchBar.hidden = isHideSearchBar;
    } else {
        // animation
        [UIView transitionWithView:self.searchAdsBar
                          duration:0.1
                           options:UIViewAnimationOptionTransitionCrossDissolve
                        animations:NULL
                        completion:NULL];
        
        self.searchAdsBar.hidden = isHideSearchBar;
        
        // animation
        [UIView transitionWithView:self.cancelSearchBtn
                          duration:0.1
                           options:UIViewAnimationOptionTransitionCrossDissolve
                        animations:NULL
                        completion:NULL];
        
        self.cancelSearchBtn.hidden = isHideSearchBar;
        //self.sideMenuBtn.hidden = !isHideSearchBar;
        if([self.notificationCountLbl.text isEqualToString:@"0"]==false && [self.notificationCountLbl.text isEqualToString:@"٠"]==false)
        {
            self.notificationCountLbl.hidden = !isHideSearchBar;
        }
        else
        {
            self.notificationCountLbl.hidden=YES;
        }
        self.searchIconBtn.hidden = !isHideSearchBar;
        self.notificationBtn.hidden = !isHideSearchBar;
        self.advertiseNowFreeBtn.hidden = !isHideSearchBar;
        self.backBtnText.hidden = !isHideSearchBar;
        self.backBtnArrow.hidden = !isHideSearchBar;
    }
}
- (void)receivedNewNotificiation:(NSString *)newNotificationCount withNotificationMsg:(NSString *)notificationMsg andProductAdvertiseId:(NSString *)productId andCategoryId:(NSString *)categoryId andIsBackendMessage:(BOOL) isHassanMessage andSubCategoryId:(NSString *)subCategoryId andTabId:(NSString *)tabId {
    
    if([newNotificationCount isEqualToString:@"0"]||[newNotificationCount isEqualToString:@"٠"]||self.notificationBtn.isHidden==true||newNotificationCount==nil)
    {
        [self.notificationCountLbl setHidden:YES];
    }
    else
    {
        if(notificationMsg!=nil)
        {
            [AGPushNoteView showWithNotificationMessage:notificationMsg];
            [AGPushNoteView setMessageAction:^(NSString *message) {
                if (productId != nil && ![productId isEqualToString:@""]) {
                    [SharedViewManager showProductDetailView:productId fromView:self];
                }else if(categoryId != nil && ![categoryId isEqualToString:@""]){
                    
                    CategoryProducts *categoryProducts = [[SharedData getCurrentStoryBoard]
                                                          instantiateViewControllerWithIdentifier:@"CategoryProducts"];
                    if(subCategoryId!= nil && ![subCategoryId isEqualToString:@""] && tabId != nil && ![tabId isEqualToString:@"0"]){
                        [categoryProducts
                         setCategoryProductsCategoryId:categoryId
                         andSubcategoryId:subCategoryId
                         andTabId:tabId];
                        
                    }else{
                        [categoryProducts
                         setCategoryProductsCategoryId:categoryId
                         andSubcategoryId:@"0"
                         andSubcategoryName:@""];
                    }
                   
                    
                    for (ProductObject *product in [SharedData getCategories]) {
                        if([product.productId isEqualToString:categoryId])
                        {
                            categoryProducts.tabBarNames = product.productAdvertiseTypes;
                            break;
                        }
                    }
                    [self.navigationController pushViewController:categoryProducts animated:YES];
                    
                } else {
                    [DialogManager showDialogWithMessage:notificationMsg];
                }
            }];
        }
        
        if(self.searchAdsBar.hidden==true&&isProfileOwner==true)
        {
            [self.notificationCountLbl setHidden:NO];
        }
        if(isHassanMessage)
            self.notificationCountLbl.text = newNotificationCount;
    }
    // self.notificationCountLbl.hidden=NO;
    
    [[UIApplication sharedApplication]
     setApplicationIconBadgeNumber:[newNotificationCount intValue]];
    
}

- (IBAction)notificationbtn:(id)sender {
    
    NotificationViewController *controller =
    (NotificationViewController *)[[SharedData getCurrentStoryBoard]
    instantiateViewControllerWithIdentifier:@"NotificationView"];
    [self.navigationController pushViewController:controller animated:YES];
}

// we didnt use navigation back btn as we use custom view in bar that cause
// issue with the navigation back btn
- (IBAction)backButtonPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

-(NSMutableArray*)getTabBarSearchAllCategories
{
    AdvertiseType *allTypes=[[AdvertiseType alloc]init];
    allTypes.advertiseTypeId=@"";
    allTypes.advertiseTypeName=NSLocalizedString(@"ALL_TYPES", nil);
    allTypes.advertiseTypeDefaultView=@"grid";
    
    return  [NSMutableArray arrayWithObject:allTypes];
}

- (void)addAdvertiseClicked
{
    [self addAdvertiseClicked:nil];
}

- (IBAction)menuButtonPressed:(id)sender {
    
    // Hide CM Pop TipVIew
    //  [_navBarLeftButtonPopTipView dismissAnimated:YES];
    //  [_navBarRightButtonPopTipView dismissAnimated:YES];
    //[self.revealViewController setMenuDelegate:self];
    //[self.revealViewController revealToggleAnimated:YES];
    
    [self.revealViewController setMenuDelegate:self];
    
    if([[SharedData getDeviceLanguage]isEqualToString:@"en"]){
        [self.revealViewController revealToggleAnimated:YES];
    }else{
        [self.revealViewController rightRevealToggleAnimated:YES];
    }
}
@end
