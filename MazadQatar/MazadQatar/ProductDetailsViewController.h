//
//  ProductDetailsViewController.h
//  MazadQatar
//
//  Created by samo on 4/17/13.
//  Copyright (c) 2013 Mohammed abdulfattah attia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MoreDescriptionViewController.h"
#import "ProductObject.h"
#import "ImageViewerTableCell.h"
#import "ImagePreviewerTableCell.h"
#import "ProductInfoTableCell.h"
#import "ProductInfoViewerTableCell.h"
#import "CommentInfoTableCell.h"
#import "SendCommentTableCell.h"
#import "ProductDetailActionBarCell.h"
#import "CommentInfoActionBarCell.h"
#import "NewProductInfoViewerTableCell.h"
#import "NewCommentInfoTableCell.h"
#import "NewSendCommentTableCell.h"
#import "NewDescriptionTableViewCell.h"
#import "ServerManager.h"
#import "ProductCommentObject.h"
#import "RegisterScreen.h"
#import "UploadingProductsManager.h"
#import "SystemUtility.h"
#import "ConfirmationDialogueViewController.h"
#import "RelatedAdsTableViewCell.h"
#import "UserProfileCell.h"

#import "GAITrackedViewController.h"
#import "AddAsCompanyTableViewCell.h"
#import <ctype.h>
#import "CustomTableViewCell.h"
#import "DisplayUtility.h"

@interface ProductDetailsViewController
    : GAITrackedViewController <
ServerManagerResponseDelegate, NewSendCommentDelegate,SendCommentDelegate,
CommentInfoActionBarDelegate,MFMessageComposeViewControllerDelegate,UIAlertViewDelegate, ImagePreviewerDelegate,RelatedAdsTableViewCellDelegate,
ImageViewerDelegate,ProductInfoTableCellDelegate, NewCommentInfoTableCellDelegate,UITextFieldDelegate,ConfirmationDialogueViewControllerDelegate,UITableViewDataSource,UITableViewDelegate,MFMailComposeViewControllerDelegate,ProductNewDescriptionTableViewCell,UITextViewDelegate,CommentInfoActionBarDelegate> {
  bool isCommentLoading;
  bool isProductDetailLoading;
    ImageViewerTableCell *imageViewTableCell;
    AddAsCompanyTableViewCell *addingAsCompanyTableCell;
    
    ProductInfoViewerTableCell *productInfoViewerTableCell;
    CommentInfoTableCell *commentInfoCell;
    SendCommentTableCell * writeCommentCellTable;
    CustomTableViewCell * objCustomTableViewCell;
  ImagePreviewerTableCell * imagePreviewTableCell;
  RelatedAdsTableViewCell * relateAdTableCell;
  ProductInfoTableCell *productInfoCell;
  NewProductInfoViewerTableCell * newproductInfoViewerTableCell;
  NewCommentInfoTableCell *newCommentInfoCell;
  NewSendCommentTableCell *newWriteCommentCellTable;
//  UserProfileCell *userProfileCell;
  CGSize keyboardSize;
  UIActivityIndicatorView *moreBtnIndicator;
  UITextField *moreBtnResultTextField;
              UIButton * moreBtn;
  bool isMoreCommentAllowed;
  bool isAddAdcertiseBtnClicked;

  int totalCountOfCells;
  NSString *productCommentlastUpdateTime;
  int productCommentsPageNumber;
  int productCommentsNumberOfPages;
    NSMutableArray * relatedAdsArry;
}
@property (strong, nonatomic) NSString *userEmail;
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property(strong,nonatomic) ProductObject *productObject;
@property(assign, nonatomic) bool isAddProductDetail;
@property(assign, nonatomic) bool isEditAdvertise;
@property(assign, nonatomic) bool isFromCategoryProducts;

@property(strong, nonatomic) IBOutlet UIButton *chatBtn;
@property(strong, nonatomic) IBOutlet UIButton *callBtn;
@property(strong, nonatomic) IBOutlet UIButton *smsBtn;

- (IBAction)callBtnClicked:(id)sender;
- (IBAction)messagBtnClicked:(id)sender;
- (IBAction)backButtonPressed:(id)sender;

- (UIColor *)colorFromHexString:(NSString *)hexString;

//-(void)getProductComments:(NSString*)requestId;

- (void)updateProductObject:(ProductObject *)productObjectParam
        andIsAppendComments:(bool)isAppendComments
         andIsDeleteComment:(bool)isDeleteComment;

- (UITableViewCell *)loadImageViewCell:(UITableView *)tableView
                             withIndex:(NSIndexPath *)indexPath;
- (UITableViewCell *)loadProductInfoCell:(UITableView *)tableView
                               withIndex:(NSIndexPath *)indexPath;

- (void)setProductId:(NSString *)productId;

- (CGFloat)returnImageViewerCellHeight:(UITableView *)tableView
                         withIndexPath:(NSIndexPath *)indexPath;

- (CGFloat)returnProductInfoCellHeight:(UITableView *)tableView
                         withIndexPath:(NSIndexPath *)indexPath;


- (CGFloat)returnWriteCommentCellHeight:(UITableView *)tableView
                          withIndexPath:(NSIndexPath *)indexPath;



- (int)getTableCellsCount;

- (void)loadData;

-(void)showProductWithId:(NSString *)productId;

-(void)createChatFromAdOwnerSideWithNumber:(NSString*)strCommenterNumber commenterName:(NSString*)strCommenterName commenterPhoto:(NSString*)strCommenterPhoto;
- (void)
moreClickedFinishedWithLoadingIndicator:(UIActivityIndicatorView *)indicator
andResultTextField:(UITextField *)resultTextFieldParam andButton:(UIButton *)moreButton;
@end
