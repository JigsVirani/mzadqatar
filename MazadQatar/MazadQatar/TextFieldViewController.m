//
//  TextFieldViewController.m
//  MazadQatar
//
//  Created by samo on 4/2/13.
//  Copyright (c) 2013 Mohammed abdulfattah attia. All rights reserved.
//

#import "TextFieldViewController.h"
#import "DisplayUtility.h"
#import "AppDelegate.h"

@interface TextFieldViewController ()

@end

@implementation TextFieldViewController
@synthesize  isCommnetController;
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self registerForKeyboardNotifications];
    // Do any additional setup after loading the view.
}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    [self createKeyboardAccessoryView];
    
    self.currentController=textView;
    
    return true;
}
- (void)textViewDidEndEditing:(UITextView *)textView
{
    self.currentController=nil;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    [self createKeyboardAccessoryView];
    
    self.currentController=textField;
}


- (void)textFieldDidEndEditing:(UITextField *)textField {
    self.currentController=nil;
}

#pragma mark : --- Textfield Delegate method


- (BOOL)textFieldShouldClear:(UITextField *)textField {
     self.isCommnetController = false;
    [textField resignFirstResponder];
    return true;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    self.isCommnetController = false;
    [textField resignFirstResponder];
    return true;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)createInputAccessoryView {
    
    CGSize screenSize = [DisplayUtility getScreenSize];
    
    if (_inputAccView == nil) {
        _inputAccView = [[UIView alloc]
                         initWithFrame:CGRectMake(10.0, 0.0, screenSize.width, 40.0)];
        // Set the view’s background color. We’ ll set it here to gray. Use any
        // color you want.
        
        [_inputAccView setBackgroundColor:[UIColor colorWithRed:(87.0 / 255.0)
                                                          green:(95.0 / 255.0)
                                                           blue:(109.0 / 255.0)
                                                          alpha:0.8]];
        
        NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
        [nc addObserver:self
               selector:@selector(orientationChanged:)
                   name:UIApplicationDidChangeStatusBarOrientationNotification
                 object:nil];
    }
    
    //if (_btnDone == nil) {
        _btnDone = [UIButton buttonWithType:UIButtonTypeCustom];
        [_btnDone setFrame:CGRectMake(screenSize.width - 80, 3, 80.0f, 40.0f)];
        [_btnDone setTitle:NSLocalizedString(@"DONE", nil)
                  forState:UIControlStateNormal];
        
        UIImage *backgroundPattern = [UIImage imageNamed:@"btn_advertise.png"];
        [_btnDone setBackgroundImage:backgroundPattern
                            forState:UIControlStateNormal];
        
        // [btnDone setBackgroundColor:[UIColor blackColor]];
        [_btnDone setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_btnDone addTarget:self
                     action:@selector(doneTyping)
           forControlEvents:UIControlEventTouchUpInside];
        
        [_inputAccView addSubview:_btnDone];
    //}
}

- (void)createKeyboardAccessoryView {
    if ([DisplayUtility isPad] == false) {
        // Call the createInputAccessoryView method we created earlier.
        // By doing that we will prepare the inputAccView.
        [self createInputAccessoryView];
        [self createKeyboardAccessoryViewInChild];
    }
}

- (void)doneTyping {
    [_btnDone removeFromSuperview];
}

- (void)createKeyboardAccessoryViewInChild {
}


- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardWillShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
    }


// Called when the UIKeyboardDidShowNotification is sent.
- (void)keyboardWasShown:(NSNotification*)aNotification
{
    self.currentContentInset= self.scrollView.scrollIndicatorInsets;
    
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    UIEdgeInsets contentInsets;
    

        contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);


    self.scrollView.contentInset = contentInsets;

        self.scrollView.scrollIndicatorInsets = contentInsets;
    
    // If active text field is hidden by keyboard, scroll it so it's visible
    // Your app might not need or want this behavior.
    CGRect aRect = self.view.frame;
    aRect.size.height -= kbSize.height;
    
  
    
    if (!CGRectContainsPoint(aRect, self.currentController.frame.origin) ) {
        [self.scrollView scrollRectToVisible:self.currentController.frame animated:YES];
    }
}

// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    self.scrollView.contentInset = self.currentContentInset;
    self.scrollView.scrollIndicatorInsets = self.currentContentInset;
}



@end
