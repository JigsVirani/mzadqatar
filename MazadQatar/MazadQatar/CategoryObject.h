//
//  Category.h
//  Mzad Qatar
//
//  Created by Waris Ali on 04/06/2014.
//  Copyright (c) 2014 Mohammed abdulfattah attia. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CategoryObject : NSObject <NSCoding>

@property(nonatomic, strong) NSString *productWebsiteUrl;
@property(nonatomic, strong) NSString *productID;
@property(nonatomic, strong) NSString *categoryID;
@property(nonatomic, strong) NSString *categoryName;
@property(nonatomic, strong) NSString *categoryImageURL;
@property(nonatomic, strong) NSMutableArray *categorySubcategories;
@property(nonatomic, strong) NSString *subCategoryParent;
@property(nonatomic, assign) BOOL isCollapsed;
@property(nonatomic, strong) NSString *isCategory;
@property(nonatomic, strong) NSMutableArray *productAdvertiseTypes;
@property(nonatomic, strong) NSString *categoryFullWidthImageURL;
@end
