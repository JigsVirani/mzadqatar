//
//  AdvertiseReportDialogueViewController.h
//  Mzad Qatar
//
//  Created by Waris Ali on 25/12/2014.
//  Copyright (c) 2014 Mohammed abdulfattah attia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OtherReasonTableViewCell.h"
#import "GoogleAnalyticsTableViewController.h"
#import "DisplayUtility.h"

@protocol ReportItemViewControllerDelegate <NSObject>
- (void)reportAdWithReasonWithReasonID:(NSString *)reasonID
                         andReasonText:(NSString *)reasonText;
@end

@interface ReportItemViewController
    : GoogleAnalyticsTableViewController <
                             OtherReasonTableViewCellDelegate> {
  NSString *reasonID;
  NSString *reasonText;
  int currentSelectedIndex;
}

@property(nonatomic, strong) id<ReportItemViewControllerDelegate> delegate;
@property(strong, nonatomic) IBOutlet UITableView *tableOfChoices;

@end
