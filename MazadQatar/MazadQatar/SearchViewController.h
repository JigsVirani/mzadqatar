//
//  SearchViewController.h
//  Mzad Qatar
//
//  Created by Paresh Vasoya on 31/01/17.
//  Copyright © 2017 Mohammed abdulfattah attia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWRevealViewController.h"

@protocol CategoryProductsDelegate <NSObject>
- (void)refreshProductListPage:(NSString *)strTitle withCategoryId:(NSString *)subCategoryId withCategoryId:(NSString *)strCategoryId withTypeId:(NSString *)strTypeId;
@end

@interface SearchViewController : UIViewController

@property (nonatomic, strong) id delegate;
@property (nonatomic, strong) NSString *strId;
@property (nonatomic, strong) NSString *strCategories;


@end
