//
//  DeepLinkVC.m
//  Mzad Qatar
//
//  Created by GuruUgam on 8/15/16.
//  Copyright © 2016 Mohammed abdulfattah attia. All rights reserved.
//

#import "DeepLinkVC.h"

@interface DeepLinkVC ()

@end

@implementation DeepLinkVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self loadWebviewData];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(void)loadWebviewData{
    
    [objWebview loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:self.strUrl]]];
}
@end
