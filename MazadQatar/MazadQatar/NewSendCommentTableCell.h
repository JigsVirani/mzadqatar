//
//  SendCommentTableCell.h
//  MazadQatar
//
//  Created by samo on 4/17/13.
//  Copyright (c) 2013 Mohammed abdulfattah attia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ServerManager.h"
#import "ProductObject.h"
#import "TextViewTableCell.h"
#import "SharedViewManager.h"
#import "DialogManager.h"
#import "NewSendCommentTableCell.h"
#import "StringUtility.h"
#import "EnterNameViewController.h"
// Delgate
@class NewSendCommentTableCell;
@protocol NewSendCommentDelegate <NSObject>
@required
- (void)commentSent;
@required
@end

@interface NewSendCommentTableCell
: TextViewTableCell <ServerManagerResponseDelegate,UIAlertViewDelegate,EnterNameViewControllerDelegate> {
    ProductObject *productObject;
    id<NewSendCommentDelegate> delegate;
}
@property(strong, nonatomic)
IBOutlet UIActivityIndicatorView *sendCommentIndicator;
@property (weak, nonatomic) IBOutlet UITextView *userComment;

//@property (weak, nonatomic) IBOutlet UITextView *userComment;
@property(strong, nonatomic) IBOutlet UILabel *sendBtnResultText;
@property(strong, nonatomic) IBOutlet UIButton *sendBtn;
- (IBAction)sendBtnClicked:(id)sender;
- (void)setProductOfComment:(ProductObject *)productObjectParam
               withDelegate:(id<NewSendCommentDelegate>)delegateParam
           inViewController:(UIViewController *)view;

- (bool)validateInput:(NSString *)name;
- (bool)validateCommentInput;
- (bool)validateCommentNameInput;
-(UIView *)roundCornersOnView:(UIView *)view onTopLeft:(BOOL)tl topRight:(BOOL)tr bottomLeft:(BOOL)bl bottomRight:(BOOL)br radius:(float)radius;
@end
