//
//  CustomTabBarController.m
//  Mzad Qatar
//
//  Created by Paresh Kacha on 08/12/15.
//  Copyright © 2015 Mohammed abdulfattah attia. All rights reserved.
//

#import "CustomTabBarController.h"

@interface CustomTabBarController ()

@end

@implementation CustomTabBarController
+ (void)initialize
{
    //the color for the text for unselected tabs
    
    if([[[UIDevice currentDevice]systemVersion] floatValue]>=10.0){
        
        [UITabBarItem.appearance setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor redColor]} forState:UIControlStateNormal];
        //the color for selected icon
        [[UITabBar appearance] setUnselectedItemTintColor:[UIColor whiteColor]];
    }else{
        
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if ([[UITabBar appearance] respondsToSelector:@selector(setUnselectedItemTintColor:)]) {
        [[UITabBar appearance] setUnselectedItemTintColor:[UIColor whiteColor]];
    }else{
        //[[UIView appearanceWhenContainedIn:[UITabBar class], nil] setTintColor:[UIColor whiteColor]];
        [[UIView appearanceWhenContainedInInstancesOfClasses:@[[UITabBar class]]]setTintColor:[UIColor whiteColor]];
    }
    
    [[UITabBar appearance] setTintColor:[UIColor whiteColor]];
    self.tabBar.itemPositioning = UITabBarItemPositioningFill;
    [[UITabBarItem appearance] setTitleTextAttributes:@{ NSForegroundColorAttributeName : [UIColor whiteColor] }
                                             forState:UIControlStateNormal];

    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7) {
        for (UITabBarItem *tbi in self.tabBar.items) {
            tbi.image = [tbi.image imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        }
    }
    [self.tabBar setSelectionIndicatorImage:[self imageFromColor:[UIColor colorWithRed:133/255.0 green:27/255.0 blue:76/255.0 alpha:1] forSize:CGSizeMake(self.tabBar.frame.size.width/5.0, self.tabBar.frame.size.height) withCornerRadius:0]];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (UIImage *)imageFromColor:(UIColor *)color forSize:(CGSize)size withCornerRadius:(CGFloat)radius
{
    CGRect rect = CGRectMake(0, 0, size.width, size.height);
    UIGraphicsBeginImageContext(rect.size);
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    // Begin a new image that will be the new image with the rounded corners
    // (here with the size of an UIImageView)
    UIGraphicsBeginImageContext(size);
    
    // Add a clip before drawing anything, in the shape of an rounded rect
    [[UIBezierPath bezierPathWithRoundedRect:rect cornerRadius:radius] addClip];
    // Draw your image
    [image drawInRect:rect];
    
    // Get the image, here setting the UIImageView image
    image = UIGraphicsGetImageFromCurrentImageContext();
    
    // Lets forget about that we were drawing
    UIGraphicsEndImageContext();
    
    return image;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
