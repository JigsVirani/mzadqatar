//
//  CommentInfoTableCell.m
//  MazadQatar
//
//  Created by samo on 4/17/13.
//  Copyright (c) 2013 Mohammed abdulfattah attia. All rights reserved.
//

#import "CommentInfoTableCell.h"
#import "DisplayUtility.h"
#import "UserSetting.h"
#import "SharedViewManager.h"

@implementation CommentInfoTableCell

NSString *requestDeleteComment = @"requestDeleteComment";
NSString *requestCallComment = @"requestCallComment";
NSString *requestReportComment = @"requestReportComment";

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
  [super setSelected:selected animated:animated];

  // Configure the view for the selected state
}

- (IBAction)callBtnClicked:(UIButton *)sender {
  [DialogManager
      showConfirmationDialogueWithDescription:
          [NSString
              stringWithFormat:@"%@ %@ %@",
                               NSLocalizedString(@"CONFIRMATION_CALL_MSG", nil),
                               productCommentObject.commenterNumber, NSLocalizedString(@"?",nil)]
                            andConfirmBtnText:NSLocalizedString(
                                                  @"CONFIRMATION_CALL_BTN_TEXT",
                                                  nil)
                                  andDelegate:self
                                 andRequestId:requestCallComment];
}

- (void)goToCommentClassCallAdvertiseMethod {

    //[SystemUtility callNumber:productCommentObject.commenterNumber];
    [SystemUtility callNumberWithCountryCode:productCommentObject.commenterCountryCode withNumber:productCommentObject.commenterNumber];
}

- (IBAction)smsBtnClicked:(id)sender {
  //[SystemUtility smsNumberInView:currentView andNumber:productCommentObject.commenterNumber andDelegate:smsDelegate];
    [SystemUtility smsNumberInView:currentView andCountryCode:productCommentObject.commenterCountryCode andNumber:productCommentObject.commenterNumber andDelegate:smsDelegate];
}

- (IBAction)deleteBtnclicked:(id)sender {
  [DialogManager
      showConfirmationDialogueWithDescription:
          [NSString
              stringWithFormat:@"%@ %@ %@",
                               NSLocalizedString(
                                   @"CONFIRMATION_DELETE_COMMENT_MSG", nil),
                               productCommentObject.commenterNumber, NSLocalizedString(@"?",nil)]
                            andConfirmBtnText:
                                NSLocalizedString(
                                    @"CONFIRMATION_DELETE_BTN_TEXT", nil)
                                  andDelegate:self
                                 andRequestId:requestDeleteComment];
}
- (void)goToDeleteAdvt {
  [ServerManager deleteComment:productCommentObject
                   AndDelegate:self
                 withRequestId:requestDeleteComment
                 withProductId:productId];
}

- (IBAction)reportBtnClicked:(id)sender {
 
    [self showReportDialogue];
 
}

- (void)setComment:(ProductCommentObject *)productCommentObjectParam
       andAdvertiseNumber:(NSString *)advertiseNumberParam
                   inView:(UIViewController *)currentViewParam
              andDelegate:
                  (id<MFMessageComposeViewControllerDelegate>)smsDelegateParam
    andCommentInfoDelegae:
        (id<CommentInfoTableCellDelegate>)commentInfoDelegateParam
              inProductId:(NSString *)productIdParam
              inTableView:(UITableView *)tableViewParam {
  productCommentObject = productCommentObjectParam;

  currentView = currentViewParam;

  smsDelegate = smsDelegateParam;

  commentInfoDelegate = commentInfoDelegateParam;

  advertiseNumber = advertiseNumberParam;

  productId = productIdParam;

  self.nameTextEdit.text = productCommentObject.commentUserName;

  self.commentText.text = productCommentObject.commentDescription;

  self.dateText.text = productCommentObject.commentDate;

  if ([productCommentObjectParam.commenterNumber
          isEqualToString:advertiseNumberParam]) {
    self.lblAdvertiseOwner.hidden = false;
  } else {
    self.lblAdvertiseOwner.hidden = true;
  }

  self.lblCommentActionResult.hidden = YES;

  // check if comment equal to advertise number or user number , so u can show
  // delete btn
  if ([[UserSetting getUserNumber] isEqualToString:advertiseNumberParam] ||
      [[UserSetting getUserNumber]
          isEqualToString:productCommentObject.commenterNumber]) {
    [self.deleteBtn setHidden:NO];
    [self.deleteImageView setHidden:NO];
  } else {
    [self.deleteBtn setHidden:YES];
    [self.deleteImageView setHidden:YES];
  }

  if (productCommentObject.isReportingAdvertise) {
    [self.reportActivityIndicator startAnimating];

    [ServerManager reportComment:productCommentObject
                     AndDelegate:self
                   withRequestId:requestReportComment];
  }
}

- (UIView *)viewWithTagNotCountingSelf:(NSInteger)tag {
  UIView *toReturn = nil;

  for (UIView *subView in self.subviews) {
    toReturn = [subView viewWithTag:tag];

    if (toReturn)
      break;
  }

  return toReturn;
}

- (void)setIsLoadingCommentView:(BOOL)isLoading {
  if (isLoading) {
    [self.loadingCommentsIndicator startAnimating];
  } else {
    [self.loadingCommentsIndicator stopAnimating];
  }
}

- (void)setIsNoCommentLblHidden:(BOOL)isHidden {
  [self.noCommentLbl setHidden:isHidden];
  [self.nameTextEdit setHidden:!isHidden];
  [self.commentText setHidden:!isHidden];
  [self.callBtn setHidden:!isHidden];
  [self.callIcon setHidden:!isHidden];
  [self.smsBtn setHidden:!isHidden];
  [self.smsIcon setHidden:!isHidden];
  [self.dateText setHidden:!isHidden];
  [self.reportBtn setHidden:!isHidden];
  [self.reportImageView setHidden:!isHidden];
}

- (void)requestFinished:(ServerManagerResult *)serverManagerResult
            ofRequestId:(NSString *)requestId
    withNumberOfRequest:(int)numberOfRequest {

  // stop animation
  if (requestId == requestReportComment) {
    [productCommentObject setIsReportingAdvertise:false];

    [self.reportActivityIndicator stopAnimating];
  } else if (requestId == requestDeleteComment) {
    [self.deleteActivityIndicator stopAnimating];
  }

  if ([serverManagerResult.opertationResult
          isEqualToString:[ServerManager ERROR_IN_CONNECTION_OPERATION]]) {

    [DialogManager showErrorInConnection];

    return;
  }

  // show status text field
  self.lblCommentActionResult.hidden = NO;

  // set status text
  if ([serverManagerResult.opertationResult
          isEqualToString:[ServerManager SUCCESS_OPERATION]]) {
    self.lblCommentActionResult.text = NSLocalizedString(@"SUCCESS_ADD", nil);
  } else if ([serverManagerResult.opertationResult
                 isEqualToString:[ServerManager FAILED_OPERATION]]) {
    self.lblCommentActionResult.text = NSLocalizedString(@"ERROR_ADD", nil);
  } else if ([serverManagerResult.opertationResult
                 isEqualToString:[ServerManager ALREADED_ADDED_OPERATION]]) {
    self.lblCommentActionResult.text = NSLocalizedString(@"ALREADY_ADD", nil);
  }

  if (requestId == requestDeleteComment) {
    [commentInfoDelegate deleteCommentCLicked:self.deleteActivityIndicator
                           andResultTextField:self.lblCommentActionResult];
  }
}

- (void)showReportDialogue {
  ReportItemViewController *controller = [[SharedData getCurrentStoryBoard]
      instantiateViewControllerWithIdentifier:@"ReportCommentViewController"];

  controller.delegate = self;

  [currentView.navigationController pushViewController:controller animated:YES];
}

- (void)reportAdWithReasonWithReasonID:(NSString *)reasonID
                         andReasonText:(NSString *)reasonText

{
  //[self.reportIndicator startAnimating];
  [productCommentObject setIsReportingAdvertise:YES];
  [productCommentObject setReportReasonId:reasonID];
  [productCommentObject setReportReasonText:reasonText];

  [tableView reloadData];
}

#pragma confirmation dialog buttons clicked events

- (void)confirmBtnClicked:(NSString *)requestId {
  if (requestId == requestCallComment) {
    //[SystemUtility callNumber:productCommentObject.commenterNumber];
      [SystemUtility callNumberWithCountryCode:productCommentObject.commenterCountryCode withNumber:productCommentObject.commenterNumber];
  } else if (requestId == requestDeleteComment) {
    [self.deleteActivityIndicator startAnimating];

    [ServerManager deleteComment:productCommentObject
                     AndDelegate:self
                   withRequestId:requestDeleteComment
                   withProductId:productId];
  }
}


@end
