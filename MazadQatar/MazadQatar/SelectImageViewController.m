//
//  SelectImageViewController.m
//  Mzad Qatar
//
//  Created by Perveen Akhtar on 07/08/2014.
//  Copyright (c) 2014 Mohammed abdulfattah attia. All rights reserved.
//

#import "SelectImageViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "SelectImageCellCollectionViewCell.h"

@interface SelectImageViewController ()
@property(strong, nonatomic) IBOutlet UINavigationBar *navBar;

@end

@implementation SelectImageViewController

@synthesize photos;

static NSString *identifier = @"SelectImageCellCollectionViewCell";

- (void)viewDidLoad {
    [super viewDidLoad];
    self.screenName = @"Select Image Screen";
    photos = [[NSMutableArray alloc] init];
    
//    UIImage *backgroundPattern = [UIImage imageNamed:@"bg.png"];
//    [self.collectionViewGridView
//     setBackgroundColor:[UIColor colorWithPatternImage:backgroundPattern]];
}

- (void)viewDidAppear:(BOOL)animated {
  [super viewDidAppear:YES];
  if (photos.count == 0) {
    [self startLoadingImagesWithRequestId:requestGetImages];
  }
}

- (void)didReceiveMemoryWarning {
  [super didReceiveMemoryWarning];
  // Dispose of any resources that can be recreated.
}

- (IBAction)doneButtonPressed:(id)sender {
  [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark -CollectionView delegate methods
- (NSInteger)collectionView:(UICollectionView *)collectionView
     numberOfItemsInSection:(NSInteger)section {
  return photos.count;
}
-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake((self.view.frame.size.width-10)/3, (self.view.frame.size.width-10)/3);
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView
                  cellForItemAtIndexPath:(NSIndexPath *)indexPath {
  SelectImageCellCollectionViewCell *cell =
      [collectionView dequeueReusableCellWithReuseIdentifier:identifier
                                                forIndexPath:indexPath];
  SelectImageModal *modal = [photos objectAtIndex:indexPath.row];

  NSURL *url = [NSURL URLWithString:modal.imageUrl];

  [cell.imageLoadingIndicator startAnimating];
  cell.tag = indexPath.row;

    NSString *imageName =
    [[modal imageId] stringByAppendingString:@".png"];
    
    [cell.imageOfSelection
     sd_setImageWithURL:url
     placeholderImage:[UIImage imageNamed:imageName]
     completed:^(UIImage *image, NSError *error,
                 SDImageCacheType cacheType, NSURL *imageURL) {
         [cell.imageLoadingIndicator stopAnimating];
         UIImage *newImage = [self imageWithImage:image scaledToSize:CGSizeMake(600.0, 600.0)] ;
         [(SelectImageModal *)[photos
                               objectAtIndex:cell.tag] setImage:newImage];

     }];
    
  return cell;
}
- (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize {
    //UIGraphicsBeginImageContext(newSize);
    // In next line, pass 0.0 to use the current device's pixel scaling factor (and thus account for Retina resolution).
    // Pass 1.0 to force exact pixel size.
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

- (void)collectionView:(UICollectionView *)collectionView
    didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
  selectedImageIndex = (int)indexPath.row;
  currentImageSelected = [[photos objectAtIndex:indexPath.row] image];
SelectImageModal *selectImageModal =
    [photos objectAtIndex:selectedImageIndex];
  // add enter text view controller

  EnterTextViewController *controller = [[SharedData getCurrentStoryBoard]
      instantiateViewControllerWithIdentifier:@"EnterTextViewController"];
  controller.delegate = self;
  controller.maxCharacterLimit=selectImageModal.imageCharacterLimit.intValue;
  [self.view addSubview:controller.view];
  [self addChildViewController:controller];
}

#pragma mark - Get Images From Server
- (void)startLoadingImagesWithRequestId:(NSString *)requestId {
  ////*/////
  MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
   [hud setLabelText:NSLocalizedString(@"LOADING_IMAGES", Nil)];

    //NSString *language = [[[NSBundle mainBundle] preferredLocalizations] objectAtIndex:0];
   NSString *language = [LanguageManager currentLanguageCode];
    
  if ([language isEqualToString:[ServerManager LANGUAGE_ARABIC]]) {
    language = [ServerManager LANGUAGE_ARABIC];
  } else {
    language = [ServerManager LANGUAGE_ENGLISH];
  }

  // get the category based on the resolution
  if ([DisplayUtility isPad]) {
    [ServerManager
            getImagesToWriteTextOnIt:language
                       andResolution:[ServerManager RESOLUTION_MEDIUM]
                        withDelegate:self
                      withCategoryId:@""
                       withRequestId:requestGetImages
        withIsOpenedFromAddAdvertise:[SharedData notOpenedFromAddAdvdrtise]];
  } else {
    [ServerManager
            getImagesToWriteTextOnIt:language
                       andResolution:[ServerManager RESOLUTION_LOW]
                        withDelegate:self
                      withCategoryId:@""
                       withRequestId:requestGetImages
        withIsOpenedFromAddAdvertise:[SharedData notOpenedFromAddAdvdrtise]];
  }
}

- (void)reload {
  [self.collectionViewGridView reloadData];
}

- (void)requestFinished:(ServerManagerResult *)serverManagerResult
            ofRequestId:(NSString *)requestId
    withNumberOfRequest:(int)numberOfRequest {

  if ([requestId isEqualToString:requestGetImages]) {
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];

    if ([serverManagerResult.opertationResult
            isEqualToString:[ServerManager ERROR_IN_CONNECTION_OPERATION]]) {
      [DialogManager showErrorInConnection];
    } else if ([serverManagerResult.opertationResult
                   isEqualToString:[ServerManager SUCCESS_OPERATION]]) {
      photos = serverManagerResult.jsonArray;

      [self.collectionViewGridView reloadData];

    } else if ([serverManagerResult.opertationResult
                   isEqualToString:[ServerManager FAILED_OPERATION]]) {
      //Comment [DialogManager showErrorInServer];
        [DialogManager showErrorWithMessage:serverManagerResult.opertationMsg];
    }
  }
}


- (void)textEntered:(NSString *)text {
  SelectImageModal *selectImageModal =
      [photos objectAtIndex:selectedImageIndex];

  UIImage *imageWithTextWritten =
      [self createNewImageWithText:text
                          andImage:currentImageSelected
                          andColor:[self colorFromHexString:selectImageModal
                                                                .imageTextColor]
                      andXposition:[selectImageModal.imageXPosition intValue]
                      andYposition:[selectImageModal.imageYPosition intValue]
                       andTextSize:[selectImageModal.imageTextSize intValue]];

  //       UIImage* imageWithTextWritten= [self createNewImageWithText:text
  //       andImage:currentImageSelected andColor:[UIColor redColor]
  //       andXposition:0 andYposition:0];

  [self.navigationController popViewControllerAnimated:NO];

  [self.delegate
      SelectImageViewControllerDelegateSelected:imageWithTextWritten];
}

- (UIColor *)colorFromHexString:(NSString *)hexString {

  unsigned rgbValue = 0;
  NSScanner *scanner = [NSScanner scannerWithString:hexString];
  [scanner setScanLocation:1]; // bypass '#' character
  [scanner scanHexInt:&rgbValue];
  return [UIColor colorWithRed:((rgbValue & 0xFF0000) >> 16) / 255.0
                         green:((rgbValue & 0xFF00) >> 8) / 255.0
                          blue:(rgbValue & 0xFF) / 255.0
                         alpha:1.0];
}

- (UIImage *)createNewImageWithText:(NSString *)title
                           andImage:(UIImage *)image
                           andColor:(UIColor *)colorText
                       andXposition:(int)xPostion
                       andYposition:(int)yPosition
                        andTextSize:(int)textSize {
  CGSize size = CGSizeMake(image.size.width, image.size.height);
  UIGraphicsBeginImageContextWithOptions(size, YES, 0.0);

  // draw base image
  [image drawInRect:CGRectMake(0, 0, size.width, size.height)];
  
  UITextView *txtView = [[UITextView alloc] init];
  [txtView setText:[NSString stringWithFormat:@"%@", title]];
  NSMutableParagraphStyle *textStyle = [[NSMutableParagraphStyle alloc] init];
  textStyle.lineBreakMode = NSLineBreakByWordWrapping;
  textStyle.alignment = NSTextAlignmentCenter;

  UIFont *textFont = [UIFont systemFontOfSize:textSize];
  NSDictionary *dictionary = @{
    NSFontAttributeName : textFont,
    NSParagraphStyleAttributeName : textStyle,
    NSForegroundColorAttributeName : colorText
  };

 // [txtView.text drawAtPoint:CGPointMake(xPostion, yPosition) withAttributes:dictionary];
    [txtView.text drawInRect:CGRectMake(xPostion, yPosition, size.width, size.height) withAttributes:dictionary];
    
  UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
  UIGraphicsEndImageContext();
  return img;
}

//-(UIImage*) createshareImage:(NSString*)title andImage:(UIImage*)image
// andDescription:(NSString*)description
//{
//    CGSize size = CGSizeMake(image.size.width, image.size.height);
//    UIGraphicsBeginImageContextWithOptions(size, YES,0.0);
//
//    //draw base image
//    [image drawInRect:CGRectMake(0, 0, size.width, size.height)];
//
//    //draw text over image
//    CGRect txtRect=CGRectMake(xPostion,yPosition, image.size.width,
//    image.size.height) ;
//    UITextView *txtView=[[[UITextView alloc]init]autorelease];
//    [txtView setText:[NSString stringWithFormat:@"%@", title]];
//    NSMutableParagraphStyle *textStyle = [[NSMutableParagraphStyle
//    defaultParagraphStyle] mutableCopy];
//    textStyle.lineBreakMode = NSLineBreakByWordWrapping;
//    textStyle.alignment = NSTextAlignmentCenter;
//
//    UIFont *textFont = [UIFont systemFontOfSize:50];
//    NSDictionary *dictionary = @{ NSFontAttributeName: textFont,
//                                  NSParagraphStyleAttributeName: textStyle,
//                                  NSForegroundColorAttributeName:colorText};
//
//
//    [txtView.text drawInRect:txtRect withAttributes:dictionary];
//
//    UIImage * img = UIGraphicsGetImageFromCurrentImageContext();
//    UIGraphicsEndImageContext();
//    return img;
//}
@end
