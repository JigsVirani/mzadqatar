//
//  SubcategoriesController.h
//  Mzad Qatar
//
//  Created by GuruUgam on 7/13/16.
//  Copyright © 2016 Mohammed abdulfattah attia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ServerManager.h"
#import "GoogleAnalyticsTableViewController.h"
#import "SearchViewController.h"
#import "GAITrackedViewController.h"
@interface SubcategoriesController : GAITrackedViewController<ServerManagerResponseDelegate,UITextFieldDelegate,UICollectionViewDelegate>

@property(nonatomic, strong) NSString *strCategoryID;
@property (nonatomic, strong) NSString *strCategoryName;


@property(strong, nonatomic) IBOutlet UILabel *notificationCountLbl;
@property (weak, nonatomic) IBOutlet UIView *searchBGView;
@property (weak, nonatomic) IBOutlet UIButton *AdvertisementButton;
/** Set width constant of Addvertise now free **/
@property (weak,nonatomic)IBOutlet NSLayoutConstraint *advertiseBtnWidthConstant;
@end
