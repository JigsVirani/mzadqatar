//
//  MenuDetailVC.m
//  Mzad Qatar
//
//  Created by ashish agrawal on 22/12/16.
//  Copyright © 2016 Mohammed abdulfattah attia. All rights reserved.
//

#import "MenuDetailVC.h"
#import "SWRevealViewController.h"
#import "CategoryObject.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "MBProgressHUD.h"
#import "SettingMenuItem.h"
#import "ProductDetailsViewController.h"
#import "CompanyRegistration.h"
#import "CustomCell.h"
#import "CategoryProducts.h"
#import "DisplayUtility.h"
@interface MenuDetailVC ()
{
    CategoryObject *categry;
    __weak IBOutlet UIButton *btnBack;
}
@end

@implementation MenuDetailVC

- (void)viewDidLoad {
    [super viewDidLoad];
    selectedIndex = -1;
    self.screenName = @"Menu Sub Table ";
    self.navigationController.navigationBarHidden = false;
    
    if([[SharedData getDeviceLanguage]isEqualToString:@"en"]){
        [btnBack setImage:[UIImage imageNamed:@"btn_back_normal.png"] forState:UIControlStateNormal];
    }else{
        [btnBack setImage:[UIImage imageNamed:@"btn_back_normal_arabic"] forState:UIControlStateNormal];
    }
}

/////==============Start getting data from Server/Array according to latest

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    //[self.view setBackgroundColor:[UIColor clearColor]];
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setSubCategry:(CategoryObject *)cat {
    categry = cat;
    self.title = categry.categoryName;
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    ////if section is 1 then return section section categories data rows
    return categry.categorySubcategories.count;
}

////============Set sections hegight
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
//    if(indexPath.row == 0){
//        return 0;
//    }
    if ([DisplayUtility isPad]) {
        return 64;
    }
    return 45;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    ////Create cell for section two
    CustomCell *cell;
    if([DisplayUtility isPad]) {
        cell = (CustomCell *)[tableView dequeueReusableCellWithIdentifier:@"CustomCellBGiPad" forIndexPath:indexPath];
    }else{
        cell = (CustomCell *)[tableView dequeueReusableCellWithIdentifier:@"CustomCellBG" forIndexPath:indexPath];
    }
    
    cell.backgroundColor = cell.contentView.backgroundColor;
    if (selectedIndex == indexPath.row) {
        cell.imgArrwBG.hidden = false;
    } else {
        cell.imgArrwBG.hidden = true;
    }
    
    CategoryObject *category = [categry.categorySubcategories objectAtIndex:indexPath.row];
    // Configure the cell...
    cell.lblName.text = category.categoryName;
    
    [cell.activity startAnimating];
    [cell.imgView sd_setImageWithURL:[NSURL URLWithString:category.categoryImageURL] placeholderImage:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        [cell.activity stopAnimating];
    }];
    
    if([LanguageManager currentLanguageIndex] == 1){
        //cell.imgArrw.transform = CGAffineTransformScale(cell.imgArrw.transform, -1, 1);
        [cell.imgArrw setImage:[UIImage imageNamed:@"ic_category_arrow_ar"]];
    }else{
        [cell.imgArrw setImage:[UIImage imageNamed:@"ic_category_arrow_en"]];
    }
    if ([DisplayUtility isPad]) {
        cell.arrowMargin.constant = 360;
    }else{
        cell.arrowMargin.constant = 220;
    }

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    AppDelegate *appdel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    SWRevealViewController *swController = (SWRevealViewController *)appdel.window.rootViewController;
    UITabBarController *tbController = (UITabBarController *)swController.frontViewController;
    UINavigationController *nvController = (UINavigationController *)[tbController.viewControllers objectAtIndex:tbController.selectedIndex];
    
    selectedIndex = indexPath.row;
    [tableView reloadRowsAtIndexPaths:[NSArray arrayWithObjects:indexPath, nil] withRowAnimation:UITableViewRowAnimationNone];
    
    CategoryObject *category = [categry.categorySubcategories objectAtIndex:indexPath.row];
    //[self.revealViewController revealToggleAnimated:YES];
    
    
    if([[SharedData getDeviceLanguage]isEqualToString:@"en"]){
        [self.revealViewController revealToggleAnimated:YES];
    }else{
        [self.revealViewController rightRevealToggleAnimated:YES];
    }
    
    //CategoryProducts *catProduct = (CategoryProducts *)[self.storyboard instantiateViewControllerWithIdentifier:@"CategoryProducts"];
    CategoryProducts *catProduct = (CategoryProducts *)[[SharedData getCurrentStoryBoard] instantiateViewControllerWithIdentifier:@"CategoryProducts"];
    
    [catProduct setCategoryProductsCategoryId:category.subCategoryParent
                             andSubcategoryId:category.categoryID
                           andSubcategoryName:category.categoryName];
    
    catProduct.tabBarNames = categry.productAdvertiseTypes;
    [nvController pushViewController:catProduct animated:YES];
}

//////=====initialize variables

- (void)ViewAdvertiselanguageChanged:(int)index
                    withLanguageName:(NSString *)languageName
{
    [UserSetting saveFilterViewAdvertiseLanguage:index];
}



@end
