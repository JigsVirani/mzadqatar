//
//  MenuTableViewController.h
//  Mzad Qatar
//
//  Created by Waris Ali on 04/06/2014.
//  Copyright (c) 2014 Mohammed abdulfattah attia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SectionView.h"
#import "ServerManager.h"
#import "CategoryProducts.h"
#import "FileManager.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "SubcategoryTableCell.h"
#import "CustomCell.h"
#import "AboutScreen.h"
#import "CategoryObject.h"
#import "ViewAdvertiseLanguageChooserViewController.h"
#import "GAITrackedViewController.h"

#define IS_IPHONE5                                                             \
  (([[UIScreen mainScreen] bounds].size.height - 568) ? NO : YES)
#define requestCategory @"requestGetCategory"
#define subcategoryChooserCellIdentifier @"SubCategoryChooserCell"

@interface MenuTableViewController
    : GAITrackedViewController <UITabBarControllerDelegate, UITableViewDataSource,
                        UITableViewDelegate, UIGestureRecognizerDelegate,ChoseViewAdvertiseLanguageChooserDelegate,ConfirmationDialogueViewControllerDelegate> {
  BOOL isOverlayShown;

  UIView *overlayView;
  UIActivityIndicatorView *activityIndicator;
  NSMutableArray *categoriesArray;

  BOOL isHeaderCollapsed;

  UIImage *dropArrowImage;
  NSMutableArray *myNewTempArray;
  NSMutableArray *categoriesFromShareData;
}
@property(nonatomic, strong) NSMutableArray *tempSectionArray;
@property(nonatomic, strong) UIImageView *categorydropArrowimageView;
@property(nonatomic, strong) UIImageView *settingdropArrowimageView;
@property(retain, nonatomic) IBOutlet UIImageView *categoryArrow;

@property(retain, nonatomic) IBOutlet UITableView *mainTableView;

@property(retain, nonatomic) IBOutlet UIImageView *categoryArrowImage;
@property(retain, nonatomic) IBOutlet UIImageView *settingArrowImage;
@property(retain, nonatomic) IBOutlet UIView *categoriesView;

@property(retain, nonatomic) IBOutlet UIView *settingView;

@property(nonatomic, strong) NSMutableArray *settingsArray;

@property(nonatomic) BOOL categoryViewBool;
@property(nonatomic) BOOL settingViewBool;

@property(nonatomic, strong) IBOutlet UITableView *tableView;
@property(nonatomic, strong) IBOutlet UITableView *settingtableView;

@property(nonatomic, retain) id<SubCategoryChooserDelegate> subCategorydelegate;

@property(nonatomic, strong) NSString *subCategorySelectedRowName;

@property(nonatomic, strong) UINavigationController *navController;

@property(retain, nonatomic)
    IBOutlet UIActivityIndicatorView *subcategoryImageLoadingIndicator;

@end
