//
//  MenuTableViewController.m
//  RevealView
//
//  Created by Waris Ali on 03/06/2014.
//  Copyright (c) 2014 Waris Ali. All rights reserved.
//

#import "MenuTableViewController.h"
#import "SWRevealViewController.h"
#import "CategoryObject.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "MBProgressHUD.h"
#import "SettingMenuItem.h"
#import "ProductDetailsViewController.h"
#import "CompanyRegistration.h"
#import "MenuDetailVC.h"

@interface MenuTableViewController () <ServerManagerResponseDelegate> {
    UIView *sectionView;
}
@property(nonatomic, strong) NSArray *allKeys;
@property(nonatomic, strong) NSMutableArray *isCollapsed;
@property(nonatomic, strong) NSMutableArray *getCategoryArray;
@property(nonatomic, strong) NSMutableArray *myArray;
@property(nonatomic, strong) NSMutableArray *isSectionCollapsed;
@end

@implementation MenuTableViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"BACK", nil) style:UIBarButtonItemStylePlain target:nil action:nil];

    self.screenName = @"Menu Table View Screen";

    [self addOverlayIndicator];
    
    /// Get categories from share data
    categoriesFromShareData = [SharedData getCategories];
    
    // Initialize variables
    [self initializeVariables];
}

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBarHidden = true;
    
    SettingMenuItem *menu_Logout = [_settingsArray objectAtIndex:5];
    if([UserSetting isUserRegistered] == YES){
        menu_Logout.settingName = NSLocalizedString(@"MENU_ITEM_LOGOUT", @"");
        menu_Logout.settingImageName = @"logout.png";
    }else{
        menu_Logout.settingName = NSLocalizedString(@"MENU_ITEM_LOGIN", @"");
        menu_Logout.settingImageName = @"login.png";
    }
    [self.mainTableView reloadData];
}
- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    [self startloadingData];
}

/////==============Start getting data from Server/Array according to latest
///update time...
- (void)startloadingData {
    //NSString *language = [[[NSBundle mainBundle] preferredLocalizations] objectAtIndex:0];
    NSString *language = [LanguageManager currentLanguageCode];
    
    int lastUpdateTimeForCategoriesFromServerInt=[[UserSetting getLastUpdateTimeForCategoriesFromServer] intValue];
    int lastUpdateTimeForCategoriesFromLocalInt=[[UserSetting getLastUpdateTimeForCategoriesFromLocal] intValue];
    NSString *langSaved=[UserSetting getAllCategoriesFileLanguage];

    if (lastUpdateTimeForCategoriesFromServerInt >lastUpdateTimeForCategoriesFromLocalInt||[language isEqualToString:langSaved] ==false) {
        [self loadDataInTableFromServer];
    } else {
        NSData *arrayData = [[NSUserDefaults standardUserDefaults]
                             objectForKey:[SharedData CATEGORY_ARRAY_FILE_KEY]];
        NSMutableArray *array =
        [NSKeyedUnarchiver unarchiveObjectWithData:arrayData];
        
        [self loadDataInTableFromArray:array];
    }
}

/////Load data from Array if lastupatedate is not updated
- (void)loadDataInTableFromArray:(NSMutableArray *)array {
    if ([array count] < 1) {
        // error in local file
        [self loadDataInTableFromServer];
        return;
    }
    
    if (_myArray)
        _myArray = nil;
    _myArray = [[NSMutableArray alloc] init];
    _tempSectionArray = [[NSMutableArray alloc] init];
    
    self.myArray = array;
    _tempSectionArray = array;
    myNewTempArray = [array mutableCopy];
    
    [self initVariables];
}

- (void)loadDataInTableFromServer {
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [hud setLabelText:NSLocalizedString(@"LOADING_CATEGORIES", Nil)];
    [self startLoadingUserCategoryWithRequestId:requestCategory
                                andIsNewSession:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section {
    ////if section is 0 then return first section rows
    if (section == 0) {
        return [_settingsArray count];
    }
    
    ////if section is 1 then return section section categories data rows
    return [_tempSectionArray count];
}

////============Set sections hegight
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([DisplayUtility isPad]) {
        return 44;
    }
    return 44;
}

////========Set Sections Header height
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if ([DisplayUtility isPad]) {
        return 64;
    }
    return 44.0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    //////Add cell stuffs to section one
    if (indexPath.section == 0) {
        ////Create custom cell for section one
        CustomCell *cell =  (CustomCell *)[tableView dequeueReusableCellWithIdentifier:@"CustomCell" forIndexPath:indexPath];
        
        SettingMenuItem *settingMenuItem =
        [_settingsArray objectAtIndex:indexPath.row];
        cell.backgroundColor = cell.contentView.backgroundColor;
        cell.lblName.text = settingMenuItem.settingName;
        cell.imgView.image = [UIImage imageNamed:settingMenuItem.settingImageName];
        return cell;
        
    }  else {
        ////Create cell for section two
        
        CustomCell *cell;
        if([DisplayUtility isPad]) {
            cell = (CustomCell *)[tableView dequeueReusableCellWithIdentifier:@"CustomCellBGiPad" forIndexPath:indexPath];
        }else{
            cell = (CustomCell *)[tableView dequeueReusableCellWithIdentifier:@"CustomCellBG" forIndexPath:indexPath];
        }
        
        cell.backgroundColor = cell.contentView.backgroundColor;
        CategoryObject *category = [_tempSectionArray objectAtIndex:indexPath.row];
        // Configure the cell...
        cell.lblName.text = category.categoryName;
        [cell.activity startAnimating];
        [cell.imgView setImageWithURL:[NSURL URLWithString:category.categoryImageURL] placeholderImage:nil options:SDWebImageRetryFailed completed:^(UIImage *image, NSError *error,SDImageCacheType cacheType) {
             [cell.activity stopAnimating];
         }];
        
        if([LanguageManager currentLanguageIndex] == 1){
            //cell.imgArrw.transform = CGAffineTransformScale(cell.imgArrw.transform, -1, 1);
            [cell.imgArrw setImage:[UIImage imageNamed:@"ic_category_arrow_ar"]];
        }else{
            [cell.imgArrw setImage:[UIImage imageNamed:@"ic_category_arrow_en"]];
        }
        if ([DisplayUtility isPad]) {
            cell.arrowMargin.constant = 360;
        }else{
            cell.arrowMargin.constant = 220;
        }
        
        return cell;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    if (section == 0) {
        UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 22)];
        
        //// Add tap gesture to click on section 2
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(sectionCategoriesTapped:)];
        
        tap.numberOfTapsRequired = 1;
        tap.delegate = self;
        [headerView addGestureRecognizer:tap];
        
        /// Add Text on header
        UILabel *lblStr = [[UILabel alloc] initWithFrame:CGRectMake(20, 12, self.view.frame.size.width-40, 20)];
        if([[SharedData getDeviceLanguage]isEqualToString:@"en"]){
            lblStr.textAlignment = NSTextAlignmentLeft;
        }else{
            lblStr.textAlignment = NSTextAlignmentRight;
        }
        lblStr.text = NSLocalizedString(@"MENU_TITLE", nil);
        lblStr.textColor = [UIColor whiteColor];
        lblStr.font = [UIFont boldSystemFontOfSize:16];
        [headerView addSubview:lblStr];
        headerView.backgroundColor = [UIColor colorWithRed:160.0/255.0 green:27.0/255.0 blue:88.0/255.0 alpha:1.0];
        
        return headerView;
    } else {
        sectionView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 22)];
        
        //// Add tap gesture to click on section 2
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(sectionCategoriesTapped:)];
        
        tap.numberOfTapsRequired = 1;
        tap.delegate = self;
        [sectionView addGestureRecognizer:tap];
        
        /// Add Text on header
        UILabel *lblStr = [[UILabel alloc] initWithFrame:CGRectMake(20, 12, self.view.frame.size.width-40, 20)];
        if([[SharedData getDeviceLanguage]isEqualToString:@"en"]){
            lblStr.textAlignment = NSTextAlignmentLeft;
        }else{
            lblStr.textAlignment = NSTextAlignmentRight;
        }
        lblStr.text = NSLocalizedString(@"MENU_CATEGORIES_TITLE", nil);
        lblStr.textColor = [UIColor whiteColor];
        lblStr.font = [UIFont boldSystemFontOfSize:16];
        
        [self updateHeader:tableView.contentOffset.y];
        [sectionView addSubview:lblStr];
       // [headerView addSubview:lblStr];
        return sectionView;
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    [self updateHeader:scrollView.contentOffset.y];
}

- (void)updateHeader:(CGFloat)contentoffSet {
    if (contentoffSet > _settingsArray.count * 44) {
        sectionView.backgroundColor = [UIColor colorWithRed:160.0/255.0 green:27.0/255.0 blue:88.0/255.0 alpha:1.0];
    } else {
        sectionView.backgroundColor = [UIColor clearColor];
    }
}


- (void)sectionHeaderTapped:(UITapGestureRecognizer *)sender {
    ////Check if the section header is already collapsed or not
    if (_settingViewBool) {
        [self initSettingElements];
        
        _settingViewBool = NO;
        
        // self.settingdropArrowimageView.transform =
        // CGAffineTransformMakeRotation(0);
        
    }
    ////if collapsed,then hide it
    else {
        _settingViewBool = YES;
        [_settingsArray removeAllObjects];
    }
    [self.mainTableView reloadSections:[NSIndexSet indexSetWithIndex:0]
                      withRowAnimation:UITableViewRowAnimationNone];
}
- (void)sectionCategoriesTapped:(UITapGestureRecognizer *)sender {
    /// CHECK IF THE CATEGORY TABLE ALREADY COLLAPSED OR NOT
    // IF ALREADY COLLAPSED,THEN CLOSE OTHERWISE COLLAPSE
    if (_categoryViewBool) {
        _tempSectionArray = [[NSMutableArray alloc] init];
        _tempSectionArray = [myNewTempArray mutableCopy];
        _categoryViewBool = NO;
        
    }
    ////if collapsed,hide it
    else {
        [_tempSectionArray removeAllObjects];
        
        _tempSectionArray = nil;
        
        _categoryViewBool = YES;
    }
    
    [self.mainTableView reloadSections:[NSIndexSet indexSetWithIndex:1]
                      withRowAnimation:UITableViewRowAnimationNone];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    AppDelegate *appdel =
    (AppDelegate *)[[UIApplication sharedApplication] delegate];
    SWRevealViewController *swController =
    (SWRevealViewController *)appdel.window.rootViewController;
    UITabBarController *tbController =
    (UITabBarController *)swController.frontViewController;
    UINavigationController *nvController =
    (UINavigationController *)[tbController.viewControllers objectAtIndex:tbController.selectedIndex];
    
    //////// FOR SECTION 0 Row clicked
    
    if (indexPath.section == 0) {
        //[self.revealViewController revealToggleAnimated:YES];
        
        if(indexPath.row == 5){
            if([UserSetting isUserRegistered] == YES){
                [DialogManager showConfirmationDialogueWithDescription:NSLocalizedString(@"WARNING_LOGOUT", @"") andConfirmBtnText:NSLocalizedString(@"WARNING_YESBUTOON", @"") andDelegate:self andRequestId:@"Logout"];
            }else{
                if([[SharedData getDeviceLanguage]isEqualToString:@"en"]){
                    [self.revealViewController revealToggleAnimated:YES];
                }else{
                    [self.revealViewController rightRevealToggleAnimated:YES];
                }
                RegisterScreen *registerScreen = (RegisterScreen*)[[SharedData getCurrentStoryBoard]
                                                                   instantiateViewControllerWithIdentifier:@"RegisterScreen"];
                [registerScreen setSuccessScreenSegueName:nil];
                [nvController pushViewController:registerScreen animated:YES];
            }
            
        }else{
            if([[SharedData getDeviceLanguage]isEqualToString:@"en"]){
                [self.revealViewController revealToggleAnimated:YES];
            }else{
                [self.revealViewController rightRevealToggleAnimated:YES];
            }
            if (indexPath.row == 0) {
                if([UserSetting isUserRegistered]){
                    
                    ProductDetailsViewController *productDetail = (ProductDetailsViewController*)[[SharedData getCurrentStoryBoard]
                                                                                                  instantiateViewControllerWithIdentifier:@"ProductDetailsViewController"];
                    [productDetail setIsAddProductDetail:YES];
                    [nvController pushViewController:productDetail animated:YES];
                }else{
                    RegisterScreen *registerScreen = (RegisterScreen*)[[SharedData getCurrentStoryBoard]
                                                                       instantiateViewControllerWithIdentifier:@"RegisterScreen"];
                    [registerScreen setSuccessScreenSegueName:nil];
                    [nvController pushViewController:registerScreen animated:YES];
                }
                
            }else if (indexPath.row == 1)
                
            {
                /*CompanyRegistration  *companyRegistration = [self.storyboard
                 instantiateViewControllerWithIdentifier:@"RegisterAsCompany"];*/
                
                CompanyRegistration  *companyRegistration = [[SharedData getCurrentStoryBoard] instantiateViewControllerWithIdentifier:@"RegisterAsCompany"];
                [nvController pushViewController:companyRegistration animated:YES];
                
            } else if (indexPath.row == 2)
                
            {
                ViewAdvertiseLanguageChooserViewController *languageScreen =
                [[SharedData getCurrentStoryBoard]
                 instantiateViewControllerWithIdentifier:
                 @"ViewAdvertiseLanguageChooserViewController"];
                [languageScreen setDelegate:self];
                [nvController pushViewController:languageScreen animated:YES];
                
            } else if (indexPath.row == 3) {
                [DialogManager showShareDialogue];
            } else if (indexPath.row == 4) {
                AboutScreen *aboutScreen =
                (AboutScreen *)[[SharedData getCurrentStoryBoard]
                                instantiateViewControllerWithIdentifier:@"AboutScreen"];
                //
                [nvController pushViewController:aboutScreen animated:YES];
            }
        }
    }
    //////// FOR SECTION 1 row clicked
    ////Differentiate between Section 0 and Section 1 Rows
    /// Selected
    else {
        
        CategoryObject *category = [_tempSectionArray objectAtIndex:indexPath.row];
        if ([category.isCategory isEqualToString:@"YES"]) {
            [self performSegueWithIdentifier:@"MenuDetailVC" sender:category];
        } else {
            
            //[self.revealViewController revealToggleAnimated:YES];
            if([[SharedData getDeviceLanguage]isEqualToString:@"en"]){
                [self.revealViewController revealToggleAnimated:YES];
            }else{
                [self.revealViewController rightRevealToggleAnimated:YES];
            }
            
           // ProductObject *productObjectSelected =
           // [categoriesFromShareData objectAtIndex:indexPath.row];
            
            CategoryProducts *catProduct = (CategoryProducts *)[self.storyboard
                                                                instantiateViewControllerWithIdentifier:@"CategoryProducts"];
            
            [catProduct setCategoryProductsCategoryId:category.subCategoryParent
                                     andSubcategoryId:category.categoryID
                                   andSubcategoryName:category.categoryName];
            
            catProduct.tabBarNames = category.productAdvertiseTypes;
            [nvController pushViewController:catProduct animated:YES];
        }
    }
}
#pragma mark confirmation dialog buttons clicked events

- (void)confirmBtnClicked:(NSString *)requestId {
    if ([requestId  isEqual: @"Logout"]) {
        NSLog(@"Logout");
        
        [UserSetting setIsUserRegistered:NO];
        /*if([[SharedData getDeviceLanguage]isEqualToString:@"en"]){
            [self.revealViewController revealToggleAnimated:YES];
        }else{
            [self.revealViewController rightRevealToggleAnimated:YES];
        }
        RegisterScreen *registerScreen = (RegisterScreen*)[[SharedData getCurrentStoryBoard]
                                                           instantiateViewControllerWithIdentifier:@"RegisterScreen"];
        [registerScreen setSuccessScreenSegueName:nil];*/
        
        AppDelegate *appdel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        SWRevealViewController *swController = (SWRevealViewController *)appdel.window.rootViewController;
        UITabBarController *tbController = (UITabBarController *)swController.frontViewController;
        
        /*UINavigationController *nvController =
        (UINavigationController *)[tbController.viewControllers objectAtIndex:tbController.selectedIndex];
        [nvController pushViewController:registerScreen animated:YES];*/
        
        UITabBarItem *tbItem = (UITabBarItem *)[tbController.tabBar.items objectAtIndex:4];
        //tbItem.badgeValue = [NSString stringWithFormat:@"%d", 0];
        tbItem.badgeValue = nil;
        
        SettingMenuItem *menu_Logout = [_settingsArray objectAtIndex:5];
        if([UserSetting isUserRegistered] == YES){
            menu_Logout.settingName = NSLocalizedString(@"MENU_ITEM_LOGOUT", @"");
            menu_Logout.settingImageName = @"logout.png";
        }else{
            menu_Logout.settingName = NSLocalizedString(@"MENU_ITEM_LOGIN", @"");
            menu_Logout.settingImageName = @"login.png";
        }
        [self.mainTableView reloadData];
    }
}

#pragma mark - Custom Functions

- (void)initVariables {
    
    // Separator Lines
    [self.mainTableView setSeparatorInset:UIEdgeInsetsZero];
    
    // Array For booleans to see if section is collapsed or not.
    if (_isCollapsed)
        _isCollapsed = nil;
    _isCollapsed = [[NSMutableArray alloc] init];
    for (int i = 0; i < _myArray.count; i++) {
        [_isCollapsed addObject:[NSNumber numberWithBool:YES]];
    }
    
    ////Number Of Sections
    if (_isSectionCollapsed)
        _isSectionCollapsed = nil;
    _isSectionCollapsed = [[NSMutableArray alloc] init];
    
    int sections = [self.mainTableView numberOfSections];
    for (int i = 0; i < sections; i++) {
        [_isSectionCollapsed addObject:[NSNumber numberWithBool:YES]];
    }
    
    // Reload Data
    [self.mainTableView reloadData];
    //[self.settingtableView reloadData];
}
//////Start sending request to get categories data
- (void)startLoadingUserCategoryWithRequestId:(NSString *)requestId
                              andIsNewSession:(bool)isNewSession {
    
    // set language needed
    //NSString *language = [[[NSBundle mainBundle] preferredLocalizations] objectAtIndex:0];
    NSString *language = [LanguageManager currentLanguageCode];
    
    if ([language isEqualToString:[ServerManager LANGUAGE_ARABIC]]) {
        language = [ServerManager LANGUAGE_ARABIC];
    } else {
        language = [ServerManager LANGUAGE_ENGLISH];
    }
    
    [ServerManager getAllCategories:language
                       withDelegate:self
                      withRequestId:requestCategory];
}

#pragma mark - ServerManagerResponse Delegatehud

- (void)requestFinished:(ServerManagerResult *)serverManagerResult
            ofRequestId:(NSString *)requestId
    withNumberOfRequest:(int)numberOfRequest {
    ////
    //[self showOverlayWithIndicator:NO];
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];

    if ([requestId isEqualToString:requestCategory]) {
        
        if ([serverManagerResult.opertationResult
             isEqualToString:[ServerManager ERROR_IN_CONNECTION_OPERATION]]) {
            [DialogManager showErrorInConnection];
        } else if ([serverManagerResult.opertationResult
                    isEqualToString:[ServerManager SUCCESS_OPERATION]]) {
            
            
            [self loadDataInTableFromArray:serverManagerResult.jsonArray];
            
            NSData *data = [NSKeyedArchiver
                            archivedDataWithRootObject:serverManagerResult.jsonArray];
            [[NSUserDefaults standardUserDefaults]
             setObject:data
             forKey:[SharedData CATEGORY_ARRAY_FILE_KEY]];
            
            [UserSetting saveLastUpdateTimeForCategoriesFromLocal:
             [UserSetting getLastUpdateTimeForCategoriesFromServer]];
            
            // save language so whenever we change locale we load file again with
            // other language
            //NSString *language = [[[NSBundle mainBundle] preferredLocalizations] objectAtIndex:0];
            NSString *language = [LanguageManager currentLanguageCode];

            if ([language isEqualToString:[ServerManager LANGUAGE_ARABIC]]) {
                [UserSetting
                 saveAllCategoriesFileLanguage:[ServerManager LANGUAGE_ARABIC]];
            } else {
                [UserSetting
                 saveAllCategoriesFileLanguage:[ServerManager LANGUAGE_ENGLISH]];
            }
        } else if ([serverManagerResult.opertationResult
                    isEqualToString:[ServerManager FAILED_OPERATION]])
            
        {
            //Comment [DialogManager showErrorInServer];
            [DialogManager showErrorWithMessage:serverManagerResult.opertationMsg];
        }
    }
}

/////Pass Bool value to this to show/Hide Activity Indicator
- (void)showOverlayWithIndicator:(BOOL)isShow {
    if (isShow) {
        isOverlayShown = true;
        [overlayView setHidden:NO];
        [activityIndicator startAnimating];
        
    } else {
        isOverlayShown = false;
        [overlayView setHidden:YES];
        [activityIndicator stopAnimating];
    }
}

////Add Activity Indicator
- (void)addOverlayIndicator {
    CGSize size = [DisplayUtility getScreenSize];
    overlayView =
    [[UIView alloc] initWithFrame:CGRectMake(0, 0, size.width, size.height)];
    overlayView.backgroundColor =
    [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];
    activityIndicator = [[UIActivityIndicatorView alloc]
                         initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    activityIndicator.center = overlayView.center;
    [overlayView addSubview:activityIndicator];
    [self.navigationController.view addSubview:overlayView];
    [overlayView setHidden:YES];
}

//////=====initialize variables
- (void)initializeVariables {
    _tempSectionArray = [[NSMutableArray alloc] init];
    myNewTempArray = [[NSMutableArray alloc] init];
    [self initSettingElements];
}

- (void)initSettingElements {
    if (_settingsArray != nil) {
        [_settingsArray removeAllObjects];
    }
    
    _settingsArray = [[NSMutableArray alloc] init];
    
    // add advertise free
    SettingMenuItem *menu_Add_advertise = [[SettingMenuItem alloc] init];
    menu_Add_advertise.settingName =
    NSLocalizedString(@"MENU_ITEM_ADD_ADVERTISE_FREE", @"");
    menu_Add_advertise.settingImageName = @"side_menu_add_advertise.png";
    [_settingsArray addObject:menu_Add_advertise];
    
    
    SettingMenuItem *registerCompanyMenuItem= [[SettingMenuItem alloc] init];
    registerCompanyMenuItem.settingName = NSLocalizedString(@"MENU_ITEM_REGISTER_AS_COMPANY", @"");
    registerCompanyMenuItem.settingImageName = @"company.png";
    [_settingsArray addObject:registerCompanyMenuItem];
    
    
    // add advertise free
    SettingMenuItem *menu_language = [[SettingMenuItem alloc] init];
    menu_language.settingName = NSLocalizedString(@"MENU_ITEM_LANGUAGE", @"");
    menu_language.settingImageName = @"side_menu_language.png";
    [_settingsArray addObject:menu_language];
    
    // add advertise free
    SettingMenuItem *menu_share_mzad = [[SettingMenuItem alloc] init];
    menu_share_mzad.settingName =
    NSLocalizedString(@"MENU_ITEM_SHARE_MZAD_QATAR", @"");
    menu_share_mzad.settingImageName = @"side_menu_share.png";
    [_settingsArray addObject:menu_share_mzad];
    
    // add advertise free
    SettingMenuItem *menu_about = [[SettingMenuItem alloc] init];
    menu_about.settingName = NSLocalizedString(@"MENU_ITEM_ABOUT", @"");
    menu_about.settingImageName = @"side_menu_about.png";
    [_settingsArray addObject:menu_about];
    
    // Logout Button
    SettingMenuItem *menu_Logout = [[SettingMenuItem alloc] init];
    
    if([UserSetting isUserRegistered] == YES){
        menu_Logout.settingName = NSLocalizedString(@"MENU_ITEM_LOGOUT", @"");
        menu_Logout.settingImageName = @"logout.png";
    }else{
        menu_Logout.settingName = NSLocalizedString(@"MENU_ITEM_LOGIN", @"");
        menu_Logout.settingImageName = @"login.png";
    }
    
    [_settingsArray addObject:menu_Logout];
}

- (void)ViewAdvertiselanguageChanged:(int)index
                    withLanguageName:(NSString *)languageName
{
    [UserSetting saveFilterViewAdvertiseLanguage:index];
}

#pragma mark- UINavgatin

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"MenuDetailVC"]) {
        
        MenuDetailVC *menuDtailVC = [segue destinationViewController];
        [menuDtailVC setSubCategry:sender];
        
    }
}

- (void)dealloc {
    // [_mainTableView release];
    //[_mainTableView release];
    //[super dealloc];
    
    [_settingsArray removeAllObjects];
}

@end
