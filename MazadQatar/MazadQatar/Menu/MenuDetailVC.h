//
//  MenuDetailVC.h
//  Mzad Qatar
//
//  Created by ashish agrawal on 22/12/16.
//  Copyright © 2016 Mohammed abdulfattah attia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ChooseListWithSearch.h"
#import "GridView.h"

@class CategoryObject;

@interface MenuDetailVC : GridView<UITabBarControllerDelegate, UITableViewDataSource,
UITableViewDelegate> {
    NSInteger selectedIndex;
}

@property(nonatomic, strong) IBOutlet UITableView *tableView;
@property(nonatomic, strong) NSString *subCategorySelectedRowName;

- (void)setSubCategry:(CategoryObject *)cat;

@end
