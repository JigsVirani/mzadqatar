//
//  GoogleAnalyticsTableViewController.h
//  Mzad Qatar
//
//  Created by AHMED HEGAB on 8/8/16.
//  Copyright © 2016 Mohammed abdulfattah attia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GoogleAnalyticsTableViewController :  UITableViewController <UITableViewDataSource, UITableViewDelegate>
{
    NSString *screenName;
}
@end

