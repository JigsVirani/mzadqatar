//
//  MyProductsCollectionReusableView.h
//  Mzad Qatar
//
//  Created by Mohammed AbdulFattah on 1/29/15.
//  Copyright (c) 2015 Mohammed abdulfattah attia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GoogleMobileAds/GoogleMobileAds.h>

@interface MyProductsCollectionReusableView : UICollectionReusableView

@property(strong, nonatomic) IBOutlet UIView *isCompanyView;
@property(strong, nonatomic) IBOutlet UIImageView *isCompanyImage;
@property(strong, nonatomic) IBOutlet UIButton *btnEditInfo;
@property(strong, nonatomic) IBOutlet UIButton *btnCall;
@property(strong, nonatomic) IBOutlet UIButton *btnLocation;
@property(strong, nonatomic) IBOutlet UIButton *btnShare;
@property(strong, nonatomic) IBOutlet UIImageView *imageUserProfile;
@property(strong, nonatomic) IBOutlet UILabel *nameUserProfile;
@property(strong, nonatomic) IBOutlet UILabel *numberUserProfile;
@property(strong, nonatomic) IBOutlet UILabel *numberOfAdsUserProfile;
@property(strong, nonatomic) IBOutlet UILabel *shortDescriptionUserProfile;
@property (weak, nonatomic) IBOutlet DFPBannerView *bannerView;
@property(strong, nonatomic) IBOutlet UIActivityIndicatorView *loadingIndicator;
@property (weak, nonatomic) IBOutlet UIButton *btnDetailArrow;
@property(strong,nonatomic)IBOutlet NSLayoutConstraint *editInfoConstant;
@property(strong,nonatomic)IBOutlet NSLayoutConstraint *editleadingConstant;

@property(strong,nonatomic)IBOutlet NSLayoutConstraint *topButtonsHeight;


/** Set width constant of Add Company **/
@property (weak,nonatomic)IBOutlet NSLayoutConstraint *cmpLogoWidthConstant,*cmpLogoHeightConstant;
@property (weak,nonatomic)IBOutlet NSLayoutConstraint *callBtnWidthConstant,*shareBtnWidthConstant,*editBtnWidthConstant,*locationBtnWidthConstant;
@property (weak,nonatomic)IBOutlet NSLayoutConstraint *callBtnHeightConstant,*shareBtnHeightConstant,*editBtnHeightConstant,*locationBtnHeightConstant;
@end
