//
//  RegisterScreen.h
//  MazadQatar
//
//  Created by samo on 4/1/13.
//  Copyright (c) 2013 Mohammed abdulfattah attia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ServerManager.h"
#import "UserSetting.h"
#import "TextFieldViewController.h"
#import "DialogManager.h"
#import "DisplayUtility.h"
#import "SWRevealViewController.h"

@interface RegisterScreen : TextFieldViewController

@property(weak) NSString *successScreenSegueName;
@property(weak,nonatomic)IBOutlet UILabel *lblTitle;
@property(strong, nonatomic) IBOutlet UILabel *notificationCountLbl;
@property(strong, nonatomic)IBOutlet UITextField *userNumberField;
@property(weak, nonatomic)IBOutlet UITextField *qatarCodeTextField;

@property(strong,nonatomic)IBOutlet UIView *viewAr;
@property(strong,nonatomic)IBOutlet UITextField *userNumberFieldAr;
@property(strong,nonatomic)IBOutlet UILabel *mobileLblAr;

@property(strong, nonatomic) IBOutlet UIView *ViewEng;

-(IBAction)registerBtnClicked;
@property(strong, nonatomic)IBOutlet UIActivityIndicatorView *registerIndicator;



@property(weak, nonatomic)IBOutlet UILabel *registerLbl,*mobileLbl;
@property(weak, nonatomic)IBOutlet UITextView *detailTxtView;
@property(weak, nonatomic)IBOutlet UIButton *registerBtn;
@property(weak, nonatomic)IBOutlet NSLayoutConstraint *btnRegisterHeightConstant;
@end
