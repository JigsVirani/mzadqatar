//
//  LanguageChooserViewController.h
//  Mzad Qatar
//
//  Created by samo on 12/19/13.
//  Copyright (c) 2013 Mohammed abdulfattah attia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ServerManager.h"
#import "GoogleAnalyticsTableViewController.h"
@class SortByChooserViewController;
@protocol SortByChooserDelegate <NSObject>
@required
- (void)sortByChooserChanged:(NSString *)sortById
             withSortByIndex:(int)sortByIndex
              withSortByName:(NSString *)sortByName;
@required
@end

@interface SortByChooserViewController
: GoogleAnalyticsTableViewController {
    BOOL isOverlayShown;
    UIActivityIndicatorView *activityIndicator;
    NSMutableArray *sortByArray;
    int selectedIndex;
}

@property(weak, nonatomic) id<SortByChooserDelegate> delegate;

-(void)setSoryByDelegate:(id<SortByChooserDelegate>)delegateParam andSelectedIndex:(int)selectedIndexParam;

@property(strong, nonatomic) IBOutlet UITableView *selectLanguageTable;

- (void)startLoadingSortByFilters;
- (void)addLoadingIndicatorToView;
- (void)showOverlayWithIndicator:(BOOL)isShow;

@end
