//
//  FilterOtherCategoriesChooser.m
//  Mzad Qatar
//
//  Created by samo on 9/7/14.
//  Copyright (c) 2014 Mohammed abdulfattah attia. All rights reserved.
//

#import "FilterOtherCategoriesChooser.h"

@interface FilterOtherCategoriesChooser () <CitiesViewControllerDelegate>

@end

@implementation FilterOtherCategoriesChooser

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.screenName = @"Filter Other Categories Chooser";
//    UIImage *backgroundPattern = [UIImage imageNamed:@"bg.png"];
//    [self.view setBackgroundColor:[UIColor colorWithPatternImage:backgroundPattern]];
    if (self.productObject.productCityName != nil) {
        [self.citiesBtnChooser setTitle:self.productObject.productCityName
                               forState:UIControlStateNormal];
    }
    
    if (self.productObject.productSubCategoryName != nil) {
        [self.subcategoriesBtnChooser
         setTitle:self.productObject.productSubCategoryName
         forState:UIControlStateNormal];
    }
    
    if (self.productObject.productPriceFrom != nil) {
        [self.priceFromTextField setText:self.productObject.productPriceFrom];
    }
    
    if (self.productObject.productPriceTo != nil) {
        [self.priceToTextField setText:self.productObject.productPriceTo];
    }
    
    if([[SharedData getDeviceLanguage]isEqualToString:@"en"]){
        [self.citiesBtnChooser setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
        [self.subcategoriesBtnChooser setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
        
        [self.citiesImgChooser setImage:[UIImage imageNamed:@"ic_english_arrow"]];
        [self.subcategoriesImgChooser setImage:[UIImage imageNamed:@"ic_english_arrow"]];
        
        /*[self.citiesBtnChooser setBackgroundImage:[UIImage imageNamed:@"bk_choose_list_btn_right.png"] forState:UIControlStateNormal];
        [self.subcategoriesBtnChooser setBackgroundImage:[UIImage imageNamed:@"bk_choose_list_btn_right.png"] forState:UIControlStateNormal];*/
    }else{
        [self.citiesBtnChooser setContentHorizontalAlignment:UIControlContentHorizontalAlignmentRight];
        [self.subcategoriesBtnChooser setContentHorizontalAlignment:UIControlContentHorizontalAlignmentRight];
        
        [self.citiesImgChooser setImage:[UIImage imageNamed:@"ic_arabic_arrow"]];
        [self.subcategoriesImgChooser setImage:[UIImage imageNamed:@"ic_arabic_arrow"]];
        
        /*[self.citiesBtnChooser setBackgroundImage:[UIImage imageNamed:@"bk_choose_list_btn_left"] forState:UIControlStateNormal];
        [self.subcategoriesBtnChooser setBackgroundImage:[UIImage imageNamed:@"bk_choose_list_btn_left"] forState:UIControlStateNormal];*/
        
        [self.citiesBtnChooser setTitleEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 8)];
        [self.subcategoriesBtnChooser setTitleEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 8)];
    }
    //[self.navigationController.navigationBar.topItem setTitle:NSLocalizedString(@"BACK", nil)];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    [super prepareForSegue:segue sender:sender];
    
    if ([[segue identifier] isEqualToString:@"SubCategorySegue"]) {
        SubCategoryChooser *subCategoryChooser = [segue destinationViewController];
        [subCategoryChooser
         setSubCategoryParamsWithCatId:self.productObject.productCategoryId
         andCurrentSubCategoryId:self.productObject.productSubCategoryId
         andIsOpenedFromAddAdvertise:false
         andDelegate:self];
        
    } else if ([[segue identifier] isEqualToString:@"CitiesSegue"]) {
        CitiesViewController *citiesViewController =
        [segue destinationViewController];
        
        [citiesViewController
         setCityParamsWithCityId:self.productObject.productCityId
         andIsOpenedFromAddAdvertise:false
         andDelegate:self];
    }
}

- (void)subCategoryChoosedInList:(NSString *)subCategoryId
              andSubCategoryName:(NSString *)subCategorName {
    [self.subcategoriesBtnChooser setTitle:subCategorName
                                  forState:UIControlStateNormal];
    
    self.productObject.productSubCategoryId = subCategoryId;
    self.productObject.productSubCategoryName = subCategorName;
}

- (void)citiesChoosedInList:(NSString *)cityId
                andCityName:(NSString *)cityName {
    [self.citiesBtnChooser setTitle:cityName forState:UIControlStateNormal];
    
    self.productObject.productCityId = cityId;
    self.productObject.productCityName = cityName;
}

- (void)createKeyboardAccessoryViewInChild {
     [self.priceFromTextField setInputAccessoryView:self.inputAccView];
     [self.priceToTextField setInputAccessoryView:self.inputAccView];
}

- (void)doneTyping {
    // When the "done" button is tapped, the keyboard should go away.
    // That simply means that we just have to resign our first responder.
    [self.priceFromTextField resignFirstResponder];
    [self.priceToTextField resignFirstResponder];
}


- (IBAction)searchBtnClicked:(id)sender {    
    self.productObject.productPriceFrom = self.priceFromTextField.text;
    self.productObject.productPriceTo = self.priceToTextField.text;
    self.productObject.isAllFiltersSelected=true;
    
    [self.delegate otherFiltersChoosedWithProductObject:self.productObject];
    
    [self.navigationController popViewControllerAnimated:YES];
}
@end
