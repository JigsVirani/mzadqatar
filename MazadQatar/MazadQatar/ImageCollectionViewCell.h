//
//  ImageCollectionViewCell.h
//  MazadQatar
//
//  Created by samo on 3/21/13.
//  Copyright (c) 2013 Mohammed abdulfattah attia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImageCollectionViewCell : UICollectionViewCell
@property(strong, nonatomic) IBOutlet UIImageView *imageViewCell;
@property(strong, nonatomic) IBOutlet UIActivityIndicatorView *loadingIndicator;
@property(strong, nonatomic) IBOutlet UILabel * titleLabel;
@property(strong, nonatomic) IBOutlet UILabel * priceLabel;
@end
