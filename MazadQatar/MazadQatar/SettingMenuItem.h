//
//  SettingMenuItem.h
//  Mzad Qatar
//
//  Created by Mohammed AbdulFattah on 2/15/15.
//  Copyright (c) 2015 Mohammed abdulfattah attia. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SettingMenuItem : NSObject

@property(nonatomic, strong) NSString *settingName;
@property(nonatomic, strong) NSString *settingImageName;

@end
