//
//  FilterCarsChooser.m
//  Mzad Qatar
//
//  Created by samo on 8/20/14.
//  Copyright (c) 2014 Mohammed abdulfattah attia. All rights reserved.
//

#import "FilterCarsChooser.h"

@interface FilterCarsChooser () <CitiesViewControllerDelegate>

@end

@implementation FilterCarsChooser

- (void)viewDidLoad {
  [super viewDidLoad];
    
    self.screenName = @"Filter Cars Screen";
    self.title = NSLocalizedString(@"Filter", nil);
//    UIImage *backgroundPattern = [UIImage imageNamed:@"bg.png"];
//    [self.view
//     setBackgroundColor:[UIColor colorWithPatternImage:backgroundPattern]];
    //Happen Use Above Code
  if (self.productObject.productCityName != nil) {
    [self.citiesChooserBtn setTitle:self.productObject.productCityName
                           forState:UIControlStateNormal];
  }

  if (self.productObject.productSubCategoryName != nil) {
    [self.motorChooserBtn setTitle:self.productObject.productSubCategoryName
                          forState:UIControlStateNormal];
  }

  if (self.productObject.productSubsubCategoryName != nil) {
    [self.modelChooserBtn setTitle:self.productObject.productSubsubCategoryName
                          forState:UIControlStateNormal];
  }

  if (self.productObject.productManfactureYearFrom != nil) {
    [self.yearFromTextField
        setText:self.productObject.productManfactureYearFrom];
  }

  if (self.productObject.productManfactureYearTo != nil) {
    [self.yearToTextField setText:self.productObject.productManfactureYearTo];
  }

  if (self.productObject.productPriceFrom != nil) {
    [self.priceFromTextField setText:self.productObject.productPriceFrom];
  }

  if (self.productObject.productPriceTo != nil) {
    [self.priceToTextField setText:self.productObject.productPriceTo];
  }

  if (self.productObject.productKmFrom != nil) {
    [self.kmFromTextField setText:self.productObject.productKmFrom];
  }

  if (self.productObject.productKmTo != nil) {
    [self.kmToTextField setText:self.productObject.productKmTo];
  }
    
    if([[SharedData getDeviceLanguage]isEqualToString:@"en"]){
        [self.citiesChooserBtn setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
        [self.motorChooserBtn setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
        [self.modelChooserBtn setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
        
        /*[self.citiesChooserBtn setBackgroundImage:[UIImage imageNamed:@"bk_choose_list_btn_right.png"] forState:UIControlStateNormal];
        [self.motorChooserBtn setBackgroundImage:[UIImage imageNamed:@"bk_choose_list_btn_right.png"] forState:UIControlStateNormal];
        [self.modelChooserBtn setBackgroundImage:[UIImage imageNamed:@"bk_choose_list_btn_right.png"] forState:UIControlStateNormal];*/
        
        [self.citiesChooserIMG setImage:[UIImage imageNamed:@"ic_english_arrow"]];
        [self.motorChooserIMG setImage:[UIImage imageNamed:@"ic_english_arrow"]];
        [self.modelChooserIMG setImage:[UIImage imageNamed:@"ic_english_arrow"]];
    }else{
        [self.citiesChooserBtn setContentHorizontalAlignment:UIControlContentHorizontalAlignmentRight];
        [self.motorChooserBtn setContentHorizontalAlignment:UIControlContentHorizontalAlignmentRight];
        [self.modelChooserBtn setContentHorizontalAlignment:UIControlContentHorizontalAlignmentRight];
        
        /*[self.citiesChooserBtn setBackgroundImage:[UIImage imageNamed:@"bk_choose_list_btn_left"] forState:UIControlStateNormal];
        [self.motorChooserBtn setBackgroundImage:[UIImage imageNamed:@"bk_choose_list_btn_left"] forState:UIControlStateNormal];
        [self.modelChooserBtn setBackgroundImage:[UIImage imageNamed:@"bk_choose_list_btn_left"] forState:UIControlStateNormal];*/
        
        [self.citiesChooserIMG setImage:[UIImage imageNamed:@"ic_arabic_arrow"]];
        [self.motorChooserIMG setImage:[UIImage imageNamed:@"ic_arabic_arrow"]];
        [self.modelChooserIMG setImage:[UIImage imageNamed:@"ic_arabic_arrow"]];
        
        [self.citiesChooserBtn setTitleEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 8)];
        [self.motorChooserBtn setTitleEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 8)];
        [self.modelChooserBtn setTitleEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 8)];
    }
    //[self.navigationController.navigationBar.topItem setTitle:NSLocalizedString(@"BACK", nil)];
}

- (void)didReceiveMemoryWarning {
  [super didReceiveMemoryWarning];
  // Dispose of any resources that can be recreated.
}

- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier
                                  sender:(id)sender {
  if ([identifier isEqualToString:@"SubSubCategorySegue"] ||
      [identifier isEqualToString:@"SubSubCategorySegueArrow"]) {
    if ([self.productObject.productSubCategoryId isEqualToString:@""] ||
        [self.productObject.productSubCategoryId isEqualToString:@"0"]) {
      [DialogManager showErrorSubCategoryNotChosen];

      return false;
    }
  }

  return true;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
  [super prepareForSegue:segue sender:sender];

  if ([[segue identifier] isEqualToString:@"SubCategorySegue"]) {
    SubCategoryChooser *subCategoryChooser = [segue destinationViewController];

    [subCategoryChooser
        setSubCategoryParamsWithCatId:self.productObject.productCategoryId
              andCurrentSubCategoryId:self.productObject.productSubCategoryId
                     andIsOpenedFromAddAdvertise:false
                          andDelegate:self];

  } else if ([[segue identifier] isEqualToString:@"SubSubCategorySegue"]) {
    SubSubCategoriesChooser *subSubCategoriesChooser =
        [segue destinationViewController];

    [subSubCategoriesChooser
        setSubSubCategoryParamsWithCatId:self.productObject.productCategoryId
                 andCurrentSubCategoryId:self.productObject.productSubCategoryId
              andCurrentSubSubCategoryId:self.productObject
                                             .productSubsubCategoryId
                andIsOpenedFormAdvertise:NO
                             andDelegate:self];
  } else if ([[segue identifier] isEqualToString:@"CitiesSegue"]) {
    CitiesViewController *citiesViewController =
        [segue destinationViewController];

    [citiesViewController
        setCityParamsWithCityId:self.productObject.productCityId
               andIsOpenedFromAddAdvertise:false
                    andDelegate:self];
  }
}

- (void)subCategoryChoosedInList:(NSString *)subCategoryId
              andSubCategoryName:(NSString *)subCategorName {
  [self.motorChooserBtn setTitle:subCategorName forState:UIControlStateNormal];
  self.productObject.productSubCategoryId = subCategoryId;
  self.productObject.productSubCategoryName = subCategorName;

  // reset subcategory
  self.productObject.productSubsubCategoryId = @"";
  self.productObject.productSubsubCategoryName =
      NSLocalizedString(@"FILTER_ALL_MODELS_TEXT", nil);
  [self.modelChooserBtn
      setTitle:NSLocalizedString(@"FILTER_ALL_MODELS_TEXT", nil)
      forState:UIControlStateNormal];
}

- (void)subsubCategoryChoosedInList:(NSString *)subsubCategoryId
              andSubsubCategoryName:(NSString *)subsubCategoryName {
  [self.modelChooserBtn setTitle:subsubCategoryName
                        forState:UIControlStateNormal];
  self.productObject.productSubsubCategoryId = subsubCategoryId;
  self.productObject.productSubsubCategoryName = subsubCategoryName;
}

- (void)citiesChoosedInList:(NSString *)cityId
                andCityName:(NSString *)cityName {
  [self.citiesChooserBtn setTitle:cityName forState:UIControlStateNormal];
  self.productObject.productCityId = cityId;
  self.productObject.productCityName = cityName;
}

- (void)createKeyboardAccessoryViewInChild {
  [self.priceFromTextField setInputAccessoryView:self.inputAccView];
  [self.priceToTextField setInputAccessoryView:self.inputAccView];
  [self.yearFromTextField setInputAccessoryView:self.inputAccView];
  [self.yearToTextField setInputAccessoryView:self.inputAccView];
  [self.kmFromTextField setInputAccessoryView:self.inputAccView];
  [self.kmToTextField setInputAccessoryView:self.inputAccView];
}

- (void)doneTyping {
  // When the "done" button is tapped, the keyboard should go away.
  // That simply means that we just have to resign our first responder.
  [self.priceFromTextField resignFirstResponder];
  [self.priceToTextField resignFirstResponder];
  [self.yearFromTextField resignFirstResponder];
  [self.yearToTextField resignFirstResponder];
  [self.kmFromTextField resignFirstResponder];
  [self.kmToTextField resignFirstResponder];
}


- (BOOL)textField:(UITextField *)textField
    shouldChangeCharactersInRange:(NSRange)range
                replacementString:(NSString *)string {
  // you only need this check if you have more than one textfield on the view:
  if (textField == self.yearFromTextField ||
      textField == self.yearToTextField) {

    int maxLength = 4;

    if (textField.text.length - range.length + string.length > maxLength) {
      if (string.length > 1) { // only show popup if cut-and-pasting:
        [DialogManager showErrorWithMessage:NSLocalizedString(
                                                @"YEAR_CHARACTER_LIMIT ", nil)];
      }
      return NO;
    }
  }
  return YES;
}

- (IBAction)searchBtnClicked:(id)sender {

  self.productObject.productKmFrom = self.kmFromTextField.text;
  self.productObject.productKmTo = self.kmToTextField.text;
  self.productObject.productManfactureYearFrom = self.yearFromTextField.text;
  self.productObject.productManfactureYearTo = self.yearToTextField.text;
  self.productObject.productPriceFrom = self.priceFromTextField.text;
  self.productObject.productPriceTo = self.priceToTextField.text;

  [self.delegate carFiltersChoosedWithProductObject:self.productObject];

  [self.navigationController popViewControllerAnimated:YES];
}



@end
