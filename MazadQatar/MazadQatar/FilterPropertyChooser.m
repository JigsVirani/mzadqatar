//
//  FilterPropertyChooser.m
//  Mzad Qatar
//
//  Created by samo on 9/7/14.
//  Copyright (c) 2014 Mohammed abdulfattah attia. All rights reserved.
//

#import "FilterPropertyChooser.h"

@interface FilterPropertyChooser () <CitiesViewControllerDelegate>

@end

@implementation FilterPropertyChooser

- (void)viewDidLoad {
  [super viewDidLoad];
    
    self.screenName = @"Filter Property Chooser";
//  UIImage *backgroundPattern = [UIImage imageNamed:@"bg.png"];
//  [self.view setBackgroundColor:[UIColor colorWithPatternImage:backgroundPattern]];

  if (self.productObject.productCityName != nil) {
    [self.citiesChooserBtn setTitle:self.productObject.productCityName
                           forState:UIControlStateNormal];
  }

  if (self.productObject.productRegionName != nil) {
    [self.regionChooserBtn setTitle:self.productObject.productRegionName
                           forState:UIControlStateNormal];
  }

  if (self.productObject.productSubCategoryName != nil) {
    [self.subCateogryChooserBtn
        setTitle:self.productObject.productSubCategoryName
        forState:UIControlStateNormal];
  }

  if (self.productObject.productFurnishedTypeName != nil) {
    [self.furnishedChooserBtn
        setTitle:self.productObject.productFurnishedTypeName
        forState:UIControlStateNormal];
  }

  if (self.productObject.productPriceFrom != nil) {
    [self.priceFromTextField setText:self.productObject.productPriceFrom];
  }

  if (self.productObject.productPriceTo != nil) {
    [self.priceToTextField setText:self.productObject.productPriceTo];
  }

  if (self.productObject.productNumberOfRoomsFrom != nil) {
    [self.roomsFromTextFIeld
        setText:self.productObject.productNumberOfRoomsFrom];
  }

  if (self.productObject.productNumberOfRoomsTo != nil) {
    [self.roomsToTextFIeld setText:self.productObject.productNumberOfRoomsTo];
  }
    
    if([[SharedData getDeviceLanguage]isEqualToString:@"en"]){
        [self.citiesChooserBtn setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
        [self.regionChooserBtn setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
        [self.subCateogryChooserBtn setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
        [self.furnishedChooserBtn setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
        
        [self.citiesChooserImg setImage:[UIImage imageNamed:@"ic_english_arrow"]];
        [self.regionChooserImg setImage:[UIImage imageNamed:@"ic_english_arrow"]];
        [self.subCateogryChooserImg setImage:[UIImage imageNamed:@"ic_english_arrow"]];
        [self.furnishedChooserImg setImage:[UIImage imageNamed:@"ic_english_arrow"]];
        
        /*[self.citiesChooserBtn setBackgroundImage:[UIImage imageNamed:@"bk_choose_list_btn_right.png"] forState:UIControlStateNormal];
        [self.regionChooserBtn setBackgroundImage:[UIImage imageNamed:@"bk_choose_list_btn_right.png"] forState:UIControlStateNormal];
        [self.subCateogryChooserBtn setBackgroundImage:[UIImage imageNamed:@"bk_choose_list_btn_right.png"] forState:UIControlStateNormal];
        [self.furnishedChooserBtn setBackgroundImage:[UIImage imageNamed:@"bk_choose_list_btn_right.png"] forState:UIControlStateNormal];*/
    }else{
        [self.citiesChooserBtn setContentHorizontalAlignment:UIControlContentHorizontalAlignmentRight];
        [self.regionChooserBtn setContentHorizontalAlignment:UIControlContentHorizontalAlignmentRight];
        [self.subCateogryChooserBtn setContentHorizontalAlignment:UIControlContentHorizontalAlignmentRight];
        [self.furnishedChooserBtn setContentHorizontalAlignment:UIControlContentHorizontalAlignmentRight];
        
        [self.citiesChooserImg setImage:[UIImage imageNamed:@"ic_arabic_arrow"]];
        [self.regionChooserImg setImage:[UIImage imageNamed:@"ic_arabic_arrow"]];
        [self.subCateogryChooserImg setImage:[UIImage imageNamed:@"ic_arabic_arrow"]];
        [self.furnishedChooserImg setImage:[UIImage imageNamed:@"ic_arabic_arrow"]];
        
        /*[self.citiesChooserBtn setBackgroundImage:[UIImage imageNamed:@"bk_choose_list_btn_left"] forState:UIControlStateNormal];
        [self.regionChooserBtn setBackgroundImage:[UIImage imageNamed:@"bk_choose_list_btn_left"] forState:UIControlStateNormal];
        [self.subCateogryChooserBtn setBackgroundImage:[UIImage imageNamed:@"bk_choose_list_btn_left"] forState:UIControlStateNormal];
        [self.furnishedChooserBtn setBackgroundImage:[UIImage imageNamed:@"bk_choose_list_btn_left"] forState:UIControlStateNormal];*/
        
        [self.citiesChooserBtn setTitleEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 8)];
        [self.regionChooserBtn setTitleEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 8)];
        [self.subCateogryChooserBtn setTitleEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 8)];
        [self.furnishedChooserBtn setTitleEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 8)];
    }
    //[self.navigationController.navigationBar.topItem setTitle:NSLocalizedString(@"BACK", nil)];
}

- (void)didReceiveMemoryWarning {
  [super didReceiveMemoryWarning];
  // Dispose of any resources that can be recreated.
}

- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier
                                  sender:(id)sender {
  if ([identifier isEqualToString:@"RegionSegue"]) {
    if ([self.productObject.productCityId isEqualToString:@""]||[self.productObject.productCityId isEqualToString:@"0"]) {
      [DialogManager showErrorCitiesNotChosen];

      return false;
    }
  }

  return true;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
  [super prepareForSegue:segue sender:sender];

  if ([[segue identifier] isEqualToString:@"CitiesSegue"]) {
    CitiesViewController *citiesViewController =
        [segue destinationViewController];

    [citiesViewController
        setCityParamsWithCityId:self.productObject.productCityId
               andIsOpenedFromAddAdvertise:false
                    andDelegate:self];
  } else if ([[segue identifier] isEqualToString:@"RegionSegue"]) {
    RegionListChooser *regionListChooser = [segue destinationViewController];

    [regionListChooser
        setRegionParamsWithRegionId:self.productObject.productRegionId
                  andSelectedCityId:self.productObject.productCityId andIsOpenedFromAddAdvertise:NO
                        andDelegate:self];
  } else if ([[segue identifier] isEqualToString:@"SubCategorySegue"]) {
    SubCategoryChooser *subCategoryChooser = [segue destinationViewController];

    [subCategoryChooser
        setSubCategoryParamsWithCatId:self.productObject.productCategoryId
              andCurrentSubCategoryId:self.productObject.productSubsubCategoryId
                     andIsOpenedFromAddAdvertise:false
                          andDelegate:self];
  } else if ([[segue identifier] isEqualToString:@"furnishedSegue"]) {
    FurnishedTypeChooserViewController *furnishedTypeChooserViewController =
        [segue destinationViewController];

    [furnishedTypeChooserViewController
        setFurnishedParamsWithFurnishedTypeId:self.productObject
                                                  .productFurnishedTypeId
                                  andDelegate:self andIsAddAdvertise:false];
  }
}

- (void)subCategoryChoosedInList:(NSString *)subCategoryId
              andSubCategoryName:(NSString *)subCategorName {
  [self.subCateogryChooserBtn setTitle:subCategorName
                              forState:UIControlStateNormal];
  self.productObject.productSubCategoryId = subCategoryId;
  self.productObject.productSubCategoryName = subCategorName;
}

- (void)citiesChoosedInList:(NSString *)cityId
                andCityName:(NSString *)cityName {
  [self.citiesChooserBtn setTitle:cityName forState:UIControlStateNormal];
  self.productObject.productCityId = cityId;
  self.productObject.productCityName = cityName;

  // reset subcategory
  self.productObject.productRegionId = @"";
  self.productObject.productRegionName =
      NSLocalizedString(@"FILTER_ALL_REGIONS_TEXT", nil);
  [self.regionChooserBtn
      setTitle:NSLocalizedString(@"FILTER_ALL_REGIONS_TEXT", nil)
      forState:UIControlStateNormal];
}

- (IBAction)searchBtnClicked:(id)sender {
  self.productObject.productNumberOfRoomsFrom = self.roomsFromTextFIeld.text;
  self.productObject.productNumberOfRoomsTo = self.roomsToTextFIeld.text;
  self.productObject.productPriceFrom = self.priceFromTextField.text;
  self.productObject.productPriceTo = self.priceToTextField.text;

  [self.delegate propertyFiltersChoosedWithProductObject:self.productObject];

  [self.navigationController popViewControllerAnimated:YES];
}

- (void)regionChoosedInList:(NSString *)regionId
              andRegionName:(NSString *)regionName {
  [self.regionChooserBtn setTitle:regionName forState:UIControlStateNormal];
  self.productObject.productRegionId = regionId;
  self.productObject.productRegionName = regionName;
}

- (void)furnishedTypeChoosedInList:(NSString *)furnishedTypeId
                  andFurnishedName:(NSString *)furnishedName {
  [self.furnishedChooserBtn setTitle:furnishedName
                            forState:UIControlStateNormal];
  self.productObject.productFurnishedTypeId = furnishedTypeId;
  self.productObject.productFurnishedTypeName = furnishedName;
}

- (void)createKeyboardAccessoryViewInChild {
  [self.priceFromTextField setInputAccessoryView:self.inputAccView];
  [self.priceToTextField setInputAccessoryView:self.inputAccView];
  [self.roomsFromTextFIeld setInputAccessoryView:self.inputAccView];
  [self.roomsToTextFIeld setInputAccessoryView:self.inputAccView];
}

- (void)doneTyping {
  // When the "done" button is tapped, the keyboard should go away.
  // That simply means that we just have to resign our first responder.
  [self.priceFromTextField resignFirstResponder];
  [self.priceToTextField resignFirstResponder];
  [self.roomsFromTextFIeld resignFirstResponder];
  [self.roomsToTextFIeld resignFirstResponder];
}

@end
