//
//  MoreDescriptionViewController.m
//  Mzad Qatar
//
//  Created by Paresh Kacha on 01/12/15.
//  Copyright © 2015 Mohammed abdulfattah attia. All rights reserved.
//

#import "MoreDescriptionViewController.h"

@interface MoreDescriptionViewController ()

@end

@implementation MoreDescriptionViewController
NSString *getMoreInfo = @"getMoreInfo";
- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    self.screenName = @"More Description Screen";
    
    propertyArry = [NSMutableArray new];
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [hud setLabelText:NSLocalizedString(@"LOADING_MOREINFORMATION", Nil)];
    [ServerManager getMoreInfo:self.productObject AndDelegate:self withRequestId:getMoreInfo];
    // Do any additional setup after loading the view.
    //[self.navigationController.navigationBar.topItem setTitle:NSLocalizedString(@"BACK", nil)];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return propertyArry.count;
}
//-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
//        [cell setSeparatorInset:UIEdgeInsetsZero];
//    }
//    
//    // Prevent the cell from inheriting the Table View's margin settings
//    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
//        [cell setPreservesSuperviewLayoutMargins:NO];
//    }
//    
//    // Explictly set your cell's layout margins
//    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
//        [cell setLayoutMargins:UIEdgeInsetsZero];
//    }
//}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    // This will create a "invisible" footer
    return 0.01f;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    // To "clear" the footer view
    return [UIView new] ;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell * cell =
    [tableView dequeueReusableCellWithIdentifier:@"moreInfoCell"
                                    forIndexPath:indexPath];
    UILabel * titleLabel = (UILabel *)[cell viewWithTag:1];
    UILabel * descriptionLabel = (UILabel *)[cell viewWithTag:2];
    NSMutableDictionary * dict = [propertyArry objectAtIndex:indexPath.row];
    if(![[[dict allKeys] lastObject] isKindOfClass:[NSNull class]] && ![[[dict allKeys] lastObject] isEqualToString:@""])
    {
        titleLabel.text = [[dict allKeys] lastObject];
    }
    else
    {
        titleLabel.text = @"-";
    }
    if(![[[dict allValues] lastObject] isKindOfClass:[NSNull class]] && ![[[dict allValues] lastObject] isEqualToString:@""])
    {
        descriptionLabel.text = [[dict allValues] lastObject];
    }
    else
    {
        descriptionLabel.text = @"-";
    }
    
    if([LanguageManager currentLanguageIndex] == 1){
        titleLabel.textAlignment = NSTextAlignmentRight;
    }else{
        titleLabel.textAlignment = NSTextAlignmentLeft;
    }
    cell.separatorInset = UIEdgeInsetsZero;
    return cell;
}
- (void)requestFinished:(ServerManagerResult *)serverManagerResult
            ofRequestId:(NSString *)requestId
    withNumberOfRequest:(int)numberOfRequest {
    if ([serverManagerResult.opertationResult
         isEqualToString:[ServerManager ERROR_IN_CONNECTION_OPERATION]]) {
        [DialogManager showErrorInConnection];
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        return;
    }

    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    if (serverManagerResult.jsonArray != [NSNull null]) {
        propertyArry = serverManagerResult.jsonArray;
    }

    [self.tableView reloadData];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
