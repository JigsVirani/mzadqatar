//
//  ImageViewerTableCell.m
//  MazadQatar
//
//  Created by samo on 4/17/13.
//  Copyright (c) 2013 Mohammed abdulfattah attia. All rights reserved.
//

#import "ImageViewerTableCell.h"
#import "SelectImageDialogueViewController.h"

@implementation ImageViewerTableCell

static NSString *CellIdentifier = @"GridCell";
// when sdwebimage finished and view deallocated , crash happen
// so we must have flag if view finished or not
// static bool isViewFinished=false;
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

- (void)loadCell:(UIViewController *)parentViewParam
       isAddCell:(bool)isAddCellParam
 isEditAdvertise:(BOOL)isEditAdvertiseParam
withProductObject:(ProductObject *)productObjectParam
isloadingProductDetail:(BOOL)isLoading
withImageViewerDelegate:(id<ImageViewerDelegate>)imageViewerDelegateParam;
{
    // isViewFinished=false;
    parentView = parentViewParam;
    isAddCell = isAddCellParam;
    imageViewerDelegate = imageViewerDelegateParam;
    isEditAdvertise = isEditAdvertiseParam;
    
    if(![[LanguageManager currentLanguageCode]isEqualToString:@"en"]){
        [self.addImageInEditBtn setTitleEdgeInsets:UIEdgeInsetsMake(0, 20, 0, 0)];
        [self.setAsDefaultInEditBtn setTitleEdgeInsets:UIEdgeInsetsMake(0, 5, 0, 0)];
        [self.deleteInEditBtn setTitleEdgeInsets:UIEdgeInsetsMake(0, 20, 0, 0)];
    }else{
        /*addImageInEditBtn;
        setAsDefaultInEditBtn;
        deleteInEditBtn;*/
    }
    if (isEditAdvertise) {
        [self startSettingImageViewerUi:productObjectParam
                              isLoading:isLoading
                              isAddCell:isAddCell];
        
        if (productObject.productImagesOnDevices.count == 0) {
            //Change By Vish 08/07/2017
            if(productObject.productImagesUrls == nil){
                productObject.productImagesUrls = [[NSMutableArray alloc]init];
            }
            productObject.productImagesOnDevices = productObject.productImagesUrls;
            
            //Change By Vish 08/07/2017
            if(productObjectParam.productMainImageUrl != nil){
                [productObject.productImagesOnDevices addObject:productObject.productMainImageUrl];
            }
        }
        [self.collectionView reloadData];
        
        [self.pageControlCount
         setNumberOfPages:productObject.productImagesOnDevices.count];
        if (productObject.productImagesOnDevices.count == 0 && isLoading == false) {
            [self hideAddImage:NO];
        }
        [self hideCommentLikeCount:YES];
    } else if (isAddCell) {
        //#TODO: commented the under line because when changing lang image doesnt
        //download agian , check if the under line issue will appear
        // not to renew with /Volumes/Projects/Projects/WorkinProjects/MzadQatarV2/MazadQatar/Mzad Qatar.xcworkspacescrolling in table view
        // if(productObject==nil||)
        //{
        
        [self hideAddProductView:YES];
        productObject = productObjectParam;
        [self.pageControlCount
         setNumberOfPages:productObject.productImagesOnDevices.count];
        [self.collectionView reloadData];
        //}
        if (productObject.productImagesOnDevices.count == 0) {
            [self hideAddImage:NO];
        }
        
        [self hideCommentLikeCount:YES];
    } else {
        [self startSettingImageViewerUi:productObjectParam
                              isLoading:isLoading
                              isAddCell:isAddCell];
        [self.pageControlCount
         setNumberOfPages:productObject.productImagesUrls.count];
    }
}

- (void)startSettingImageViewerUi:(ProductObject *)productObjectParam
                        isLoading:(BOOL)isLoading
                        isAddCell:(bool)isAddCellParam {
    if (isLoading)
    {
        [self.loadingProductDetailsIndicator startAnimating];
    }else
    {
        [self.loadingProductDetailsIndicator stopAnimating];
        //  if(productObject==nil)
        //  {
        productObject = productObjectParam;
        //      NSLog(@"count=%lu",(unsigned long)[productObject retainCount]);
        //  }
        isAddCell = isAddCellParam;
        [self.btnLikeCount setTitle:productObject.productLikeCount
                           forState:UIControlStateNormal];
        [self.btnCommentsCount setTitle:productObject.productCommentCount
                               forState:UIControlStateNormal];
        // [self.collectionView reloadData];
    }
    [self hideAddProductView:YES];
}

//========================collection view cell

//- (UICollectionReusableView *)collectionView:(UICollectionView
//*)collectionView viewForSupplementaryElementOfKind:(NSString *)kind
//atIndexPath:(NSIndexPath *)indexPath
//{
//    if(isAddCell&&productObject.productImagesDownloaded.count>0)
//    {
//        UICollectionReusableView *reusableview = nil;
//
//        ImageCollectionFooterCell *footerview = [collectionView
//        dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader
//        withReuseIdentifier:@"HeaderView" forIndexPath:indexPath];
//
//        reusableview = footerview;
//
//        footerSize=footerview.frame.size;
//
//        return reusableview;
//    }
//    else
//    {
//        ImageCollectionViewCell *emptyCell=[[[ImageCollectionViewCell alloc]
//        init] autorelease];
//        [emptyCell setFrame:CGRectZero];
//        return emptyCell;
//    }
//
//    //return nil;
//}

- (NSInteger)collectionView:(UICollectionView *)collectionView
     numberOfItemsInSection:(NSInteger)section {
    if (isEditAdvertise) {
        return productObject.productImagesOnDevices.count;
    } else if (isAddCell == true) {
        return [productObject.productImagesOnDevices count];
    } else {
        return [productObject.productImagesUrls count];
    }
}

//- (CGSize)collectionView:(UICollectionView *)collectionView
//layout:(UICollectionViewLayout*)collectionViewLayout
//referenceSizeForHeaderInSection:(NSInteger)section
//{
//    if(isAddCell==false)
//    {
//        return CGSizeMake(0, 0);
//    }
//    else
//    {
//        if(productObject.productImagesDownloaded.count>0)
//        {
//            return CGSizeMake(collectionView.frame.size.width/3,
//            collectionView.frame.size.height);
//
//        }
//        else
//        {
//            return CGSizeMake(0, 0);
//        }
//    }
//}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    CGFloat pageWidth = self.collectionView.frame.size.width - 20;
    self.pageControlCount.currentPage =
    self.collectionView.contentOffset.x / pageWidth;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)cv
                  cellForItemAtIndexPath:(NSIndexPath *)indexPath;
{
    // reuse cell
    ImageCollectionViewCell *cell =
    [cv dequeueReusableCellWithReuseIdentifier:CellIdentifier
                                  forIndexPath:indexPath];
    
    cell.imageViewCell.layer.cornerRadius = 5.0;
    cell.imageViewCell.clipsToBounds = YES;
    
    if (isEditAdvertise == true) {
        //        if(indexPath.row>=productObject.productImagesDownloaded.count||productObject.productImagesDownloaded.count==0)
        //        {
        //            cell=[self startSettingImageFromServer:cell
        //            withIndexPath:indexPath
        //            fromArray:productObject.productImagesUrls];
        //        }
        //        else
        //        {
        //            cell=[self startLoadingFromImagesDownloaded:cell
        //            withIndexPath:indexPath
        //            fromArray:productObject.productImagesDownloaded];
        //        }
        
        if ([[productObject.productImagesOnDevices objectAtIndex:indexPath.row]
             isKindOfClass:[UIImage class]]) {
            cell =
            [self startLoadingFromImagesDownloaded:cell
                                     withIndexPath:indexPath
                                         fromArray:productObject
             .productImagesOnDevices];
        } else {
            cell = [self
                    startSettingImageFromServer:cell
                    withIndexPath:indexPath
                    fromArray:productObject.productImagesOnDevices];
        }
    } else if (isAddCell == true) {
        cell = [self
                startLoadingFromImagesDownloaded:cell
                withIndexPath:indexPath
                fromArray:productObject.productImagesOnDevices];
    } else {
        cell = [self startSettingImageFromServer:cell
                                   withIndexPath:indexPath
                                       fromArray:productObject.productImagesUrls];
    }
    
    return cell;
}

- (ImageCollectionViewCell *)
startLoadingFromImagesDownloaded:(ImageCollectionViewCell *)cell
withIndexPath:(NSIndexPath *)indexPath
fromArray:(NSMutableArray *)imageArray {
    [cell.loadingIndicator startAnimating];
    [cell.imageViewCell setImage:[imageArray objectAtIndex:indexPath.row]];
    [cell.loadingIndicator stopAnimating];
    return cell;
}

- (ImageCollectionViewCell *)
startSettingImageFromServer:(ImageCollectionViewCell *)cell
withIndexPath:(NSIndexPath *)indexPath
fromArray:(NSMutableArray *)imageArray {
    NSString *imageName =
    [[productObject productId] stringByAppendingString:@".png"];
    NSString *url = [imageArray objectAtIndex:indexPath.row];
    
    [cell setUserInteractionEnabled:false];
    [cell.loadingIndicator startAnimating];
    if ([url isKindOfClass:[NSString class]]) {
        
        [cell.imageViewCell
         sd_setImageWithURL:[NSURL URLWithString:url]
         placeholderImage:[UIImage imageNamed:imageName]
         completed:^(UIImage *image, NSError *error,
                     SDImageCacheType cacheType, NSURL *imageURL) {
             [cell setUserInteractionEnabled:true];
             
//             if (image != nil) {
//                 if (indexPath.row == 0) {
//                     productObject.productShareImage=image;
//                }
//             }
             
             [cell.loadingIndicator stopAnimating];       }];
        
    }
    return cell;
}

//===========================image navigation browser
- (void)collectionView:(UICollectionView *)collectionView
didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    // Create & present browser
    MWPhotoBrowser *browser = [[MWPhotoBrowser alloc] initWithDelegate:self];
    // Set options
    //  browser.wantsFullScreenLayout = YES; // Decide if you want the photo
    //  browser full screen, i.e. whether the status bar is affected (defaults to
    //  YES)
    if (isAddCell) {
        
        browser.displayActionButton = YES; // Show action button to save, copy or
        // email photos (defaults to NO)
        
    }
    [browser setCurrentPhotoIndex:indexPath.row]; // Example: allows second image
    // to be presented first
    // Present
    [parentView.navigationController pushViewController:browser animated:YES];
    
}

- (NSUInteger)numberOfPhotosInPhotoBrowser:(MWPhotoBrowser *)photoBrowser {
    if (isEditAdvertise == true) {
        return productObject.productImagesOnDevices.count;
    } else if (isAddCell == true) {
        return productObject.productImagesOnDevices.count;
    } else {
        return productObject.productImagesUrls.count;
    }
}

- (id<MWPhoto>)photoBrowser:(MWPhotoBrowser *)photoBrowser
               photoAtIndex:(NSUInteger)index {
    // if image is not downloaded return url
    if (index < productObject.productImagesOnDevices.count) {
        if ([[productObject.productImagesOnDevices objectAtIndex:index]
             isKindOfClass:[UIImage class]]) {
            return [MWPhoto photoWithImage:[productObject.productImagesOnDevices
                                            objectAtIndex:index]];
        } else {
            return [MWPhoto
                    photoWithURL:[NSURL URLWithString:[productObject.productImagesUrls
                                                       objectAtIndex:index]]];
        }
    } else {
        return [MWPhoto
                photoWithURL:[NSURL URLWithString:[productObject.productImagesUrls
                                                   objectAtIndex:index]]];
    }
}

- (void)imageDeleted:(int)imageIndex {
    if (isEditAdvertise) {
        [productObject setIsResetProductImages:@"true"];
    }
    
    [productObject setIsUploadAdvertiseImages:YES];
    
    [productObject.productImagesOnDevices removeObjectAtIndex:imageIndex];
    
    [self.pageControlCount
     setNumberOfPages:productObject.productImagesOnDevices.count];
    
    // show only result in case of images more than one as it will be noticed if
    // no images
    if (productObject.productImagesOnDevices.count > 0) {
        [_editButtonsResultTextField setHidden:false];
        [_editButtonsResultTextField
         setText:NSLocalizedString(@"EDIT_DELETE_IMAGE_SUCCESS", nil)];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [_editButtonsResultTextField setHidden:true];
        });
    } else {
        [self hideAddImage:NO];
    }
    
    [self.collectionView reloadData];
}

- (void)imageDefaultImageChanged:(int)imageIndex {
    if (isEditAdvertise) {
        [productObject setIsResetProductImages:@"true"];
    }
    
    [productObject setIsUploadAdvertiseImages:YES];
    
    [productObject.productImagesOnDevices
     insertObject:[productObject.productImagesOnDevices
                   objectAtIndex:imageIndex]
     atIndex:0];
    
    // add 1 because we add element at index 0
    [productObject.productImagesOnDevices removeObjectAtIndex:imageIndex + 1];
    
    // show only result in case of images more than one as it will be noticed if
    // no images
    if (productObject.productImagesOnDevices.count > 0) {
        [_editButtonsResultTextField setHidden:false];
        [_editButtonsResultTextField
         setText:NSLocalizedString(@"EDIT_SET_DEFAULT_IMAGE_SUCCESS", nil)];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [_editButtonsResultTextField setHidden:true];
        });
    }
    
    [self.collectionView reloadData];
}

// camera image
- (void)didFinishPickingMediaWithInfo:(UIImage *)imageCaptured {
    if (isEditAdvertise) {
        [productObject setIsResetProductImages:@"true"];
    }
    
    
    if (productObject.productImagesOnDevices.count > 14) {
        [DialogManager showErrorMaxImagesLimit];
        return;
    }
    
    [imageViewerDelegate imageAddedToProductObject:imageCaptured
                                   isEditAdvertise:isEditAdvertise];
}

- (void)SelectImageDelegatePhoto:(UIImage *)photo {
    if (productObject.productImagesOnDevices.count > 14) {
        [DialogManager showErrorMaxImagesLimit];
        return;
    }
    
    UIImage *imageCompressed = [self compreessImage:photo];
    
    UIImage *imageResized =
    [self resizeImage:imageCompressed toSize:CGSizeMake(600, 600)];
    
    NSData *imgData = UIImageJPEGRepresentation(imageResized, 0);
    
    if (imgData.length > [SharedData maxImageSize]) {
        [DialogManager showErrorImageSizeLimit];
        return;
    }
    
    if (isEditAdvertise) {
        [productObject setIsResetProductImages:@"true"];
    }
    
    [productObject setIsUploadAdvertiseImages:YES];
    
    [imageViewerDelegate imageAddedToProductObject:imageResized
                                   isEditAdvertise:isEditAdvertise];
}

- (UIImage *)resizeImage:(UIImage *)image toSize:(CGSize)newSize;
{
    UIGraphicsBeginImageContext(newSize);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}

- (UIImage *)compreessImage:(UIImage *)photo {
    NSData *data = UIImageJPEGRepresentation(photo, 0.40);
    
    CGFloat compression = 0.4f;
    CGFloat maxCompression = 0.1f;
    
    while ([data length] > [SharedData compressImageSizeToReduce] &&
           compression > maxCompression) {
        compression -= 0.1;
        data = UIImageJPEGRepresentation(photo, compression);
    }
    
    return [UIImage imageWithData:data];
}
//=====================prepare the segue

- (IBAction)addImageButtonClicked:(UIButton *)sender {
    // issue in keyboard when moving anotther screen
    [parentView.view endEditing:YES];
    
    [self showSelectImageDialog:sender];
}

- (void)showSelectImageDialog:(UIButton *)sender {
    SelectImageDialogueViewController *controller = [
                                                     [SharedData getCurrentStoryBoard] instantiateViewControllerWithIdentifier:
                                                     @"SelectImageDialogViewController"];
    controller.delegate = self;
    
    [parentView.view addSubview:controller.view];
    [parentView addChildViewController:controller];
}

- (void)hideAddProductView:(BOOL)isHide;
{
    [self hideAddImage:isHide];
    if (productObject.isHideAddCommentsAndLikes == true) {
        [self hideCommentLikeCount:YES];
    } else {
        [self hideCommentLikeCount:!isHide];
    }
}

- (void)hideAddImage:(bool)isHide {
    
    if(isHide == NO){
        NSMutableAttributedString *mutableAttString = [[NSMutableAttributedString alloc] init];
        NSDictionary *attrs1 = @{NSFontAttributeName:[UIFont fontWithName:@"HelveticaNeue" size:14.0],NSForegroundColorAttributeName :[UIColor whiteColor]};
        NSDictionary *attrs2 = @{NSFontAttributeName:[UIFont fontWithName:@"HelveticaNeue" size:14.0],NSForegroundColorAttributeName :[UIColor colorWithRed:178/255.0 green:57/255.0 blue:103/255.0 alpha:1.0]};
        
        NSAttributedString *attString1 =[[NSAttributedString alloc] initWithString:NSLocalizedString(@"Image_Add_Info_1", nil) attributes:attrs1];
        NSAttributedString *attString2 =[[NSAttributedString alloc] initWithString:NSLocalizedString(@"Image_Add_Info_2", nil) attributes:attrs2];
        
        [mutableAttString appendAttributedString:attString1];
        [mutableAttString appendAttributedString:attString2];
        
        self.labelAtLeastImages.attributedText = mutableAttString;
    }
    
    if([[LanguageManager currentLanguageCode]isEqualToString:@"ar"]){
        [self.labelAtLeastImages setTextAlignment:NSTextAlignmentRight];
    }else{
        [self.labelAtLeastImages setTextAlignment:NSTextAlignmentLeft];
    }
    
    [self.imageInitialAddImage setHidden:isHide];
    [self.labelInitialAddImage setHidden:isHide];
    [self.labelAtLeastImages setHidden:isHide];
    
    if (isEditAdvertise == true) {
        //        [self.smallAddImage setHidden:isHide];
        //        [self.smallAddImageText setHidden:isHide];
        [self hideEditAdButtons:!isHide];
    } else if (isAddCell) {
        //[self.smallAddImage setHidden:!isHide];
        // [self.smallAddImageText setHidden:!isHide];
        [self hideEditAdButtons:!isHide];
        
    } else {
        [self.smallAddImage setHidden:isHide];
        [self.smallAddImageText setHidden:isHide];
    }
}

- (void)hideEditAdButtons:(bool)isHide {
    [self.setAsDefaultInEditBtn setHidden:isHide];
    [self.setAsDefaultInEditIcon setHidden:isHide];
    [self.deleteInEditBtn setHidden:isHide];
    [self.deleteInEditIcon setHidden:isHide];
    [self.addImageInEditBtn setHidden:isHide];
    [self.addImageInEditIcon setHidden:isHide];
}

- (void)hideCommentLikeCount:(bool)isHide {
    [self.btnCommentsCount setHidden:isHide];
    [self.btnLikeCount setHidden:isHide];
}

- (IBAction)deleteInEditClicked:(id)sender {
    [self imageDeleted:(int)_pageControlCount.currentPage];
}

- (IBAction)setAsDefaultInEditClicked:(id)sender {
    [self imageDefaultImageChanged:(int)_pageControlCount.currentPage];
}

- (void)dealloc {
    
    if (isAddCell == false) {
        //      [productObject release];
    }
    // isViewFinished=true;
}
@end
