//
//  NotificationViewCell.m
//  Mzad Qatar
//
//  Created by Perveen Akhtar on 02/07/2014.
//  Copyright (c) 2014 Mohammed abdulfattah attia. All rights reserved.
//

#import "NotificationViewCell.h"

@implementation NotificationViewCell


NSString *stopNotificationRequest = @"stopNotificationRequest";

- (id)initWithStyle:(UITableViewCellStyle)style
    reuseIdentifier:(NSString *)reuseIdentifier {
  self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
  if (self) {
    // Initialization code
  }
  return self;
}

- (void)setNotiifcationProductId:(NotificationModal *)notificationModelParam {
  notificationModel = notificationModelParam;

  if (notificationModel.isStoppingNotification) {
    [self hideStopNotificationUi:NO];
    [self.stopNotificationResult
        setText:notificationModelParam.stoppingNotificationResult];
    [self.stopNotifcationIndicator startAnimating];
  } else {
    [self hideStopNotificationUi:YES];
    [self.stopNotifcationIndicator stopAnimating];
  }
}

- (void)hideStopNotificationUi:(BOOL)isHide {
  [self.stopNotificationResult setHidden:isHide];
  [self.stopNotifcationIndicator setHidden:isHide];
  [self.stopNotificationButton setHidden:!isHide];
  [self.stopNotificationUnderline setHidden:!isHide];
}

- (IBAction)stopNotifcationClicked:(id)sender {
  notificationModel.isStoppingNotification = YES;

  [self hideStopNotificationUi:NO];

  [self.stopNotifcationIndicator startAnimating];

  [ServerManager stopNotificationOfProductId:notificationModel.productId
                           andNotificationId:notificationModel.notificationId
                                andRequestId:stopNotificationRequest
                                 andDelegate:self];
}

- (void)requestFinished:(ServerManagerResult *)serverManagerResult
            ofRequestId:(NSString *)requestId
    withNumberOfRequest:(int)numberOfRequest {
  notificationModel.isStoppingNotification = NO;

  if ([requestId isEqualToString:stopNotificationRequest]) {
    [self.stopNotifcationIndicator stopAnimating];

    notificationModel.stoppingNotificationResult =
        serverManagerResult.opertationMsg;

    [self.stopNotificationResult setHidden:NO];
    [self.stopNotificationResult setText:serverManagerResult.opertationMsg];
  }
}

@end
