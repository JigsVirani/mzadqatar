//
//  CitiesAndRegionsObject.m
//  Mzad Qatar
//
//  Created by samo on 8/28/14.
//  Copyright (c) 2014 Mohammed abdulfattah attia. All rights reserved.
//

#import "CitiesObject.h"

@implementation CitiesObject

NSString *CITY_ID_KEY = @"CITY_ID_KEY";
NSString *CITY_NAMES_KEY = @"CITY_NAMES_KEY";
NSString *CITY_REGIONS_KEY = @"CITY_REGIONS_KEY";

- (void)encodeWithCoder:(NSCoder *)aCoder {

  [aCoder encodeObject:self.cityId forKey:CITY_ID_KEY];
  [aCoder encodeObject:self.cityName forKey:CITY_NAMES_KEY];
  [aCoder encodeObject:self.regions forKey:CITY_REGIONS_KEY];
}

- (id)initWithCoder:(NSCoder *)aDecoder {
  self = [super init];
  if (!self) {
    return nil;
  }

  self.cityId = [aDecoder decodeObjectForKey:CITY_ID_KEY];
  self.cityName = [aDecoder decodeObjectForKey:CITY_NAMES_KEY];
  self.regions = [aDecoder decodeObjectForKey:CITY_REGIONS_KEY];

  return self;
}

@end
