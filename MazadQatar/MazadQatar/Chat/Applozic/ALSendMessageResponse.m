//
//  ALSendMessageResponse.m
//  Applozic
//
//  Created by Devashish on 06/11/15.
//  Copyright © 2015 applozic Inc. All rights reserved.
//

#import "ALSendMessageResponse.h"

@implementation ALSendMessageResponse

-(id)initWithJSONString:(NSString *)JSONString
{
//    [self parseMessage:JSONString];
    NSData *data = [JSONString dataUsingEncoding:NSUTF8StringEncoding];
    id json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
    [self parseMessage:json];
    return self;
}

-(void)parseMessage:(id) json;
{
    self.messageKey = [self getStringFromJsonValue:json[@"messageKey"]];
    self.createdAt = [self getNSNumberFromJsonValue:json[@"createdAt"]];
    self.conversationId =  [self getNSNumberFromJsonValue:json[@"conversationId"]];
    
}


//-(void)parseMessage:(id) json;
//{
//    NSLog(@"messageKey = %@", json[@"messageKey"]);
//    NSLog(@"createdAt = %@", json[@"createdAt"]);
//    NSLog(@"conversationId = %@", json[@"conversationId"]);
//    self.messageKey = [self getStringFromJsonValue:json[@"messageKey"]];
//    self.createdAt = [self getNSNumberFromJsonValue:json[@"createdAt"]];
//    self.conversationId =  [self getNSNumberFromJsonValue:json[@"conversationId"]];
//    
//}


-(BOOL)isSuccess{
    
    return (self.messageKey && self.createdAt);
}

@end
