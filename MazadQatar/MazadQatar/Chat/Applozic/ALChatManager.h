//
//  ALChatManager.h
//  applozicdemo
//
//  Created by Devashish on 28/12/15.
//  Copyright © 2015 applozic Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ALChatLauncher.h"
#import "ALUser.h"
#import "ALConversationService.h"
#import "ALRegisterUserClientService.h"
#import "MBProgressHUD.h"

#define APPLOZIC_APPLICATION_ID @"e909393819b58ab70e8fe2e4eca818e"
//#define APPLOZIC_APPLICATION_ID @"33475c00e17e32fb4e569acc1d8542cdd"
//#define APPLOZIC_APPLICATION_ID @"2b1efb3d68c5cc5e3033a7644e958b408"
//#define APPLOZIC_APPLICATION_ID @"ebdaadt1dbae4127e9ab14c0fb015abc158587c1"


@interface ALChatManager : NSObject

@property(nonatomic,strong) ALChatLauncher * chatLauncher;

@property(nonatomic,strong) NSArray * permissableVCList;

-(instancetype)initWithApplicationKey:(NSString *)applicationKey;

-(void)registerUser:(ALUser * )alUser;

-(void)registerUserWithCompletion:(ALUser *)alUser withHandler:(void(^)(ALRegistrationResponse *rResponse, NSError *error))completion;

//-(void)registerUserBlockMethod:(ALUser *)alUser completionHandler:(void(^)(BOOL success))block

@property (nonatomic,retain) NSString * userID;

-(void)launchChat: (UIViewController *)fromViewController;

-(void)ALDefaultChatViewSettings;

-(void)launchChatForUserWithDefaultText:(NSString * )userId andFromViewController:(UIViewController*)viewController;

-(void)registerUserAndLaunchChat:(ALUser *)alUser andFromController:(UIViewController*)viewController forUser:(NSString*)userId withGroupId:(NSNumber*)groupID;

-(void)launchChatForUserWithDisplayName:(NSString * )userId withGroupId:(NSNumber*)groupID andwithDisplayName:(NSString*)displayName andFromViewController:(UIViewController*)fromViewController;

-(void)createAndLaunchChatWithSellerWithConversationProxy:(ALConversationProxy*)alConversationProxy fromViewController:(UIViewController*)fromViewController andLoader:(MBProgressHUD*)hud;

-(void)launchListWithUserORGroup: (NSString *)userId ORWithGroupID: (NSNumber *)groupId andFromViewController:(UIViewController*)fromViewController;

-(void)launchOpenGroupWithKey:(NSNumber *)channelKey fromViewController:(UIViewController *)viewController;

-(BOOL)isUserHaveMessages:(NSString *)userId;

-(void) launchContactScreenWithMessage:(ALMessage *)alMessage andFromViewController:(UIViewController*)viewController;

-(NSString *)getApplicationKey;

-(void)launchChatListWithParentKey:(NSNumber *)parentGroupKey andFromViewController:(UIViewController *)viewController;

//TODO:MZAD
-(void)launchGroupOfTwoWithClientId:(NSString*)clientGroupId
                       withMetaData:(NSMutableDictionary*)metadata
                        andWithUser:(NSString *)userId
              andFromViewController:(UIViewController *)viewController
                          andLoader:(MBProgressHUD*)hud;

@end
