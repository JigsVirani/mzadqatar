//
//  ALContactCell.m
//  ChatApp
//
//  Copyright (c) 2015 AppLozic. All rights reserved.
//

#import "ALContactCell.h"
#import "SharedData.h"

@implementation ALContactCell

- (void)awakeFromNib {
    
    [super awakeFromNib];
    if([[SharedData getDeviceLanguage]isEqualToString:@"ar"]){
        self.arrowBtn.transform=CGAffineTransformMakeRotation(M_PI);
    }
    if ([UIScreen mainScreen].bounds.size.width >= 375 && [UIScreen mainScreen].bounds.size.width <= 375) {
        self.mUserNameLabel.font = [UIFont boldSystemFontOfSize:16.0];
        self.mTitleLabel.font = [UIFont systemFontOfSize:15.0];
        self.mMessageLabel.font = [UIFont fontWithName:@"Helvetica Neue" size:15.0];
        self.imageNameLabel.font = [UIFont systemFontOfSize:15.0];
        self.mTimeLabel.font = [UIFont fontWithName:@"Helvetica Neue" size:12.0];
    }
    else if ([UIScreen mainScreen].bounds.size.width >= 768) {
        self.mUserNameLabel.font = [UIFont boldSystemFontOfSize:22.0];
        self.mTitleLabel.font = [UIFont systemFontOfSize:20.0];
        self.mMessageLabel.font = [UIFont fontWithName:@"Helvetica Neue" size:20.0];
        self.imageNameLabel.font = [UIFont systemFontOfSize:20.0];
        self.mTimeLabel.font = [UIFont fontWithName:@"Helvetica Neue" size:16.0];
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
  
    [super setSelected:selected animated:animated];

}

@end
