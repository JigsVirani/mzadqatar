//
//  SelectImageDialogueViewController.h
//  Mzad Qatar
//
//  Created by Waris Ali on 29/12/2014.
//  Copyright (c) 2014 Mohammed abdulfattah attia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FDTakeController.h"
#import "SelectImageViewController.h"
#import "GAITrackedViewController.h"
@protocol SelectImageDelegate <NSObject>
- (void)SelectImageDelegatePhoto:(UIImage *)photo;
@end

@interface SelectImageDialogueViewController
    : GAITrackedViewController <UICollectionViewDelegate,
                        UIImagePickerControllerDelegate,
                        UINavigationControllerDelegate, FDTakeDelegate,
                        SelectImageViewControllerDelegate>

//@property (retain, nonatomic) IBOutlet UIImageView *upperImgBar;
@property(nonatomic, unsafe_unretained) id<SelectImageDelegate> delegate;
@property(strong, nonatomic) FDTakeController *takeController;
- (IBAction)cameraBtnClicked:(id)sender;
- (IBAction)galleryBtnClicked:(id)sender;
- (IBAction)createImageClicked:(id)sender;
- (IBAction)closeBtnClicked:(id)sender;

@end
