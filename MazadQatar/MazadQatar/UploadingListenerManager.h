//
//  UploadingListenerBroadcast.h
//  MazadQatar
//
//  Created by samo on 3/31/13.
//  Copyright (c) 2013 Mohammed abdulfattah attia. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UploadingProductsManager.h"

@interface UploadingListenerManager : NSObject <ProductsUploadingDelegate> {
  id<ProductsUploadingDelegate> __weak productUploadingDelegate;
  NSMutableArray *uploadingListners;
}

@property(weak) id<ProductsUploadingDelegate> productUploadingDelegate;
@property NSMutableArray *uploadingListners;

+ (UploadingListenerManager *)getManager;
- (void)registerUploadingListner:(id<ProductsUploadingDelegate>)delegate;
- (void)unRegisterUploadingListner:(id<ProductsUploadingDelegate>)delegate;
@end
