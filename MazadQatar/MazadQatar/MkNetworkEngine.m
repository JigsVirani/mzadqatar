//
//  FileUploadEngine.m
//  MazadQatar
//
//  Created by samo on 3/24/13.
//  Copyright (c) 2013 Mohammed abdulfattah attia. All rights reserved.
//

#import "MkNetworkEngine.h"

@implementation MkNetworkEngine

- (MKNetworkOperation *)postDataToServer:(NSDictionary *)params
                                    path:(NSString *)path {
  MKNetworkOperation *op =
      [self operationWithPath:path params:params httpMethod:@"POST" ssl:NO];

  return op;
}
@end
