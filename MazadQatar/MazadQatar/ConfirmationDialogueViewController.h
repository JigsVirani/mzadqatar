//
//  ConfirmationDialogueViewController.h
//  Mzad Qatar
//
//  Created by Waris Ali on 20/01/2015.
//  Copyright (c) 2015 Mohammed abdulfattah attia. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "GAITrackedViewController.h"
/// Delegates

@protocol ConfirmationDialogueViewControllerDelegate <NSObject>
- (void)confirmBtnClicked:(NSString *)requestId;
@optional
- (void)cancelBtnClicked:(NSString *)requestId;
@end

@interface ConfirmationDialogueViewController : GAITrackedViewController

@property(strong, nonatomic) IBOutlet UILabel *confirmationDescriptionLabel;
@property(strong, nonatomic) IBOutlet UILabel *firstBtnText;

- (IBAction)confirmBtnClicked:(id)sender;
- (IBAction)cancelBtnClicked:(id)sender;
@property(nonatomic, weak)
    id<ConfirmationDialogueViewControllerDelegate> delegate;
@property(nonatomic, weak) NSString *confirmButtonText;
@property(nonatomic, weak) NSString *confirmationDescriptionText;
@property(nonatomic, weak) NSString *requestId;

@end
