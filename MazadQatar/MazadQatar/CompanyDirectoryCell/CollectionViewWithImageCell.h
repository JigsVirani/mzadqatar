//
//  CollectionViewWithImageCell.h
//  Mzad Qatar
//
//  Created by Ashish Agrawal on 12/8/16.
//  Copyright © 2016 Mohammed abdulfattah attia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CollectionViewWithImageCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *adImage;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *adImageIndicator;
@property (weak, nonatomic) IBOutlet UILabel *lblAdsCount;
@property (weak, nonatomic) IBOutlet UILabel *lblAds;

@end
