//
//  CollectionViewWithImageCell.m
//  Mzad Qatar
//
//  Created by Ashish Agrawal on 12/8/16.
//  Copyright © 2016 Mohammed abdulfattah attia. All rights reserved.
//

#import "CollectionViewWithImageCell.h"

@implementation CollectionViewWithImageCell

- (void)awakeFromNib {
    
    [super awakeFromNib];    
    self.adImage.layer.cornerRadius = 8.0;
    self.adImage.layer.masksToBounds = true;
    // Initialization code
}

@end
