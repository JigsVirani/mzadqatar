//
//  ErrorManager.m
//  MazadQatar
//
//  Created by samo on 5/19/13.
//  Copyright (c) 2013 Mohammed abdulfattah attia. All rights reserved.
//

#import "DialogManager.h"
#import "AGPushNoteView.h"
#import "SelectAdvertiseLanguageViewController.h"
#import "ViewAdvertiseLanguageChooserViewController.h"

@implementation DialogManager

+ (void)showErrorInServer {
    [self showErrorDialogWithTitle:NSLocalizedString(@"ERROR", nil)
                    andDescription:NSLocalizedString(@"ERROR_IN_SERVER", nil)
                     andActionText:NSLocalizedString(@"DISMISS", nil)];
}
+ (void)showErrorInServerbyMessage:(NSString *)errorMsg {
    [self showErrorDialogWithTitle:NSLocalizedString(@"ERROR", nil)
                    andDescription:NSLocalizedString(errorMsg, nil)
                     andActionText:NSLocalizedString(@"DISMISS", nil)];
}
+ (void)showErrorRegisterAsCompany {
    [self showErrorDialogWithTitle:NSLocalizedString(@"NOTIFICATION_ALERT", nil)
                    andDescription:NSLocalizedString(@"REGISTER_AS_COMPANY", nil)
                     andActionText:@"COMPANY"];
}
+ (void)showErrorInDeletingAdvertise {
    [self showErrorDialogWithTitle:NSLocalizedString(@"ERROR", nil)
                    andDescription:NSLocalizedString(
                                                     @"ERROR_DELETEING_USER_PRODUCT", nil)
                     andActionText:NSLocalizedString(@"DISMISS", nil)];
}

+ (void)showErrorInRefreshingAdvertise {
    [self showErrorDialogWithTitle:NSLocalizedString(@"ERROR", nil)
                    andDescription:NSLocalizedString(
                                                     @"ERROR_REFRESHING_USER_PRODUCT", nil)
                     andActionText:NSLocalizedString(@"DISMISS", nil)];
}

+ (void)showErrorWithMessage:(NSString *)msg {
    [self showErrorDialogWithTitle:NSLocalizedString(@"ERROR", nil)
                    andDescription:msg
                     andActionText:NSLocalizedString(@"DISMISS", nil)];
}

+ (void)showDialogWithMessage:(NSString *)msg {
    [self showErrorDialogWithTitle:NSLocalizedString(@"NOTIFICATION_ALERT", nil)
                    andDescription:msg
                     andActionText:NSLocalizedString(@"DISMISS", nil)];
}

+ (void)showCopyConfirmation:(NSString *)msg {
    [self showErrorDialogWithTitle:NSLocalizedString(@"Copy", nil)
                    andDescription:msg
                     andActionText:NSLocalizedString(@"DISMISS", nil)];
}

+ (void)showErrorRegisterationNumberEmtpy {
    [self
     showErrorDialogWithTitle:NSLocalizedString(@"ERROR", nil)
     andDescription:NSLocalizedString(@"REGISTER_NUMBER_EMPTY", nil)
     andActionText:NSLocalizedString(@"DISMISS", nil)];
}

+ (void)showErrorCompanyNumberEmtpy {
    [self
     showErrorDialogWithTitle:NSLocalizedString(@"ERROR", nil)
     andDescription:NSLocalizedString(@"COMPANY_NUMBER_EMPTY", nil)
     andActionText:NSLocalizedString(@"DISMISS", nil)];
}
+ (void)showErrorCompanyNameEmtpy{
    [self
     showErrorDialogWithTitle:NSLocalizedString(@"ERROR", nil)
     andDescription:NSLocalizedString(@"COMPANY_NAME_EMPTY", nil)
     andActionText:NSLocalizedString(@"DISMISS", nil)];

}
+ (void)showErrorCompanyEmailEmtpy{
    [self
     showErrorDialogWithTitle:NSLocalizedString(@"ERROR", nil)
     andDescription:NSLocalizedString(@"COMPANY_EMAIL_EMPTY", nil)
     andActionText:NSLocalizedString(@"DISMISS", nil)];

}
+ (void)showErrorCompanyDetailsEmtpy{
    [self
     showErrorDialogWithTitle:NSLocalizedString(@"ERROR", nil)
     andDescription:NSLocalizedString(@"COMPANY_DETAILS_EMPTY", nil)
     andActionText:NSLocalizedString(@"DISMISS", nil)];

}

+ (void)showErrorCompanyEmailValidation{
    [self
     showErrorDialogWithTitle:NSLocalizedString(@"ERROR", nil)
     andDescription:NSLocalizedString(@"COMPANY_EMAIL_VALID", nil)
     andActionText:NSLocalizedString(@"DISMISS", nil)];
    
}



+ (void)showErrorActivationNumberEmtpy {
    [self showErrorDialogWithTitle:NSLocalizedString(@"ERROR", nil)
                    andDescription:NSLocalizedString(@"ACTIVATION_NUMBER_EMPTY",
                                                     nil)
                     andActionText:NSLocalizedString(@"DISMISS", nil)];
}

+ (void)showErrorProductImagesCount {
    [self showErrorDialogWithTitle:NSLocalizedString(@"ERROR", nil)
                    andDescription:NSLocalizedString(@"ERROR_IMAGES_COUNT", nil)
                     andActionText:NSLocalizedString(@"DISMISS", nil)];
}
+ (void)showErrorProductLanguage {
    [self showErrorDialogWithTitle:NSLocalizedString(@"ERROR", nil)
                    andDescription:NSLocalizedString(@"ERROR_PRODUCT_LANGUAGE", nil)
                     andActionText:NSLocalizedString(@"DISMISS", nil)];
}
+ (void)showErrorProductTitleEmpty {
    [self showErrorDialogWithTitle:NSLocalizedString(@"ERROR", nil)
                    andDescription:NSLocalizedString(@"ERROR_PRODUCT_TITLE_EMPTY",
                                                     nil)
                     andActionText:NSLocalizedString(@"DISMISS", nil)];
}

+ (void)showErrorProductTitleEnglishEmpty {
    [self showErrorDialogWithTitle:NSLocalizedString(@"ERROR", nil)
                    andDescription:NSLocalizedString(
                                                     @"ERROR_PRODUCT_TITLE_ENGLISH_EMPTY", nil)
                     andActionText:NSLocalizedString(@"DISMISS", nil)];
}

+ (void)showErrorProductTitleArabicEmpty {
    [self showErrorDialogWithTitle:NSLocalizedString(@"ERROR", nil)
                    andDescription:NSLocalizedString(
                                                     @"ERROR_PRODUCT_TITLE_ARABIC_EMPTY", nil)
                     andActionText:NSLocalizedString(@"DISMISS", nil)];
}

+ (void)showErrorProductPriceEmpty {
    [self showErrorDialogWithTitle:NSLocalizedString(@"ERROR", nil)
                    andDescription:NSLocalizedString(@"ERROR_PRODUCT_PRICE_EMPTY",
                                                     nil)
                     andActionText:NSLocalizedString(@"DISMISS", nil)];
}

+ (void)showErrorProductDescriptionEmpty {
    [self showErrorDialogWithTitle:NSLocalizedString(@"ERROR", nil)
                    andDescription:NSLocalizedString(
                                                     @"ERROR_PRODUCT_DESCRIPTION_EMPTY", nil)
                     andActionText:NSLocalizedString(@"DISMISS", nil)];
}

+ (void)showErrorProductDescriptionEnglishEmpty {
    [self showErrorDialogWithTitle:NSLocalizedString(@"ERROR", nil)
                    andDescription:NSLocalizedString(
                                                     @"ERROR_PRODUCT_DESCRIPTION_ENGLISH_EMPTY",
                                                     nil)
                     andActionText:NSLocalizedString(@"DISMISS", nil)];
}

+ (void)showErrorProductDescriptionArabicEmpty {
    [self showErrorDialogWithTitle:NSLocalizedString(@"ERROR", nil)
                    andDescription:NSLocalizedString(
                                                     @"ERROR_PRODUCT_DESCRIPTION_ARABIC_EMPTY",
                                                     nil)
                     andActionText:NSLocalizedString(@"DISMISS", nil)];
}

+ (void)showErrorCategoryNotChosen {
    [self showErrorDialogWithTitle:NSLocalizedString(@"ERROR", nil)
                    andDescription:NSLocalizedString(
                                                     @"ERROR_CATEGORY_NOT_CHOOSESN", nil)
                     andActionText:NSLocalizedString(@"DISMISS", nil)];
}

+ (void)showErrorSubCategoryNotChosen {
    [self showErrorDialogWithTitle:NSLocalizedString(@"ERROR", nil)
                    andDescription:NSLocalizedString(
                                                     @"ERROR_SUBCATEGORY_NOT_CHOOSESN", nil)
                     andActionText:NSLocalizedString(@"DISMISS", nil)];
}

+ (void)showErrorRegionChoosedBeforeCities {
    [self showErrorDialogWithTitle:NSLocalizedString(@"ERROR", nil)
                    andDescription:NSLocalizedString(
                                                     @"ERROR_REGION_CHOOSED_BEFORE_CITIES", nil)
                     andActionText:NSLocalizedString(@"DISMISS", nil)];
}

+ (void)showErrorAdvertiseTypeNotChoosen {
    [self showErrorDialogWithTitle:NSLocalizedString(@"ERROR", nil)
                    andDescription:NSLocalizedString(
                                                     @"ERROR_ADVERTISE_TYPE_NOT_CHOOSEN", nil)
                     andActionText:NSLocalizedString(@"DISMISS", nil)];
}

+ (void)showErrorCitiesNotChosen {
    [self
     showErrorDialogWithTitle:NSLocalizedString(@"ERROR", nil)
     andDescription:NSLocalizedString(@"ERROR_CITY_NOT_CHOOSEN", nil)
     andActionText:NSLocalizedString(@"DISMISS", nil)];
}

+ (void)showErrorRegionNotChosen {
    [self showErrorDialogWithTitle:NSLocalizedString(@"ERROR", nil)
                    andDescription:NSLocalizedString(@"ERROR_REGION_NOT_CHOOSESN",
                                                     nil)
                     andActionText:NSLocalizedString(@"DISMISS", nil)];
}

+ (void)showErrorSubSubCagegoryNotChosen {
    [self showErrorDialogWithTitle:NSLocalizedString(@"ERROR", nil)
                    andDescription:NSLocalizedString(
                                                     @"ERROR_SUBSUBCATEGORY_NOT_CHOOSESN", nil)
                     andActionText:NSLocalizedString(@"DISMISS", nil)];
    
}

+ (void)showErrorManfactureYearNotChosen {
    [self showErrorDialogWithTitle:NSLocalizedString(@"ERROR", nil)
                    andDescription:NSLocalizedString(
                                                     @"ERROR_MANFACTURE_YEAR_NOT_CHOOSESN", nil)
                     andActionText:NSLocalizedString(@"DISMISS", nil)];
}

+ (void)showErrorKmNotChosen {
    [self
     showErrorDialogWithTitle:NSLocalizedString(@"ERROR", nil)
     andDescription:NSLocalizedString(@"ERROR_KM_NOT_CHOOSESN", nil)
     andActionText:NSLocalizedString(@"DISMISS", nil)];
}
+ (void)showErrorFurnishedTypeNotChosen {
    [self showErrorDialogWithTitle:NSLocalizedString(@"ERROR", nil)
                    andDescription:NSLocalizedString(
                                                     @"ERROR_FURNISHED_TYPE_NOT_CHOOSESN", nil)
                     andActionText:NSLocalizedString(@"DISMISS", nil)];
}

+ (void)showErrorNumberOfRoomsNotChosen {
    [self showErrorDialogWithTitle:NSLocalizedString(@"ERROR", nil)
                    andDescription:NSLocalizedString(
                                                     @"ERROR_NUMBER_OF_ROOMS_NOT_CHOOSESN", nil)
                     andActionText:NSLocalizedString(@"DISMISS", nil)];
}

+ (void)showErrorCompleteAllFieldsInCategoryFilters {
    [self showErrorDialogWithTitle:NSLocalizedString(@"ERROR", nil)
                    andDescription:
     NSLocalizedString(
                       @"ERROR_COMPLETE_ALL_FIELDS_IN_CATEGORY_FILTERS", nil)
                     andActionText:NSLocalizedString(@"DISMISS", nil)];
}

+ (void)showErrorUserCommentNameEmpty {
    [self showErrorDialogWithTitle:NSLocalizedString(@"ERROR", nil)
                    andDescription:NSLocalizedString(
                                                     @"ERROR_USER_COMMENT_NAME_EMPTY", nil)
                     andActionText:NSLocalizedString(@"DISMISS", nil)];
}

+ (void)showErrorUpdateUserNameEmpty {
    [self
     showErrorDialogWithTitle:NSLocalizedString(@"ERROR", nil)
     andDescription:NSLocalizedString(@"ERROR_UPDATE_USER_NAME", nil)
     andActionText:NSLocalizedString(@"DISMISS", nil)];
}
+ (void)showErrorUpdateUserLocationEmpty {
    [self
     showErrorDialogWithTitle:NSLocalizedString(@"ERROR", nil)
     andDescription:NSLocalizedString(@"ERROR_UPDATE_USER_LOCATION", nil)
     andActionText:NSLocalizedString(@"DISMISS", nil)];
}
+ (void)showErrorUserCommentEmpty {
    [self showErrorDialogWithTitle:NSLocalizedString(@"ERROR", nil)
                    andDescription:NSLocalizedString(@"ERROR_USER_COMMENT_EMPTY",
                                                     nil)
                     andActionText:NSLocalizedString(@"DISMISS", nil)];
}

+ (void)showErrorProductNotFound {
    [self
     showErrorDialogWithTitle:NSLocalizedString(@"ERROR", nil)
     andDescription:NSLocalizedString(@"PRODUCT_NOT_AVAILABLE", nil)
     andActionText:NSLocalizedString(@"DISMISS", nil)];
}

+ (void)showErrorMaxImagesLimit {
    [self
     showErrorDialogWithTitle:NSLocalizedString(@"ERROR", nil)
     andDescription:NSLocalizedString(@"ERROR_MAX_IMAGES_LIMIT", nil)
     andActionText:NSLocalizedString(@"DISMISS", nil)];
}

+ (void)showErrorImageSizeLimit {
    [self showErrorDialogWithTitle:NSLocalizedString(@"ERROR", nil)
                    andDescription:NSLocalizedString(@"MAX_IMAGE_SIZE_LIMIT", nil)
                     andActionText:NSLocalizedString(@"DISMISS", nil)];
}

+ (void)showErrorActivationKeyNotCorrect {
    [self showErrorDialogWithTitle:NSLocalizedString(@"ERROR", nil)
                    andDescription:NSLocalizedString(
                                                     @"ACTIVATION_KEY_NOT_CORRECT", nil)
                     andActionText:NSLocalizedString(@"DISMISS", nil)];
}

+ (void)showSuccessActivationKeySent {
    [self showErrorDialogWithTitle:NSLocalizedString(@"SUCESS", nil)
                    andDescription:NSLocalizedString(
                                                     @"ACTIVATION_KEY_SENT_SUCCESS", nil)
                     andActionText:NSLocalizedString(@"DISMISS", nil)];
}
+ (void)showSuccessMessage:(NSString*) message {
    
    [self showErrorDialogWithTitle:NSLocalizedString(@"SUCESS", nil)
                    andDescription:message
                     andActionText:NSLocalizedString(@"DISMISS", nil)];
}
+ (void)showErrorSendingActivationKey {
    [self showErrorDialogWithTitle:NSLocalizedString(@"ERROR", nil)
                    andDescription:NSLocalizedString(@"ACTIVATION_KEY_SENT_FAIL",
                                                     nil)
                     andActionText:NSLocalizedString(@"DISMISS", nil)];
}

+ (void)showErrorInConnection {
    [self showErrorDialogWithTitle:NSLocalizedString(@"ERROR", nil)
                    andDescription:NSLocalizedString(@"ERROR_IN_CONNECTION", nil)
                     andActionText:NSLocalizedString(@"DISMISS", nil)];
}

+ (void)showErrorUserIsBlocked {
    [self
     showErrorDialogWithTitle:NSLocalizedString(@"ERROR", nil)
     andDescription:NSLocalizedString(@"ERROR_USER_IS_BLOCKED_FROM_ADMIN", nil)
     andActionText:NSLocalizedString(@"DISMISS", nil)];
}

+ (void)showErrorFeedbackEmpty {
    [self showErrorDialogWithTitle:NSLocalizedString(@"ERROR", nil)
                    andDescription:NSLocalizedString(@"ERROR_FEEDBACK_EMPTY", nil)
                     andActionText:NSLocalizedString(@"DISMISS", nil)];
}

+ (void)showErrorResendSmSLimit {
    [self showErrorDialogWithTitle:NSLocalizedString(@"ERROR", nil)
                    andDescription:NSLocalizedString(@"ERROR_RESEND_LIMIT", nil)
                     andActionText:NSLocalizedString(@"DISMISS", nil)];
}

+ (void)showPressEditAdvertiseDialog {
    [self showErrorDialogWithTitle:NSLocalizedString(@"NOTIFICATION_ALERT", nil)
                    andDescription:NSLocalizedString(
                                                     @"SHOW_PRESS_EDIT_ADVERTISE_ALERT", nil)
                     andActionText:NSLocalizedString(@"OK", nil)];
}
+ (void)showPressEditInfoDialog {
    [self showErrorDialogWithTitle:NSLocalizedString(@"NOTIFICATION_ALERT", nil)
                    andDescription:NSLocalizedString(
                                                     @"SHOW_PRESS_EDIT_INFO_ALERT", nil)
                     andActionText:NSLocalizedString(@"OK", nil)];
}

+ (void)showChooseCategoryFirst {
    [self
     showErrorDialogWithTitle:NSLocalizedString(@"NOTIFICATION_ALERT", nil)
     andDescription:NSLocalizedString(@"CHOOSE_CATEGORY_FIRST", nil)
     andActionText:NSLocalizedString(@"DISMISS", nil)];
}

+ (void)showErrorNoEmailAccountAvailable {
    [self
     showErrorDialogWithTitle:NSLocalizedString(@"ERROR", nil)
     andDescription:NSLocalizedString(@"EMAIL_ACCOUNT_NOT_SET", nil)
     andActionText:NSLocalizedString(@"DISMISS", nil)];
}

+ (void)showUserFilterViewAdvertiseLanguageDialog {
    UIViewController *topController =
    [UIApplication sharedApplication].keyWindow.rootViewController;
    ErrorDialogueViewController *controller = [[SharedData getCurrentStoryBoard]
                                               instantiateViewControllerWithIdentifier:
                                               @"SelectAdvertiseLanguageViewController"];
    
    [topController.view addSubview:controller.view];
    
    [topController addChildViewController:controller];
}
+ (void)showErrorDialogWithTitle:(NSString *)title
                  andDescription:(NSString *)descrition
                   andActionText:(NSString *)actionText {
    UIViewController *topController =
    [UIApplication sharedApplication].keyWindow.rootViewController;
    ErrorDialogueViewController *controller = [[SharedData getCurrentStoryBoard]
                                               instantiateViewControllerWithIdentifier:@"ErrorViewController"];
    
    [topController.view addSubview:controller.view];
    
    [topController addChildViewController:controller];
    
    [controller setDialogTitle:title
                 andDescrition:descrition
                 andActionText:actionText];
}

+ (void)showShareDialogue {
    UIViewController *topController =
    [UIApplication sharedApplication].keyWindow.rootViewController;
    ShareAppViewController *controller = [[SharedData getCurrentStoryBoard]
                                          instantiateViewControllerWithIdentifier:@"ShareAppViewController"];
    
    if([topController.view.subviews containsObject:controller.view ]==false)
    {
        [topController.view addSubview:controller.view];
        [topController addChildViewController:controller];
    }
}
+ (void)showLocationDialogue:(NSString *)strLocationName andDelegate:(id<LocationDialogueDelegate>)delegate andRequestId:(NSString *)requestId {

    UIViewController *topController =[UIApplication sharedApplication].keyWindow.rootViewController;
    LocationDialogue *controller = [[SharedData getCurrentStoryBoard]instantiateViewControllerWithIdentifier:@"LocationDialogue"];
    
    controller.strLocationNameText = strLocationName;
    controller.delegate = delegate;
    controller.requestId = requestId;
    
    if([topController.view.subviews containsObject:controller.view ]==false)
    {
        [topController.view addSubview:controller.view];
        [topController addChildViewController:controller];
    }
}

+ (void)showMapDialogue:(NSString *)strLocationName andDelegate:(id<LocationDialogueDelegate>)delegate andRequestId:(NSString *)requestId {
    
    UIViewController *topController =[UIApplication sharedApplication].keyWindow.rootViewController;
    LocationDialogue *controller = [[SharedData getCurrentStoryBoard]instantiateViewControllerWithIdentifier:@"MapDialogue"];
    
    controller.strLocationNameText = strLocationName;
    controller.delegate = delegate;
    controller.requestId = requestId;
    
    if([topController.view.subviews containsObject:controller.view ]==false)
    {
        [topController.view addSubview:controller.view];
        [topController addChildViewController:controller];
    }
}
+ (void)showRateDialogue {
    UIViewController *topController =
    [UIApplication sharedApplication].keyWindow.rootViewController;
    RateViewController *controller = [[SharedData getCurrentStoryBoard]
                                      instantiateViewControllerWithIdentifier:@"RateViewController"];
    
    if([topController.view.subviews containsObject:controller.view ]==false)
    {
        [topController.view addSubview:controller.view];
        [topController addChildViewController:controller];
    }
}
+(void) makeToastOnly:(NSString *)toastMessage
{

}
+(void) makeToastWithString:(NSString *)toastMessage
{
    UIViewController *topController =
    [UIApplication sharedApplication].keyWindow.rootViewController;
    [topController.view makeToast:toastMessage];
}

+(void) toastWithImageAndText:(NSString *)toastMessage 
{
    UIViewController *topController =
    [UIApplication sharedApplication].keyWindow.rootViewController;
    [topController.view makeToast:toastMessage
                                     duration:2.0
                                     position:CSToastPositionCenter
                                        title:nil
                                        image:[UIImage imageNamed:@"Activate-Icon.png"]
                                        style:nil
                                   completion:nil];
    
}



+ (void)showConfirmationDialogueWithDescription:(NSString *)descriptionText
andConfirmBtnText:(NSString *)confirmBtnText
andDelegate:
(id<ConfirmationDialogueViewControllerDelegate>)
delegate
andRequestId:(NSString *)requestId {
    UIViewController *topController =
    [UIApplication sharedApplication].keyWindow.rootViewController;
    
    ConfirmationDialogueViewController *controller =
    [[SharedData getCurrentStoryBoard]
     instantiateViewControllerWithIdentifier:
     @"ConfirmationDialogueViewController"];
    
    controller.delegate = delegate;
    controller.confirmationDescriptionText = descriptionText;
    controller.confirmButtonText = confirmBtnText;
    controller.requestId = requestId;
    
    [topController.view addSubview:controller.view];
    [topController addChildViewController:controller];
}

@end
