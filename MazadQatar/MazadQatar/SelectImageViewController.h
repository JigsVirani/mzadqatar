//
//  SelectImageViewController.h
//  Mzad Qatar
//
//  Created by Perveen Akhtar on 07/08/2014.
//  Copyright (c) 2014 Mohammed abdulfattah attia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ServerManager.h"
#import "ServerManagerResult.h"
#import "GridView.h"
#import "NotificationViewController.h"
#import "SelectImageModal.h"
#import "FDTakeController.h"
#import "EnterTextViewController.h"
#import "GAITrackedViewController.h"

/// Delegate
@protocol SelectImageViewControllerDelegate <NSObject>
@required
- (void)SelectImageViewControllerDelegateSelected:(UIImage *)image;
@end

#define requestGetImages @"requestGetImages"

@interface SelectImageViewController
    : GAITrackedViewController <ServerManagerResponseDelegate,
                        UICollectionViewDataSource, UICollectionViewDelegate,
                        EnterTextViewControllerDelegate> {
  int selectedImageIndex;
  UIImage *currentImageSelected;
}

@property(nonatomic, strong) NSMutableArray *photos;
@property(strong, nonatomic) IBOutlet UICollectionView *collectionViewGridView;
- (IBAction)doneButtonPressed:(id)sender;
@property(nonatomic, unsafe_unretained)
    id<SelectImageViewControllerDelegate> delegate;

@end
