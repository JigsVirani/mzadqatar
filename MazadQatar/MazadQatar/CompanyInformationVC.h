//
//  WHYViewController.h
//  Mzad Qatar
//
//  Created by GuruUgam on 7/28/16.
//  Copyright © 2016 Mohammed abdulfattah attia. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "GAITrackedViewController.h"


@interface CompanyInformationVC : GAITrackedViewController<UITableViewDelegate,UITableViewDataSource>

@end
