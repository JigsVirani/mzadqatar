//
//  TextViewTableCell.h
//  MazadQatar
//
//  Created by samo on 4/21/13.
//  Copyright (c) 2013 Mohammed abdulfattah attia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TextViewTableCell : UITableViewCell <UITextViewDelegate>

{
    
  UIViewController *parentView;

  bool isRotating;

  CGFloat animatedDistance;
    
    UIView *inputAccView;
    
    UIButton *btnDone;
}

@property(nonatomic)         UIEdgeInsets                 currentContentInset;   
- (void)createInputAccessoryView ;
@end
