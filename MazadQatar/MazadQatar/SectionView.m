//
//  SectionView.m
//  RevealView
//
//  Created by Waris Ali on 03/06/2014.
//  Copyright (c) 2014 Waris Ali. All rights reserved.
//

#import "SectionView.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface SectionView ()
@property BOOL isCollapsed;
@property BOOL isAnimating;
@property(nonatomic, strong) UIImageView *tmpImageView;
@end

@implementation SectionView

- (id)initWithFrame:(CGRect)frame
              label:(NSString *)label
                tag:(NSInteger)tag
          collapsed:(BOOL)isCollapsed
           imageURL:(NSString *)imageURL {

  self = [super initWithFrame:frame];
  if (self) {
    // Attributes
    _isCollapsed = isCollapsed;
    _isAnimating = NO;

    // Self
    self.tag = tag;
    self.backgroundColor = COLOR_SECTIONVIEW;
    UITapGestureRecognizer *tapGesture =
        [[UITapGestureRecognizer alloc] initWithTarget:self
                                                action:@selector(viewTapped)];
    [self addGestureRecognizer:tapGesture];

    // Adding separator line
    UIView *separatorLine = [[UIView alloc]
        initWithFrame:CGRectMake(0, frame.size.height - 0.5, frame.size.width,
                                 0.5)];
    separatorLine.backgroundColor = COLOR_SEPARATOR;
    [self addSubview:separatorLine];

    // Adding imageview for dropdown
    _tmpImageView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"category_droparrow"]];

    //         _tmpImageView.frame = CGRectMake(242, 20, 8, 15);
    _tmpImageView.frame = CGRectMake(242, 18, 8, 15);
    [self addSubview:_tmpImageView];
    if (!isCollapsed)
      _tmpImageView.transform =
          CGAffineTransformRotate(_tmpImageView.transform, M_PI / 2);

    // Customizing Label
    UILabel *tmpLabel = [[UILabel alloc] initWithFrame:CGRectMake(55, 2, 200, 40)];
    tmpLabel.text = label;
    tmpLabel.textColor = COLOR_WHITECOLOR;
    tmpLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:16.0];

    UIActivityIndicatorView *subcategoryImageLoadingIndicator;
    if (imageURL != nil) {
      subcategoryImageLoadingIndicator = [[UIActivityIndicatorView alloc]
          initWithActivityIndicatorStyle:
              UIActivityIndicatorViewStyleWhite];
      subcategoryImageLoadingIndicator.frame = CGRectMake(7, 7, 30, 30);
      [self addSubview:subcategoryImageLoadingIndicator];
      [subcategoryImageLoadingIndicator startAnimating];
    }
    _sectionView = [[UIImageView alloc]
        initWithFrame:CGRectMake(15, 14, 28, 18)];


      [_sectionView
       sd_setImageWithURL:[NSURL
                           URLWithString:imageURL]
       placeholderImage:nil
       completed:nil];

      
    [self addSubview:_sectionView];

    [self addSubview:tmpLabel];
  }
  return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

#pragma mark - Custom Functions

- (void)viewTapped {
  if (_isAnimating) {
    return;
  } else {
    _isAnimating = YES;
    [UIView animateWithDuration:0.3
        animations:^{
          if (_isCollapsed) {
            _tmpImageView.transform =
                CGAffineTransformRotate(_tmpImageView.transform, M_PI / 2);
            _isCollapsed = NO;

          } else {
            _tmpImageView.transform =
                CGAffineTransformRotate(_tmpImageView.transform, -M_PI / 2);
            _isCollapsed = YES;
          }
        }
        completion:^(BOOL finished) {
          _isAnimating = NO;
        }];
    [self.delegate sectionTapped:(int)self.tag];
  }
}

@end
