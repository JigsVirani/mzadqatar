//
//  AddPropertyFiltersViewController.h
//  Mzad Qatar
//
//  Created by samo on 9/18/14.
//  Copyright (c) 2014 Mohammed abdulfattah attia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TextFieldViewController.h"
#import "CitiesViewController.h"
#import "SubCategoryChooser.h"
#import "SubSubCategoriesChooser.h"
#import "DialogManager.h"
#import "TextFieldViewController.h"
#import "AdvertiseTypeChooser.h"
#import "RegionListChooser.h"
#import "FurnishedTypeChooserViewController.h"

// Delgate
@class AddPropertyFiltersViewController;
@protocol AddPropertyFiltersViewControllerDelgate <NSObject>
@required
- (void)propertyFiltersChoosedWithProductObject:
        (ProductObject *)productObjectParam;
@required
@end

@interface AddPropertyFiltersViewController
    : TextFieldViewController <
          SubCategoryChooserDelegate, CitiesViewControllerDelegate,
          AdvertiseTypeChooserDelegate, RegionViewControllerDelegate,
          FurnishedTypeChooserDelegate> {
  // NSString *currentCategoryId;
  // NSString *currentSubCategoryId;
  // int currentSubCategoryIndex;
  // NSString *currentCityIdSelected;
  // int currentCityIndexSelected;
  // int currentAdvertiseTypeIndexSelected;
  // NSString* currentAdvertiseTypeIdSelected;
  // int currentIsFurnishedIndexSelected;
  // NSString* currentIsFurnishedeIdSelected;
  // NSString *currentRegionId;
  // int currentRegionIndexSelected;
}

@property(weak) ProductObject *productObject;

@property(strong, nonatomic) IBOutlet UIButton *chooseAdvertiseTypeBtn;
@property(strong, nonatomic) IBOutlet UIButton *chooseCityBtn;
@property(strong, nonatomic) IBOutlet UIButton *chooseRegionBtn;
@property(strong, nonatomic) IBOutlet UIButton *chooseSubcategoryBtn;
@property(strong, nonatomic) IBOutlet UIButton *isFurnishedBtn;
@property(strong, nonatomic) IBOutlet UITextView *numberOfRoomsTextField;

@property(strong, nonatomic) IBOutlet UIImageView *chooseAdvertiseTypeImg;
@property(strong, nonatomic) IBOutlet UIImageView *chooseCityImg;
@property(strong, nonatomic) IBOutlet UIImageView *chooseRegionImg;
@property(strong, nonatomic) IBOutlet UIImageView *chooseSubcategoryImg;
@property(strong, nonatomic) IBOutlet UIImageView *isFurnishedImg;

@property(nonatomic, strong)
    id<AddPropertyFiltersViewControllerDelgate> delegate;

- (IBAction)doneBtnCLicked:(id)sender;
- (bool)validatePropertyFilters;
@end
