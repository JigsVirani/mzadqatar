//
//  CompanyTableViewCell.m
//  Mzad Qatar
//
//  Created by AHMED HEGAB on 7/24/16.
//  Copyright © 2016 Mohammed abdulfattah attia. All rights reserved.
//

#import "CompanyTableViewCell.h"
#import "CollectionViewWithImageCell.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "DisplayUtility.h"

@implementation CompanyTableViewCell

- (void)awakeFromNib {
    
    [super awakeFromNib];
    
    self.collView.dataSource = self;
    self.lbl.layer.cornerRadius = 5.0;
    self.lbl.layer.masksToBounds = true;
    // Initialization code
    
    if ([DisplayUtility isPad]) {
        [_companyTitle setFont:[UIFont systemFontOfSize:20.0]];
         [_companyDetails setFont:[UIFont systemFontOfSize:18.0]];
        [_lbl setFont:[UIFont systemFontOfSize:18.0]];
        [_adsCount setFont:[UIFont systemFontOfSize:16.0]];
        [_viewsCount setFont:[UIFont systemFontOfSize:16.0]];
        [_adLbl setFont:[UIFont systemFontOfSize:16.0]];
        [_viewLbl setFont:[UIFont systemFontOfSize:16.0]];
    }
}

- (id)initWithStyle:(UITableViewCellStyle)style
    reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        self.backgroundView = [[UIImageView alloc] initWithImage:[ [UIImage imageNamed:@"Comp_BG.png"] stretchableImageWithLeftCapWidth:0.0 topCapHeight:5.0] ];

    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

#pragma mark- UICollectionViewDataSource

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    if(self.productObject.productImagesUrls != nil){
        NSInteger len = self.productObject.productImagesUrls.count;
        
        if (len > 3) {
            return 4;
        }
        
        if (len < self.productObject.productCompanyNumberOfAds.integerValue) {
            len += 1;
        }
        
        return len;
    }
    return 0;
}



- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    

    CollectionViewWithImageCell* cell = [collectionView  dequeueReusableCellWithReuseIdentifier:@"CollectionViewWithImageCell" forIndexPath:indexPath];
    
    
    NSInteger len = self.productObject.productImagesUrls.count;
    
    if (indexPath.row == 3) {
        [self setNumberData:cell];
        cell.lblAdsCount.text = [NSString stringWithFormat:@"%lu +",self.productObject.productCompanyNumberOfAds.integerValue];
    } else if (indexPath.row >= len ) {
        [self setNumberData:cell];
        cell.lblAdsCount.text = [NSString stringWithFormat:@"%lu +",self.productObject.productCompanyNumberOfAds.integerValue];
    } else {
        cell.lblAdsCount.hidden = true;
        cell.lblAds.hidden = true;
        cell.adImage.backgroundColor = [UIColor whiteColor];
        
        if(self.productObject.productImagesUrls != nil){
            if (self.productObject.productImagesUrls[indexPath.row] != nil) {
                if ([self.productObject.productImagesUrls[indexPath.row] isKindOfClass:[NSString class]]) {
                    
                    cell.adImageIndicator.hidden = false;
                    
                    [cell.adImageIndicator startAnimating];
                    [cell.adImage sd_setImageWithURL:[NSURL URLWithString:self.productObject.productImagesUrls[indexPath.row]] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                        [cell.adImageIndicator stopAnimating];
                    }];
                } else {
                    [cell.adImageIndicator stopAnimating];
                }
            } else {
                [cell.adImageIndicator stopAnimating];
            }
        } else {
            [cell.adImageIndicator stopAnimating];
        }
    }
    
    return cell;
}

-(void)setNumberData:(CollectionViewWithImageCell *)cell {
    cell.adImageIndicator.hidden = true;
    cell.lblAdsCount.hidden = false;
    cell.lblAds.hidden = false;
    cell.adImage.backgroundColor = [UIColor colorWithRed:159.0/255.0 green:18.0/255.0 blue:87.0/255.0 alpha:1.0];
    cell.adImage.image = nil;
}



@end
