//
//  TableViewCell.m
//  Mzad Qatar
//
//  Created by Mohammed AbdulFattah on 1/11/15.
//  Copyright (c) 2015 Mohammed abdulfattah attia. All rights reserved.
//
#import "OtherReasonTableViewCell.h"
#import "DialogManager.h"

@implementation OtherReasonTableViewCell

NSString *reasonLastId = @"6";

- (void)awakeFromNib {
  // Initialization code
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    if (selected == true) {
        [self.otherTextField becomeFirstResponder];
    }
    
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

- (IBAction)submitReasonClicked:(id)sender {
    if( [self.otherTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length==0)
    {
        [self.otherTextField endEditing:YES];
        [DialogManager showErrorWithMessage:NSLocalizedString(@"ERROR_REPORT_TEXT_EMPTY", nil)];
        return;
    }
    [self.delegate submitOtherReasonClicked:reasonLastId
                              andReasonText:self.otherTextField.text];
}



- (void)textViewDidBeginEditing:(UITextView *)textView {
  [self setAccessoryType:UITableViewCellAccessoryCheckmark];
  self.submitOtherReasonButton.hidden = NO;
}
@end
