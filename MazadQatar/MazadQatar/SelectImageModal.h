//
//  SelectImageModal.h
//  Mzad Qatar
//
//  Created by Waris Ali on 19/12/2014.
//  Copyright (c) 2014 Mohammed abdulfattah attia. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SelectImageModal : NSObject

@property(strong) NSString *imageId;
@property(strong) NSString *imageUrl;
@property(strong) UIImage *image;
@property(strong) NSString *imageCharacterLimit;
@property(strong) NSString *imageTextColor;
@property(strong) NSString *imageXPosition;
@property(strong) NSString *imageYPosition;
@property(strong) NSString *imageTextSize;

@end
