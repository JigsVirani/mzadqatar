//
//  CategoryChooser.h
//  MazadQatar
//
//  Created by samo on 5/13/13.
//  Copyright (c) 2013 Mohammed abdulfattah attia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SharedData.h"
#import "ProductObject.h"
#import "ServerManager.h"
#import "FileManager.h"
#import "CategoryObject.h"
#import "MBProgressHUD.h"
#import "ChooseListWithSearch.h"

// Delgate
@class SubCategoryChooser;
@protocol SubCategoryChooserDelegate <NSObject>
@required
- (void)subCategoryChoosedInList:(NSString *)subCategoryId
              andSubCategoryName:(NSString *)subCategorName;
@required
@end

@interface SubCategoryChooser
    : ChooseListWithSearch <UITableViewDataSource, UITableViewDelegate> {
  //    BOOL isOverlayShown;
  //    UIView *overlayView;
  //    UIActivityIndicatorView *activityIndicator;
  NSMutableArray *productItems;
  NSString *currentCategoryId;
  bool isOpenedFromAddAdvertise;
  NSString *currentSelectedSubCategoryId;
  id<SubCategoryChooserDelegate> delegate;
}

- (void)setSubCategoryParamsWithCatId:(NSString *)categoryId
              andCurrentSubCategoryId:(NSString *)currentSelectedsubCategoryID
                     andIsOpenedFromAddAdvertise:(BOOL)isOpenedFromAddAdvertiseParam
                          andDelegate:
                              (id<SubCategoryChooserDelegate>)delegateParam;

//@property (retain, nonatomic) IBOutlet UITableView *subCategoryTableView;

@end
