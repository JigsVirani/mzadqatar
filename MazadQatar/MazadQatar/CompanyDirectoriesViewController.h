//
//  CompanyDirectoriesViewController.h
//  Mzad Qatar
//
//  Created by AHMED HEGAB on 11/7/16.
//  Copyright © 2016 Mohammed abdulfattah attia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GridView.h"
#import "MBProgressHUD.h"
#import "AppDelegate.h"
#import "CompanyTableViewCell.h"
#import "CategoryTypesBarCell.h"
#import "CompanyGridCell.h"
#import "SearchViewController.h"

#define IS_IPHONE5                                                             \
(([[UIScreen mainScreen] bounds].size.height - 568) ? NO : YES)

#define requestInitialCompanies @"requestInitialCompanies"
#define requestMoreCompanies @"requestMoreCompanies"
#define requestRefreshCompanies @"requestRefreshCompanies"
#define requestPullNewDataCategoryProducts @"requestPullNewDataCategoryProducts"

@interface CompanyDirectoriesViewController : GridView <ServerManagerResponseDelegate, UIGestureRecognizerDelegate,
  CategoryTypesBarCellDelegate> {
      
      NSString *selectedSubCategoryId;
}



@property(nonatomic, assign) int currentCategoryTypeBarIndex;

@property(nonatomic) BOOL adjustsFontSizeToFitWidth;
@property(nonatomic, strong) NSMutableArray *tabBarNames;
@property(weak) NSString *productsLanguage;
@property(strong, nonatomic) IBOutlet UICollectionView *companyCollectionView;
@property (weak, nonatomic) IBOutlet UISearchBar *customSearchBar;
@property (weak, nonatomic) IBOutlet UIView *parentViewSearchBar;
@property (weak, nonatomic) IBOutlet UILabel *lblPlaceHlder;
@property(strong,nonatomic) NSString *productId;
@property(strong,nonatomic) NSString *searchTxt;
@property (weak, nonatomic) IBOutlet UIView *search_View;
@property (weak,nonatomic)IBOutlet NSLayoutConstraint *viewConstraints;
@property BOOL search;




// IBActions
- (IBAction)btnchangeView:(UIButton *)sender;

/// Methods
- (void)selectedTabFromSubMenu:(NSString *)strCategoryID withTabID:(NSString *)strTabID;
- (void)setCategoryProductsCategoryId:(NSString *)categoryIdParam
                     andSubcategoryId:(NSString *)subcategoryIdParam
                   andSubcategoryName:(NSString *)subcategoryName;



/** Outlet To Increase Font Size & Layout **/
@property (weak, nonatomic) IBOutlet UIButton *addCompanyBtn;
@property(weak,nonatomic)IBOutlet UILabel *headerTitleLbl;
/** Set width constant of Add Company **/
@property (weak,nonatomic)IBOutlet NSLayoutConstraint *addCompanyBtnWidthConstant;
@end
