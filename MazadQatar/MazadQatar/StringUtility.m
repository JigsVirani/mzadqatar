//
//  StringUtility.m
//  MazadQatar
//
//  Created by samo on 6/10/13.
//  Copyright (c) 2013 Mohammed abdulfattah attia. All rights reserved.
//

#import "StringUtility.h"
#import "RNDecryptor.h"
#import "RNEncryptor.h"

@implementation StringUtility

+ (int)isTextEmpty:(NSString *)text {
  NSCharacterSet *whitespace =
      [NSCharacterSet whitespaceAndNewlineCharacterSet];
  NSString *trimmed = [text stringByTrimmingCharactersInSet:whitespace];
  return (int)[trimmed length];
}


+ (CGSize)getSize:(NSString *)string
      andFontSize:(int)fontSize
           isBold:(bool)isBold
       isPortrait:(bool)isPortrait {
  CGSize displaySize = [DisplayUtility getScreenSize:isPortrait];
  CGSize size = CGSizeZero;

  if (isBold) {
    // size = [string sizeWithFont:[UIFont boldSystemFontOfSize:fontSize]
    //         constrainedToSize:CGSizeMake(displaySize.width-60, MAXFLOAT)
    //             lineBreakMode:NSLineBreakByWordWrapping];

    NSAttributedString *attributedText = [[NSAttributedString alloc]
        initWithString:string
            attributes:@{
              NSFontAttributeName : [UIFont boldSystemFontOfSize:fontSize]
            }];

    CGRect rect = [attributedText
        boundingRectWithSize:CGSizeMake(displaySize.width - 50, CGFLOAT_MAX)
                     options:NSStringDrawingUsesLineFragmentOrigin
                     context:nil];
    size = rect.size;
  } else {
    // size = [string sizeWithFont:[UIFont systemFontOfSize:fontSize]
    //          constrainedToSize:CGSizeMake(displaySize.width-60, MAXFLOAT)
    //             lineBreakMode:NSLineBreakByWordWrapping];

    NSAttributedString *attributedText = [[NSAttributedString alloc]
        initWithString:string
            attributes:@{
              NSFontAttributeName : [UIFont systemFontOfSize:fontSize]
            }];

    CGRect rect = [attributedText
        boundingRectWithSize:CGSizeMake(displaySize.width - 50, CGFLOAT_MAX)
                     options:NSStringDrawingUsesLineFragmentOrigin
                     context:nil];
    size = rect.size;
  }

  // we must add 10 so we can get it exactly as story board
  size.height = size.height;

  return size;
}
+ (CGSize)getSizeWithNewSize:(NSString *)string
      andFontSize:(int)fontSize
           isBold:(bool)isBold
       isPortrait:(bool)isPortrait {
    CGSize displaySize = [DisplayUtility getScreenSize:isPortrait];
    CGSize size = CGSizeZero;
    
    if (isBold) {
        // size = [string sizeWithFont:[UIFont boldSystemFontOfSize:fontSize]
        //         constrainedToSize:CGSizeMake(displaySize.width-60, MAXFLOAT)
        //             lineBreakMode:NSLineBreakByWordWrapping];
        
        NSAttributedString *attributedText = [[NSAttributedString alloc]
                                              initWithString:string
                                              attributes:@{
                                                           NSFontAttributeName : [UIFont boldSystemFontOfSize:fontSize]
                                                           }];
        
        CGRect rect = [attributedText
                       boundingRectWithSize:CGSizeMake(displaySize.width - 8, CGFLOAT_MAX)
                       options:NSStringDrawingUsesLineFragmentOrigin
                       context:nil];
        size = rect.size;
    } else {
        // size = [string sizeWithFont:[UIFont systemFontOfSize:fontSize]
        //          constrainedToSize:CGSizeMake(displaySize.width-60, MAXFLOAT)
        //             lineBreakMode:NSLineBreakByWordWrapping];
        
        NSAttributedString *attributedText = [[NSAttributedString alloc]
                                              initWithString:string
                                              attributes:@{
                                                           NSFontAttributeName : [UIFont systemFontOfSize:fontSize]
                                                           }];
        
        CGRect rect = [attributedText
                       boundingRectWithSize:CGSizeMake(displaySize.width - 8, CGFLOAT_MAX)
                       options:NSStringDrawingUsesLineFragmentOrigin
                       context:nil];
        size = rect.size;
    }
    
    // we must add 10 so we can get it exactly as story board
    size.height = size.height;
    
    return size;
}
+ (CGSize)getSizeForComment:(NSString *)string
                 andFontSize:(int)fontSize
                      isBold:(bool)isBold
                  isPortrait:(bool)isPortrait {
    CGSize displaySize = [DisplayUtility getScreenSize:isPortrait];
    CGSize size = CGSizeZero;
    
    if (isBold) {
        // size = [string sizeWithFont:[UIFont boldSystemFontOfSize:fontSize]
        //         constrainedToSize:CGSizeMake(displaySize.width-60, MAXFLOAT)
        //             lineBreakMode:NSLineBreakByWordWrapping];
        
        NSAttributedString *attributedText = [[NSAttributedString alloc]
                                              initWithString:string
                                              attributes:@{
                                                           NSFontAttributeName : [UIFont boldSystemFontOfSize:fontSize]
                                                           }];
        
        CGRect rect = [attributedText
                       boundingRectWithSize:CGSizeMake(displaySize.width - 92, CGFLOAT_MAX)
                       options:NSStringDrawingUsesLineFragmentOrigin
                       context:nil];
        size = rect.size;
    } else {
        // size = [string sizeWithFont:[UIFont systemFontOfSize:fontSize]
        //          constrainedToSize:CGSizeMake(displaySize.width-60, MAXFLOAT)
        //             lineBreakMode:NSLineBreakByWordWrapping];
        
        NSAttributedString *attributedText = [[NSAttributedString alloc]
                                              initWithString:string
                                              attributes:@{
                                                           NSFontAttributeName : [UIFont systemFontOfSize:fontSize]
                                                           }];
        
        CGRect rect = [attributedText
                       boundingRectWithSize:CGSizeMake(displaySize.width - 92, CGFLOAT_MAX)
                       options:NSStringDrawingUsesLineFragmentOrigin
                       context:nil];
        size = rect.size;
    }
    
    // we must add 10 so we can get it exactly as story board
    size.height = size.height;
    return size;
}

+ (NSData *)encryptString:(NSString *)encryptString {
  NSData *data = [encryptString dataUsingEncoding:NSUTF8StringEncoding];
  NSError *error;
  NSData *encryptedData =
      [RNEncryptor encryptData:data
                  withSettings:kRNCryptorAES256Settings
                      password:@"Mzad Qatar Key Encryption 66524147"
                         error:&error];
  return encryptedData;
}

+ (NSString *)initDecryptedString:(NSData *)decryptString {
  NSError *error;
  NSData *decryptedData =
      [RNDecryptor decryptData:decryptString
                  withPassword:@"Mzad Qatar Key Encryption 66524147"
                         error:&error];
  NSString *decryptedString =
      [[NSString alloc] initWithData:decryptedData
                            encoding:NSUTF8StringEncoding];
  return decryptedString;
}

+(BOOL) NSStringIsValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = NO; // Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
    NSString *stricterFilterString = @"[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}";
    NSString *laxString = @".+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}
@end
