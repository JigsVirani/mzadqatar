//
//  CategoryTypesBarCell.m
//  Mzad Qatar
//
//  Created by Mohammed AbdulFattah on 2/9/15.
//  Copyright (c) 2015 Mohammed abdulfattah attia. All rights reserved.
//

#import "CategoryTypesBarCell.h"
#import "DisplayUtility.h"

@implementation CategoryTypesBarCell


- (void) awakeFromNib
{
    [super awakeFromNib];
    
    if ([DisplayUtility isPad]) {
        _btnCategoryTypeName.titleLabel.font = [UIFont systemFontOfSize:22.0];
    }else{
        _btnCategoryTypeName.titleLabel.font = [UIFont systemFontOfSize:16.0];
    }
    
}
- (IBAction)btnCategoryTypeNameClicked:(id)sender {
  [self.categoryTypesBarCellDelegate
      btnCategoryTypeNameClickedWithId:self.advertiseType.advertiseTypeId
                              andIndex:self.tabIndex];
}
@end
