//
//  SystemUtility.h
//  Mzad Qatar
//
//  Created by samo on 10/24/13.
//  Copyright (c) 2013 Mohammed abdulfattah attia. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MessageUI/MessageUI.h>

@interface SystemUtility : NSObject

//+ (void)callNumber:(NSString *)number;
+ (void)callNumberWithCountryCode:(NSString *)CountryCode withNumber:(NSString *)number;
+ (void)smsNumberInView:(UIViewController *)currentView
            andCountryCode:(NSString *)CountryCode
              andNumber:(NSString *)number
            andDelegate:(id<MFMessageComposeViewControllerDelegate>)delegate;
+ (bool)checkConnection;
@end
