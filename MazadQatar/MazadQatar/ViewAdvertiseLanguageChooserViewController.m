//
//  LanguageChooserViewController.m
//  Mzad Qatar
//
//  Created by samo on 12/19/13.
//  Copyright (c) 2013 Mohammed abdulfattah attia. All rights reserved.
//

#import "ViewAdvertiseLanguageChooserViewController.h"
#import "UserSetting.h"
#import "AppDelegate.h"
#import "LanguageManager.h"

@interface ViewAdvertiseLanguageChooserViewController ()
{
    BOOL isNeedToReloadViews;
    NSString *currentLangugeCode;
}
@end

@implementation ViewAdvertiseLanguageChooserViewController

- (id)initWithStyle:(UITableViewStyle)style {
  self = [super initWithStyle:style];
  if (self) {
    // Custom initialization
  }
  return self;
}

- (void)viewDidLoad {
    
    self->screenName = @"View Advertise Language Chooser";
    [super viewDidLoad];
    
    [self.view setBackgroundColor:[UIColor blackColor]];
    isNeedToReloadViews = false;
    currentLangugeCode = [LanguageManager currentLanguageCode];
    
    if(self.isHideBackButton == YES){
        self.navigationItem.hidesBackButton = YES;
    }else{
        self.navigationItem.hidesBackButton = NO;
    }
    
    if([[SharedData getDeviceLanguage]isEqualToString:@"en"]){
        self.title = @"Select language";
    }else{
        self.title = @"اختار اللغه";
    }
    
//    UIImage *backgroundPattern = [UIImage imageNamed:@"bg.png"];
//    [self.selectLanguageTable setBackgroundColor:[UIColor colorWithPatternImage:backgroundPattern]];
    
    self.selectedIndex = [UserSetting getUserAddAdvertiseLanguage];
    
    //self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    //[self.navigationController setToolbarHidden:YES animated:YES];
    //[self.navigationController.navigationBar.topItem setTitle:NSLocalizedString(@"BACK", nil)];
}
-(void)viewWillDisappear:(BOOL)animated{
    
    [super viewWillDisappear:animated];
    self.tabBarController.tabBar.hidden = NO;
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    self.tabBarController.tabBar.hidden = true;
}

- (void)didReceiveMemoryWarning {
  [super didReceiveMemoryWarning];
  // Dispose of any resources that can be recreated.
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
  return 4;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = nil;
    // comments of product
    if(indexPath.row == 0) {
        cell = [tableView dequeueReusableCellWithIdentifier:@"englishAndArabicIdentifier" forIndexPath:indexPath];
    }else if(indexPath.row == 1) {
        cell = [tableView dequeueReusableCellWithIdentifier:@"englishIdentifier" forIndexPath:indexPath];
    }else if(indexPath.row == 2) {
        cell = [tableView dequeueReusableCellWithIdentifier:@"arabicIdentifier" forIndexPath:indexPath];
    }else{
        
        LanguageCell *cell = (LanguageCell *)[tableView dequeueReusableCellWithIdentifier:@"LanguageCell"];
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.backgroundColor = [UIColor clearColor];
        
        if([currentLangugeCode isEqualToString:@"ar"]){
            
            [cell.stackViewEng setHidden:true];
            [cell.stackViewAr setHidden:false];
            
            cell.btnArabicApp.layer.borderColor = [[UIColor clearColor] CGColor];
            cell.btnEnglishApp.layer.borderColor = [[UIColor lightGrayColor] CGColor];
            
            [cell.btnArabicApp setBackgroundColor:[self colorWithHex:@"#A01B58" alpha:1.0]];
            [cell.btnEnglishApp setBackgroundColor:[UIColor clearColor]];
            
            cell.btnArabicAppAr.layer.borderColor = [[UIColor clearColor] CGColor];
            cell.btnEnglishAppAr.layer.borderColor = [[UIColor lightGrayColor] CGColor];
            
            [cell.btnArabicAppAr setBackgroundColor:[self colorWithHex:@"#A01B58" alpha:1.0]];
            [cell.btnEnglishAppAr setBackgroundColor:[UIColor clearColor]];
            
        }else{
            
            [cell.stackViewAr setHidden:true];
            [cell.stackViewEng setHidden:false];
            
            cell.btnEnglishApp.layer.borderColor = [[UIColor clearColor] CGColor];
            cell.btnArabicApp.layer.borderColor = [[UIColor lightGrayColor] CGColor];
            
            [cell.btnEnglishApp setBackgroundColor:[self colorWithHex:@"#A01B58" alpha:1.0]];
            [cell.btnArabicApp setBackgroundColor:[UIColor clearColor]];
            
            cell.btnEnglishAppAr.layer.borderColor = [[UIColor clearColor] CGColor];
            cell.btnArabicAppAr.layer.borderColor = [[UIColor lightGrayColor] CGColor];
            
            [cell.btnEnglishAppAr setBackgroundColor:[self colorWithHex:@"#A01B58" alpha:1.0]];
            [cell.btnArabicAppAr setBackgroundColor:[UIColor clearColor]];
        }
        
        if([UserSetting getUserFilterViewAdvertiseLanguage]==2){
            
            cell.btnArabicAdv.layer.borderColor = [[UIColor clearColor] CGColor];
            cell.btnEnglishArabic.layer.borderColor = [[UIColor lightGrayColor] CGColor];
            cell.btnEnglishAdv.layer.borderColor = [[UIColor lightGrayColor] CGColor];
            
            [cell.btnArabicAdv setBackgroundColor:[self colorWithHex:@"#A01B58" alpha:1.0]];
            [cell.btnEnglishArabic setBackgroundColor:[UIColor clearColor]];
            [cell.btnEnglishAdv setBackgroundColor:[UIColor clearColor]];
            
        }else if([UserSetting getUserFilterViewAdvertiseLanguage]==1){
            
            cell.btnEnglishAdv.layer.borderColor = [[UIColor clearColor] CGColor];
            cell.btnEnglishArabic.layer.borderColor = [[UIColor lightGrayColor] CGColor];
            cell.btnArabicAdv.layer.borderColor = [[UIColor lightGrayColor] CGColor];
            
            [cell.btnEnglishAdv setBackgroundColor:[self colorWithHex:@"#A01B58" alpha:1.0]];
            [cell.btnEnglishArabic setBackgroundColor:[UIColor clearColor]];
            [cell.btnArabicAdv setBackgroundColor:[UIColor clearColor]];
        }else{
            
            cell.btnEnglishArabic.layer.borderColor = [[UIColor clearColor] CGColor];
            cell.btnEnglishAdv.layer.borderColor = [[UIColor lightGrayColor] CGColor];
            cell.btnArabicAdv.layer.borderColor = [[UIColor lightGrayColor] CGColor];
            
            [cell.btnEnglishArabic setBackgroundColor:[self colorWithHex:@"#A01B58" alpha:1.0]];
            [cell.btnEnglishAdv setBackgroundColor:[UIColor clearColor]];
            [cell.btnArabicAdv setBackgroundColor:[UIColor clearColor]];
        }
        
        [cell.btnEnglishApp setTag:indexPath.row];
        [cell.btnArabicApp setTag:indexPath.row];
        [cell.btnEnglishAppAr setTag:indexPath.row];
        [cell.btnArabicAppAr setTag:indexPath.row];
        
        [cell.btnEnglishArabic setTag:indexPath.row];
        [cell.btnEnglishAdv setTag:indexPath.row];
        [cell.btnArabicAdv setTag:indexPath.row];
        
        [cell.btnEnglishApp addTarget:self action:@selector(btnEnglisAppLanguageTapped:) forControlEvents:UIControlEventTouchUpInside];
        [cell.btnArabicApp addTarget:self action:@selector(btnArabicAppLanguageTapped:) forControlEvents:UIControlEventTouchUpInside];
        
        [cell.btnEnglishAppAr addTarget:self action:@selector(btnEnglisAppLanguageTapped:) forControlEvents:UIControlEventTouchUpInside];
        [cell.btnArabicAppAr addTarget:self action:@selector(btnArabicAppLanguageTapped:) forControlEvents:UIControlEventTouchUpInside];
        
        [cell.btnEnglishArabic addTarget:self action:@selector(btnEnglisArabicTapped:) forControlEvents:UIControlEventTouchUpInside];
        [cell.btnEnglishAdv addTarget:self action:@selector(btnEnglisAdvLanguageTapped:) forControlEvents:UIControlEventTouchUpInside];
        [cell.btnArabicAdv addTarget:self action:@selector(btnArabicAdvLanguageTapped:) forControlEvents:UIControlEventTouchUpInside];
        
        [cell.btnContinue addTarget:self action:@selector(btnContinuewTapped:) forControlEvents:UIControlEventTouchUpInside];
        
        return cell;
    }
    
    /*if(indexPath.row == [UserSetting getUserFilterViewAdvertiseLanguage]) {
     cell.accessoryType = UITableViewCellAccessoryCheckmark;
     } else {
     cell.accessoryType = UITableViewCellAccessoryNone;
     }*/
    
    cell.backgroundColor = [UIColor clearColor];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
  //return 70;
    if(indexPath.row == 0) {
        return 0;
    }else if (indexPath.row == 1) {
        return 0;
    } else if (indexPath.row == 2) {
        return 0;
    }else{
        return 580;
    }
}
-(UIColor*)colorWithHex:(NSString*)hex alpha:(CGFloat)alpha {
    
    assert(7 == [hex length]);
    assert('#' == [hex characterAtIndex:0]);
    
    NSString *redHex = [NSString stringWithFormat:@"0x%@", [hex substringWithRange:NSMakeRange(1, 2)]];
    NSString *greenHex = [NSString stringWithFormat:@"0x%@", [hex substringWithRange:NSMakeRange(3, 2)]];
    NSString *blueHex = [NSString stringWithFormat:@"0x%@", [hex substringWithRange:NSMakeRange(5, 2)]];
    
    unsigned redInt = 0;
    NSScanner *rScanner = [NSScanner scannerWithString:redHex];
    [rScanner scanHexInt:&redInt];
    
    unsigned greenInt = 0;
    NSScanner *gScanner = [NSScanner scannerWithString:greenHex];
    [gScanner scanHexInt:&greenInt];
    
    unsigned blueInt = 0;
    NSScanner *bScanner = [NSScanner scannerWithString:blueHex];
    [bScanner scanHexInt:&blueInt];
    
    return [self colorWith8BitRed:redInt green:greenInt blue:blueInt alpha:alpha];
}
-(UIColor*)colorWith8BitRed:(NSInteger)red green:(NSInteger)green blue:(NSInteger)blue alpha:(CGFloat)alpha {
    return [UIColor colorWithRed:(red/255.0) green:(green/255.0) blue:(blue/255.0) alpha:alpha];
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    /*self.selectedIndex = (int)indexPath.row;
    [self.delegate ViewAdvertiselanguageChanged:(int)indexPath.row withLanguageName:[UserSetting getUserAddAdvertiseLanguageName:(int)indexPath.row]];
    [self.navigationController popViewControllerAnimated:YES];*/
}

#pragma mark App Button Event
-(IBAction)btnEnglisAppLanguageTapped:(UIButton *)sender{
    
    currentLangugeCode = @"en";
    isNeedToReloadViews = true;
    
    LanguageCell *cell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:sender.tag inSection:0]];
    [self setCellAppButtonStyle:cell];
}
-(IBAction)btnArabicAppLanguageTapped:(UIButton *)sender{
    
    currentLangugeCode = @"ar";
    isNeedToReloadViews = true;
    
    LanguageCell *cell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:sender.tag inSection:0]];
    [self setCellAppButtonStyle:cell];
}

#pragma mark setApp Button style
-(void)setCellAppButtonStyle:(LanguageCell *)cell{
    
    if([currentLangugeCode isEqualToString:@"en"]){
        
        cell.btnEnglishApp.layer.borderColor = [[UIColor clearColor] CGColor];
        cell.btnArabicApp.layer.borderColor = [[UIColor lightGrayColor] CGColor];
        
        [cell.btnEnglishApp setBackgroundColor:[self colorWithHex:@"#A01B58" alpha:1.0]];
        [cell.btnArabicApp setBackgroundColor:[UIColor clearColor]];
        
        cell.btnEnglishAppAr.layer.borderColor = [[UIColor clearColor] CGColor];
        cell.btnArabicAppAr.layer.borderColor = [[UIColor lightGrayColor] CGColor];
        
        [cell.btnEnglishAppAr setBackgroundColor:[self colorWithHex:@"#A01B58" alpha:1.0]];
        [cell.btnArabicAppAr setBackgroundColor:[UIColor clearColor]];
    }else{
        
        cell.btnArabicApp.layer.borderColor = [[UIColor clearColor] CGColor];
        cell.btnEnglishApp.layer.borderColor = [[UIColor lightGrayColor] CGColor];
        
        [cell.btnArabicApp setBackgroundColor:[self colorWithHex:@"#A01B58" alpha:1.0]];
        [cell.btnEnglishApp setBackgroundColor:[UIColor clearColor]];
        
        cell.btnArabicAppAr.layer.borderColor = [[UIColor clearColor] CGColor];
        cell.btnEnglishAppAr.layer.borderColor = [[UIColor lightGrayColor] CGColor];
        
        [cell.btnArabicAppAr setBackgroundColor:[self colorWithHex:@"#A01B58" alpha:1.0]];
        [cell.btnEnglishAppAr setBackgroundColor:[UIColor clearColor]];
    }
}

#pragma mark Advertise Button Event
-(IBAction)btnEnglisArabicTapped:(UIButton *)sender{
    
    [UserSetting saveFilterViewAdvertiseLanguage:0];
    LanguageCell *cell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:sender.tag inSection:0]];
    [self setCellAddButtonStyle:cell];
    
    if(self.isHideBackButton == NO){
        [self.delegate ViewAdvertiselanguageChanged:0 withLanguageName:[UserSetting getUserAddAdvertiseLanguageName:0]];
    }
}
-(IBAction)btnEnglisAdvLanguageTapped:(UIButton *)sender{
    
    [UserSetting saveFilterViewAdvertiseLanguage:1];
    LanguageCell *cell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:sender.tag inSection:0]];
    [self setCellAddButtonStyle:cell];
    
    if(self.isHideBackButton == NO){
        [self.delegate ViewAdvertiselanguageChanged:1 withLanguageName:[UserSetting getUserAddAdvertiseLanguageName:1]];
    }
}
-(IBAction)btnArabicAdvLanguageTapped:(UIButton *)sender{
    
    [UserSetting saveFilterViewAdvertiseLanguage:2];
    LanguageCell *cell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:sender.tag inSection:0]];
    [self setCellAddButtonStyle:cell];
    
    if(self.isHideBackButton == NO){
        [self.delegate ViewAdvertiselanguageChanged:2 withLanguageName:[UserSetting getUserAddAdvertiseLanguageName:2]];
    }
}

#pragma mark setAdvertise Button style
-(void)setCellAddButtonStyle:(LanguageCell *)cell{
    
    if([UserSetting getUserFilterViewAdvertiseLanguage]==2){
        
        cell.btnArabicAdv.layer.borderColor = [[UIColor clearColor] CGColor];
        cell.btnEnglishArabic.layer.borderColor = [[UIColor lightGrayColor] CGColor];
        cell.btnEnglishAdv.layer.borderColor = [[UIColor lightGrayColor] CGColor];
        
        [cell.btnArabicAdv setBackgroundColor:[self colorWithHex:@"#A01B58" alpha:1.0]];
        [cell.btnEnglishArabic setBackgroundColor:[UIColor clearColor]];
        [cell.btnEnglishAdv setBackgroundColor:[UIColor clearColor]];
        
    }else if([UserSetting getUserFilterViewAdvertiseLanguage]==1){
        
        cell.btnEnglishAdv.layer.borderColor = [[UIColor clearColor] CGColor];
        cell.btnEnglishArabic.layer.borderColor = [[UIColor lightGrayColor] CGColor];
        cell.btnArabicAdv.layer.borderColor = [[UIColor lightGrayColor] CGColor];
        
        [cell.btnEnglishAdv setBackgroundColor:[self colorWithHex:@"#A01B58" alpha:1.0]];
        [cell.btnEnglishArabic setBackgroundColor:[UIColor clearColor]];
        [cell.btnArabicAdv setBackgroundColor:[UIColor clearColor]];
    }else{
        
        cell.btnEnglishArabic.layer.borderColor = [[UIColor clearColor] CGColor];
        cell.btnEnglishAdv.layer.borderColor = [[UIColor lightGrayColor] CGColor];
        cell.btnArabicAdv.layer.borderColor = [[UIColor lightGrayColor] CGColor];
        
        [cell.btnEnglishArabic setBackgroundColor:[self colorWithHex:@"#A01B58" alpha:1.0]];
        [cell.btnEnglishAdv setBackgroundColor:[UIColor clearColor]];
        [cell.btnArabicAdv setBackgroundColor:[UIColor clearColor]];
    }
}

#pragma mark Continuew Button Event
-(IBAction)btnContinuewTapped:(UIButton *)sender{
    
    if([UserSetting getUserFilterViewAdvertiseLanguage] == -1){
        [UserSetting saveFilterViewAdvertiseLanguage:0];
    }
    if(isNeedToReloadViews == true){
        
        if([currentLangugeCode isEqualToString:@"en"]){
            [LanguageManager saveLanguageByIndex:0];
        }else{
            [LanguageManager saveLanguageByIndex:1];
        }
        AppDelegate *delegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
        NSString *storyboardName = @"MainStoryboard_iPhone";
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:storyboardName bundle:nil];
        delegate.window.rootViewController = [storyboard instantiateInitialViewController];
    }else{
        [self.navigationController popViewControllerAnimated:NO];
    }
}
@end

@implementation LanguageCell
-(void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
-(void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}

@end
