//
//  ErrorManager.h
//  MazadQatar
//
//  Created by samo on 5/19/13.
//  Copyright (c) 2013 Mohammed abdulfattah attia. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ErrorDialogueViewController.h"
#import "ShareAppViewController.h"
#import "RateViewController.h"
#import "LocationDialogue.h"
#import "SharedData.h"
#import "ConfirmationDialogueViewController.h"
#import "UIView+Toast.h"

//#import "MenuTableViewController.h"

@interface DialogManager : NSObject
+(void) makeToastWithString:(NSString *)toastMessage;
+(void) toastWithImageAndText:(NSString *)toastMessage;
+(void) makeToastOnly:(NSString *)toastMessage;
+ (void)showErrorRegisterAsCompany;
+ (void)showErrorInServer;
+ (void)showErrorInServerbyMessage:(NSString *)errorMsg;
+ (void)showCopyConfirmation:(NSString *)msg;
+ (void)showErrorInDeletingAdvertise;
+ (void)showErrorInRefreshingAdvertise;
+ (void)showErrorWithMessage:(NSString *)msg;
+ (void)showDialogWithMessage:(NSString *)msg;
+ (void)showErrorRegisterationNumberEmtpy;
+ (void)showErrorActivationNumberEmtpy;
+ (void)showErrorProductImagesCount;
+ (void)showErrorProductTitleEmpty;
+ (void)showErrorProductTitleEnglishEmpty;
+ (void)showErrorProductTitleArabicEmpty;
+ (void)showErrorProductPriceEmpty;
+ (void)showErrorProductDescriptionEmpty;
+ (void)showErrorProductDescriptionEnglishEmpty;
+ (void)showErrorProductDescriptionArabicEmpty;
+ (void)showErrorCategoryNotChosen;
+ (void)showErrorSubCategoryNotChosen;
+ (void)showErrorRegionChoosedBeforeCities;
+ (void)showErrorAdvertiseTypeNotChoosen;
+ (void)showErrorCitiesNotChosen;
+ (void)showErrorProductLanguage;
+ (void)showErrorRegionNotChosen;
+ (void)showErrorSubSubCagegoryNotChosen;
+ (void)showErrorManfactureYearNotChosen;
+ (void)showErrorKmNotChosen;
+ (void)showErrorFurnishedTypeNotChosen;
+ (void)showErrorNumberOfRoomsNotChosen;
+ (void)showErrorCompleteAllFieldsInCategoryFilters;
+ (void)showErrorUserCommentNameEmpty;
+ (void)showErrorUpdateUserNameEmpty;
+ (void)showErrorUpdateUserLocationEmpty;
+ (void)showErrorUserCommentEmpty;
+ (void)showErrorProductNotFound;
+ (void)showErrorMaxImagesLimit;
+ (void)showErrorImageSizeLimit;
+ (void)showErrorActivationKeyNotCorrect;
+ (void)showSuccessActivationKeySent;
+ (void)showErrorSendingActivationKey;
+ (void)showErrorInConnection;
+ (void)showErrorUserIsBlocked;
+ (void)showErrorFeedbackEmpty;
+ (void)showErrorResendSmSLimit;
+ (void)showUserFilterViewAdvertiseLanguageDialog;
+ (void)showPressEditAdvertiseDialog;
+ (void)showPressEditInfoDialog;
+ (void)showChooseCategoryFirst;
+ (void)showErrorNoEmailAccountAvailable;
+ (void)showShareDialogue;
+ (void)showSuccessMessage:(NSString*) message;
+ (void)showErrorCompanyNumberEmtpy;
+ (void)showErrorCompanyNameEmtpy;
+ (void)showErrorCompanyEmailEmtpy;
+ (void)showErrorCompanyDetailsEmtpy;
+ (void)showErrorCompanyEmailValidation;

+ (void)showLocationDialogue:(NSString *)strLocationName andDelegate:(id<LocationDialogueDelegate>)delegate andRequestId:(NSString *)requestId;
+ (void)showMapDialogue:(NSString *)strLocationName andDelegate:(id<LocationDialogueDelegate>)delegate andRequestId:(NSString *)requestId;
+ (void)showRateDialogue;
+ (void)
    showConfirmationDialogueWithDescription:(NSString *)descriptionText
                          andConfirmBtnText:(NSString *)confirmBtnText
                                andDelegate:
                                    (id<ConfirmationDialogueViewControllerDelegate>)
                                        delegate
                               andRequestId:(NSString *)requestId;

@end
