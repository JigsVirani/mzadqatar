//
//  GridCell.m
//  MazadQatar
//
//  Created by samo on 3/20/13.
//  Copyright (c) 2013 Mohammed abdulfattah attia. All rights reserved.
//

#import "ProductGridCell.h"

@implementation ProductGridCell

NSString *requestConfirmDeleteAdvertise = @"requestConfirmDeleteAdvertise";

- (void) awakeFromNib
{
    [super awakeFromNib];
    if([DisplayUtility isPad]){
       [self.productTitle setFont:[UIFont systemFontOfSize:22.0]];
       [self.productPrice setFont:[UIFont systemFontOfSize:18.0]];
        self.cmpLogoWidthConstant.constant = 90.0;
        self.cmpLogoHeightConstant.constant = 25.0;
        self.btnDeleteUnderAdHeightConstant.constant = 50.0;
        //self.btnDeleteUnderAd.titleLabel.font = [UIFont systemFontOfSize:16.0];
    }
}

- (IBAction)btnDeleteUnderAdClicked:(id)sender {

  [DialogManager
      showConfirmationDialogueWithDescription:
          [NSString
              stringWithFormat:@"%@ %@",
                               NSLocalizedString(
                                   @"CONFIRMATION_DELETE_ADVERTISE_MSG", nil),
                                NSLocalizedString(@"?",nil)]
                            andConfirmBtnText:
                                NSLocalizedString(
                                    @"CONFIRMATION_DELETE_BTN_TEXT", nil)
                                  andDelegate:self
                                 andRequestId:requestConfirmDeleteAdvertise];
}

- (IBAction)opedEditAdvertise:(id)sender {
  [delegate advertiseEditBtnClicked:sender];
}
- (IBAction)refreshAdvertiseClicked:(id)sender {
  [delegate refreshAdvertiseClicked:sender];
}

- (IBAction)btnDeleteClicked:(id)sender {

  [DialogManager
      showConfirmationDialogueWithDescription:
          [NSString
              stringWithFormat:@"%@ %@",
                               NSLocalizedString(
                                   @"CONFIRMATION_DELETE_ADVERTISE_MSG", nil),
                               NSLocalizedString(@"?",nil)]
                            andConfirmBtnText:
                                NSLocalizedString(
                                    @"CONFIRMATION_DELETE_BTN_TEXT", nil)
                                  andDelegate:self
                                 andRequestId:requestConfirmDeleteAdvertise];
}

- (void)confirmBtnClicked:(NSString *)requestId {
  if (requestId == requestConfirmDeleteAdvertise) {
    // stopped now we changed the ui
    [delegate cellDeleted:self];
  }
}

- (void)setDelegate:(id<ProductGridCellDelegate>)delegateParam {
  delegate = delegateParam;
}
@end
