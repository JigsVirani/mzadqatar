//
//  GridCell.h
//  MazadQatar
//
//  Created by samo on 3/20/13.
//  Copyright (c) 2013 Mohammed abdulfattah attia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ServerManager.h"
#import "ServerManagerResult.h"
#import "SharedGraphicInfo.h"
#import "DialogManager.h"
#import "ConfirmationDialogueViewController.h"
#import <GoogleMobileAds/GoogleMobileAds.h>

// Delgate
@class ProductGridCell;
@protocol ProductGridCellDelegate <NSObject>
@required
- (void)cellDeleted:(UICollectionViewCell *)cell;
@required
@optional
- (void)advertiseEditBtnClicked:(id)sender;
- (void)refreshAdvertiseClicked:(id)sender;
@optional
@end

@interface ProductGridCell
    : UICollectionViewCell <ConfirmationDialogueViewControllerDelegate> {
  id<ProductGridCellDelegate> delegate;
}
@property (weak, nonatomic) IBOutlet DFPBannerView  *bannerView;
@property(strong, nonatomic) IBOutlet UIImageView *productImageView;
@property(strong, nonatomic) IBOutlet UIImageView *productImageViewTwo;
@property(strong, nonatomic) IBOutlet UIImageView *productImageViewThree;

@property(strong, nonatomic) IBOutlet UILabel *productTitle;
@property(strong, nonatomic) IBOutlet UILabel *productPrice;

@property(strong, nonatomic) IBOutlet UIActivityIndicatorView *loadingIndicator;
@property(strong, nonatomic) IBOutlet UIActivityIndicatorView *loadingIndicatorTwo;
@property(strong, nonatomic) IBOutlet UIActivityIndicatorView *loadingIndicatorThree;


@property(strong, nonatomic) IBOutlet UIProgressView *uploadingProgressPar;
@property(strong, nonatomic) IBOutlet UIView *isCompanyView;
@property(strong, nonatomic) IBOutlet UIButton *btnLike;
@property(strong, nonatomic) IBOutlet UIButton *btnComment;
@property(strong, nonatomic) IBOutlet UILabel *lblLike;
@property(strong, nonatomic) IBOutlet UILabel *lblComment;
@property(strong, nonatomic) IBOutlet UIButton *btnCLose;
@property(strong, nonatomic) IBOutlet UIActivityIndicatorView *btnCloseLoading;
@property(strong, nonatomic) IBOutlet UIButton *btnDeleteUnderAd;
@property(strong, nonatomic) IBOutlet UIImageView *iconUnderAd;
@property(strong, nonatomic)
    IBOutlet UIActivityIndicatorView *loadingIndicatorUnderDeleteAd;
@property(strong, nonatomic) IBOutlet UIButton *refreshAdvertiseBtn;
@property(strong, nonatomic)
    IBOutlet UIActivityIndicatorView *refreshAdvertiseIndicator;
@property(strong, nonatomic) IBOutlet UIButton *btnEditPaidAdvertise;

@property(strong, nonatomic) IBOutlet UIButton * adEdit;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lblHeightConstant;
- (IBAction)btnDeleteUnderAdClicked:(id)sender;

- (IBAction)btnDeleteClicked:(id)sender;

- (void)setDelegate:(id<ProductGridCellDelegate>)delegateParam;

@property(strong, nonatomic) IBOutlet UIImageView *imageEditPaidAdvertise;
@property(strong, nonatomic) IBOutlet UIImageView *refreshAdvertiseImage;
@property(strong, nonatomic) IBOutlet UIImageView *deleteImage;
/** Set width constant of Add Company **/
@property (weak,nonatomic)IBOutlet NSLayoutConstraint *cmpLogoWidthConstant,*cmpLogoHeightConstant,*btnDeleteUnderAdHeightConstant;
@end
