//
//  UserProfileViewController.h
//  Mzad Qatar
//
//  Created by Mohammed AbdulFattah on 1/28/15.
//  Copyright (c) 2015 Mohammed abdulfattah attia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProductObject.h"

#import "GAITrackedViewController.h"

@interface EditUserProfileViewController
    : GAITrackedViewController <UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property(nonatomic, strong) ProductObject *productObject;

@end
