//
//  SearchTableCell.h
//  Mzad Qatar
//
//  Created by Paresh Vasoya on 31/01/17.
//  Copyright © 2017 Mohammed abdulfattah attia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchTableCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel *lblSerchTitle;
@property (nonatomic, weak) IBOutlet UILabel *lblSearchCategory;
@property (nonatomic, weak) IBOutlet UIImageView *imgArrow;

@end
