//
//  ActivationScreen.m
//  MazadQatar
//
//  Created by samo on 4/1/13.
//  Copyright (c) 2013 Mohammed abdulfattah attia. All rights reserved.
//

#import "ActivationScreen.h"

//#import "TestFlight.h"

@interface ActivationScreen ()

@end

@implementation ActivationScreen

NSString *requestActivationBtnClicked = @"activationBtnClicked";
NSString *requestResendActivationCode = @"requestResendActivationCode";
NSString *getUserProfile = @"getUserProfile";

@synthesize successScreenSegueName;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if ([DisplayUtility isPad]) {
        self.movementDistance = 2000; // tweak as needed
    } else {
        self.movementDistance = 70; // tweak as needed
    }
    self.screenName = @"Activation Screen";
    // Do any additional setup after loading the view.
    _activationNumberField.textColor = [UIColor whiteColor];
    
    [self.navigationController.navigationBar setTintColor: [UIColor whiteColor]];
    
    self.detailTxtView.text = NSLocalizedString(@"Activation_Description", nil);
    
    if([DisplayUtility isPad]){
        self.activationLbl.font = [UIFont systemFontOfSize:22.0];
        self.activationNumberField.font = [UIFont systemFontOfSize:20.0];
        self.detailTxtView.font = [UIFont systemFontOfSize:20.0];
        self.activateBtn.titleLabel.font = [UIFont systemFontOfSize:22.0];
        self.resendBtn.titleLabel.font = [UIFont systemFontOfSize:22.0];
        self.btnResendHeightConstant.constant = 40.0;
         self.btnActivateHeightConstant.constant = 40.0;
    }
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [self.view endEditing:YES];
    
    if ([UserSetting isUserRegistered] == true) {
        [self.navigationController popViewControllerAnimated:NO];
        return;
    }
    
    CGSize displaySize = [DisplayUtility getScreenSize];
    self.scrollView.contentSize = CGSizeMake(displaySize.width, displaySize.height);
    self.tabBarController.tabBar.hidden = true;
}
-(void)textFieldDidBeginEditing:(UITextField *)textField{
    if([[UIScreen mainScreen]bounds].size.height <= 568){
        [self.scrollView setContentOffset:CGPointMake(0, textField.frame.origin.y-100) animated:NO];
    }else if([[UIScreen mainScreen]bounds].size.height <= 667){
        [self.scrollView setContentOffset:CGPointMake(0, textField.frame.origin.y-175) animated:NO];
    }else if([[UIScreen mainScreen]bounds].size.height <= 736){
        [self.scrollView setContentOffset:CGPointMake(0, textField.frame.origin.y-250) animated:NO];
    }
}
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [self.view endEditing:YES];
    self.tabBarController.tabBar.hidden = false;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)activateBtnClicked:(id)sender {
    
    [self.view endEditing:YES];
    
    
    if ([self.activationNumberField.text length] == 0) {
        [DialogManager showErrorActivationNumberEmtpy];
        return;
    }
    
    [self.activateIndicator startAnimating];

    [ServerManager checkActivationCode:self.activationNumberField.text
                        AndCountryCode:@"00974"
                             AndNumber:self.userRegisteredNumber
                           AndDelegate:self
                         withRequestId:requestActivationBtnClicked];
}

- (IBAction)resendBtnClicked:(id)sender {
    
    [self.resendIndicator startAnimating];

    [ServerManager registeUserWithName:@""
                        AndCountryCode:@"00974"
                             AndNumber:self.userRegisteredNumber
                              AndEmail:@""
                          andUserToken:[UserSetting getuserToken]
                           AndDelegate:self
                         withRequestId:requestResendActivationCode];
}

- (void)requestFinished:(ServerManagerResult *)serverManagerResult
            ofRequestId:(NSString *)requestId
    withNumberOfRequest:(int)numberOfRequest {
    
    [self.activateIndicator stopAnimating];
    [self.resendIndicator stopAnimating];
    
    if ([serverManagerResult.opertationResult
         isEqualToString:[ServerManager ERROR_IN_CONNECTION_OPERATION]]) {
        [DialogManager showErrorInConnection];
    }
    
    if (requestId == requestActivationBtnClicked) {
        if ([serverManagerResult.opertationResult
             isEqualToString:[ServerManager SUCCESS_OPERATION]]) {
            //    [TestFlight passCheckpoint:@"success activation in activation
            //    screen"];
            
            
            
            [UserSetting saveUserSettingWithName:@""
                                 withCountryCode:@"00974"
                                      withNumber:self.userRegisteredNumber
                                       withEmail:@""];
            
            [UserSetting setIsUserRegistered:YES];
            
            [self.activateIndicator startAnimating];
            [ServerManager getUserProductsWithDelegate:self withUserProductsPageNumber:0 withUserProductsNumberOfPages:0 withRequestId:getUserProfile withUpdatTime:[SharedData getUserProductsLastUpdateTime] byLanguage: [UserSetting getAdvertiseLanguageServerParam: [UserSetting getUserFilterViewAdvertiseLanguage]] isAdSupported:@"true" countOfRows:@"0" withAdvertiseResolution:@"" andVisitorCountryCode:[UserSetting getUserCountryCode] andVisitorNumber:[UserSetting getUserNumber]];
            
        } else if ([serverManagerResult.opertationResult
                    isEqualToString:[ServerManager FAILED_OPERATION]]) {
            //   [TestFlight passCheckpoint:@"Sms limit in activation screen"];
            [DialogManager showErrorWithMessage:serverManagerResult.opertationMsg];
        }
        //        else
        //        {
        //           // [TestFlight passCheckpoint:@"activation code error in
        //           activation screen"];
        //
        //            [DialogManager showErrorActivationKeyNotCorrect];
        //       }
    } else if (requestId == requestResendActivationCode) {
        if ([serverManagerResult.opertationResult
             isEqualToString:[ServerManager SUCCESS_OPERATION]]) {
            //  [TestFlight passCheckpoint:@"Sms success in activation screen"];
            [DialogManager showSuccessActivationKeySent];
        } else if ([serverManagerResult.opertationResult
                    isEqualToString:[ServerManager FAILED_OPERATION]]) {
            //   [TestFlight passCheckpoint:@"Sms limit in activation screen"];
            [DialogManager showErrorWithMessage:serverManagerResult.opertationMsg];
        } else {
            // [TestFlight passCheckpoint:@"unkown error resend btn in activation
            // screen"];
            [DialogManager showErrorSendingActivationKey];
        }
    }
    else if (requestId == getUserProfile) {
        
        [self.activateIndicator startAnimating];
        
        ALChatManager * chatManager = [[ALChatManager alloc] initWithApplicationKey:APPLOZIC_APPLICATION_ID];
        [chatManager registerUserWithCompletion:[UserSetting generateUserForApplozic] withHandler:^(ALRegistrationResponse *rResponse, NSError *error) {
            
            [self.activateIndicator stopAnimating];
            
            if (error) {
                [DialogManager makeToastWithString:NSLocalizedString(@"There is some problem with chat", nil)];
                //[DialogManager makeToastOnly:NSLocalizedString(@"There is some problem with chat", nil)];
                return;
            }
            
            NSArray *array = [self.navigationController viewControllers];
            
            [self.navigationController
             popToViewController:[array objectAtIndex:array.count - 3]
             animated:NO];
            
        }];
    }
}

//===================================================prepare for segue

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender

    {
        
    }



- (BOOL)shouldAutorotate {
    return YES;
}

- (void)viewDidUnload {
    // [self setActivationNumberField:nil];
    // [self setScrollView:nil];
    [super viewDidUnload];
}

- (void)willRotateToInterfaceOrientation:
(UIInterfaceOrientation)toInterfaceOrientation
                                duration:(NSTimeInterval)duration {
    [self.view endEditing:YES];
    [super willRotateToInterfaceOrientation:toInterfaceOrientation
                                   duration:duration];
}

@end
