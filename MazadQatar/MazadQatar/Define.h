//
//  Define.h
//  Mzad Qatar
//
//  Created by Waris Ali on 04/06/2014.
//  Copyright (c) 2014 Mohammed abdulfattah attia. All rights reserved.
//

#ifndef Mzad_Qatar_Define_h
#define Mzad_Qatar_Define_h

#define COLOR_SEPARATOR [UIColor colorWithRed:81.0/255.0 green:86.0/255.0 blue:92.0/255.0 alpha:1.0]
//#define COLOR_SEPARATOR [UIColor colorWithRed:70.0/255.0 green:70.0/255.0 blue:70.0/255.0 alpha:1.0]
#define COLOR_TABLEBACKGROUND [UIColor colorWithRed:33.0/255.0 green:33.0/255.0 blue:33.0/255.0 alpha:1.0]
#define COLOR_SECTIONVIEW [UIColor colorWithRed:24.0/255.0 green:24.0/255.0 blue:26.0/255.0 alpha:1.0]
#define COLOR_NAVIGATIONBAR [UIColor colorWithRed:160.0/255.0 green:27.0/255.0 blue:88.0/255.0 alpha:1.0]
#define COLOR_WHITECOLOR [UIColor whiteColor]
#define COLOR_BLACKCOLOR [UIColor blackColor]

#endif
