//
//  GoogleAnalyticsTableViewController.m
//  Mzad Qatar
//
//  Created by AHMED HEGAB on 8/8/16.
//  Copyright © 2016 Mohammed abdulfattah attia. All rights reserved.
//

#import "GoogleAnalyticsTableViewController.h"
#import "GAI.h"
#import "GAIDictionaryBuilder.h"

#import "GAIFields.h"

@interface GoogleAnalyticsTableViewController ()


@end

@implementation GoogleAnalyticsTableViewController

- (void)viewDidLoad{
    
    [super viewDidLoad];
    
    id tracker = [[GAI sharedInstance] defaultTracker];
    
    // This screen name value will remain set on the tracker and sent with
    // hits until it is set to a new value or to nil.
    [tracker set:kGAIScreenName
           value:screenName];
    
    // New SDK versions
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
