//
//  LanguageChooserViewController.h
//  Mzad Qatar
//
//  Created by samo on 12/19/13.
//  Copyright (c) 2013 Mohammed abdulfattah attia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GoogleAnalyticsTableViewController.h"

@class LanguageChooserViewController;
@protocol ChoseLanguageChooserDelegate <NSObject>
@required
- (void)languageChanged:(int)index withLanguageName:(NSString *)languageName;
@required
@end

@interface LanguageChooserViewController
    : GoogleAnalyticsTableViewController

@property(assign) int selectedIndex;

@property(nonatomic, strong) id<ChoseLanguageChooserDelegate> delegate;
@property(strong, nonatomic) IBOutlet UITableView *selectLanguageTable;

@end
