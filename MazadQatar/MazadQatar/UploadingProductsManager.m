//
//  UploadingProductsManager.m
//  MazadQatar
//
//  Created by samo on 3/31/13.
//  Copyright (c) 2013 Mohammed abdulfattah attia. All rights reserved.
//

#import "UploadingProductsManager.h"
#import "UploadingListenerManager.h"
#import "ImageViewWithRetryCount.h"

@implementation UploadingProductsManager

NSString *requestAddImageToProductId = @"requestAddImageToProductId";
NSString *requestAddDataToProductId = @"requestAddDataToProductID";
int totalRetryOfUploadingImage = 3;

- (void)startProductObjectUploadloading:(ProductObject *)productToUpload {
  if ([SharedData isAllowLog]) {
    /* NSLog(@"===================================================");
     NSLog(@"start uploading product =%p",productToUpload);
     NSLog(@"count of image to upload =%lu",(unsigned
     long)productToUpload.productImagesOnDevices.count);
     NSLog(@"==================================================="); */
  }

  uploadingDelegate =
      [[UploadingListenerManager getManager] productUploadingDelegate];

  productObjectToUpload = productToUpload;

  [productToUpload setIsUploading:YES];

  [self performSelectorInBackground:@selector(uploadProduct) withObject:nil];
}

- (void)uploadProduct {
  [self addUploadingProgress:0];

  [self addDataWithoutImagesToServer];
}

- (void)addDataWithoutImagesToServer {
  // so it can be handled in view as still uploading after finish uploading we
  // set it false
  [ServerManager addProductInfoWithoutImages:productObjectToUpload
                                withDelegate:self
                               withRequestId:requestAddDataToProductId];
}

- (void)addImagesWithoudDataToServer {
  if (productImagesRetryArray == nil) {
    productImagesRetryArray = [[NSMutableArray alloc] init];
  }

  for (int i = 0; i < [productObjectToUpload.productImagesOnDevices count];
       i++) {
    // add images for retry if failed
    ImageViewWithRetryCount *imageWithRetryCount =
        [[ImageViewWithRetryCount alloc] init];
    imageWithRetryCount.imageView =
        [productObjectToUpload.productImagesOnDevices objectAtIndex:i];
    [productImagesRetryArray addObject:imageWithRetryCount];

    [self addImageToServer:i];
  }
}

- (void)addImageToServer:(int)imageIndex {
  NSString *isMainImage = @"0";

  if (imageIndex == 0) {
    isMainImage = @"0";
  } else {
    isMainImage = @"1";
  }

  if ([[productObjectToUpload.productImagesOnDevices objectAtIndex:imageIndex]
          isKindOfClass:[UIImage class]]) {
    [ServerManager
        addImageToProductId:productObjectToUpload.productId
                isMainImage:isMainImage
                  withImage:[productObjectToUpload.productImagesOnDevices
                                objectAtIndex:imageIndex]
               withDelegate:self
              withRequestId:requestAddImageToProductId
        withNumberOfRequest:imageIndex];
  } else {
    [ServerManager
        addImageUrlToProductId:productObjectToUpload.productId
                   isMainImage:isMainImage
                     withImage:[productObjectToUpload.productImagesOnDevices
                                   objectAtIndex:imageIndex]
                  withDelegate:self
                 withRequestId:requestAddImageToProductId
           withNumberOfRequest:imageIndex];
  }
}

- (void)requestFinished:(ServerManagerResult *)serverManagerResult
            ofRequestId:(NSString *)requestId
    withNumberOfRequest:(int)numberOfRequest {
  if ([serverManagerResult.opertationResult
          isEqualToString:[ServerManager ERROR_IN_CONNECTION_OPERATION]]) {
    [DialogManager showErrorInConnection];
  }

  if ([serverManagerResult.opertationResult
          isEqualToString:[ServerManager FAILED_OPERATION]]) {
    //Comment [DialogManager showErrorInServer];
      [DialogManager showErrorWithMessage:serverManagerResult.opertationMsg];
  }
    if ([serverManagerResult.opertationResult
         isEqualToString:[ServerManager REGISTER_AS_COMPANY]]) {
        [DialogManager showErrorRegisterAsCompany];
    }
  if (requestId == requestAddImageToProductId) {
    if ([serverManagerResult.opertationResult
            isEqualToString:[ServerManager SUCCESS_OPERATION]] == false) {
      ImageViewWithRetryCount *imageWithRetryCount =
          [productImagesRetryArray objectAtIndex:numberOfRequest];

      if (imageWithRetryCount.retrycount < totalRetryOfUploadingImage) {
        [self addImageToServer:numberOfRequest];
        imageWithRetryCount.retrycount++;
        return;
      }
    }

    float amountIncreasedOfProdgress =
        0.75 / [productObjectToUpload.productImagesOnDevices count];

    [self addUploadingProgress:amountIncreasedOfProdgress];
  } else if (requestId == requestAddDataToProductId) {
    // user is blocked
    if ([serverManagerResult.opertationResult
            isEqualToString:[ServerManager ERROR_USER_BLOCKED_FROM_ADMIN]]) {
      [DialogManager showErrorUserIsBlocked];
    }

    NSMutableArray *productItems = serverManagerResult.jsonArray;
 
    ProductObject *productObjectFromServer = [productItems objectAtIndex:0];
  
    productObjectToUpload.productId = productObjectFromServer.productId;
      

    if ([productObjectToUpload isUploadAdvertiseImages] == true) {
      [self addUploadingProgress:0.25];

      [self addImagesWithoudDataToServer];
    } else {
      [self addUploadingProgress:1];
        
        if(serverManagerResult.opertationMsg != nil){
            
            [DialogManager makeToastWithString:serverManagerResult.opertationMsg];
            
        }
    }
   
  }
}

- (void)addUploadingProgress:(float)progress {

  if (productObjectToUpload.productUploadingPercent == 0) {
    if (uploadingDelegate != nil) {
      [uploadingDelegate uploadingStarted];
    }
  }

  // get the index in uploading list
  NSMutableArray *productsUploadingList =
      [SharedGraphicInfo getcurrentUploadingProductList];

  if ([SharedData isAllowLog]) {
    /* NSLog(@"===================================================");
     NSLog(@"productUploadingList count is=%lu",(unsigned
     long)productsUploadingList.count); */
  }

  int productIndex =
      (int)[productsUploadingList indexOfObject:productObjectToUpload];

  // update uploading percent
  productObjectToUpload.productUploadingPercent += progress;

  if ([SharedData isAllowLog]) {
    /* NSLog(@"GridCell uploadingProgress
     is=%f",productObjectToUpload.productUploadingPercent);
     //update in list
     NSLog(@"updating the uploading list of product
     id=%p",productObjectToUpload);
     NSLog(@"==================================================="); */
  }

  if (productObjectToUpload.productUploadingPercent > 0.99) {
    if (uploadingDelegate != nil) {
      // release arrya of retry
      [productImagesRetryArray removeAllObjects];

      [uploadingDelegate uploadingFinished:productObjectToUpload];

      [[SharedGraphicInfo getcurrentUploadingProductList]
          removeObjectAtIndex:productIndex];
    }
  } else {
    if (uploadingDelegate != nil) {
      [uploadingDelegate uploadingProgressOfProduct:productObjectToUpload];
    }
  }
}
@end
