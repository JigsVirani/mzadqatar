//
//  NotificationViewController.h
//  Mzad Qatar
//
//  Created by Perveen Akhtar on 02/07/2014.
//  Copyright (c) 2014 Mohammed abdulfattah attia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ServerManager.h"
#import "MBProgressHUD.h"
#import "NotificationViewCell.h"
#import "DialogManager.h"
#import "GAITrackedViewController.h"

//// Delegate
@class NotificationViewController;
@protocol NotificationViewControllerDelegate <NSObject>
@required
- (void)receivedNewNotificiation:(NSString *)newNotificationCount withNotificationMsg:(NSString*)notificationMsg andProductAdvertiseId:(NSString*)productId andCategoryId:(NSString *)categoryId andIsBackendMessage:(BOOL) hassanMessage andSubCategoryId:(NSString *)subCategoryId andTabId:(NSString *)tabId;

@required
@end

#define requestGetInitialNotification @"requestGetInitialNotification"
#define requestGetMoreNotification @"requestGetMorNotification"
#define requestGetNewNotificationcount @"requestCheckNewNotification"
#define requestSetNotificationRead @"requestSetNotificationRead"
#define requestReadAllNotification @"requestReadAllNotification"
//#define requestDeleteNotification @"requestDeleteNotification"
//#define requestDeleteAllNotification @"requestDeleteAllNotification"
#define requeststopNotificationRequest @"stopNotificationRequest"


@interface NotificationViewController
: GAITrackedViewController <ServerManagerResponseDelegate, UITableViewDataSource,
UITableViewDelegate,ConfirmationDialogueViewControllerDelegate> {
    BOOL isOverlayShown;
    UIView *overlayView;
    UIActivityIndicatorView *activityIndicator;
    NSMutableArray *notificationArray;
    bool loadMoreWhereAvailable;
    NSTimer *timer;
    int selectedNotification;
}

+ (NotificationViewController *)getInstance;

- (void)startLoadingNotificationWithRequestId:(NSString *)requestId
                              andIsNewSession:(bool)isNewSession andNumberPerPage:(NSString*)numberPerPage ;

//-(void)startLoadingPushNotifications;

@property (weak, nonatomic) IBOutlet UILabel *NoNotificationLable;
@property(nonatomic) int pageNumber;
@property(nonatomic, strong) id<NotificationViewControllerDelegate> delegate;
//@property(nonatomic,strong) NSString *count;
@property(assign) int notificationCount;
@property(strong, nonatomic) IBOutlet UITableView *notificationTableView;
@property(nonatomic, strong) NSString *pageNumberCount;
- (void)refreshNowNotificationCount;
- (void)displayNotificationMessageOfFirebase:(NSString*) message andProductAdvertiseId:(NSString *)productId andCategoryId:(NSString *)categoryId andSubCategoryId:(NSString *)subCategoryId andTabId:(NSString *)tabId ;

@end
