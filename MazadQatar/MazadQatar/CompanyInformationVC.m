//
//  WHYViewController.m
//  Mzad Qatar
//
//  Created by GuruUgam on 7/28/16.
//  Copyright © 2016 Mohammed abdulfattah attia. All rights reserved.
//

#import "CompanyInformationVC.h"
#import "DialogManager.h"
#import "SystemUtility.h"
#import "CompanyDetailCell.h"
#import "CompanyRegistration.h"

@interface CompanyInformationVC ()<MFMailComposeViewControllerDelegate>
{
    IBOutlet UITableView *tblView;
    NSMutableArray *arryWhyList;
}
@end

@implementation CompanyInformationVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    
    self.screenName = @"Company Information Screen";
    //arryWhyList = [[NSMutableArray alloc]initWithObjects:@"Adding advertise in seperate list than personal users",@"Refresh ads every 3 days",@"Adding your company in companies directory",@"Giving priority for u to appear more in related ads",@"Showing ur ads in personal users list", nil];
    
    arryWhyList = [[NSMutableArray alloc]initWithObjects:NSLocalizedString(@"COMPANY_INFO_ONE", nil),
                   NSLocalizedString(@"COMPANY_INFO_TWO", nil),
                   NSLocalizedString(@"COMPANY_INFO_THREE", nil),
                   NSLocalizedString(@"COMPANY_INFO_FOUR", nil),
                   NSLocalizedString(@"COMPANY_INFO_FIVE", nil),@"", nil];
    // Do any additional setup after loading the view.
    //[self.navigationController.navigationBar.topItem setTitle:NSLocalizedString(@"BACK", nil)];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [arryWhyList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    CompanyDetailCell *cell;
    
    if(indexPath.row == 5){
        
        cell = (CompanyDetailCell *)[tableView dequeueReusableCellWithIdentifier:@"cellInfo" forIndexPath:indexPath];
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
        
        [cell.registerAsCompany addTarget:self action:@selector(registerAsCompanyBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
    
         return cell;
    }else{
        cell = (CompanyDetailCell *)[tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
        
        cell.lblNum.text = [NSString stringWithFormat:@"%d",indexPath.row+1];
        cell.lblTtitle.text = [NSString stringWithFormat:@"%@",[arryWhyList objectAtIndex:indexPath.row]];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        cell.lblLine.hidden = NO;
        cell.lblTtitle.textColor = [UIColor whiteColor];
    }
    [cell setBackgroundColor:[UIColor clearColor]];
    return cell;
}
- (IBAction)callBtnClicked:(UIButton *)sender {
    //[SystemUtility callNumber:@"33342920"];
    [SystemUtility callNumberWithCountryCode:@"00974" withNumber:@"33342920"];
}
- (IBAction)registerAsCompanyBtnClicked:(id)sender {
    
    
    CompanyRegistration  *companyRegistration = [[SharedData getCurrentStoryBoard]
                                                   instantiateViewControllerWithIdentifier:@"RegisterAsCompany"];
    
    [self.navigationController pushViewController:companyRegistration animated:YES];
    
}

#pragma mark : Email Button Click

- (IBAction)emailBtnClicked:(UIButton *)sender {
    // To address
    NSArray *toRecipents = [NSArray arrayWithObject:@"support@mzadqatar.com"];
    
    if ([MFMailComposeViewController canSendMail]) {
        
        MFMailComposeViewController *mc =
        [[MFMailComposeViewController alloc] init];
        [mc setToRecipients:toRecipents];
        mc.mailComposeDelegate = self;
        
        // Present mail view controller on screen
        [self presentViewController:mc animated:YES completion:NULL];
    } else {
        [DialogManager showErrorNoEmailAccountAvailable];
    }
}
#pragma mark -- Mail Composer Delegate
- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result {
    [self dismissViewControllerAnimated:YES completion:Nil];
}
- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error {
    // Close the Mail Interface
    [self dismissViewControllerAnimated:YES completion:NULL];
}

- (void)viewDidUnload {
    
    [super viewDidUnload];
}
@end
