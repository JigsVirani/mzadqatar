//
//  SubSubCategoriesChooser.h
//  Mzad Qatar
//
//  Created by samo on 9/2/14.
//  Copyright (c) 2014 Mohammed abdulfattah attia. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ChooseListWithSearch.h"
#import "CategoryObject.h"
#import "SharedData.h"
#import "SubcategoryTableCell.h"

// Delgate
@class SubSubCategoriesChooser;
@protocol SubSubCategoriesChooserDelegate <NSObject>
@required
- (void)subsubCategoryChoosedInList:(NSString *)subsubCategoryId
              andSubsubCategoryName:(NSString *)subsubCategoryName;
@required
@end

@interface SubSubCategoriesChooser
    : ChooseListWithSearch <UITableViewDataSource, UITableViewDelegate> {
  NSMutableArray *productItems;

  NSString *currentCategoryId;
  NSString *currentSubCategoryId;
  bool isOpenedFromAddAdvertise;
  NSString *currentSelectedSubSubCategoryId;
  id<SubSubCategoriesChooserDelegate> delegate;
}

- (void)setSubSubCategoryParamsWithCatId:(NSString *)categoryIdParam
                 andCurrentSubCategoryId:
                     (NSString *)currentSelectedsubCategoryIDParam
              andCurrentSubSubCategoryId:(NSString *)subSubCategoryIdParam
                andIsOpenedFormAdvertise:(BOOL)isOpenedFromAdvertiseParam
                             andDelegate:(id<SubSubCategoriesChooserDelegate>)
                                             delegateParam;

@end
