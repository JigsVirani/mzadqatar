//
//  UploadingProductsManager.h
//  MazadQatar
//
//  Created by samo on 3/31/13.
//  Copyright (c) 2013 Mohammed abdulfattah attia. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ServerManager.h"
#import "ServerManagerResult.h"
#import "SharedGraphicInfo.h"

// Delgate
@class UploadingProductsManager;

@protocol ProductsUploadingDelegate <NSObject>
@required
- (void)uploadingStarted;
- (void)uploadingProgressOfProduct:(ProductObject *)productObjectToUpload;
- (void)uploadingFinished:(ProductObject *)productObjectToUpload;
@required
@end

@interface UploadingProductsManager : NSObject {
  id<ProductsUploadingDelegate> uploadingDelegate;
  ProductObject *productObjectToUpload;
  NSMutableArray *productImagesRetryArray;
}
- (void)startProductObjectUploadloading:(ProductObject *)productToUpload;

- (void)addDataWithoutImagesToServer;
- (void)addImagesWithoudDataToServer;
- (void)addUploadingProgress:(float)progress;
- (void)uploadProduct;

@end
