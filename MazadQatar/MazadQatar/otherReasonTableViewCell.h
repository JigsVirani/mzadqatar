//
//  TableViewCell.h
//  Mzad Qatar
//
//  Created by Mohammed AbdulFattah on 1/11/15.
//  Copyright (c) 2015 Mohammed abdulfattah attia. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol OtherReasonTableViewCellDelegate <NSObject>
- (void)submitOtherReasonClicked:(NSString *)reasonIdParam
                   andReasonText:(NSString *)reasonTextParam;
@end

@interface OtherReasonTableViewCell : UITableViewCell <UITextViewDelegate>
@property(strong, nonatomic) IBOutlet UITextView *otherTextField;
@property(strong, nonatomic) IBOutlet UIButton *submitOtherReasonButton;
@property(nonatomic, strong) id<OtherReasonTableViewCellDelegate> delegate;

@end
