//
//  CategoryProducts.m
//  MazadQatar
//
//  Created by samo on 3/9/13.
//  Copyright (c) 2013 Mohammed abdulfattah attia. All rights reserved.
//

#import "CategoryProducts.h"
#import "ServerManager.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "DisplayUtility.h"
#import "ServerManagerResult.h"
#import "ProductDetailsViewController.h"
#import "DisplayUtility.h"
#import "SWRevealViewController.h"
#import "CitiesViewController.h"
#import "CategoryTypesBarCell.h"
#import "GAI.h"
#import "GAIDictionaryBuilder.h"
#import "SearchViewController.h"


@interface CategoryProducts () <ServerManagerResponseDelegate,GADInterstitialDelegate,CategoryProductsDelegate>

@property(nonatomic, strong) DFPInterstitial *interstitial;
@property(nonatomic, strong) NSString *language;
@end

static int categoryProductPageNumber = 1;
// static NSString* categoryProductLastUpdateTime= @"0";
static int categoryProductNumberOfPages = 50;
NSString *GRID_VIEW = @"grid";
NSString *LIST_VIEW = @"list";
NSString *COMPANY_VIEW = @"company";

NSString *CategoryTypesBarCellIdentifier = @"CategoryTypesBarCell";
NSString *sortNameAfterSearch;

NSString *FreeCellIdentifier = @"FreeCell";
NSString *PaidCellIdentifier = @"PaidCell";

NSString *companyFreeCellIdentifier = @"companyViewFreeCell";
NSString *companyPaidCellIdentifier = @"companyViewPaidCell";
// new

@implementation CategoryProducts

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"BACK", nil) style:UIBarButtonItemStylePlain target:nil action:nil];
    self.language = [LanguageManager currentLanguageCode];
    
    self.screenName = @"Category Products Screen";
    [[_AdvertisementButton layer] setBorderWidth:1.0f];
    [[_AdvertisementButton layer] setBorderColor:[UIColor whiteColor].CGColor];
    
    [self initVariables];
    [self startLoadingCategoryProductsWithRequestId:requestInitialCategoryProducts
                                    andIsNewSession:YES];
    
    self.btnAddCmpany.layer.masksToBounds = true;
    self.btnAddCmpany.layer.cornerRadius = 20.0;//self.btnAddCmpany.bounds.size.width/2;
    //Add DFPBannerView Manually into CollectionView And tableView
    
    UIView *headerView;
    DFPBannerView *bannerView;
    UIView *headerView1;
    DFPBannerView *bannerViewGrid;
    UIActivityIndicatorView *loadingIndicatorTbl = [[UIActivityIndicatorView alloc]init];
    UIActivityIndicatorView *loadingIndicatorCl = [[UIActivityIndicatorView alloc]init];
    
    if([DisplayUtility isPad]){
        [loadingIndicatorTbl setFrame:CGRectMake(384, 40, 20, 20)];
        [loadingIndicatorCl setFrame:CGRectMake(384, 40, 20, 20)];
        
        headerView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 100)];
        bannerView = [[DFPBannerView alloc]initWithFrame:CGRectMake(20, 5, headerView.frame.size.width-40, 90)];
        
        headerView1 = [[UIView alloc]initWithFrame:CGRectMake(0,-100, self.view.frame.size.width, 100)];
        bannerViewGrid = [[DFPBannerView alloc]initWithFrame:CGRectMake(20, 5, headerView1.frame.size.width-40, 90)];
        
        if([self.language isEqualToString:[ServerManager LANGUAGE_ARABIC]]) {
            bannerView.adUnitID = @"/271990408/ProductPageiOSTabletAppTopBannerArabic";
            bannerViewGrid.adUnitID = @"/271990408/ProductPageiOSTabletAppTopBannerArabic";
        }else{
            
            bannerView.adUnitID = @"/271990408/ProductPageiOSTabletAppTopBannerEnglish";
            bannerViewGrid.adUnitID = @"/271990408/ProductPageiOSTabletAppTopBannerEnglish";
        }
        [self.collectionViewGridView setContentInset:UIEdgeInsetsMake(100, 0, 0, 0)];
    }else{
        
        [loadingIndicatorTbl setFrame:CGRectMake(self.view.center.x-10, 25, 20, 20)];
        [loadingIndicatorCl setFrame:CGRectMake(self.view.center.x-10, 25, 20, 20)];
        
        headerView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 60)];
        bannerView = [[DFPBannerView alloc]initWithFrame:CGRectMake(self.view.center.x-160, 5, 320, 50)];
        
        headerView1 = [[UIView alloc]initWithFrame:CGRectMake(0,-55, self.view.frame.size.width, 55)];
        bannerViewGrid = [[DFPBannerView alloc]initWithFrame:CGRectMake(self.view.center.x-160, 5, 320, 50)];
        
        [self.collectionViewGridView setContentInset:UIEdgeInsetsMake(65, 0, 0, 0)];
        
        if([self.language isEqualToString:[ServerManager LANGUAGE_ARABIC]]) {
            bannerView.adUnitID = @"/271990408/ProductPageiOSMobileAppTopBannerArabic";
            bannerViewGrid.adUnitID = @"/271990408/ProductPageiOSMobileAppTopBannerArabic";
        }else{
            bannerView.adUnitID = @"/271990408/ProductPageiOSMobileAppTopBannerEnglish";
            bannerViewGrid.adUnitID = @"/271990408/ProductPageiOSMobileAppTopBannerEnglish";
        }
    }
    
    bannerView.adSize = kGADAdSizeSmartBannerPortrait;
    bannerView.rootViewController = self;
    [bannerView loadRequest:[DFPRequest request]];
    [headerView addSubview:loadingIndicatorTbl];
    [loadingIndicatorTbl startAnimating];
    [headerView addSubview:bannerView];
    self.tableView.tableHeaderView = headerView;
    
    bannerViewGrid.adSize = kGADAdSizeSmartBannerPortrait;
    bannerViewGrid.rootViewController = self;
    [bannerViewGrid loadRequest:[DFPRequest request]];
    [headerView1 addSubview:loadingIndicatorCl];
    [loadingIndicatorCl startAnimating];
    [headerView1 addSubview:bannerViewGrid];
    [self.collectionViewGridView addSubview:headerView1];
    
    self.companyTableView.estimatedRowHeight = 70;
    self.companyTableView.rowHeight = UITableViewAutomaticDimension;
    
    //[headerView setBackgroundColor:[UIColor orangeColor]];
    //[bannerViewGrid setBackgroundColor:[UIColor orangeColor]];
    
    /*NSDate *date = [NSDate date];
    if(![[NSUserDefaults standardUserDefaults]objectForKey:@"CurrentDateProductScreen"]){
        [self loadInterstitialAd];
        [appDelegate Add24HoursToDate:date withKey:@"24HoursDateProductScreen" withCurrentDateKey:@"CurrentDateProductScreen"];
    }else{
        if([[appDelegate.dateFormatter dateFromString:[appDelegate.dateFormatter stringFromDate:date]]compare:[[NSUserDefaults standardUserDefaults]objectForKey:@"24HoursDateProductScreen"]]==NSOrderedDescending){
            [self loadInterstitialAd];
            [appDelegate Add24HoursToDate:[[NSUserDefaults standardUserDefaults]objectForKey:@"24HoursDateProductScreen"] withKey:@"24HoursDateProductScreen" withCurrentDateKey:@"CurrentDateProductScreen"];
        }
    }*/
    [self loadInterstitialAd];
    
    /** Call Function To change layout **/
    [self setLayoutForIpad];
    //[self.navigationController.navigationBar.topItem setTitle:NSLocalizedString(@"BACK", nil)];
}

- (void) setLayoutForIpad
{
    if (IS_IPAD) {
        _topBarHeightConstant.constant = 60.0;
        _bottomBarHeightConstant.constant = 60.0;
        _filterBtnDownMenu.titleLabel.font = [UIFont systemFontOfSize:22.0];
        _subcategoryBtnDownMenu.titleLabel.font = [UIFont systemFontOfSize:22.0];
        _sortByBtnDownMenu.titleLabel.font = [UIFont systemFontOfSize:22.0];
        _selectedViewType.titleLabel.font = [UIFont systemFontOfSize:22.0];
    }
}
-(void)loadInterstitialAd{
    if([DisplayUtility isPad]){
        if([self.language isEqualToString:[ServerManager LANGUAGE_ARABIC]]) {
            self.interstitial = [[DFPInterstitial alloc] initWithAdUnitID:@"/271990408/ProductPageiOSTabletAppFullPageArabic"];
        }else{
            self.interstitial = [[DFPInterstitial alloc] initWithAdUnitID:@"/271990408/ProductPageiOSTabletAppFullPageEnglish"];
        }
    }else{
        if([self.language isEqualToString:[ServerManager LANGUAGE_ARABIC]]) {
            self.interstitial = [[DFPInterstitial alloc] initWithAdUnitID:@"/271990408/ProductPageiOSMobileAppFullPageArabic"];
        }else{
            self.interstitial = [[DFPInterstitial alloc] initWithAdUnitID:@"/271990408/ProductPageiOSMobileAppFullPageEnglish"];
        }
    }
    self.interstitial.delegate = self;
    [self.interstitial loadRequest:[DFPRequest request]];
}
#pragma mark - interstitialDelegates
// Called when an interstitial ad request succeeded.
- (void)interstitialDidReceiveAd:(DFPInterstitial *)ad {
    NSLog(@"interstitialDidReceiveAd");
    [self.interstitial presentFromRootViewController:self];
}
// Called when an interstitial ad request failed.
- (void)interstitial:(DFPInterstitial *)ad didFailToReceiveAdWithError:(GADRequestError *)error {
    NSLog(@"interstitial:didFailToReceiveAdWithError: %@", [error localizedDescription]);
}
// Called just before presenting an interstitial.
- (void)interstitialWillPresentScreen:(DFPInterstitial *)ad {
    NSLog(@"interstitialWillPresentScreen");
}
// Called before the interstitial is to be animated off the screen.
- (void)interstitialWillDismissScreen:(DFPInterstitial *)ad {
    NSLog(@"interstitialWillDismissScreen");
}
// Called just after dismissing an interstitial and it has animated off the screen.
- (void)interstitialDidDismissScreen:(DFPInterstitial *)ad {
    NSLog(@"interstitialDidDismissScreen");
}
// Called just before the app will background or terminate because the user clicked on an
// ad that will launch another app (such as the App Store).
- (void)interstitialWillLeaveApplication:(DFPInterstitial *)ad {
    NSLog(@"interstitialWillLeaveApplication");
}


//-(void) setButtonTextLablesProperties
//{
//    _sortByBtnDownMenu.titleLabel.numberOfLines = 1;
//    _sortByBtnDownMenu.titleLabel.adjustsFontSizeToFitWidth = YES;
//    [_sortByBtnDownMenu.titleLabel setTextAlignment:NSTextAlignmentCenter];
//    _sortByBtnDownMenu.titleLabel.lineBreakMode = NSLineBreakByClipping;
//    _subcategoryBtnDownMenu.titleLabel.numberOfLines = 1;
//    _subcategoryBtnDownMenu.titleLabel.adjustsFontSizeToFitWidth = YES;
//    _subcategoryBtnDownMenu.titleLabel.lineBreakMode = NSLineBreakByClipping;
//    [_subcategoryBtnDownMenu.titleLabel setTextAlignment:NSTextAlignmentCenter];
//}

- (void)initVariables {
    
    self.tableView.tableFooterView =
    [[UIView alloc] initWithFrame:CGRectZero];
    
    self.companyTableView.tableFooterView =
    [[UIView alloc] initWithFrame:CGRectZero];
    
    isCatgoryProductScreen = true;
    
    // init array that will cacehe tabs arrays
    tabsProductList = [[NSMutableArray alloc] init];
    [self resetArrayOfProducts];
    //    for (int i = 0; i < _tabsProductList.count; i++) {
    //        [tabsProductList addObject:[[NSMutableArray alloc] init]];
    //    }
    
    if (self.searchString == nil) {
        self.searchString = @"";
    }
    
    
    if ([categoryProductObject.productSubCategoryId isEqualToString:@"0"]) {
        categoryProductObject.productSubCategoryName =
        NSLocalizedString(@"ALL_SUBCATEGORIES", Nil);
    }
    else if ([categoryProductObject.productSubCategoryId isEqualToString:@""] ||
             categoryProductObject.productSubCategoryId == nil) {
        categoryProductObject.productSubCategoryId = @"0";
        categoryProductObject.productSubCategoryName =
        NSLocalizedString(@"ALL_SUBCATEGORIES", Nil);
    }
    else
    {
        [self.subcategoryBtnDownMenu setTitle:categoryProductObject.productSubCategoryName forState:UIControlStateNormal];
//        [self.subcategoryBtnDownMenu setBackgroundColor:[UIColor colorWithRed:82/255.0 green:82/255.0 blue:82/255.0 alpha:1.0]];
//        [self.filterBtnDownMenu setBackgroundColor:[UIColor colorWithRed:82/255.0 green:82/255.0 blue:82/255.0 alpha:1.0]];
//        [self.sortByBtnDownMenu setBackgroundColor:[UIColor colorWithRed:82/255.0 green:82/255.0 blue:82/255.0 alpha:1.0]];
//        [self.sortByBtnDownMenu setTitle:sortNameAfterSearch forState:UIControlStateNormal];
    }
    if ([self.soryById isEqualToString:@""] || self.soryById == nil) {
        self.soryById = @"0";
        self.soryByName = NSLocalizedString(@"SORT_BY_LATEST", Nil);
    }
    
    if([categoryProductObject.productCategoryId isEqualToString:@"0"])
    {
//        [self.filterBtnDownMenu setBackgroundColor:[UIColor colorWithRed:78/255.0 green:1/255.0 blue:37/255.0 alpha:1.0]];
        [self.filterBtnDownMenu setTitle:NSLocalizedString(@"All_FILTERS", nil) forState:UIControlStateNormal];
        [self.filterBtnDownMenu setUserInteractionEnabled:false];
        
//        [self.subcategoryBtnDownMenu setBackgroundColor:[UIColor colorWithRed:78/255.0 green:1/255.0 blue:37/255.0 alpha:1.0]];
        [self.subcategoryBtnDownMenu setTitle:NSLocalizedString(@"ALL_SUBCATEGORIES", nil) forState:UIControlStateNormal];
        [self.subcategoryBtnDownMenu setUserInteractionEnabled:false];
        
    }
    
    self.productsLanguage =
    [UserSetting getAdvertiseTypeServerParam:
     [UserSetting getUserFilterViewAdvertiseLanguage]];
    
    loadMoreWhereAvailable = YES;
    
    ipadCellWidth = 242;
    iphoneCellWidth = floor(([UIScreen mainScreen].bounds.size.width-20)/2.0);
    
    [self calculateNumberOfAdvertiseBasedOnRowCount];
    
    [self addRefreshControlToGrid];
        
//    UIImage *backgroundPattern = [UIImage imageNamed:@"bg.png"];
//    [self.tableView
//     setBackgroundColor:[UIColor colorWithPatternImage:backgroundPattern]];
    
    // init the grid or list initial view
    AdvertiseType *tabBarName = [_tabBarNames objectAtIndex:self.currentCategoryTypeBarIndex];
    //AdvertiseType *tabBarName = [_tabBarNames objectAtIndex:0];
    
    categoryProductObject.productAdvertiseTypeId = tabBarName.advertiseTypeId;
    
    self.navigationController.interactivePopGestureRecognizer.delegate = self;
    
    
    if([[SharedData getDeviceLanguage]isEqualToString:@"ar"]){
        ///////////////Add Left Swipe Gesture
        
        UISwipeGestureRecognizer *swipeleft =
        [[UISwipeGestureRecognizer alloc] initWithTarget:self
                                                  action:@selector(swipeleft:)];
        swipeleft.direction = UISwipeGestureRecognizerDirectionRight;
        [self.view addGestureRecognizer:swipeleft];
        
        /////////////Add Right Swipe Gesture
        UISwipeGestureRecognizer *swiperight =
        [[UISwipeGestureRecognizer alloc] initWithTarget:self
                                                  action:@selector(swiperight:)];
        swiperight.direction = UISwipeGestureRecognizerDirectionLeft;
        [self.view addGestureRecognizer:swiperight];
    }else
    {
        ///////////////Add Left Swipe Gesture
        
        UISwipeGestureRecognizer *swipeleft =
        [[UISwipeGestureRecognizer alloc] initWithTarget:self
                                                  action:@selector(swipeleft:)];
        swipeleft.direction = UISwipeGestureRecognizerDirectionLeft;
        [self.view addGestureRecognizer:swipeleft];
        
        /////////////Add Right Swipe Gesture
        UISwipeGestureRecognizer *swiperight =
        [[UISwipeGestureRecognizer alloc] initWithTarget:self
                                                  action:@selector(swiperight:)];
        swiperight.direction = UISwipeGestureRecognizerDirectionRight;
        [self.view addGestureRecognizer:swiperight];
    }
    
    
    //init the collection view tab current index
    //[self resetCategoriesTabCollectionViewInCategoryTypeId:categoryProductObject.productCategoryId andIndex:self.currentCategoryTypeBarIndex];
    [self resetCategoriesTabCollectionViewInCategoryTypeId:[[self.tabBarNames
                                                             objectAtIndex:self.currentCategoryTypeBarIndex] advertiseTypeId] andIndex:self.currentCategoryTypeBarIndex];
    
    [self setCategoryAdvertiseType: tabBarName];
  
    
    
}

- (void)viewWillAppear:(BOOL)animated {
    // i=0;
    [super viewWillAppear:YES];
    //[self displayTab];
}

#pragma mark -
#pragma mark - UIButton Action

- (IBAction)btnSearchAction:(id)sender
{
    BOOL isExist = FALSE;
    for (UIViewController *vc in self.navigationController.viewControllers) {
        if ([vc isKindOfClass:[SearchViewController class]]) {
            //It exists
            isExist = TRUE;
            break;
        }
    }
    
    if (isExist) {
        [self.navigationController popViewControllerAnimated:YES];
    }else{
        [self performSegueWithIdentifier:@"segueSearchCategoryProduct" sender:nil];
    }
}

- (IBAction)btnBackAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:TRUE];
}

- (void)refreshProductListPage:(NSString *)strTitle withCategoryId:(NSString *)subCategoryId withCategoryId:(NSString *)strCategoryId withTypeId:(NSString *)strTypeId
{
    
    /*[self setCategoryProductsCategoryId:strCategoryId
     andSubcategoryId:strCategoryId
     andSubcategoryName:@""];
     [self setSearchString:strTitle];
     [self setSoryById:@""];
     [self setSoryByName:@""];
     [self setSortByIndex:-1];
     [self setTabBarNames: [self getTabBarSearchAllCategories:strTypeId]];
     [self setCurrentCategoryTypeBarIndex:0];*/
    
    self.currentCategoryTypeBarIndex = [strTypeId intValue];
    [self btnCategoryTypeNameClickedWithId:[[self.tabBarNames objectAtIndex:self.currentCategoryTypeBarIndex] advertiseTypeId]andIndex:self.currentCategoryTypeBarIndex];
    //[self getNewData:strTitle];
}

-(NSMutableArray*)getTabBarSearchAllCategories:(NSString *)strTypeId
{
    AdvertiseType *allTypes=[[AdvertiseType alloc]init];
    allTypes.advertiseTypeId=strTypeId;
    allTypes.advertiseTypeName=NSLocalizedString(@"ALL_TYPES", nil);
    allTypes.advertiseTypeDefaultView=@"grid";
    
    return  [NSMutableArray arrayWithObject:allTypes];
}

#pragma mark - category types bar collection view
- (NSInteger)collectionView:(UICollectionView *)collectionView
     numberOfItemsInSection:(NSInteger)section {
    if ([collectionView.restorationIdentifier
         isEqualToString:@"categoryTypesRestorationId"]) {
        return [_tabBarNames count];
    } else {
        return [super collectionView:collectionView numberOfItemsInSection:section];
    }
}

// The cell that is returned must be retrieved from a call to
// -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView
                  cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    if ([collectionView.restorationIdentifier
         isEqualToString:@"categoryTypesRestorationId"]) {
        CategoryTypesBarCell *cell = [collectionView
                                      dequeueReusableCellWithReuseIdentifier:CategoryTypesBarCellIdentifier
                                      forIndexPath:indexPath];
        
        AdvertiseType *advertiseType =
        [self.tabBarNames objectAtIndex:indexPath.row];
        
        NSString *tittle = [NSString stringWithFormat:@"%@",advertiseType.advertiseTypeName];
        [cell.btnCategoryTypeName setTitle:tittle
                                  forState:UIControlStateNormal];
        [cell setAdvertiseType:advertiseType];
        [cell setCategoryTypesBarCellDelegate:self];
        [cell setTabIndex:indexPath.row];
        
        if (categoryProductObject.productAdvertiseTypeId ==
            advertiseType.advertiseTypeId) {
            [cell.selectedTypeBarImage setHidden:NO];
            self.currentCategoryTypeBarIndex = (int)indexPath.row;
        } else {
            [cell.selectedTypeBarImage setHidden:YES];
        }
        
        return cell;
    } else {
        return
        [super collectionView:collectionView cellForItemAtIndexPath:indexPath];
    }
}

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    if (_categoryTypesCollectionView == collectionView) {
        if ([DisplayUtility isPad]) {
            return CGSizeMake([UIScreen mainScreen].bounds.size.width/3.0, 60.0);
        }
    }
    
    if ([collectionView.restorationIdentifier
         isEqualToString:@"categoryTypesRestorationId"]) {
        AdvertiseType *advertiseType =
        [self.tabBarNames objectAtIndex:indexPath.row];
        
        NSDictionary *attributes =
        @{NSFontAttributeName : [UIFont systemFontOfSize:18.0]};
        // NSString class method: boundingRectWithSize:options:attributes:context is
        // available only on ios7.0 sdk.
        CGRect rect = [advertiseType.advertiseTypeName
                       boundingRectWithSize:CGSizeMake(CGFLOAT_MAX, 40)
                       options:NSStringDrawingUsesLineFragmentOrigin
                       attributes:attributes
                       context:nil];
        
        // minm 106 width
        if ([DisplayUtility isPad]) {
            if (rect.size.width < 200) {
                return CGSizeMake(200, 40);
            }
        } else {
            if (rect.size.width < 106) {
                return CGSizeMake(106, 40);
            }
        }
        return CGSizeMake(rect.size.width + 20, 40);
    } else {
        
        ProductObject *productObject = [productItems objectAtIndex:indexPath.row];
        if (productObject.isAd) {
            if ([DisplayUtility isPad]) {
                return CGSizeMake(768, 275);
            } else {
                return CGSizeMake(300, 275);
            }
        } else {
            if ([DisplayUtility isPad]) {
                return CGSizeMake(ipadCellWidth, 323);
            } else {
                //return CGSizeMake(iphoneCellWidth, 260*([UIScreen mainScreen].bounds.size.width-20)/320.0);
                return CGSizeMake(iphoneCellWidth, iphoneCellWidth + 80);
            }
        }
    }
}

- (void)btnCategoryTypeNameClickedWithId:(NSString *)categoryTypeID
                                andIndex:(int)tabIndex;
{
    self.collectionViewGridView.hidden = YES;
    self.tableView.hidden = YES;
    self.companyTableView.hidden = YES;
    [self resetCategoriesTabCollectionViewInCategoryTypeId:categoryTypeID andIndex:tabIndex];
    
    [self loadFromLocalOrServer];
}

-(void)resetCategoriesTabCollectionViewInCategoryTypeId:(NSString *)categoryTypeID andIndex:(int)tabIndex
{
    //stop any scroll;
    [self.tableView setContentOffset:self.tableView.contentOffset animated:NO];
    
    [self.companyTableView setContentOffset:self.tableView.contentOffset animated:NO];
    
    [self.collectionViewGridView setContentOffset:self.collectionViewGridView.contentOffset animated:NO];

    
   
    
    
    AdvertiseType *tabBarName = [_tabBarNames objectAtIndex:tabIndex];
    
    // set the default view based on server parap
    [self setCategoryAdvertiseType: tabBarName];
    
    
    self.currentCategoryTypeBarIndex = tabIndex;
    
    categoryProductObject.productAdvertiseTypeId = categoryTypeID;
    
    [self.categoryTypesCollectionView reloadData];
    [self performSelector:@selector(displayTab) withObject:self afterDelay:1.0];
}
- (void)displayTab
{
    //scroll to selected tab
    [self.categoryTypesCollectionView
     scrollToItemAtIndexPath:
     [NSIndexPath indexPathForRow:self.currentCategoryTypeBarIndex inSection:0]
     atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally
     animated:NO];
}
- (void) setCategoryAdvertiseType: (AdvertiseType *) tabBarName{
    
    self.btnAddCmpany.hidden = true;
    if([tabBarName.advertiseTypeDefaultView isEqualToString:GRID_VIEW]){
        
        categoryAdvertiseType = GRID;
        
        [self showGrid];
    }else if([tabBarName.advertiseTypeDefaultView isEqualToString:LIST_VIEW]){
        
       categoryAdvertiseType = LIST;
        
       [self showlist];
    }
    else{
        self.btnAddCmpany.hidden = false;
        categoryAdvertiseType = COMPANY;
        
        [self showCompany];
    }
}

- (void)loadFromLocalOrServer {
    
    
    if ([[tabsProductList objectAtIndex:self.currentCategoryTypeBarIndex] count] > 0) {
        productItems = [tabsProductList objectAtIndex:self.currentCategoryTypeBarIndex];
        
        if (productItems.count == 0) {
            if(categoryAdvertiseType == COMPANY){
                [self addLabelToMiddleWithText:NSLocalizedString(@"NO_COMPANY_AVAILABLE", nil)
                              inCollectionView:NO];
            }else
            {
            [self addLabelToMiddleWithText:NSLocalizedString(@"NO_ADS_AVAILABLE", nil)
                          inCollectionView:NO];
            }
        } else {
            [self removeLabelfromMiddleOfView];
        }
        
        [self reloadListOrGrid:requestInitialCategoryProducts];
        
    } else {
        isPullNewData = true;
        [self startLoadingCategoryProductsWithRequestId:requestInitialCategoryProducts andIsNewSession:YES];
        isPullNewData = false;
    }
}

-(void)reloadListOrGrid:(NSString*)requestId
{
    dispatch_async(dispatch_get_main_queue(), ^{
        
        
        switch (categoryAdvertiseType) {
            case COMPANY:
                //scroll to top
                if([requestId isEqualToString:requestInitialCategoryProducts])
                {
//                    [self.companyTableView setContentOffset:
//                     
//                     CGPointMake(0, -self.collectionViewGridView.contentInset.top) animated:NO];
                    
                    [self.companyTableView setContentOffset:
                     CGPointMake(0, -self.companyTableView.contentInset.top) animated:NO];
                    
                }
                
                [self.companyTableView reloadData];
                break;
            case LIST:
                if([requestId isEqualToString:requestInitialCategoryProducts])
                {
//                    [self.tableView setContentOffset:
//                     CGPointMake(0, -self.collectionViewGridView.contentInset.top) animated:NO];
                    [self.tableView setContentOffset:
                                          CGPointMake(0, -self.tableView.contentInset.top) animated:NO];
                }
                
                [self.tableView reloadData];
                break;
            case GRID:
                //scroll to top
                if([requestId isEqualToString:requestInitialCategoryProducts])
                {
                    [self.collectionViewGridView setContentOffset:
                     CGPointMake(0, -self.collectionViewGridView.contentInset.top) animated:NO];
                }
                [self.collectionViewGridView reloadData];
             
                break;
                
            default:
                break;
        }
    
      
    });
}

#pragma mark - Swipe Gesture
- (void)swipeleft:(UISwipeGestureRecognizer *)gestureRecognizer {
    ////Don't Scroll if page number is equal to scrollview last button
    if (self.currentCategoryTypeBarIndex == _tabBarNames.count - 1)
    {
        return;
    }
    self.currentCategoryTypeBarIndex++;
    //    pageNumber++;
    //    [self swipeLeftRight];
    check = NO;
    [self btnCategoryTypeNameClickedWithId:
     [[self.tabBarNames
       objectAtIndex:self.currentCategoryTypeBarIndex] advertiseTypeId]
                                  andIndex:self.currentCategoryTypeBarIndex];
}

- (void)swiperight:(UISwipeGestureRecognizer *)gestureRecognizer {
    ////Don't move view if you are on the first page
    if (self.currentCategoryTypeBarIndex == 0)
        return;
    
    self.currentCategoryTypeBarIndex--;
    
    [self.categoryTypesCollectionView
     scrollToItemAtIndexPath:
     [NSIndexPath indexPathForRow:self.currentCategoryTypeBarIndex inSection:0]
     atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally
     animated:NO];
    check = NO;
    [self btnCategoryTypeNameClickedWithId:
     [[self.tabBarNames
       objectAtIndex:self.currentCategoryTypeBarIndex] advertiseTypeId]
                                  andIndex:self.currentCategoryTypeBarIndex];
}


#pragma mark - WebService functions

- (void)calculateNumberOfAdvertiseBasedOnRowCount {
    // we need advertises to fill all the row ,so we must send the count right ,
    // we calculate it by dividing number of row by current count per page , then
    // we multiply it by row count we get the right total.
    int numberToMultiplyByRowCount =
    ceil((double)categoryProductNumberOfPages /
         (double)[SharedData numberOfAdvertiseInOneRow]);
    categoryProductNumberOfPages =
    [SharedData numberOfAdvertiseInOneRow] * numberToMultiplyByRowCount;
}

- (void)requestFinished:(ServerManagerResult *)serverManagerResult
            ofRequestId:(NSString *)requestId
    withNumberOfRequest:(int)numberOfRequest {
        
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    if ([serverManagerResult.opertationResult
         isEqualToString:[ServerManager ERROR_IN_CONNECTION_OPERATION]]) {
        [DialogManager showErrorInConnection];
        
    }
    
    if ([requestId isEqual:requestInitialCategoryProducts] ||
        [requestId isEqual:requestMoreCategoryProducts] ||
        [requestId isEqual:requestRefreshCategoryProducts]) {
        if (refreshControl.isRefreshing) {
            [refreshControl endRefreshing];
        }
        
        // error in server
        if ([serverManagerResult.opertationResult
             isEqualToString:[ServerManager FAILED_OPERATION]]) {
            //Comment [DialogManager showErrorInServer];
            [DialogManager showErrorWithMessage:serverManagerResult.opertationMsg];
            return;
        }
        
        if ([requestId isEqual:requestInitialCategoryProducts] ||
            [requestId isEqual:requestRefreshCategoryProducts]) {
            loading = NO;
            
       
            
            if(serverManagerResult.jsonArray !=nil)
            {
                [SharedData
                 setCategoryDataLastUpdateTime:serverManagerResult.lastUpdateTime];
                
                [tabsProductList replaceObjectAtIndex:numberOfRequest
                                           withObject:serverManagerResult.jsonArray];
                productItems = tabsProductList[numberOfRequest];
            }
            
           // [[NotificationViewController getInstance] refreshNowNotificationCount];
            
        } else if ([requestId isEqual:requestMoreCategoryProducts]) {
            loading = NO;
            
            if (serverManagerResult.jsonArray.count > 0) {
                [[tabsProductList objectAtIndex:numberOfRequest]
                 addObjectsFromArray:serverManagerResult.jsonArray];
                
                productItems =
                [tabsProductList objectAtIndex:numberOfRequest];
            } else {
                loadMoreWhereAvailable = NO;
            }
        }
        
//        if (![requestId isEqual:requestRefreshCategoryProducts]) {
//            categoryProductPageNumber++;
//        }
        
        //#FORWARIS : Why repeated ?
        if (productItems.count == 0) {
            if(categoryAdvertiseType == COMPANY){
                [self addLabelToMiddleWithText:NSLocalizedString(@"NO_COMPANY_AVAILABLE", nil)
                              inCollectionView:NO];
            }else
            {
                [self addLabelToMiddleWithText:NSLocalizedString(@"NO_ADS_AVAILABLE", nil)
                              inCollectionView:NO];
            }
            
        } else {
            [self removeLabelfromMiddleOfView];
        }
        // if search from category screen we remove the bar
        //        if (self.isSearchOpenedFromCategoryScreen == true) {
        //            // isHideBar=true;
        //        }
        [self reloadListOrGrid:requestId];
    }
}

- (void)loadData:(NSNumber *)isNewSession {
    [self startLoadingCategoryProductsWithRequestId:requestMoreCategoryProducts
                                    andIsNewSession:NO];
}

- (IBAction)btnchangeView:(UIButton *)sender {
    switch (categoryAdvertiseType) {
        case COMPANY:
            
            [self showCompany];
            
            break;
        case LIST:
            [self showGrid];
            break;
        case GRID:
            //scroll to top
            
            [self showlist];
            break;
            
        default:
            break;
    }

}
- (void)showCompany {
    
    categoryAdvertiseType = COMPANY;
    self.collectionViewGridView.hidden = YES;
    self.tableView.hidden = YES;
    self.companyTableView.hidden = NO;
    [self.companyTableView reloadData];
    
    [self.selectedViewType setTitle:NSLocalizedString(@"COMPANY_TITLE", nil)
                           forState:UIControlStateNormal];
    
    UIImage *img = [UIImage imageNamed:@"grid_icon.png"];
    self.gridOrListImage.image = img;
}

- (void)showGrid {
    
    self.companyTableView.hidden = YES;
    self.tableView.hidden = YES;
    self.collectionViewGridView.hidden = NO;
    
    if (check == NO) {
        categoryAdvertiseType = GRID;
        
        [self.collectionViewGridView reloadData];
        
        [self.selectedViewType setTitle:NSLocalizedString(@"LIST_TITLE", nil)
                               forState:UIControlStateNormal];
        
        UIImage *img = [UIImage imageNamed:@"list_icon.png"];
        
        // [img setFrame:CGRectMake(270, 454, 17, 19)];
        self.gridOrListImage.image = img;
    }
}

- (void)showlist {
    
    self.collectionViewGridView.hidden = YES;
    self.companyTableView.hidden = YES;
    self.tableView.hidden = NO;
    
    if (check == NO) {
        categoryAdvertiseType = LIST;
        
        [self.tableView reloadData];
        
        [self.selectedViewType setTitle:NSLocalizedString(@"GRID_TITLE", nil)
                               forState:UIControlStateNormal];
        
        UIImage *img = [UIImage imageNamed:@"grid_icon.png"];
        
        self.gridOrListImage.image = img;
    }
}
-(void)scrollViewDidScroll:(UIScrollView *)sender
{
    check = YES;
    [self performSelector:@selector(scrollViewDidEndScrollingAnimation:) withObject:nil afterDelay:0.3];
}
-(void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView
{
    check = NO;
}
- (void)startLoadingCategoryProductsWithRequestId:(NSString *)requestId
                                  andIsNewSession:(bool)isNewSession {
    
    loading = YES;
    
    if ([requestId isEqual:requestInitialCategoryProducts]) {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [hud setLabelText:NSLocalizedString(@"LOADING_CATEGORYPRODUCTS", Nil)];
        
        //[self showOverlayWithIndicator:true];
        
        // when switching between tabs and there is 100 item loaded , all items are
        // gone because we load only 50
        //        if([self.productItems count]!=0&&isPullNewData==false)
        //        {
        //            [self refreshCurrentGrid];
        //
        //            return;
        //        }
        
        loadMoreWhereAvailable = true;
        
        // reset it for next call
        isPullNewData = false;
        
        // categoryProductLastUpdateTime=0;
        [SharedData setCategoryDataLastUpdateTime:@"0"];
        
        // [self showOverlayWithIndicator:true];
        
        categoryProductPageNumber = 1;
    }
    else
    {
        categoryProductPageNumber++;
        //categoryProductPageNumber=ceilf((double)((float)self.productItems.count/(float)categoryProductNumberOfPages))+1;
    }
        
    [ServerManager
     getCategoryProductsItems:categoryProductObject.productCategoryId
     withDelegate:self
     withNewSession:isNewSession
     withTypeOfUpdate:
     [ServerManager GET_UPDATES_BEFORE_LAST_UPDATE_TIME]
     withCategoryProductPageNumber:categoryProductPageNumber
     withCategoryProductLastUpdateTime:[SharedData
                                        getCategoryDataLastUpdateTime]
     withCategoryProductNumberOfPages:categoryProductNumberOfPages
     withSearchStr:self.searchString
     withRequestId:requestId
     withAdvertiseLangType:self.productsLanguage
     withSubCategoryId:categoryProductObject
     .productSubCategoryId
     withSortById:self.soryById
     withAdvertiseLanguage:
     [UserSetting getAdvertiseLanguageServerParam:[UserSetting getUserFilterViewAdvertiseLanguage]]
     isAdSupported:@"true"
     countOfRows:self.advertiseInOneRowCountString
     withAdvertiseResolution:self.advertiseResolution
     withCategoryAdvertiseTypeId:(categoryProductObject.productAdvertiseTypeId != nil ? categoryProductObject.productAdvertiseTypeId : @"0")
     withCityId:categoryProductObject.productCityId
     withRegionId:categoryProductObject.productRegionId
     withSubSubCategoryId:categoryProductObject
     .productSubsubCategoryId
     withManfactureYearFrom:categoryProductObject
     .productManfactureYearFrom
     withManfactureTo:categoryProductObject
     .productManfactureYearTo
     withKmFrom:categoryProductObject.productKmFrom
     withKmTo:categoryProductObject.productKmTo
     withNumberOfRoomFrom:categoryProductObject
     .productNumberOfRoomsFrom
     withNumberOfRoomsTo:categoryProductObject
     .productNumberOfRoomsTo
     withPriceFrom:categoryProductObject.productPriceFrom
     withPriceTo:categoryProductObject.productPriceTo withFurnishedTypeId:categoryProductObject.productFurnishedTypeId
     andNumberOfRequest:self.currentCategoryTypeBarIndex];
}

//-(void)refreshCurrentGrid
//{
//    [ServerManager getCategoryProductsItems:productId withDelegate:self
//    withNewSession:NO withTypeOfUpdate:[ServerManager
//    GET_UPDATES_BEFORE_LAST_UPDATE_TIME] withCategoryProductPageNumber:1
//    withCategoryProductLastUpdateTime:@"0"
//    withCategoryProductNumberOfPages:(categoryProductPageNumber-1)*categoryProductNumberOfPages
//    withSearchStr:self.searchString
//    withRequestId:requestRefreshCategoryProducts
//    withAdvertiseLangType:self.productsLanguage
//    withSubCategoryId:productsSubCategoryId withSortById:soryById
//    withAdvertiseLanguage: [UserSetting
//    getAdvertiseLanguageServerParam:[UserSetting
//    getUserFilterViewAdvertiseLanguage]]];
//
//}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


- (void)getNewData:(NSString *)searchStr {
//    if ([searchStr isEqualToString:@""] == false) {
//        [SharedViewManager
//         showCategoryProductsWithSearch:self
//         andProductId:categoryProductObject.productCategoryId
//         andSearchStr:searchStr
//         andSubCategoryId:categoryProductObject
//         .productSubCategoryId
//         andSubCategoryName:categoryProductObject
//         .productSubCategoryName
//         andSortById:self.soryById
//         andSortByName:self.soryByName
//         andsortByIndex:self.sortByIndex
//         andTabBarName:self.tabBarNames
//         andDefaultTabSelected:self.currentCategoryTypeBarIndex] ;
//        
//        return;
//    }
    self.searchString=searchStr;
    [self startLoadingCategoryProductsWithRequestId:requestInitialCategoryProducts andIsNewSession:YES];
}

- (void)viewDidUnload {
    [super viewDidUnload];
}

-(void)filtersChanged:(ProductObject *)productObjectParam
{
    //reset arrays
    [self resetArrayOfProducts];
    
    [self.subcategoryBtnDownMenu setTitle:productObjectParam.productSubCategoryName
                                 forState:UIControlStateNormal];
    categoryProductObject = productObjectParam;
    
    [self.filterBtnDownMenu setBackgroundColor:[UIColor colorWithRed:153/255.0 green:35/255.0 blue:91/255.0 alpha:1.0]];
//    [self.subcategoryBtnDownMenu setBackgroundColor:[UIColor colorWithRed:82/255.0 green:82/255.0 blue:82/255.0 alpha:1.0]];
    
    isPullNewData = true;
    
    // it will be loaded in view did appear
    [self startLoadingCategoryProductsWithRequestId:requestInitialCategoryProducts
                                    andIsNewSession:YES];
    
    isPullNewData = false;
}

- (void)carFiltersChoosedWithProductObject:(ProductObject *)productObjectParam {
    
    
    [self filtersChanged:productObjectParam];
    
}

- (void)propertyFiltersChoosedWithProductObject:
(ProductObject *)productObjectParam {
    
    [self filtersChanged:productObjectParam];
}

- (void)otherFiltersChoosedWithProductObject:
(ProductObject *)productObjectParam {
    
    [self filtersChanged:productObjectParam];
    
}

- (void)setCategoryProductsCategoryId:(NSString *)categoryIdParam
                     andSubcategoryId:(NSString *)subcategoryIdParam
                   andSubcategoryName:(NSString *)subcategoryName  {
    if (categoryProductObject == nil) {
        categoryProductObject = [[ProductObject alloc] init];
    }
    
    categoryProductObject.productCategoryId = categoryIdParam;
    categoryProductObject.productSubCategoryId = subcategoryIdParam;
    categoryProductObject.productSubCategoryName = subcategoryName;

}
- (void)setCategoryProductsCategoryId:(NSString *)categoryIdParam
andSubcategoryId:(NSString *)subcategoryIdParam
andTabId:(NSString *)tabId
    {
         NSString *index = @"";
        if (categoryProductObject == nil) {
            categoryProductObject = [[ProductObject alloc] init];
        }
        
        categoryProductObject.productCategoryId = categoryIdParam;
        categoryProductObject.productSubCategoryId = subcategoryIdParam;
        
        for (ProductObject *product in [SharedData getCategories]) {
            if([product.productId isEqualToString: categoryProductObject.productCategoryId ])
            {
                self.tabBarNames = product.productAdvertiseTypes;
                break;
            }
        }
    

        if(subcategoryIdParam!= nil && ![subcategoryIdParam isEqualToString:@""] && tabId != nil && ![tabId isEqualToString:@"0"])
        {
            for (AdvertiseType *adName in self.tabBarNames) {
                if ([adName.advertiseTypeId isEqualToString:tabId]) {
                    index = [NSString stringWithFormat:@"%lu",(unsigned long)[self.tabBarNames indexOfObject: adName]];
                    break;
                }
            }
            NSData *arrayData = [[NSUserDefaults standardUserDefaults]objectForKey:[SharedData CATEGORY_ARRAY_FILE_KEY]];
            NSMutableArray *arrCategories = [NSKeyedUnarchiver unarchiveObjectWithData:arrayData];
            if(arrCategories.count>0){
                
                CategoryObject *categoryMain = [[CategoryObject alloc]init];
                for (CategoryObject *category in arrCategories) {
                    if([category.categoryID isEqualToString:categoryIdParam]){
                        categoryMain = category;
                        break;
                    }
                }
                NSMutableArray *arrSubcategories = [[NSMutableArray alloc]init];
                for (CategoryObject *obj in categoryMain.categorySubcategories) {
                    
                    if([obj.categoryID isEqualToString:subcategoryIdParam]){
                         categoryProductObject.productSubCategoryName = obj.categoryName;
                        break;
                    }
                }
                
            }

        [self selectedTabFromSubMenu:categoryIdParam withTabID:index];
        }else{
            
            [self
             setCategoryProductsCategoryId:categoryIdParam
             andSubcategoryId:@"0"
             andSubcategoryName:@""];
        }
        
}
- (void)selectedTabFromSubMenu:(NSString *)strCategoryID withTabID:(NSString *)strTabID
{
    [self btnCategoryTypeNameClickedWithId:strCategoryID andIndex:[strTabID intValue]];
}
#pragma mark - Table view delegate
- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath{
   return UITableViewAutomaticDimension;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    //issue happen than some times table refresh by in old table , so it crash as elements in array
    //less than row required , so we return empty values in this case
    if(categoryAdvertiseType == COMPANY){
        return UITableViewAutomaticDimension;
    }else
        if(categoryAdvertiseType == LIST){
        if(indexPath.row>(productItems.count-1))
        {
            return 140;
        }
        ProductObject* productObject = [productItems objectAtIndex:indexPath.row];
        if ([DisplayUtility isPad]) {
            if (productObject.isAd)
                return 275;
            return 220;
        }
        else {
            if (productObject.isAd) {
                return 275;
            } else {
                return 165;
            }
        }
    }
    
}

#pragma mark - Table view data source


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    NSUInteger numberOfRow = 0;
    
    switch (categoryAdvertiseType) {
            
        case COMPANY:
            
            numberOfRow = productItems.count;
            break;
            
        case LIST:
            
            numberOfRow = productItems.count;
            break;
            
        case GRID:
            //scroll to top
            numberOfRow = 0;
            break;
            
        default:
            break;
    }
    return numberOfRow;
}
- (UITableViewCell *)returnCompanyTableViewCell:(NSIndexPath *)indexPath {
    
    ProductObject *productObject;
    CompanyTableViewCell *cell;
    
    if (indexPath.row > productItems.count - 1)
    {
        
    }
    
//    productObject = [productItems objectAtIndex:indexPath.row];
    cell = (CompanyTableViewCell *)
    [self.companyTableView dequeueReusableCellWithIdentifier:@"CompanyTableViewCell"];
    
    UIImageView *av = [[UIImageView alloc] init];
    av.backgroundColor = [UIColor clearColor];
    av.opaque = NO;
    av.image = [UIImage imageNamed:@"Backg.png"];
    cell.backgroundView = av;
    
    
    if (indexPath.row < productItems.count)
    {
        
        ProductObject *productObject = [productItems objectAtIndex:indexPath.row];
        cell.productObject = productObject;
        [cell.collView reloadData];
        // load image using sdwebimage framework handles all tasks of image download
        NSString *imageName =
        [[productObject productId] stringByAppendingString:@".png"];
        
        [self setImageIndicatorForImageView:cell.companyImage imageURL:productObject.productMainImageUrl imageName: imageName imageIndicator:cell.companyImageIndicator rounded:true];
        
        if(productObject.productImagesUrls != nil && productObject.productImagesUrls.count > 0){
            cell.lbl.hidden = true;
            cell.collView.hidden = false;
            
        } else {
            cell.lbl.hidden = false;
            cell.collView.hidden = true;
        }
        
        if(!([productObject.productNumberOfViews isEqualToString: nil]|| [productObject.productNumberOfViews isEqualToString: @"null"]) ){
            
            [cell.viewsCount
             setText:[NSString
                      stringWithFormat:@"%@", productObject.productNumberOfViews]];
        }else{
            [cell.viewsCount
             setText:[NSString
                      stringWithFormat:@"%@", @"0"]];
        }
        
        if(!([productObject.productCompanyNumberOfAds isEqualToString: nil]|| [productObject.productCompanyNumberOfAds isEqualToString: @"null"]) ){
            
            [cell.adsCount
             setText:[NSString
                      stringWithFormat:@"%@", productObject.productCompanyNumberOfAds]];
        }else{
            [cell.adsCount
             setText:[NSString
                      stringWithFormat:@"%@", @"0"]];
        }
        
        [cell.companyTitle setText:productObject.productTilte];
    }
    
    
    return cell;
    
}

- (void) setImageIndicatorForImageView:(UIImageView*) imageView imageURL:(NSString*) url imageName:(NSString*)imageName imageIndicator:(UIActivityIndicatorView*) imageIndicator rounded:(BOOL) isRounded{
    //        cell.customImageView.layer.cornerRadius = 5.0;
    //        cell.customImageView.clipsToBounds = YES;
    imageView.contentMode = UIViewContentModeScaleToFill;
    
    [imageView setAlpha:1];
    
    [imageIndicator startAnimating];
    // if image is null , it came as array type object , so we check by type if
    // its string
    if ([url isKindOfClass:[NSString class]]) {
        [imageView
         sd_setImageWithURL:[NSURL
                             URLWithString:url]
         placeholderImage:[UIImage imageNamed:imageName]
         completed:^(UIImage *image, NSError *error,
                     SDImageCacheType cacheType, NSURL *imageURL) {
             [imageIndicator stopAnimating];
             
            imageView.layer.cornerRadius = imageView.frame.size.height/2;
             imageView.layer.masksToBounds = true;
         }];
    } else {
        [imageIndicator stopAnimating];
    }
}

- (void) setImageIndicatorForImageView:(UIImageView*) imageView imageURL:(NSString*) url imageName:(NSString*)imageName imageIndicator:(UIActivityIndicatorView*) imageIndicator{
    //        cell.customImageView.layer.cornerRadius = 5.0;
    //        cell.customImageView.clipsToBounds = YES;
            imageView.contentMode = UIViewContentModeScaleToFill;
    
    [imageView setAlpha:1];
    
    [imageIndicator startAnimating];
    // if image is null , it came as array type object , so we check by type if
    // its string
    if ([url isKindOfClass:[NSString class]]) {
        [imageView
         sd_setImageWithURL:[NSURL
                             URLWithString:url]
         placeholderImage:[UIImage imageNamed:imageName]
         completed:^(UIImage *image, NSError *error,
                     SDImageCacheType cacheType, NSURL *imageURL) {
             [imageIndicator stopAnimating];
         }];
    } else {
        [imageIndicator stopAnimating];
    }


}
- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    // collectionViewIndexPath=indexPath.row;
    
    //issue happen than some times table refresh by in old table , so it crash as elements in array
    //less than row required , so we return empty values in this case
    UITableViewCell *uiTableViewCell;
    
    switch (categoryAdvertiseType) {
            
        case COMPANY:
            
            uiTableViewCell = [self returnCompanyTableViewCell:indexPath];
            
            break;
            
        case LIST:
            
            uiTableViewCell = [self returnTableViewCell:indexPath];
            
            break;
            
            
        default:
            
            break;
    }
    
    uiTableViewCell.backgroundColor = [UIColor clearColor];
    
    return uiTableViewCell;
}
- (UITableViewCell *)returnTableViewCell:(NSIndexPath *)indexPath {
    
    ProductObject *productObject;
    CustomTableViewCell *cell;
    
    if(indexPath.row>(productItems.count-1))
    {
        cell = (CustomTableViewCell *)
        [self.tableView dequeueReusableCellWithIdentifier:FreeCellIdentifier];
        
        return cell;
    }
    
    productObject = [productItems objectAtIndex:indexPath.row];
    
    if (productObject.isAd)
    {
        //cell = (CustomTableViewCell *)
        //[self.tableView dequeueReusableCellWithIdentifier:PaidCellIdentifier];
        NSString *identifier = [NSString stringWithFormat:@"Identifier_%d-%d-%d", (int)indexPath.section, (int)indexPath.row, (int)indexPath.item];
        [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:identifier];
        
        UITableViewCell *cellNew = [self.tableView dequeueReusableCellWithIdentifier:identifier forIndexPath:indexPath];
        
        if(productObject.isLoadedAddInList==false){
            
            DFPBannerView *bannerView;
            UIActivityIndicatorView *loadingIndicator = [[UIActivityIndicatorView alloc]init];
            UILabel *lblAdText;
            if([DisplayUtility isPad]){
                bannerView = [[DFPBannerView alloc]initWithFrame:CGRectMake(234,20,300,250)];
                lblAdText = [[UILabel alloc]initWithFrame:CGRectMake(234, 5, 300, 15)];
                if([self.language isEqualToString:[ServerManager LANGUAGE_ARABIC]]) {
                    bannerView.adUnitID = @"/271990408/ProductPageiOSTabletAppMPUArabic";
                    lblAdText.text = @"اعلان";
                    [lblAdText setTextAlignment:NSTextAlignmentRight];
                }else{
                    bannerView.adUnitID = @"/271990408/ProductPageiOSTabletAppMPUEnglish";
                    lblAdText.text = @"ADVERTISEMENT";
                    [lblAdText setTextAlignment:NSTextAlignmentLeft];
                }
            }else{
                if(cellNew.frame.origin.x==0){
                    bannerView = [[DFPBannerView alloc]initWithFrame:CGRectMake(self.view.center.x-150,20,300,250)];
                    lblAdText = [[UILabel alloc]initWithFrame:CGRectMake(self.view.center.x-150, 5, 300, 15)];
                }else{
                    
                    if([self.language isEqualToString:[ServerManager LANGUAGE_ARABIC]]) {
                        bannerView = [[DFPBannerView alloc]initWithFrame:CGRectMake(20,20,300,250)];
                    }else{
                        bannerView = [[DFPBannerView alloc]initWithFrame:CGRectMake(0,20,300,250)];
                        
                    }
                    lblAdText = [[UILabel alloc]initWithFrame:CGRectMake(3, 5, 300, 15)];
                }
                
                if([self.language isEqualToString:[ServerManager LANGUAGE_ARABIC]]) {
                    bannerView.adUnitID = @"/271990408/ProductPageiOSMobileAppMPUArabic";
                    lblAdText.text = @"اعلان";
                    [lblAdText setTextAlignment:NSTextAlignmentRight];
                }else{
                    bannerView.adUnitID = @"/271990408/ProductPageiOSMobileAppMPUEnglish";
                    lblAdText.text = @"ADVERTISEMENT";
                    [lblAdText setTextAlignment:NSTextAlignmentLeft];
                }
            }
            
            [loadingIndicator startAnimating];
            
            bannerView.rootViewController = self;
            [bannerView loadRequest:[DFPRequest request]];
            bannerView.tag = 111;
            
            lblAdText.textColor = [UIColor lightGrayColor];
            lblAdText.font = [UIFont systemFontOfSize:12.0 weight:UIFontWeightHeavy];
            
            [loadingIndicator setFrame:CGRectMake(bannerView.frame.origin.x+150,bannerView.frame.size.height/2.0, 20, 20)];
            
            [cellNew.contentView addSubview:lblAdText];
            [cellNew.contentView addSubview:loadingIndicator];
            [cellNew.contentView addSubview:bannerView];
            
            productObject.isLoadedAddInList = true;
        }
        
        cellNew.backgroundColor = [UIColor clearColor];
        [cellNew setBackgroundColor:[UIColor redColor]];
        return cellNew;
    }
    else
    {
        cell = (CustomTableViewCell *)
        [self.tableView dequeueReusableCellWithIdentifier:FreeCellIdentifier];
    }
    
    // if (cell == nil) {
    //        cell =(CustomTableViewCell *) [[UITableViewCell alloc]
    //        initWithStyle:UITableViewCellStyleDefault
    //                                      reuseIdentifier:CellIdentifier];
    //}
    
    if(loadMoreWhereAvailable)
    {
        if (indexPath.row >= (productItems.count * 0.40)) {
            
            if (!loading) {
                
                loading = YES;
                // loadRequest is the method that loads the next batch of data.
                BOOL myBool = NO;
                NSNumber *passedValue = [NSNumber numberWithBool:myBool];
                [self loadData:passedValue];
            }
        }
    }
    if (indexPath.row < productItems.count) {
        ProductObject *productObject = [productItems objectAtIndex:indexPath.row];
        
        // load image using sdwebimage framework handles all tasks of image download
        NSString *imageName =
        [[productObject productId] stringByAppendingString:@".png"];
        NSString *url = [productObject productMainImageUrl];
        cell.customImageView.contentMode = UIViewContentModeScaleToFill;
        [cell.customImageView setAlpha:1];
        //        cell.customImageView.layer.cornerRadius = 5.0;
        //        cell.customImageView.clipsToBounds = YES;
        [cell.activityIndicator startAnimating];
        // if image is null , it came as array type object , so we check by type if
        // its string
        if ([url isKindOfClass:[NSString class]]) {
            [cell.customImageView
             sd_setImageWithURL:[NSURL
                                 URLWithString:url]
             placeholderImage:[UIImage imageNamed:imageName]
             completed:^(UIImage *image, NSError *error,
                         SDImageCacheType cacheType, NSURL *imageURL) {
                 [cell.activityIndicator stopAnimating];
             }];
        } else {
            [cell.activityIndicator stopAnimating];
        }
        
        // set the title
        [cell.title setText:productObject.productTilte];
        
        if (productObject.isAd == true) {
            
            //=================================================
            // Log setting open event with category="ui", action="open", and label="settings".
            id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
            [tracker send:
             [[GAIDictionaryBuilder createEventWithCategory:@"ui"
                                                     action:productObject.productTilte
                                                      label:@"advertisement"
                                                      value:nil] build]];
            //=================================================
            
            [cell.qr setText:[NSString
                              stringWithFormat:@"%@", productObject.productPrice]];
        } else {
            [cell.qr
             setText:[NSString
                      stringWithFormat:@"%@ %@", productObject.productPrice,
                      NSLocalizedString(@"PRICE_QR", nil)]];
        }
        
        [cell.likes setTitle:[NSString stringWithFormat:@" %@ ", productObject.productLikeCount]forState:UIControlStateNormal];
        [cell.comments setTitle:[NSString stringWithFormat:@" %@ ", productObject.productCommentCount]forState:UIControlStateNormal];
        [cell.desc setText:productObject.productDescription];
        
        if([[SharedData getDeviceLanguage]isEqualToString:@"en"]){
            [cell.likes setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
            [cell.comments setContentHorizontalAlignment:UIControlContentHorizontalAlignmentRight];
        }else{
//            [cell.likes setTitleEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 0)];
//            [cell.comments setImageEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 0)];
//            [cell.likes setContentHorizontalAlignment:UIControlContentHorizontalAlignmentRight];
//            [cell.comments setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
        }
        
        if([productObject.productUserisCompany isEqualToString:@"0"]){
            cell.isCompanyView.layer.cornerRadius = 5.0f;
            cell.isCompanyView.layer.borderWidth = 1.0f;
            cell.isCompanyView.layer.borderColor = [UIColor colorWithRed:0.98 green:0.66 blue:0.19 alpha:1.0].CGColor;
            cell.isCompanyView.layer.masksToBounds = YES;
//            cell.isCompanyView.hidden = NO;
            cell.constViewCompanyWidth.constant = 62;
        }else{
//            cell.isCompanyView.hidden = YES;
            cell.constViewCompanyWidth.constant = 0;
        }
    }
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    ProductObject *productObjectSelected = [productItems objectAtIndex:indexPath.row];
    
    if([productObjectSelected.productAdvClickType isEqualToString:@"1"]){
        
        [[UIApplication sharedApplication]openURL:[NSURL URLWithString:productObjectSelected.productAdvertiseUrl]];
        return;
    }else if([productObjectSelected.productAdvClickType isEqualToString:@"2"]){
        
        UserProducts *userProducts = [[SharedData getCurrentStoryBoard]instantiateViewControllerWithIdentifier:@"UserProducts"];
        [userProducts setUserProfileProductObject:productObjectSelected];
        [userProducts showBackButton:YES];
        [self.navigationController pushViewController:userProducts animated:YES];
        
    }else if([productObjectSelected.productAdvClickType isEqualToString:@"0"]){
        
        ProductDetailsViewController *productDetail = [self.storyboard
                                                       instantiateViewControllerWithIdentifier:@"ProductDetailsViewController"];
        [productDetail setProductId:productObjectSelected.productId];
        productDetail.isFromCategoryProducts = true;
        productDetail.productObject = productObjectSelected;
        [self.navigationController pushViewController:productDetail animated:YES];
    }else{
        
        if([productObjectSelected.productAdvertiseUrl isEqualToString:@""] == false) {
            
            [self performSegueWithIdentifier:@"ProfileSegue" sender:productObjectSelected];
        }else{
            // open category products and set the index
            ProductDetailsViewController *productDetail = [self.storyboard
                                                           instantiateViewControllerWithIdentifier:@"ProductDetailsViewController"];
            [productDetail setProductId:productObjectSelected.productId];
            productDetail.isFromCategoryProducts = true;
            [self.navigationController pushViewController:productDetail animated:YES];
        }
    }
}

#pragma mark - segue methods

- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender {
    
    if ([identifier isEqualToString:@"ImageViewClicked"] || [identifier isEqualToString:@"ImageAdViewClicked"]) {
        
        UICollectionViewCell *cell = (UICollectionViewCell *)sender;
        NSIndexPath *indexPath = [self.collectionViewGridView indexPathForCell:cell];
        ProductObject *productObjectSelected = [productItems objectAtIndex:indexPath.row];
        
        if([identifier isEqualToString:@"ImageViewClicked"]){
            if([productObjectSelected.productAdvClickType isEqualToString:@"1"]){
                [[UIApplication sharedApplication]openURL:[NSURL URLWithString:productObjectSelected.productAdvertiseUrl]];
                return false;
            }else if([productObjectSelected.productAdvClickType isEqualToString:@"2"]){
                
                UserProducts *userProducts = [[SharedData getCurrentStoryBoard]instantiateViewControllerWithIdentifier:@"UserProducts"];
                [userProducts setUserProfileProductObject:productObjectSelected];
                [userProducts showBackButton:YES];
                [self.navigationController pushViewController:userProducts animated:YES];
                return false;
            }else{
                return true;
            }
        }else{
            if ([productObjectSelected.productAdvertiseUrl isEqualToString:@""] == false) {
                
                [[UIApplication sharedApplication]openURL:[NSURL URLWithString:productObjectSelected.productAdvertiseUrl]];
                return false;
            }
        }
    }
    return true;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    [super prepareForSegue:segue sender:sender];
    
    if ([[segue identifier] isEqualToString:@"ImageViewClicked"] ||
        [[segue identifier] isEqualToString:@"ImageAdViewClicked"]) {
        // get the index clicked
        UICollectionViewCell *cell = (UICollectionViewCell *)sender;
        NSIndexPath *indexPath =
        [self.collectionViewGridView indexPathForCell:cell];
        
        ProductObject *productObjectSelected =
        [productItems objectAtIndex:indexPath.row];
        
        if([[segue identifier] isEqualToString:@"ImageAdViewClicked"]||[productObjectSelected.productAdvClickType isEqualToString:@"0"]||productObjectSelected.productAdvClickType == nil){
            // open category products and set the index
            ProductDetailsViewController *productDetail = [segue destinationViewController];
            
            [productDetail setProductId:productObjectSelected.productId];
            productDetail.isFromCategoryProducts = true;
            productDetail.productObject = productObjectSelected;
        }else{
            if([productObjectSelected.productAdvClickType isEqualToString:@"2"]){
                
                UserProducts *userProducts = [[SharedData getCurrentStoryBoard]instantiateViewControllerWithIdentifier:@"UserProducts"];
                [userProducts setUserProfileProductObject:productObjectSelected];
                [userProducts showBackButton:YES];
                [self.navigationController pushViewController:userProducts animated:YES];
            }
        }
        
    } else if ([[segue identifier] isEqualToString:@"SortByMenuClicked"]) {
        
        SortByChooserViewController *sortByChooserViewController =
        [segue destinationViewController];
        [sortByChooserViewController setSoryByDelegate:self andSelectedIndex:self.sortByIndex];
        //        [sortByChooserViewController setDelegate:self];
        //        [sortByChooserViewController setSelectedIndex:self.soryById];
        
    } else if ([[segue identifier] isEqualToString:@"SubCategoryMenuClicked"]) {
        
        SubCategoryChooser *subCategoryChooser = [segue destinationViewController];
        
        [subCategoryChooser
         setSubCategoryParamsWithCatId:categoryProductObject.productCategoryId
         andCurrentSubCategoryId:categoryProductObject.productSubCategoryId
         andIsOpenedFromAddAdvertise:false
         andDelegate:self];
    }  else if ([[segue identifier] isEqualToString:@"ProfileSegue"]) {
        UserProducts *userProfileViewController = [segue destinationViewController];
        [userProfileViewController setUserProfileProductObject:sender];
        [userProfileViewController showBackButton:YES];
    } else if ([[segue identifier] isEqualToString:@"segueSearchCategoryProduct"]){
        SearchViewController *searchController = segue.destinationViewController;
        searchController.delegate = self;
        searchController.strId = categoryProductObject.productCategoryId;
        
        NSString *strTabName = @"";
        
        for (int i=0; i<self.tabBarNames.count; i++) {
            AdvertiseType *advertiseType =
            [self.tabBarNames objectAtIndex:i];
            
            if (categoryProductObject.productAdvertiseTypeId ==
                advertiseType.advertiseTypeId) {
                strTabName = advertiseType.advertiseTypeName;
                break;
            }
        }
        
        NSString *strCategory = @"";
        
        if ([categoryProductObject.productSubCategoryName isEqualToString:NSLocalizedString(@"ALL_SUBCATEGORIES", nil)]) {
            strCategory = NSLocalizedString(@"All Subcategories", nil);
        }else{
            strCategory = categoryProductObject.productSubCategoryName;
        }
        
        searchController.strCategories = [NSString stringWithFormat:@"%@ > %@ > %@", self.strCategory, strCategory, strTabName];
    }

}

#pragma mark - Down Menu filters - other menu filters in segue

- (IBAction)filterMenuClicked:(id)sender {
    NSString *currentCategoryFilterType = nil;
   
    for (ProductObject *productObjectLoadedFromServer in
         [SharedData getCategories]) {
        if ([productObjectLoadedFromServer.productId
             isEqualToString:categoryProductObject.productCategoryId]) {
            currentCategoryFilterType =
            productObjectLoadedFromServer.productFilterType;
            break;
        }
    }
    
    if ([currentCategoryFilterType
         isEqualToString:[SharedData CAR_FILTER_VALUE]]) {
        FilterCarsChooser *filterCarsChooser = [[SharedData getCurrentStoryBoard]
                                                instantiateViewControllerWithIdentifier:@"FilterCarsChooser"];

        [filterCarsChooser setProductObject:[categoryProductObject copyWithZ]];
        [filterCarsChooser setDelegate:self];
        
        [[self navigationController] pushViewController:filterCarsChooser
                                               animated:YES];
    } else if ([currentCategoryFilterType
                isEqualToString:[SharedData PROPERTY_FILTER_VALUE]]) {
        FilterPropertyChooser *filterPropertyChooser = [[SharedData getCurrentStoryBoard]
                                                        instantiateViewControllerWithIdentifier:@"FilterPropertyChooser"];
        [filterPropertyChooser setProductObject:[categoryProductObject copyWithZ]];
        [filterPropertyChooser setDelegate:self];
        
        [[self navigationController] pushViewController:filterPropertyChooser
                                               animated:YES];
    } else if ([currentCategoryFilterType
                isEqualToString:[SharedData OTHER_FILTER_VALUE]]) {
        FilterOtherCategoriesChooser *filterOtherCategoriesChooser =
        [[SharedData getCurrentStoryBoard] instantiateViewControllerWithIdentifier:
         @"FilterOtherCategoriesChooser"];
        [filterOtherCategoriesChooser setProductObject:[categoryProductObject copyWithZ]];
        [filterOtherCategoriesChooser setDelegate:self];
        
        [[self navigationController] pushViewController:filterOtherCategoriesChooser
                                               animated:YES];
    }
}

- (void)subCategoryChoosedInList:(NSString *)subCategoryId
              andSubCategoryName:(NSString *)subCategorName {
    [self resetArrayOfProducts];
    [self.subcategoryBtnDownMenu setTitle:subCategorName
                                 forState:UIControlStateNormal];
//      [self.subcategoryBtnDownMenu setBackgroundColor:[UIColor colorWithRed:82/255.0 green:82/255.0 blue:82/255.0 alpha:1.0]];
    //set subcategory and reset subsubcategory
    [categoryProductObject setProductSubCategoryId:subCategoryId];
    [categoryProductObject setProductSubCategoryName:subCategorName];
    [categoryProductObject setProductSubsubCategoryId: @""];
    [categoryProductObject setProductSubsubCategoryName:nil];
    
    [self startLoadingCategoryProductsWithRequestId:requestInitialCategoryProducts
                                    andIsNewSession:YES];
}

- (void)sortByChooserChanged:(NSString *)sortById
             withSortByIndex:(int)sortByIndex
              withSortByName:(NSString *)sortByName {
    [self resetArrayOfProducts];
//      [self.sortByBtnDownMenu setBackgroundColor:[UIColor colorWithRed:82/255.0 green:82/255.0 blue:82/255.0 alpha:1.0]];
    [self.sortByBtnDownMenu setTitle:sortByName forState:UIControlStateNormal];
    self.sortByIndex = sortByIndex;
    self.soryById=sortById;
    [self startLoadingCategoryProductsWithRequestId:requestInitialCategoryProducts
                                    andIsNewSession:YES];
    sortNameAfterSearch = sortByName;
}

-(void)resetArrayOfProducts
{
    for (int i = 0; i < _tabBarNames.count; i++) {
        if(i<[tabsProductList count])
        {
            [tabsProductList objectAtIndex:i];
            [tabsProductList objectAtIndex:i];
            [tabsProductList replaceObjectAtIndex:i withObject:[[NSMutableArray alloc] init]];
        }
        else
        {
            [tabsProductList addObject:[[NSMutableArray alloc] init]];
        }
    }
}

- (void)listMethod {
    [self.selectedViewType setTitle:NSLocalizedString(@"LIST_TITLE", nil)
                           forState:UIControlStateNormal];
    UIImage *img = [UIImage imageNamed:@"list_icon.png"];
    self.gridOrListImage.image = img;
}


- (void)showSearchBar:(BOOL)isHideSearchBar
{
    if (![DisplayUtility isPad]) {
        self.navigationItem.hidesBackButton=YES;
    }
    [super showSearchBar:NO];
    self.navigationItem.titleView.frame = CGRectMake(0, 6, self.view.frame.size.width-10, 32);

}

- (IBAction)cancelSearchBar:(id)sender
{
    [super showSearchBar:YES];
    self.navigationItem.hidesBackButton=NO;
    
}
@end
