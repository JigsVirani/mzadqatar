//
//  ErrorDialogueViewController.m
//  Mzad Qatar
//
//  Created by iOS Developer on 15/12/2014.
//  Copyright (c) 2014 Mohammed abdulfattah attia. All rights reserved.
//

#import "ErrorDialogueViewController.h"

@interface ErrorDialogueViewController ()

@end

@implementation ErrorDialogueViewController

- (void)viewDidLoad {
  [super viewDidLoad];
    
    self.screenName = @"Error Dialogue";
}

- (void)didReceiveMemoryWarning {
  [super didReceiveMemoryWarning];
  // Dispose of any resources that can be recreated.
}

- (IBAction)btnClose:(id)sender {

  [self.view removeFromSuperview];
}
- (IBAction)registerAsCompany:(id)sender
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"REGISTERCOMPANY"
                                                        object:self];
    [self.view removeFromSuperview];
}
- (void)setDialogTitle:(NSString *)title
         andDescrition:(NSString *)descritipn
         andActionText:(NSString *)actionText {
    if ([actionText isEqualToString:@"COMPANY"]) {
        self.registerView.hidden = NO;
        [self.dialogTitle setText:title];
        [self.dialogDescirption setText:descritipn];
        [self.registerAsComapny setTitle:NSLocalizedString(@"REGISTER_COMPANY", nil) forState:UIControlStateNormal];
        [self.dismissButton setTitle:NSLocalizedString(@"DISMISS", nil) forState:UIControlStateNormal];
        self.registerAsComapny.layer.cornerRadius = self.dismissButton.bounds.size.height/2.0;
        [self.registerAsComapny setClipsToBounds:YES];
    }else
    {
    self.registerView.hidden = YES;
    [self.dialogTitle setText:title];
    [self.dialogDescirption setText:descritipn];
    [self.actionBtn setText:actionText];
    }
}


@end
