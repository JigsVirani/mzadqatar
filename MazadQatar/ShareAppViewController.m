//
//  ShareAppViewController.m
//  Mzad Qatar
//
//  Created by iOS Developer on 17/12/2014.
//  Copyright (c) 2014 Mohammed abdulfattah attia. All rights reserved.
//

#import "ShareAppViewController.h"
#import "UserSetting.h"
#import "ServerManager.h"
#import "SharedData.h"

@interface ShareAppViewController ()

@end

@implementation ShareAppViewController

- (void)viewDidLoad {
  [super viewDidLoad];
    
    self.screenName = @"Share App Dialogue";
  share = [[SocialShareUtility alloc] init];

  rateController = [[iRate alloc] init];
}

- (void)didReceiveMemoryWarning {
  [super didReceiveMemoryWarning];
  // Dispose of any resources that can be recreated.
}

- (IBAction)noThanksClikced:(id)sender {
    [SharedData setIsRateShareDialogShown:false];

  [rateController declineThisVersion];

  [self.view removeFromSuperview];
}

- (IBAction)laterClicked:(id)sender {
    [SharedData setIsRateShareDialogShown:false];

  [rateController remindLater];

  [self.view removeFromSuperview];
}

- (IBAction)btnClose:(UIButton *)sender {
    [SharedData setIsRateShareDialogShown:false];

  [rateController remindLater];

  [self.view removeFromSuperview];
}

- (IBAction)facebookClicked:(id)sender {
  [share shareOnFacebookInView:self
                      withText:NSLocalizedString(@"SHARE_MZAD_QATAR_TEXT", nil)
                     withImage:[self getShareAppImage]
                       withUrl:nil];
}

- (IBAction)twitterClicked:(id)sender {
  [share shareOnTwitterInView:self
                     withText:NSLocalizedString(@"SHARE_TWITTER_MZAD_QATAR_TEXT", nil)
                    withImage:[self getShareAppImage]
                      withUrl:nil];
}

- (IBAction)whatsappClicked:(id)sender {
  [share
      shareOnWhatsAppInView:self
                   withText:[NSString stringWithFormat:@"%@ %@",NSLocalizedString(@"SHARE_MZAD_QATAR_TEXT", nil),@"http://mzadqatar.com/app"]];
}

- (IBAction)instagramClicked:(id)sender {
  [share shareOnInstagramInView:self withImage:[self getShareAppImage]];
}

- (IBAction)otherSharesClicked:(id)sender {

  [share shareOnOtherAppsInView:self
             andFrameToSHowFrom:((UIButton *)sender).frame
                       withText:NSLocalizedString(@"SHARE_MZAD_QATAR_TEXT", nil)
                      withImage:[self getShareAppImage]
                        withUrl:@"http://mzadqatar.com/app"];
}

- (UIImage *)getShareAppImage {

  UIImage *shareImage = nil;

    shareImage = [UIImage imageNamed:@"share-image.jpg"];

  return shareImage;
}
@end
