//
//  CategoryProducts.h
//  MazadQatar
//
//  Created by samo on 3/9/13.
//  Copyright (c) 2013 Mohammed abdulfattah attia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GridView.h"
#import "MBProgressHUD.h"
#import "AppDelegate.h"
#import "FilterPropertyChooser.h"
#import "FilterOtherCategoriesChooser.h"
#import "FilterCarsChooser.h"
#import "CustomTableViewCell.h"
#import "CompanyTableViewCell.h"
#import "CategoryTypesBarCell.h"

#define IS_IPHONE5                                                             \
  (([[UIScreen mainScreen] bounds].size.height - 568) ? NO : YES)

#define requestInitialCategoryProducts @"requestInitialCategoryProducts"
#define requestMoreCategoryProducts @"requestMoreCategoryProducts"
#define requestRefreshCategoryProducts @"requestRefreshCategoryProducts"
#define requestPullNewDataCategoryProducts @"requestPullNewDataCategoryProducts"

@interface CategoryProducts
    : GridView <ServerManagerResponseDelegate, UIGestureRecognizerDelegate,
                SubCategoryChooserDelegate, SortByChooserDelegate,
                FilterCarsChooserDelegate, FilterOtherCategoriesChooserDelegate,
                FilterPropertyChooserDelegate, CategoryTypesBarCellDelegate, UIScrollViewDelegate> {
                    enum CategoryAdvertiseType: NSInteger
{
GRID,
LIST,
COMPANY
} ;
enum CategoryAdvertiseType categoryAdvertiseType;
                    
  NSMutableArray *tabsProductList;
                    AppDelegate *appDelegate;
                    BOOL check;
}

@property(nonatomic, assign) int currentCategoryTypeBarIndex;

@property(nonatomic) BOOL adjustsFontSizeToFitWidth;
@property (weak, nonatomic) IBOutlet UIButton *AdvertisementButton;
@property(nonatomic, strong) NSMutableArray *tabBarNames;
@property(nonatomic, strong) NSString *strCategory;
@property(weak) NSString *productsLanguage;
@property(strong, nonatomic) IBOutlet UITableView *tableView;

@property(strong, nonatomic) IBOutlet UITableView *companyTableView;

@property(strong, nonatomic) IBOutlet UIButton *subcategoryBtnDownMenu;
@property(strong, nonatomic) IBOutlet UIButton *filterBtnDownMenu;
@property(strong, nonatomic) IBOutlet UIButton *sortByBtnDownMenu;
@property(strong, nonatomic) IBOutlet UIButton *btnAddCmpany;

@property(strong, nonatomic) IBOutlet UIButton *selectedViewType;
@property(strong, nonatomic) IBOutlet UIImageView *gridOrListImage;
@property(strong, nonatomic)
    IBOutlet UICollectionView *categoryTypesCollectionView;



// IBActions
- (IBAction)btnchangeView:(UIButton *)sender;

/// Methods
- (void)selectedTabFromSubMenu:(NSString *)strCategoryID withTabID:(NSString *)strTabID;
- (void)startLoadingCategoryProductsWithRequestId:(NSString *)requestId
                                  andIsNewSession:(bool)isNewSession;
- (void)setCategoryProductsCategoryId:(NSString *)categoryIdParam
                     andSubcategoryId:(NSString *)subcategoryIdParam
                   andSubcategoryName:(NSString *)subcategoryName;

- (void)setCategoryProductsCategoryId:(NSString *)categoryIdParam
                     andSubcategoryId:(NSString *)subcategoryIdParam
                   andTabId:(NSString *)tabId;



/** Set width constant of Add Company **/
@property (weak,nonatomic)IBOutlet NSLayoutConstraint *topBarHeightConstant,*bottomBarHeightConstant;

@end
