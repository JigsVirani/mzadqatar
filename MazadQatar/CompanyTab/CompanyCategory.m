
#import "CompanyCategory.h"
@interface CompanyCategory ()<ServerManagerResponseDelegate,UIScrollViewDelegate>
{
	__weak IBOutlet NSLayoutConstraint *featureAdsHeight;
__weak IBOutlet NSLayoutConstraint *subViewHeights;
}
@property (strong)NSMutableArray *arrServices,*arrFeatureAd;
@end


#define requestCompaniesCategories @"action=getCompaniesCategories"
@implementation CompanyCategory
NSString *requestId_service_grid = @"get service grid";



- (IBAction)displayCompanyScreen:(UIButton *)sender {
    
    CompanyRegistration  *companyRegistration = [[SharedData getCurrentStoryBoard]
                    instantiateViewControllerWithIdentifier:@"RegisterAsCompany"];
    [self.navigationController pushViewController:companyRegistration animated:YES];
}


#pragma mark - LifeCycle Method -
-(void)viewDidDisappear:(BOOL)animated{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"BACK", nil) style:UIBarButtonItemStylePlain target:nil action:nil];

	self.search_View.layer.cornerRadius=5.0;
	self.search_View.layer.borderWidth=1.0;
	self.search_View.layer.borderColor=[UIColor colorWithRed:85/255.0 green:85/255.0 blue:85/255.0 alpha:1.0].CGColor;
    [[UITextField appearanceWhenContainedIn:[UISearchBar class], nil] setDefaultTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    [self getDataFromServer];
    [self setContentSizeSubview];
    // Do any additional setup after loading the view.
    
    if([[SharedData getDeviceLanguage]isEqualToString:@"ar"])
    {
        [self.adCollection setTransform:CGAffineTransformMakeScale(-1, 1)];
    }
    
    [_addCompanyBtn.layer setBorderColor:[UIColor whiteColor].CGColor];
    [_addCompanyBtn.layer setBorderWidth:1.5f];
    [_addCompanyBtn setTitle:NSLocalizedString(@"Add Company", nil) forState:UIControlStateNormal];
    if (IS_IPAD) {
        _addCompanyBtn.titleLabel.font = [UIFont systemFontOfSize:22.0];
        _addCompanyBtnWidthConstant.constant = 200.0;
        [_headerTitleLbl setFont:[UIFont boldSystemFontOfSize:22.0]];
    }
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
}
-(void) viewDidAppear:(BOOL)animated
{
	[super viewDidAppear:animated];
	self.tabBarController.tabBar.hidden = NO;
}
- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}

#pragma mark Button - Action
- (IBAction)btnSearchAction:(id)sender
{
    SearchViewController *controller = [[SharedData getCurrentStoryBoard]
                                        instantiateViewControllerWithIdentifier:@"SearchViewController"];
    [self.navigationController pushViewController:controller animated:YES];
}
#pragma mark Get - Company Category Data
- (void) getDataFromServer
{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [hud setLabelText:NSLocalizedString(@"LOADING_BANNERS", Nil)];
    //NSString *language = [[[NSBundle mainBundle] preferredLocalizations] objectAtIndex:0];
    NSString *language = [LanguageManager currentLanguageCode];
    
    if ([language isEqualToString:[ServerManager LANGUAGE_ARABIC]]) {
        language = [ServerManager LANGUAGE_ARABIC];
    } else {
        language = [ServerManager LANGUAGE_ENGLISH];
    }
    [ServerManager getAllCompanyCategories:language withDelegate:self withRequestId:requestCompaniesCategories];
}
- (void)requestFinished:(ServerManagerResult *)serverManagerResult ofRequestId:(NSString *)requestId withNumberOfRequest:(int)numberOfRequest
{
    self.categoryScroll.hidden = NO;
    NSArray *jsonObjects = [serverManagerResult.jsonDict objectForKey:@"products"];
    self.arrServices = [NSMutableArray new];
    for (NSDictionary *productDict in jsonObjects) {
        ProductObject *productObject = [[ProductObject alloc] init];
        productObject.productId = [productDict objectForKey:@"productId"];
        productObject.productName = [productDict objectForKey:@"productName"];
        productObject.productMainImage = [productDict objectForKey:@"productMainImage"];
        productObject.companiesCategoryImage = [productDict objectForKey:@"companiesCategoryImage"];
        [self.arrServices addObject:productObject];
    }
    self.arrFeatureAd = [NSMutableArray new];
    self.arrFeatureAd = [[serverManagerResult.jsonDict objectForKey:@"companiesFeatureAds"] mutableCopy];
    [self.productCollection reloadData];
    [self.adCollection reloadData];
    [self performSelector:@selector(setContentSizeSubview) withObject:self afterDelay:0.2];
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
}
-(void)setContentSizeSubview
{
	if (IS_IPAD) {
		featureAdsHeight.constant = 220;
	}else
		featureAdsHeight.constant = 80;
	self.pageControl.numberOfPages=self.arrFeatureAd.count;
    subViewHeights.constant = self.productCollection.contentSize.height+44;
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark - Scrollview Delegate -
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
	CGFloat pageWidth = self.adCollection.frame.size.width;
	self.pageControl.currentPage = self.adCollection.contentOffset.x / pageWidth;
}
-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    [_customSearchBar resignFirstResponder];
}
#pragma mark - Collection View Delegate and DataSource Method -
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    if (collectionView == self.adCollection) {
        return [self.arrFeatureAd count];
    }
    return [self.arrServices count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
     if (collectionView == self.adCollection) {
         static NSString *identifier = @"BannerCell";
         ProductGridCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
         cell.productImageView.layer.cornerRadius = 5.0;
         cell.productImageView.clipsToBounds = YES;
         [cell.loadingIndicator startAnimating];
         NSString *adUrl = [[self.arrFeatureAd objectAtIndex:indexPath.item]valueForKey:@"companyFeaturedImage"];
         [cell.productImageView sd_setImageWithURL:[NSURL URLWithString:adUrl] placeholderImage:[UIImage imageNamed:@"blankImage"] options:SDWebImageRetryFailed completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
             [cell.loadingIndicator stopAnimating];
         }];
         if([[SharedData getDeviceLanguage]isEqualToString:@"ar"])
         {
             [cell.contentView setTransform:CGAffineTransformMakeScale(-1, 1)];
         }
         return cell;
     }else
     {
         UICollectionViewCell *cell;
         if (IS_IPAD) {
             cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"IpadCell" forIndexPath:indexPath];
         }else
         {
             cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
         }
    
    cell.layer.borderColor = [[UIColor grayColor]colorWithAlphaComponent:0.3].CGColor;
    ProductObject *serviceObject = [self.arrServices objectAtIndex:indexPath.row];
    UILabel *lblTitle = (UILabel *)[cell viewWithTag:101];
    lblTitle.text = serviceObject.productName;
    
    UIImageView *imgService = (UIImageView *)[cell viewWithTag:100];
    [imgService sd_setImageWithURL:[NSURL URLWithString:serviceObject.companiesCategoryImage] placeholderImage:[UIImage imageNamed:@"placeholder.png"]];
    return cell;
     }
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    CGSize cellSize;
    if (collectionView == self.adCollection) {
        cellSize.width = ([UIScreen mainScreen].bounds.size.width-20);
		if (IS_IPAD) {
			cellSize.height = 220;
		}else
        cellSize.height = 80;
        return cellSize;
    }else
    {
		if (IS_IPAD) {
			 return CGSizeMake([UIScreen mainScreen].bounds.size.width/4.0, [UIScreen mainScreen].bounds.size.width/4.0);
		}else
        return CGSizeMake([UIScreen mainScreen].bounds.size.width/3.0, [UIScreen mainScreen].bounds.size.width/3.0);
    }
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    if (collectionView == self.adCollection) {
        UserProducts *controller = [[SharedData getCurrentStoryBoard]
                                    instantiateViewControllerWithIdentifier:@"UserProducts"];
        ProductObject *commentUserObject=[[ProductObject alloc] init];
        commentUserObject.productCountryCode=[UserSetting getUserCountryCode];
        commentUserObject.productUserNumber=[[self.arrFeatureAd objectAtIndex:indexPath.item]valueForKey:@"companyProfileNumber"];
        [controller setHideTabBar:YES];
        [controller setUserProfileProductObject:commentUserObject];
        [controller showBackButton:YES];
        [self.navigationController pushViewController:controller animated:YES];
    }else
    {
        CompanyDirectoriesViewController *controller =[self.storyboard instantiateViewControllerWithIdentifier:@"CompanyDirectories"];
        ProductObject *productObject =[self.arrServices objectAtIndex:indexPath.item];
        controller.productId = productObject.productId;
        [self.navigationController pushViewController:controller animated:YES];
    }
}

#pragma mark SearchBar Delegate
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    
    [searchBar resignFirstResponder];
    if (![[self.navigationController.viewControllers lastObject] isKindOfClass:[CompanyDirectoriesViewController class]]) {
        CompanyDirectoriesViewController *companyObj = [self.storyboard instantiateViewControllerWithIdentifier:@"CompanyDirectories"];
        companyObj.searchTxt = searchBar.text;
        companyObj.search = YES;
        [self.navigationController pushViewController:companyObj animated:YES];
    }
}
-(void)searchBarTextDidEndEditing:(UISearchBar *)searchBar{
    [searchBar resignFirstResponder];
}
@end
