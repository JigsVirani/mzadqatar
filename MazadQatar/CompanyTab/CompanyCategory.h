
#import <UIKit/UIKit.h>
#import "ServerManager.h"
#import "ProductGridCell.h"
#import "GridView.h"
#import "CompanyRegistration.h"
#import "AppDelegate.h"
#import "CompanyDirectoriesViewController.h"
#import "CategoryProducts.h"
#import "SearchViewController.h"
#import "UserProducts.h"

@interface CompanyCategory : GridView

@property (weak, nonatomic) IBOutlet UITextField *search_TxtField;
@property (weak, nonatomic) IBOutlet UIPageControl *pageControl;
@property (strong,nonatomic) IBOutlet UICollectionView *adCollection,*productCollection;
@property (weak, nonatomic) IBOutlet UIView *search_View;
@property (weak,nonatomic) IBOutlet UIScrollView *categoryScroll;
@property (weak, nonatomic) IBOutlet UISearchBar *customSearchBar;

/** Outlet To Increase Font Size **/
@property(weak,nonatomic)IBOutlet UILabel *headerTitleLbl;
@property (weak, nonatomic) IBOutlet UIButton *addCompanyBtn;
/** Set width constant of Add Company **/
@property (weak,nonatomic)IBOutlet NSLayoutConstraint *addCompanyBtnWidthConstant;
@end
