//
//  ShareAppViewController.h
//  Mzad Qatar
//
//  Created by iOS Developer on 17/12/2014.
//  Copyright (c) 2014 Mohammed abdulfattah attia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SocialShareUtility.h"
#import "RateViewController.h"

#import "GAITrackedViewController.h"
@interface ShareAppViewController : GAITrackedViewController {
  SocialShareUtility *share;
  iRate *rateController;
}
@property(strong, nonatomic) IBOutlet UIImageView *imageViewupperBar;

- (IBAction)btnClose:(UIButton *)sender;

@end
