//
//  ErrorDialogueViewController.h
//  Mzad Qatar
//
//  Created by iOS Developer on 15/12/2014.
//  Copyright (c) 2014 Mohammed abdulfattah attia. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "GAITrackedViewController.h"
@interface ErrorDialogueViewController : GAITrackedViewController

- (IBAction)btnClose:(id)sender;

- (void)setDialogTitle:(NSString *)title
         andDescrition:(NSString *)descritipn
         andActionText:(NSString *)actionText;

@property(strong, nonatomic) IBOutlet UILabel *dialogTitle;

@property(strong, nonatomic) IBOutlet UILabel *dialogDescirption;
@property(strong, nonatomic) IBOutlet UILabel *actionBtn;

@property (strong,nonatomic) IBOutlet UIView *registerView;
@property (strong,nonatomic) IBOutlet UIButton *dismissButton,*registerAsComapny;
@end
